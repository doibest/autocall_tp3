<?php

/*工号信息缓存*/
$arrCacheWorkNo = array (
  'admin' => 
  array (
    'cn_name' => 'admin',
    'extension' => '8888',
    'dept_id' => '1',
  ),
  78503 => 
  array (
    'cn_name' => '',
    'extension' => '78503',
    'dept_id' => '6',
  ),
  78504 => 
  array (
    'cn_name' => '',
    'extension' => '78504',
    'dept_id' => '2',
  ),
  78505 => 
  array (
    'cn_name' => '',
    'extension' => '78505',
    'dept_id' => '3',
  ),
  78506 => 
  array (
    'cn_name' => '',
    'extension' => '78506',
    'dept_id' => '4',
  ),
  78501 => 
  array (
    'cn_name' => '',
    'extension' => '78501',
    'dept_id' => '1',
  ),
  78502 => 
  array (
    'cn_name' => '',
    'extension' => '78502',
    'dept_id' => '5',
  ),
);


/*部门信息缓存*/
$arrCacheDept = array (
  1 => 
  array (
    'dept_name' => '总公司',
    'dept_pid' => '0',
    'dept_pname' => NULL,
    'dept_leader' => 'admin',
  ),
  2 => 
  array (
    'dept_name' => '测试',
    'dept_pid' => '0',
    'dept_pname' => NULL,
    'dept_leader' => '86234',
  ),
  3 => 
  array (
    'dept_name' => '测试_子',
    'dept_pid' => '2',
    'dept_pname' => '测试',
    'dept_leader' => '78501',
  ),
  4 => 
  array (
    'dept_name' => '测试_子_子',
    'dept_pid' => '3',
    'dept_pname' => '测试_子',
    'dept_leader' => 'admin',
  ),
  5 => 
  array (
    'dept_name' => '总公司——子',
    'dept_pid' => '1',
    'dept_pname' => '总公司',
    'dept_leader' => 'admin',
  ),
  6 => 
  array (
    'dept_name' => '总公司——孙',
    'dept_pid' => '5',
    'dept_pname' => '总公司——子',
    'dept_leader' => '78505',
  ),
  7 => 
  array (
    'dept_name' => '总公司',
    'dept_pid' => '0',
    'dept_pname' => NULL,
    'dept_leader' => 'admin',
  ),
)
?>