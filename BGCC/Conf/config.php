<?php
global $arrConf;
$arrConf['dbdir'] = '/var/www/db';
$arrConf['dsn'] = array(	                               
                            "samples"   =>  "sqlite3:///$arrConf[dbdir]/samples.db",
                        );
$arrConf['basePath'] = '/var/www/html';
$arrConf['theme'] = 'default'; 

$arr_config = include("./ThinkPHP/Conf/config.php");
return $arr_config;
?>