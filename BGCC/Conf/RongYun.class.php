<?php
class ServerAPI{
	
    private $appKey;                //appKey
    private $appSecret;             //secret
    const   SERVERAPIURL = 'http://api.cn.ronghub.com/';    //请求服务地址
    private $format;                //数据格式 json/xml


    /**
     * 参数初始化
     * @param $appKey
     * @param $appSecret
     * @param string $format
     */
    public function __construct($appKey,$appSecret,$format = 'json'){
        $this->appKey = $appKey;
        $this->appSecret = $appSecret;
        $this->format = $format;
    }

    /**
     * 获取 Token 方法
     * @param $userId   用户 Id，最大长度 32 字节。是用户在 App 中的唯一标识码，必须保证在同一个 App 内不重复，重复的用户 Id 将被当作是同一用户。
     * @param $name     用户名称，最大长度 128 字节。用来在 Push 推送时，或者客户端没有提供用户信息时，显示用户的名称。
     * @param $portraitUri  用户头像 URI，最大长度 1024 字节。
     * @return json|xml
     */
    public function  getToken($userId,$name,$portraitUri,$appSec,$appKey) {
        try{
            if(empty($userId))
                throw new Exception('用户 Id 不能为空');
            if(empty($name))
                throw new Exception('用户名称 不能为空');
            if(empty($portraitUri))
                throw new Exception('用户头像 URI 不能为空');
			$postData = 'userId='.$userId.'&name='.$name.'&portraitUri='.$portraitUri;
            $ret = $this->http_post('https://api.cn.rong.io/user/getToken.json ',$postData,$appSec,$appKey);
			if(empty($ret))
                throw new Exception('请求失败');
            return $ret;
        }catch (Exception $e) {
            print_r($e->getMessage());
        }
    }
	
	
	private function http_post($url,$param,$appSec,$appKey){
		//参数初始化
		$nonce = mt_rand();
		$timeStamp = time();
		$signature = sha1($appSec.$nonce.$timeStamp);    //$appSec是平台分配
		$httpHeader = array(
			'App-Key:'.$appKey,   //平台分配
			'Nonce:'.$nonce,        //随机数
			'Timestamp:'.$timeStamp,    //时间戳
			'Signature:'.$signature,         //签名
			'Content-Type: application/x-www-form-urlencoded',
		);

		//创建http header
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_POST, 1);
			if($param != ''){
			curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		}else{
			showMsg(0,'缺少相应参数');
		}
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	
	
}
$para_sys = readS();
$rongyun = new ServerAPI($para_sys["appKey"],$para_sys["appSecret"]);


?>