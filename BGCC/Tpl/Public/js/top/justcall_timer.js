// JavaScript Document

/**
 *=================================================
 * @功能：集时通讯状态处理计时器
 * @作者：罗杰
 * @日期：2012 02 17
 *=================================================
 */

var h,m,s,hstr,mstr,sstr,timestr;
var etime = 0; //总秒数
var int_restart = 0;


/**
 *=================================================
 * @功能：定时器、计时器
 * @作者：罗杰
 * @日期：2012 02 17
 *=================================================
 */
function timer(){
	h = Math.floor(etime / 3600); //时
	m = Math.floor(etime / 60) % 60; //分
	s = Math.floor(etime % 60); //秒  
	h < 0 ? h = 0 : h = h;
	m < 0 ? m = 0 : m = m;
	s < 0 ? s = 0 : s = s; 
	h.toString().length < 2 ? hstr = "0" + h.toString() : hstr = h; //1显示01
	m.toString().length < 2 ? mstr = "0" + m.toString() : mstr = m; //1显示01
	s.toString().length < 2 ? sstr = "0" + s.toString() : sstr = s; //1显示01 
	timestr = hstr + ":" + mstr + ":" + sstr; 
	document.getElementById("status_time").innerHTML = timestr;
	etime = etime + 1;
	
}

/**
 *=================================================
 * @功能：开启服务器
 * @作者：罗杰
 * @日期：2012 02 17
 *=================================================
 */
function startTimer(etime_start){
	endTimer();
	int_restart =1 ;
	h=0;
	m=0;
	s=0;
	hstr=0;
	mstr=0;
	sstr=0;
	timestr=0;
	etime = etime_start;  
	t= window.setInterval('timer()',1000);
}

/**
 *=================================================
 * @功能：重启服务器
 * @作者：罗杰
 * @日期：2012 02 17
 *=================================================
 */
function endTimer(){
	if(int_restart==1){
	clearTimeout(t);
	return false;
	}
} 

/**
 *=================================================
 * @功能：标识时间改变
 * @作者：罗杰
 * @日期：2012 02 17
 *=================================================
 */
function monitorTimer(){
	var mark =  $("#timer_mark").attr("value");
	if(mark == 1){ 
		restart_session_timer(); //从写计时器
		startTimer(0);
		$("#timer_mark").val(0);	//设置标签为0 
	}
}

function startmonitorTimer(){
	 window.setInterval('monitorTimer()',1000);
}

/**
*=================================================
* @功能：清空计时器
* @作者：罗杰
* @日期：2012 02 17
*=================================================
*/
function restart_session_timer(){//恢复通话
		var postData = "module=justCortrol&action=RestartTimer";
		jQuery.ajax({     
		type: "POST",     
		url:  "index.php",   
		cache: false,
		data: postData,  
			success: function(data) {  
			},     
			error: function(err){      
				art.dialog.alert("错误:" + err);    
			}     
		});	
}
 
/**
 *=================================================
 * @功能：同步考勤记录
 * @作者：罗杰
 * @日期：2012 02 17
 *=================================================
 */
function reload_Synchronous(){ 

	var mark_synchronous =  $("#synchronous_mark").attr("value"); 
	var j ;
	if(mark_synchronous !="N"){   
   	//同步  
 		$("#changeStatus li a").each(function(i){
   			if($(this).attr("alt") == mark_synchronous){ //if start 
			 var fColor = $(this).css('color');  //获取状态字体颜色'
			 var imgSrc  = $(this).find("img").attr('src'); //获取状态图片路径
	 
			 //var status = $(this).attr("alt");  
			 $("#statuing img").attr('src', imgSrc);
			 $("#txt_status").html($(this).attr("name")).css('color',fColor); 	
			 $("#synchronous_mark").val("N");	//设置标签为
			 //$("#statuing img").attr('alt', status);						 
			}//if end
 		}); 
	}
}

 /**
  *=================================================
  * @功能：加载开启记录计时器
  * @作者：罗杰
  * @日期：2012 02 17
  *=================================================
  */
function reload_SynchronousTimer(){
	 window.setInterval('reload_Synchronous()',2000);
}

 