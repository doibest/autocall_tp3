

// JavaScript Document

/**
 *=================================================
 * @功能：集时通讯话务控制前端Ajax处理，公共类
 * @作者：罗杰
 * @日期：2012 02 17
 *=================================================
 */
 
function Trim (str) {
       var reg = /^\s+|\s+$/g;         
	   return str.replace(reg, "");    
};


/**
 *=================================================
 * @功能：判断是否是整数
 * @作者：罗杰
 * @日期：2012 02 17
 *=================================================
 */
function isDigit(s) { 
	var patrn=/^[0-9]{1,20}$/; 
	if (!patrn.exec(s)) 
	return false 
	return true 
}

function doSpeedDial(){
	var called =  $("#speed_dial").val() ;  
	if(called == ""){ 
		art.dialog.alert('被叫号码不能为空！');
	}else{
		clickDial(called);
		
	}
}
 
 /**
 * 功能：点击拨号
 *
 * @param 主叫号码(分机) caller
 * @param 被叫号码  called
 */
 function clickDial(called){ 
 	 
	if("" == called) { 
		art.dialog.alert('被叫号码不能为空！');
		return ;
	} 
	var postData = "module=justCortrol&action=clickDial&called=" + called;
	jQuery.ajax({     
	type: "POST",     
	url:  "index.php",   
	async: true, 
	cache: false,
	data: postData,  
		success: function(data) {
			data = Trim(data); 
			if(data=="0"){ 
				art.dialog.alert('无法呼叫！');
				return;
			}else if(data == "Success"){ 
				return;
			}else if(data = "Agent not idle"){
				art.dialog.alert('分机不可用！'); 
				return;				
			}else if(data = "Invalid agent"){
				art.dialog.alert('无效分机！');
			}else{
				art.dialog.alert('呼叫失败！'); 
				return;					
			}									
		},     
		error: function(err){        
			art.dialog.alert("错误#001:" + err); 
		}     
	});		
}


 /**
 * 功能：未接来电回复
 *
 * @param 被叫号码  called
 * @param 表名  table
 */
 function callReply(called, table){ 
 	 
	if("" == called) { 
		art.dialog.alert('被叫号码不能为空！');
		return ;
	} 
	var postData = "module=justCortrol&action=callReply&called=" + called + "&table=" + table; 
	jQuery.ajax({     
	type: "POST",     
	url:  "index.php",   
	cache: false,
	data: postData,  
		success: function(data) {  
			data = Trim(data); 
			if(data=="0"){  
				art.dialog.alert('无法呼叫！');
				return;
			}else if(data == "Success"){
				//alert("呼叫成功");
				return;
			}else if(data == "Ringing"){ 
				art.dialog.alert('主叫振铃中！');
				return;				
			}else if(data == "Busy"){ 
				art.dialog.alert('主叫不可用！');
				return;	
			}else if(data == "Agent not idle"){
				art.dialog.alert('分机不可用！'); 
				return;	
			}else if(data == "Error"){ 
				art.dialog.alert('呼叫失败！'); 
				return;				
			}else{ 
				art.dialog.alert('呼叫失败！'); 
				return;					
			}									
		},     
		error: function(err){        
			art.dialog.alert("错误#002:" + err); 
		}     
	});		
}
 
 /**
 * 功能：分机示忙
 * 作者：罗杰
 * 日期：2012 02 20

 *@parm	uuid //示忙状态 
 */
function doSetDND(uuid){
	var postData = "module=justCortrol&action=SetDND&uuid="+uuid;
	var result = "";
	jQuery.ajax({     
	type: "POST",     
	async: false, 
	url:  "index.php",   
	cache: false,
	data: postData,  
		success: function(data) {    
			result =  Trim(data);   
		},     
		error: function(err){            
			art.dialog.alert("错误#003:" + err);
		}     
	});	 
	return result;
}

 /**
 * 功能：分机示闲
 * 作者：罗杰
 * 日期：2012 02 20 
 *
 */
function doUnsetDND(){
	var postData = "module=justCortrol&action=unsetDND";
	var result = "";
	jQuery.ajax({     
	type: "POST",     
	url:  "index.php",  
	async: false,  
	cache: false,
	data: postData,  
		success: function(data) {    
			data = Trim(data); 
 			result =  data;
		},     
		error: function(err){        
			art.dialog.alert("错误#004:" + err);   
		}     
	});	
	return result;
}

 /**
 * 功能：通话质检
 * 作者：罗杰
 * 日期：2012 02 20
 */
function doFeedBack(){
	var postData = "module=justCortrol&action=feedBack";
	jQuery.ajax({     
	type: "POST",     
	url:  "index.php",   
	cache: false,
	data: postData,  
		success: function(data) { 
		    data = Trim(data); 
			if("Success" == data){//质检成功  
				art.dialog.alert("通话质检成功").time(2);  
				return;
			}else if("Error" == data){//质检失败 
				art.dialog.alert("通话质检失败");  
				return;
			}else if("Agent not in use" == data ){ 
				art.dialog.alert("当前分机不符合质检条件");  
				return;
			}else { 
				art.dialog.alert("无法质检");  
				return;
			}
		},     
		error: function(err){       
			art.dialog.alert("错误#005:" + err);  
			return;     
		}     
	});		
}

 /**
 * 功能：通话转接
 * 作者：罗杰
 * 日期：2012 02 20
 */
function doTransferCall(extension){
	var newcall = '';
	art.dialog.prompt('请输入需要转接的分机号？', function(data){ 
        newcall = data 
 		if(!isDigit(newcall)){//如果是非整数
			 art.dialog.alert("请输入正确的分机号");  
		} else{
		//转接的AJAX操作  start
		var postData = "module=justCortrol&action=transferCall&extension=" + extension+"&strExten="+newcall;
		jQuery.ajax({     
		type: "POST",     
		url:  "index.php",   
		cache: false,
		data: postData,  
			success: function(data) { 
			    data = Trim(data); 
				if("Success" == data){//成功
					art.dialog.alert("呼叫转接成功").time(2);  
					return;
				}else if("Error" == data){//失败 
					art.dialog.alert("呼叫转接失败"); 
					return;
				}else if("Agent not in use" == data ){ 
					art.dialog.alert("当前分机不在通话中"); 
				}else if("Exten is not idle" == data){ 
					art.dialog.alert("对方分机不可用"); 
				}else if("agent or exten not specified" == data){ 
					art.dialog.alert("转出分机或者被转分机为空"); 
				}else{ 
					art.dialog.alert("无效进行通话转接"); 
				}
			},     
			error: function(err){       
				art.dialog.alert("错误#006:" + err);      
			}     
		});	
		//转接的AJAX操作  end			 
		}
	}, '');
}


 /**
 * 功能：通话保持或者恢复通话
 * 作者：罗杰
 * 日期：2012 02 20
 */
function doKeepRestoreCalling(){
	//keep
	 var txt=$("#keep").attr("value");
	 if(1 == txt){
		doRestoreCalling();
	 }else{
	 	doKeepCalling();
	 }
}

 /**
 * 功能：通话保持
 * 作者：罗杰
 * 日期：2012 02 20
 */
function doKeepCalling(){  
		var postData = "module=justCortrol&action=keepCalling";
		jQuery.ajax({     
		type: "POST",     
		url:  "index.php",
		async: false, 
		cache: false,
		data: postData,  
			success: function(data) { 
				data = Trim(data);  
				if("Success" == data){//   
					$("#imgkeep").attr({'src':"images/opera/keeping.png","title":"恢复通话"});	
					$("#keep").val(1);		 
					art.dialog.alert("通话保持成功").time(2);    
					return;
				}else if("Error" == data){// 
					$("#keep").val(0);
					art.dialog.alert("通话保持失败");  
					return;
				}else if("Agent not in use" == data ){ 
					$("#keep").val(0);
					art.dialog.alert("当前分机不在通话中"); 
				}else if("Channel not exist" == data ){ 
					$("#keep").val(0);
					art.dialog.alert("通话恢复失败，对方已挂机"); 
				}else { 
					$("#keep").val(0);
					art.dialog.alert("无效操作"); 
				}
			},     
			error: function(err){ 
				$("#keep").val(0);
				art.dialog.alert("错误#007:" + err); 
			}     
		});		
}

 /**
 * 功能：恢复通话
 * 作者：罗杰
 * 日期：2012 02 20
 */
function doRestoreCalling(){//恢复通话
		var postData = "module=justCortrol&action=restoreCalling";
		jQuery.ajax({     
		type: "POST",     
		url:  "index.php",   
		cache: false,
		data: postData,  
			success: function(data) {  
				data = Trim(data);   
				if("Success" == data){// 
					art.dialog.alert("通话恢复成功").time(2);  
					$("#imgkeep").attr({'src':"images/opera/keep.gif","title":"通话保持"});
					$("#keep").val(0);	
					return;
				}else if("Error" == data){// 
					$("#keep").val(0);
					art.dialog.alert("通话恢复失败");
					return;
				}else if("Agent not in use" == data ){ 
					$("#keep").val(0);
					art.dialog.alert("当前分机不在通话中");
				}else if("Channel not exist" == data ){ 
					$("#keep").val(0);
					art.dialog.alert("通话恢复失败，对方已挂机"); 
				}else { 
					$("#keep").val(1);
					art.dialog.alert("无效操作");
				}
			},     
			error: function(err){    
				$("#keep").val(0);
				art.dialog.alert("错误#008:" + err);    
			}     
		});	
}

/**
 * 功能：三方通话
 * 作者：刘小波
 * 日期：2012 03 14
 */
function dothreewayCalling(strExten){//恢复通话
		var postData = "module=justCortrol&action=threewayCalling&strExten="+strExten;
		jQuery.ajax({     
		type: "POST",     
		url:  "index.php",   
		cache: false,
		data: postData,  
			success: function(data) {  
				data = Trim(data);
				if("Success" == data){// 
					art.dialog.alert("三方通话成功").time(2);  
					//$("#imgkeep").attr('src', "images/opera/keep.gif");
					$("#imgkeep").attr("src","images/opera/keep.png");
                 	$("#adapter").attr("src","images/opera/zj.png");
	                $("#feedback").attr("src","images/opera/jj.png");
					$("#keep").val(0);	
					return;
				}else if("Error" == data){// 
					art.dialog.alert("三方通话失败");
					return;
				}else if("create Failed" == data ){ 
					art.dialog.alert("创建三方通话失败");
				}else if("invite Failed" == data ){ 
					art.dialog.alert("邀请第三方通话失败");
				}else { 
					art.dialog.alert("无法发起三方通话");
				}
			},     
			error: function(err){      
				art.dialog.alert("错误#009:" + err);    
			}     
		});	
  }
 
 
 
 /**
 *=================================================
 * @功能：在页面加载的时候触发状态、用于获取当前状态值
 * @作者：罗杰
 * @日期：2012 03 17
 *=================================================
 */
function getOnceStaues(){
	var postData = "module=justCortrol&action=OnceStaues";
	jQuery.ajax({     
	type: "POST",     
	url:  "index.php",   
	cache: false,
	data: postData,  
		success: function(data) {  
			data = Trim(data);  
			if(1==data){
				doUnsetDND();
			}
		},     
		error: function(err){      
			art.dialog.alert("错误#010:" + err);    
		}     
	});		
}

 /**
 *=================================================
 * @功能：挂断电话
 * @作者：罗杰
 * @日期：2012 03 17
 *=================================================
 */
function doHangUp(){
	var postData = "module=justCortrol&action=HangUp"; 
	jQuery.ajax({     
	type: "POST",    
	async: false, 
	url:  "index.php",   
	cache: false,
	data: postData,  
		success: function(data) {   
				data = Trim(data);  
				if("Success" == data){// 
					window.parent.document.getElementById("pop").contentWindow.setIble();
					art.dialog.alert("挂机成功").time(1);
					return;
				}else if("Error" == data){// 
					art.dialog.alert("挂机失败");
					return;
				}else if("Agent not in use" == data ){ 
					art.dialog.alert("当前分机不在通话中");
				}else if('No such channel' == data){
					art.dialog.alert("通话不存在");
				}else { 
					art.dialog.alert("无效挂机");
				}
		},     
		error: function(err){      
			art.dialog.alert("错误#011:" + err);    
		}     
	});		
}


 /**
  *=================================================
  * @功能：加载心跳请求
  * @作者：罗杰
  * @日期：2012 02 17
  *=================================================
  */

function heartbeatTime(){
	var postData = "module=justCortrol&action=heartbeatTime"; 
	jQuery.ajax({     
	type: "POST",     
	url:  "index.php",   
	cache: false,
	data: postData,  
		success: function(data) {   
				data = Trim(data);  
		},     
		error: function(err){      
			art.dialog.alert("错误#012:" + err);    
		}     
	});
}

function heartbeat(){
	 window.setInterval('heartbeatTime()',30000);
}