<?php

class IndexAction extends Action {

	function index(){
		//检测是否有登录
		checkLogin();
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		header("Content-Type:text/html; charset=utf-8");
		$para_sys = readS();
		if($_GET["l"]){
			setcookie("lang",$_GET["l"]);
			setcookie("think_language",$_GET["l"]);
		}else{
			//dump($_COOKIE);die;
			setcookie("lang",'zh_CN');
			setcookie("think_language",'zh_CN');
		}

		$para_sys = readS();
		setcookie("think_language",$para_sys["language"]);

		$username = $_SESSION["user_info"]['username'];
		//前后台公共的
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);


		$enterprise_number = $_SESSION['user_info']['enterprise_number'];
		$r_id = $_SESSION['user_info']['r_id'];
		$arrTenantRoles = getTenantRoles();
		$thisTenantRole = $arrTenantRoles[$r_id];
		//dump($arrTenantRoles);die;

		$menu_table = "menu";
		$module_table = "module";
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = M("$menu_table");
		$where = "";
		foreach($arrAL as $val){
			$str = "find_in_set('".$val."',me.sys_info) ";
			$where .= empty($where) ? "( $str" : " OR $str";
		}
		$where .= " ) AND mo.enabled='Y' "; //AND me.pid=0
		$where_sub = "1 ";

		$user_priv = $_SESSION["user_priv"];
		foreach($user_priv as $key=>$val){
			if( in_array($key,$thisTenantRole)){
				$arrK[] = $key;
			}
			foreach($user_priv[$key] as $k=>$v){
				if( in_array($k,$thisTenantRole) ){
					$arrK[] = $k;
				}
			}
		}
		$arrMData = $menu->table("$menu_table me")->field("me.id AS id,me.icon2 as iconCls,me.name,me.moduleclass, me.order,me.url,me.pid AS pid")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where($where)->order("`order` ASC")->select();

		$arrMD = $this->getTree($arrMData,"0","pid","id");
		//dump($arrMD);die;
		$_SESSION['menu'] = Array();//存放子菜单的父菜单，为以后判断子菜单的权限的方便
		foreach($arrMD as $key=>&$val){
			//非admin用户只看已有的权限
			if( !in_array($val["name"],$arrK)  && $username != 'admin' ){
				unset($arrMD[$key]);
				continue;
			}
			$pid_menu = $val["name"];
			foreach($val["subMenu"] as $km=>&$vm){
				//非admin用户只看已有的权限
				if( !in_array($vm["name"],$arrK) ){
					unset($val["subMenu"][$km]);
					continue;
				}
				$_SESSION['menu'][$vm['name']] = $pid_menu;
			}
			//非admin用户只看已有的权限
			//sort($val["subMenu"]);
			$val["subMenu"] = $this->array_sort($val["subMenu"],'order','asc',"no");
		}
		//dump($arrMD);die;
		//sort($arrMD);
		$arrMenu = $this->array_sort($arrMD,'order','asc',"no");
		$this->assign("menu",$arrMenu);
		//echo $menu->getLastSql();
		//dump($arrMenu);die;
		//dump($tpl_menu);die;
		$logo_replace = M("logo_replace");
		$arrLogo = $logo_replace->where("id = '2'")->find();
		$logo_img = $arrLogo["logopath"].$arrLogo["logoname"];
		$this->assign("logo",$logo_img);
        $this->assign("system",$para_sys);
		$this->assign("user_info",$_SESSION["user_info"]);

		//前台界面
		//求出初始化登陆进去的状态
		$pbx_ip = "127.0.0.1";
		$this->assign("pbx_ip",$pbx_ip);
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$arrHint = $bmi->getHint($_SESSION["user_info"]['extension']);
		//dump($arrHint);die;
		$DND_status = $bmi->getDNDAll();
		$dnd = $DND_status[$_SESSION["user_info"]['extension']];
		if($dnd){
			$this->assign("hint","PutDND");
		}else{
			$stat = $arrHint["stat"];
			if($stat == "Idle"){
				$hint = "DelDND";
			}elseif($stat == "InUse"){
				$hint = "Answer";
			}elseif($stat == "Unavailable"){
				$hint = "UnRegistered";
			}else{
				$hint = $stat;
			}
			$this->assign("hint",$hint);
		}
		//dump($hint);die;
		//前台进入管理界面的权限
		$menuname = "Seating monitoring";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$arrAdmin = getAdministratorNum();
		if( in_array($username,$arrAdmin) ){
			$this->assign("agent_manage","Y");
		}else{
			$this->assign("agent_manage",$priv['agent_manage']);
		}

		$in_customer_menu = "Client system";
		if( in_array("inbound",$arrAL) || in_array($in_customer_menu,$arrTenantRoles[$r_id]) ){
			$wj_in = "Y";
		}else{
			$wj_in = "N";
		}
		$out_customer_menu = "Telemarketing";
		if( in_array("outbound",$arrAL) || in_array($out_customer_menu,$arrTenantRoles[$r_id]) ){
			$wj_out = "Y";
		}else{
			$wj_out = "N";
		}
		if( in_array("IPPBX",$arrAL) ){
			$wj_IPPBX = "Y";
		}else{
			$wj_IPPBXt = "N";
		}
		if($wj_in == "Y" && $wj_out == "Y"){
			$sys_type = "all";
		}elseif($wj_in == "Y"){
			$sys_type = "inbound";
		}elseif($wj_out == "Y"){
			$sys_type = "outbound";
		}elseif($wj_IPPBX == "Y"){
			$sys_type = "IPPBX";
		}
		$this->assign("sys_type",$sys_type);

		$Workflow_menu = "Workflow Settings";
		if( in_array("wf",$arrAL) && in_array($Workflow_menu,$arrTenantRoles[$r_id]) ){
			$this->assign("Workflow","Y");
		}else{
			$this->assign("Workflow","N");
		}

		$Exam_menu = "Exam Training Management";
		if( in_array("ks",$arrAL) && in_array($Exam_menu,$arrTenantRoles[$r_id]) ){
			$this->assign("ExamTraining","Y");
		}else{
			$this->assign("ExamTraining","N");
		}


		$order_info = M("order_info");
		$start_time1 = date("Y-m-d")." 00:00:00";
		$end_time1 = date("Y-m-d")." 23:59:59";
		$where_order = "createtime>='$start_time1' AND createtime<='$end_time1'";
		$arrOrderData = $order_info->order("sum(goods_amount) desc")->field("createname,sum(goods_amount) as goods_amount,count(*) as order_number")->where($where_order)->group("createname")->limit("3")->select();
		$i = 0;
		foreach($arrOrderData as $val){
			$arrOrderData[$i]["order_sort"] = $i+1;
			$i++;
		}
		//echo $order_info->getLastSql();die;
		$this->assign("arrOrderData",$arrOrderData);
		//dump($arrOrderData);die;

		//外呼平台
		$task = M("sales_task");
		$user_info = session("user_info");
		$list = $task->where("dept_id='". $user_info["d_id"]  ."' and enable='Y'")->order("createtime desc")->select();
		$arrT = $bmi->getAllQueueMember();
		$n = 0;
		foreach($list as &$val){
			if($val["calltype"] == "prediction"){
				$val["calltype"] = "autocall";
			}
			$id = $val["id"];
			//$myQueueName = "AutoCall_".$db_name."_".$id;
			$myQueueName = "AutoCall_".$id;
			if( array_key_exists($myQueueName,$arrT) ){
				$val['queueStatus'] = $arrT[$myQueueName]['membername'];
			}
			$n++;
		}
		unset($n);
		//dump($arrT);die;

		$exten = $_SESSION["user_info"]["extension"];
		$i = 0;
		foreach($list as &$val){
			if($val["calltype"] == "autocall"){
				if( in_array($exten,$val["queueStatus"]) ){
					$list[$i]["extenStatus"] = "<span id='Checked_In".$val["id"]."' style='color:red;'><span  class='checkedIn' taskId='".$val["id"]."'>已签入</span></span>";
				}else{
					$list[$i]["extenStatus"] = "<span class='checkedOut' id='Not_checked".$val["id"]."'>未签入</span>";
				}
			}
			$task_name[$val["id"]] = $val["name"];
			$i++;
		}

		$this->assign("taskId",$maxTaskId);
		$this->assign("task_name",json_encode($task_name));

		$this->assign("task",$list);

		//呼入平台
		if( in_array($username,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$username);
		}
		$menuname_cm = "Customer Data";
		$p_menuname_cm = $_SESSION['menu'][$menuname_cm]; //父菜单
		$priv_cm = $_SESSION["user_priv"][$p_menuname_cm][$menuname_cm];
		if($para_sys["agent_customer_menu"] != "Y"){
			$priv_cm["add"] = "Y";
			$priv_cm["edit"] = "Y";
			//$priv_cm["delete"] = "Y";
			$priv_cm["transmission"] = "Y";
		}
		$this->assign("priv",$priv_cm);

		//销售漏斗
		$menuname1 = "Sales Funnel";
		$p_menuname1 = $_SESSION['menu'][$menuname1]; //父菜单
		$priv_Sales = $_SESSION["user_priv"][$p_menuname1][$menuname1];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv_Sales["view"] == "Y"){
				$this->assign("priv_Sales","Y");
			}else{
				$this->assign("priv_Sales","N");
			}
		}else{
			$this->assign("priv_Sales","Y");
		}

		//服务记录
		$menuname2 = "Service records";
		$p_menuname2 = $_SESSION['menu'][$menuname2]; //父菜单
		$priv1_Service = $_SESSION["user_priv"][$p_menuname2][$menuname2];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Service["view"] == "Y"){
				$this->assign("priv1_Service","Y");
			}else{
				$this->assign("priv1_Service","N");
			}
		}else{
			$this->assign("priv1_Service","Y");
		}

		//订单列表
		$menuname3 = "Order Management";
		$p_menuname3 = $_SESSION['menu'][$menuname3]; //父菜单
		$priv1_Order = $_SESSION["user_priv"][$p_menuname3][$menuname3];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Order["view"] == "Y"){
				$this->assign("priv1_Order","Y");
			}else{
				$this->assign("priv1_Order","N");
			}
		}else{
			$this->assign("priv1_Order","Y");
		}
		$order_menu = "Commodity Management";
		if( in_array($order_menu,$arrTenantRoles[$r_id]) ){
			$this->assign("order_menu","Y");
		}else{
			$this->assign("order_menu","N");
		}


		//回访记录列表
		$menuname4 = "Visit Record";
		$p_menuname4 = $_SESSION['menu'][$menuname4]; //父菜单
		$priv1_Visit_Record = $_SESSION["user_priv"][$p_menuname4][$menuname4];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Visit_Record["view"] == "Y"){
				$this->assign("priv1_Visit_Record","Y");
			}else{
				$this->assign("priv1_Visit_Record","N");
			}
		}else{
			$this->assign("priv1_Visit_Record","Y");
		}

		//回访内容列表
		$menuname5 = "Visit Content";
		$p_menuname5 = $_SESSION['menu'][$menuname5]; //父菜单
		$priv1_Visit_Content = $_SESSION["user_priv"][$p_menuname5][$menuname5];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Visit_Content["view"] == "Y"){
				$this->assign("priv1_Visit_Content","Y");
			}else{
				$this->assign("priv1_Visit_Content","N");
			}
		}else{
			$this->assign("priv1_Visit_Content","Y");
		}

		//转交历史
		$menuname6 = "Forwarded History";
		$p_menuname6 = $_SESSION['menu'][$menuname6]; //父菜单
		$priv1_History = $_SESSION["user_priv"][$p_menuname6][$menuname6];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_History["view"] == "Y"){
				$this->assign("priv1_History","Y");
			}else{
				$this->assign("priv1_History","N");
			}
		}else{
			$this->assign("priv1_History","Y");
		}

		//服务记录
		$menuname7 = "Work Order reports";
		$p_menuname7 = $_SESSION['menu'][$menuname7]; //父菜单
		$priv1_Work = $_SESSION["user_priv"][$p_menuname7][$menuname7];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Work["view"] == "Y"){
				$this->assign("priv1_Work","Y");
			}else{
				$this->assign("priv1_Work","N");
			}
		}else{
			$this->assign("priv1_Work","Y");
		}

		//客户资料--相关文件
		$menuname8 = "Related Documents";
		$p_menuname8 = $_SESSION['menu'][$menuname8]; //父菜单
		$priv1_File = $_SESSION["user_priv"][$p_menuname8][$menuname8];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_File["view"] == "Y"){
				$this->assign("priv1_File","Y");
			}else{
				$this->assign("priv1_File","N");
			}
		}else{
			$this->assign("priv1_File","Y");
		}

		//后台界面
		if( check_reload()){
			$this->assign("reload_need","");
		}else{
			$this->assign("reload_need","display:none;");
		}

		$themes_color = $_SESSION["themes_color_$username"];
		if($themes_color){
			$this->assign("themes_color",$themes_color);
		}else{
			$users_themes = M("users_themes");
			$arrFCT = $users_themes->where("username = '$username'")->find();
			$themesColor = empty($arrFCT["themes_color"]) ? "blue" : $arrFCT["themes_color"];
			$this->assign("themes_color",$themesColor);
		}

		$role_web = $_SESSION["user_info"]["interface"];
		$this->assign("role_web",$role_web);

		//头像
		$user_img = $_SESSION["user_info"]["user_img"];
		$file_user_img = "include/data/headImg/".$user_img;
		if( $user_img && file_exists($file_user_img) ){
			$this->assign("user_img",$file_user_img);
		}else{
			$this->assign("user_img","Agent/Tpl/public/html5/img/no-user.jpg");
		}

		$this->display();
	}


	function getTree($data, $pId,$field_pid,$field_id) {
		$tree = '';
		foreach($data as $k =>$v) {
			if($v[$field_pid] == $pId)    {
				$v['subMenu'] = $this->getTree($data, $v[$field_id],$field_pid,$field_id);
				if ( empty($v["subMenu"])  ) {
					unset($v['subMenu']) ;
				}
				$tree[] = $v;
			}
		}
		return $tree;
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	function setThemes(){
		$username = $_SESSION["user_info"]['username'];
		$themes_color = $_REQUEST["themes_color"];
		$_SESSION["themes_color_$username"] = $themes_color;
		if($themes_color){
			echo json_encode(array('success'=>true,'data'=>$_SESSION["themes_color_$username"]));
		}else{
			echo json_encode(array('msg'=>"没有主题颜色"));
		}
	}

    public function index_tabs(){
		checkLogin();
		if($_GET["l"]){
			setcookie("lang",$_GET["l"]);
			setcookie("think_language",$_GET["l"]);
		}else{
			//dump($_COOKIE);die;
			setcookie("lang",'zh_CN');
			setcookie("think_language",'zh_CN');
		}

		$para_sys = readS();
		setcookie("think_language",$para_sys["language"]);

		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);

		if($_REQUEST['agent_username']){
			//if(!$_SESSION["user_info"]){
			if(!$_SESSION["user_info"] || $_SESSION['user_info']['username']!=$_REQUEST['agent_username']){
				header("Location:index.php?m=Index&a=Login");
			}
		}else{
			if(!$_SESSION["user_info"]){
				header("Location:index.php?m=Index&a=Login");
			}
			if($_SESSION["user_info"]["interface"] == "pbx"){
				goback("pbx用户不能登录！");
			}
			if($_SESSION["user_info"]["interface"] == "dimission"){
				goback("离职员工不能登录！");
			}
			if($_SESSION["user_info"]["interface"] == "agent"){
				if( strstr($checkRole[2],"wechat") ){
					$checkRole[2] = trim(str_replace(",wechat","",$checkRole[2]));
				}
				if( $checkRole[2] == "IPPBX" ){
					goback("您装的IPPBX系统，没有坐席界面！");
				}

				if( $checkRole[2] == "interface" ){
					goback("您装的使用接口系统，除admin外没有可使用界面！");
				}

				$username = $_SESSION["user_info"]["username"];
				$users = new Model("users");
				$users->where("username='$username'")->save(array("login_status"=>"Y"));
				$this->loginUserCaChe();

				//dump($_SESSION);die;
				//判断是否已经登录了
				$report = M('report_agent_login');
				$arrTmp = $report->where("username='$username' AND status='登录中'")->find();

				//if( $arrTmp ){
				if( 0 ){
					$logintime	=	$arrTmp['logintime'];
					goBack("您于 {$logintime} 已经在其它坐席端登录,请先注销后再登录或者联系管理员!","");
				}else{
					//记录登录报表到数据表report_agent_login
					$logintime = Date('Y-m-d H:i:s');
					$arr = Array(
						'username'	=> $_SESSION["user_info"]["username"],
						'logintime'	=>	$logintime,
						'lastheartbeat'	=>	$logintime,
						'status'	=>	'登录中',
						'during'	=>	0
					);
					$report->data($arr)->add();
					header("Location:agent.php");
				}
			}
		}
		if( check_reload()){
			$this->assign("reload_need","");
		}else{
			$this->assign("reload_need","display:none;");
		}

		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}

		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model("$menu_table");
		$mainMenu = $menu->table("$menu_table me")->field("me.id AS id,name,me.icon,moduleclass,order,url,me.pid AS pid")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where("me.pid=0 AND enabled='Y'")->order("`order` ASC")->select();
		//dump($mainMenu);die;

		$_SESSION['menu'] = Array();//存放子菜单的父菜单，为以后判断子菜单的权限的方便
		foreach( $mainMenu AS $row ){
			$mainId = $row['id'];
			//$subMenu = $menu->where("pid=$mainId")->order("`order` ASC")->select();
			$subMenu = $menu->table("$menu_table me")->field("me.id AS id,me.icon as iconCls,name,moduleclass, order,url,me.pid AS pid")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			if( 'admin'==$_SESSION["user_info"]['username'] ){//如果是管理员，默认显示所有模块加载的菜单
				/*
				$tpl_menu[$row['name']]['thisMenu'] = $row['name'];
				$tpl_menu[$row['name']]['iconCls'] = $row['icon'];
				$tpl_menu[$row['name']]['subMenu'] = $subMenu;
				*/
				//下面这样做，是为了让管理员过期后只显示一个菜单
				if( array_key_exists($row['name'],$_SESSION["user_priv"]) ){
					$tpl_menu[$row['name']]['thisMenu'] = $row['name'];//一级菜单应该显示
					$tpl_menu[$row['name']]['iconCls'] = $row['icon'];//一级菜单应该显示
					foreach( $subMenu AS $arrRow ){
						if( array_key_exists($arrRow['name'],$_SESSION["user_priv"][$row['name']]) ){
							$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
							$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
						}
					}
				}

			}else{//非管理员
				//$_SESSION["user_priv"]是权限数组
				//dump($_SESSION["user_priv"]);die;

				if( array_key_exists($row['name'],$_SESSION["user_priv"]) ){
					$tpl_menu[$row['name']]['thisMenu'] = $row['name'];//一级菜单应该显示
					$tpl_menu[$row['name']]['iconCls'] = $row['icon'];//一级菜单应该显示
					//下面查询一级菜单下面的二级菜单有哪些应该显示

					foreach( $subMenu AS $arrRow ){
						if( array_key_exists($arrRow['name'],$_SESSION["user_priv"][$row['name']]) ){
							$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
							$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
						}
					}
				}
			}
		}
		//$para_sys = readS();
		//dump($tpl_menu);die;

		$logo_replace = new Model("logo_replace");
		$arrLogo = $logo_replace->where("id = '2'")->find();
		$logo_img = $arrLogo["logopath"].$arrLogo["logoname"];
		$this->assign("logo",$logo_img);
		//dump($_SESSION["user_info"]);die;
        $this->assign("system",$para_sys);
		$this->assign("userinfo",$_SESSION["user_info"]);
		$this->assign("menu",$tpl_menu);

		$paraSys = readS();
		$version = $paraSys["version_num"];
		$name = "/var/www/html/data/version/robotupdate-".$version."-*.tar.gz";
		//$arrFile = glob("/var/www/html/data/version/*.tar.gz");
		$arrFile = glob($name);
		$fileName = array_pop(explode("/",$arrFile[0]));
		$arrV = explode(".",$fileName);
		$versionTmp = explode("-",$arrV[0]);
		$next_version = $versionTmp[3]."-".$versionTmp[4];
		//dump($version);
		//dump($arrFile);die;

		//用户选择性升级
		if($paraSys["Secondary_development"] != "Y"){   //没做过二次开发的可以升级
			if($paraSys["automaticUpgrades"] == "N"){    //升级提醒
				$this->assign("automaticUpgrades","N");
				if($arrFile){
					$this->assign("upgrade","Y");
					$this->assign("fileName",$fileName);
					$this->assign("next_version",$next_version);
					$this->assign("version",$version);
				}else{
					$this->assign("upgrade","N");
				}
			}else{
				if($arrFile){
					$this->assign("automaticUpgrades","Y");  //自动升级
					$this->assign("fileName",$fileName);
				}else{
					$this->assign("upgrade","N");
				}
			}
		}

		/*
		//广告、新闻等弹窗
		$arrMsg = $this->versionNewsData();
		if($arrMsg){
			$i = 0;
			foreach($arrMsg as $val){
				if( mb_strlen($val["content"]) < 50){
					$arrMsg[$i]["keyword"] = $val["content"];
				}else{
					$arrMsg[$i]["keyword"] = mb_substr($val["content"],0,50,'utf-8')."...";
				}
				$arrMsg[$i]["index"] = $i;
				$i++;
			}
			unset($i);
			if(count($arrMsg)>0){
				$this->assign("arrMsg",$arrMsg);
				$this->assign("sysMessage","Y");
			}else{
				$this->assign("sysMessage","N");
			}
		}

		//强制升级------做过二次开发的也要强制升级
		$version2 = file_get_contents("/var/www/html/BGCC/Conf/forcedUp.txt");
		$name2 = "/var/www/html/data/forcedUpgrade/robotforced-".$version2."-*.tar.gz";
		$arrFile2 = glob($name2);
		$fileName2 = array_pop(explode("/",$arrFile2[0]));
		if($arrFile2){
			$this->assign("forcedUpgrades","Y");  //强制升级
			$this->assign("fileName2",$fileName2);
		}else{
			$this->assign("forcedUpgrades","N");


			//强制升级------做过二次开发的不给强制升级
			$name3 = "/var/www/html/data/forcedUpgrade/robotupdate-".$version."-*.tar.gz";
			$arrFile3 = glob($name3);
			$fileName3 = array_pop(explode("/",$arrFile3[0]));
			if($arrFile3){
				if($paraSys["Secondary_development"] != "Y"){  //做过二次开发的不给强制升级
					$this->assign("forcedUpgrades_web","Y");  //强制升级
					$this->assign("fileName3",$fileName3);
				}else{
					$this->assign("forcedUpgrades_web","N");
				}
			}else{
				$this->assign("forcedUpgrades_web","N");
			}
		}


		//dump($arrH);
		//dump($arrMsg);die;
		*/


		$menu_count = count($tpl_menu);
		if($menu_count>13){
			$this->assign("height","90px;");
			$this->assign("img_name","toolbar_bg_93");
		}else{
			$this->assign("height","63px;");
			$this->assign("img_name","toolbar_bg");
		}


		$this->display();
    }

	function versionNewsData(){
		set_time_limit(0);
		$ctx = stream_context_create(array(
			'http' => array(
				'timeout' => 10    //设置一个超时时间，单位为秒
			)
		));
		$arrData = @file_get_contents("http://al.robot365.com/Index/Conf/verisonNews.php",0,$ctx);
		$arrF = json_decode($arrData,true);
		$nowtime = date("Y-m-d H:i:s");
		foreach($arrF as $val){
			if( strtotime($val["starttime"]) <= strtotime($nowtime) && strtotime($val["endtime"]) >= strtotime($nowtime) && $val["status"] == "Y" ){
				$arrT[] = $val;
			}
		}

		//去掉用户已经查看了的新闻
		$user_name = $_SESSION["user_info"]["username"];
		$arrDT = require "BGCC/Conf/newsMessage.php";
		$arrTD = $arrDT["markNews_".$user_name];

		foreach($arrTD as $val){
			$arrID[] = array_shift(explode(",",$val));
		}
		//dump($arrID);die;
		if($arrID){
			foreach($arrT as $val){
				if( !in_array($val["id"],$arrID) ){
					$arrH[] = $val;
				}
			}
			return $arrH;
		}else{
			return $arrT;
		}
	}

	function viewNewsData(){
		set_time_limit(0);
		$ctx = stream_context_create(array(
			'http' => array(
				'timeout' => 10    //设置一个超时时间，单位为秒
			)
		));
		$arrData = @file_get_contents("http://al.robot365.com/Index/Conf/verisonNews.php",0,$ctx);
		$arrF = json_decode($arrData,true);
		$nowtime = date("Y-m-d H:i:s");
		foreach($arrF as $val){
			if( $val["starttime"] <= $nowtime && $val["endtime"] >= $nowtime && $val["status"] == "Y" ){
				$arrT[] = $val;
			}
		}
		return $arrT;
	}

	function markNews(){
		$id = $_REQUEST["id"];
		$endtime = $_REQUEST["endtime"];
		$username = $_SESSION["user_info"]["username"];
		$msg = $id.",".$endtime;
		//$username = "test806";
		//$arrData = array("markNews_".$username => $id);
		$arrData = array("markNews_".$username => array($msg));
		$arrT = require "BGCC/Conf/newsMessage.php";
		$keyname = "markNews_".$username;

		if(is_array($arrT)){
			foreach($arrT[$keyname] as $val){
				$arrID[] = array_shift(explode(",",$val));
			}

			if(array_key_exists($keyname,$arrT)){
				if( !in_array($id,$arrID) ){
					array_push($arrT[$keyname],$msg);
				}
			}else{
				//array_push($arrT,$arrData);
				foreach($arrData as $key=>$value) {
					foreach($value as $k=>$v) {
					  $arrT[$key][$k] = $v;
					}
				}
			}
		}else{
			$arrT = $arrData;
		}


		//已经过期的数据 删除掉
		$nowtime = date("Y-m-d H:i:s");
		foreach($arrT[$keyname] as $val){
			if( strtotime(substr($val,-19)) < strtotime($nowtime) ){
				unset($val);
			}else{
				$tmp_arr[] = $val;
			}
		}
		$arrT[$keyname] = $tmp_arr;


		$content = "<?php\nreturn " .var_export($arrT,true) .";\n ?>";
		file_put_contents("BGCC/Conf/newsMessage.php",$content);
	}


	function seeMoreNews(){
		$type = $_REQUEST["type"];
		/*
		$arrMsg = $this->viewNewsData();
		if($type == "one"){
			$id = $_REQUEST["id"];
			foreach($arrMsg as $val){
				if($val["id"] == $id){
					$arrM[] = $val;
				}
			}
			$this->assign("arrMsg",$arrM);
		}else{
			$this->assign("arrMsg",$arrMsg);
		}
		//dump($arrM);die;
		*/
		$this->display();
	}

	public function controlPanel(){
		checkLogin();

		$this->display();
	}
	public function pbx(){
		checkLogin();
		$this->display();
		$pbx->addextension();
	}


    function Login(){
        header("Content-Type:text/html; charset=utf-8");

		$logo_img = getLogoImg();
		$this->assign("logo",$logo_img[1]);
		$this->assign("background",$logo_img[3]);
		/*
		$para_sys = readS();
		$this->assign("url",$para_sys["official_url"]);
		*/

		$system_set = new Model("bgcrm.system_set");
		$arr = $system_set->where("`name`='official_url'")->find();
		$this->assign("url",$arr["svalue"]);


		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("inbound",$arrAL) ){
			$wj_in = "Y";
		}else{
			$wj_in = "N";
		}
		if( in_array("outbound",$arrAL) ){
			$wj_out = "Y";
		}else{
			$wj_out = "N";
		}
		if( in_array("IPPBX",$arrAL) ){
			$wj_IPPBX = "Y";
		}else{
			$wj_IPPBXt = "N";
		}
		if($wj_in == "Y" && $wj_out == "Y"){
			$content = @file_get_contents("data/system/admin-all.php");
		}elseif($wj_in == "Y"){
			$content = @file_get_contents("data/system/admin-inbound.php");
		}elseif($wj_out == "Y"){
			$content = @file_get_contents("data/system/admin-outbound.php");
		}elseif($wj_IPPBX == "Y"){
			$content = @file_get_contents("data/system/admin-ippbx.php");
		}
		$role = new Model("role");
		$roleData = $role->where("r_id=1")->find();
		$systype = explode("-",$roleData["system_type"]);

		$arrData = array(
			"action_list"=>$content,
			"system_type"=>"Y-".$checkRole[2],
		);
		if( $systype[0] == "N" ){
			$role->data($arrData)->where("r_id = 1")->save();
		}else{
			if($checkRole[2] != $systype[1]){
				$role->data($arrData)->where("r_id = 1")->save();
			}
		}
		//dump($arrData);die;

		//判断租户权限--有：Y，无：N
		if( in_array("zh",$arrAL) ){
			$tenant_enable = "Y";
		}else{
			$tenant_enable = "N";
		}
		//dump($tenant_enable);die;
		$this->assign("tenant_enable",$tenant_enable);

        $this->display("","utf-8");
    }
    public function newUserLogin($value='')
    {
$warrant_role = empty($_SESSION["warrant_role"]) ? file_get_contents("data/system/warrant.php") : $_SESSION["warrant_role"];
        if(empty($_POST['username'])) {
            goBack(L("The username must be filled")."！","");
        }elseif (empty($_POST['password'])){
            goBack("密码不能为空！","");
        }
		$sql = "SELECT * FROM $db_name.users u LEFT JOIN $db_name.role r ON u.r_id=r.r_id  WHERE username='". $_POST["username"] ."'";
		$user = M("$db_name.users");
		$arrUser = $user->query($sql);
		$arrUser = $arrUser[0];
		$tenant_enable = $_REQUEST["tenant_enable"];
		$db_name = '';
		if($tenant_enable == "Y"){
			$workno = $_POST["username"];
			if($workno == 'admin'){ //管理员登录
				$enterprise_number = 'admin';
				$db_name = 'bgcrm';
			}else{
				$where = "enterprise_number = '$workno' ";
				if( is_numeric($workno) ){ //非数字，一定是租户管理员
					$where .= "  OR ($workno >= start_work_number AND $workno <= end_work_number)";
				}
				$tenants = M("bgcrm.tenants");
				$arr = $tenants->where($where)->find();
				//dump($arr);die;
				if($arr){
					$enterprise_number = $arr['enterprise_number'];
					$start_lease_time = $arr['start_lease_time'];
					$end_lease_time = $arr['end_lease_time'];
					if($enterprise_number == "admin"){
						//如果是运营商--用bgcrm数据库
						$db_name = 'bgcrm';
					}else{
						$db_name = 'bgcrm' .$arr['id'];
					}
				}else{
					goback("租户工号不存在，请联系运营商开户！");
				}
			}

			$nowDate = Date('Y-m-d');
			if( $db_name !== 'bgcrm' ){
				if($nowDate <$start_lease_time){
					goback("租户已经开户，但当还未到使用日期！请在" .$start_lease_time."日后再登录系统！","index.php?m=Index&a=Login");
				}else if($nowDate >$end_lease_time){
					goback("租用时间已过期，如需登陆，请联系运营商延期！","index.php?m=Index&a=Login");
				}else{
					//使用中
				}
			}

		}else{
			$db_name = 'bgcrm';
			$isExpired = false; //专门为管理员设置的标记
			$date = strtotime(date("Y-m-d"));
			$checkRole = getSysinfo();
			if( $_POST['username'] != "admin" ){
				$expireDate = strtotime($checkRole[1]);
				if( $date > $expireDate || $warrant_role == "N" ){
					goback("该系统已经过试用期或者授权已收回，请联系管理员","index.php?m=Index&a=Login");
				}
				$lic = $checkRole[3];
				if( $lic != '1'){
					goback("授权文件错误！","index.php?m=Index&a=Login");
				}
				if( $checkRole[2] == "interface" ){
					goback("您装的使用接口系统，除admin外没有可使用界面！");
				}
			}else{
				$expireDate = strtotime($checkRole[1]);
				if( $date > $expireDate ){
					$isExpired = true; //专门为管理员设置的标记
				}

			}

		}
		//dump($db_name);die;
		$sql = "SELECT * FROM $db_name.users u LEFT JOIN $db_name.role r ON u.r_id=r.r_id  WHERE username='". $_POST["username"] ."'";
		$user = M("$db_name.users");
		$arrUser = $user->query($sql);
		$arrUser = $arrUser[0];

		if( !$arrUser["username"] ){
			goBack(L("Without this user"),"");
		}elseif($arrUser["password"] != $_POST["password"]){
			goBack(L("Wrong password"),"");
		}elseif($arrUser["interface"] == "pbx"){
			goBack("PBX用户不能登录！","");
		}else{
			 $_SESSION["user_info"] = $arrUser;
			 $_SESSION["user_priv"] = json_decode( $arrUser['action_list'],true);//权限
			 //dump($_SESSION["user_priv"]);die;
			 //下面专门让admin过期后登陆只有 上载授权文件这一个菜单页面
			 if($tenant_enable != "Y"){
				 if( ($_POST['username'] == "admin" && $isExpired) || $warrant_role == "N"  ){
					$_SESSION["user_priv"] = Array(
						"Information Settings"	=> Array(
							"System Infomation"	=> Array("view"=>'Y'),
						)
					);
				 }
			 }
			 $_SESSION["db_name"] = $db_name;
			 cookie('user_info',$arrUser);
			 header("Location:index.php");
			 //header("Location:index.php?m=Index&a=layout");
		}
    }

    function userLogin(){
		$warrant_role = empty($_SESSION["warrant_role"]) ? file_get_contents("data/system/warrant.php") : $_SESSION["warrant_role"];
        if(empty($_POST['username'])) {
            goBack(L("The username must be filled")."！","");
        }elseif (empty($_POST['password'])){
            goBack("密码不能为空！","");
        }
		$sql = "SELECT * FROM $db_name.users u LEFT JOIN $db_name.role r ON u.r_id=r.r_id  WHERE username='". $_POST["username"] ."'";
		$user = M("$db_name.users");
		$arrUser = $user->query($sql);
		$arrUser = $arrUser[0];
		$tenant_enable = $_REQUEST["tenant_enable"];
		$db_name = '';
		if($tenant_enable == "Y"){
			$workno = $_POST["username"];
			if($workno == 'admin'){ //管理员登录
				$enterprise_number = 'admin';
				$db_name = 'bgcrm';
			}else{
				$where = "enterprise_number = '$workno' ";
				if( is_numeric($workno) ){ //非数字，一定是租户管理员
					$where .= "  OR ($workno >= start_work_number AND $workno <= end_work_number)";
				}
				$tenants = M("bgcrm.tenants");
				$arr = $tenants->where($where)->find();
				//dump($arr);die;
				if($arr){
					$enterprise_number = $arr['enterprise_number'];
					$start_lease_time = $arr['start_lease_time'];
					$end_lease_time = $arr['end_lease_time'];
					if($enterprise_number == "admin"){
						//如果是运营商--用bgcrm数据库
						$db_name = 'bgcrm';
					}else{
						$db_name = 'bgcrm' .$arr['id'];
					}
				}else{
					goback("租户工号不存在，请联系运营商开户！");
				}
			}

			$nowDate = Date('Y-m-d');
			if( $db_name !== 'bgcrm' ){
				if($nowDate <$start_lease_time){
					goback("租户已经开户，但当还未到使用日期！请在" .$start_lease_time."日后再登录系统！","index.php?m=Index&a=Login");
				}else if($nowDate >$end_lease_time){
					goback("租用时间已过期，如需登陆，请联系运营商延期！","index.php?m=Index&a=Login");
				}else{
					//使用中
				}
			}

		}else{
			$db_name = 'bgcrm';
			$isExpired = false; //专门为管理员设置的标记
			$date = strtotime(date("Y-m-d"));
			$checkRole = getSysinfo();
			if( $_POST['username'] != "admin" ){
				$expireDate = strtotime($checkRole[1]);
				if( $date > $expireDate || $warrant_role == "N" ){
					goback("该系统已经过试用期或者授权已收回，请联系管理员","index.php?m=Index&a=Login");
				}
				$lic = $checkRole[3];
				if( $lic != '1'){
					goback("授权文件错误！","index.php?m=Index&a=Login");
				}
				if( $checkRole[2] == "interface" ){
					goback("您装的使用接口系统，除admin外没有可使用界面！");
				}
			}else{
				$expireDate = strtotime($checkRole[1]);
				if( $date > $expireDate ){
					$isExpired = true; //专门为管理员设置的标记
				}

			}

		}
		//dump($db_name);die;
		$sql = "SELECT * FROM $db_name.users u LEFT JOIN $db_name.role r ON u.r_id=r.r_id  WHERE username='". $_POST["username"] ."'";
		$user = M("$db_name.users");
		$arrUser = $user->query($sql);
		$arrUser = $arrUser[0];

		if( !$arrUser["username"] ){
			goBack(L("Without this user"),"");
		}elseif($arrUser["password"] != $_POST["password"]){
			goBack(L("Wrong password"),"");
		}elseif($arrUser["interface"] == "pbx"){
			goBack("PBX用户不能登录！","");
		}else{
			 $_SESSION["user_info"] = $arrUser;
			 $_SESSION["user_priv"] = json_decode( $arrUser['action_list'],true);//权限
			 //dump($_SESSION["user_priv"]);die;
			 //下面专门让admin过期后登陆只有 上载授权文件这一个菜单页面
			 if($tenant_enable != "Y"){
				 if( ($_POST['username'] == "admin" && $isExpired) || $warrant_role == "N"  ){
					$_SESSION["user_priv"] = Array(
						"Information Settings"	=> Array(
							"System Infomation"	=> Array("view"=>'Y'),
						)
					);
				 }
			 }
			 $_SESSION["db_name"] = $db_name;
			 cookie('user_info',$arrUser);
			 header("Location:index.php");
			 //header("Location:index.php?m=Index&a=layout");
		}

    }

    function userLogout(){
        header("Content-Type:text/html; charset=utf-8");

		//dump($_SESSION);die;
		//更新登录报表状态到数据表report_agent_login
		$username = $_SESSION["user_info"]["username"];
		if( $_SESSION["user_info"]["interface"]=="agent" ){
			$logouttime = Date('Y-m-d H:i:s');

			$users = new Model("users");
			$users->where("username='$username'")->save(array("login_status"=>"N"));
			$this->loginUserCaChe();

			$task_id = isset($_SESSION['outbound_current_task']['id'])?$_SESSION['outbound_current_task']['id']:NULL;
			$report = M('report_agent_login');
			$arrTmp = $report->where("username='$username' AND status='登录中'")->find();
			$logintime	=	$arrTmp['logintime'];
			$arr = Array(
				'lastheartbeat'	=>	$logouttime,
				'status'	=>	'正常注销',
				'last_task_id'	=>	$task_id,
				'during'	=>	strtotime($logouttime) - strtotime($logintime),
			);
			$report->data($arr)->where("username='$username' AND logintime='$logintime'")->save();
			//dump($report->getLastSql());die;
			//正常注销的时候还要把这个成员从asterisk项目队列中移动出来
			if( $task_id ){
				import('ORG.Pbx.bmi');
				$bmi = new bmi();
				$bmi->loadAGI();
				$queuename = "AutoCall_". $task_id;
				$ext = $_SESSION['user_info']['extension'];
				$interface	= "Local/$ext@BG-QueueAgent";
				$cliCmd = "queue remove member $interface from $queuename";
				$bmi->asm->command($cliCmd);
			}
		}


		$themes_color = $_SESSION["themes_color_$username"];
		$mod = M("users_themes");
		$count = $mod->where("username = '$username'")->count();
		//echo $mod->getLastSql();
		if($count > '0'){
			$arrData = array(
				"themes_color"=>$themes_color,
			);
			$res = $mod->data($arrData)->where("username = '$username'")->save();
		}else{
			$arrData = array(
				"username"=>$username,
				"themes_color"=>$themes_color,
			);
			$res = $mod->data($arrData)->add();
		}


        unset($_SESSION['user_info']);
        unset($_SESSION['user_priv']);
        unset($_SESSION['db_name']);
        unset($_SESSION['menu']);
		unset($_SESSION['outbound_current_task']);

        goBack(L("You have been logout"),"index.php");
    }

    function homeLogin(){
        if(empty($_POST['username'])) {
            goBack("帐号必须填写！","");
        }elseif (empty($_POST['password'])){
            goBack("密码必须填写！","");
        }

        $sql = "SELECT * FROM users u LEFT JOIN role r ON u.r_id=r.r_id  WHERE username='". $_POST["username"] ."'";
        $user = M("users");
        $arrUser = $user->query($sql);
        $arrUser = $arrUser[0];

        if( !$arrUser["username"] ){
            goBack("没有这个用户！","");
        }elseif($arrUser["password"] != $_POST["password"]){
            goBack("密码不正确！","");
        }else{
            $_SESSION["user_info"] = $arrUser;
            header("Location:home.php");
        }
    }


	//验证码
    function  verify(){
        import("ORG.Util.Image");
        Image::buildImageVerify(4,1,"png",40,28);
    }

    //检测验证码
    function checkVerify(){
        if( $_SESSION['verify'] == md5($_POST['verify']) ) return true;
        else return false;
    }



	function test(){
		header("Content-Type:text/html; charset=utf-8");
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model('menu');
		$mainMenu = $menu->table("menu me")->field("me.id AS id,name,me.icon,moduleclass,order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=0 AND enabled='Y'")->order("`order` ASC")->select();
		//dump($mainMenu);die;

		$_SESSION['menu'] = Array();//存放子菜单的父菜单，为以后判断子菜单的权限的方便
		foreach( $mainMenu AS $row ){
			$mainId = $row['id'];
			//$subMenu = $menu->where("pid=$mainId")->order("`order` ASC")->select();
			//$subMenu = $menu->table("menu me")->field("me.id AS id,me.icon as iconCls,name,moduleclass, order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			$subMenu = $menu->table("menu me")->field("me.id AS id,name,url")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			if( 'admin'==$_SESSION["user_info"]['username'] ){
				//下面这样做，是为了让管理员过期后只显示一个菜单
				if( array_key_exists($row['name'],$_SESSION["user_priv"]) ){
					$tpl_menu[$row['name']]['thisMenu'] = L($row['name']);//一级菜单应该显示
					$tpl_menu[$row['name']]['id'] = $row['id'];//一级菜单应该显示
					//下面查询一级菜单下面的二级菜单有哪些应该显示

					foreach( $subMenu AS &$arrRow ){
						$arrRow["name"] = L($arrRow["name"]);
						$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
					}
				}

			}
		}

		$arr_all = array('分机','中继','时间组','等待音乐','系统录音','呼出路由','基本设置','编码','坐席监控','硬件检测','备份/恢复','网络','VPN 连接','日期/时间','重启/关机','版本升级','系统参数','系统信息','修改logo','部门','角色','用户','通话记录');  //三者相同的菜单
		$arr_all2 = array('PBX设置','系统管理','信息设置','组织结构','通话记录');  //三者相同的菜单

		$arr_outbound = array('外呼CID','外呼报表','外呼质检','外呼统计报表','外呼任务列表','外呼黑名单','质检评分');   //外呼独有的的菜单
		$arr_outbound2 = array('外呼平台');   //外呼独有的的菜单

		$arr_inbound = array('面板设置','传真邮件模版','事项类型','待办事项', '邮件模板','短信模板','短信查询', '费率','通话详单','队列漏接来电','工单报表','回访质检','话费账单', '服务报表','订单报表','客户资料','资料转交','服务类型','服务记录','文件类别','转交历史', '回访记录','回访内容','今日统计','商品列表','商品分类','商品品牌','商品类型','商品属性','订单管理','货单列表','队列报表');   //呼入独有的的菜单
		$arr_inbound2 = array('呼入平台','商品管理');   //呼入独有的的菜单


		$arr_in_out_bound = array('公告类型','公告','Faq类型','Faq内容管理','接口说明','质检类型','客户字段','坐席报表','示忙报表','系统报表');   //外呼与呼入共有的菜单
		$arr_in_out_bound2 = array('知识库','报表统计');   //外呼与呼入共有的菜单

		$arr_in_pbx = array('队列','自动语音应答','语音公告','时间条件','号码池','密码池','呼入路由','功能代码','呼入黑名单','会议室目录','开会');  //IPPBX与呼入共有的菜单
		$arr_in_pbx2 = array('会议');  //IPPBX与呼入共有的菜单

		$count = count($arr_all)+count($arr_outbound)+count($arr_inbound)+count($arr_in_out_bound)+count($arr_in_pbx);
		$arr_me = array_merge($arr_all,$arr_outbound,$arr_inbound,$arr_in_out_bound,$arr_in_pbx);


		//dump($count);
		foreach($tpl_menu as $key=>$val){
			$pid_name[] = $val["thisMenu"];
			if( in_array($val["thisMenu"],$arr_all2) ){
				$allId[] = $val["id"];   //三者相同的菜单
			}
			if( in_array($val["thisMenu"],$arr_outbound2) ){
				$all_outboundid[] = $val["id"];   //外呼独有的的菜单
			}
			if( in_array($val["thisMenu"],$arr_inbound2) ){
				$all_inboundid[] = $val["id"];     //呼入独有的的菜单
			}
			if( in_array($val["thisMenu"],$arr_in_out_bound2) ){
				$in_out_pid[] = $val["id"];     //外呼与呼入共有的菜单
			}
			if( in_array($val["thisMenu"],$arr_in_pbx2) ){
				$in_pbx_pid[] = $val["id"];     //IPPBX与呼入共有的菜单
			}
			foreach($val["subMenu"] as $vm){
				if( in_array($vm["name"],$arr_all) ){
					$all_sub[] = $vm['id'];	   //三者相同的菜单
				}
				if( in_array($vm["name"],$arr_outbound) ){
					$outbound_sub[] = $vm['id'];    //外呼独有的的菜单
				}
				if( in_array($vm["name"],$arr_inbound) ){
					$inbound_sub[] = $vm['id'];    //呼入独有的的菜单
				}
				if( in_array($vm["name"],$arr_in_out_bound) ){
					$in_out_sub[] = $vm['id'];    //外呼与呼入共有的菜单
				}
				if( in_array($vm["name"],$arr_in_pbx) ){
					$in_pbx_sub[] = $vm['id'];    //IPPBX与呼入共有的菜单
				}
				$sub_name[] = $vm["name"];
			}
		}
		//$arr_u = array_unique($arr_me,$sub_name);

		$str_pid = "'".implode("','",$allId)."'";
		$sub_id = "'".implode("','",$all_sub)."'";
		$all_id = $str_pid.",".$sub_id;    //三者相同的菜单id
		$res_all = $menu->where("id in ($all_id)")->save(array("sys_type"=>'{"outbound":"Y","inbound":"Y","IPPBX":"Y"}'));

		$outbound_id = "'".implode("','",$outbound_sub)."'";
		$outbound_id2 = "'".implode("','",$all_outboundid)."'";
		$outbound_all_id = $outbound_id.",".$outbound_id2;    //外呼独有的的菜单id
		$res_outbound = $menu->where("id in ($outbound_id)")->save(array("sys_type"=>'{"outbound":"Y","inbound":"N","IPPBX":"N"}'));

		$inbound_id = "'".implode("','",$inbound_sub)."'";
		$inbound_id2 = "'".implode("','",$all_inboundid)."'";
		$inbound_all_id = $inbound_id.",".$inbound_id2;     //呼入独有的的菜单id
		$res_inbound = $menu->where("id in ($inbound_all_id)")->save(array("sys_type"=>'{"outbound":"N","inbound":"Y","IPPBX":"N"}'));


		$in_out_id = "'".implode("','",$in_out_sub)."'";
		$in_out_id2 = "'".implode("','",$in_out_pid)."'";
		$in_out_all_id = $in_out_id.",".$in_out_id2;    //外呼与呼入共有的菜单
		$res_inbound = $menu->where("id in ($in_out_all_id)")->save(array("sys_type"=>'{"outbound":"Y","inbound":"Y","IPPBX":"N"}'));


		$in_pbx_id = "'".implode("','",$in_pbx_sub)."'";
		$in_pbx_id2 = "'".implode("','",$in_pbx_pid)."'";
		$in_pbx_all_id = $in_pbx_id.",".$in_pbx_id2;   //IPPBX与呼入共有的菜单
		$res_inbound = $menu->where("id in ($in_pbx_all_id)")->save(array("sys_type"=>'{"outbound":"N","inbound":"Y","IPPBX":"Y"}'));


		foreach($sub_name as $val){
			if( !in_array($val,$arr_me) ){
				$tt[] = $val;    //剩余那些菜单没写上的
			}
		}

		//dump($pid_name);
		//dump(count($sub_name));die;
		//dump($tpl_menu);die;
	}

	function loginUserCaChe(){
		$users = new Model("users");
		$arrData = $users->field("username,login_status")->where("login_status='Y'")->select();
		foreach($arrData as $val){
			$tmp[] = $val["username"];
		}
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F('loginUser',$tmp,"BGCC/Conf/crm/$db_name/");
		//F('loginUser',$tmp,"BGCC/Conf/");
	}

	function layout(){
		//检测是否有登录
		checkLogin();
		header("Content-Type:text/html; charset=utf-8");
		$para_sys = readS();
		if($_GET["l"]){
			setcookie("lang",$_GET["l"]);
			setcookie("think_language",$_GET["l"]);
		}else{
			//dump($_COOKIE);die;
			setcookie("lang",'zh_CN');
			setcookie("think_language",'zh_CN');
		}

		setcookie("think_language",$para_sys["language"]);

		//前后台公共的
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model("$menu_table");
		$mainMenu = $menu->table("$menu_table me")->field("me.id AS id,name,me.icon,moduleclass,order,url,me.pid AS pid")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where("me.pid=0 AND enabled='Y'")->order("`order` ASC")->select();
		//dump($mainMenu);die;

		$_SESSION['menu'] = Array();//存放子菜单的父菜单，为以后判断子菜单的权限的方便
		foreach( $mainMenu AS $row ){
			$mainId = $row['id'];
			//$subMenu = $menu->where("pid=$mainId")->order("`order` ASC")->select();
			$subMenu = $menu->table("$menu_table me")->field("me.id AS id,me.icon as iconCls,name,moduleclass, order,url,me.pid AS pid")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			if( 'admin'==$_SESSION["user_info"]['username'] ){//如果是管理员，默认显示所有模块加载的菜单
				/*
				$tpl_menu[$row['name']]['thisMenu'] = $row['name'];
				$tpl_menu[$row['name']]['iconCls'] = $row['icon'];
				$tpl_menu[$row['name']]['subMenu'] = $subMenu;
				*/
				//下面这样做，是为了让管理员过期后只显示一个菜单
				if( array_key_exists($row['name'],$_SESSION["user_priv"]) ){
					$tpl_menu[$row['name']]['thisMenu'] = $row['name'];//一级菜单应该显示
					$tpl_menu[$row['name']]['iconCls'] = $row['icon'];//一级菜单应该显示
					foreach( $subMenu AS $arrRow ){
						if( array_key_exists($arrRow['name'],$_SESSION["user_priv"][$row['name']]) ){
							$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
							$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
						}
					}
				}

			}else{//非管理员
				//$_SESSION["user_priv"]是权限数组
				//dump($_SESSION["user_priv"]);die;

				if( array_key_exists($row['name'],$_SESSION["user_priv"]) ){
					$tpl_menu[$row['name']]['thisMenu'] = $row['name'];//一级菜单应该显示
					$tpl_menu[$row['name']]['iconCls'] = $row['icon'];//一级菜单应该显示
					//下面查询一级菜单下面的二级菜单有哪些应该显示

					foreach( $subMenu AS $arrRow ){
						if( array_key_exists($arrRow['name'],$_SESSION["user_priv"][$row['name']]) ){
							$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
							$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
						}
					}
				}
			}
		}
		//$para_sys = readS();
		//dump($tpl_menu);die;
		$logo_replace = new Model("logo_replace");
		$arrLogo = $logo_replace->where("id = '2'")->find();
		$logo_img = $arrLogo["logopath"].$arrLogo["logoname"];
		$this->assign("logo",$logo_img);
		//dump($_SESSION["user_info"]);die;
        $this->assign("system",$para_sys);
		$this->assign("user_info",$_SESSION["user_info"]);
		$this->assign("menu",$tpl_menu);

		//前台界面
		//求出初始化登陆进去的状态
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$arrHint = $bmi->getHint($_SESSION["user_info"]['extension']);
		$DND_status = $bmi->getDNDAll();
		$dnd = $DND_status[$_SESSION["user_info"]['extension']];
		if($dnd){
			$this->assign("hint","PutDND");
		}else{
			$stat = $arrHint["stat"];
			if($stat == "Idle"){
				$hint = "DelDND";
			}elseif($stat == "InUse"){
				$hint = "Answer";
			}elseif($stat == "Unavailable"){
				$hint = "UnRegistered";
			}else{
				$hint = $stat;
			}
			$this->assign("hint",$hint);
		}
		//dump($hint);die;
		//前台进入管理界面的权限
		$menuname = "Seating monitoring";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("agent_manage",$priv['agent_manage']);


		if( in_array("inbound",$arrAL) ){
			$wj_in = "Y";
		}else{
			$wj_in = "N";
		}
		if( in_array("outbound",$arrAL) ){
			$wj_out = "Y";
		}else{
			$wj_out = "N";
		}
		if( in_array("IPPBX",$arrAL) ){
			$wj_IPPBX = "Y";
		}else{
			$wj_IPPBXt = "N";
		}
		if($wj_in == "Y" && $wj_out == "Y"){
			$sys_type = "all";
		}elseif($wj_in == "Y"){
			$sys_type = "inbound";
		}elseif($wj_out == "Y"){
			$sys_type = "outbound";
		}elseif($wj_IPPBX == "Y"){
			$sys_type = "IPPBX";
		}
		$this->assign("sys_type",$sys_type);


		if( in_array("wf",$arrAL) ){
			$this->assign("Workflow","Y");
		}else{
			$this->assign("Workflow","N");
		}

		if( in_array("ks",$arrAL) ){
			$this->assign("ExamTraining","Y");
		}else{
			$this->assign("ExamTraining","N");
		}


		$order_info = M("order_info");
		$start_time1 = date("Y-m-d")." 00:00:00";
		$end_time1 = date("Y-m-d")." 23:59:59";
		$where_order = "createtime>='$start_time1' AND createtime<='$end_time1'";
		$arrOrderData = $order_info->order("sum(goods_amount) desc")->field("createname,sum(goods_amount) as goods_amount,count(*) as order_number")->where($where_order)->group("createname")->limit("3")->select();
		$i = 0;
		foreach($arrOrderData as $val){
			$arrOrderData[$i]["order_sort"] = $i+1;
			$i++;
		}
		//echo $order_info->getLastSql();die;
		$this->assign("arrOrderData",$arrOrderData);
		//dump($arrOrderData);die;

		//外呼平台
		$task = M("sales_task");
		$user_info = session("user_info");
		$list = $task->where("dept_id='". $user_info["d_id"]  ."' and enable='Y'")->order("createtime desc")->select();

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrT = $bmi->getAllQueueMember();

		$n = 0;
		foreach($list as $val){
			$id = $val["id"];
			if( array_key_exists("AutoCall_$id",$arrT) ){
				$list[$n]['queueStatus'] = $arrT["AutoCall_$id"]['membername'];
			}
			$n++;
		}
		unset($n);

		$exten = $_SESSION["user_info"]["extension"];
		$i = 0;
		foreach($list as $val){
			if($val["calltype"] == "autocall"){
				if( in_array($exten,$val["queueStatus"]) ){
					$list[$i]["extenStatus"] = "<span id='Checked_In".$val["id"]."' style='color:red;'><span  class='checkedIn' taskId='".$val["id"]."'>已签入</span></span>";
				}else{
					$list[$i]["extenStatus"] = "<span class='checkedOut' id='Not_checked".$val["id"]."'>未签入</span>";
				}
			}
			$task_name[$val["id"]] = $val["name"];
			$i++;
		}

		$this->assign("taskId",$maxTaskId);
		$this->assign("task_name",json_encode($task_name));

		$this->assign("task",$list);

		//呼入平台
		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}
		$menuname_cm = "Customer Data";
		$p_menuname_cm = $_SESSION['menu'][$menuname_cm]; //父菜单
		$priv_cm = $_SESSION["user_priv"][$p_menuname_cm][$menuname_cm];
		if($para_sys["agent_customer_menu"] != "Y"){
			$priv_cm["add"] = "Y";
			$priv_cm["edit"] = "Y";
			//$priv_cm["delete"] = "Y";
			$priv_cm["transmission"] = "Y";
		}
		$this->assign("priv",$priv_cm);

		//销售漏斗
		$menuname1 = "Sales Funnel";
		$p_menuname1 = $_SESSION['menu'][$menuname1]; //父菜单
		$priv_Sales = $_SESSION["user_priv"][$p_menuname1][$menuname1];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv_Sales["view"] == "Y"){
				$this->assign("priv_Sales","Y");
			}else{
				$this->assign("priv_Sales","N");
			}
		}else{
			$this->assign("priv_Sales","Y");
		}

		//服务记录
		$menuname2 = "Service records";
		$p_menuname2 = $_SESSION['menu'][$menuname2]; //父菜单
		$priv1_Service = $_SESSION["user_priv"][$p_menuname2][$menuname2];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Service["view"] == "Y"){
				$this->assign("priv1_Service","Y");
			}else{
				$this->assign("priv1_Service","N");
			}
		}else{
			$this->assign("priv1_Service","Y");
		}

		//订单列表
		$menuname3 = "Order Management";
		$p_menuname3 = $_SESSION['menu'][$menuname3]; //父菜单
		$priv1_Order = $_SESSION["user_priv"][$p_menuname3][$menuname3];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Order["view"] == "Y"){
				$this->assign("priv1_Order","Y");
			}else{
				$this->assign("priv1_Order","N");
			}
		}else{
			$this->assign("priv1_Order","Y");
		}

		//回访记录列表
		$menuname4 = "Visit Record";
		$p_menuname4 = $_SESSION['menu'][$menuname4]; //父菜单
		$priv1_Visit_Record = $_SESSION["user_priv"][$p_menuname4][$menuname4];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Visit_Record["view"] == "Y"){
				$this->assign("priv1_Visit_Record","Y");
			}else{
				$this->assign("priv1_Visit_Record","N");
			}
		}else{
			$this->assign("priv1_Visit_Record","Y");
		}

		//回访内容列表
		$menuname5 = "Visit Content";
		$p_menuname5 = $_SESSION['menu'][$menuname5]; //父菜单
		$priv1_Visit_Content = $_SESSION["user_priv"][$p_menuname5][$menuname5];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Visit_Content["view"] == "Y"){
				$this->assign("priv1_Visit_Content","Y");
			}else{
				$this->assign("priv1_Visit_Content","N");
			}
		}else{
			$this->assign("priv1_Visit_Content","Y");
		}

		//转交历史
		$menuname6 = "Forwarded History";
		$p_menuname6 = $_SESSION['menu'][$menuname6]; //父菜单
		$priv1_History = $_SESSION["user_priv"][$p_menuname6][$menuname6];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_History["view"] == "Y"){
				$this->assign("priv1_History","Y");
			}else{
				$this->assign("priv1_History","N");
			}
		}else{
			$this->assign("priv1_History","Y");
		}

		//服务记录
		$menuname7 = "Work Order reports";
		$p_menuname7 = $_SESSION['menu'][$menuname7]; //父菜单
		$priv1_Work = $_SESSION["user_priv"][$p_menuname7][$menuname7];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Work["view"] == "Y"){
				$this->assign("priv1_Work","Y");
			}else{
				$this->assign("priv1_Work","N");
			}
		}else{
			$this->assign("priv1_Work","Y");
		}

		//客户资料--相关文件
		$menuname8 = "Related Documents";
		$p_menuname8 = $_SESSION['menu'][$menuname8]; //父菜单
		$priv1_File = $_SESSION["user_priv"][$p_menuname8][$menuname8];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_File["view"] == "Y"){
				$this->assign("priv1_File","Y");
			}else{
				$this->assign("priv1_File","N");
			}
		}else{
			$this->assign("priv1_File","Y");
		}

		//后台界面
		if( check_reload()){
			$this->assign("reload_need","");
		}else{
			$this->assign("reload_need","display:none;");
		}

		$this->display();
	}

	//重置登陆
	function resetLogin(){
		$workno = $_REQUEST["workno"];
		$users = M("users");
		$result = $users->where("username='$workno'")->save(array("permit_login"=>"Y"));
		if($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'重置成功！'));
		}else{
			echo json_encode(array('msg'=>'重置失败！'));
		}
	}

}

