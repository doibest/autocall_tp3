<?php
//商品配件
class ShopFittingAction extends Action{
	function fittingList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Fitting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$this->display();
	}


	//城市级联
	function test(){
		$this->display();
	}

	function countryCombox(){
		$region = new Model("region");
		$regData = $region->where("parent_id = 0")->select();
		$allCate = array(
			"region_id"=>"01",
			"parent_id"=>"-1",
			"region_name"=>"请选择...",
			"region_type"=>"0",
			"agency_id"=>"0",
		);
		array_unshift($regData,$allCate);
		echo json_encode($regData);
	}
	function provinceCombox(){
		$parent_id = $_REQUEST['parent_id'];
		if($parent_id){
			$region = new Model("region");
			$regData = $region->where("parent_id = '$parent_id'")->select();
			$allCate = array(
				"region_id"=>"01",
				"parent_id"=>"-1",
				"region_name"=>"请选择...",
				"region_type"=>"0",
				"agency_id"=>"0",
			);
			array_unshift($regData,$allCate);
			echo json_encode($regData);
		}else{
			$allCate[0] = array(
				"region_id"=>"01",
				"parent_id"=>"01",
				"region_name"=>"请选择...",
				"region_type"=>"0",
				"agency_id"=>"0",
			);
			echo json_encode($allCate);
		}
	}

	function cityTree(){
		checkLogin();
		$this->display();
	}

	function cityTreeData(){
		$region = new Model("region");
		import('ORG.Util.Page');
		$count = $region->where("parent_id != '0'")->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$regionData = $region->order("region_id asc")->field("region_id as id,region_name as text,parent_id")->where("parent_id != '0'")->select();


		$i = 0;
		//$row = array('Y'=>'是','N'=>'否');
		foreach($regionData as &$val){
			//$type = $row[$val['cat_enabled']];
			//$val['cat_enabled'] = $type;
			//if($val['parent_id'] != "0"){
				$regionData[$i]['state'] = "closed";
			//}
			$i++;
		}
		unset($i);



		//$arrTree = $this->getTree($regionData,0);   //将where条件去掉
		$arrTree = $this->getTree($regionData,1);
		//dump($arrTree);die;

		$j = 0;
		foreach($arrTree as $v){
			//if( $arrTree[$j]["cat_pid"] == "0" && !$arrTree[$j]["children"]){
			if( $arrTree[$j]["parent_id"] == "0"){
				$arrTree[$j]['iconCls'] = "shop";  //顶级分类的图标，叶子节点的图标在getTree()方法中设置。
			}
			$j++;
		}
		unset($j);

		/*
		//打开时只有顶级部门中 有子部门的 是闭合状态，其他的都是是打开状态
		$i = 0;
		foreach($arrTree as $v){
			if( is_array($arrTree[$i]['children'])==true ){
				$arrTree[$i]['state'] = "closed";
			}
			$i++;
		}
		unset($i);
		*/

		//dump($arrTree);die;
		//$strJSON = json_encode($arrTree);
		//echo ($strJSON);

		$rowsList = count($arrTree) ? $arrTree : false;
		$arrCat["total"] = $count;
		$arrCat["rows"] = $rowsList;
		//dump($arrmail);die;
		echo json_encode($arrCat);
	}


	function getTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['parent_id'] == $pId)    {
				$v['children'] = $this->getTree($data, $v['id']);

				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				/*
				if ( empty($v["children"]) && $v['state'] =='closed'){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}
				if ( $v["children"]  && $v['state'] =='closed'){
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				*/
				if ( empty($v["children"])){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}else{
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

}

?>
