<?php
/*
nginx中需要配置：
add_header Access-Control-Allow-Origin "*";
*/
class InterfaceAction extends Action{
	function index(){
		checkLogin();
		$this->display();
	}

	function iptest(){
		header("Access-Control-Allow-Origin: *");
		$aa = $this->getClientIP();
		$jsonmsg = json_encode(array('result'=>'success','msg'=>$aa));
		echo $jsonmsg;
	}

	function getClientIP(){
		header("Access-Control-Allow-Origin: *");
		global $ip;
		if (getenv("HTTP_CLIENT_IP")){
			$ip = getenv("HTTP_CLIENT_IP");
		}else if(getenv("HTTP_X_FORWARDED_FOR")){
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		}else if(getenv("REMOTE_ADDR")){
			$ip = getenv("REMOTE_ADDR");
		}else{
			$ip = "Unknow";
		}

		$result = false;
		$mod = M("interface_ip_list");
		$arrData = $mod->select();

		if($arrData){
			$arrIP = array();
			foreach($arrData as $val){
				$arrIP[] = $val["client_ip"];
			}
			if( in_array($ip,$arrIP) ){
				$result = true;
			}
		}else{
			$result = true;
		}

		return $result;
	}

	function interfaceList(){
		checkLogin();
		$CTI_IP = $_SERVER["SERVER_ADDR"];
		$this->assign("CTI_IP",$CTI_IP);
		$extension =  $_SESSION["user_info"]["extension"];
		import('ORG.Pbx.bmi');

		$bmi = new bmi();
		$extension_status = $bmi->getHints();   //获取分机状态
		$stat = $extension_status[$extension]['stat'];
		//dump($stat);
		$cf_status = $bmi->getCFAll("cf");	 //转接
		$DND_status = $bmi->getDNDAll();
		$dnd = $DND_status[$extension];
		//dump($dnd);die;
		if($dnd){
			$this->assign("state","示忙");
			//$this->assign("status","示忙");
		}else{
			if($stat == "State:Idle"){
				$this->assign("state","在线");
				//$this->assign("status","示闲");
			}
			if($stat == "State:Unavailable"){
				$this->assign("state","离线");
			}
		}
		$cf_tpl = $cf_status[$extension];
		if($cf_tpl){
			$this->assign("state","转接");
		}
		if($stat == "State:Ringing"){
			$this->assign("state","振铃");
		}
		if($stat == "State:InUse"){
			$this->assign("state","通话中");
		}
		$this->assign("extension",$extension);

		$ivr = new Model("asterisk.ivr");
		$ivrList = $ivr->field("ivr_id,displayname")->where("displayname <> 'Unnamed'")->select();
		$this->assign("ivrList",$ivrList);


		$this->display();
	}

	/*
	function jsonp(){
		$filepath = "/var/www/html/Agent/Tpl/public/js/jsonp.js";
		$file_handle = fopen($filepath, "r");
		while (!feof($file_handle)) {
		   $line = fgets($file_handle);
		   echo $line;
		}
		fclose($file_handle);

		//$this->display();
	}
	*/

	/*//示忙
	function DNDoperating(){
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];

		$users = new Model("users");
		$user_exten = $users->where("extension = '$extension' AND r_id!=4")->count();
		if($user_exten){
			$arrExt = explode(',',$extension);
			foreach($arrExt as $val){
				$result = $bmi->putDND($val);
			}
			$jsonmsg = json_encode(array('success'=>true,'msg'=>'示忙成功 '));
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
		}else{
			$jsonmsg = json_encode(array('success'=>true,'msg'=>'您的分机号没有绑定用户！'));
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
		}
	}*/

	//示忙----wjj rewrite
	function DNDoperating(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];

		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$extension'")->count();
		if($user_exten){
			$arrExt = explode(',',$extension);
			foreach($arrExt as $val){
				$return = $bmi->putDND($val);
			}
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'示忙成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}



	/*//示闲
	function DelDNDoperating(){
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];

		$users = new Model("users");
		$user_exten = $users->where("extension = '$extension' AND r_id!=4")->count();
		if($user_exten){
			$arrExt = explode(',',$extension);
			foreach($arrExt as $val){
				$result = $bmi->delDND($val);
			}
			//无条件相信bmi执行成功
			//echo "ok";
			$jsonmsg = json_encode(array('success'=>true,'msg'=>'示闲成功 '));
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
		}else{
			$jsonmsg = json_encode(array('success'=>true,'msg'=>'您的分机号没有绑定用户！'));
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
		}
	}*/
	function DelDNDoperating(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];
		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$extension'")->count();
		if($user_exten){
			$arrExt = explode(',',$extension);
			foreach($arrExt as $val){
				$return = $bmi->delDND($val);
			}
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'示闲成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'示闲失败，您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}



	/*//强拆
	function hangups(){
		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];

		$users = new Model("users");
		$user_exten = $users->where("extension = '$extension' AND r_id!=4")->count();
		if($user_exten){
			$arrExt = explode(',',$extension);
			foreach($arrExt as $val){
				$result = $bmi->hangup($val);
			}
			$jsonmsg = json_encode(array('success'=>true,'msg'=>'强拆成功！ '));
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
		}else{
			$jsonmsg = json_encode(array('success'=>true,'msg'=>'您的分机号没有绑定用户！'));
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
		}
	}*/
	//强拆
	function hangups(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];
		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$extension'")->count();
		if($user_exten){
			$arrExt = explode(',',$extension);
			foreach($arrExt as $val){
				$return = $bmi->hangup2($val);
			}
			if( $return == "ok"){
				$jsonmsg = json_encode(array('result'=>'Success','msg'=>'强拆成功！'));
			}else if( $return == "channel not exist"){
				$jsonmsg = json_encode(array('result'=>'Error','msg'=>'强拆失败，通话不存在！'));
			}else{
				$jsonmsg = json_encode(array('result'=>'Error','msg'=>'强拆失败，出现一个未知错误！'));
			}
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'强拆失败，可能您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}


	/*//呼叫
	function calls(){
		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$src_exten = $_REQUEST['src_exten'];    //主叫
		$dst_exten = $_REQUEST['dst_exten'];    //被叫

		$users = new Model("users");
		$user_src_exten = $users->where("extension = '$src_exten' AND r_id!=4")->count();
		$user_dst_exten = $users->where("extension = '$dst_exten' AND r_id!=4")->count();
		if($user_src_exten && $user_dst_exten){
			$result = $bmi->call2($src_exten,$dst_exten,$src_exten);
			//dump($src_exten);
			//dump($dst_exten);
			$jsonmsg = json_encode(array('success'=>true,'msg'=>'呼叫成功 '));
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
		}else{
			$jsonmsg = json_encode(array('success'=>true,'msg'=>'您的分机号没有绑定用户！'));
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
		}

	}*/

	//手机打打手机【坐席分机为手机】--先呼坐席手机再打客户号码
	function phoneCallPhone(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$agent_phone = $_REQUEST['agent_phone'];    //坐席手机号
		$client_phone = $_REQUEST['client_phone'];    //客户手机号
		$trunk_name = "SIP/".$_REQUEST['trunk_name'];    //中继名称
		$bmi->clickCallPhoneToPhone($agent_phone,$client_phone,$trunk_name);
		$jsonmsg = json_encode(array('result'=>'Success','msg'=>'呼叫成功！'));
		echo $jsonmsg;
	}

	function calls(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$src_exten = $_REQUEST['src_exten'];    //主叫
		$dst_exten = $_REQUEST['dst_exten'];    //被叫

		$users = new Model("users");
		$user_src_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$src_exten'")->count();
		if($user_src_exten){
			$return = $bmi->call2($src_exten,$dst_exten,$src_exten);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'呼叫成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'呼叫失败！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}

	//监听
	function monitors(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];
		$spied = $_REQUEST['spied'];
		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$extension'")->count();
		$user_spied_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$spied'")->count();
		if($user_exten && $user_spied_exten){
			$result = $bmi->interface_monitor($extension,$spied);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>$extension .'监听' .$spied .'成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'监听失败，可能您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}

	//强插
	function chanspys(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];
		$spied = $_REQUEST['spied'];

		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$extension'")->count();
		$user_spied_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$spied'")->count();
		if($user_exten && $user_spied_exten){
			$result = $bmi->interface_chanspy($extension,$spied);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>$extension .'强插' .$spied .'成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'强插失败，可能您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}



	//转接
	function transferCall(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$src_exten = $_REQUEST['src_exten'];
		$adapter_exten = $_REQUEST['adapter_exten'];

		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("extension = '$src_exten'")->count();
		$user_spied_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("extension = '$adapter_exten'")->count();
		if($user_exten){
			$result = $bmi->cci_transferCall($src_exten,$adapter_exten);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'转接成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'转接失败，可能您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}


	//通话保持
	function holdCall(){
		//echo "1111";die;
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$exten = $_REQUEST['extension'];	//分机
		//$holdedChannel = $bmi->holdCall($exten);

		$arrHint = $bmi->getHint($exten);
		$device = $arrHint['device'];
		//dump($device);die;
		//$parkedExten = $bmi->callPause($exten,$device);
		$holdedChannel = $bmi->callPause($exten,$device);

		if( $holdedChannel ){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'保持成功！','holdedChannel'=>$holdedChannel));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'保持失败，通话不存在或者其它未知错误！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
      echo $jsonmsg;
	}

	//恢复呼叫保持
	function resumeHoldedCall(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$exten = $_REQUEST['extension'];
		$holdedChannel = $_REQUEST['holdedChannel'];	//三方通话被保持的通道
		//$result = $bmi->resumeHoldedCall($exten,$holdedChannel);
		$result = $bmi->restoreCall($exten,$holdedChannel);
		if( $result ){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'恢复通话成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'恢复通话失败！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
      echo $jsonmsg;
	}

	//三方通话
	function multiCallBAK(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$exten = $_REQUEST['extension'];	//分机
		$thirdPhone = $_REQUEST['thirdPhone'];	//被邀请的电话
		//取得分机对方的通道

		//取得随机的会议室号码

		//转接

		//会议室发送主动邀请

		if($exten && $thirdPhone){
			$result = $bmi->multiCall($exten,$thirdPhone);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'三方通话成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'三方通话失败，可能您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
      echo $jsonmsg;
	}

	//三方通话
	function multiCall(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];	//分机
		$thirdPhone = $_REQUEST['thirdPhone'];	//被邀请的电话

		if($extension && $thirdPhone){
			$arrHint = $bmi->getHint($extension);
			$device = $arrHint['device'];
			$result = $bmi->nway2($device,$thirdPhone);
			//$result = $bmi->multiCall($exten,$thirdPhone);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'三方通话成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'三方通话失败，可能您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
      echo $jsonmsg;
	}

	//踢除三方通话的坐席
	function kickMultiCall(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$thirdPhone = $_REQUEST['thirdPhone'];	//被邀请的电话

		if($thirdPhone){
			$result = $bmi->kickMultiCall($thirdPhone);
			if( $result ){
				$jsonmsg = json_encode(array('result'=>'Success','msg'=>'剔除三方通话成功！'));
			}else{
				$jsonmsg = json_encode(array('result'=>'Error','msg'=>'剔除三方通话失败！'));
			}

		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'剔除三方通话失败！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
      echo $jsonmsg;
	}


	function jsonp(){
		//header("Access-Control-Allow-Origin: *");
		$tt = $_REQUEST['type'];
		//dump($tt);
		if($tt=="jsonp"){
			//$filepath = "/var/www/html/Agent/Tpl/public/js/jsonp.js";
			$filepath = "Agent/Tpl/public/js/jsonp.js";
			$file_handle = fopen($filepath, "r");
			while (!feof($file_handle)) {
			   $line = fgets($file_handle);
			   echo $line;
			}
			fclose($file_handle);
		}
		if($tt=="jquerys"){
			//$jquerypath = "/var/www/html/Agent/Tpl/public/js/jquery-1.8.0.min.js";
			$jquerypath = "Agent/Tpl/public/js/jquery-1.8.0.min.js";
			$jquery_handle = fopen($jquerypath, "r");
			while (!feof($jquery_handle)) {
			   $jqueryline = fgets($jquery_handle);
			   echo $jqueryline;
			}
			fclose($jquery_handle);
		}
		if($tt=="easyuis"){
			//$uipath = "/var/www/html/Agent/Tpl/public/js/jquery.easyui.min.js";
			$uipath = "Agent/Tpl/public/js/jquery.easyui.min.js";
			$ui_handle = fopen($uipath, "r");
			while (!feof($ui_handle)) {
			   $uiline = fgets($ui_handle);
			   echo $uiline;
			}
			fclose($ui_handle);
		}
	}

	//返回分机状态
	function extensionState(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		if($_REQUEST['extension']){
			$extension = $_REQUEST['extension'];
		}else{
			$extension =  $_SESSION["user_info"]["extension"];
		}
		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$extension'")->count();
		$arrData = array();
		if($user_exten){
			import('ORG.Pbx.bmi');
			$bmi = new bmi();
			$extension_status = $bmi->getHints();   //获取分机状态
			$stat = $extension_status[$extension]['stat'];
			//dump($stat);die;
			$cf_status = $bmi->getCFAll("cf");	 //转接
			$DND_status = $bmi->getDNDAll();
			$dnd = $DND_status[$extension];
			if($dnd){
				$arrData["state"] = "示忙";
			}else{
				if($stat == "State:Idle"){
					$arrData["state"] = "空闲";
				}
				if($stat == "State:Unavailable"){
					$arrData["state"] = "离线";
				}
			}
			$cf_tpl = $cf_status[$extension];
			if($cf_tpl){
				$arrData["state"] = "转移";
			}
			if($stat == "State:Ringing"){
				$arrData["state"] = "振铃";
			}
			if($stat == "State:InUse"){
				$arrData["state"] = "通话中";
			}
			$arrData["extension"] = $extension;
			$arrData["result"] = "Success";
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>$arrData));
		}else{
			$arrData["result"] = "Error";
			$arrData["msg"] = "分机号不存在或者分机号没有绑定用户！";
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>$arrData));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
      echo $jsonmsg;
	}

	//迁入队列
	function queueMoved(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$bmi->asm->Command("queue reload all");
		$ext = $_REQUEST['extension'];
		$queue_name = $_REQUEST['queue_name'];

		//判断队列是否存在
		$aa = $bmi->asm->Command("queue show $queue_name");
		$bb = $aa["data"];
		if( strpos($bb,"No such queue")){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'队列不存在！'));
			//echo $_REQUEST['callback'].'('.$jsonmsg.')';
         echo $jsonmsg;
			die;
		}
		//dump($bb);die;
		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$ext'")->count();
		if($user_exten){
			$action = "QueueAdd";
			$parameters = Array(
				"Queue"	=>	$queue_name,
				"Interface"	=>	"Local/$ext@BG-QueueAgent/n",
				"Penalty"	=>	"0",
				"Paused"	=>	"no",
				"MemberName"	=>	"$ext",
			);
			//dump($parameters);die;
			$bmi->asm->send_request($action,$parameters);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>"分机${ext}成功迁入队列${queue_name}！"));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}

	//迁出队列
	function queueRemove(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$queuename = $_REQUEST['queue_name'];
		$exten = $_REQUEST['extension'];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();

		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("u.extension = '$exten'")->count();
		if($user_exten){
			$memberInfo = $bmi->getDynamicQueueMember($queuename);
			//dump($memberInfo);die;
			if( $memberInfo ){
				foreach( $memberInfo AS $v ){
					if($v['membername'] == $exten){
						$cliCmd = "queue remove member ".$v['interface']." from $queuename";
						$bmi->asm->command($cliCmd);
					}
				}
			}
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>"分机${exten}成功从队列${queuename}中迁出！"));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}

	//代接
	function pickUps(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$extension = $_REQUEST['extension'];
		$pickup_name = $_REQUEST['pickup_name'];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();

		$users = new Model("users");
		$user_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("extension = '$extension'")->count();
		$user_spied_exten = $users->table("users u")->field("u.r_id,r.interface,u.extension")->join("role r on u.r_id=r.r_id")->where("extension = '$pickup_name'")->count();
		if($user_exten && $user_spied_exten){
			$bmi->pickup($extension,$pickup_name);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'代接成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'您的分机号没有绑定用户！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}



	//IVR列表
	function ivrDataList(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$ivr = new Model("asterisk.ivr");
		$ivrList = $ivr->field("ivr_id,displayname")->where("displayname <> 'Unnamed'")->select();
		$arrIVR = Array();
		foreach($ivrList AS $id=>$name){
			$arrIVR["$id"] = $name;
		}
		//echo $_REQUEST['callback'].'('.json_encode($arrIVR).')';
		if($arrIVR){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>$arrIVR));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'您在系统没有设置IVR！'));
		}
		echo $jsonmsg;
	}

	//ivr转接
	function ivrTransfer(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$ivr_id = $_REQUEST['ivrId'];
		$exten = $_REQUEST['extension'];

		if($ivr_id){
			import('ORG.Pbx.bmi');
			$bmi = new bmi();

			$bmi->transferIVR($exten,$ivr_id);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'IVR转接成功！'));

		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'参数错误！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}




	//下载接口示例模板
	/*
	function DownloadTemplate(){
		$file = $_REQUEST['file'];
		$path = "BGCC/Tpl/Public/download/";
		$realfile = $path."interface.html";
		if(!file_exists($realfile)){
			$this->error("File $realfile is not exist!");
		}
        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}
	*/
	//下载接口示例模板————modified by wjj
	function DownloadTemplate_bak(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$CTI_IP = $_SERVER["SERVER_ADDR"];
		//$CTI_IP = "www.baidu.com";
		$file_type = $_REQUEST["file_type"];
		$path = "BGCC/Tpl/Public/download/";
		if($file_type == "doc"){
			$realfile = $path."interface.doc";
			$filename = "interface.doc";
		}else{
			$realfile = $path."interface.html";
			$filename = "interface.html";
		}
		$this->assign("CTI_IP",$CTI_IP);
		$content = $this->fetch($realfile);

		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(strlen($content)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=$filename");
        echo $content;
	}


	//下载文件
	function DownloadTemplate(){
		$CTI_IP = $_SERVER["SERVER_ADDR"];
		//$CTI_IP = "www.baidu.com";
		$file_type = $_REQUEST["file_type"];
		$path = "BGCC/Tpl/Public/download/";
		if($file_type == "doc"){
			$realfile = $path."interface.doc";
			$filename = "interface.doc";
		}else{
			$realfile = $path."interface.html";
			$filename = "interface.html";
		}

        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}



	//当前服务器的来电弹屏
	function screenPops(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		checkLogin();
		$extension = $_REQUEST['extension'];
		//$exten = $_SESSION["user_info"]["extension"];
		$exten = isset($extension) ? $extension : $_SESSION["user_info"]["extension"];
		$this->assign("exten",$exten);


		$CTI_IP = $_SERVER["SERVER_ADDR"];
		$this->assign("CTI_IP",$CTI_IP);

		$this->display();
	}

	function getTan(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$msg = $_REQUEST['phone_num'];
		$jsonmsg = json_encode($msg);
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
      echo $jsonmsg;
	}


	//CDR通话记录接口
	function listCDR(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$src = $_REQUEST['src']; //主叫
		$dst = $_REQUEST['dst'];	//被叫
		$calldate_start = $_REQUEST['calldate_start']; //起始时间,格式: XXXX-XX-XX
		$calldate_end = $_REQUEST['calldate_end']; //起始时间,格式: XXXX-XX-XX
		$disposition = $_REQUEST['disposition']; //呼叫状态,如果接听传 ANSWERED，未接听传 NOANSWER
		$page = $_REQUEST['page']; //请求页面——【此参数必须给出】
		$pagesize = $_REQUEST['pagesize']; //页面显示记录条数
		if(!isset($_REQUEST['page']) && !isset($_REQUEST['pagesize'])){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'请求参数page和pagesize不正确！'));
		}else{
			$where = "1 ";
			if($src)$where .= "AND src='$src'";
			if($dst)$where .= "AND src='$dst'";
			if($calldate_start)$where .= "AND calldate>='$calldate_start 00:00:00'";
			if($calldate_end)$where .= "AND calldate<='$calldate_end 00:00:00'";
			if("ANSWERED" == $disposition ){
				$where .= "AND disposition='ANSWERED'";
			}else if("NOANSWER" == $disposition ){
				$where .= "AND disposition!='ANSWERED'";
			}else{
				;
			}

			$limit = (string)(($page-1)*$pagesize) ."," .$pagesize;
			$cdr = M("asteriskcdrdb.cdr");
			$count = $cdr->where($where)->count();
			//取数据
			$arrTmp = $cdr->field("calldate,src,dst,duration,billsec,disposition,userfield,uniqueid,outnum,calltype,workno,dept_id")->where($where)->order("calldate ASC")->limit($limit)->select();
			$currentPageCount = count($arrTmp);
			$msg = "总共有{$count}条记录,本次查询第{$page}页,当前页成功返回{$currentPageCount}条通话记录，记录内容详见data属性:";
			//dump($cdr->getLastSql());
			//dump($arrTmp);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>$msg,'total'=>$count,'currentPageCount'=>$currentPageCount,'querystring'=>Array('src'=>$src,'dst'=>$dst,'calldate_start'=>$calldate_start,'calldate_end'=>$calldate_end,'disposition'=>$disposition,'page'=>$page,'pagesize'=>$pagesize),'data'=>$arrTmp));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}

	//CDR通话记录接口2
	function listCDR_Record(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$src = $_REQUEST['src']; //主叫
		$dst = $_REQUEST['dst'];	//被叫
		$calldate_start = $_REQUEST['calldate_start']; //起始时间,格式: XXXX-XX-XX
		$calldate_end = $_REQUEST['calldate_end']; //起始时间,格式: XXXX-XX-XX
		$disposition = empty($_REQUEST['disposition']) ? "ANSWERED" : $_REQUEST['disposition']; //呼叫状态,如果接听传 ANSWERED，未接听传 NOANSWER
		$page = $_REQUEST['page']; //请求页面——【此参数必须给出】
		$pagesize = $_REQUEST['pagesize']; //页面显示记录条数
		if(!isset($_REQUEST['page']) && !isset($_REQUEST['pagesize'])){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'请求参数page和pagesize不正确！'));
		}else{
			$where = "uniqueid is not null AND uniqueid !=''";
			if($src)$where .= "AND src='$src'";
			if($dst)$where .= "AND src='$dst'";
			if($calldate_start)$where .= "AND calldate>='$calldate_start 00:00:00'";
			if($calldate_end)$where .= "AND calldate<='$calldate_end 00:00:00'";
			if("ANSWERED" == $disposition ){
				$where .= "AND disposition='ANSWERED'";
			}else if("NOANSWER" == $disposition ){
				$where .= "AND disposition!='ANSWERED'";
			}else{
				;
			}

			$limit = (string)(($page-1)*$pagesize) ."," .$pagesize;
			$cdr = M("asteriskcdrdb.cdr");
			$count = $cdr->where($where)->count();
			//取数据
			$arrTmp = $cdr->field("calldate,src,dst,billsec,disposition,uniqueid,calltype,workno,dept_id")->where($where)->order("calldate ASC")->limit($limit)->select();    //->limit($limit)
			foreach($arrTmp as &$val){
				$val["uniqueid"] = trim($val["uniqueid"]);
				$arrTmp2 = explode('.',$val["uniqueid"]);
				$timestamp = $arrTmp2[0];
				$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
				$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";
				if(file_exists($WAVfile) ){
					$val["filename"] = $WAVfile;
					$val["size"] = $this->toSize(filesize($WAVfile));
					$ttt[] = $val;
				}else{
					$val["filename"] = "";
					$val["size"] = "";
				}

			}
			$currentPageCount = count($arrTmp);
			$msg = "总共有{$count}条记录,本次查询第{$page}页,当前页成功返回{$currentPageCount}条通话记录，记录内容详见data属性:";
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>$msg,'total'=>$count,'data'=>$arrTmp));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}


	function getRecord(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$uniqueid = trim($_REQUEST['uniqueid']);
		$cdr = new Model("asteriskcdrdb.cdr");
		$arrTmp = $cdr->field("calldate,src,dst,billsec,disposition,uniqueid,calltype,workno,dept_id")->where("uniqueid ='$uniqueid'")->find();
		$arrTmp2 = explode('.',$uniqueid);
		$timestamp = $arrTmp2[0];
		$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
		$WAVfile = $dirPath ."/".$uniqueid.".WAV";
		if($arrTmp){
			if(file_exists($WAVfile) ){
				$arrTmp["filename"] = $WAVfile;
				//$arrTmp["size"] = $this->toSize(filesize($WAVfile));
				$arrTmp["downloadURL"] = "index.php?m=CDR&a=downloadCDR&uniqueid=${uniqueid}";

			}else{
				$arrTmp["filename"] = "";
				//$arrTmp["size"] = "";
				$arrTmp["downloadURL"] = "";
			}
		}
		if($arrTmp){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'获取成功!','data'=>$arrTmp));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>"录音id不存在！"));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
      echo $jsonmsg;
	}


	//转换目录大小的 单位
	function toSize($size){
		$dw="Byte";
		if($size>=pow(2,40)){
			$size=round($size/pow(2.40),2);
			$dw="TB";
		}elseif($size>=pow(2,30)){
			$size=round($size/pow(2,30),2);
			$dw="GB";
		}elseif($size>=pow(2,20)){
			$size=round($size/pow(2,20),2);
			$dw="MB";
		}elseif($size>=pow(2,10)){
			$size=round($size/pow(2,10),2);
			$dw="KB";
		}else{
			$dw="bytes";
		}
		return $size.$dw;
	}

	//短信接口
	function interfaceSMS(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$phone = $_REQUEST['phone'];
		$sendcontent = $_REQUEST['message'];
		$retime = isset($_REQUEST['time']) ? $_REQUEST['time'] : date("Y-m-d H:i:s");
		//$time = strtotime($retime);
		$message = utf2gb($sendcontent);
		$result = asterSendSMS($phone,$message,$retime);

		if($result == "2000"){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'短信发送成功'));
		}
		if($result == "4002"){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'短信发送失败,短信平台授权错误(用户不存在、密码错误、权限不足等)!'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}


	//外呼任务列表
	function taskDataList(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$task = new Model("sales_task");
		$taskList = $task->field("id,name")->where("enable='Y'")->select();
		$arrTask = Array();
		foreach($taskList AS $id=>$name){
			$arrTask["$id"] = $name;
		}
		//dump($arrTask);die;
		//echo $_REQUEST['callback'].'('.json_encode($arrTask).')';
		if($arrTask){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>$arrTask));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>"您在系统中没有创建外呼任务！"));
		}
		echo $jsonmsg;
	}

	//添加一条数据到外呼任务中
	function addOnePhoneToTask(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$task_id = $_REQUEST['task_id'];
		$name = $_REQUEST['name'];
		$phone = $_REQUEST['phone'];
		//$unique = $_REQUEST['unique'];
		$unique = "all";
		$arr = Array(
			'name'	=> $name,
			'phone1'	=>	$phone,
		);
		$source = new Model("sales_source_${task_id}");
		if($unique == "one"){
			//从传过来的任务表中查询号码重复
			$count_phone = $source->where("phone1 = '$phone'")->count();
			if($count_phone > 0){
				$jsonmsg = json_encode(array('result'=>'unique','msg'=>'添加失败，此号码已存在!'));
				//echo $_REQUEST['callback'].'('.$jsonmsg.')';
            echo $jsonmsg;
				exit;
			}
		}
		if($unique == "all"){
			//从所有任务表中查询号码重复
			$task = new Model("sales_task");
			$taskData = $task->field("id,name")->where("enable='Y'")->select();
			$ph =  new Model();
			foreach($taskData as $val){
				$resCount[] = $ph->query("SELECT COUNT(*) AS ph_count FROM `sales_source_".$val["id"]."` WHERE phone1 = '$phone'");
			}
			foreach($resCount as $val){
				$count[] = $val[0]["ph_count"];
			}
			$count_phone = array_sum($count);
			if($count_phone > 0){
				$jsonmsg = json_encode(array('result'=>'unique','msg'=>'添加失败，此号码已存在该任务或其他任务中!'));
				//echo $_REQUEST['callback'].'('.$jsonmsg.')';
            echo $jsonmsg;
				exit;
			}
		}
		//dump($jsonmsg);die;
		$result = $source->add($arr);
		if( $result ){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'添加号码成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'添加号码失败！'));
		}
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}


	function agentStatusInfoData(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$u_extension = $_SESSION["user_info"]["extension"];
        import('ORG.Util.Page');
		import('ORG.Pbx.bmi');

		$bmi = new bmi();
		$extension_status = $bmi->getHints();   //获取分机状态
		$cf_status = $bmi->getCFAll("cf");	 //转接
		$DND_status = $bmi->getDNDAll();
		//dump($DND_status);

		$channels = $bmi->getAllChannelsInfo("CallerID");
		//dump($channels);exit;

        $users=new Model("Users");

        $list = $users->table('users u')->order('u.extension')->field('u.username,u.cn_name,r.r_name,d.d_name,u.extension')->join('department d on u.d_id=d.d_id ')->join('role r on u.r_id=r.r_id')->where("u.extension <> ' '")->select();
		//,u.fax,u.email,u.en_name,u.extension_type
		$i = 0;
		foreach( $list As $val ){
			$ext = $val["extension"];
			$list[$i]['stat'] = substr($extension_status[$ext]['stat'],6);
			$list[$i]['Billsec'] = $channels[$ext]['Duration'];
			$dn = $DND_status[$ext];
			if($dn){
				$list[$i]['stat'] = 'DND';
			}
			$cf_tpl = $cf_status[$ext];
			if($cf_tpl){
				$list[$i]['stat'] = 'cf';
			}
			$i++;

		}
		$arrData = json_encode($list);
		//echo $_REQUEST['callback'].'('.$arrData.')';
		$jsonmsg = json_encode(array('result'=>'Success','msg'=>$list));
		echo $jsonmsg;

	}


	//通话统计接口
	function callStatistics(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$src = $_REQUEST['src']; //主叫
		$dst = $_REQUEST['dst'];	//被叫
		$calldate_start = $_REQUEST['calldate_start']; //起始时间,格式: XXXX-XX-XX
		$calldate_end = $_REQUEST['calldate_end']; //起始时间,格式: XXXX-XX-XX

		$where = "1 ";
		if($src)$where .= "AND src='$src'";
		if($dst)$where .= "AND src='$dst'";
		if($calldate_start)$where .= "AND calldate>'$calldate_start 00:00:00'";
		if($calldate_end)$where .= "AND calldate<'$calldate_end 00:00:00'";


		$cdr = M("asteriskcdrdb.cdr");
		$count = $cdr->where($where)->count();
		/*workno:工号，callInTotal：呼入总数，callInAnsweredTotal：呼入接听总数，
		noAnswerTotal：未接号码总数【呼入跟呼出】，callInNoAnswerTotal：呼入未接号码总数
		answerdTotal：接听总数【呼入跟呼出】，callInAnswerdTotal：呼入接听总数
		*/
		$arrTmp = $cdr->field("workno,sum(case when calltype = 'IN' then 1 else 0 end) as callInTotal,sum(case when calltype = 'IN' and disposition = 'ANSWERED' then 1 else 0 end) as callInAnsweredTotal,sum(case when disposition != 'ANSWERED' then 1 else 0 end) as noAnswerTotal,sum(case when disposition != 'ANSWERED' and calltype = 'IN' then 1 else 0 end) as callInNoAnswerTotal,sum(case when disposition = 'ANSWERED' then 1 else 0 end) as answerdTotal,sum(case when disposition = 'ANSWERED' and calltype = 'IN' then 1 else 0 end) as callInAnswerdTotal")->where($where)->order("calldate ASC")->group("workno")->select();

		$arrData = json_encode($arrTmp);
		//echo $_REQUEST['callback'].'('.$arrData.')';
		$jsonmsg = json_encode(array('result'=>'Success','msg'=>$arrTmp));
		echo $jsonmsg;
	}

	function getBlack(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arr = $bmi->getBlackList();
		$arrBlackPhone = array();
		foreach($arr as $val){
			$arrBlackPhone[] = $val[0];
		}
		$arrData = json_encode($arrBlackPhone);
		//dump($arrBlackPhone);die;
		//echo $_REQUEST['callback'].'('.$arrData.')';
		if($arrBlackPhone){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>$arrBlackPhone));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'没有黑名单记录！'));
		}
		echo $jsonmsg;
	}

	function addBlack(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$phone = $_REQUEST["phone"];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arr = $bmi->insertBlack($phone);
		if($arr["data"] == "Privilege: Command\r\nUpdated database successfully\n"){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'添加成功！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'添加失败！'));
		}
		//dump($arr);die;
		//$arrData = json_encode($arr);
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}


	function deleteBlack(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$phone = $_REQUEST["phone"];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arr = $bmi->delBlack($phone);
		if($arr["data"] == "Privilege: Command\r\nDatabase entry removed.\n"){
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'移除成功！'));
		}elseif($arr["data"] == "Privilege: Command\r\nDatabase entry does not exist.\n"){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'此号码不在黑名单中！'));
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'移除失败！'));
		}
		//dump($arr);die;
		//$arrData = json_encode($arr);
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		echo $jsonmsg;
	}

	//模拟登陆接口
	function loginInterface(){
		//header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		//$warrant_role = $_SESSION["warrant_role"];
		$warrant_role = empty($_SESSION["warrant_role"]) ? file_get_contents("data/system/warrant.php") : $_SESSION["warrant_role"];
		//dump($warrant_role);die;
        if(empty($_REQUEST['username'])) {
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'用户名不能为空！'));
			//echo $jsonmsg;
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
			die;
        }elseif (empty($_REQUEST['password'])){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'密码不能为空！'));
			//echo $jsonmsg;
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
			die;
        }
		$isExpired = false; //专门为管理员设置的标记
		//$date = date("Ymd");
		$date = strtotime(date("Y-m-d"));
		//dump($date);die;
		//$date = "20141205";
		$checkRole = getSysinfo();
		if( $_REQUEST['username'] != "admin" ){
			$expireDate = strtotime($checkRole[1]);
			if( $date > $expireDate || $warrant_role == "N" ){
				$jsonmsg = json_encode(array('result'=>'Error','msg'=>'该系统已经过试用期或者授权已收回，请联系管理员'));
				//echo $jsonmsg;
				echo $_REQUEST['callback'].'('.$jsonmsg.')';
				die;
			}
			$lic = $checkRole[3];
			if( $lic != '1'){
				$jsonmsg = json_encode(array('result'=>'Error','msg'=>'授权文件错误！'));
				//echo $jsonmsg;
				echo $_REQUEST['callback'].'('.$jsonmsg.')';
				die;
			}
		}else{
			$expireDate = strtotime($checkRole[1]);
			if( $date > $expireDate ){
				$isExpired = true; //专门为管理员设置的标记
			}

		}
		//dump($date);
		//dump($expireDate);die;
		//dump($_SESSION);die;
        $user = M("users");
		$username = $_REQUEST["username"];
		$password = $_REQUEST["password"];
		$arrUser = $user->table("users u")->join("role r on (u.r_id=r.r_id)")->where("username = '$username' AND password = '$password'")->find();
		//dump($arrUser);die;
		if($arrUser){
			$_SESSION["user_info"] = $arrUser;
			$_SESSION["user_priv"] = json_decode( $arrUser['action_list'],true);

			cookie('user_info',$arrUser);
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'用户名或密码错误！'));
			echo $_REQUEST['callback'].'('.$jsonmsg.')';
			//echo $jsonmsg;
			die;
		}

		//dump($_SESSION["user_info"]);die;
		$arrUserInfo = array("username"=>$arrUser["username"],"extension"=>$arrUser["extension"],"extension_type"=>$arrUser["extension_type"],"cn_name"=>$arrUser["cn_name"],"en_name"=>$arrUser["en_name"],"phone"=>$arrUser["phone"],"roleName"=>$arrUser["r_name"]);
		if($arrUser){
			$this->privMenu($_REQUEST['username'],$_SESSION["user_priv"]);
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'模拟登录成功！'));  //,'usermsgk'=>$arrUser["username"],'userInfo'=>$arrUserInfo
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'模拟登录失败！'));
		}

		echo $_REQUEST['callback'].'('.$jsonmsg.')';
		//echo $jsonmsg;
    }

	function privMenu($username,$user_priv){
		//header("Access-Control-Allow-Origin: *");
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model('menu');
		$mainMenu = $menu->table("menu me")->field("me.id AS id,name,me.icon,moduleclass,order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=0 AND enabled='Y'")->order("`order` ASC")->select();
		//dump($mainMenu);die;

		$_SESSION['menu'] = Array();//存放子菜单的父菜单，为以后判断子菜单的权限的方便
		foreach( $mainMenu AS $row ){
			$mainId = $row['id'];
			//$subMenu = $menu->where("pid=$mainId")->order("`order` ASC")->select();
			$subMenu = $menu->table("menu me")->field("me.id AS id,me.icon as iconCls,name,moduleclass, order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			if( 'admin'==$username ){//如果是管理员，默认显示所有模块加载的菜单
				//下面这样做，是为了让管理员过期后只显示一个菜单
				if( array_key_exists($row['name'],$user_priv) ){
					$tpl_menu[$row['name']]['thisMenu'] = $row['name'];//一级菜单应该显示
					$tpl_menu[$row['name']]['iconCls'] = $row['icon'];//一级菜单应该显示
					//下面查询一级菜单下面的二级菜单有哪些应该显示

					foreach( $subMenu AS $arrRow ){
						if( array_key_exists($arrRow['name'],$user_priv[$row['name']]) ){
							$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
							$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
						}
					}
				}

			}else{ //非管理员
				if( array_key_exists($row['name'],$user_priv) ){
					$tpl_menu[$row['name']]['thisMenu'] = $row['name'];//一级菜单应该显示
					$tpl_menu[$row['name']]['iconCls'] = $row['icon'];//一级菜单应该显示
					//下面查询一级菜单下面的二级菜单有哪些应该显示

					foreach( $subMenu AS $arrRow ){
						if( array_key_exists($arrRow['name'],$user_priv[$row['name']]) ){
							$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
							$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
						}
					}
				}
			}
		}
	}

	//查看所有的分机号
	function viewExtension(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$exten = new Model("asterisk.users");
		$arrData = $exten->table("asterisk.users u")->field("u.extension,d.tech")->join("asterisk.devices d on (u.extension = d.id)")->select();
		//$jsonmsg = json_encode($arrData);
		//echo $_REQUEST['callback'].'('.$jsonmsg.')';
		$jsonmsg = json_encode(array('result'=>'Error','msg'=>$arrData));
		echo $jsonmsg;
	}

	//工号绑定分机
	function usernameToExten(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		$username = $_REQUEST["username"];
		$exten = $_REQUEST["exten"];
		$users = new Model("users");
		$arrUser = $users->field("username,extension")->where("username='$username'")->find();
		$arrExten = $users->field("username,extension")->where("extension='$exten'")->find();
		if(!$arrUser){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'该工号不存在！'));
			//echo $_REQUEST['callback'].'('.$jsonmsg.')';
			echo $jsonmsg;
			die;
		}
		$countUser2 = $users->where("username='$username' AND extension='$exten'")->count();
		if($countUser2 > 0){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'该工号已经跟该分机绑定了！'));
			//echo $_REQUEST['callback'].'('.$jsonmsg.')';
			echo $jsonmsg;
			die;
		}
		$extens = new Model("asterisk.users");
		$countExten = $extens->table("asterisk.users u")->field("u.extension,d.tech")->join("asterisk.devices d on (u.extension = d.id)")->where("u.extension = '$exten'")->count();
		if($countExten <= 0){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'该分机不存在！'));
			//echo $_REQUEST['callback'].'('.$jsonmsg.')';
			echo $jsonmsg;
			die;
		}
		$thisExtension = $arrUser["extension"];
		$otherUser = $arrExten["username"];

		$result = $users->where("username='$username'")->save(array("extension"=>$exten));
		if($result != false){
			$users->where("username='$otherUser'")->save(array("extension"=>$thisExtension));
			$this->usersCache();
			$jsonmsg = json_encode(array('result'=>'Success','msg'=>'绑定成功！'));
			//echo $_REQUEST['callback'].'('.$jsonmsg.')';
			echo $jsonmsg;
		}else{
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'绑定失败！'));
			//echo $_REQUEST['callback'].'('.$jsonmsg.')';
			echo $jsonmsg;
		}
	}

	function usersCache(){
		$users = new Model("users");
		$sellData = $users->order("username asc")->table("users u")->field("u.username,u.cn_name,u.r_id,u.d_id,r.agent_type")->join("role r on u.r_id = r.r_id")->where("r.agent_type = 'sell'")->select();
		foreach($sellData as $ksy=>$val){
			$sellUser[] = $val['username'];
			$sellDept[] = $val['d_id'];
			//$sell_cnUser[$val['username']] = $val['cn_name'];
		}

		$serviceData = $users->order("username asc")->table("users u")->field("u.username,u.r_id,u.d_id,r.agent_type")->join("role r on u.r_id = r.r_id")->where("r.agent_type = 'service'")->select();
		foreach($serviceData as $ksy=>$val){
			$serviceUser[] = $val['username'];
			$serviceDept[] = $val['d_id'];
			//$service_cnUser[$val['username']] = $val['cn_name'];
		}

		$uData = $users->order("username asc")->table("users u")->field("u.username,u.cn_name,u.extension,u.phone,u.d_id,d.d_name")->join("department d on u.d_id = d.d_id")->select();
		foreach($uData as $key=>$val){
			$exten_user[$val['username']] = $val["extension"];
			$cn_user[$val['username']] = $val["cn_name"];
			$deptId_user[$val['username']] = $val["d_id"];
			$deptName_user[$val['username']] = $val["d_name"];
			if($val["extension"]){
				$exten_cnName[$val["extension"]] = $val["cn_name"];
			}
			if($val["phone"]){
				$phone_cnName[$val["phone"]] = $val["cn_name"];
			}
		}

		$department = new Model("department");
		$deptData = $department->select();
		foreach($deptData as $val){
			$deptId[$val["d_id"]] = $val["d_name"];
		}

		$arrDu = $users->field("username,d_id")->select();
		$arrDeptIdUser = $this->groupBy($arrDu,"d_id","username");

		$userArr = array(
						"sell_user"=>$sellUser,
						"service_user"=>$serviceUser,
						"sell_dept"=>array_unique($sellDept),
						"service_dept"=>array_unique($serviceDept),
						"cn_user"=>$cn_user,
						"deptId_user"=>$deptId_user,
						"deptName_user"=>$deptName_user,
						"deptId_name"=>$deptId,
						"exten_cnName"=>$exten_cnName,
						"phone_cnName"=>$phone_cnName,
						"exten_user"=>$exten_user,
						"deptIdUser"=>$arrDeptIdUser,
						//"cn_user"=>array_merge($sell_cnUser,$service_cnUser),
					);
		F('users',$userArr,"BGCC/Conf/");
	}

	function groupBy($arr, $key_field,$value_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row[$value_field];
		}
		return $ret;
	}


	function getTimeIntervalRate(){
		$type = $_REQUEST["type"];
		checkLogin2($type);
		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];

		if($start_date && $end_date){
			$result = exec("/var/lib/asterisk/agi-bin/autocall/tmpRate.php -s $start_date -e $end_date ");
		}else{
			$result = exec("/var/lib/asterisk/agi-bin/autocall/tenantRate.php -t $start_date ");
		}
		echo json_encode($result);
	}

	function downloadCDR(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}

		Header("Content-type: text/html; charset=utf-8");
		$uniqueid = $_REQUEST['uniqueid'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];

		$mod = M("asteriskcdrdb.cdr");
		$arrData = $mod->where("uniqueid = '$uniqueid'")->find();
		$src = $arrData["src"];
		$dst = $arrData["dst"];

		$Ymd = Date('Ymd',$timestamp);
		$His = Date('His',$timestamp);
		$mp3Filename = "$Ymd-$His-[$src]-[$dst].mp3";

		$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
		$WAVfile = $dirPath ."/$uniqueid.WAV";
		//echo $WAVfile;die;
		if(!file_exists($WAVfile)){
			echo "录音文件不存在!!\n";die;
		}
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        //header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        //header("Content-Disposition: attachment;filename=" ."$uniqueid.mp3");
        header("Content-Disposition: attachment;filename=" .$mp3Filename);
        system("/usr/bin/sox $WAVfile -t mp3 -");
	}


	//播放录音接口
	//http://192.168.10.13/index.php?m=Interface&a=playRecord&uniqueid=1476177280.0
	function playRecord(){
		header("Access-Control-Allow-Origin: *");
		$clientIP = $this->getClientIP();
		if(!$clientIP){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'抱歉，您的电脑无权访问该接口！'));
			echo $jsonmsg;
			die;
		}
		$uniqueid = $_REQUEST['uniqueid'];
		if( !$uniqueid ){
			$jsonmsg = json_encode(array('result'=>'Error','msg'=>'录音文件不存在!'));
			echo $jsonmsg;
			die;
		};
		$PlayURL = "index.php?m=Interface&a=downloadRecord&uniqueid={$uniqueid}";
		$PlayURL = urlencode($PlayURL);
		echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head>
			<title>播放通话录音</title>
			<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
			<link href='include/lib/ligerUI/skins/Gray/css/modules.css' rel='stylesheet' type='text/css' />
			</head>
			<body>
			<div style='padding-left:470px;margin-top:30px;'>
			<embed width='365' height='50' align='middle' src='include/ui/kaixin_player.swf' flashvars='url=".$PlayURL."&autoplay=1' wmode='transparent' loop='false' menu='false' quality='high' scale='noscale' salign='lt' bgcolor='#ffffff' allowscriptaccess='sameDomain' allowfullscreen='false' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer' />
			</div>
			</body>
			</html>";
    }

}

?>

