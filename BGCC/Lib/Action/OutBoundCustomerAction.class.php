<?php
class OutBoundCustomerAction extends Action{
	function outBoundList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "OutBound Customer";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['list_enabled'] == "Y" ){
				if($val['en_name'] != "createuser" ){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					$arrField[0][$i]['width'] = "100";
				}
				if( $val['tiptools_enabled'] == 'Y' ||  $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}

		$arrFd = array("field"=>"modifytime","title"=>"最后呼叫时间","width"=>"140","sortable"=>"true");
		$arrFd2 = array("field"=>"ck","checkbox"=>true);
		$arrFd3 = array("field"=>"dealuser","title"=>"创建人工号","width"=>"100");
		$arrFd4 = array("field"=>"cn_name","title"=>"创建人姓名","width"=>"100");
		$arrFd5 = array("field"=>"dealresult_id","title"=>"客户状态","width"=>"100");
		$arrFd6 = array("field"=>"calledflag","title"=>"呼叫状态","width"=>"100");
		$arrFd7 = array("field"=>"brand","title"=>"是否已经转交到呼入平台","width"=>"150");
		$arrFd8 = array("field"=>"key_value","title"=>"按键值","width"=>"50");
		$arrFd9 = array("field"=>"recordtag","title"=>"坐席标记","width"=>"80");

		array_unshift($arrField[0],$arrFd6);
		array_unshift($arrField[0],$arrFd5);
		array_unshift($arrField[0],$arrFd4);
		array_unshift($arrField[0],$arrFd3);
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd2);
		//array_push($arrField[0],$arrFd7);
		array_push($arrField[0],$arrFd8);
		array_push($arrField[0],$arrFd9);
		//dump($fielddTpl);die;
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("fielddTpl",$fielddTpl);

		$areaTpl = implode(",",$areaTpl2);
		$this->assign("areaTpl",$areaTpl);

		$para_sys = readS();
		$export_rule = $para_sys["export_rule"];
		$this->assign("export_rule",$export_rule);

		$this->display();
	}

	function outBoundCMData(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$dealresult = $_REQUEST['dealresult'];
		$calledflag = $_REQUEST['calledflag'];
		$key_value = $_REQUEST['key_value'];
		$recordtag = $_REQUEST['recordtag'];
		$start_time = $_REQUEST['start_time'];
		$end_time = $_REQUEST['end_time'];
		$search_type = $_REQUEST["search_type"];

		$where = "1";
		$where .= empty($startime)?"":" AND modifytime >='$startime'";
        $where .= empty($endtime)?"":" AND modifytime <='$endtime'";

		$arrField = array('modifytime','dealuser','cn_name','dealresult_id','calledflag');
		$arrTitle = array('最后呼叫时间','创建人工号','创建人姓名','客户状态','呼叫状态');
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){    //&& $val['text_type'] != '3'
				$arrField[] = $val['en_name'];
				$arrTitle[] = $val['cn_name'];
				$arrF[] = $val['en_name'];
				//$$val['en_name'] = $_REQUEST[$val['en_name']];

				if( $val['text_type'] == '3'){
					$areaTpl[$val["en_name"]] = $val["en_name"];
				}elseif($val['text_type'] == '4'){
					$start = $val['en_name']."_start";
					$$start = $_REQUEST[$start];
					$end = $val['en_name']."_end";
					$$end = $_REQUEST[$end];
				}else{
					$$val['en_name'] = $_REQUEST[$val['en_name']];
				}
			}
		}

		array_push($arrField,"key_value");
		array_push($arrTitle,"按键值");
		array_push($arrField,"recordtag");
		array_push($arrTitle,"坐席标记");


		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3' && $vm['en_name'] != 'createuser'){
				$aa[] = $vm['en_name'];
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					if($vm['text_type'] == '2'){
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
					}else{
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
					}
				}


				if($vm['text_type'] == '4'){
					$start = $vm['en_name']."_start";
					$end = $vm['en_name']."_end";
					 $where .= empty($$start)?"":" AND ".$vm['en_name'] ." >= '".$$start."'";
					 $where .= empty($$end)?"":" AND ".$vm['en_name'] ." <= '".$$end."'";
				}
			}
		}
        $where .= empty($createuser)?"":" AND dealuser = '$createuser'";
		if($dealresult == "0"){
			$where .= " AND dealresult_id = '0'";
		}else{
			$where .= empty($dealresult)?"":" AND dealresult_id = '$dealresult'";
		}
		$where .= empty($calledflag)?"":" AND calledflag = '$calledflag'";
		//$where .= empty($key_value)?"":" AND key_value = '$key_value'";
		if($key_value == "0"){
			$where .= " AND key_value = '0'";
		}elseif($key_value == "all"){
			$where .= " AND key_value is not null  AND key_value!='' ";
		}elseif($key_value == "not"){
			$where .= " AND (key_value is null OR key_value = '')";
		}else{
			$where .= empty($key_value)?"":" AND key_value = '$key_value'";
		}

		if($recordtag == "all"){
			$where .= " AND recordtag is not null AND recordtag !=''";
		}elseif($recordtag == "not"){
			$where .= " AND (recordtag is null OR recordtag = '')";
		}else{
			$where .= empty($recordtag)?"":" AND recordtag = '$recordtag'";
		}
		$where .= empty($start_time)?"":" AND createtime >= '$start_time'";
		$where .= empty($end_time)?"":" AND createtime <= '$end_time'";

		//dump($where);die;
		$task_id = $_REQUEST["task_id"];
		//dump($task_id);die;
		$source = new Model("sales_source_".$task_id);

		$para_sys = readS();
		$export_rule = $para_sys["export_rule"];

		if($search_type != "xls"){
			$count = $source->where($where)->count();
		}
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		$sort = $_REQUEST["sort"];
		$order = $_REQUEST["order"];
		if($sort){
			$usort = $sort." ".$order;
		}else{
			$usort = "modifytime desc";
		}

		$fields = "id,dealuser,calledflag,createtime,modifytime,dealresult_id,brand,key_value,recordtag,".implode(",",$arrF);
		if($search_type == "xls" && $export_rule != "page"){
			$sourceData = $source->order($usort)->field($fields)->where($where)->select();
		}else{
			$sourceData = $source->order($usort)->field($fields)->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}

		$sqlSearch = $source->getLastSql();

		//echo $source->getLastSql();
		//dump($sourceData);die;
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		//$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($sourceData as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;

					$vm['cn_name'] = $cnName[$vm["dealuser"]];
				}
			}
		}

		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$statusArr2 = array("1"=>"未呼叫","2"=>"未接听","3"=>"已接听");
		$brandArr = array("Y"=>"是","N"=>"否");
		$mark = getMark();
		foreach($sourceData as &$val){
			$val["massPhone"] = $val['phone1'];
			$val["massPhone2"] = $val['phone2'];
			$val["massEmail"] = $val['email'];

			$dealresult = $statusArr[$val["dealresult_id"]];
			$val["dealresult_id"] = $dealresult;

			$calledflag = $statusArr2[$val["calledflag"]];
			$val["calledflag"] = $calledflag;

			$recordtag = $mark[$val["recordtag"]];
			$val["recordtag"] = $recordtag;

			if($val["brand"]){
				$brand = $brandArr[$val["brand"]];
				$val["brand"] = $brand;
			}

			if($username != "admin"){
				if($para_sys["outboundCustomer"] == "yes"){
					if($para_sys["hide_phone"] =="yes"){
						$val["phone1"] = substr($val["phone1"],0,3)."***".substr($val["phone1"],-4);
						if($val["phone2"]){
							$val["phone2"] = substr($val["phone2"],0,3)."***".substr($val["phone2"],-4);
						}
					}
					if($val['email']){
						if($para_sys["hide_email"] =="yes"){
							$val["email"] = "***".strstr($val["email"],"@");
						}
					}
					if($val['qq_number']){
						if($para_sys["hide_qq"] =="yes"){
							$val["qq_number"] = "***".substr($val["qq_number"],3);
						}
					}
				}
			}


			foreach($cmFields as &$vm){
				if($vm['list_enabled'] == 'Y'){
					if($vm["text_type"] == "4"){
						if($val[$vm["en_name"]] == "0000-00-00 00:00:00"){
							$val[$vm["en_name"]] = "";
						}
					}elseif($vm["text_type"] == "5"){
						if($val[$vm["en_name"]] == "0000-00-00"){
							$val[$vm["en_name"]] = "";
						}
					}
				}
			}

		}

		if($search_type == "xls"){
			$xls_count = count($arrField);
			$excelTiele = "外呼资料".date("Y-m-d");
			//dump($sourceData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$sourceData,$excelTiele);
			die;
		}

		//dump($where);die;
		//dump($sourceData);die;
		$rowsList = count($sourceData) ? $sourceData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["sears"] = base64_encode($sqlSearch);
		$ary["searchWhere"] = $where;

		echo json_encode($ary);
	}


	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	function editOutBoundCustomer(){
		checkLogin();
		$extension = $_SESSION["user_info"]["extension"];
		$task_id = $_REQUEST['task_id'];
		$id = $_REQUEST['id'];

		$source = new Model("sales_source_".$task_id);

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');
		$para_sys = readS();

		foreach($fielddTpl2 as $val){
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields" && $val["en_name"] != "sms_cust"){
					$fielddTpl[] = $val;
				}
			}else{
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
					$fielddTpl[] = $val;
				}
			}
		}
		//$fields = "`".implode("`,`",$arrF)."`".",`country`,`province`,`city`,`district`";
		$fields = "`".implode("`,`",$arrF)."`";
		$this->assign("fielddTpl",$fielddTpl);
		//dump($fielddTpl);die;
		$sourceData =  $source->field($fields)->where("id = '$id'")->find();
		//echo $source->getLastSql();die;

		$this->assign("task_id",$task_id);
		$this->assign("exten",$extension);
		$this->assign("id",$id);
		$this->assign("custList",$sourceData);

		$this->display();
	}


	//编辑客户资料
	function updateCustomer(){
		$username = $_SESSION["user_info"]["username"];
		//dump($id);
		$task_id = $_REQUEST['task_id'];
		$id = $_REQUEST['id'];

		$source = new Model("sales_source_".$task_id);

		//$customer = new Model("customer");
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
				if($val['en_name'] == 'phone1'){
					if($_REQUEST['message_phone'] == "ms"){
						$arrData['phone1'] = $_REQUEST['mess_phone'];
					}else{
						$arrData['phone1'] = $_REQUEST['phone1'];
					}
				}elseif($val['en_name'] == 'email'){
					if($_REQUEST['message_email'] == "ms"){
						$arrData['email'] = $_REQUEST['mess_email'];
					}else{
						$arrData['email'] = $_REQUEST['email'];
					}
				}else{
					$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
				}
			}
		}
		$arrData["modifytime"] = date("Y-m-d H:i:s");

		//dump($cmFields);die;
		//dump($arrData);die;
		$result = $source->data($arrData)->where("id = $id")->save();
		//echo $source->getLastSql();die;
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'客户资料修改成功!','editid'=>$id));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function saveTransFerees(){
		$task_id = $_REQUEST["task_id"];
		$id = $_REQUEST["id"];
		$calledflag = $_REQUEST["calledflag"];
		$dealresult_trans = $_REQUEST["dealresult_trans"];
		$deptuser = $_REQUEST["deptuser"];

		$arrData = array(
			"dealuser"=>$deptuser,
			"locked"=>"Y",
		);

		if($dealresult_trans != "Y"){
			$arrData["dealresult_id"] = $dealresult_trans;
		}

		$source = new Model("sales_source_".$task_id);
		$result = $source->data($arrData)->where("id in ($id)")->save();
		//dump($calledflag);
		//dump($arrData);die;

		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"转交成功！"));
		} else {
			echo json_encode(array('msg'=>'转交失败！'));
		}
	}

	function saveTransFereesBak20160325(){
		$task_id = $_REQUEST["task_id"];
		$id = $_REQUEST["id"];
		$calledflag = $_REQUEST["calledflag"];
		$dealresult_trans = $_REQUEST["dealresult_trans"];
		$deptuser = $_REQUEST["deptuser"];
		$arrId = explode(",",$id);
		$count = count($arrId);

		if($dealresult_trans != "Y"){
			$dealresult_id = $_REQUEST["dealresult_trans"];
		}else{
			if($calledflag != "3"){
				$dealresult_id = "0";
			}
		}

		$source = new Model("sales_source_".$task_id);
		if($calledflag){
			if($calledflag != "3" ){
				$arrData = array(
					"dealuser"=>$deptuser,
					"locked"=>"Y",
					"dealresult_id"=>$dealresult_id,
				);
			}else{
				$arrData = array(
					"dealuser"=>$deptuser,
					"dealresult_id"=>$dealresult_id,
				);
				if($dealresult_trans == "Y"){
					array_pop($arrData);
				}
			}
		}else{
			$arrData = array(
				"dealuser"=>$deptuser,
				"dealresult_id"=>$dealresult_id,
			);
			if($dealresult_trans == "Y"){
				array_pop($arrData);
			}
		}

		//dump($calledflag);
		//dump($arrData);die;
		for($i=0;$i<$count;$i++){
			$result[$i] = $source->data($arrData)->where("id = $arrId[$i] ")->save();
		}

		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"转交成功！"));
		} else {
			echo json_encode(array('msg'=>'转交失败！'));
		}
	}

	function saveTransFereesBak(){
		$task_id = $_REQUEST["task_id"];
		$id = $_REQUEST["id"];
		$deptuser = $_REQUEST["deptuser"];
		$arrId = explode(",",$id);
		$count = count($arrId);

		$source = new Model("sales_source_".$task_id);
		$arrData = array("dealuser"=>$deptuser);
		for($i=0;$i<$count;$i++){
			$result[$i] = $source->data($arrData)->where("id = $arrId[$i] ")->save();
		}

		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"转交成功！"));
		} else {
			echo json_encode(array('msg'=>'转交失败！'));
		}
	}

	//用PHPExcel导出----不能导出大数据
	function exportCustomerToExcel33(){
		set_time_limit(0);
		@ini_set('memory_limit', '-1');
		$callStartTime = microtime(true);
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$dealresult = $_REQUEST['dealresult'];
		$calledflag = $_REQUEST['calledflag'];
		$key_value = $_REQUEST['key_value'];
		$recordtag = $_REQUEST['recordtag'];

		$where = "1";
		$where .= empty($startime)?"":" AND modifytime >='$startime'";
        $where .= empty($endtime)?"":" AND modifytime <='$endtime'";

		//$arrFd5 = array("field"=>"dealresult_id","title"=>"客户状态","width"=>"100");
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != "createuser" ){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
				//$$val['en_name'] = $_REQUEST[$val['en_name']];
				if($val['text_type'] == '4'){
					$start = $val['en_name']."_start";
					$$start = $_REQUEST[$start];
					$end = $val['en_name']."_end";
					$$end = $_REQUEST[$end];
				}else{
					$$val['en_name'] = $_REQUEST[$val['en_name']];
				}
			}
		}
		if($recordtag == "all"){
			$where .= " AND recordtag is not null AND recordtag !=''";
		}elseif($recordtag == "not"){
			$where .= " AND (recordtag is null OR recordtag = '')";
		}else{
			$where .= empty($recordtag)?"":" AND recordtag = '$recordtag'";
		}


		array_unshift($field,"dealresult_id");
		array_unshift($title,"客户状态");

		array_unshift($field,"dealuser");
		array_unshift($title,"创建人工号");

		array_unshift($field,"modifytime");
		array_unshift($title,"最后呼叫时间");

		array_push($field,"key_value");
		array_push($title,"按键值");

		array_push($field,"recordtag");
		array_push($title,"坐席标记");

		$count = count($field);
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3' && $vm['en_name'] != 'createuser'){
				$aa[] = $vm['en_name'];
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					if($vm['text_type'] == '2'){
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
					}else{
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
					}
				}


				if($vm['text_type'] == '4'){
					$start = $vm['en_name']."_start";
					$end = $vm['en_name']."_end";
					 $where .= empty($$start)?"":" AND ".$vm['en_name'] ." >= '".$$start."'";
					 $where .= empty($$end)?"":" AND ".$vm['en_name'] ." <= '".$$end."'";
				}
			}
		}
        $where .= empty($createuser)?"":" AND dealuser = '$createuser'";
        if($dealresult == "0"){
			$where .= " AND dealresult_id = '0'";
		}else{
			$where .= empty($dealresult)?"":" AND dealresult_id = '$dealresult'";
		}
		$where .= empty($calledflag)?"":" AND calledflag = '$calledflag'";
		//$where .= empty($key_value)?"":" AND key_value = '$key_value'";
		if($key_value == "0"){
			$where .= " AND key_value = '0'";
		}elseif($key_value == "all"){
			$where .= " AND key_value is not null  AND key_value!='' ";
		}elseif($key_value == "not"){
			$where .= " AND (key_value is null OR key_value = '')";
		}else{
			$where .= empty($key_value)?"":" AND key_value = '$key_value'";
		}

		//dump($where);die;
		$task_id = $_REQUEST["task_id"];
		$source = new Model("sales_source_".$task_id);

		$fields = "id,dealuser,createtime,modifytime,dealresult_id,key_value,".implode(",",$field);
		$sourceData = $source->order("modifytime desc,createtime desc")->field($fields)->where($where)->select();

		//echo $source->getLastSql();die;
		//dump($sourceData);die;
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		//$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($sourceData as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;

					$vm['cn_name'] = $cnName[$vm["dealuser"]];
				}
			}
		}

		//dump($sourceData);die;
		vendor("PHPExcel176.PHPExcel");
		/*
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB');
		*/
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array( 'cacheTime' => 600,'memoryCacheSize'  => '64MB'   );
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];
		//dump($lm);
		//dump($letters2);die;
		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$mark = getMark();
		/*
		foreach($sourceData as &$val){
			$dealresult = $statusArr[$val["dealresult_id"]];
			$val["dealresult_id"] = $dealresult;
			$recordtag = $mark[$val["recordtag"]];
			$val["recordtag"] = $recordtag;
		}
		*/

		$field_key = array_flip($field);
		//dump($field_key);die;
		$start_row = 2;
		foreach($sourceData as &$val){
			$dealresult = $statusArr[$val["dealresult_id"]];
			$val["dealresult_id"] = $dealresult;
			$recordtag = $mark[$val["recordtag"]];
			$val["recordtag"] = $recordtag;

			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
				if($j == $field_key["phone1"] || $j == $field_key["phone2"] ){
					$objPHPExcel->getActiveSheet()->setCellValueExplicit($letters2[$j].$start_row, $val[$field[$j]],PHPExcel_Cell_DataType::TYPE_STRING);
				}
			}
			$start_row++;
		}
		//dump($sourceData);die;

        //sheet命名
        $objPHPExcel->getActiveSheet()->setTitle('客户资料');
		$objPHPExcel->setActiveSheetIndex(0);

		$name = "外呼客户资料";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}


	//用Excel相关函数导出----可以导出5万条数据
	function exportCustomerToExcel(){
		set_time_limit(0);
		@ini_set('memory_limit', '-1');
		$callStartTime = microtime(true);
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$dealresult = $_REQUEST['dealresult'];
		$calledflag = $_REQUEST['calledflag'];
		$key_value = $_REQUEST['key_value'];
		$recordtag = $_REQUEST['recordtag'];

		$where = "1";
		$where .= empty($startime)?"":" AND modifytime >='$startime'";
        $where .= empty($endtime)?"":" AND modifytime <='$endtime'";

		//$arrFd5 = array("field"=>"dealresult_id","title"=>"客户状态","width"=>"100");
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != "createuser" ){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
				//$$val['en_name'] = $_REQUEST[$val['en_name']];
				if($val['text_type'] == '4'){
					$start = $val['en_name']."_start";
					$$start = $_REQUEST[$start];
					$end = $val['en_name']."_end";
					$$end = $_REQUEST[$end];
				}else{
					$$val['en_name'] = $_REQUEST[$val['en_name']];
				}
			}
		}
		if($recordtag == "all"){
			$where .= " AND recordtag is not null AND recordtag !=''";
		}elseif($recordtag == "not"){
			$where .= " AND (recordtag is null OR recordtag = '')";
		}else{
			$where .= empty($recordtag)?"":" AND recordtag = '$recordtag'";
		}


		array_unshift($field,"dealresult_id");
		array_unshift($title,"客户状态");

		array_unshift($field,"dealuser");
		array_unshift($title,"创建人工号");

		array_unshift($field,"modifytime");
		array_unshift($title,"最后呼叫时间");

		array_push($field,"key_value");
		array_push($title,"按键值");

		array_push($field,"recordtag");
		array_push($title,"坐席标记");

		$count = count($field);
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3' && $vm['en_name'] != 'createuser'){
				$aa[] = $vm['en_name'];
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					if($vm['text_type'] == '2'){
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
					}else{
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
					}
				}


				if($vm['text_type'] == '4'){
					$start = $vm['en_name']."_start";
					$end = $vm['en_name']."_end";
					 $where .= empty($$start)?"":" AND ".$vm['en_name'] ." >= '".$$start."'";
					 $where .= empty($$end)?"":" AND ".$vm['en_name'] ." <= '".$$end."'";
				}
			}
		}
        $where .= empty($createuser)?"":" AND dealuser = '$createuser'";
        if($dealresult == "0"){
			$where .= " AND dealresult_id = '0'";
		}else{
			$where .= empty($dealresult)?"":" AND dealresult_id = '$dealresult'";
		}
		$where .= empty($calledflag)?"":" AND calledflag = '$calledflag'";
		//$where .= empty($key_value)?"":" AND key_value = '$key_value'";
		if($key_value == "0"){
			$where .= " AND key_value = '0'";
		}elseif($key_value == "all"){
			$where .= " AND key_value is not null  AND key_value!='' ";
		}elseif($key_value == "not"){
			$where .= " AND (key_value is null OR key_value = '')";
		}else{
			$where .= empty($key_value)?"":" AND key_value = '$key_value'";
		}

		//dump($where);die;
		$task_id = $_REQUEST["task_id"];
		$source = new Model("sales_source_".$task_id);

		$fields = "id,dealuser,createtime,modifytime,dealresult_id,key_value,".implode(",",$field);
		$sourceData = $source->order("modifytime desc,createtime desc")->field($fields)->where($where)->select();

		//echo $source->getLastSql();die;
		//dump($sourceData);die;
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		//$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($sourceData as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;

					$vm['cn_name'] = $cnName[$vm["dealuser"]];
				}
			}
		}

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];


		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$mark = getMark();

		$field_key = array_flip($field);


		$filename ="外呼客户资料".date('Y-m-d');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type:text/html;charset=UTF-8");
		$filename = iconv("utf-8","gb2312",$filename);
        header("Content-Disposition: attachment;filename=$filename.xls ");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();
        $start_row	=	0;

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
			xlsWriteLabel($start_row,$i,utf2gb($title[$i]));
		}

        $start_row++;
		foreach($sourceData as &$val){
			$dealresult = $statusArr[$val["dealresult_id"]];
			$val["dealresult_id"] = $dealresult;
			$recordtag = $mark[$val["recordtag"]];
			$val["recordtag"] = $recordtag;

			for($j=0;$j<$count;$j++){
				 xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
			}
			$start_row++;
		}
        xlsEOF();
	}



	function exportCustomerToCsv(){
		set_time_limit(0);
		@ini_set('memory_limit', '-1');
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$dealresult = $_REQUEST['dealresult'];
		$calledflag = $_REQUEST['calledflag'];
		$key_value = $_REQUEST['key_value'];
		$recordtag = $_REQUEST['recordtag'];

		$where = "1";
		$where .= empty($startime)?"":" AND modifytime >='$startime'";
        $where .= empty($endtime)?"":" AND modifytime <='$endtime'";

		//$arrFd5 = array("field"=>"dealresult_id","title"=>"客户状态","width"=>"100");
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != "createuser" ){
					$field[] = $val['en_name'];
					//$title[] = $val['cn_name'];
					$title[] = iconv("utf-8","gb2312",$val['cn_name']);
				}
				//$$val['en_name'] = $_REQUEST[$val['en_name']];
				if($val['text_type'] == '4'){
					$start = $val['en_name']."_start";
					$$start = $_REQUEST[$start];
					$end = $val['en_name']."_end";
					$$end = $_REQUEST[$end];
				}else{
					$$val['en_name'] = $_REQUEST[$val['en_name']];
				}
			}
		}

		array_unshift($field,"dealresult_id");
		array_unshift($title,iconv("utf-8","gb2312","客户状态"));

		array_unshift($field,"dealuser");
		array_unshift($title,iconv("utf-8","gb2312","创建人工号"));

		array_unshift($field,"modifytime");
		array_unshift($title,iconv("utf-8","gb2312","最后呼叫时间"));

		array_push($field,"key_value");
		array_push($title,iconv("utf-8","gb2312","按键值"));

		array_push($field,"recordtag");
		array_push($title,iconv("utf-8","gb2312","坐席标记"));

		$count = count($field);
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3' && $vm['en_name'] != 'createuser'){
				$aa[] = $vm['en_name'];
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					if($vm['text_type'] == '2'){
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
					}else{
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
					}
				}


				if($vm['text_type'] == '4'){
					$start = $vm['en_name']."_start";
					$end = $vm['en_name']."_end";
					 $where .= empty($$start)?"":" AND ".$vm['en_name'] ." >= '".$$start."'";
					 $where .= empty($$end)?"":" AND ".$vm['en_name'] ." <= '".$$end."'";
				}
			}
		}
        $where .= empty($createuser)?"":" AND dealuser = '$createuser'";
        if($dealresult == "0"){
			$where .= " AND dealresult_id = '0'";
		}else{
			$where .= empty($dealresult)?"":" AND dealresult_id = '$dealresult'";
		}
		$where .= empty($calledflag)?"":" AND calledflag = '$calledflag'";
		//$where .= empty($key_value)?"":" AND key_value = '$key_value'";
		if($key_value == "0"){
			$where .= " AND key_value = '0'";
		}elseif($key_value == "all"){
			$where .= " AND key_value is not null  AND key_value!='' ";
		}elseif($key_value == "not"){
			$where .= " AND (key_value is null OR key_value = '')";
		}else{
			$where .= empty($key_value)?"":" AND key_value = '$key_value'";
		}

		if($recordtag == "all"){
			$where .= " AND recordtag is not null AND recordtag !=''";
		}elseif($recordtag == "not"){
			$where .= " AND (recordtag is null OR recordtag = '')";
		}else{
			$where .= empty($recordtag)?"":" AND recordtag = '$recordtag'";
		}

		$task_id = $_REQUEST["task_id"];
		$source = new Model("sales_source_".$task_id);


		//$fields = "id,dealuser,createtime,dealresult_id,".implode(",",$field);
		$fields = implode(",",$field);
		//dump($title);
		//dump($field);
		//dump($fields);die;
		$sourceData = $source->order("createtime desc")->field($fields)->where($where)->select();
		//echo $source->getLastSql();die;
		//dump($sourceData);die;
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		//$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($sourceData as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;

					//$vm['cn_name'] = $cnName[$vm["dealuser"]];
				}
			}
		}

		//dump($sourceData);die;

		$csvContent = implode(",",$title)."\r\n";

		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$mark = getMark();
		foreach($sourceData as &$val){
			$dealresult = $statusArr[$val["dealresult_id"]];
			$val["dealresult_id"] = $dealresult;
			$recordtag = $mark[$val["recordtag"]];
			$val["recordtag"] = $recordtag;

			$vv = implode(',',str_replace(",","",$val));
			$vv = str_replace("\r","",$vv);
			$vv = str_replace("\n","",$vv);
			$csvContent .= iconv("utf-8","gb2312",$vv) ."\r\n";
		}
		//dump($csvContent);die;
		$name = "外呼客户资料".date("Ymd");
		$filename = iconv("utf-8","gb2312",$name);
		$this->export_csv($filename,$csvContent);
	}

	function export_csv($filename,$data){
		$content = iconv("utf-8","gb2312",$data);
		$d = date("D M j G:i:s T Y");
        header('HTTP/1.1 200 OK');
        header('Date: ' . $d);
        header('Last-Modified: ' . $d);
        header("Content-Type: application/force-download");
        header("Content-Length: " . strlen($data));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".$filename.".csv");
        echo $data;
	}

	//导出客户资料(导出本页的客户资料)
	function exportCustomerToExcel2(){
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != "createuser" ){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
				$$val['en_name'] = $_REQUEST[$val['en_name']];
			}
		}

		array_unshift($field,"dealresult_id");
		array_unshift($title,"客户状态");

		array_unshift($field,"dealuser");
		array_unshift($title,"创建人工号");

		array_unshift($field,"modifytime");
		array_unshift($title,"最后呼叫时间");

		array_push($field,"key_value");
		array_push($title,"按键值");


		$count = count($field);
		//dump($field);
		//dump($title);die;

		$sears = base64_decode($_GET["sears"]);
		$customer = new Model("customer");
		$cmlist = $customer->query($sears);

		$row = getSelectCache();
		//dump($row);
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}



		vendor("PHPExcel176.PHPExcel");
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];
		//dump($lm);
		//dump($letters2);die;
		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($cmlist as &$val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
			}
			$start_row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$name = "客户资料";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

	}


	function saveTransFereesToCustomer(){
		$username = $_SESSION["user_info"]["username"];
		$deptuser = $_REQUEST["deptuser"];
		$task_id = $_REQUEST["task_id"];
		$id = $_REQUEST["id"];

		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
		}
		$fields = "id,dealuser,".implode(",",$arrF);
		$source = new Model("sales_source_".$task_id);
		$arrData = $source->field($fields)->where("id in ($id) AND (brand != 'Y' OR brand IS NULL)")->select();
		//echo $source->getLastSql();die;
		$customer = new Model("customer");
		foreach($arrData as $key=>&$val){
			$val["createuser"] = $deptuser;
			$val["customer_source"] = "complete";
			$val["autocall_visit"] = "Y";
			$val["transferees"] = $val["dealuser"]."->".$deptuser;
			$arrD[] = $val;
			array_shift($arrD[$key]);
			array_shift($arrD[$key]);
			$arrTrans[] = $val;
			$arrID[$val["phone1"]] = $val["id"];
		}

		$customer = new Model("customer");
		foreach($arrD as $key=>&$val){
			$val["createtime"] = date("Y-m-d H:i:s");
			$res = $customer->data($arrD[$key])->add();
			$out_id = $arrID[$val["phone1"]];
			$this->insertTransfer($res,$out_id,$task_id,$deptuser);
		}
		if ($res){
			$source->where("id in ($id)")->save(array("brand"=>"Y"));
			echo json_encode(array('success'=>true,'msg'=>'转交成功！'));
		} else {
			echo json_encode(array('msg'=>'转交失败，可能所选数据已经转交过了！'));
		}
	}

	function insertTransfer($customer_id,$id,$task_id,$deptuser){
		$username = $_SESSION["user_info"]["username"];
		$transfer = new Model("customer_transferees");
		$arrData = array(
			"customer_id" => $customer_id,
			"createtime" => date("Y-m-d H:i:s"),
			"transferType" => "out",
			"forwardedname" => $username,
			"recipient" => $deptuser,
			"task_id" => $task_id,
			"outBound_customer_id" => $id,
		);
		$transfer->data($arrData)->add();
		return true;
	}

	function saveTransFereesToCustomer2(){
		$username = $_SESSION["user_info"]["username"];
		$deptuser = $_REQUEST["deptuser"];
		$task_id = $_REQUEST["task_id"];
		$id = $_REQUEST["id"];

		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
		}
		$fields = "id,dealuser,".implode(",",$arrF);
		$source = new Model("sales_source_".$task_id);
		$arrData = $source->field($fields)->where("id in ($id)")->select();
		$customer = new Model("customer");
		foreach($arrData as $key=>&$val){
			$val["createuser"] = $deptuser;
			$val["customer_source"] = "complete";
			$val["autocall_visit"] = "Y";
			$val["transferees"] = $val["dealuser"]."->".$deptuser;
			$arrD[] = $val;
			array_shift($arrD[$key]);
			array_shift($arrD[$key]);
			$arrTrans[] = $val;
		}

		foreach($arrD as $val){
			$val["createtime"] = date("Y-m-d H:i:s");
			$arrT[] = "('".implode("','",$val)."')";
		}

		$addField = "`".implode("`,`",$arrF)."`,customer_source,autocall_visit,transferees,createtime";
		$value = implode(",",$arrT);
		$sql = "insert into customer($addField) values $value";
		$result = $source->execute($sql);
		//echo $sql;
		//dump($arrT);die;
		foreach($arrTrans as $val){
			$arrV[] = "("."'',".
						"'".date("Y-m-d H:i:s")."',".
						"'out',".
						"'".$username."',".
						"'".$deptuser."',".
						"'".$task_id."',".
						"'".$val["id"]."'".
						")";
		}
		$value2 = implode(",",$arrV);
		$sql2 = "insert into customer_transferees(customer_id,createtime,transferType,forwardedname,recipient,task_id,outBound_customer_id) values $value2";
		$result2 = $source->execute($sql2);

		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'转交成功！'));
		} else {
			echo json_encode(array('msg'=>'转交失败！'));
		}

	}


	function deleteCustomer(){
		$id = $_REQUEST["id"];
		$task_id = $_REQUEST["task_id"];
		//dump($task_id);die;
		$source = new Model("sales_source_".$task_id);
		$result = $source->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//按搜索条件分配
	function saveTransFerees2(){
		$username = $_SESSION['user_info']['username'];
		$allocation_num = $_REQUEST["allocation_num"];
		$have_dealuser = $_REQUEST["have_dealuser"];
		$searchWhere = $_REQUEST["searchWhere"];

		if($have_dealuser == "N"){
			$searchWhere .= " AND (dealuser is null OR dealuser = '')";
		}

		$task_id = $_REQUEST["task_id"];
		//dump($task_id);die;
		$customer = new Model("sales_source_".$task_id);
		$arrF = $customer->order("modifytime desc")->field("id,phone1")->where($searchWhere)->limit($allocation_num)->select();

		//dump($searchWhere);
		//dump($allocation_num);
		//dump($arrF);die;
		$workname = $_REQUEST["deptuser"];
		if(!$workname){
			echo json_encode(array('msg'=>'请选择要分配的工号！'));
			die;
		}
		$arrU = explode(",",$workname);
		//dump($workname);die;
		foreach($arrU as &$val){
			if($val){
				$workname_arr[] = $val;
			}
		}
		$count_user = count($workname_arr);
		$user_avg = floor($allocation_num/$count_user);

		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];
		//dump($userArr);die;

		foreach($arrF as $key=>&$val){
			$yushu = (floor($key/$user_avg))%$count_user;
			$val["workname"] = $workname_arr[$yushu];
		}
		//dump($arrF);die;

		$arrData = $this->groupBy($arrF,"workname");
		foreach($arrData as $key=>$val){
			$arrUser[] = $key;
			$arrUserId[$key] = $val;
			$arr = array(
				"dealuser"=>$key,
				"locked"=>"Y",
			);
			$result[] = $customer->data($arr)->where("id in (".$val.")")->save();

		}
		//dump($arrUserId);die;

		//dump($arrU);die;
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"客户资料分配成功","user_name"=>$arrUser,"id"=>$arrUserId));
		} else {
			echo json_encode(array('msg'=>'出现未知错误！'));
		}

	}

	function groupBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row["id"];
		}
		foreach($ret as $key=>$val){
			$arrT[$key] = implode(",",$ret[$key]);
		}
		//dump($arrT);die;
		return $arrT;
	}

	function taskSearchList(){
		checkLogin();

		$this->display();
	}

}
?>
