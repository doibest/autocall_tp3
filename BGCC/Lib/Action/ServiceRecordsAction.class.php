<?php
//服务记录
class ServiceRecordsAction extends Action{
	function serviceList(){
		checkLogin();
		$menuname = "Service records";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$web_type = $_REQUEST["web_type"];
		if($web_type == "agent"){
			$this->assign("web_type","agent");
		}else{
			$this->assign("web_type","back");
		}

		$this->display();
	}

	function serciveDataList(){
		$username = $_SESSION["user_info"]["username"];

		$createtime_start = $_REQUEST["createtime_start"];
		$createtime_end = $_REQUEST["createtime_end"];
		$seat = $_REQUEST["seat"];
		$name = $_REQUEST["name"];
		$phone = $_REQUEST["phone"];
		$servicename = $_REQUEST["servicename"];
		$status = $_REQUEST["status"];
		$web_type = $_REQUEST["web_type"];

		$where = "1 ";
		$where .= empty($createtime_start) ? "" : " AND sv.createtime >= '$createtime_start'";
		$where .= empty($createtime_end) ? "" : " AND sv.createtime <= '$createtime_end'";
		$where .= empty($seat) ? "" : " AND sv.seat = '$seat'";
		$where .= empty($name) ? "" : " AND c.name like '%$name%'";
		$where .= empty($phone) ? "" : " AND sv.phone like '%$phone%'";
		$where .= empty($servicename) ? "" : " AND sv.servicetype_id = '$servicename'";
		$where .= empty($status) ? "" : " AND sv.status = '$status'";
		/*
		if($username != "admin"){
			$where .= " AND sv.seat = '$username'";
		}
		*/

		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";

		if($web_type == "agent"){
			if($username != "admin"){
				$where .= " AND (sv.seat = '$username' OR sv.recipientseat = '$username' )";
			}
		}else{
			if($username != "admin"){
				$where .= " AND (sv.seat in ($deptUser) OR sv.recipientseat = '$username' )";
			}
		}

		$servicerecords = new Model("servicerecords");
		import('ORG.Util.Page');
		$count = $servicerecords->table("servicerecords sv")->field("c.name,sv.phone,sv.id,sv.seat,sv.createtime,sv.customer_id,sv.recording,sv.status,st.servicename")->join("customer c  on sv.customer_id = c.id")->join("servicetype st on sv.servicetype_id = st.id")->where($where)->count();

		$para_sys = readS();
        $_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$serverList = $servicerecords->order("sv.createtime desc")->table("servicerecords sv")->field("c.name,sv.phone,sv.id,sv.seat,sv.createtime,sv.customer_id,sv.recording,sv.status,st.servicename")->join("customer c  on sv.customer_id = c.id")->join("servicetype st on sv.servicetype_id = st.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();


		$i = 0;
		foreach($serverList as &$val){
			$val["phone_mess"] = $val["phone"];
			if($username != "admin"){
				if($para_sys["hide_phone"] =="yes"){
					$val["phone"] = substr($val["phone"],0,3)."***".substr($val["phone"],-4);
				}
			}

			$val["recording"] = trim($val["recording"]);
			$arrTmp = explode('.',$val["recording"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["recording"].".WAV";
			if(file_exists($WAVfile) ){
				$val["operations"] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["recording"])."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .trim($val["recording"]) ."&src=" .trim($val["seat"]) ."&dst=" .trim($val["phone"]) ."'> 下载 </a>" ;
			}else{
				$val["operations"] = "";
			}

			$id = $serverList[$i]["customer_id"];
			$svid = $serverList[$i]["id"];
			$serverList[$i]["name"] = "<a href='javascript:void(0);' onclick=\"service_editCustomer($id,"."'".$val["name"]."'".");\">". $val["name"] ."</a>";
			$serverList[$i]["operating"] = "<a href='javascript:void(0);'  onclick=\"openProcess($id,$svid,"."'".$val["servicename"]."'".");\">". 转交历史 ."</a>";
			$i++;
		}

		//echo $servicerecords->getLastSql();
		//dump($serverList);die;

		$rowsList = count($serverList) ? $serverList : false;
		$aryService["total"] = $count;
		$aryService["rows"] = $rowsList;

		echo json_encode($aryService);
	}

	function forwardedList(){
		checkLogin();
		$id = $_GET["id"];
		$process = new Model("process");
		import('ORG.Util.Page');
		$count = $process->where("customerid = $id")->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$processData = $process->order("p.createtime desc")->field("c.name,p.createtime,p.recipient,p.forwardedname,p.noticetype,p.modifycontent")->table("`process` p")->join("customer c on p.customerid = c.id")->join("servicerecords s on p.service_id = s.id ")->limit($page->firstRow.','.$page->listRows)->where("customerid = $id")->select();

		$rowsList = count($processData) ? $processData : false;
		$arrProcess["total"] = $count;
		$arrProcess["rows"] = $rowsList;
		$strTmp = json_encode($arrProcess);
		$this->assign("strTmp",$strTmp);

		$this->display();
	}


	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }


}

?>
