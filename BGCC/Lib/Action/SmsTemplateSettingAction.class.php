<?php
class SmsTemplateSettingAction extends Action{
	function smsList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "System SMS Template";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function agentSmsList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "System SMS Template";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function smsData(){
		$username = $_SESSION['user_info']['username'];
		$smst = new Model("smstemplate");
		if($username == "admin"){
			$count = $smst->count();
		}else{
			$count = $smst->where("createname = '$username'")->count();
		}

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == "admin"){
			$smsData = $smst->field("id,smstype as stype,smstype,templatename,smscontent,timing,autotime")->select();
		}else{
			$smsData = $smst->field("id,smstype as stype,smstype,templatename,smscontent,timing,autotime")->where("createname = '$username'  OR smstype = 'general' ")->select();
		}

		$status_row = array('system'=>'系统模板','general'=>'常用模板','personal'=>'私人模板','holiday'=>'节日模板','private'=>'私人模板');
		foreach($smsData as &$val){
			$status = $status_row[$val['stype']];
			$val['stype'] = $status;
		}

		$rowsList = count($smsData) ? $smsData : false;
		$arrSms["total"] = $count;
		$arrSms["rows"] = $rowsList;

		echo json_encode($arrSms);
	}


	function insertSmsSetting(){
		$username = $_SESSION['user_info']['username'];
		$cn_name = $_SESSION['user_info']['cn_name'];
		if($_REQUEST['timing']){
			$timing = "Y";
			$autotime = $_REQUEST['autotime'];
			$smstmp_status = "timing";
		}else{
			$timing = "N";
			$autotime = "NULL";
		}
		$sms = new Model("smstemplate");
		$arrData = Array(
			'smstype'=>$_POST['smstype'],
			'templatename'=>$_POST['templatename'],
			'smscontent'=>$_POST['smscontent'],
			'timing'=>$timing,
			'autotime'=>$autotime,
			'smstmp_status'=>$smstmp_status,
			'createname'=>$username,
			'cn_name'=>$cn_name,
		);
		//dump($arrData);die;
		$result = $sms->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'短信模板添加成功！'));
		} else {
			echo json_encode(array('msg'=>'短信模板添加失败！'));
		}
	}


	function updateTemplate(){
		if($_REQUEST['timing']){
			$timing = "Y";
			//$autotime = $_REQUEST['autotime'];
		}else{
			$timing = "Y";
		}
		$id = $_REQUEST['id'];
		$sms = new Model("smstemplate");
		$arrData = Array(
			'templatename'=>$_POST['templatename'],
			'smstype'=>$_POST['smstype'],
			'smscontent'=>$_POST['smscontent'],
			'timing'=>$timing,
			'autotime'=>$_REQUEST['autotime'],
		);
		$result = $sms->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteSmsTemplate(){
		$id = $_REQUEST["id"];
		$sms = new Model("smstemplate");
		$result = $sms->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

}

?>
