<?php
class RebootShutdownAction extends Action{
	//重启/关机
	function listRebootShutdown(){
		checkLogin();
		$action = $_POST['action'];

		if( $action ){
			//$retmsg = $ret_no = NULL;
			if( $action == "reboot" ){
				exec("sudo -u root /sbin/shutdown -r now", $retmsg, $ret_no);
				 //$this->assign('message',$retmsg);
			}
			if( $action == "shutdown" ){
				exec("sudo -u root /sbin/shutdown -h now", $retmsg, $ret_no);
				 //$this->assign('message',$retmsg);
			}
		}

		//分配增删改的权限
		$menuname = "Reboot/Shutdown";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("edit",$priv['edit']);

		$this->display();
	}
}
?>
