<?php
class ManifestListAction extends Action{
	function shippinglist(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Manifest List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function ManifestData(){
		$username = $_SESSION["user_info"]["username"];
		$order_num = $_REQUEST['order_num'];
		$consignee = $_REQUEST['consignee'];
		$status = $_REQUEST['status'];

		$where = "1 ";
		$where .= empty($order_num)?"":" AND order_num like '%$order_num%'";
		$where .= empty($consignee)?"":" AND consignee like '%$consignee%'";
		if($status){
			if($status == "4"){
				$where .= " AND order_status = '$status'";
			}else{
				$where .= " AND shipping_status = '$status'";
			}
		}

		$r_id = $_SESSION["user_info"]["r_id"];
		$role = new Model("role");
		$rData = $role->where("r_id = '$r_id'")->find();
		$menuname = "Order Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$order_info = new Model("order_info");
		if(  $username == "admin" || $priv['members_bomb'] == "Y"  ){
			$count = $order_info->where("$where AND delivery_order != 'not' AND delivery_order is NOT NULL AND enbaled ='Y'")->count();
		}else{
			$count = $order_info->where("$where AND createname ='$username' AND delivery_order != 'not' AND delivery_order is NOT NULL AND enbaled ='Y'")->count();
		}
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if(  $username == "admin" || $priv['members_bomb'] == "Y"  ){
			$arrOrderData = $order_info->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where("$where AND delivery_order != 'not' AND delivery_order is NOT NULL AND enbaled ='Y'")->select();
		}else{
			$arrOrderData = $order_info->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where("$where AND createname ='$username' AND delivery_order != 'not' AND delivery_order is NOT NULL AND enbaled ='Y'")->select();
		}

		$i = 0;
		$row_order_status = array('0'=>'未确认','1'=>'已确认','2'=>'取消','3'=>'无效','4'=>'退货','5'=>'已分单','6'=>'部分分单');
		$row_pay_status = array('N'=>'未付款','M'=>'付款中','Y'=>'已付款');
		$row_shipping_status = array('0'=>'退货','1'=>'已发货','2'=>'收货确认','3'=>'配货中','4'=>'已发货(部分商品)','5'=>'发货中');
		foreach($arrOrderData as &$val){
			$val["phone_source"] = $val["phone"];
			if($username != "admin"){
				if($para_sys["hide_phone"] =="yes"){
					$val["phone"] = substr($val["phone"],0,3)."***".substr($val["phone"],-4);
				}
			}
			$order_status = $row_order_status[$val['order_status']];
			$val['order_status'] = $order_status;

			$pay_status = $row_pay_status[$val['pay_status']];
			$val['pay_status'] = $pay_status;

			$shipping_status = $row_shipping_status[$val['shipping_status']];
			$val['shipping_status'] = $shipping_status;
			$i++;
		}
		unset($i);

		//dump($arrOrderData);die;
		//echo $order_info->getLastSql();die;
		$rowsList = count($arrOrderData) ? $arrOrderData : false;
		$arrOrder["total"] = $count;
		$arrOrder["rows"] = $rowsList;

		echo json_encode($arrOrder);
	}

	function viewManifest(){
		checkLogin();
		$id = $_REQUEST['id'];
		$order_info = new Model("order_info");
		$orlist = $order_info->where("id = '$id'")->find();
		$this->assign('orlist',$orlist);
		$this->assign('id',$id);

		$this->display();
	}

	function deleteShipping(){
		$id = $_REQUEST['id'];
		$order_info = new Model("order_info");
		$arrData = array(
			"enbaled"=>"N",
		);
		$result = $order_info->data($arrData)->where("id = '$id'")->save();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}
	}
}

?>
