<?php

class NetworkAction extends Action {
    public function index(){
		checkLogin();
		import('ORG.Net.NetworkSet');

		$vNet = new NetworkSet();
		$arrNetwork = $vNet->obtener_configuracion_red();
        $arrEths = $vNet->obtener_interfases_red_fisicas();

		$this->assign("arrNetwork", $arrNetwork);
		$this->assign("arrEths", $arrEths);

		//分配增删改的权限
		$menuname = "Network";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


        $this->display();
    }

	function networkData(){
		import('ORG.Net.NetworkSet');
		$vNet = new NetworkSet();
        $arrEths = $vNet->obtener_interfases_red_fisicas();
		foreach($arrEths AS $eth=>$value){
			$arrEths[$eth]['ip'] =  $arrEths[$eth]['Inet Addr'];
			$arrEths[$eth]['id'] =  $eth;
		}
		$arrE = array_values($arrEths);
		$rowsList = count($arrE) ? $arrE : false;
		$arrNetwork["total"] = count($arrEths);
		$arrNetwork["rows"] = $rowsList;
		//dump($arrEths);die;
		echo json_encode($arrNetwork);
	}

	public function dnsEdit(){
		checkLogin();
		import('ORG.Net.NetworkSet');
		$vNet = new NetworkSet();
		$arrNetwork = $vNet->obtener_configuracion_red();
        $arrEths = $vNet->obtener_interfases_red_fisicas();
		$this->assign("arrNetwork", $arrNetwork);
		$this->display();
	}

	public function dnsSave(){

		import('ORG.Net.NetworkSet');
		$vNet = new NetworkSet();
		 $arrNetConf['host'] = $_POST['hostname'];
		$arrNetConf['dns_ip_1'] = $_POST['pdns'];
		$arrNetConf['dns_ip_2'] = $_POST['sdns'];
		$arrNetConf['gateway_ip'] = $_POST['gateway'];

		$vNet->escribir_configuracion_red_sistema($arrNetConf);
		header("Content-Type:text/html; charset=utf-8");
		if(!empty($vNet->errMsg)) {
			//$smarty->assign("mb_message", $pNet->errMsg);
			goBack($vNet->errMsg,"");
		} else {
			//header("Location: index.php?menu=network");
			goBack(gb2utf("已保存"),"index.php?m=Network&a=dnsEdit");
		}

	}

	public function ethEdit(){
		checkLogin();
		import('ORG.Net.NetworkSet');

		$id = $_GET["id"];
		$vNet = new NetworkSet();
		$arrNetwork = $vNet->obtener_configuracion_red();
        $arrEths = $vNet->obtener_interfases_red_fisicas();
		$arrEth = $arrEths[$id];
		$this->assign("dev_id", $id);
		$this->assign("arrEth", $arrEth);
		$this->display();
	}

	public function ethSave(){

		import('ORG.Net.NetworkSet');
		$vNet = new NetworkSet();
		if($vNet->escribirConfiguracionInterfaseRed($_POST['dev_id'], $_POST['type'], $_POST['ip'], $_POST['mask'])) {
				goBack(gb2utf("已保存"),"index.php?m=Network&a=ethEdit&id=".$_POST['dev_id']);
            } else {
                goBack($vNet->errMsg,"");
            }


	}
}
?>
