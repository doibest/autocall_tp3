<?php
class HardwareAction extends Action{
	//硬件列表
	function listHardware(){
		checkLogin();
		$htmlHW = $this->lsdahdi();
		$this->assign("htmlHW",$htmlHW);
		$this->display();
	}


	//检测硬件
	function detectHardware(){
		set_time_limit(80);
		$chk_dahdi_replace = "";
		$chk_there_is_sangoma = "";
		if($_REQUEST['chk_dahdi_replace'] == 'true'){
			$chk_dahdi_replace = "-o";
		}
		if($_REQUEST['chk_there_is_sangoma'] == 'true'){
			$chk_there_is_sangoma = "-t";
		}
		$respuesta = $retorno = NULL;
		//echo "/usr/sbin/hardware_detector $chk_there_is_sangoma $chk_dahdi_replace", $respuesta, $retorno;die;
        exec("sudo /usr/sbin/hardware_detector $chk_there_is_sangoma $chk_dahdi_replace", $respuesta, $retorno);
		$htmlHW = $this->lsdahdi();
		echo $htmlHW;

	}

	function lsdahdi(){
		$tarjetas = array();
        $exist_data = "no";
		$errMsg = "";
		$respuesta = $retorno = NULL;
        exec('/usr/sbin/lsdahdi',$respuesta,$retorno);
		/*
		$respuesta = array(
  0 => '### Span  1: WCT1/0 "Digium Wildcard TE110P T1/E1 Card 0" (MASTER) HDB3/CCS/CRC4',
  1 => '  1 PRI        Clear       (In use) (EC: OSLEC)',
  2 => '  2 PRI        Clear       (In use) (EC: OSLEC)',
  3 => '  3 PRI        Clear       (In use) (EC: OSLEC)',
  4 => '  4 PRI        Clear       (In use) (EC: OSLEC)',
  5 => '  5 PRI        Clear       (In use) (EC: OSLEC)',
  6 => '  6 PRI        Clear       (In use) (EC: OSLEC)',
  7 => '  7 PRI        Clear       (In use) (EC: OSLEC)',
  8 => '  8 PRI        Clear       (In use) (EC: OSLEC)',
  9 => '  9 PRI        Clear       (In use) (EC: OSLEC)',
  10 => ' 10 PRI        Clear       (In use) (EC: OSLEC)',
  11 => ' 11 PRI        Clear       (In use) (EC: OSLEC)',
  12 => ' 12 PRI        Clear       (In use) (EC: OSLEC)',
  13 => ' 13 PRI        Clear       (In use) (EC: OSLEC)',
  14 => ' 14 PRI        Clear       (In use) (EC: OSLEC)',
  15 => ' 15 PRI        Clear       (In use) (EC: OSLEC)',
  16 => ' 16 PRI        HDLCFCS     (In use)',
  17 => ' 17 PRI        Clear       (In use) (EC: OSLEC)',
  18 => ' 18 PRI        Clear       (In use) (EC: OSLEC)',
  19 => ' 19 PRI        Clear       (In use) (EC: OSLEC)',
  20 => ' 20 PRI        Clear       (In use) (EC: OSLEC)',
  21 => ' 21 PRI        Clear       (In use) (EC: OSLEC)',
  22 => ' 22 PRI        Clear       (In use) (EC: OSLEC)',
  23 => ' 23 PRI        Clear       (In use) (EC: OSLEC)',
  24 => ' 24 PRI        Clear       (In use) (EC: OSLEC)',
  25 => ' 25 PRI        Clear       (In use) (EC: OSLEC)',
  26 => ' 26 PRI        Clear       (In use) (EC: OSLEC)',
  27 => ' 27 PRI        Clear       (In use) (EC: OSLEC)',
  28 => ' 28 PRI        Clear       (In use) (EC: OSLEC)',
  29 => ' 29 PRI        Clear       (In use) (EC: OSLEC)',
  30 => ' 30 PRI        Clear       (In use) (EC: OSLEC)',
  31 => ' 31 PRI        Clear       (In use) (EC: OSLEC)',
);


$respuesta = array (
  0 => '### Span  1: TE4/0/1 "T4XXP (PCI) Card 0 Span 1" (MASTER) HDB3/CCS ClockSource',
  1 => '  1 PRI        Clear       (In use)',
  2 => '  2 PRI        Clear       (In use)',
  3 => '  3 PRI        Clear       (In use)',
  4 => '  4 PRI        Clear       (In use)',
  5 => '  5 PRI        Clear       (In use)',
  6 => '  6 PRI        Clear       (In use)',
  7 => '  7 PRI        Clear       (In use)',
  8 => '  8 PRI        Clear       (In use)',
  9 => '  9 PRI        Clear       (In use)',
  10 => ' 10 PRI        Clear       (In use)',
  11 => ' 11 PRI        Clear       (In use)',
  12 => ' 12 PRI        Clear       (In use)',
  13 => ' 13 PRI        Clear       (In use)',
  14 => ' 14 PRI        Clear       (In use)',
  15 => ' 15 PRI        Clear       (In use)',
  16 => ' 16 PRI        HDLCFCS     (In use)',
  17 => ' 17 PRI        Clear       (In use)',
  18 => ' 18 PRI        Clear       (In use)',
  19 => ' 19 PRI        Clear       (In use)',
  20 => ' 20 PRI        Clear       (In use)',
  21 => ' 21 PRI        Clear       (In use)',
  22 => ' 22 PRI        Clear       (In use)',
  23 => ' 23 PRI        Clear       (In use)',
  24 => ' 24 PRI        Clear       (In use)',
  25 => ' 25 PRI        Clear       (In use)',
  26 => ' 26 PRI        Clear       (In use)',
  27 => ' 27 PRI        Clear       (In use)',
  28 => ' 28 PRI        Clear       (In use)',
  29 => ' 29 PRI        Clear       (In use)',
  30 => ' 30 PRI        Clear       (In use)',
  31 => ' 31 PRI        Clear       (In use)',
  32 => '### Span  2: TE4/0/2 "T4XXP (PCI) Card 0 Span 2" HDB3/CCS RED',
  33 => ' 32 PRI        Clear       (In use)  RED',
  34 => ' 33 PRI        Clear       (In use)  RED',
  35 => ' 34 PRI        Clear       (In use)  RED',
  36 => ' 35 PRI        Clear       (In use)  RED',
  37 => ' 36 PRI        Clear       (In use)  RED',
  38 => ' 37 PRI        Clear       (In use)  RED',
  39 => ' 38 PRI        Clear       (In use)  RED',
  40 => ' 39 PRI        Clear       (In use)  RED',
  41 => ' 40 PRI        Clear       (In use)  RED',
  42 => ' 41 PRI        Clear       (In use)  RED',
  43 => ' 42 PRI        Clear       (In use)  RED',
  44 => ' 43 PRI        Clear       (In use)  RED',
  45 => ' 44 PRI        Clear       (In use)  RED',
  46 => ' 45 PRI        Clear       (In use)  RED',
  47 => ' 46 PRI        Clear       (In use)  RED',
  48 => ' 47 PRI        HDLCFCS     (In use)  RED',
  49 => ' 48 PRI        Clear       (In use)  RED',
  50 => ' 49 PRI        Clear       (In use)  RED',
  51 => ' 50 PRI        Clear       (In use)  RED',
  52 => ' 51 PRI        Clear       (In use)  RED',
  53 => ' 52 PRI        Clear       (In use)  RED',
  54 => ' 53 PRI        Clear       (In use)  RED',
  55 => ' 54 PRI        Clear       (In use)  RED',
  56 => ' 55 PRI        Clear       (In use)  RED',
  57 => ' 56 PRI        Clear       (In use)  RED',
  58 => ' 57 PRI        Clear       (In use)  RED',
  59 => ' 58 PRI        Clear       (In use)  RED',
  60 => ' 59 PRI        Clear       (In use)  RED',
  61 => ' 60 PRI        Clear       (In use)  RED',
  62 => ' 61 PRI        Clear       (In use)  RED',
  63 => ' 62 PRI        Clear       (In use)  RED',
  64 => '### Span  3: TE4/0/3 "T4XXP (PCI) Card 0 Span 3" HDB3/CCS RED',
  65 => ' 63 PRI        Clear       (In use)  RED',
  66 => ' 64 PRI        Clear       (In use)  RED',
  67 => ' 65 PRI        Clear       (In use)  RED',
  68 => ' 66 PRI        Clear       (In use)  RED',
  69 => ' 67 PRI        Clear       (In use)  RED',
  70 => ' 68 PRI        Clear       (In use)  RED',
  71 => ' 69 PRI        Clear       (In use)  RED',
  72 => ' 70 PRI        Clear       (In use)  RED',
  73 => ' 71 PRI        Clear       (In use)  RED',
  74 => ' 72 PRI        Clear       (In use)  RED',
  75 => ' 73 PRI        Clear       (In use)  RED',
  76 => ' 74 PRI        Clear       (In use)  RED',
  77 => ' 75 PRI        Clear       (In use)  RED',
  78 => ' 76 PRI        Clear       (In use)  RED',
  79 => ' 77 PRI        Clear       (In use)  RED',
  80 => ' 78 PRI        HDLCFCS     (In use)  RED',
  81 => ' 79 PRI        Clear       (In use)  RED',
  82 => ' 80 PRI        Clear       (In use)  RED',
  83 => ' 81 PRI        Clear       (In use)  RED',
  84 => ' 82 PRI        Clear       (In use)  RED',
  85 => ' 83 PRI        Clear       (In use)  RED',
  86 => ' 84 PRI        Clear       (In use)  RED',
  87 => ' 85 PRI        Clear       (In use)  RED',
  88 => ' 86 PRI        Clear       (In use)  RED',
  89 => ' 87 PRI        Clear       (In use)  RED',
  90 => ' 88 PRI        Clear       (In use)  RED',
  91 => ' 89 PRI        Clear       (In use)  RED',
  92 => ' 90 PRI        Clear       (In use)  RED',
  93 => ' 91 PRI        Clear       (In use)  RED',
  94 => ' 92 PRI        Clear       (In use)  RED',
  95 => ' 93 PRI        Clear       (In use)  RED',
  96 => '### Span  4: TE4/0/4 "T4XXP (PCI) Card 0 Span 4" HDB3/CCS RED',
  97 => ' 94 PRI        Clear       (In use)  RED',
  98 => ' 95 PRI        Clear       (In use)  RED',
  99 => ' 96 PRI        Clear       (In use)  RED',
  100 => ' 97 PRI        Clear       (In use)  RED',
  101 => ' 98 PRI        Clear       (In use)  RED',
  102 => ' 99 PRI        Clear       (In use)  RED',
  103 => '100 PRI        Clear       (In use)  RED',
  104 => '101 PRI        Clear       (In use)  RED',
  105 => '102 PRI        Clear       (In use)  RED',
  106 => '103 PRI        Clear       (In use)  RED',
  107 => '104 PRI        Clear       (In use)  RED',
  108 => '105 PRI        Clear       (In use)  RED',
  109 => '106 PRI        Clear       (In use)  RED',
  110 => '107 PRI        Clear       (In use)  RED',
  111 => '108 PRI        Clear       (In use)  RED',
  112 => '109 PRI        HDLCFCS     (In use)  RED',
  113 => '110 PRI        Clear       (In use)  RED',
  114 => '111 PRI        Clear       (In use)  RED',
  115 => '112 PRI        Clear       (In use)  RED',
  116 => '113 PRI        Clear       (In use)  RED',
  117 => '114 PRI        Clear       (In use)  RED',
  118 => '115 PRI        Clear       (In use)  RED',
  119 => '116 PRI        Clear       (In use)  RED',
  120 => '117 PRI        Clear       (In use)  RED',
  121 => '118 PRI        Clear       (In use)  RED',
  122 => '119 PRI        Clear       (In use)  RED',
  123 => '120 PRI        Clear       (In use)  RED',
  124 => '121 PRI        Clear       (In use)  RED',
  125 => '122 PRI        Clear       (In use)  RED',
  126 => '123 PRI        Clear       (In use)  RED',
  127 => '124 PRI        Clear       (In use)  RED',
);
*/
        if($retorno==0 && count($respuesta) > 1){
			$card_count = 0;
            foreach($respuesta as $key => $linea){
                $estado_asterisk       = 'Unknown';
                $estado_asterisk_color = "gray";
                $estado_dahdi_image    = "conn_unkown_icon";
                if(preg_match("/^### Span[[:space:]]+([[:digit:]]{1,}): ([[:alnum:]| |-|\/]+)(.*)$/",$linea,$regs)){
					//var_dump($regs);die;
					$card_count ++;
					$tarjetas[$card_count-1] = Array();
					$tarjetas[$card_count-1]['cardno'] = $regs[1];
					$tarjetas[$card_count-1]['cardstring'] = $regs[0];
					$tarjetas[$card_count-1]['port'] = Array();

                }else if(preg_match("/[[:space:]]*([[:digit:]]+) ([[:alnum:]_]+)[[:space:]]+([[:alnum:]-]+)[[:space:]]*(.*)/",$linea,$regs1)){
                    //dump($regs1);die;
                   if(preg_match("/Hardware-assisted/i",$regs1[3].$regs1[4]) || preg_match("/HDLC(FCS)?/i",$regs1[3])){
                        if(preg_match("/In use.*RED/i",$regs1[4])){
                            $estado_asterisk       = 'Detected by Asterisk';
                            $estado_asterisk_color = "green";
                            $estado_dahdi_image    = "conn_alarm_HC_icon";
                        }
                        else if(preg_match("/In use/i",$regs1[4])){
                            $estado_asterisk       = 'Detected by Asterisk';
                            $estado_asterisk_color = "green";
                            $estado_dahdi_image    = "conn_ok_HC_icon";
                        }
                        else if(preg_match("/RED/i",$regs1[4])){
                            $estado_asterisk       = 'Not detected by Asterisk';
                            $estado_asterisk_color = "#FF7D7D";
                            $estado_dahdi_image    = "conn_alarm_HC_icon";
                        }
                        else{
                            $estado_asterisk       = 'Not detected by Asterisk';
                            $estado_asterisk_color = "#FF7D7D";
                            $estado_dahdi_image    = "conn_ok_HC_icon";
                        }
                   }else if(preg_match("/In use.*RED/i",$regs1[4])){
                        $estado_asterisk       = 'Detected by Asterisk';
                        $estado_asterisk_color = "green";
                        $estado_dahdi_image    = "conn_alarm_icon";
                   }else if(preg_match("/In use/i",$regs1[4])){
                        $estado_asterisk       = 'Detected by Asterisk';
                        $estado_asterisk_color = "green";
                        $estado_dahdi_image    = "conn_ok_icon";
                   }else if(preg_match("/RED/i",$regs1[4])){
                        $estado_asterisk       = 'Not detected by Asterisk';
                        $estado_asterisk_color = "#FF7D7D";
                        $estado_dahdi_image    = "conn_alarm_icon";
                   }else{
                        $estado_asterisk       = 'Not detected by Asterisk';
                        $estado_asterisk_color = "#FF7D7D";
                        $estado_dahdi_image    = "conn_ok_icon";
                   }
				$tarjetas[$card_count-1]['port'][$regs1[1]] = $estado_dahdi_image; //卡的编号的颜色

                }
            }
        }

        if(count($tarjetas)<=0){ //si no hay tarjetas instaladas
            $errMsg = L("No cards were detected on your system. Please press the \"Detect New Hardware\" button to detect new hardware.");
            $tarjetas = array();
        }
        if(count($tarjetas)==1){
            if(preg_match("/^DAHDI_DUMMY\/1/i", $valor))
            {
                $errMsg = "Cards undetected on your system, press for detecting hardware detection.";
                $tarjetas = array();
            }
        }
		//dump($tarjetas);die;
		$htmlHW = "";
		if($tarjetas){
			foreach($tarjetas AS $v){
				$htmlHW .= "<table class='card'>";
				$htmlHW .= "<caption>";
				$htmlHW .= $v['cardstring'];
				//下面是表格内容
				$i = 0;
				foreach($v['port'] AS $key=>$val){
					//每行显示16个
					if($i == 0)$htmlHW .= "<tr>";
					$htmlHW .= "<td class='port'><span class='" .$val ."'>" .$key ."</span></td>";
					$i ++;
					if($i == 16){$htmlHW .= "</tr>";$i=0;}
				}
				if($i!=0 && $i<16)$htmlHW .= "</tr>";

				$htmlHW .= "</caption>";
				$htmlHW .= "</table>";
			}

		}else{
			//$htmlHW = "<span class='error'>" .$errMsg ."</span>";
			$htmlHW = "<div class='error'>" .$errMsg ."</div>";
		}
		return $htmlHW;

	}






}
?>

