<?php
class DepartmentAction extends Action{
		//显示部门管理的主页面
    function listDepartmentParam(){
		checkLogin();
		$users=new Model("users");

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$dept_id = rtrim($deptst,",");
		$where = "1";
		if($username != "admin"){
			$where .= " AND d_id in ($dept_id)";
		}
        $arrUsers=$users->where($where)->select();
        $this->assign('ulist',$arrUsers);

		//分配增删改的权限
		$menuname = "Department";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		$this->display();

    }

	function departmentData(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$department=new Model("Department");
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		//$page = new Page($count,$page_rows);
		$page = new Page($count,"500");

		$dept = $department->field("d_id as id,d_name as text,d_pid,d_leader")->limit($page->firstRow.','.$page->listRows)->select();

		//所有 有子部门的 部门都是闭合状态的
		$i = 0;
		foreach($dept as $v){
			$dept[$i]['state'] = "closed";
			if($v["id"] == $d_id){
				$selfDept[] = $v;
			}
			$i++;
		}
		unset($i);

		if($username == "admin"){
			$arrTree = $this->getTree($dept,0);
		}else{
			$arrTreeTmp = $this->getTree($dept,$d_id);
			$selfDept[0]["children"] = $arrTreeTmp;
			$arrTree = $selfDept;
		}


		$j = 0;
		foreach($arrTree as $v){
			if( $arrTree[$j]["d_pid"] == "0" && !$arrTree[$j]["children"]){
				$arrTree[$j]['iconCls'] = "icon-files";
			}
			$j++;
		}
		unset($j);

		/*
		//打开时只有顶级部门中 有子部门的 是闭合状态，其他的都是是打开状态
		$i = 0;
		foreach($arrTree as $v){
			if( is_array($arrTree[$i]['children'])==true ){
				$arrTree[$i]['state'] = "closed";
			}
			$i++;
		}
		unset($i);
		*/
		//$strJSON = json_encode($arrTree);
		//dump($arrTree);die;

		$rowsList = count($arrTree) ? $arrTree : false;
		$count = $department->count();
		$arrDept["total"] = $count;
		$arrDept["rows"] = $rowsList;
		//dump($arrmail);die;
		echo json_encode($arrDept);
	}

	//执行 添加部门 的操作
    function insertDepartment(){
        $department=new Model("Department");
        $arrData = array(
			"d_name" => $_REQUEST["text"],
			"d_pid" => $_REQUEST["d_pid"],
			"d_leader" => $_REQUEST["d_leader"],
		);
		//dump($arrData);die;
		$result = $department->data($arrData)->add();
		if ($result){
			$this->usersCache();
			echo json_encode(array('success'=>true,'msg'=>'部门添加成功！'));
		} else {
			echo json_encode(array('msg'=>'部门添加失败！'));
		}
    }

    //对部门名称执行更新操作
	function updateDepartment(){
        $id = $_REQUEST['id'];
		$department=new Model("Department");
		$arrData = array(
			"d_name" => $_REQUEST["text"],
			"d_pid" => $_REQUEST["d_pid"],
			"d_leader" => $_REQUEST["d_leader"],
		);
		//dump($arrData);die;
		$result = $department->data($arrData)->where("d_id=$id")->save();
		//echo $department->getLastSql();die;
		if ($result !== false){
			$this->usersCache();
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

    //删除所选中的部门
    function deleteDepartment(){
		$id = $_REQUEST["id"];
		$department=new Model("Department");
		$result = $department->where("d_id in ($id)")->delete();
		if ($result){
			$this->usersCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
    }



	//外呼/销售工号、部门id，客服工号、部门id，工号对应的中文名
	function usersCache(){
		$users = new Model("users");
		$sellData = $users->order("username asc")->table("users u")->field("u.username,u.cn_name,u.r_id,u.d_id,r.agent_type")->join("role r on u.r_id = r.r_id")->where("r.agent_type = 'sell'")->select();
		foreach($sellData as $ksy=>$val){
			$sellUser[] = $val['username'];
			$sellDept[] = $val['d_id'];
			//$sell_cnUser[$val['username']] = $val['cn_name'];
		}

		$serviceData = $users->order("username asc")->table("users u")->field("u.username,u.r_id,u.d_id,r.agent_type")->join("role r on u.r_id = r.r_id")->where("r.agent_type = 'service'")->select();
		foreach($serviceData as $ksy=>$val){
			$serviceUser[] = $val['username'];
			$serviceDept[] = $val['d_id'];
			//$service_cnUser[$val['username']] = $val['cn_name'];
		}

		$uData = $users->order("username asc")->table("users u")->field("u.username,u.cn_name,u.extension,u.phone,u.d_id,d.d_name")->join("department d on u.d_id = d.d_id")->select();
		foreach($uData as $key=>$val){
			$exten_user[$val['username']] = $val["extension"];
			$cn_user[$val['username']] = $val["cn_name"];
			$deptId_user[$val['username']] = $val["d_id"];
			$deptName_user[$val['username']] = $val["d_name"];
			if($val["extension"]){
				$exten_cnName[$val["extension"]] = $val["cn_name"];
			}
			if($val["phone"]){
				$phone_cnName[$val["phone"]] = $val["cn_name"];
			}
		}

		$department = new Model("department");
		$deptData = $department->select();
		foreach($deptData as $val){
			$deptId[$val["d_id"]] = $val["d_name"];
		}

		$userArr = array(
						"sell_user"=>$sellUser,
						"service_user"=>$serviceUser,
						"sell_dept"=>array_unique($sellDept),
						"service_dept"=>array_unique($serviceDept),
						"cn_user"=>$cn_user,
						"deptId_user"=>$deptId_user,
						"deptName_user"=>$deptName_user,
						"deptId_name"=>$deptId,
						"exten_cnName"=>$exten_cnName,
						"phone_cnName"=>$phone_cnName,
						"exten_user"=>$exten_user,
						//"cn_user"=>array_merge($sell_cnUser,$service_cnUser),
					);
		//dump($userArr);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F('users',$userArr,"BGCC/Conf/crm/$db_name/");
		//F('users',$userArr,"BGCC/Conf/");
		//dump($userArr);die;
	}


	function getTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['d_pid'] == $pId)    {
				$v['children'] = $this->getTree($data, $v['id']);

				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				if ( empty($v["children"]) && $v['state'] =='closed'){
					$v['state'] =  'open';
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	function deptTree(){
		$department=new Model("Department");
		$dept = $department->field("d_id as id,d_name as text,d_pid")->select();
		$addDept = $_GET['addDept'];
		if( $addDept == "depts"){
			$alldept = array(
				"text"=>"顶级部门",
				"id"=>"0",
				"d_pid"=>"0",
			);
		}
		if( $addDept == "gg"){
			$alldept = array(
				"text"=>"所有部门",
				"id"=>"0",
				"d_pid"=>"0",
			);
		}
		//所有 有子部门的 部门都是闭合状态的
		$i = 0;
		foreach($dept as $v){
			$dept[$i]['state'] = "closed";
			$i++;
		}
		unset($i);
		$arrTree = $this->getTree($dept,0);
		/*
		//打开时只有顶级部门中 有子部门的 是闭合状态，其他的都是是打开状态
		$i = 0;
		foreach($arrTree as $v){
			if( is_array($arrTree[$i]['children'])==true ){
				$arrTree[$i]['state'] = "closed";
			}
			$i++;
		}
		unset($i);
		*/
		$j = 0;
		foreach($arrTree as $v){
			if( $arrTree[$j]["d_pid"] == "0" && !$arrTree[$j]["children"]){
				$arrTree[$j]['iconCls'] = "icon-files";
			}
			$j++;
		}
		unset($j);
		if( $addDept != "userDept" ){
			array_unshift($arrTree,$alldept);
		}
		//dump($arrTree);
		echo  json_encode($arrTree);
	}


	function getEditDeptTree(){
		$department=new Model("Department");
		$dept = $department->field("d_id as id,d_name as text,d_pid")->select();
		$editDept = $_GET['editDept'];
		if( $editDept == "depts"){
			$alldept = array(
				"text"=>"顶级部门",
				"id"=>"0",
				"d_pid"=>"0",
			);
		}
		if( $editDept == "gg"){
			$alldept = array(
				"text"=>"所有部门",
				"id"=>"0",
				"d_pid"=>"0",
			);
		}
		//$arrTree = $this->getTree2($dept,0);
		$arrTree = $this->getTree($dept,0);
		$j = 0;
		foreach($arrTree as $v){
			if( $arrTree[$j]["d_pid"] == "0" && !$arrTree[$j]["children"]){
				$arrTree[$j]['iconCls'] = "icon-files";
			}
			$j++;
		}
		unset($j);
		if( $editDept != "userDept" ){
			array_unshift($arrTree,$alldept);
		}
		echo  json_encode($arrTree);
	}


	//可以看到本部门及子部门
	function getDepartmentArr(){
		$ptlid = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];

		//dump($ptlid);die;
        import("ORG.Util.DepartmentTree");
        $department=new Model("Department");
		$pname = $department->field("d_name as text,d_id as id,d_pid")->where("d_id = $ptlid")->find();
        $Tree = new Tree();

		$rows = $department->field("d_name as text,d_id as id,d_pid")->order("d_orderid")->select();

		if($username == "admin"){
			$arrTree = $this->getTree2($rows,0);
		}else{
			$arrTree = $this->getTree2($rows,$ptlid);
		}

		$pname["children"] = $arrTree;
		$arrTreeData[0] =  $pname;
		//dump($arrTree);
		//dump($arrTreeData);
		if($username == "admin"){
			//echo json_encode($arrTree);
			$arrData = $arrTree;
		}else{
			//echo json_encode($arrTreeData);
			$arrData = $arrTreeData;
		}


		if($arrData){
			$arr = array("id"=>"","text"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","text"=>"请选择..."));
		}

		echo json_encode($arrData);
    }

	//可以看到本部门及子部门
	function getDepartmentArr2(){
		$ptlid = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];

		//dump($ptlid);die;
        import("ORG.Util.DepartmentTree");
        $department=new Model("Department");
		$pname = $department->field("d_name as text,d_id as id,d_pid")->where("d_id = $ptlid")->find();
        $Tree = new Tree();

		$rows = $department->field("d_name as text,d_id as id,d_pid")->order("d_orderid")->select();

		if($username == "admin"){
			$arrTree = $this->getTree2($rows,0);
		}else{
			$arrTree = $this->getTree2($rows,$ptlid);
		}
		$alldept = array(
			"text"=>"所有部门",
			"id"=>"-1",
			"d_pid"=>"0",
		);
		array_unshift($arrTree,$alldept);

		$pname["children"] = $arrTree;
		$arrTreeData[0] =  $pname;
		//dump($arrTree);
		//dump($arrTreeData);
		if($username == "admin"){
			echo json_encode($arrTree);
		}else{
			echo json_encode($arrTreeData);
		}
    }

	function getTree2($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['d_pid'] == $pId)    {
                     $v['children'] = $this->getTree2($data, $v['id']);

                      if ( empty($v["children"])  )  unset($v['children']) ;
                      $tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }


	function userData(){
		$users = new Model("users");
		$name = $_REQUEST['name'];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$deptSet = rtrim($deptst,",");

		if($username == "admin"){
			$where = "1 ";
		}else{
			$where = "d_id in ($deptSet) ";
		}
		$where .= empty($name)?"":" AND ( username like '%$name%' OR cn_name like '%$name%' )";

		$count = $users->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$userData = $users->field("username,extension,d_id,r_id,cn_name,en_name,email,fax,extension_type,extension_mac,phone,out_pin,routeid")->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		foreach($userData as &$val){
			$val["cn_name"] = $val["username"]."/".$val["cn_name"];
		}

		$rowsList = count($userData) ? $userData : false;
		$arrU["total"] = $count;
		$arrU["rows"] = $rowsList;

		echo json_encode($arrU);
	}

	function taskUsersData(){
		$task = new Model("sales_task");
		$name = $_REQUEST['name'];

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$dept_id = rtrim($deptst,",");

		$where = "1 ";
		if($username != 'admin'){
			$where .= " AND u.d_id IN ($dept_id)";
		}
		$where .= empty($name)?"":" AND ( s.createuser like '%$name%' OR u.cn_name like '%$name%' )";

		$count = $task->table("sales_task s")->field("s.createuser,s.id,u.cn_name")->join("users u on s.createuser = u.username")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$taskUData = $task->order("s.createuser asc")->table("sales_task s")->field("s.createuser,s.id,u.cn_name")->join("users u on s.createuser = u.username")->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		$i = 0;
		foreach($taskUData as $val){
			$taskUData[$i]['name'] = $val['createuser']."/".$val['cn_name'];
			$i++;
		}



		//echo $task->getLastSql();
		//dump($taskUData);die;

		$rowsList = count($taskUData) ? $taskUData : false;
		$arrU["total"] = $count;
		$arrU["rows"] = $rowsList;

		echo json_encode($arrU);

	}


	//可以看到本部门及子部门的员工
	function getDeptUserArr(){
		$username = $_SESSION['user_info']['username'];
		$ptlid = $_SESSION["user_info"]["d_id"];
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();
		$pname = $department->field("d_name as text,d_id as id,d_pid")->where("d_id = $ptlid")->find();

		$users = new Model("users");
		$userList = $users->order("username asc")->field("username as text,d_id as pid")->select();
		//dump($userList);

		$j = 0;
		foreach($dept as $vm){
			//$types[$j]['iconCls'] = "icon-file";
			$dept[$j]['state'] = "closed";
			$j++;
		}


		foreach($userList as &$val){
			$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}
        //$arrTree = $this->getTree($dept,0);
		if($username == "admin"){
			$arrTree = $this->getDeptTree($dept,0);
		}else{
			$arrTree = $this->getDeptTree($dept,$ptlid);
		}
		$pname["children"] = $arrTree;

		$arrTreeData[0] =  $pname;

		//dump($arrTreeData);die;
		$strJSON = json_encode($arrTreeData);
		echo ($strJSON);
    }




	//可以看到本部门及子部门的员工  和员工姓名  id为工号
	function getDeptUserArr2(){
		$username = $_SESSION['user_info']['username'];
		$ptlid = $_SESSION["user_info"]["d_id"];
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();
		$pname = $department->field("d_name as text,d_id as id,d_pid")->where("d_id = $ptlid")->find();

		$users = new Model("users");
		$userList = $users->order("username asc")->field("username as text,username as id,d_id as pid,cn_name")->select();
		//dump($userList);

		foreach($userList as &$val){
			$val['text'] = $val['text']."/".$val["cn_name"];
		}

		$j = 0;
		foreach($dept as $vm){
			//$types[$j]['iconCls'] = "icon-file";
			$dept[$j]['state'] = "closed";
			$j++;
		}
		//echo var_export($dept);echo "=====</br>";echo var_export($userList);echo "====</br>";

		foreach($userList as &$val){
			//$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}
        //$arrTree = $this->getTree($dept,0);
		if($username == "admin"){
			$arrTree = $this->getDeptTree($dept,0);
		}else{
			$arrTree = $this->getDeptTree($dept,$ptlid);
		}
		$pname["children"] = $arrTree;

		$arrTreeData[0] =  $pname;

		//dump($arrTreeData);die;
		if($username == "admin"){
			$strJSON = json_encode($arrTree);
		}else{
			$strJSON = json_encode($arrTreeData);
		}
		echo ($strJSON);
    }



	//可以看到本部门及子部门的员工
	function getDeptAllUserArr(){
		$username = $_SESSION['user_info']['username'];
		$ptlid = $_SESSION["user_info"]["d_id"];
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();

		$users = new Model("users");
		$userList = $users->field("username as text,d_id as pid")->select();
		//dump($userList);

		$j = 0;
		foreach($dept as $vm){
			//$types[$j]['iconCls'] = "icon-file";
			$dept[$j]['state'] = "closed";
			$j++;
		}


		foreach($userList as &$val){
			$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}
        //$arrTree = $this->getTree($dept,0);
		$arrTree = $this->getDeptTree($dept,0);
		$strJSON = json_encode($arrTree);
		echo ($strJSON);
    }


	//可以看到本部门及子部门的员工 和员工姓名
	function getDeptAllUserArr2(){
		$username = $_SESSION['user_info']['username'];
		$ptlid = $_SESSION["user_info"]["d_id"];
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();

		$users = new Model("users");
		$userList = $users->field("username as text,username as id,d_id as pid,cn_name")->select();
		//dump($userList);

		foreach($userList as &$val){
			$val['text'] = $val['text']."/".$val["cn_name"];
		}

		$j = 0;
		foreach($dept as $vm){
			//$types[$j]['iconCls'] = "icon-file";
			$dept[$j]['state'] = "closed";
			$j++;
		}


		foreach($userList as &$val){
			$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}
		if($username == "admin"){
			$arrTree = $this->getDeptTree($dept,0);
		}else{
			$arrTree = $this->getDeptTree($dept,$ptlid);
		}
		$pname["children"] = $arrTree;

		$arrTreeData[0] =  $pname;

        //$arrTree = $this->getTree($dept,0);
		//$arrTree = $this->getDeptTree($dept,0);
		$strJSON = json_encode($arrTreeData);
		echo ($strJSON);
    }


	function getDeptTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
					$v['children'] = $this->getDeptTree($data, $v['tid']);
					if ( empty($v["children"])  )  unset($v['children']) ;
					/*
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['children'] =  array(array());
					*/
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['state'] =  'open';
				 $tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
/*
	function departmentTree(){
		$department=new Model("Department");
		$dept = $department->field("d_id as id,d_name as text,d_pid")->select();
		$i = 0;
		foreach($dept as $v){
			$dept[$i]['state'] = "closed";
			$i++;
		}
		unset($i);
		$arrTree = $this->getTree($dept,0);
		echo  json_encode($arrTree);
	}


	function getTree2($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['d_pid'] == $pId)    {
				$v['children'] = $this->getTree($data, $v['id']);

				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }
*/
}
?>
