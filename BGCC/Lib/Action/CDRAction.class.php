<?php
class CDRAction extends Action{

	//普通cdr的下载链接
	// index.php?m=CDR&a=downloadCDR&uniqueid=1385522687.4
	function downloadCDR(){
		Header("Content-type: text/html; charset=utf-8");
		$uniqueid = $_GET['uniqueid'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		//wjj-add-2014
		$src = $_REQUEST['src']?$_REQUEST['src']:"";
		$dst = $_REQUEST['dst']?$_REQUEST['dst']:"";
		$Ymd = Date('Ymd',$timestamp);
		$His = Date('His',$timestamp);
		$mp3Filename = "$Ymd-$His-[$src]-[$dst].mp3";
		//wjj-add-2014--end
		$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
		$WAVfile = $dirPath ."/$uniqueid.WAV";
		//echo $WAVfile;die;
		if(!file_exists($WAVfile)){
			echo "录音文件不存在!!\n";die;
		}
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        //header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        //header("Content-Disposition: attachment;filename=" ."$uniqueid.mp3");
        header("Content-Disposition: attachment;filename=" .$mp3Filename);
        system("/usr/bin/sox $WAVfile -t mp3 -");
	}


	//预览式外呼CDR下载
	// index.php?m=CDR&a=downloadCDR&uniqueid=1385522687.4
	function download_previewCDR(){
		Header("Content-type: text/html; charset=utf-8");
		$uniqueid = $_GET['uniqueid'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		//wjj-add-2014
		$src = $_REQUEST['src']?$_REQUEST['src']:"";
		$dst = $_REQUEST['dst']?$_REQUEST['dst']:"";
		$Ymd = Date('Ymd',$timestamp);
		$His = Date('His',$timestamp);
		$mp3Filename = "$Ymd-$His-[$src]-[$dst].mp3";
		//wjj-add-2014--end
		$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
		$WAVfile = $dirPath ."/$uniqueid.WAV";
		if(!file_exists($WAVfile)){
			echo "录音文件不存在!!\n";die;
		}
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        //header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        //header("Content-Disposition: attachment;filename=" ."$uniqueid.mp3");
        header("Content-Disposition: attachment;filename=" .$mp3Filename);
        system("/usr/bin/sox $WAVfile -t mp3 -");
	}


	//自动外呼CDR下载
	// index.php?m=CDR&a=downloadCDR&uniqueid=1385522687.4
	function download_autocallCDR(){
		Header("Content-type: text/html; charset=utf-8");
		$uniqueid = $_GET['uniqueid'];
		if( !$uniqueid ){ echo "录音文件不存在!!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		//wjj-add-2014
		$src = $_REQUEST['src']?$_REQUEST['src']:"";
		$dst = $_REQUEST['dst']?$_REQUEST['dst']:"";
		$Ymd = Date('Ymd',$timestamp);
		$His = Date('His',$timestamp);
		$mp3Filename = "$Ymd-$His-[$src]-[$dst].mp3";
		//wjj-add-2014--end
		$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
		$WAVfile = $dirPath ."/$uniqueid.WAV";
		if(!file_exists($WAVfile)){
			echo "录音文件不存在!\n";die;
		}
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        //header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        //header("Content-Disposition: attachment;filename=" ."$uniqueid.mp3");
        header("Content-Disposition: attachment;filename=" .$mp3Filename);
        system("/usr/bin/sox $WAVfile -t mp3 -");
	}

	//播放录音
    function playRecordMeeting(){
		$uniqueid = $_REQUEST['uniqueid'];
		$month = $_REQUEST['month'];
		$day = $_REQUEST['day'];
		$meeting_num = $_REQUEST["meeting_num"];

		header('Content-Type: text/html; charset=utf-8');
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$AudioURL = "index.php?m=CDR&a=downloadMeeting&uniqueid=${uniqueid}&month=${month}&day=${day}&meeting_num=${meeting_num}";
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);

		$this->display();
    }




	//下载会议录音
	function downloadMeeting(){
		Header("Content-type: text/html; charset=utf-8");
		$uniqueid = $_REQUEST['uniqueid'];
		$month = $_REQUEST['month'];
		$day = $_REQUEST['day'];
		$meeting_num = $_REQUEST["meeting_num"];
		$path = "/var/spool/asterisk/meetme/".$month."/".$day."/".$meeting_num."/";
		$WAVfile = $path.$uniqueid.".WAV";
		//echo $WAVfile;die;
		if(!file_exists($WAVfile)){
			echo "录音文件不存在!!\n";die;
		}
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        //header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=" ."$uniqueid.mp3");
        system("/usr/bin/sox $WAVfile -t mp3 -");
	}


	//播放会议录音
    function playRecord(){
		$uniqueid = $_GET['uniqueid'];
		header('Content-Type: text/html; charset=utf-8');
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$AudioURL = "index.php?m=CDR&a=downloadCDR&uniqueid=${uniqueid}";
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);

		$this->display();
    }


	//播放系统录音
	function playSysRecord(){
		$file_path = $_GET['file_path'];
		header('Content-Type: text/html; charset=utf-8');
		if(!file_exists($file_path)){
			echo "录音文件不存在!!\n";die;
		}
		$AudioURL = "index.php?m=CDR&a=downloadSystem&file_path=${file_path}";
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);

		$this->display();
	}

	//下载系统录音
	function downloadSystem(){
		Header("Content-type: text/html; charset=utf-8");
		$WAVfile = $_REQUEST['file_path'];
		$filename = array_shift(explode(".",basename($WAVfile)));
		//echo $WAVfile;die;
		if(!file_exists($WAVfile)){
			echo "录音文件不存在!!\n";die;
		}
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        //header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=" ."$filename.mp3");
        system("/usr/bin/sox $WAVfile -t mp3 -");
	}

}
?>
