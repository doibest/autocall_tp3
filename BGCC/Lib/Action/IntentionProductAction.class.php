<?php
class IntentionProductAction extends Action{
	function test(){
		$this->display();
	}


	function testData(){
		set_time_limit(0);
		ini_set('memory_limit','512M');
		$customer = new Model("customer");
		$count = $customer->count();


		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){    //&& $val['text_type'] != '3'
				$arrF[] = $val['en_name'];
			}
		}
		$fields = "id,dealuser,".implode(",",$arrF);
		$arrData = $customer->field("`id`,`dealuser`,`name`,`phone1`,`createuser`,`company`,`sex`,`phone2`,`qq_number`,`email`,`zipcode`,`groups`,`grade`,`address`,`birthday`,`nickname`,`description`")->select();
		//echo $customer->getLastSql();

		dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//产品意向
	function intentionGoodsList(){
		checkLogin();
		$customer_id = $_REQUEST["customer_id"];
		$this->assign("customer_id",$customer_id);

		/*
		//400号码
		$Number400 = $_REQUEST["Number400"];
		$this->assign("Number400",$Number400);
		//意向产品
		$productA = $_REQUEST["productA"];
		$this->assign("productA",$productA);
		//产品id
		$goods_id = $_REQUEST["goods_id"];
		$this->assign("goods_id",$goods_id);
		//部门id
		$dept_id = $_REQUEST["dept_id"];
		$this->assign("dept_id",$dept_id);
		*/

		//呼入路由did号
		$did_num = $_REQUEST["did_num"];
		$this->assign("did_num",$did_num);

		$this->display();
	}

	function intentionGoodsData(){
		$customer_id = $_REQUEST["customer_id"];
		$username = $_SESSION['user_info']['username'];

		$order_customer_product = new Model("order_customer_product");
		$fields = "op.id,op.create_time,op.customer_id,op.create_user,op.modify_time,op.UseDepartment,op.ReceivingDepartment,op.Number400,op.productA,op.groups,op.grade,op.media_source,op.source_type,g.good_name,d.d_name";

		$where = "op.customer_id = '$customer_id' AND op.create_user = '$username'";

		$count = $order_customer_product->table("order_customer_product op")->field($fields)->join("shop_goods g on (op.productA = g.id)")->join("department d on (op.UseDepartment = d.d_id)")->where($where)->count();

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $order_customer_product->order("op.modify_time desc,op.create_time desc")->table("order_customer_product op")->field($fields)->join("shop_goods g on (op.productA = g.id)")->join("department d on (op.UseDepartment = d.d_id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$rows = getSelectCache();
		foreach($arrData as &$val){
			$groups = $rows["groups"][$val["groups"]];
			$val["groups"] = $groups;

			$grade = $rows["grade"][$val["grade"]];
			$val["grade"] = $grade;

			$source_type = $rows["source_type"][$val["source_type"]];
			$val["source_type"] = $source_type;

		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addIntention(){
		checkLogin();
		$customer_id = $_REQUEST["customer_id"];
		$this->assign("customer_id",$customer_id);

		//呼入路由did号
		$did_num = $_REQUEST["did_num"];
		$this->assign("did_num",$did_num);


		$product = new Model("cite_table");
		$arrDidInfo = $product->table("cite_table c")->field('c.linenumber,c.400number,c.career,g.good_name,d.d_name,g.id as goods_id,d.d_id')->join('shop_goods g on c.goods_grid = g.id')->join('department d on c.d_id = d.d_id')->where("linenumber='$did_num'")->find();

		$this->assign("arrDidInfo",$arrDidInfo);

		$this->display();
	}

	function insertGoodsIntention(){
		$username = $_SESSION['user_info']['username'];
		$order_customer_product = new Model("order_customer_product");
		$arrData = array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'customer_id'=>$_REQUEST['customer_id'],
			'Number400'=>$_REQUEST['Number400'],
			'ReceivingDepartment'=>$_REQUEST['ReceivingDepartment'],
			'UseDepartment'=>$_REQUEST['UseDepartment'],
			'productA'=>$_REQUEST['productA'],
			'groups'=>$_REQUEST['groups'],
			'grade'=>$_REQUEST['grade'],
			'source_type'=>$_REQUEST['source_type'],
			'media_source'=>$_REQUEST['media_source'],
		);
		//dump($arrData);die;
		$result = $order_customer_product->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function getFieldsValue(){
		//header("Content-Type:text/html; charset=utf-8");
		$field_name = $_REQUEST["field_name"];
		$arrF = getSelectCache();
		$arrT = $arrF[$field_name];
		foreach($arrT as $key=>$val){
			$arrData[] = array(
				"id"=>$key,
				"name"=>$val
			);
		}
		$arr = array("id"=>"","name"=>"请选择...");
		array_unshift($arrData,$arr);
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	function editIntention(){
		checkLogin();
		$customer_id = $_REQUEST["customer_id"];
		$this->assign("customer_id",$customer_id);
		$did_num = $_REQUEST["did_num"];
		$this->assign("did_num",$did_num);
		$id = $_REQUEST["id"];
		$this->assign("id",$id);

		$order_customer_product = new Model("order_customer_product");
		$arrData = $order_customer_product->where("id = '$id'")->find();
		//dump($arrData);die;
		$this->assign("arrDidInfo",$arrData);

		$this->display();
	}

	function updateGoodsIntention(){
		$id = $_REQUEST['id'];
		$order_customer_product = new Model("order_customer_product");
		$arrData = array(
			'modify_time'=>date("Y-m-d H:i:s"),
			'Number400'=>$_REQUEST['Number400'],
			'ReceivingDepartment'=>$_REQUEST['ReceivingDepartment'],
			'UseDepartment'=>$_REQUEST['UseDepartment'],
			'productA'=>$_REQUEST['productA'],
			'groups'=>$_REQUEST['groups'],
			'grade'=>$_REQUEST['grade'],
			'source_type'=>$_REQUEST['source_type'],
			'media_source'=>$_REQUEST['media_source'],
		);
		$result = $order_customer_product->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteIntention(){
		$id = $_REQUEST["id"];
		$order_customer_product = new Model("order_customer_product");
		$result = $order_customer_product->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

}
?>
