<?php

class PbxAction extends Action {

    public function index(){
		checkLogin();
        $this->display();
    }
	public function pbx(){
		$this->display();
	}

	public function codecInstall(){
		$this->display();

	}

	public function codecSetup(){
		if($_GET['codetype']== 'g729' || $_GET['codetype']== 'g723'){
			$codec_ary["g723"] = array(
				"athlon"=>"codec_g723-ast18-gcc4-glibc-athlon-sse.so",
				"atom"=>"codec_g723-ast18-gcc4-glibc-atom.so",
				"opteron"=>"codec_g729-ast18-gcc4-glibc-opteron.so",
				"core"=>"codec_g723-ast18-gcc4-glibc-x86_64-core2.so",
				"pentium"=>"codec_g723-ast18-gcc4-glibc-pentium4.so",
				"opteron_64"=>"codec_g723-ast18-gcc4-glibc-x86_64-opteron.so",
				"pentium_64"=>"codec_g723-ast18-gcc4-glibc-x86_64-pentium4.so",
				"core_64"=>"codec_g723-ast18-gcc4-glibc-core2.so");
			$codec_ary["g729"] = array(
				"athlon"=>"codec_g729-ast18-gcc4-glibc-athlon-sse.so",
				"atom"=>"codec_g729-ast18-gcc4-glibc-atom.so",
				"opteron"=>"odec_g729-ast18-gcc4-glibc-opteron.so",
				"core"=>"codec_g729-ast18-gcc4-glibc-core2.so",
				"pentium"=>"codec_g729-ast18-gcc4-glibc-pentium4.so",
				"opteron_64"=>"codec_g729-ast18-gcc4-glibc-x86_64-opteron.so",
				"pentium_64"=>"codec_g729-ast18-gcc4-glibc-x86_64-pentium4.so",
				"core_64"=>"codec_g729-ast18-gcc4-glibc-x86_64-core2.so"
			);
			$errorNo = $errorStr = NULL;

			$cpuinfo = exec("cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c 2>&1",&$errorNO,&$errorStr);

			if(stristr($cpuinfo,'core') ||  stristr($cpuinfo,'Celeron' )){
				$cpu = "core";
			}elseif(stristr($cpuinfo,"pentium") || stristr($cpuinfo,"xeon")){
				$cpu = "pentium";
			}elseif(stristr($cpuinfo,"atom")){
				$cpu = "atom";
			}elseif(stristr($cpuinfo,"athlon")){
				$cpu = "athlon";
			}elseif(stristr($cpuinfo,"opteron")){
				$cpu = "opteron";
			}else{
				$cpu = "core";
			}
			$osinfo = exec("uname -a",&$errorNO,&$errorStr);
			if(stristr($osinfo,"64")){
				$os = "64";
			}
			$codec = $os ? $cpu."_".$os:$cpu.$os;
			$file = $codec_ary[$_GET['codetype']][$codec];;
			exec("cp -f /web/lch.cc/data/codec/".$file
				." /usr/lib$os/asterisk/modules/codec_".$_GET["codetype"].".so",&$errorNO,&$errorStr);

			echo "sudo /bin/cp -f /web/lch.cc/data/codec/".$file
				." /usr/lib$os/asterisk/modules/codec_".$_GET["codetype"].".so";
			import('ORG.Pbx.bmi');
			$bmi = new bmi();
			$rs = $bmi->exec("module load codec_".$_GET["codetype"].".so");
		}else{
			echo "only install g729 or g723";
		}
	}

	public function codecData(){

		import('ORG.Pbx.bmi');
		$bmi = new bmi();

		$rs = $bmi->exec(" core show translation");
		$lines 	= explode("\n", $rs['data']);
        foreach($lines as $key => $line)
        {
			if($i>4)
            {
				$array =  preg_split("/\s+/",$line);
				$array["codec_name"] = $array[1];
				$array["status"] = $array[4] =='-' ? "不支持" : "支持";
				if(!empty($array["codec_name"])){
					$arr[] = $array;
				}
			}
			$i++;
		}
		$rowsList = count($arr) ? $arr : false;
		$arrCodec["total"] = count($rowsList);
		$arrCodec["rows"] = $rowsList;
		echo json_encode($arrCodec);
	}

	public function extension(){
		$devices = M("devices");
        import("ORG.Util.Page");
        $count = $devices->count();
        $p = new Page($count,"50");
		$sql = "SELECT d.*,u.* FROM asterisk.devices d LEFT JOIN asterisk.users u ON u.extension = d.id
				ORDER BY id ASC" ;

        $list = $devices->query($sql);
        $page = $p->show();

        $this->assign("page", $page);
		$this->assign("count", count($list));
        $this->assign("list", $list);

		$menuname = "Extensions";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	public function extensionData(){
		$devices = M("devices");
        $count = $devices->count();
		$sql = "SELECT d.*,u.* FROM asterisk.devices d LEFT JOIN asterisk.users u ON u.extension = d.id
				ORDER BY id ASC" ;
        $list = $devices->query($sql);

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$disExtension = Array(); //转换成显示的
		$i = $j = 0;
		foreach($list AS &$v){
			$v['callin_billsec_total'] = ceil($v['callin_billsec_total']/60);
			$v['avg_callin'] = ceil($v['avg_callin']/60);
			$v['callout_billsec_total'] = ceil($v['callout_billsec_total']/60);
			$v['avg_callout'] = ceil($v['avg_callout']/60);

			if($i >= $start && $j < $length){
				$disExtension[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}
		//dump($disExtension);die;
		$rowsList = count($disExtension) ? $disExtension : false;
		$arrExtension["total"] = count($list);
		$arrExtension["rows"] = $rowsList;

		echo json_encode($arrExtension);
	}

	public function extensionBatch(){
		$this->display();
	}
	public function pbxAdmin(){
		$_REQUEST['action'] = isset($_POST["action"]) ? $_POST["action"] : $_REQUEST['action'];
		include_once ("admin.php");
		$content = getContent($this ,"exteinsion",false);
		$this->assign("content", $content);
		$this->display();

	}
	public function extensionDownload(){
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');

		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
    	$arrAMP  = $pConfig->leer_configuracion(false);

		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";

		$pDB = new paloDB($dsnAsterisk);
		if(!empty($pDB->errMsg)) {
			echo $arrLang["Error when connecting to database"]."\n".$pDB->errMsg;
		}

		header("Cache-Control: private");
		header("Pragma: cache");
		header('Content-Type: text/csv; charset=iso-8859-1; header=present');
		header("Content-disposition: attachment; filename=extensions-".date("YmdHis",time()).".csv");

		echo $this->backup_extensions($pDB);

	}

	//批量删除分机
	public function extensionBatchDelete(){

		import('ORG.Pbx.extensionsBatch');
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');

		$arrExtension = explode(",",$_POST["del_ary"]);
		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAMP  = $pConfig->leer_configuracion(false);
		$arrAST  = $pConfig->leer_configuracion(false);

		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";

		$pDB = new paloDB($dsnAsterisk);

		$pConfig = new paloConfig($arrAMP['ASTETCDIR']['valor'], "asterisk.conf", "=", "[[:space:]]*=[[:space:]]*");
		$pLoadExtension = new paloSantoLoadExtension($pDB);
		$pLoadExtension->deleteAryExtension($arrExtension);
		$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());
		$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
		if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
				$Messages .= $pLoadExtension->errMsg;
		//echo "{1}";
		if($pLoadExtension){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}

	}
	//批量添加分机
	public function extensionBatchSave(){

		import('ORG.Pbx.extensionsBatch');
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');
		$base_dir=dirname($_SERVER['SCRIPT_FILENAME']);
		$templates_dir=(isset($arrConf['templates_dir']))?$arrConf['templates_dir']:'themes';
		$local_templates_dir="$base_dir/modules/$module_name/".$templates_dir.'/'.$arrConf['theme'];

		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAMP  = $pConfig->leer_configuracion(false);

		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";

		$pDB = new paloDB($dsnAsterisk);
		if(!empty($pDB->errMsg)) {
			$smarty->assign("mb_message", $arrLang["Error when connecting to database"]."<br/>".$pDB->errMsg);
		}


		$pConfig = new paloConfig($arrAMP['ASTETCDIR']['valor'], "asterisk.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAST  = $pConfig->leer_configuracion(false);
			 //valido el tipo de archivo
		if (!eregi('.csv$', $_FILES['userfile']['name'])) {

			$smarty->assign("mb_title", $arrLang["Validation Error"]);
			$smarty->assign("mb_message", $arrLang["Invalid file extension.- It must be csv"]);
		}else {
			if(is_uploaded_file($_FILES['userfile']['tmp_name'])) {

				$ruta_archivo = "/tmp/".$_FILES['userfile']['name'];
				copy($_FILES['userfile']['tmp_name'], $ruta_archivo);
				//Funcion para cargar las extensiones
				$message = $this->load_extension_from_csv( $arrLang, $ruta_archivo, $base_dir, $pDB, $arrAST, $arrAMP);
			}else {

				$this->error("上传文件失败!");
			}
		}

		$this->success("$message!","index.php?m=Pbx&a=extensionBatch");
	}


	function backup_extensions($pDB)
	{
		global $arrLang;
		import('ORG.Pbx.extensionsBatch');
		$csv = "";
		$pLoadExtension = new paloSantoLoadExtension($pDB);
		$arrResult = $pLoadExtension->queryExtensions();



		if(!$arrResult){

		$csv .= "\"Display Name\",\"User Extension\",\"Direct DID\",\"Outbound CID\",\"Call Waiting\",".
					"\"Secret\",\"Voicemail Status\",\"Voicemail Password\",\"VM Email Address\",".
					"\"VM Pager Email Address\",\"VM Options\",\"VM Email Attachment\",".
					"\"VM Play CID\",\"VM Play Envelope\",\"VM Delete Vmail\",\"Context\",\"Tech\"\n";
		}else{
			//cabecera
			$csv .= "\"Display Name\",\"User Extension\",\"Direct DID\",\"Outbound CID\",\"Call Waiting\",".
					"\"Secret\",\"Voicemail Status\",\"Voicemail Password\",\"VM Email Address\",".
					"\"VM Pager Email Address\",\"VM Options\",\"VM Email Attachment\",".
					"\"VM Play CID\",\"VM Play Envelope\",\"VM Delete Vmail\",\"Context\",\"Tech\"\n";
			foreach($arrResult as $key => $extension)
			{

	//////////////////////////////////////////////////////////////////////////////////
			// validando para que coja las comillas
				$extension['outboundcid'] = ereg_replace("\"",'“',$extension['outboundcid']);
				$extension['outboundcid'] = ereg_replace("\"",'”', $extension['outboundcid']);
	//////////////////////////////////////////////////////////////////////////////////
				$csv .= "\"{$extension['name']}\",\"{$extension['extension']}\",\"{$extension['directdid']}\",\"{$extension['outboundcid']}\",".
						"\"{$extension['callwaiting']}\",\"{$extension['secret']}\",\"{$extension['voicemail']}\",".
						"\"{$extension['vm_secret']}\",\"{$extension['email_address']}\",\"{$extension['pager_email_address']}\",".
						"\"{$extension['vm_options']}\",\"{$extension['email_attachment']}\",\"{$extension['play_cid']}\",".
						"\"{$extension['play_envelope']}\",\"{$extension['delete_vmail']}\",\"{$extension['context']}\",\"{$extension['tech']}\"".
						"\n";
			}
		}
		return $csv;
	}

	function load_extension_from_csv( $arrLang, $ruta_archivo, $base_dir, $pDB, $arrAST, $arrAMP){
		$Messages = "";
		$arrayColumnas = array();
		$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());

		$result = $this->isValidCSV($arrLang, $ruta_archivo, $arrayColumnas);
		if($result != "valided"){
			$this->error("$result");
			return;
		}

		$hArchivo = fopen($ruta_archivo, 'rt');
		$cont = 0;
		$pLoadExtension = new paloSantoLoadExtension($pDB);

		if($hArchivo) {
			//Linea 1 header ignorada
			$tupla = fgetcsv($hArchivo, 4096, ",");
			//Desde linea 2 son datos

			while ($tupla = fgetcsv($hArchivo, 4096, ",")) {
				if(is_array($tupla) && count($tupla)>=3)
				{
					$Name               = $tupla[$arrayColumnas[0]];
					$Ext                = $tupla[$arrayColumnas[1]];
					$Direct_DID         = isset($arrayColumnas[2]) ?$tupla[$arrayColumnas[2]]:"";
					$Outbound_CID       = isset($arrayColumnas[3]) ?$tupla[$arrayColumnas[3]]:"";
					$Call_Waiting       = isset($arrayColumnas[4]) ?$tupla[$arrayColumnas[4]]:"";
					$Secret             = $tupla[$arrayColumnas[5]];
					$VoiceMail          = isset($arrayColumnas[6]) ?$tupla[$arrayColumnas[6]]:"";
					$VoiceMail_PW       = isset($arrayColumnas[7]) ?$tupla[$arrayColumnas[7]]:"";
					$VM_Email_Address   = isset($arrayColumnas[8]) ?$tupla[$arrayColumnas[8]]:"";
					$VM_Pager_Email_Addr= isset($arrayColumnas[9]) ?$tupla[$arrayColumnas[9]]:"";
					$VM_Options         = isset($arrayColumnas[10]) ?$tupla[$arrayColumnas[10]]:"";
					$VM_EmailAttachment = isset($arrayColumnas[11])?$tupla[$arrayColumnas[11]]:"";
					$VM_Play_CID        = isset($arrayColumnas[12])?$tupla[$arrayColumnas[12]]:"";
					$VM_Play_Envelope   = isset($arrayColumnas[13])?$tupla[$arrayColumnas[13]]:"";
					$VM_Delete_Vmail    = isset($arrayColumnas[14])?$tupla[$arrayColumnas[14]]:"";
					$Context            = isset($arrayColumnas[15])?$tupla[$arrayColumnas[15]]:"from-internal";
					$Tech               = strtolower($tupla[$arrayColumnas[16]]);
	//////////////////////////////////////////////////////////////////////////////////
					// validando para que coja las comillas
					$Outbound_CID = ereg_replace('“', "\"", $Outbound_CID);
					$Outbound_CID = ereg_replace('”', "\"", $Outbound_CID);
	//////////////////////////////////////////////////////////////////////////////////
					//Paso 1: creando en la tabla sip
					if(!$pLoadExtension->createTechDevices($Ext,$Secret,$VoiceMail,$Context,$Tech))
					{
						$Messages .= "分机: $Ext - 更新类型失败: ".$pLoadExtension->errMsg."<br />";
					}else{

						//Paso 2: creando en la tabla users
						if(!$pLoadExtension->createUsers($Ext,$Name,$VoiceMail,$Direct_DID,$Outbound_CID))
							$Messages .= "分机: $Ext - 更新用户失败: ".$pLoadExtension->errMsg."<br />";

						//Paso 3: creando en la tabla devices
						if(!$pLoadExtension->createDevices($Ext,$Tech,$Name))
							$Messages .= "分机: $Ext - 创建失败: ".$pLoadExtension->errMsg."<br />";

						//Paso 4: creando en el archivo /etc/asterisk/voicemail.conf los voicemails
						if(!$pLoadExtension->writeFileVoiceMail(
							$Ext,$Name,$VoiceMail,$VoiceMail_PW,$VM_Email_Address,
							$VM_Pager_Email_Addr,$VM_Options,$VM_EmailAttachment,$VM_Play_CID,
							$VM_Play_Envelope, $VM_Delete_Vmail)
						  )
							$Messages .= "分机: $Ext - 更新语音信箱失败<br />";

						//Paso 5: Configurando el call waiting
						if(!$pLoadExtension->processCallWaiting($Call_Waiting,$Ext))
							$Messages .= "分机: $Ext - 更新呼叫等待失败<br />";

						$outboundcid = ereg_replace("\"", "'", $Outbound_CID);
						$outboundcid = ereg_replace("\"", "'", $outboundcid);
						$outboundcid = ereg_replace(" ", "", $outboundcid);
						if(!$pLoadExtension->putDataBaseFamily($data_connection, $Ext, $Tech, $Name, $VoiceMail, $outboundcid))
							$Messages .= "分机: $Ext - 更新数据库失败<br />";
						$cont++;
					}
					////////////////////////////////////////////////////////////////////////
					//Paso 7: Escribiendo en tabla incoming
				if($Direct_DID !== ""){
					if(!$pLoadExtension->createDirect_DID($Ext,$Direct_DID))
					$Messages .= "分机: $Ext - 更新did失败<br />";
				}
						/////////////////////////////////////////////////////////////////////////
					}
				}
				//Paso 6: Realizo reload
				if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
					$Messages .= $pLoadExtension->errMsg;
					$Messages .= "共成功添加: $cont 个分机<br />";

			}


			unlink($ruta_archivo);
			return $Messages;
	}

	function isValidCSV($arrLang, $sFilePath, &$arrayColumnas){
		$hArchivo = fopen($sFilePath, 'rt');
		$cont = 0;
		$ColName = -1;




		if ($hArchivo) {
			$tupla = fgetcsv($hArchivo, 4096, ",");

			if(count($tupla)>=4)
			{

				for($i=0; $i<count($tupla); $i++)
				{
					if($tupla[$i] == 'Display Name')            $arrayColumnas[0] = $i;
					else if($tupla[$i] == 'User Extension')     $arrayColumnas[1] = $i;
					else if($tupla[$i] == 'Direct DID')         $arrayColumnas[2] = $i;
					else if($tupla[$i] == 'Outbound CID')       $arrayColumnas[3] = $i;
					else if($tupla[$i] == 'Call Waiting')       $arrayColumnas[4] = $i;
					else if($tupla[$i] == 'Secret')             $arrayColumnas[5] = $i;
					else if($tupla[$i] == 'Voicemail Status')   $arrayColumnas[6] = $i;
					else if($tupla[$i] == 'Voicemail Password') $arrayColumnas[7] = $i;
					else if($tupla[$i] == 'VM Email Address')   $arrayColumnas[8] = $i;
					else if($tupla[$i] == 'VM Pager Email Address') $arrayColumnas[9] = $i;
					else if($tupla[$i] == 'VM Options')         $arrayColumnas[10] = $i;
					else if($tupla[$i] == 'VM Email Attachment')$arrayColumnas[11] = $i;
					else if($tupla[$i] == 'VM Play CID')        $arrayColumnas[12] = $i;
					else if($tupla[$i] == 'VM Play Envelope')   $arrayColumnas[13] = $i;
					else if($tupla[$i] == 'VM Delete Vmail')    $arrayColumnas[14] = $i;
					else if($tupla[$i] == 'Context')            $arrayColumnas[15] = $i;
					else if($tupla[$i] == 'Tech')               $arrayColumnas[16] = $i;
				}
				if(isset($arrayColumnas[0]) && isset($arrayColumnas[1]) && isset($arrayColumnas[5]) && isset($arrayColumnas[16]))
				{
					//Paso 2: Obtener Datos (Validacion que esten llenos los mismos de las cabeceras)
					$count = 2;
					while ($tupla = fgetcsv($hArchivo, 4096,",")) {
						if(is_array($tupla) && count($tupla)>=3)
						{
							$Ext = $tupla[$arrayColumnas[1]];
							if($Ext != '')
								$arrExt[] = array("ext" => $Ext);
							else return $arrLang["Can't exist a extension empty. Line"].": $count. - ". $arrLang["Please read the lines in the footer"];

							$Secret       = $tupla[$arrayColumnas[5]];
							if($Secret == '')
								return $arrLang["Can't exist a secret empty. Line"].": $count. - ". $arrLang["Please read the lines in the footer"];

							$Display      = $tupla[$arrayColumnas[0]];
							if($Display == '')
								return $arrLang["Can't exist a display name empty. Line"].": $count. - ". $arrLang["Please read the lines in the footer"];

				$Tech         = $tupla[$arrayColumnas[16]];
							if($Tech == '')
								return $arrLang["Can't exist a technology empty. Line"].": $count. - ". $arrLang["Please read the lines in the footer"];
				else
					$arrTech[] = strtolower($Tech);
						}
						$count++;
					}

					//Paso 3: Validacion extensiones repetidas
					if(is_array($arrExt) && count($arrExt) > 0){
						foreach($arrExt as $key1 => $values1){
							foreach($arrExt as $key2 => $values2){
								if( ($values1['ext']==$values2['ext'])  &&  ($key1!=$key2) ){
									return "{$arrLang["Error, extension"]} ".$values1['ext']." {$arrLang["repeat in lines"]} ".($key1 + 2)." {$arrLang["with"]} ".($key2 + 2);
								}
							}
				if( $arrTech[$key1]!="sip" && $arrTech[$key1]!="iax" && $arrTech[$key1]!="iax2" ){
								return "{$arrLang["Error, extension"]} ".$values1['ext']." {$arrLang["has a wrong tech in line"]} ".($key1 + 2).". {$arrLang["Tech must be sip or iax"]}";
							}
						}
						return "valided";
					}
				}else return $arrLang["Verify the header"] ." - ". $arrLang["At minimum there must be the columns"].": \"Display Name\", \"User Extension\", \"Secret\", \"Tech\"";
			}
			else return $arrLang["Verify the header"] ." - ". $arrLang["Incomplete Columns"];
		}else return $arrLang["The file is incorrect or empty"] .": $sFilePath";
	}

	//显示cid
	function listCID(){

		$menuname = "SetCID";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$trunks = new Model("asterisk.trunks");
		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$trunkData = $trunks->field("trunkid,name")->select();
		$this->assign("trunkData",$trunkData);
		$this->display();
	}

	//显示cid的数据
	function listCIDData(){
		$cid = M("asterisk.exten_trunk_cid");
        import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		mysql_query("set names latin1");

		$list = $cid->order("exten ASC")->table("asterisk.exten_trunk_cid e")->field("e.exten,e.trunkid,e.number,t.name")->join("asterisk.trunks t on e.trunkid = t.trunkid")->limit($page->firstRow.','.$page->listRows)->select();

		//echo $cid->getLastSql();die;
		$count = $cid->table("asterisk.exten_trunk_cid e")->join("asterisk.trunks t on e.trunkid = t.trunkid")->count();

		$rowsList = count($list) ? $list : false;
		$arrDID["total"] = $count;
		$arrDID["rows"] = $rowsList;

		echo json_encode($arrDID);
	}

	function insertCID(){
		$trunks = new Model("asterisk.exten_trunk_cid");
		$arrData = array(
			"exten"=>$_REQUEST['exten'],
			"trunkid"=>$_REQUEST['trunkid'],
			"number"=>$_REQUEST['number'],
		);
		//dump($arrData);die;
		$result = $trunks->data($arrData)->add();
		//echo $trunks->getLastSql();die;

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$action = "DBPut";
		//不同的分机走不同的中继————孟飞的功能
		$parameters = Array("Family"=>'AMPUSER',"Key"=>$_REQUEST['exten'].'/trunk',"Val"=>$_REQUEST['trunkid']);
		$bmi->asm->send_request($action,$parameters);
		//上面那个还需要在freePBX中的[macro-outbound-callerid] 里面加上一句 exten => s,n,Set(DIAL_TRUNK=${DB(AMPUSER/${AMPUSER}/trunk)}) ，在设置EXTEN_TRUNK_CID的值之前添加
		//分机走了这个中继后，要绑定一个分机号一个CID————天波的功能
		$parameters = Array("Family"=>'AMPUSER',"Key"=>$_REQUEST['exten'].'/'.$_REQUEST['trunkid'],"Val"=>$_REQUEST['number']);
		$bmi->asm->send_request($action,$parameters);

		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateCID(){
		$trunkid = $_GET['trunkid'];
		$exten = $_GET['exten'];
		$trunks = new Model("asterisk.exten_trunk_cid");
		mysql_query("set names latin1");
		$arrData = array(
			"exten"=>$_POST['exten'],
			"trunkid"=>$_POST['trunkid'],
			"number"=>$_POST['number'],
		);
		//dump($arrData);die;
		$result = $trunks->data($arrData)->where("exten ='$exten' AND trunkid = '$trunkid'")->save();
		//echo $trunks->getLastSql();die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$action = "DBPut";

		//不同的分机走不同的中继————孟飞的功能
		$parameters = Array("Family"=>'AMPUSER',"Key"=>$_REQUEST['exten'].'/trunk',"Val"=>$_REQUEST['trunkid']);
		$bmi->asm->send_request($action,$parameters);
		//上面那个还需要在freePBX中的[macro-outbound-callerid] 里面加上一句 exten => s,n,Set(DIAL_TRUNK=${DB(AMPUSER/${AMPUSER}/trunk)}) ，在设置EXTEN_TRUNK_CID的值之前添加
		//分机走了这个中继后，要绑定一个分机号一个CID————天波的功能
		$parameters = Array("Family"=>'AMPUSER',"Key"=>$_REQUEST['exten'].'/'.$_REQUEST['trunkid'],"Val"=>$_REQUEST['number']);
		$bmi->asm->send_request($action,$parameters);
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteCID(){
		$trunkid = $_REQUEST['trunkid'];
		$exten = $_REQUEST['exten'];
		$trunks = new Model("asterisk.exten_trunk_cid");
		$result = $trunks->where("exten ='$exten' AND trunkid = '$trunkid'")->delete();
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$action = "DBDel";
		$parameters = Array("Family"=>'AMPUSER',"Key"=>$_REQUEST['exten'].'/'.$_REQUEST['trunkid']);
		$bmi->asm->send_request($action,$parameters);

		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}

	}
}
