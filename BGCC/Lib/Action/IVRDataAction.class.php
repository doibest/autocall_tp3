<?php
class IVRDataAction extends Action{
	function ivrList(){
		checkLogin();
		$this->display();
	}

	function ivrDataList(){
		$ivr = new Model("asterisk.ivr");
		$count = $ivr->where("displayname <> '__install_done'")->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$ivrData = $ivr->limit($page->firstRow.",".$page->listRows)->where("displayname <> '__install_done'")->select();
		$rowsList = count($ivrData) ? $ivrData : false;
		$arrIvr["total"] = $count;
		$arrIvr["rows"] = $rowsList;
		echo json_encode($arrIvr);
	}

	function addIvr(){
		checkLogin();
		include_once ("/var/www/html/admin.php");
		include "/var/www/html/admin/modules/ivr/functions.inc.php";
		$content = getContent($this ,"exteinsion",false);

		$drawselects_tpl = drawselects($goto,0);
		//$drawselects_tpl = drawselects($dest,$count,false,false);
		$this->assign("drawselects_tpl",$drawselects_tpl);
		$this->display();
	}

	function editIvr(){
		checkLogin();
		$this->display();
	}
}

?>
