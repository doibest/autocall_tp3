<?php
class MailSettingAction extends Action{
	function mailList(){
		checkLogin();

		//分配增删改的权限
		$menuname = "System Mail Template";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function mailData(){
		$mailt = new Model("mailtemplate");
		$count = $mailt->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$mailData = $mailt->field("id,templatetype as tmptype,templatetype,title,mailcontent")->select();

		$status_row = array('system'=>'系统模板','personal'=>'私人模板');
		foreach($mailData as &$val){
			$status = $status_row[$val['tmptype']];
			$val['tmptype'] = $status;
		}

		$rowsList = count($mailData) ? $mailData : false;
		$arrmail["total"] = $count;
		$arrmail["rows"] = $rowsList;

		echo json_encode($arrmail);
	}

	function insertMailSetting(){
		$mail = new Model("mailtemplate");
		$username = $_SESSION['user_info']['username'];
		$arrData = Array(
			'username'=>$username,
			'templatetype'=>$_REQUEST['templatetype'],
			'title'=>$_REQUEST['title'],
			'mailcontent'=>$_REQUEST['mailcontent'],
		);
		$result = $mail->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'邮件模板添加成功！'));
		} else {
			echo json_encode(array('msg'=>'邮件模板添加失败！'));
		}
	}

	function updateTemplate(){
		$id = $_REQUEST['id'];
		$mail = new Model("mailtemplate");
		$arrData = Array(
			'templatetype'=>$_REQUEST['templatetype'],
			'title'=>$_REQUEST['title'],
			'mailcontent'=>$_REQUEST['mailcontent'],
		);
		$result = $mail->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function deleteMailTemplate(){
		$id = $_REQUEST["id"];
		$mail = new Model("mailtemplate");
		$result = $mail->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function agentMailList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Mail Settings";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function agentMailData(){
		$user_mail = new Model("user_mail");
		$count = $user_mail->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $user_mail->limit($page->firstRow.','.$page->listRows)->select();

		$row = array("Y"=>"启用","N"=>"禁用");
		foreach($arrData as &$val){
			$val["mail_enable2"] = $val["mail_enable"];
			$mail_enable = $row[$val["mail_enable2"]];
			$val["mail_enable2"] = $mail_enable;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}




	function insertMail(){
		$mail = new Model("user_mail");
		$agentname = $_REQUEST['username'];
		$count = $mail->where("agentname = '$agentname'")->count();
		if($count > 0){
			echo json_encode(array('msg'=>'该坐席的邮箱已经设置了！'));
			die;
		}
		$arrData = Array(
			'agentname'=>$_REQUEST['username'],
			'mail_host'=>$_REQUEST['mail_host'],
			'mail_from'=>$_REQUEST['mail_from'],
			'mail_password'=>$_REQUEST['mail_password'],
			'mail_enable'=>$_REQUEST['mail_enable'],
		);
		//dump($arrData);die;
		$result = $mail->data($arrData)->add();
		if ($result){
			$this->mailCache();
			echo json_encode(array('success'=>true,'msg'=>'邮件模板添加成功！'));
		} else {
			echo json_encode(array('msg'=>'邮件模板添加失败！'));
		}
	}

	function updateMail(){
		$id = $_REQUEST['id'];
		$mail = new Model("user_mail");
		$arrData = Array(
			'mail_host'=>$_REQUEST['mail_host'],
			'mail_from'=>$_REQUEST['mail_from'],
			'mail_password'=>$_REQUEST['mail_password'],
			'mail_enable'=>$_REQUEST['mail_enable'],
		);
		$result = $mail->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			$this->mailCache();
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function deleteMail(){
		$id = $_REQUEST["id"];
		$mail = new Model("user_mail");
		$result = $mail->where("id in ($id)")->delete();
		if ($result){
			$this->mailCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function mailCache(){
		$mail = new Model("user_mail");
		$arrData = $mail->select();
		foreach($arrData as $key=>$val){
			$arrF[$val["agentname"]] = $val;
		}
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F("emailSmtp",$arrF,"BGCC/Conf/crm/$db_name/");
		//F("emailSmtp",$arrF,"BGCC/Conf/");
	}

}

?>
