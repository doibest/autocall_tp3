<?php
//日报管理
class DailyManagementsAction extends Action{
	function dailyTypeList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Daily Type";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$this->display();
	}

	function DailyTypeData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$mod = M("daily_type");
		$count = $mod->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->limit($page->firstRow.','.$page->listRows)->select();
		$daily_enabled_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$val['daily_enabled2'] = $daily_enabled_row[$val['daily_enabled']];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	function insertDailyType(){
		$username = $_SESSION['user_info']['username'];
		$mod = M("daily_type");
		$arrData = array(
			'daily_type_name'=>$_REQUEST['daily_type_name'],
			'daily_enabled'=>$_REQUEST['daily_enabled'],
		);
		$result = $mod->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateDailyType(){
		$id = $_REQUEST['id'];
		$mod = M("daily_type");
		$arrData = array(
			'daily_type_name'=>$_REQUEST['daily_type_name'],
			'daily_enabled'=>$_REQUEST['daily_enabled'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function deleteDailyType(){
		$id = $_REQUEST["id"];
		$mod = M("daily_type");
		$result = $mod->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function getDailyType(){
		$mod = M("daily_type");
		$arrData = $mod->order("id asc")->field("id,daily_type_name as name")->where("daily_enabled = 'Y'")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	function addDaily(){
		checkLogin();
		$users = M("users");
		$arrU = $users->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$date = date("Y-m-d");
		$this->assign("date",$date);

		$this->display();
	}

	function insertDaily(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$file_path = "include/data/daily/";
		mkdirs($file_path);
		$upload->savePath= $file_path;  //上传路径

		$upload->saveRule="uniqid";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		//$upload->allowExts=array('al');     //准许上传的文件后缀


		$username = $_SESSION['user_info']['username'];
		$content = str_replace("\\","",$_POST["content"]);
		$mod = new Model('daily_content');
		$sharing_personnel = implode(",",$_REQUEST["sharing_personnel"]);
		//dump($arrData);die;

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = array(
					"createtime" =>date("Y-m-d H:i:s"),
					"create_user" =>$username,
					"title" =>$_POST["title"],
					"content" =>$content,
					"type_id" =>$_POST["type_id"],
					"daily_date" =>$_POST["daily_date"],
					"recipient_user" =>$sharing_personnel,
					"sharing_personnel" =>$sharing_personnel,
				);
				$result = $mod->data($arrData)->add();
				if ($result){
					echo json_encode(array('success'=>true,'msg'=>'日报添加成功！'));
				} else {
					echo json_encode(array('msg'=>'日报添加失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info = $upload->getUploadFileInfo();
			//dump($info);die;
			$arrData = array(
				"createtime" =>date("Y-m-d H:i:s"),
				//"create_user" =>$_POST["create_user"],
				"create_user" =>$username,
				"title" =>$_POST["title"],
				"content" =>$content,
				"type_id" =>$_POST["type_id"],
				"daily_date" =>$_POST["daily_date"],
				"recipient_user" =>$sharing_personnel,
				"sharing_personnel" =>$sharing_personnel,
			);
			$result = $mod->data($arrData)->add();
			//echo $mod->getLastSql();die;
			if ($result){
				$daily_annex = new Model('daily_annex');
				foreach($info as $val){
					$arrDataA = array(
						"createtime" =>date("Y-m-d H:i:s"),
						"create_user" =>$username,
						"daily_id" =>$result,
						"file_path_name" =>$val["savepath"].$val["savename"],
						"file_name" =>$val["name"],
					);
					$res = $daily_annex->data($arrDataA)->add();
				}
				if($res){
					$count = $daily_annex->where("daily_id = '$result'")->count();
					$arr = array(
						"annex_num"=>$count,
					);
					$mod->data($arr)->where("id = '$result'")->save();
				}
				echo json_encode(array('success'=>true,'msg'=>'日报添加成功！'));
			} else {
				echo json_encode(array('msg'=>'日报添加失败！'));
			}
		}

	}

	function userData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$mod = M("users");
		$count = $mod->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->order("username asc")->field("username,cn_name")->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function viewDaily(){
		checkLogin();
		$users = M("users");
		$arrU = $users->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$web_type = empty($_REQUEST["web_type"]) ? "my" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$this->display();
	}

	function dailyList(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$web_type = $_REQUEST["web_type"];
		$user_name = $_REQUEST["user_name"];

		$where = "1 ";
		$where .= empty($user_name) ? "" : " AND create_user = '$user_name'";

		$mod = M("daily_content");
		$arrData = $mod->order("daily_date desc")->where($where)->select();
		$count = $mod->where($where)->count();

		$daily_type_name = $this->getDailyTypeRow();
		foreach($arrData as &$val){
			$val["daily_type_name"] = $daily_type_name[$val["type_id"]];
		}
		$this->assign("arrData",$arrData);
		$this->assign("web_type",$web_type);
		$this->assign("count",$count);

		$this->display();
	}


	function getDailyTypeRow(){
		$mod = M("daily_type");
		$arrData = $mod->order("id asc")->field("id,daily_type_name")->select();
		$arrF = array();
		foreach($arrData as $val){
			$arrF[$val["id"]] = $val["daily_type_name"];
		}
		return $arrF;
	}

	function dailyContentView(){
		checkLogin();
		$id = $_GET['id'];
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$daily_content = new Model("daily_content");
		if($id){
			$this->assign("id",$id);
			$dailyContentData = $daily_content->where("id = $id")->find();
			$dailyContentData['cn_name'] = $cnName[$dailyContentData["create_user"]];

			$this->assign("dailyContentData",$dailyContentData);
			$CTI_IP = $_SERVER["SERVER_ADDR"];
			$this->assign("CTI_IP",$CTI_IP);

			$click_num = $dailyContentData["click_num"]+1;
			$daily_content->where("id = $id")->save(array("click_num"=>$click_num));
		}
		//dump($dailyContentData);die;

		//浏览
		$browse_num = $dailyContentData["browse_num"]+1;
		$daily_content->where("id = $id")->save(array("browse_num"=>$browse_num));

		$username = $_SESSION["user_info"]["username"];
		$daily_operating_record = M("daily_operating_record");
		$collection_num = $daily_operating_record->where("daily_id = '$id' AND operation_type = '1' AND create_user = '$username'")->count();   //收藏
		if($collection_num > 0){
			$this->assign("collection_num","Y");
		}else{
			$this->assign("collection_num","N");
		}
		$thumb_up_num = $daily_operating_record->where("daily_id = '$id' AND operation_type = '2' AND create_user = '$username'")->count();  //点赞
		if($thumb_up_num > 0){
			$this->assign("thumb_up_num","Y");
		}else{
			$this->assign("thumb_up_num","N");
		}

		//评论
		$arrReviewData = $daily_operating_record->order("create_time desc")->where("daily_id = '$id' AND operation_type = '3'")->select();
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrReviewData as &$val){
			$val['cn_name'] = $cnName[$val["create_user"]];
		}
		$arrReview = $this->getReviewTree($arrReviewData,"0","pid","id");
		$this->assign("arrReview",$arrReview);
		//dump($arrReview);die;

		//分享
		if($username == $dailyContentData["create_user"]){
			$this->assign("operation","Y");
		}else{
			$this->assign("operation","N");
		}
		$users = M("users");
		$arrU = $users->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$department = M("department");
		$arrDept = $department->select();
		$this->assign("arrDept",$arrDept);

		//分配增删改的权限
		$menuname = "My Document";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);


		$this->display();
	}

	/*
	$data： 数组
	$pId： 顶级父id的值【一般为0】
	$field_pid：父id字段【如：d_pid】
	$field_id： id字段【如d_id】
	*/
	function getReviewTree($data, $pId,$field_pid,$field_id) {
		$tree = '';
		foreach($data as $k =>$v) {
			if($v[$field_pid] == $pId)    {
				$v['children'] = $this->getReviewTree($data, $v[$field_id],$field_pid,$field_id);
				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				$tree[] = $v;     //unset($data[$k]);
			}
		}
		return $tree;
	}


	function saveCollection(){
		$username = $_SESSION["user_info"]["username"];
		$id = $_REQUEST["id"];
		$operation_type = $_REQUEST["operation_type"];
		$mod = M("daily_operating_record");
		$arrData = array(
			"create_time"=>date("Y-m-d H:i:s"),
			"create_user"=>$username,
			"daily_id"=>$id,
			"operation_type"=>$operation_type,
		);
		if($operation_type == "1"){
			$msg = '收藏成功！';
		}elseif($operation_type == "2"){
			$msg = '点赞成功！';
		}elseif($operation_type == "3"){
			$msg = '评论成功！';
			$arrData["comment_content"] = $_REQUEST["comment_content"];
		}else{
			$msg = '...';
		}
		$result = $mod->data($arrData)->add();
		if ($result){
			$arrF = $mod->where("daily_id = '$id' AND operation_type = '$operation_type'")->select();
			$count = $mod->where("daily_id = '$id' AND operation_type = '$operation_type'")->count();
			$arrT = array();
			foreach($arrF as $val){
				$arrT[] = $val["create_user"];
			}
			$arrT2 = array_unique($arrT);
			$collection_user = implode(",",$arrT2);
			$daily_content = M("daily_content");
			if($operation_type == "1"){
				$arr = array(
					"collection_user"=>$collection_user,
					"collection_num"=>$count,
				);
			}elseif($operation_type == "2"){
				$arr = array(
					"thumb_up_user"=>$collection_user,
					"thumb_up_num"=>$count,
				);
			}elseif($operation_type == "3"){
				$arr = array(
					"review_user"=>$collection_user,
					"review_num"=>$count,
				);
			}else{
				$msg = '...';
			}
			$daily_content->data($arr)->where("id = '$id'")->save();
			echo json_encode(array('success'=>true,'msg'=>$msg));
		} else {
			echo json_encode(array('msg'=>'收藏失败！'));
		}
	}

	function saveShare(){
		$id = $_REQUEST['id'];
		$mod = M("daily_content");
		$arrData = array(
			'sharing_personnel'=>$_REQUEST['sharing_personnel'],
			'recipient_user'=>$_REQUEST['sharing_personnel'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"分享成功！"));
		} else {
			echo json_encode(array('msg'=>'分享失败！'));
		}
	}



	function annexData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$daily_id = $_REQUEST["daily_id"];
		$where = "1 ";
		$where .= empty($daily_id) ? "" : " AND daily_id = '$daily_id'";

		$mod = M("daily_annex");
		$count = $mod->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $mod->getLastSql

		$arrPdf = array("doc","docx","xls","xlsx","ppt","pptx");
		$arrVideo = array("mp4","webm","ogv");
		$arrAudio = array("mp3","wav");
		$arrPicture = array('jpg', 'gif', 'png', 'jpeg');
		foreach($arrData as &$val){
			$val["suffix"] = array_pop(explode(".",$val["file_path_name"]));
			$val["suffix_old"] = array_pop(explode(".",$val["file_path_name"]));
			if($val["suffix"] == "txt"){
				$val["file_content"] = file_get_contents($val["file_path_name"]);
				$val["file_content"] = str_replace("\r\n","<br/>",$val["file_content"]);
				if( mb_detect_encoding($val["file_content"],"UTF-8, ISO-8859-1, GBK") != "UTF-8" ){
					$val["file_content"] = gb2utf($val["file_content"]);
				}

			}else{
				$val["file_content"] = "";
			}

			if( in_array($val["suffix"],$arrPdf) ){
				$val["suffix"] = "word";
			}

			//视频
			if( in_array($val["suffix"],$arrVideo) ){
				$val["suffix"] = "video";
			}
			//音频
			if( in_array($val["suffix"],$arrAudio) ){
				$val["suffix"] = "audio";
			}

			//图片
			if( in_array($val["suffix"],$arrPicture) ){
				$val["suffix"] = "picture";
			}

			if($val["suffix_old"] == "mp4"){
				$val["media_type"] =  "m4v";
			}elseif($val["suffix_old"] == "webm"){
				$val["media_type"] =  "webmv";
			}elseif($val["suffix_old"] == "ogv"){
				$val["media_type"] =  "ogv";
			}elseif($val["suffix_old"] == "mp3"){
				$val["media_type"] =  "mp3";
			}elseif($val["suffix_old"] == "wav"){
				$val["media_type"] =  "wav";
			}else{
				$val["media_type"] =  "";
			}

			$val["operating"] = "<a href='index.php?m=DailyManagement&a=DownloadAnnex&filename=".$val['file_path_name']."'>下载</a>  &nbsp;&nbsp;&nbsp; <a href='javascript:void(0);' onclick=\"deleteAnnex("."'".$val["id"]."'".")\" >删除</a>  &nbsp;&nbsp;&nbsp; <a href='javascript:void(0);' onclick=\"viewAnnex("."'".$val["suffix"]."','".$val["suffix_old"]."','".$val["media_type"]."','".$val["file_path_name"]."','".$val["file_content"]."'".")\" >查看</a>";
		}
		//dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	//下载文件
	function DownloadAnnex(){
		$realfile = $_REQUEST["filename"];
		//echo $realfile;die;
		if(!file_exists($realfile)){
			echo goback("没有找到 $realfile 文件");
		}
        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}

	function saveAnnex(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$file_path = "include/data/daily/";
		mkdirs($file_path);
		$upload->savePath= $file_path;  //上传路径

		$upload->saveRule="uniqid";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖

		$username = $_SESSION['user_info']['username'];
		$daily_id = $_REQUEST["daily_id"];
		$daily_annex=new Model('daily_annex');

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData = array(
				"createtime" =>date("Y-m-d H:i:s"),
				"create_user" =>$username,
				"daily_id" =>$daily_id,
				"file_path_name" =>$info[0]["savepath"].$info[0]["savename"],
				"file_name" =>$info[0]["name"],
			);
			$result = $daily_annex->data($arrData)->add();
			if ($result){
				$faq_content = M("faq_content");
				$count = $daily_annex->where("daily_id = '$daily_id'")->count();
				$arr = array(
					"annex_num"=>$count,
				);
				$faq_content->data($arr)->where("id = '$daily_id'")->save();
				echo json_encode(array('success'=>true,'msg'=>'附件上传成功！'));
			} else {
				echo json_encode(array('msg'=>'附件上传失败！'));
			}
		}

	}

	function deleteAnnex(){
		$id = $_REQUEST["id"];
		$mod = M("daily_annex");
		$arrData = $mod->where("id = '$id'")->find();
		$result = $mod->where("id in ($id)")->delete();
		if ($result){
			unlink($arrData["file_path_name"]);
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function deleteDaily(){
		$id = $_REQUEST["id"];
		$mod = M("daily_content");
		$daily_annex = M("daily_annex");
		$arrData = $daily_annex->where("faq_id = '$id'")->select();
		$result = $mod->where("id in ($id)")->delete();
		$res = $daily_annex->where("faq_id in ($id)")->delete();
		if ($result){
			if($arrData){
				foreach($arrData as $val){
					unlink($val["file_path_name"]);
				}
			}
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function saveReview(){
		$username = $_SESSION["user_info"]["username"];
		$id = $_REQUEST["id"];
		$pid = $_REQUEST["pid"];
		$daily_id = $_REQUEST["daily_id"];
		$comment_content = $_REQUEST["comment_content"];
		$operation_type = $_REQUEST["operation_type"];

		$mod = M("daily_operating_record");
		$arrData = array(
			"create_time"=>date("Y-m-d H:i:s"),
			"create_user"=>$username,
			"daily_id"=>$daily_id,
			"pid"=>$id,
			"operation_type"=>$operation_type,
			"comment_content"=>$comment_content,
		);
		$result = $mod->data($arrData)->add();
		if ($result){
			$arrF = $mod->where("daily_id = '$daily_id' AND operation_type = '$operation_type'")->select();
			$count = $mod->where("daily_id = '$daily_id' AND operation_type = '$operation_type'")->count();
			$arrT = array();
			foreach($arrF as $val){
				$arrT[] = $val["create_user"];
			}
			$arrT2 = array_unique($arrT);
			$collection_user = implode(",",$arrT2);
			$faq_content = M("faq_content");
			if($operation_type == "1"){
				$arr = array(
					"collection_user"=>$collection_user,
					"collection_num"=>$count,
				);
			}elseif($operation_type == "2"){
				$arr = array(
					"thumb_up_user"=>$collection_user,
					"thumb_up_num"=>$count,
				);
			}elseif($operation_type == "3"){
				$arr = array(
					"review_user"=>$collection_user,
					"review_num"=>$count,
				);
				$msg = '回复成功！';
			}else{
				$msg = '...';
			}
			$faq_content->data($arr)->where("id = '$daily_id'")->save();
			//$sql = $faq_content->getLastSql();
			echo json_encode(array('success'=>true,'msg'=>$msg));
		} else {
			echo json_encode(array('msg'=>'收藏失败！'));
		}
	}

}
?>
