<?php
class FileInfoAction extends Action{
	//客户相关文件信息列表
	function fileInfoList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Related Documents";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$web_type = $_REQUEST["web_type"];
		if($web_type == "agent"){
			$this->assign("web_type","agent");
		}else{
			$this->assign("web_type","back");
		}

		$this->display();
	}

	function fileInfoData(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$filecontent = new Model("filecontent");

		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$name = $_REQUEST["name"];
		$phone1 = $_REQUEST["phone1"];
		$filetype_id = $_REQUEST["filetype_id"];
		$contract_number = $_REQUEST["contract_number"];
		$repayment_status = $_REQUEST["repayment_status"];
		$create_user = $_REQUEST["create_user"];
		$web_type = $_REQUEST["web_type"];

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND fc.createtime >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND fc.createtime <= '$end_time'";
		$where .= empty($name) ? "" : " AND c.name like '%$name%'";
		$where .= empty($phone1) ? "" : " AND c.phone1 like '%$phone1%'";
		$where .= empty($filetype_id) ? "" : " AND fc.filetype_id = '$filetype_id'";
		$where .= empty($contract_number) ? "" : " AND fc.contract_number like '%$contract_number%'";
		$where .= empty($repayment_status) ? "" : " AND fc.repayment_status = '$repayment_status'";
		$where .= empty($create_user) ? "" : " AND c.createuser = '$create_user'";

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		if($web_type == "agent"){
			if($username != "admin"){
				$where .= " AND (fc.createuser = '$username' OR c.createuser = '$username')";
			}
		}else{
			if($username != "admin"){
				$where .= " AND c.dept_id in ($deptSet)";
			}
		}

		$field = "fc.id,fc.createtime,fc.filetype_id,fc.customerid,fc.filecontentname,fc.filepath,fc.description,fc.createuser,fc.contract_number,fc.contract_amount,fc.repayment_installments,fc.has_also_amount,fc.repay_plan_date,fc.repayment_status,c.name,c.phone1,c.company,c.createuser as create_user,c.dept_id,ft.filename";

		$count = $filecontent->table("filecontent fc")->field($field)->join("customer c on (fc.customerid = c.id)")->join("filetype ft on (fc.filetype_id = ft.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $filecontent->order("fc.createtime desc")->table("filecontent fc")->field($field)->join("customer c on (fc.customerid = c.id)")->join("filetype ft on (fc.filetype_id = ft.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["remaining_amount"] = $val["contract_amount"] - $val["has_also_amount"];
			if($val["repay_plan_date"] == "0000-00-00"){
				$val["repay_plan_date"] = "";
			}

			$val["operating"] = "<a href='agent.php?m=CustomerData&a=downloadFile&path=".$val["filepath"]."&filename=".$val["filecontentname"]."' >下载</a> &nbsp;&nbsp;&nbsp;";

			if($val["filetype_id"] == "1"){
				$status = $val["contract_amount"]-$val["has_also_amount"];
				if($web_type == "agent"){
					$val["operating"] .=  "<a href='javascript:void(0);' onclick=\"fileInfoAgent("."'".$val["id"]."','".$val["customerid"]."','".$val["contract_number"]."'".")\" >付款信息</a>";
				}else{
					$val["operating"] .=  "<a href='javascript:void(0);' onclick=\"fileInfoBack("."'".$val["id"]."','".$val["customerid"]."','".$val["contract_number"]."'".")\" >付款信息</a>";
				}
			}
			$val["createuser"] = $val["createuser"]."  - ".$cnName[$val["createuser"]];
			$val["create_user"] = $val["create_user"]."  - ".$cnName[$val["create_user"]];

			$arr_contract_amount[] = $val["contract_amount"];
			$arr_has_also_amount[] = $val["has_also_amount"];
			$arr_remaining_amount[] = $val["remaining_amount"];

		}

		//echo $filecontent->getLastSql();die;
		//dump($arrData);die;


		$arrFooter = array(array(
			"repayment_status" => "本页总计",
			"contract_amount" => array_sum($arr_contract_amount),
			"has_also_amount" => array_sum($arr_has_also_amount),
			"remaining_amount" => array_sum($arr_remaining_amount),
		));

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter;

		echo json_encode($arrT);
	}


    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}


	function exportFileData(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$filecontent = new Model("filecontent");

		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$name = $_REQUEST["name"];
		$phone1 = $_REQUEST["phone1"];
		$filetype_id = $_REQUEST["filetype_id"];
		$contract_number = $_REQUEST["contract_number"];
		$repayment_status = $_REQUEST["repayment_status"];
		$create_user = $_REQUEST["create_user"];
		$web_type = $_REQUEST["web_type"];

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND fc.createtime >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND fc.createtime <= '$end_time'";
		$where .= empty($name) ? "" : " AND c.name like '%$name%'";
		$where .= empty($phone1) ? "" : " AND c.phone1 like '%$phone1%'";
		$where .= empty($filetype_id) ? "" : " AND fc.filetype_id = '$filetype_id'";
		$where .= empty($contract_number) ? "" : " AND fc.contract_number like '%$contract_number%'";
		$where .= empty($repayment_status) ? "" : " AND fc.repayment_status = '$repayment_status'";
		$where .= empty($create_user) ? "" : " AND c.createuser = '$create_user'";

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		if($web_type == "agent"){
			if($username != "admin"){
				$where .= " AND (fc.createuser = '$username' OR c.createuser = '$username')";
			}
		}else{
			if($username != "admin"){
				$where .= " AND c.dept_id in ($deptSet)";
			}
		}

		$field = "fc.id,fc.createtime,fc.filetype_id,fc.customerid,fc.filecontentname,fc.filepath,fc.description,fc.createuser,fc.contract_number,fc.contract_amount,fc.repayment_installments,fc.has_also_amount,fc.repay_plan_date,fc.repayment_status,c.name,c.phone1,c.company,c.createuser as create_user,c.dept_id,ft.filename";

		$arrData = $filecontent->order("fc.createtime desc")->table("filecontent fc")->field($field)->join("customer c on (fc.customerid = c.id)")->join("filetype ft on (fc.filetype_id = ft.id)")->where($where)->select();

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["remaining_amount"] = $val["contract_amount"] - $val["has_also_amount"];

			$val["createuser"] = $val["createuser"]."  - ".$cnName[$val["createuser"]];
			$val["create_user"] = $val["create_user"]."  - ".$cnName[$val["create_user"]];
		}

		$field = array("createtime","name","phone1","company","createuser","create_user","filename","contract_number","repayment_status","contract_amount","has_also_amount","remaining_amount","repayment_installments","repay_plan_date","description");
		$title = array("记录时间","客户名称","客户手机","客户公司名称","填写资料的坐席","所属业务员","文件类型","合同编号","付款状态","合同金额","已付金额","剩余金额","付款期数","计划还款日","备注");
		$count = count($field);
		$excelTiele = "客户相关文件".date("Y-m-d");

		$this->exportDataFunction($count,$field,$title,$arrData,$excelTiele);
	}




	function exportDataFunction($count,$field,$title,$arrData,$excelTiele){
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type:text/html;charset=UTF-8");
		$filename = iconv("utf-8","gb2312",$excelTiele);
        header("Content-Disposition: attachment;filename=$excelTiele.xls ");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();
        $start_row	=	0;

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
			xlsWriteLabel($start_row,$i,utf2gb($title[$i]));
		}

        $start_row++;
		foreach($arrData as &$val){
			for($j=0;$j<$count;$j++){
				 xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
			}
			$start_row++;
		}
        xlsEOF();
	}


	function payInfoList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Payment Information";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$web_type = $_REQUEST["web_type"];
		if($web_type == "agent"){
			$this->assign("web_type","agent");
		}else{
			$this->assign("web_type","back");
		}

		$this->display();
	}

	function payInfoData(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$customer_pay = new Model("customer_pay");

		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$name = $_REQUEST["name"];
		$phone1 = $_REQUEST["phone1"];
		$contract_number = $_REQUEST["contract_number"];
		$create_user = $_REQUEST["create_user"];
		$pay_start_time = $_REQUEST["pay_start_time"];
		$pay_end_time = $_REQUEST["pay_end_time"];
		$web_type = $_REQUEST["web_type"];

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND cp.createtime >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND cp.createtime <= '$end_time'";
		$where .= empty($pay_start_time) ? "" : " AND cp.pay_date >= '$pay_start_time'";
		$where .= empty($pay_end_time) ? "" : " AND cp.pay_date <= '$pay_end_time'";
		$where .= empty($name) ? "" : " AND c.name like '%$name%'";
		$where .= empty($phone1) ? "" : " AND c.phone1 like '%$phone1%'";
		$where .= empty($contract_number) ? "" : " AND fc.contract_number like '%$contract_number%'";
		$where .= empty($create_user) ? "" : " AND c.createuser = '$create_user'";

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);  //取上级部门
		$deptSet = rtrim($deptst,",");
		if($web_type == "agent"){
			if($username != "admin"){
				$where .= " AND (cp.create_name = '$username' OR c.createuser = '$username'  )";
			}
		}else{
			if($username != "admin"){
				$where .= " AND c.dept_id in ($deptSet)";
			}
		}

		$field = "cp.id,cp.customer_id,cp.filecontent_id,cp.create_name,cp.createtime,cp.pay_date,cp.payment_types,cp.payment_process,cp.repayment_times,cp.repayment_amount,cp.remark,fc.id,fc.createtime as fc_createtime,fc.filetype_id,fc.filecontentname,fc.filepath,fc.description,fc.createuser,fc.contract_number,fc.contract_amount,fc.repayment_installments,fc.has_also_amount,fc.repay_plan_date,fc.repayment_status,c.name,c.phone1,c.company,c.createuser as create_user,c.dept_id";

		$count = $customer_pay->table("customer_pay cp")->field($field)->join("customer c on (cp.customer_id = c.id)")->join("filecontent fc on (cp.filecontent_id = fc.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $customer_pay->order("cp.createtime desc")->table("customer_pay cp")->field($field)->join("customer c on (cp.customer_id = c.id)")->join("filecontent fc on (cp.filecontent_id = fc.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();


		$filecontent = new Model("filecontent");
		$arrF = $filecontent->field("id,contract_amount,has_also_amount")->where("filetype_id = '1'")->select();
		foreach($arrF as &$val){
			$val["remaining_amount"] = $val["contract_amount"]-$val["has_also_amount"];
			$arrFID[$val["id"]] = $val["remaining_amount"];
		}


		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$arr_repayment_amount[] = $val["repayment_amount"];
			$val["create_name"] = $val["create_name"]."  - ".$cnName[$val["create_name"]];
			$val["create_user"] = $val["create_user"]."  - ".$cnName[$val["create_user"]];
			$val["remaining_amount"] = $arrFID[$val["filecontent_id"]];
		}
		$arrFooter = array(array(
			"repayment_times" => "本页总计:",
			"repayment_amount" => array_sum($arr_repayment_amount),
		));


		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter;

		echo json_encode($arrT);
	}


	function exportPayData(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$customer_pay = new Model("customer_pay");

		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$name = $_REQUEST["name"];
		$phone1 = $_REQUEST["phone1"];
		$contract_number = $_REQUEST["contract_number"];
		$create_user = $_REQUEST["create_user"];
		$pay_start_time = $_REQUEST["pay_start_time"];
		$pay_end_time = $_REQUEST["pay_end_time"];
		$web_type = $_REQUEST["web_type"];

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND cp.createtime >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND cp.createtime <= '$end_time'";
		$where .= empty($pay_start_time) ? "" : " AND cp.pay_date >= '$pay_start_time'";
		$where .= empty($pay_end_time) ? "" : " AND cp.pay_date <= '$pay_end_time'";
		$where .= empty($name) ? "" : " AND c.name like '%$name%'";
		$where .= empty($phone1) ? "" : " AND c.phone1 like '%$phone1%'";
		$where .= empty($contract_number) ? "" : " AND fc.contract_number like '%$contract_number%'";
		$where .= empty($create_user) ? "" : " AND c.createuser = '$create_user'";

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);  //取上级部门
		$deptSet = rtrim($deptst,",");
		if($web_type == "agent"){
			if($username != "admin"){
				$where .= " AND (cp.create_name = '$username' OR c.createuser = '$username'  )";
			}
		}else{
			if($username != "admin"){
				$where .= " AND c.dept_id in ($deptSet)";
			}
		}

		$field = "cp.id,cp.customer_id,cp.create_name,cp.createtime,cp.pay_date,cp.payment_types,cp.payment_process,cp.repayment_times,cp.repayment_amount,cp.remark,fc.id,fc.createtime as fc_createtime,fc.filetype_id,fc.filecontentname,fc.filepath,fc.description,fc.createuser,fc.contract_number,fc.contract_amount,fc.repayment_installments,fc.has_also_amount,fc.repay_plan_date,fc.repayment_status,c.name,c.phone1,c.company,c.createuser as create_user,c.dept_id";

		$arrData = $customer_pay->order("cp.createtime desc")->table("customer_pay cp")->field($field)->join("customer c on (cp.customer_id = c.id)")->join("filecontent fc on (cp.filecontent_id = fc.id)")->where($where)->select();

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$arr_repayment_amount[] = $val["repayment_amount"];
			$val["create_name"] = $val["create_name"]."  - ".$cnName[$val["create_name"]];
			$val["create_user"] = $val["create_user"]."  - ".$cnName[$val["create_user"]];
		}

		$field = array("createtime","pay_date","name","phone1","company","create_user","payment_types","contract_number","repayment_amount","remark");
		$title = array("创建时间","付款日期","客户名称","客户手机","客户公司名称","所属业务员","付款类型","合同编号","此次付款金额","付款备注");
		$count = count($field);
		$excelTiele = "客户相关文件".date("Y-m-d");

		$this->exportDataFunction($count,$field,$title,$arrData,$excelTiele);

	}


}
?>
