<?php
class TimeGroupsAction extends Action{
	function timegroupsList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Time Groups";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function timeGroupsData(){
		$timegroup = new Model("asterisk.timegroups_groups");
		$count = $timegroup->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$TGData = $timegroup->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($TGData) ? $TGData : false;
		$arrTG["total"] = $count;
		$arrTG["rows"] = $rowsList;

		echo json_encode($arrTG);
	}

	function addTimeGroup(){
		checkLogin();
		$year_month = date("Y-m");
		$timestamp = strtotime($year_month . "-10 00:00:00");
		$day = date('t',$timestamp);
		$days = $day+1;
		$this->assign("days",$days);
		$this->display();
	}

	function insertTimeGroup(){
		$timegroup = new Model("asterisk.timegroups_groups");
		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$res = $timegroup ->add(array("description"=>$_REQUEST['description']));

		$start_time = $_REQUEST['start_time'];
		$end_time = $_REQUEST['end_time'];
		$week_start = $_REQUEST['week_start'];
		$end_week = $_REQUEST['end_week'];
		$start_day = $_REQUEST['start_day'];
		$end_day = $_REQUEST['end_day'];
		$start_month = $_REQUEST['start_month'];
		$end_month = $_REQUEST['end_month'];

		$time = $this->timesJudge($start_time,$end_time);
		$week_time = $this->timesJudge($week_start,$end_week);
		$day_time = $this->timesJudge($start_day,$end_day);
		$month_time = $this->timesJudge($start_month,$end_month);

		$time_group = $time."|".$week_time."|".$day_time."|".$month_time;

		$timegroups_details = new Model("asterisk.timegroups_details");
		$arrData = array(
			"time"=>$time_group,
			"timegroupid"=>$res,
		);
		$res2 = $timegroups_details->data($arrData)->add();
		if( $res && $res2){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function timesJudge($a,$b){
		if( !empty($a) && !empty($b) ){
			$times = $a."-".$b;
		}elseif( empty($a) && !empty($b) ){
			$times = $b;
		}elseif( !empty($a) && empty($b) ){
			$times = $a;
		}else{
			$times = "*";
		}
		return $times;
	}

	function editTimeGroup(){
		checkLogin();
		$id = $_REQUEST["id"];
		if($id){
			$this->assign("id",$id);
			$timegroup = new Model("asterisk.timegroups_groups");
			mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
			$tglist = $timegroup->where("id = '$id'")->find();
			$this->assign("tglist",$tglist);

			$timegroups_details = new Model("asterisk.timegroups_details");
			$tdlist = $timegroups_details->where("timegroupid = '$id'")->select();
			foreach($tdlist as $val){
				$time[] = explode("|",$val['time']);
			}
			foreach($time as $v){
				$times[] = explode("-",$v[0]);
				$week[] = explode("-",$v[1]);
				$day[] = explode("-",$v[2]);
				$month[] = explode("-",$v[3]);
			}
			$tm = $this->arrAdditional($times,"start_time","end_time");
			$weeks = $this->arrAdditional($week,"start_week","end_week");
			$days = $this->arrAdditional($day,"start_day","end_day");
			$months = $this->arrAdditional($month,"start_month","end_month");

			//将一个数组附加到另一个数组的子数组里面
			foreach($tdlist as $key=>$value) {
				foreach($value as $k=>$v) {
				  $tm[$key][$k] = $v;
				}
			}
			foreach($tm as $key=>$value) {
				foreach($value as $k=>$v) {
				  $weeks[$key][$k] = $v;
				}
			}
			foreach($weeks as $key=>$value) {
				foreach($value as $k=>$v) {
				  $days[$key][$k] = $v;
				}
			}
			foreach($days as $key=>$value) {
				foreach($value as $k=>$v) {
				  $months[$key][$k] = $v;
				}
			}
			$this->assign("tdlist",$months);
		}
		$year_month = date("Y-m");
		$timestamp = strtotime($year_month . "-10 00:00:00");
		$day1 = date('t',$timestamp);
		$days1 = $day1+1;
		$this->assign("days",$days1);
		$this->display();
	}

	function arrAdditional($times,$start,$end){
		$i = 0;
		foreach($times as $t){
			$times[$i][$start] = $t[0];
			if($times[$i][0] !== "*"){
				if($times[$i][1] == ""){
					$times[$i][$end] = $t[0];
				}else{
					$times[$i][$end] = $t[1];
				}
			}else{
				$times[$i][$start] = "-";
				$times[$i][$end] = "-";
			}
			$i++;
		}
		return $times;
	}

	function updateTimeGroup(){
		$id = $_REQUEST["id"];
		$tgd_id = $_REQUEST["tgd_id"];
		$timegroup = new Model("asterisk.timegroups_groups");
		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$res = $timegroup->where("id = '$id'")->save(array("description"=>$_REQUEST['description']));

		$timegroups_details = new Model("asterisk.timegroups_details");
		$tdlist = $timegroups_details->where("timegroupid = '$id'")->select();
		foreach($tdlist as $val){
			$tgd_id[] = $val['id'];
		}
		$count = count($tgd_id);
		for($i=0;$i<$count;$i++){
			$start_time[$tgd_id[$i]] = $_REQUEST['start_time'.$tgd_id[$i]];
			$end_time[$tgd_id[$i]] = $_REQUEST['end_time'.$tgd_id[$i]];
			$week_start[$tgd_id[$i]] = $_REQUEST['week_start'.$tgd_id[$i]];
			$end_week[$tgd_id[$i]] = $_REQUEST['end_week'.$tgd_id[$i]];
			$start_day[$tgd_id[$i]] = $_REQUEST['start_day'.$tgd_id[$i]];
			$end_day[$tgd_id[$i]] = $_REQUEST['end_day'.$tgd_id[$i]];
			$start_month[$tgd_id[$i]] = $_REQUEST['start_month'.$tgd_id[$i]];
			$end_month[$tgd_id[$i]] = $_REQUEST['end_month'.$tgd_id[$i]];

			$time[$tgd_id[$i]] = $this->arrTimesJudge($start_time[$tgd_id[$i]],$end_time[$tgd_id[$i]]);
			$week_time[$tgd_id[$i]] = $this->arrTimesJudge($week_start[$tgd_id[$i]],$end_week[$tgd_id[$i]]);
			$day_time[$tgd_id[$i]] = $this->timesJudge($start_day[$tgd_id[$i]],$end_day[$tgd_id[$i]]);
			$month_time[$tgd_id[$i]] = $this->timesJudge($start_month[$tgd_id[$i]],$end_month[$tgd_id[$i]]);

			$time_group[$tgd_id[$i]] = $time[$tgd_id[$i]]."|".$week_time[$tgd_id[$i]]."|".$day_time[$tgd_id[$i]]."|".$month_time[$tgd_id[$i]];
			$arrData[$tgd_id[$i]] = array(
				"time"=>$time_group[$tgd_id[$i]],
			);
			$result[$tgd_id[$i]] = $timegroups_details->where("id = '$tgd_id[$i]'")->save(array("time"=>$time_group[$tgd_id[$i]]));
		}

		if( $res !== false){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function arrTimesJudge($a,$b){
		//dump($a);
		if($a !== $b ){
			if( $a !== "-" && $b !== "-" ){
				$times = $a."-".$b;
			}elseif( $a == "-" && $b !== "-" ){
				$times = $b;
			}elseif(  $a !== "-" && $b == "-" ){
				$times = $a;
			}else{
				$times = "*";
			}
		}else{
			if( $a == "-" && $b =="-"){
				$times = "*";
			}else{
				$times = $a;
			}
		}
		//dump($times);die;
		return $times;
	}

	function saveTimeGroup(){
		$id = $_REQUEST['id'];
		$start_time = $_REQUEST['start_time'];
		$end_time = $_REQUEST['end_time'];
		$week_start = $_REQUEST['week_start'];
		$end_week = $_REQUEST['end_week'];
		$start_day = $_REQUEST['start_day'];
		$end_day = $_REQUEST['end_day'];
		$start_month = $_REQUEST['start_month'];
		$end_month = $_REQUEST['end_month'];

		$time = $this->timesJudge($start_time,$end_time);
		$week_time = $this->timesJudge($week_start,$end_week);
		$day_time = $this->timesJudge($start_day,$end_day);
		$month_time = $this->timesJudge($start_month,$end_month);

		//dump($time);die;
		$time_group = $time."|".$week_time."|".$day_time."|".$month_time;
		//dump($time_group);die;
		$timegroups_details = new Model("asterisk.timegroups_details");
		$arrData = array(
			"time"=>$time_group,
			"timegroupid"=>$id,
		);
		//dump($arrData);die;
		$res2 = $timegroups_details->data($arrData)->add();
		if( $res2 ){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function deleteTimeGroups(){
		$id = $_REQUEST['id'];
		$timegroup = new Model("asterisk.timegroups_groups");
		$res = $timegroup->where("id in ($id)")->delete();
		$timegroups_details = new Model("asterisk.timegroups_details");
		$res2 = $timegroups_details->where("timegroupid in ($id)")->delete();
		if ($res){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}
}

?>
