<?php
class CustomerSystemAction extends Action{
	function customerList(){
		checkLogin();
		$users= new Model("users");
		$usersList = $users->field("username,extension,d_id,r_id,cn_name,en_name,email,fax,extension_type,extension_mac,phone,out_pin,routeid")->select();
		$this->assign("usersList",$usersList);
		//分配增删改的权限
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['list_enabled'] == "Y" ){
				if($val['en_name'] != "createuser" ){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					//$arrField[0][$i]['fitColumns'] = true;
					$arrField[0][$i]['width'] = "100";
				}
				/*
				if( $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
				*/
				if( $val['tiptools_enabled'] == 'Y' ||  $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}
		/*
		$arrFd = array("field"=>"createtime","title"=>"创建时间","width"=>"120");
		$arrFd2 = array("field"=>"ck","checkbox"=>true);
		$arrFd3 = array("field"=>"createuser","title"=>"创建人工号","width"=>"100");
		$arrFd4 = array("field"=>"cn_name","title"=>"创建人姓名","width"=>"100");
		$arrFd5 = array("field"=>"d_name","title"=>"所属部门","width"=>"100");
		//$arrFd6 = array("field"=>"share_deptid","title"=>"共享部门","width"=>"100");
		$arrFd7 = array("field"=>"dealuser","title"=>"共享坐席","width"=>"100");
		array_unshift($arrField[0],$arrFd7);
		//array_unshift($arrField[0],$arrFd6);
		array_unshift($arrField[0],$arrFd5);
		array_unshift($arrField[0],$arrFd4);
		array_unshift($arrField[0],$arrFd3);
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd2);
		*/
		$arrFd = array("field"=>"dealuser","title"=>"共享坐席","width"=>"120");
		array_unshift($arrField[0],$arrFd);

		$arrFrozen = array(
						array("field"=>"ck","checkbox"=>true),
						array("field"=>"createtime","title"=>"创建时间","width"=>"120"),
						array("field"=>"createuser","title"=>"创建人工号","width"=>"100"),
						array("field"=>"cn_name","title"=>"创建人姓名","width"=>"100"),
						array("field"=>"d_name","title"=>"所属部门","width"=>"100"),
					);

		//dump($fielddTpl);die;
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("arrFrozen",json_encode($arrFrozen));
		$this->assign("fielddTpl",$fielddTpl);

		$areaTpl = implode(",",$areaTpl2);
		$this->assign("areaTpl",$areaTpl);

		$para_sys = readS();
		$export_rule = $para_sys["export_rule"];
		$this->assign("export_rule",$export_rule);

		$this->display();
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	function customerDataList(){
		$para_sys = readS();
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$visit_starttime = $_REQUEST['visit_starttime'];
		$visit_endtime = $_REQUEST['visit_endtime'];
		$trans = $_REQUEST['trans'];
		$deptId = $_REQUEST['dept_id'];

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$deptSet = rtrim($deptst,",");

		$deptst2 = $this->getMeAndSubDeptName($arrDep,$deptId);
		$deptSet2 = rtrim($deptst2,",");

		$where = "(1 ";
		$where .= " AND c.recycle = 'N'";
		$where .= empty($deptId)?"":" AND u.d_id in ($deptSet2)";
		$where .= empty($startime)?"":" AND c.createtime >'$startime'";
        $where .= empty($endtime)?"":" AND c.createtime <'$endtime'";
		$where .= empty($visit_starttime)?"":" AND c.recently_visittime >'$visit_starttime'";
        $where .= empty($visit_endtime)?"":" AND c.recently_visittime <'$visit_endtime'";

		//dump($_REQUEST);die;


		if($para_sys["equal_all"] == "back" || $para_sys["equal_all"] == "all"){
			$arr = $_REQUEST;
			foreach($arr as $k=>$v){
				if($v && $k != "page"  && $k != "rows"  && $k != "searchmethod"  && $k != "trans" ){
					$arrS[$k] = $v;
				}
			}
			//精确查询可以查所有的客户资料
			if($_REQUEST['searchmethod']){
				if($username == "admin" ){
					$where .= " AND 1)";
				}else{
					if( $searchmethod == "equal" && $arrS){
						$where .= " AND 1)";
					}else{
						$where .= "  AND 1)  AND u.d_id in ($deptSet)";
					}
				}
			}else{
				if($username != "admin"){
					$where .= " AND 1)  AND u.d_id in ($deptSet)";
				}else{
					$where .= " AND 1)";
				}
			}
		}else{
			if($username != "admin"){
				$where .= " AND 1)  AND u.d_id in ($deptSet)";
			}else{
				$where .= " AND 1)";
			}
		}


		//备选字段
		if($para_sys["hide_field"]=='yes'){
			if(!$_REQUEST['searchmethod']){
				$where .= " AND c.hide_fields='Y'";
			}else{
				if($searchmethod != "equal"){
					$where .= " AND hide_fields='Y'";
				}
			}
		}


		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){    //&& $val['text_type'] != '3'
				$arrF[] = "c.".$val['en_name'];
				//$$val['en_name'] = $_REQUEST[$val['en_name']];

				if( $val['text_type'] == '3'){
					$areaTpl[$val["en_name"]] = $val["en_name"];
				}elseif($val['text_type'] == '4'){
					$start = $val['en_name']."_start";
					$$start = $_REQUEST[$start];
					$end = $val['en_name']."_end";
					$$end = $_REQUEST[$end];
				}else{
					$$val['en_name'] = $_REQUEST[$val['en_name']];
				}
			}
		}
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3'){
				$aa[] = $vm['en_name'];
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					if($vm['text_type'] == '2'){
						$where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." = '".$$vm['en_name']."'";
					}else{
						$where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
					}
				}

				if($vm['text_type'] == '4'){
					$start = $vm['en_name']."_start";
					$end = $vm['en_name']."_end";
					 $where .= empty($$start)?"":" AND ".$vm['en_name'] ." >= '".$$start."'";
					 $where .= empty($$end)?"":" AND ".$vm['en_name'] ." <= '".$$end."'";
				}
			}
		}

		//dump($where);die;


		//匹配上次查询条件--start
		$search_last_sql = new Model("search_last_sql");
		if($where != "(1  AND c.recycle = 'N' AND 1) AND c.hide_fields='Y'"){
			$search_res = $search_last_sql->where("table_name = 'customer'")->save( array('last_sql'=>$where) );
		}
		$search_data = $search_last_sql->where("table_name = 'customer'")->find();
		if($_REQUEST['search'] == "lastTime"){
			$where = $search_data['last_sql'];
		}else{
			$where = $where;
		}
		//匹配上次查询条件--end

		$customer = new Model("customer");
		import('ORG.Util.Page');

		if($username == "admin"){
			$count = $customer->table("customer c")->join("users u on (c.createuser = u.username)")->where($where)->count();    //可以看到要转交的资料

		}else{
			$count = $customer->table("customer c")->join("users u on (c.createuser = u.username)")->where($where)->count();
		}

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		//$fields = "`c.id`,`c.createtime`,"."`".implode("`,`",$arrF)."`";
		$fields = "c.id,c.dealuser,c.share_deptid,c.createtime,".implode(",",$arrF).",u.cn_name";
		if($username == "admin"){
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		}else{
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}
		$sqlSearch = $customer->getLastSql();
		//dump($cmlist);die;
		//echo $customer->getLastSql();die;

		$userArr = readU();
		//$cnName = $userArr["cn_user"];
		//$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;

					$vm['d_name'] = $deptName_user[$vm["createuser"]];
				}
			}
		}
		//dump($cmlist);die;
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$userArr = readU();
		$cn_user = $userArr['cn_user'];
		$user_work = array_flip($cn_user);


		$i = 0;
		//dump($areaTpl);die;
		foreach($cmlist as &$vm){
			$vm['customer_service'] = $cn_user[$vm['customer_service']];
			$cmlist[$i]["massPhone"] = $vm['phone1'];
			$cmlist[$i]["massEmail"] = $vm['email'];
			$cmlist[$i]["massPhone2"] = $vm['phone2'];
			$cmlist[$i]["massQQ"] = $vm['qq_number'];
			$cmlist[$i]["massBank"] = $vm['bank_account'];
			/*
			foreach($areaTpl as $val){
				$cmlist[$i][$val."s1"] = $vm[$val];
				if(mb_strlen($vm[$val],'utf-8') >15){
					$cmlist[$i][$val] = mb_substr($vm[$val],0,15,'utf-8')."...";
				}else{
					$cmlist[$i][$val] = $vm[$val];
				}
			}
			*/

			if($para_sys['hide_rule'] == "role"){
				if($username != "admin"){
					if($priv["hide_phone"] =="Y"){
						$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
						if($vm["phone2"]){
							$cmlist[$i]["phone2"] = substr($vm["phone2"],0,3)."***".substr($vm["phone2"],-4);
						}
					}
					if($vm['email']){
						if($priv["hide_email"] =="Y"){
							$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
						}
					}
					if($vm['qq_number']){
						if($priv["hide_qq"] =="Y"){
							$cmlist[$i]["qq_number"] = "***".substr($vm["qq_number"],3);
						}
					}
				}
			}else{
				if($username != "admin"){
					if($para_sys["hide_phone"] =="yes"){
						$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
						if($vm["phone2"]){
							$cmlist[$i]["phone2"] = substr($vm["phone2"],0,3)."***".substr($vm["phone2"],-4);
						}
					}
					if($vm['email']){
						if($para_sys["hide_email"] =="yes"){
							$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
						}
					}
					if($vm['qq_number']){
						if($para_sys["hide_qq"] =="yes"){
							$cmlist[$i]["qq_number"] = "***".substr($vm["qq_number"],3);
						}
					}
				}
			}
			/*
			*/

			$i++;
		}
		unset($i);


		//dump($cmlist);die;
		$rowsList = count($cmlist) ? $cmlist : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["sears"] = base64_encode($sqlSearch);

		echo json_encode($ary);
	}


	function outboundList(){
		checkLogin();
		$users= new Model("users");
		$usersList = $users->field("username,extension,d_id,r_id,cn_name,en_name,email,fax,extension_type,extension_mac,phone,out_pin,routeid")->select();
		$this->assign("usersList",$usersList);
		//分配增删改的权限
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['list_enabled'] == "Y" ){
				if($val['en_name'] != "createuser" ){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					//$arrField[0][$i]['fitColumns'] = true;
					$arrField[0][$i]['width'] = "100";
				}
				/*
				if( $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}*/
				if( $val['tiptools_enabled'] == 'Y' ||  $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}

		$arrFd = array("field"=>"createtime","title"=>"创建时间","width"=>"120");
		$arrFd2 = array("field"=>"ck","checkbox"=>true);
		$arrFd3 = array("field"=>"createuser","title"=>"创建人工号","width"=>"100");
		$arrFd4 = array("field"=>"cn_name","title"=>"创建人姓名","width"=>"100");
		$arrFd5 = array("field"=>"d_name","title"=>"所属部门","width"=>"100");
		array_unshift($arrField[0],$arrFd5);
		array_unshift($arrField[0],$arrFd4);
		array_unshift($arrField[0],$arrFd3);
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd2);
		//dump($fielddTpl);die;
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("fielddTpl",$fielddTpl);
		$areaTpl = implode(",",$areaTpl2);
		$this->assign("areaTpl",$areaTpl);

		$this->display();
	}


	function customerDataTrans(){
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$trans = $_REQUEST['trans'];
		$deptId = $_REQUEST['dept_id'];

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$deptSet = rtrim($deptst,",");

		$deptst2 = $this->getMeAndSubDeptName($arrDep,$deptId);
		$deptSet2 = rtrim($deptst2,",");

		$where = "(1 ";
		$where .= " AND c.recycle = 'N'";
		$where .= empty($deptId)?"":" AND u.d_id in ($deptSet2)";
		$where .= empty($startime)?"":" AND c.createtime >'$startime'";
        $where .= empty($endtime)?"":" AND c.createtime <'$endtime'";
		if($username != "admin"){
			$where .= " AND 1)  AND u.d_id in ($deptSet)";
		}else{
			$where .= " AND 1)";
		}
		//dump($_REQUEST);die;

		/*
		//精确查询可以查所有的客户资料
		if($_REQUEST['searchmethod']){
			if($username == "admin" ){
				$where .= " AND 1)";
			}else{
				if( $searchmethod == "equal"){
					$where .= " AND 1)";
				}else{
					$where .= "  AND 1)  AND u.d_id in ($deptSet)";
				}
			}
		}else{
			if($username != "admin"){
				$where .= " AND 1)  AND u.d_id in ($deptSet)";
			}else{
				$where .= " AND 1)";
			}
		}*/


		//备选字段
		$para_sys = readS();
		if($para_sys["hide_field"]=='yes'){
			if(!$_REQUEST['searchmethod']){
				$where .= " AND c.hide_fields='Y'";
			}else{
				if($searchmethod != "equal"){
					$where .= " AND hide_fields='Y'";
				}
			}
		}


		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y' ){   //&& $val['text_type'] != '3'
				$arrF[] = "c.".$val['en_name'];
				$$val['en_name'] = $_REQUEST[$val['en_name']];
				if( $val['text_type'] == '3'){
					$areaTpl[$val["en_name"]] = $val["en_name"];
				}
			}
		}
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3'){
				$aa[] = $vm['en_name'];
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					if($vm['text_type'] == '2'){
						$where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." = '".$$vm['en_name']."'";
					}else{
						$where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
					}
				}
			}
		}
		$where .= " AND (customer_source = 'autocall' OR customer_source = 'incall' ) AND autocall_visit = 'N'";
		//dump($where);die;


		//匹配上次查询条件--start
		$search_last_sql = new Model("search_last_sql");
		if($where != "1 "){
			$search_res = $search_last_sql->where("table_name = 'customer'")->save( array('last_sql'=>$where) );
		}
		$search_data = $search_last_sql->where("table_name = 'customer'")->find();
		if($_REQUEST['search'] == "lastTime"){
			$where = $search_data['last_sql'];
		}else{
			$where = $where;
		}
		//匹配上次查询条件--end

		$customer = new Model("customer");
		import('ORG.Util.Page');

		if($username == "admin"){
			$count = $customer->table("customer c")->join("users u on (c.createuser = u.username)")->where($where)->count();    //可以看到要转交的资料

		}else{
			$count = $customer->table("customer c")->join("users u on (c.createuser = u.username)")->where($where)->count();
		}

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		//$fields = "`c.id`,`c.createtime`,"."`".implode("`,`",$arrF)."`";
		$fields = "c.id,c.createtime,".implode(",",$arrF).",u.cn_name";
		if($username == "admin"){
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		}else{
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}
		//dump($cmlist);die;
		//dump($customer->getLastSql());die;

		$userArr = readU();
		//$cnName = $userArr["cn_user"];
		//$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;

					$vm['d_name'] = $deptName_user[$vm["createuser"]];
				}
			}
		}
		//dump($cmlist);die;
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$userArr = readU();
		$cn_user = $userArr['cn_user'];
		$user_work = array_flip($cn_user);

		$para_sys = readS();
		$i = 0;
		foreach($cmlist as &$vm){
			$vm['customer_service'] = $cn_user[$vm['customer_service']];
			$cmlist[$i]["massPhone"] = $vm['phone1'];
			$cmlist[$i]["massEmail"] = $vm['email'];
			$cmlist[$i]["massQQ"] = $vm['qq_number'];
			$cmlist[$i]["massBank"] = $vm['bank_account'];
			/*
			foreach($areaTpl as $val){
				$cmlist[$i][$val."s1"] = $vm[$val];
				if(mb_strlen($vm[$val],'utf-8') >15){
					$cmlist[$i][$val] = mb_substr($vm[$val],0,15,'utf-8')."...";
				}else{
					$cmlist[$i][$val] = $vm[$val];
				}
			}
			*/
			if($para_sys['hide_rule'] == "role"){
				if($username != "admin"){
					if($priv["hide_phone"] =="Y"){
						$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
					}
					if($vm['email']){
						if($priv["hide_email"] =="Y"){
							$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
						}
					}
					if($vm['qq_number']){
						if($priv["hide_qq"] =="Y"){
							$cmlist[$i]["qq_number"] = "***".substr($vm["qq_number"],3);
						}
					}
				}
			}else{
				if($username != "admin"){
					if($para_sys["hide_phone"] =="yes"){
						$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
					}
					if($vm['email']){
						if($para_sys["hide_email"] =="yes"){
							$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
						}
					}
					if($vm['qq_number']){
						if($para_sys["hide_qq"] =="yes"){
							$cmlist[$i]["qq_number"] = "***".substr($vm["qq_number"],3);
						}
					}
				}
			}
			/*
			*/

			$i++;
		}
		unset($i);


		//dump($cmlist);die;
		$rowsList = count($cmlist) ? $cmlist : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
	}



	function editCustomer(){
		checkLogin();
		$id = $_GET["id"];
		$username = $_SESSION["user_info"]["username"];
		$uniqueid = $_GET["uniqueid"];
		$phone_num = $_GET["phone_num"];

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');
		$para_sys = readS();

		foreach($fielddTpl2 as $val){
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields"){
					$fielddTpl[] = $val;
				}
			}else{
				if($val['field_enabled'] == 'Y'){
					$fielddTpl[] = $val;
				}
			}
		}
		//$fields = "`".implode("`,`",$arrF)."`".",`country`,`province`,`city`,`district`";
		$fields = "`".implode("`,`",$arrF)."`,openid";
		//dump($fielddTpl);die;
		$this->assign("fielddTpl",$fielddTpl);

		//dump($id);
		if($id){
			$this->assign("id",$id);
			$this->assign("uniqueid",$uniqueid);
			$this->assign("phone_num",$phone_num);
			$customer = new Model("customer");
			$custList = $customer->field($fields)->where("id = $id")->find();
			//dump($custList);die;

			$menuname = "Customer Data";
			$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
			$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
			$para_sys = readS();
			if($para_sys['hide_rule'] == "role"){
				if($priv["hide_phone"] =="Y"){
					$custList["phone"] = $custList["phone1"];
					if($username == "admin"){
						$custList["phone1"] = $custList["phone1"];
						$this->assign("message_phone","show_phone");
					}else{
						$custList["phone1"] = substr($custList["phone1"],0,3)."***".substr($custList["phone1"],-4);
						//$custList["phone1"] = $custList["phone1"];
						$this->assign("message_phone","ms");
					}
				}else{
					$custList["phone"] = $custList["phone1"];
					$this->assign("message_phone","show_phone");
				}
				if($custList['email']){
					if($priv["hide_email"] =="Y"){
						$custList["email2"] = $custList["email"];if($username == "admin"){
							$custList["email"] = $custList["email"];
							$this->assign("message_email","show_email");
						}else{
							$custList["email"] = "***".strstr($custList["email"],"@");
							//$custList["email"] = $custList["email"];
							$this->assign("message_email","ms");
						}
					}else{
						$custList["email2"] = $custList["email"];
						$this->assign("message_email","show_email");
					}
				}
			}else{
				if($para_sys["hide_phone"] =="yes"){
					$custList["phone"] = $custList["phone1"];
					if($username == "admin"){
						$custList["phone1"] = $custList["phone1"];
						$this->assign("message_phone","show_phone");
					}else{
						$custList["phone1"] = substr($custList["phone1"],0,3)."***".substr($custList["phone1"],-4);
						//$custList["phone1"] = $custList["phone1"];
						$this->assign("message_phone","ms");
					}
				}else{
					$custList["phone"] = $custList["phone1"];
					$this->assign("message_phone","show_phone");
				}
				if($custList['email']){
					if($para_sys["hide_email"] =="yes"){
						$custList["email2"] = $custList["email"];
						if($username == "admin"){
							$custList["email"] = $custList["email"];
							$this->assign("message_email","show_email");
						}else{
							$custList["email"] = "***".strstr($custList["email"],"@");
							//$custList["email"] = $custList["email"];
							$this->assign("message_email","ms");
						}
					}else{
						$custList["email2"] = $custList["email"];
						$this->assign("message_email","show_email");
					}
				}
			}

			//dump($custList);
			$this->assign("custList",$custList);

			$sears = $_GET["sears"];
			$this->assign("sears",$sears);

		}
		$this->display();
	}

	//编辑客户资料
	function updateCustomer(){
		$sms_cust = isset($_REQUEST['sms_cust']) ? "Y" : "N";

		$username = $_SESSION["user_info"]["username"];
		$id = $_POST["id"];
		//dump($id);
		$customer = new Model("customer");
		$cmFields = getFieldCache();
		$para_sys = readS();
		foreach($cmFields as $val){
			$tmp[$val['en_name']] = $val['cn_name'];
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields" && $val["en_name"] != "sms_cust"){
					if($val['en_name'] == 'phone1'){
						if($_REQUEST['message_phone'] == "ms"){
							$arrData['phone1'] = $_REQUEST['mess_phone'];
						}else{
							$arrData['phone1'] = $_REQUEST['phone1'];
						}
					}elseif($val['en_name'] == 'email'){
						if($_REQUEST['message_email'] == "ms"){
							$arrData['email'] = $_REQUEST['mess_email'];
						}else{
							$arrData['email'] = $_REQUEST['email'];
						}
					}else{
						$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
					}
				}
			}else{
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
					if($val['en_name'] == 'phone1'){
						if($_REQUEST['message_phone'] == "ms"){
							$arrData['phone1'] = $_REQUEST['mess_phone'];
						}else{
							$arrData['phone1'] = $_REQUEST['phone1'];
						}
					}elseif($val['en_name'] == 'email'){
						if($_REQUEST['message_email'] == "ms"){
							$arrData['email'] = $_REQUEST['mess_email'];
						}else{
							$arrData['email'] = $_REQUEST['email'];
						}
					}else{
						$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
					}
				}
			}
		}
		$openid = empty($_REQUEST["nickname"]) ? "" : $_REQUEST["openid"];
		$arrData["modifytime"] = date("Y-m-d H:i:s");
		$arrData["customer_source"] = "complete";
		$arrData["sms_cust"] = $sms_cust;
		$arrData["openid"] = $openid;
		//$arrData["country"] = $_REQUEST['country'];
		//$arrData["province"] = $_REQUEST['province'];
		//$arrData["city"] = $_REQUEST['city'];
		//$arrData["district"] = $_REQUEST['district'];

		//dump($cmFields);die;
		//dump($arrData);die;
		$upBeforeData = $customer->field($fields)->where("id = $id")->find();

		$result = $customer->data($arrData)->where("id = $id")->save();

		//修改记录开始-------------------------------
		$upAfterData = $customer->field($fields)->where("id = $id")->find();
		$arrModify = Array();
		foreach($upBeforeData AS $k=>$v){
			if( $v !== $upAfterData[$k]){
				$arrModify[$k] = Array(
					'upbefore'=>$v,
					'upafter'=>$upAfterData[$k],
					'name'=>$k,
					'field'=>$k,
				);
			}

		}
		//dump($arrModify);die;
		$row = getSelectCache();
		$i = 0;
		foreach($arrModify as $k=>$vm){
			$arrModify[$k]['name'] = $tmp[$vm['name']];
			//$arrModify[$k]['upbefore'] = $row[$k][$vm['upbefore']];
			//$arrModify[$k]['upafter'] = $row[$k][$vm['upafter']];
			$i++;
		}
		//修改记录结束-------------------------------

		//dump($arrModify);die;

		//echo $customer->getLastSql();die;
		if ($result !== false){
			$this->insertModifyRecord($id,json_encode($arrModify));
			echo json_encode(array('success'=>true,'msg'=>'客户资料修改成功!','editid'=>$id));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	//添加修改记录
	function insertModifyRecord($id,$arrModify){
		$modify = new Model("customer_modify_record");
		$arrData = array(
			"customer_id"=>$id,
			"modifytime"=>date("Y-m-d H:i:s"),
			"modifiedby"=>$_SESSION["user_info"]["username"],
			"modify_content"=>$arrModify,
		);
		$result = $modify->data($arrData)->add();
	}




	//将客户资料放到回收站
	function recycleCustomer(){
		$id = $_REQUEST["id"];
		$arrId = explode(",",$id);
		$count = count($arrId);
		//dump($id);die;
		$customer = new Model("customer");


		$arrData = array(
			recycle => 'Y',
		);
		for($i=0;$i<$count;$i++){
			$result[$i] = $customer->data($arrData)->where("id = $arrId[$i]")->save();
		}
		if ($result[0] !== false){
			echo json_encode(array('success'=>true,'msg'=>"删除成功！"));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}

	}

	//还原客户资料
	function reductionCustomer(){
		$id = $_REQUEST["id"];
		$arrId = explode(",",$id);
		$count = count($arrId);
		//dump($id);die;
		$customer = new Model("customer");


		$arrData = array(
			recycle => 'N',
		);
		for($i=0;$i<$count;$i++){
			$result[$i] = $customer->data($arrData)->where("id = $arrId[$i]")->save();
		}
		if ($result[0] !== false){
			echo json_encode(array('success'=>true,'msg'=>"删除成功！"));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}

	}

	function deleteCustomer(){
		$id = $_REQUEST["id"];
		//dump($id);die;
		$customer = new Model("customer");
		$result = $customer->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//资料转交
	function saveTransFerees(){
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST["id"];
		$arrId = explode(",",$id);
		$count = count($arrId);

		$users = new Model("users");
		$uD = $users->where("username = '".$_REQUEST["deptuser"]."'")->find();

		//dump($uD['d_id']);die;
		$customer = new Model("customer");
		$arrData = array(
			"createuser"=>$_REQUEST["deptuser"],
			"dept_id"=>$uD['d_id'],
			"autocall_visit"=>"Y",
			"customer_source"=>"complete",
		);

		foreach($arrId as $v){
			$tmp[$v] = array(
				'customer_id'=>$v,
				'forwardedname'=>$username,
				'recipient'=>$_REQUEST["deptuser"],
			);
		}

		for($i=0;$i<$count;$i++){
			$result[$i] = $customer->data($arrData)->where("id = $arrId[$i]")->save();
		}

		$sql = "insert into customer_transferees(forwardedname, recipient, customer_id,createtime) values ";
		$value = "";
		foreach( $tmp AS $row ){
			$str = "(";
			$str .= "'" .$row['forwardedname']. "',";
			$str .= "'" .$row['recipient']. "',";
			$str .= "'" .$row['customer_id']. "',";
			$str .= "'" .date("Y-m-d H:i:s"). "'";

			$str .= ")";
			$value .= empty($value)?"$str":",$str";
			$i++;
		}
		if( $value ){
			$sql .= $value;
			$res = $customer->execute($sql);
		}

		//转交时 将回访记录也 一起转交
		$visit_record = new Model("visit_record");
		$arrV = array(
			"createname"=>$_REQUEST["deptuser"],
		);
		for($i=0;$i<$count;$i++){
			$result[$i] = $visit_record->data($arrV)->where("customer_id = $arrId[$i] ")->save();
		}


		if ($result){
			$userArr = readU();
			$cnName = $userArr["cn_user"];
			$cmData = $customer->field("id,name,phone1,qq_number")->where("id in ($id) ")->select();
			$i = 0;
			foreach($cmData as $val){
				$cmData[$i]['type'] = "AB";
				$cmData[$i]['acceptuser'] = $_REQUEST["deptuser"];  // 转交给   接收人
				$cmData[$i]['acceptuser_name'] = $cnName[$_REQUEST["deptuser"]];  // 转交给   接收人姓名
				$cmData[$i]['giveuser'] = $username; //谁转交的    转交人
				$cmData[$i]['giveuser_name'] = $cnName[$username]; //谁转交的    转交人姓名
				$i++;
			}
			unset($i);
			$messages = json_encode($cmData);

			echo json_encode(array('success'=>true,'msg'=>'客户资料转交成功',"messages"=>$messages,"recipient"=>$_REQUEST["deptuser"]));
		} else {
			echo json_encode(array('msg'=>'出现未知错误！'));
		}
	}



	function visitProcess(){
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();

		$users = new Model("users");
		$userList = $users->field("username as text,d_id as pid,cn_name,username as id")->select();

		$j = 0;
		foreach($dept as $vm){
			$dept[$j]['iconCls'] = "door";
			$dept[$j]['state'] = "closed";
			$dept[$j]['attributes'] = "department".",".$vm['tid'];
			$j++;
		}
		unset($j);

		$i = 0;
		foreach($userList as $v){
			$userList[$i]['iconCls'] = "icon-user_chat";
			$userList[$i]['attributes'] = "user".",".$v['cn_name'];
			$i++;
		}
		unset($i);

		foreach($userList as $val){
			$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}
        $arrTree = $this->getTree($dept,0);
		//dump($arrTree);die;
		$strJSON = json_encode($arrTree);
		echo ($strJSON);

	}

	//添加回访
	function addVisit(){
		checkLogin();
		$id = $_REQUEST['id'];
		//dump($id);
		$this->assign("id",$id);
		$this->display();
	}

	function saveVisits(){
		$customer_id = explode(",",$_REQUEST['id']);
		$policy_type = $_REQUEST['policy_type'];
		$time_period = $_REQUEST['time_period'];
		$olduser = explode(",",$_REQUEST['recipient']);
		$reuser = Array();

		foreach( $olduser as $k=>$v){
			if( $v )
			array_push($reuser,$v);
		}
		//dump($reuser);die;
		$recipient = $_REQUEST['recipient'];
		$cycle = $_REQUEST['cycle'];
		$infos = explode(",",$_REQUEST['info']);

		foreach($infos as $vm){
			$arrInfo[] = explode("->",$vm);
		}

		$date = date("Y-m-");
		if($policy_type == '2'){  //规则性
			if($time_period == '1'){  //每周
				$n = 0;
				foreach($arrInfo as $val){
					if($arrInfo[$n][1] == '星期一'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Monday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期二'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Tuesday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期三'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Wednesday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期四'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Thursday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期五'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Friday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期六'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Saturday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期日'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Sunday"))." ".$_REQUEST['start_time'].":00";
					}
					$n++;
				}
				foreach($arrInfo as $val){
					$info[] = $val['time'];
				}
			}else{  //每月
				foreach($arrInfo as $val){
					if($val[1] <= 9){
						$info[] = $date."0".$val[1]." ".$val[0].":00";
					}else{
						$info[] = $date.$val[1]." ".$val[0].":00";
					}
				}
			}
		}else{
			foreach($arrInfo as $val){
				$info[] = $val[1]." ".$val[0].":00";
			}
		}

		//dump($arrInfo);die;
		if($policy_type == '2'){  //规则性
			if($time_period == '1'){  //每周
				for($i=0;$i<count($info);$i++){
					for($j=0;$j<$cycle;$j++){
						$visit_time[] =  date('Y-m-d H:i:s',strtotime($info[$i]." +".$j." week"));
					}
				}

			}else{  //每月
				for($i=0;$i<count($info);$i++){
					for($j=0;$j<$cycle;$j++){
						$visit_time[] =  date('Y-m-d H:i:s',strtotime($info[$i]." +".$j." month"));
					}
				}
			}
		}else{  //不规则性
			$visit_time = $info;
		}
		foreach($customer_id as $val){
			$visit[$val] = $visit_time;
		}

		$avg = floor(count($visit)/count($reuser));
		//dump($avg);die;
		//dump($visit);die;
		$assign = Array();
		$start = 0;
		$end = $avg*(count($reuser)-1);
		$i = 0;
		$value = "";
		$index = 0;
		foreach($visit AS $key=>$val){
				if($i<$end){
					$user = $reuser[$index];
					foreach($val AS $v){
						$s = "($key,'$v','N','$user')";
						$value .= empty($value) ? $s : ",".$s;
					}

				}else{
					$user = $reuser[count($reuser)-1];
					foreach($val AS $v){
						$s = "($key,'$v','N','$user')";
						$value .= empty($value) ? $s : ",".$s;
					}
				}
			$i++;
			if($i%$avg == 0) $index++;
		}
		//dump($value);die;
		$sql = "insert into visit_customer(customer_id, visit_time, visit_status,visit_name ) values ";
		$sql .= $value;
		//dump($sql);die;

		/*
		//只分配一人的
		$sql = "insert into visit_customer(customer_id, visit_time, visit_status,visit_name ) values ";
		$value = "";
		foreach( $visit AS $key=>$row ){
			foreach($row AS $v){
				$s = "($key,'$v','N','$recipient')";
				$value .= empty($value) ? $s : ",".$s;
			}
		}
		$sql .= $value;
		*/


		$visit_customer = new Model("visit_customer");
		$result = $visit_customer->execute($sql);

		$customer = new Model("customer");
		$res = $customer->where("id in (".$_REQUEST['id'].")")->save(array('visit_task_enabled'=>'Y'));

		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	//查看回放
	function eidtVisit(){
		checkLogin();
		$id = $_REQUEST['id'];
		$this->assign("id",$id);
		$this->display();
	}

	function visitDataList(){
		$id = $_REQUEST['id'];
		$username = $_SESSION['user_info']['username'];
		$visit_customer = new Model("visit_customer");
		if($username == 'admin'){
			$count = $visit_customer->where("customer_id = '$id' AND visit_status = 'N'")->count();
		}else{
			$count = $visit_customer->where("customer_id = '$id' AND visit_status = 'N' AND visit_name='$username'")->count();
		}

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == 'admin'){
			$vsData = $visit_customer->order("visit_time asc")->limit($page->firstRow.','.$page->listRows)->where("customer_id = '$id' AND visit_status = 'N'")->select();
		}else{
			$vsData = $visit_customer->order("visit_time asc")->limit($page->firstRow.','.$page->listRows)->where("customer_id = '$id' AND visit_status = 'N' AND visit_name='$username'")->select();
		}

		$rowsList = count($vsData) ? $vsData : false;
		$arrVS["total"] = $count;
		$arrVS["rows"] = $rowsList;

		echo json_encode($arrVS);
	}

	//删除回访
	function deleteVisit(){
		$id = $_REQUEST["id"];
		$visit_customer = new Model("visit_customer");
		$result = $visit_customer->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//修改记录
	function modifyRecord(){
		checkLogin();
		$id = $_REQUEST['id'];
		$modify = new Model("customer_modify_record");

		$count = $modify->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$modData = $modify->order("m.modifytime desc")->table("customer_modify_record m")->field("m.id,m.customer_id,m.modifytime,m.modifiedby,m.modify_content,c.name")->join("customer c on (m.customer_id = c.id)")->where("m.customer_id = '$id'")->limit($page->firstRow.','.$page->listRows)->select();
		//echo $modify->getLastSql();
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if( $val['text_type'] == '2'){
				$arrT[] = $val['en_name'];
			}
		}
		$row = getSelectCache();

		foreach($modData as &$val){
			$val['modify_content'] = json_decode($val['modify_content'],true);
			foreach($val['modify_content'] as $key=>&$vm){
				if( in_array($key,$arrT) ){
					$vm['upbefore'] = $row[$key][$vm['upbefore']];
					$vm['upafter'] = $row[$key][$vm['upafter']];
				}
			}
		}

		$this->assign("modData",$modData);
		$this->display();
	}



	function customerVisitData(){
		$id = $_REQUEST["id"];
		$visit_record = new Model("visit_record");
		$count = $visit_record->where("customer_id = '$id'")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.createname,v.createtime,v.visit_type,v.visit_content,v.accountcode,u.cn_name")->join("users u on v.createname = u.username")->where("customer_id = '$id'")->limit($page->firstRow.','.$page->listRows)->select();
		$row = array("phone"=>"电话回访","qq"=>"QQ回访");
		$i = 0;
		foreach($visitData as &$val){
			$val["accountcode"] = trim($val["accountcode"]);
			$arrTmp = explode('.',$val["accountcode"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["accountcode"].".WAV";

			if($val['visit_type'] == 'phone' && file_exists($WAVfile) ){
				$visitData[$i]['operations'] .= "<a href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["accountcode"])."'".")\" >播放</a>";
			}else{
				$visitData[$i]['operations'] = "";
			}

			$visit_type = $row[$val['visit_type']];
			$val['visit_type'] = $visit_type;
			$i++;
		}
		unset($i);
		//dump($visitData);die;
		$rowsList = count($visitData) ? $visitData : false;
		$arrV["total"] = $count;
		$arrV["rows"] = $rowsList;

		echo json_encode($arrV);
	}


	//回收站
	function recycleList_bak(){
		checkLogin();
		$users= new Model("users");
		$usersList = $users->field("username,extension,d_id,r_id,cn_name,en_name,email,fax,extension_type,extension_mac,phone,out_pin,routeid")->select();
		$this->assign("usersList",$usersList);
		//分配增删改的权限
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['en_name'] != "createuser" ){
				$arrField[0][$i]['field'] = $val['en_name'];
				$arrField[0][$i]['title'] = $val['cn_name'];
				$arrField[0][$i]['fitColumns'] = true;
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}

		$arrFd = array("field"=>"createtime","title"=>"创建时间");
		$arrFd2 = array("field"=>"ck","checkbox"=>true);
		$arrFd3 = array("field"=>"createuser","title"=>"创建人工号");
		$arrFd4 = array("field"=>"cn_name","title"=>"创建人姓名");
		$arrFd5 = array("field"=>"d_name","title"=>"所属部门");
		array_unshift($arrField[0],$arrFd5);
		array_unshift($arrField[0],$arrFd4);
		array_unshift($arrField[0],$arrFd3);
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd2);
		//dump($fielddTpl);die;
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("fielddTpl",$fielddTpl);

		$this->display();
	}

	function recycleList(){
		checkLogin();
		$users= new Model("users");
		$usersList = $users->field("username,extension,d_id,r_id,cn_name,en_name,email,fax,extension_type,extension_mac,phone,out_pin,routeid")->select();
		$this->assign("usersList",$usersList);
		//分配增删改的权限
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['list_enabled'] == "Y" ){
				if($val['en_name'] != "createuser" ){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					//$arrField[0][$i]['fitColumns'] = true;
					$arrField[0][$i]['width'] = "100";
				}
				/*
				if( $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
				*/
				if( $val['tiptools_enabled'] == 'Y' ||  $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}
		$arrFd = array("field"=>"dealuser","title"=>"共享坐席","width"=>"120");
		array_unshift($arrField[0],$arrFd);

		$arrFrozen = array(
						array("field"=>"ck","checkbox"=>true),
						array("field"=>"createtime","title"=>"创建时间","width"=>"120"),
						array("field"=>"createuser","title"=>"创建人工号","width"=>"100"),
						array("field"=>"cn_name","title"=>"创建人姓名","width"=>"100"),
						array("field"=>"d_name","title"=>"所属部门","width"=>"100"),
					);

		//dump($fielddTpl);die;
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("arrFrozen",json_encode($arrFrozen));
		$this->assign("fielddTpl",$fielddTpl);

		$areaTpl = implode(",",$areaTpl2);
		$this->assign("areaTpl",$areaTpl);

		$para_sys = readS();
		$export_rule = $para_sys["export_rule"];
		$this->assign("export_rule",$export_rule);

		$this->display();
	}


	//回收站
	function customerRecycleFata(){
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$trans = $_REQUEST['trans'];
		$deptId = $_REQUEST['dept_id'];

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$deptSet = rtrim($deptst,",");

		$deptst2 = $this->getMeAndSubDeptName($arrDep,$deptId);
		$deptSet2 = rtrim($deptst2,",");

		$where = "(1 ";
		$where .= " AND c.recycle = 'Y'";
		$where .= empty($deptId)?"":" AND u.d_id in ($deptSet2)";
		$where .= empty($startime)?"":" AND c.createtime >'$startime'";
        $where .= empty($endtime)?"":" AND c.createtime <'$endtime'";

		//dump($_REQUEST);die;


		//精确查询可以查所有的客户资料
		if($_REQUEST['searchmethod']){
			if($username == "admin" ){
				$where .= " AND 1)";
			}else{
				if( $searchmethod == "equal"){
					$where .= " AND 1)";
				}else{
					$where .= "  AND 1)  AND u.d_id in ($deptSet)";
				}
			}
		}else{
			if($username != "admin"){
				$where .= " AND 1)  AND u.d_id in ($deptSet)";
			}else{
				$where .= " AND 1)";
			}
		}


		//备选字段
		$para_sys = readS();
		if($para_sys["hide_field"]=='yes'){
			if(!$_REQUEST['searchmethod']){
				$where .= " AND c.hide_fields='Y'";
			}else{
				if($searchmethod != "equal"){
					$where .= " AND hide_fields='Y'";
				}
			}
		}


		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y' && $val['text_type'] != '3'){
				$arrF[] = "c.".$val['en_name'];
				$$val['en_name'] = $_REQUEST[$val['en_name']];
			}
		}
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3'){
				$aa[] = $vm['en_name'];
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					$where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
				}
			}
		}

		//dump($where);die;


		//匹配上次查询条件--start
		$search_last_sql = new Model("search_last_sql");
		if($where != "1 "){
			$search_res = $search_last_sql->where("table_name = 'customer'")->save( array('last_sql'=>$where) );
		}
		$search_data = $search_last_sql->where("table_name = 'customer'")->find();
		if($_REQUEST['search'] == "lastTime"){
			$where = $search_data['last_sql'];
		}else{
			$where = $where;
		}
		//匹配上次查询条件--end

		$customer = new Model("customer");
		import('ORG.Util.Page');

		if($username == "admin"){
			$count = $customer->table("customer c")->join("users u on (c.createuser = u.username)")->where($where)->count();    //可以看到要转交的资料

		}else{
			$count = $customer->table("customer c")->join("users u on (c.createuser = u.username)")->where($where)->count();
		}

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		//$fields = "`c.id`,`c.createtime`,"."`".implode("`,`",$arrF)."`";
		$fields = "c.id,c.createtime,".implode(",",$arrF).",u.cn_name";
		if($username == "admin"){
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		}else{
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}
		//dump($cmlist);die;
		//dump($customer->getLastSql());die;

		$userArr = readU();
		//$cnName = $userArr["cn_user"];
		//$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;

					$vm['d_name'] = $deptName_user[$vm["createuser"]];
				}
			}
		}
		//dump($cmlist);die;
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$userArr = readU();
		$cn_user = $userArr['cn_user'];
		$user_work = array_flip($cn_user);

		$para_sys = readS();
		$i = 0;
		foreach($cmlist as &$vm){
			$vm['customer_service'] = $cn_user[$vm['customer_service']];
			$cmlist[$i]["massPhone"] = $vm['phone1'];
			$cmlist[$i]["massEmail"] = $vm['email'];
			$cmlist[$i]["massQQ"] = $vm['qq_number'];
			$cmlist[$i]["massBank"] = $vm['bank_account'];

			if($para_sys['hide_rule'] == "role"){
				if($username != "admin"){
					if($priv["hide_phone"] =="Y"){
						$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
					}
					if($vm['email']){
						if($priv["hide_email"] =="Y"){
							$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
						}
					}
					if($vm['qq_number']){
						if($priv["hide_qq"] =="Y"){
							$cmlist[$i]["qq_number"] = "***".substr($vm["qq_number"],3);
						}
					}
				}
			}else{
				if($username != "admin"){
					if($para_sys["hide_phone"] =="yes"){
						$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
					}
					if($vm['email']){
						if($para_sys["hide_email"] =="yes"){
							$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
						}
					}
					if($vm['qq_number']){
						if($para_sys["hide_qq"] =="yes"){
							$cmlist[$i]["qq_number"] = "***".substr($vm["qq_number"],3);
						}
					}
				}
			}
			/*
			*/

			$i++;
		}
		unset($i);


		//dump($cmlist);die;
		$rowsList = count($cmlist) ? $cmlist : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
	}





	//转交历史
	function transFereesList(){
		checkLogin();
		$this->display();
	}

	function transFereesData(){
		$name = $_REQUEST['name'];
		$service_start = $_REQUEST['service_start'];
		$service_end = $_REQUEST['service_end'];
		$forwardedname = $_REQUEST['forwardedname'];
		$recipient = $_REQUEST['recipient'];

		$where = "1 ";
		$where .= empty($service_start)?"":" AND t.createtime > '$service_start'";
		$where .= empty($service_end)?"":" AND t.createtime < '$service_end'";
		$where .= empty($forwardedname)?"":" AND t.forwardedname like '%$forwardedname%'";
		$where .= empty($recipient)?"":" AND t.recipient like '%$recipient%'";
		$where .= empty($name)?"":" AND c.name like '%$name%'";

		$username = $_SESSION['user_info']['username'];
		$customer_transferees = new Model("customer_transferees");
		if($username == 'admin'){
			$count = $customer_transferees->table("customer_transferees t")->join("customer c on t.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where($where)->count();
		}else{
			$count = $customer_transferees->table("customer_transferees t")->join("customer c on t.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where("$where AND forwardedname = '$username'")->count();
		}

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == 'admin'){
			$transData = $customer_transferees->order("t.createtime desc")->table("customer_transferees t")->field("t.forwardedname, t.recipient, t.customer_id,t.createtime,t.transferType,c.name")->join("customer c on t.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}else{
			$transData = $customer_transferees->order("t.createtime desc")->table("customer_transferees t")->field("t.forwardedname, t.recipient, t.customer_id,t.createtime,t.transferType,c.name")->join("customer c on t.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where("$where AND forwardedname = '$username'")->select();
		}


		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$row = array("in"=>"呼入平台转交","out"=>"外呼平台转交");
		foreach($transData as &$val){
			$transferType = $row[$val["transferType"]];
			$val["transferType"] = $transferType;

			$val['forwardedname_cn_name'] = $cnName[$val["forwardedname"]];
			$val['recipient_cn_name'] = $cnName[$val["recipient"]];
		}


		$rowsList = count($transData) ? $transData : false;
		$arrTrans["total"] = $count;
		$arrTrans["rows"] = $rowsList;

		echo json_encode($arrTrans);
	}

	//某条客户的转交历史
	function viewforwardedHistory(){
		checkLogin();
		$id = $_REQUEST['id'];
		$this->assign("id",$id);
		$this->display();
	}

	function forwardedHistoryData(){
		$id = $_REQUEST['id'];
		$username = $_SESSION['user_info']['username'];
		$customer_transferees = new Model("customer_transferees");
		if($username == 'admin'){
			$count = $customer_transferees->where("customer_id = '$id'")->count();
		}else{
			//$count = $customer_transferees->where("forwardedname = '$username' AND customer_id = '$id'")->count();
			$count = $customer_transferees->where(" customer_id = '$id'")->count();
		}

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == 'admin'){
			$transData = $customer_transferees->table("customer_transferees t")->field("t.forwardedname, t.recipient, t.customer_id,t.createtime,c.name")->join("customer c on t.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where("customer_id = '$id'")->select();
		}else{
			//$transData = $customer_transferees->table("customer_transferees t")->field("t.forwardedname, t.recipient, t.customer_id,t.createtime,c.name")->join("customer c on t.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where("forwardedname = '$username' AND customer_id = '$id'")->select();
			$transData = $customer_transferees->table("customer_transferees t")->field("t.forwardedname, t.recipient, t.customer_id,t.createtime,c.name")->join("customer c on t.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where("customer_id = '$id'")->select();
		}


		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$i = 0;
		foreach($transData as $val){
			$transData[$i]['forwardedname_cn_name'] = $cnName[$val["forwardedname"]];
			$transData[$i]['recipient_cn_name'] = $cnName[$val["recipient"]];
			$i++;
		}



		$rowsList = count($transData) ? $transData : false;
		$arrTrans["total"] = $count;
		$arrTrans["rows"] = $rowsList;

		echo json_encode($arrTrans);
	}


	//回访内容
	function visitContent(){
		checkLogin();
		checkLogin();
		//分配增删改的权限
		$menuname = "Visit Content";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function visitContentData(){
		$username = $_SESSION['user_info']['username'];
		$vis = new Model("visit_content");
		if($username == 'admin'){
			$count = $vis->count();
		}else{
			$count = $vis->where("createname = '$username'")->count();
		}
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == 'admin'){
			$visData = $vis->order("id desc")->limit($page->firstRow.','.$page->listRows)->select();
		}else{
			$visData = $vis->order("id desc")->limit($page->firstRow.','.$page->listRows)->where("createname = '$username'")->select();
		}

		$rowsList = count($visData) ? $visData : false;
		$arrVis["total"] = $count;
		$arrVis["rows"] = $rowsList;

		echo json_encode($arrVis);
	}

	function insertVisitContent(){
		$username = $_SESSION['user_info']['username'];
		$vis = new Model("visit_content");
		$arrData = array(
			'title'=>$_POST['title'],
			'content'=>$_POST['content'],
			'createname'=>$username,
		);
		$result = $vis->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'回访内容添加成功！'));
		} else {
			echo json_encode(array('msg'=>'回访内容添加失败！'));
		}
	}

	function updateVisitContent(){
		$id = $_REQUEST['id'];
		$vis = new Model("visit_content");
		$arrData = array(
			'title'=>$_POST['title'],
			'content'=>$_POST['content'],
		);
		$result = $vis->data($arrData)->where("id='$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteVisitContent(){
		$id = $_REQUEST["id"];
		$vis = new Model("visit_content");
		$result = $vis->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//导出客户资料(把查出来的全部导出)
	function exportCustomerToExcel(){
		ini_set('memory_limit', '-1');
		set_time_limit(0);
		$callStartTime = microtime(true);

		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$trans = $_REQUEST['trans'];
		$deptId = $_REQUEST['dept_id'];

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$deptSet = rtrim($deptst,",");

		$deptst2 = $this->getMeAndSubDeptName($arrDep,$deptId);
		$deptSet2 = rtrim($deptst2,",");


		$where = "1 ";
		$where .= " AND c.recycle = 'N'";
		$where .= empty($deptId)?"":" AND u.d_id in ($deptSet2)";
		$where .= empty($startime)?"":" AND c.createtime >'$startime'";
        $where .= empty($endtime)?"":" AND c.createtime <'$endtime'";
		if($username != "admin"){
			$where .= " AND u.d_id in ($deptSet)";
		}
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				$field[] = $val['en_name'];
				$field2[] = "c.".$val['en_name'];
				$title[] = $val['cn_name'];
				//$$val['en_name'] = $_REQUEST[$val['en_name']];
				if($val['text_type'] == '4'){
					$start = $val['en_name']."_start";
					$$start = $_REQUEST[$start];
					$end = $val['en_name']."_end";
					$$end = $_REQUEST[$end];
				}else{
					$$val['en_name'] = $_REQUEST[$val['en_name']];
				}
			}
		}
		array_unshift($field,"createtime");
		array_unshift($title,"创建时间");
		$count = count($field);
		//$fields = "`createtime`,"."`".implode("`,`",$field)."`";
		$fields = "c.createtime,".implode(",",$field2);
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'){
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					$where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
				}


				if($vm['text_type'] == '4'){
					$start = $vm['en_name']."_start";
					$end = $vm['en_name']."_end";
					 $where .= empty($$start)?"":" AND ".$vm['en_name'] ." >= '".$$start."'";
					 $where .= empty($$end)?"":" AND ".$vm['en_name'] ." <= '".$$end."'";
				}
			}
		}
		//dump($field);
		//dump($title);die;

		$customer = new Model("customer");
		if($username == "admin"){
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->where($where)->select();
		}else{

			if($trans){
				if($trans == 'autocall'){
					$where .= " AND (customer_source = 'autocall' || customer_source = 'incall' ) AND autocall_visit = 'N' AND dept_id = '$dept_id'";  // AND createuser='$username'
				}
				/*
				if($trans == 'incall'){
					$where .= " AND customer_source != 'autocall' AND customer_source != 'incall'  ";
				}*/
			}
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->where($where)->select();  //"$where AND createuser = '$username' "
		}

		$row = getSelectCache();
		//dump($row);
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}
		//echo $customer->getLastSql();
		//dump($cmlist);die;
		/*
		$i = 0;
		foreach($cmlist as $vm){
			$para_sys = readS();
			if($para_sys["hide_phone"] =="yes"){
				$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
			}
			if($vm['email']){
				if($para_sys["hide_email"] =="yes"){
					$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
				}
			}
			$i++;
		}
		*/




		vendor("PHPExcel176.PHPExcel");
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];
		//dump($lm);
		//dump($letters2);die;
		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$field_key = array_flip($field);
		$start_row = 2;
		foreach($cmlist as $val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
				if($j == $field_key["phone1"] || $j == $field_key["phone2"] ){
					$objPHPExcel->getActiveSheet()->setCellValueExplicit($letters2[$j].$start_row, $val[$field[$j]],PHPExcel_Cell_DataType::TYPE_STRING);
				}
			}
			$start_row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$name = "客户资料";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		/*
		$sheetname=iconv("utf-8","gb2312", $name);
		$filename=$showtime=$sheetname.'-'.date("Y-m-d-H-i-s").'.xlsx';
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		//header('Content-Disposition: attachment;filename="01simple.xls"');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		*/

		/*
		$filename ="客户资料";
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type:text/html;charset=UTF-8");
		$filename = iconv("utf-8","gb2312",$filename);
        header("Content-Disposition: attachment;filename=$filename.xls ");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();
        $start_row	=	0;

        xlsWriteLabel($start_row,0,utf2gb("创建时间"));
        xlsWriteLabel($start_row,1,utf2gb("客户姓名"));
        xlsWriteLabel($start_row,2,utf2gb("公司名称"));
        xlsWriteLabel($start_row,3,utf2gb("电话"));
        xlsWriteLabel($start_row,4,utf2gb("手机"));
        xlsWriteLabel($start_row,5,utf2gb("邮件地址"));
        xlsWriteLabel($start_row,6,utf2gb("传真"));
        xlsWriteLabel($start_row,7,utf2gb("邮编"));
        xlsWriteLabel($start_row,8,utf2gb("群组"));
        xlsWriteLabel($start_row,9,utf2gb("等级"));
        xlsWriteLabel($start_row,10,utf2gb("客户地址"));
        xlsWriteLabel($start_row,11,utf2gb("业务员"));
        xlsWriteLabel($start_row,12,utf2gb("描述"));

        $start_row++;
        foreach($cmlist as $val)  {
            xlsWriteLabel($start_row,0,utf2gb($val["createtime"]));
            xlsWriteLabel($start_row,1,utf2gb($val["name"]));
            xlsWriteLabel($start_row,2,utf2gb($val["company"]));
            xlsWriteLabel($start_row,3,utf2gb($val["phone2"]));
            xlsWriteLabel($start_row,4,utf2gb($val["phone1"]));
            xlsWriteLabel($start_row,5,utf2gb($val["email"]));
            xlsWriteLabel($start_row,6,utf2gb($val["fax"]));
            xlsWriteLabel($start_row,7,utf2gb($val["zipcode"]));
            xlsWriteLabel($start_row,8,utf2gb($val["groups"]));
            xlsWriteLabel($start_row,9,utf2gb($val["grade"]));
            xlsWriteLabel($start_row,10,utf2gb($val["address"]));
            xlsWriteLabel($start_row,11,utf2gb($val["createuser"]));
            xlsWriteLabel($start_row,12,utf2gb($val["description"]));
           // xlsWriteNumber($start_row,6,utf2gb($val["smoney"]));
            $start_row++;
        }
        xlsEOF();
		*/
	}

	//导出客户资料(把查出来的全部导出)
	function exportCustomerToCsv(){
		set_time_limit(0);
		ini_set('memory_limit', '-1');
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$trans = $_REQUEST['trans'];
		$deptId = $_REQUEST['dept_id'];

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$deptSet = rtrim($deptst,",");

		$deptst2 = $this->getMeAndSubDeptName($arrDep,$deptId);
		$deptSet2 = rtrim($deptst2,",");


		$where = "1 ";
		$where .= " AND c.recycle = 'N'";
		$where .= empty($deptId)?"":" AND u.d_id in ($deptSet2)";
		$where .= empty($startime)?"":" AND c.createtime >'$startime'";
        $where .= empty($endtime)?"":" AND c.createtime <'$endtime'";
		if($username != "admin"){
			$where .= " AND u.d_id in ($deptSet)";
		}
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				$field[] = $val['en_name'];
				$field2[] = "c.".$val['en_name'];
				//$title[] = $val['cn_name'];
				$title[] = iconv("utf-8","gb2312",$val['cn_name']);
				//$$val['en_name'] = $_REQUEST[$val['en_name']];
				if($val['text_type'] == '4'){
					$start = $val['en_name']."_start";
					$$start = $_REQUEST[$start];
					$end = $val['en_name']."_end";
					$$end = $_REQUEST[$end];
				}else{
					$$val['en_name'] = $_REQUEST[$val['en_name']];
				}
			}
		}
		array_unshift($field,"createtime");
		array_unshift($title,iconv("utf-8","gb2312","创建时间"));
		$count = count($field);
		//$fields = "`createtime`,"."`".implode("`,`",$field)."`";
		$fields = "c.createtime,".implode(",",$field2);
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'){
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					$where .= empty($$vm['en_name'])?"":" AND c.".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
				}
			}


			if($vm['text_type'] == '4'){
				$start = $vm['en_name']."_start";
				$end = $vm['en_name']."_end";
				 $where .= empty($$start)?"":" AND ".$vm['en_name'] ." >= '".$$start."'";
				 $where .= empty($$end)?"":" AND ".$vm['en_name'] ." <= '".$$end."'";
			}
		}
		//dump($field);
		//dump($title);die;

		$customer = new Model("customer");
		if($username == "admin"){
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->where($where)->select();
		}else{

			if($trans){
				if($trans == 'autocall'){
					$where .= " AND (customer_source = 'autocall' || customer_source = 'incall' ) AND autocall_visit = 'N' AND dept_id = '$dept_id'";  // AND createuser='$username'
				}
			}
			$cmlist = $customer->order("createtime desc")->table("customer c")->field($fields)->join("users u on (c.createuser = u.username)")->where($where)->select();  //"$where AND createuser = '$username' "
		}

		$row = getSelectCache();
		//dump($row);
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}

		$csvContent = implode(",",$title)."\r\n";

		foreach($cmlist as &$val){
			/*
			if(substr($val["phone1"],0,1) == "0"|| substr($val["phone2"],0,1) == "0"){
				$val["phone1"] = $val["phone1"]."\t";
				$val["phone2"] = $val["phone2"]."\t";   //防止前缀0被去掉  后遗症就是字段里确实有一个\t在里面，入如果只是拿来看到是没有问题，但是还要把这个东西导入异构系统时 \t 会导致不可预知的错误
			}
			*/

			$vv = implode(',',str_replace(",","",$val));
			$vv = str_replace("\r","",$vv);
			$vv = str_replace("\n","",$vv);
			$csvContent .= iconv("utf-8","gb2312",$vv) ."\r\n";
		}
		//dump($cmlist);die;
		$name = "客户资料".date("Ymd");
		$filename = iconv("utf-8","gb2312",$name);
		$this->export_csv($filename,$csvContent);
	}

	function export_csv($filename,$data){
		$content = iconv("utf-8","gb2312",$data);
		$d = date("D M j G:i:s T Y");
        header('HTTP/1.1 200 OK');
        header('Date: ' . $d);
        header('Last-Modified: ' . $d);
        header("Content-Type: application/force-download");
        header("Content-Length: " . strlen($data));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".$filename.".csv");
        echo $data;
	}

	//导出客户资料(导出本页的客户资料)
	function exportCustomerToExcel2(){
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				$field[] = $val['en_name'];
				$title[] = $val['cn_name'];
				$$val['en_name'] = $_REQUEST[$val['en_name']];
			}
		}
		array_unshift($field,"createtime");
		array_unshift($title,"创建时间");
		$count = count($field);
		//dump($field);
		//dump($title);die;

		$sears = base64_decode($_GET["sears"]);
		$customer = new Model("customer");
		$cmlist = $customer->query($sears);

		$row = getSelectCache();
		//dump($row);
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}

		/*
		$i = 0;
		foreach($cmlist as $vm){
			$para_sys = readS();
			if($para_sys["hide_phone"] =="yes"){
				$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
			}
			if($vm['email']){
				if($para_sys["hide_email"] =="yes"){
					$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
				}
			}
			$i++;
		}
		*/




		vendor("PHPExcel176.PHPExcel");
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];
		//dump($lm);
		//dump($letters2);die;
		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$field_key = array_flip($field);
		$start_row = 2;
		foreach($cmlist as $val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
				if($j == $field_key["phone1"] || $j == $field_key["phone2"] ){
					$objPHPExcel->getActiveSheet()->setCellValueExplicit($letters2[$j].$start_row, $val[$field[$j]],PHPExcel_Cell_DataType::TYPE_STRING);
				}
			}
			$start_row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$name = "客户资料";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

	}


	//导出指定的客户资料
	function exportFilterCustomerToExcel(){
		$id = $_GET["id"];
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				$field[] = $val['en_name'];
				$title[] = $val['cn_name'];
			}
		}
		array_unshift($field,"createtime");
		array_unshift($title,"创建时间");
		$count = count($field);
		$fields = "`createtime`,"."`".implode("`,`",$field)."`";
		$customer = new Model("customer");
		$cmlist = $customer->order("createtime desc")->field($fields)->where("id in ($id)")->select();

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}

		/*
		$i = 0;
		foreach($cmlist as $vm){
			$para_sys = readS();
			if($para_sys["hide_phone"] =="yes"){
				$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
			}
			if($vm['email']){
				if($para_sys["hide_email"] =="yes"){
					$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
				}
			}
			$i++;
		}
		*/


		vendor("PHPExcel176.PHPExcel");
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];
		//dump($lm);
		//dump($letters2);die;
		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($cmlist as $val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
			}
			$start_row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$name = "客户资料";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}




	//导入客户资料
	function doImportCustomerData(){
		if( empty($_FILES["customer_name"]["tmp_name"]) ){
			//goBack("请选择文件!","");
			echo json_encode(array('msg'=>'请选择文件!'));
			exit;
		}
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
			if($val["text_type"] == "2"){
				$selectField[] = $val["en_name"];
			}
		}
		//dump($selectField);die;
		//array_unshift($field,"createtime");
		//array_unshift($title,"创建时间");
		$count = count($field)+2;
		$fields = "`".implode("`,`",$field)."`".",`createtime`,`createuser`,`dept_id`";
		$time = time();
		$tmp_f = $_FILES["customer_name"]["tmp_name"];  // /tmp/phptBkWXb
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		$field_key = array_flip($field);
		//dump($field_key);die;
		//dump($count);die;

		$customer = new Model("customer");

		//-------------------去重
		$customerData = $customer->field($fields)->select();
		foreach($customerData as $val){
			$phone[] = $val["phone1"];
			$qq[] = $val["qq_number"];
		}
		foreach($qq as $v){
			if($v){
				$qq2[] = $v;
			}
		}
		//-------------------去重
		//dump($phone);
		$sql = "insert into customer($fields) values ";
		$value = "";
		//dump($sql);die;
		if($suffix == "xls"){
			$dst_f = TMP_UPLOAD ."customer_name_".$time.".xls";
			//echo $dst_f;die;  // /tmp/IPPBX_Tmp_Upload/customer_name_1364281330.xls
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrCustomer = $data->sheets[0];
			array_shift($arrCustomer["cells"]);
			$arrCustomer = $arrCustomer["cells"];

			$userArr = readU();
			$deptId_user = $userArr["deptId_user"];
			if($_REQUEST['workname']){
				$workname = $_REQUEST['workname'];
				$dept_id = $deptId_user[$workname];
			}else{
				$workname = $_SESSION['user_info']['username'];
				$dept_id = $_SESSION['user_info']['d_id'];
			}
			//dump($arrCustomer);die;
			$total_num = count($arrCustomer);

			foreach($arrCustomer as $val){
				if( !in_array($val[$field_key["phone1"]+1],$phone) ){
					$ph_unique[] = $val;
				}
			}

			foreach($ph_unique as $val){
				if( !in_array($val[$field_key["qq_number"]+1],$qq2) ){
					$qq_unique[] = $val;
				}
			}
			$num = count($qq_unique);
			$nouse_num = $total_num-$num;
			$arrCustomer = $qq_unique;
			/*
			$selectValue = getSelectCache();
			foreach($selectValue as $key=>$val){
				$selectKey[$key] = array_flip($val);
			}

			$selectF = array_flip($selectField);
			foreach($selectKey as $key=>$val){
				$selectKey2[$selectF[$key]+1] = $val;
			}

			foreach($selectF as $vm){
				foreach($arrCustomer as &$val){
					$status = $selectKey2[$val[$vm+1]];
					$val[$vm+1] = $status;
				}
			}
			//dump($field_key);
			dump($arrCustomer);die;
			dump($selectF);
			dump($selectKey);die;*/
			//dump($arrCustomer);die;

			foreach( $arrCustomer AS $row ){
				$str = "(";
				for($i=1;$i<=($count-2);$i++){
					$str .= "'" .$row[$i]. "',";
				}
				$str .= "'" .date("Y-m-d H:i:s"). "',";
				$str .= "'" .$workname. "',";
				$str .= "'" .$dept_id. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
		}else{
			//goBack("文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!","");
			echo json_encode(array('msg'=>'文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!'));
			exit;
		}

		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			//dump($sql);die;
			$result = $customer->execute($sql);
		}
		//判断导入结果
		if( $result ){
			echo json_encode(array('success'=>true,'msg'=>'成功导入 '.$result.' 条客户资料，有'.$nouse_num.'条为号码已存在或QQ号已存在的!'));
		}else{
			echo json_encode(array('msg'=>'导入资料时出现未知错误！手机号或QQ号已存在！'));
		}
	}

	function test(){
		$userArr = readU();
		$cn_user = $userArr['cn_user'];
		$user_work = array_flip($cn_user);
		$deptId_user = $userArr["deptId_user"];

		//sell_user：销售部员工工号， service_user：客服部员工工号， sell_dept：销售本部门id， service_dept：客服部部门id， cn_user：工号=>姓名， deptId_user：工号=>部门id， deptName_user：工号=>部门名称

		//dump($userArr);die;
		$this->display();
	}

	//导入客户资料
	function doImportCustomerData2(){
		if( empty($_FILES["customer_name"]["tmp_name"]) ){
			//goBack("请选择文件!","");
			echo json_encode(array('msg'=>'请选择文件!'));
			exit;
		}
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				//if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				//}
			}
			if($val["text_type"] == "2"){
				$selectField[] = $val["en_name"];
			}
		}
		//dump($selectField);die;
		//array_unshift($field,"createtime");
		//array_unshift($title,"创建时间");
		$count = count($field)+2;
		//$fields = "`".implode("`,`",$field)."`".",`createtime`,`createuser`,`dept_id`";
		$fields = "`".implode("`,`",$field)."`".",`createtime`,`dept_id`";
		$time = time();
		$tmp_f = $_FILES["customer_name"]["tmp_name"];  // /tmp/phptBkWXb
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		$field_key = array_flip($field);
		//dump($field_key);die;
		//dump($count);die;

		$customer = new Model("customer");

		//-------------------去重
		$customerData = $customer->field($fields)->select();
		foreach($customerData as $val){
			$phone[] = $val["phone1"];
			$qq[] = $val["qq_number"];
		}
		foreach($qq as $v){
			if($v){
				$qq2[] = $v;
			}
		}
		//-------------------去重
		//dump($phone);
		$sql = "insert into customer($fields) values ";
		$value = "";
		//dump($sql);die;
		if($suffix == "xls"){
			$dst_f = TMP_UPLOAD ."customer_name_".$time.".xls";
			//echo $dst_f;die;  // /tmp/IPPBX_Tmp_Upload/customer_name_1364281330.xls
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrCustomer = $data->sheets[0];
			array_shift($arrCustomer["cells"]);
			$arrCustomer = $arrCustomer["cells"];

			$userArr = readU();
			$cn_user = $userArr['cn_user'];
			$user_work = array_flip($cn_user);
			$deptId_user = $userArr["deptId_user"];


			//dump($arrCustomer);die;
			//dump($arrCustomer);die;
			$total_num = count($arrCustomer);

			foreach($arrCustomer as $val){
				if( !in_array($val[$field_key["phone1"]+1],$phone) ){
					$ph_unique[] = $val;
				}
			}

			foreach($ph_unique as $val){
				if( !in_array($val[$field_key["qq_number"]+1],$qq2) ){
					$qq_unique[] = $val;
				}
			}
			$num = count($qq_unique);
			$nouse_num = $total_num-$num;
			$arrCustomer = $qq_unique;


			$i = 0;
			foreach($arrCustomer as &$val){
				$val[$field_key["createuser"]+1] = $user_work[$val[$field_key["createuser"]+1]];
				$arrCustomer[$i]['deptID'] = $deptId_user[$val[$field_key["createuser"]+1]];
				$i++;
			}

			//dump($arrCustomer);die;

			foreach( $arrCustomer AS $row ){
				$str = "(";
				for($i=1;$i<=($count-2);$i++){
					$str .= "'" .$row[$i]. "',";
				}
				$str .= "'" .date("Y-m-d H:i:s"). "',";
				//$str .= "'" .$workname. "',";
				$str .= "'" .$row['deptID']. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
		}else{
			//goBack("文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!","");
			echo json_encode(array('msg'=>'文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!'));
			exit;
		}

		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			//dump($sql);die;
			$result = $customer->execute($sql);
		}
		//判断导入结果
		if( $result ){
			echo json_encode(array('success'=>true,'msg'=>'成功导入 '.$result.' 条客户资料，有'.$nouse_num.'条为号码已存在或QQ号已存在的!'));
		}else{
			echo json_encode(array('msg'=>'导入资料时出现未知错误！手机号或QQ号已存在！'));
		}
	}

	//导入客户资料
	function doImportCustomerData3(){
		set_time_limit(0);
		ini_set('memory_limit','512M');
		if( empty($_FILES["customer_name"]["tmp_name"]) ){
			//goBack("请选择文件!","");
			echo json_encode(array('msg'=>'请选择文件!'));
			exit;
		}
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
			if($val["text_type"] == "2"){
				$selectField[] = $val["en_name"];
			}
			//==========================选择框开始===============================
			if($val["text_type"] == "2" && $val['list_enabled'] == 'Y'){
				$ttField[] = $val["en_name"];
			}
			//==========================选择框结束===============================
		}
		//dump($selectField);die;
		//array_unshift($field,"createtime");
		//array_unshift($title,"创建时间");
		$count = count($field)+2;
		$fields = "`".implode("`,`",$field)."`".",`createtime`,`createuser`";
		//$fields = "`".implode("`,`",$field)."`".",`createtime`,`dept_id`";
		$time = time();
		$tmp_f = $_FILES["customer_name"]["tmp_name"];  // /tmp/phptBkWXb
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		$field_key = array_flip($field);
		//dump($field_key);die;
		//dump($suffix);die;

		$customer = new Model("customer");

		//-------------------去重
		$customerData = $customer->field($fields)->select();
		foreach($customerData as $val){
			$phone[] = $val["phone1"];
			$qq[] = $val["qq_number"];
		}
		foreach($qq as $v){
			if($v){
				$qq2[] = $v;
			}
		}
		//-------------------去重
		//dump($phone);
		$sql = "insert into customer($fields) values ";
		$value = "";
		//dump($sql);die;
		if($suffix == "xls" || $suffix == "xlsx" ){
			if($suffix == "xls"){
			$dst_f = TMP_UPLOAD ."customer_name_".$time.".xls";
			}
			if($suffix == "xlsx"){
			$dst_f = TMP_UPLOAD ."customer_name_".$time.".xlsx";
			}
			//echo $dst_f;die;  // /tmp/IPPBX_Tmp_Upload/customer_name_1364281330.xls
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrCustomer = $data->sheets[0];
			array_shift($arrCustomer["cells"]);
			$arrCustomer = $arrCustomer["cells"];
			$avg = ceil(count($arrCustomer)/8);

			$userArr = readU();
			$cn_user = $userArr['cn_user'];
			$user_work = array_flip($cn_user);
			$deptId_user = $userArr["deptId_user"];

			//dump($arrCustomer);die;
			$systemParam = readS();
			$total_num = count($arrCustomer);
			//dump($systemParam["repeatImportCustomer"]);die;

			//去重start【根据系统参数设定的值去重】
			if($systemParam["repeatImportCustomer"] == "phone"){
				foreach($arrCustomer as &$val){
					$val[$field_key["phone1"]+1] = str_replace("-","",trim($val[$field_key["phone1"]+1]));
					if( !in_array($val[$field_key["phone1"]+1],$phone) ){
						$ph_unique[] = $val;
					}
				}
				$num = count($ph_unique);
				$nouse_num = $total_num-$num;
				$msg = "有 $nouse_num 条为手机号码已存在的!";
				$arrCustomer = $ph_unique;
			}elseif($systemParam["repeatImportCustomer"] == "qq"){
				foreach($arrCustomer as $val){
					if( !in_array($val[$field_key["qq_number"]+1],$qq2) ){
						$qq_unique[] = $val;
					}
				}
				$num = count($qq_unique);
				$nouse_num = $total_num-$num;
				$msg = "有 $nouse_num 条为QQ号已存在的!";
				$arrCustomer = $qq_unique;
			}elseif($systemParam["repeatImportCustomer"] == "all"){
				foreach($arrCustomer as $val){
					$val[$field_key["phone1"]+1] = str_replace("-","",trim($val[$field_key["phone1"]+1]));
					if( !in_array($val[$field_key["phone1"]+1],$phone) ){
						$ph_unique[] = $val;
					}
				}
				foreach($ph_unique as $val){
					if( !in_array($val[$field_key["qq_number"]+1],$qq2) ){
						$qq_unique[] = $val;
					}
				}
				$num = count($qq_unique);
				$nouse_num = $total_num-$num;
				$msg = "有 $nouse_num 条为手机号码或QQ号已存在的!";
				$arrCustomer = $qq_unique;
			}else{
				$arrCustomer = $arrCustomer;
				$msg = "";
			}
			//dump($arrCustomer);die;
			/*
			$total_num = count($arrCustomer);

			foreach($arrCustomer as $val){
				if( !in_array($val[$field_key["phone1"]+1],$phone) ){
					$ph_unique[] = $val;
				}
			}

			foreach($ph_unique as $val){
				if( !in_array($val[$field_key["qq_number"]+1],$qq2) ){
					$qq_unique[] = $val;
				}
			}
			$num = count($qq_unique);
			$nouse_num = $total_num-$num;
			$arrCustomer = $qq_unique;

			*/
			//去重end【根据系统参数设定的值去重】

			//==========================选择框开始===============================
			$selectValue = getSelectCache();
			foreach($selectValue as $key=>$val){
				$selectKey[$key] = array_flip($val);
			}

			foreach($selectKey as $key=>$val){
				if(in_array($key,$ttField)){
					$arrK[$key] = $val;
				}
			}
			$selectKey = $arrK;
			//dump($arrK);die;
			//dump($selectKey);die;
			$selectF = array_flip($selectField);
			$i = 0;
			foreach($selectKey as $key=>$val){
				$selectKey[$key]['index'] = $key;
				$i++;
			}
			foreach($selectKey as &$val){
				$val['index'] = $field_key[$val['index']]+1;
				$selectKey2[$val['index']] = $val;
			}

			foreach($selectKey2 as $val){
				$index[] = $val['index'];
			}


			foreach($arrK as $k=>$vm){
				$cm_select[] = $field_key[$k]+1;
			}


			foreach($cm_select as $vm){
				foreach($arrCustomer as &$val){
					$tt = $selectKey2[$vm][$val[$vm]];
					$val[$vm] = $tt;
				}
			}
			//==========================选择框结束==============================

			//dump($field_key);
			//dump($arrCustomer);die;



			$workname = $_GET['workname'];
			foreach($arrCustomer as &$val){
				$val["createtime"] = date("Y-m-d H:i:s");
				if($workname){
					$val["workname"] = $workname;
				}else{
					$val["workname"] = $_SESSION['user_info']['username'];
				}
			}

			//dump($arrCustomer);die;


			foreach( $arrCustomer AS $key=>&$row ){
				$row[$field_key["phone1"]+1] = str_replace("-","",trim($row[$field_key["phone1"]+1]));
				$row[$field_key["phone2"]+1] = str_replace("-","",trim($row[$field_key["phone2"]+1]));

				$str = "(";
				for($i=1;$i<=($count-2);$i++){
					$str .= "'" .str_replace("'","",$row[$i]). "',";
				}
				//$str .= "'" .date("Y-m-d H:i:s"). "',";
				$str .= "'" .$row['createtime']. "',";
				$str .= "'" .$row['workname']. "'";

				$str .= ")";

				if(count($arrCustomer) >= 8){
					if($key>$avg && $key<=$avg*2){
						$value2 .= empty($value2)?$str:",$str";
					}elseif($key>$avg*2 && $key<=$avg*3){
						$value3 .= empty($value3)?$str:",$str";
					}elseif($key>$avg*3 && $key<=$avg*4){
						$value4 .= empty($value4)?$str:",$str";
					}elseif($key>$avg*4 && $key<=$avg*5){
						$value5 .= empty($value5)?$str:",$str";
					}elseif($key>$avg*5 && $key<=$avg*6){
						$value6.= empty($value6)?$str:",$str";
					}elseif($key>$avg*6 && $key<=$avg*7){
						$value7 .= empty($value7)?$str:",$str";
					}elseif($key>$avg*7 && $key<=$avg*8){
						$value8 .= empty($value8)?$str:",$str";
					}elseif($key>$avg*8 && $key<=$avg*9){
						$value9 .= empty($value9)?$str:",$str";
					}elseif($key<=$avg){
						$value .= empty($value)?$str:",$str";
					}
				}else{
					$value .= empty($value)?$str:",$str";
				}
				//$value .= empty($value)?"$str":",$str";


			}


		}else{
			//goBack("文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!","");
			echo json_encode(array('msg'=>'文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!'));
			exit;
		}
		/*
		dump($value);
		dump($value1);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);
		dump($value9);
		dump($arrCustomer);die;
		*/
		//导入动作，执行SQL语句
		//$result = false;
		if( $value ){
			$sql .= $value;
			//dump($sql);die;
			$result = $customer->execute($sql);
		}
		unset($sql);
		$sql = "insert into customer($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			//echo $sql;die;
			$result2 = $customer->execute($sql);
		}
		unset($sql);
		$sql = "insert into customer($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			//echo $sql;die;
			$result3 = $customer->execute($sql);
		}
		unset($sql);
		$sql = "insert into customer($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			//echo $sql;die;
			$result4 = $customer->execute($sql);
		}
		unset($sql);
		$sql = "insert into customer($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			//echo $sql;die;
			$result5 = $customer->execute($sql);
		}
		unset($sql);
		$sql = "insert into customer($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			//echo $sql;die;
			$result6 = $customer->execute($sql);
		}
		unset($sql);
		$sql = "insert into customer($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			//echo $sql;die;
			$result7 = $customer->execute($sql);
		}
		unset($sql);
		$sql = "insert into customer($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			//echo $sql;die;
			$result8 = $customer->execute($sql);
		}
		unset($sql);
		$sql = "insert into customer($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			//echo $sql;die;
			$result9 = $customer->execute($sql);
		}

		//判断导入结果
		if( $result !== false ){
			$totals = count($arrCustomer);
			$paraSys = readS();
			if( $paraSys["repeatImportCustomer"] == "phone" || $paraSys["repeatImportCustomer"] == "all" ){
				$cfnum = $this->removePhone();
			}else{
				$cfnum = "0";
			}
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9-$cfnum;
			$fail = $totals - $num;
			//$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			//echo json_encode(array('success'=>true,'msg'=>'成功导入 '.$result.' 条客户资料，有'.$nouse_num.'条为号码已存在或QQ号已存在的!'));
			$message = "成功导入 $num 条客户资料! ".$msg;
			echo json_encode(array('success'=>true,'msg'=>$message));
		}else{
			echo json_encode(array('msg'=>'导入资料时出现未知错误！excel中可能有特殊字符'));
		}
	}

	function removePhone(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$customer = new Model("customer");
		$arrData = $customer->order("id asc")->field("id,phone1")->select();
		$arrTmp = $this->getRepeatArray($arrData,"phone1");
		foreach($arrTmp as $val){
			$arrId[] = $val["id"];
		}
		//dump($arrTmp);die;
		$strId = "'".implode("','",$arrId)."'";
		$res = $customer->where("id in ($strId)")->delete();
	}

	function getRepeatArray($arr, $key){
		$tmp_arr = array();
		foreach($arr as $k => $v){
			if(in_array($v[$key], $tmp_arr)){
				$tmp[] = $v;
			}else{
				$tmp_arr[] = $v[$key];
			}
		}
		sort($tmp);
		return $tmp;
	}

	function removePhone2(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$customer = new Model("customer");
		$sql = "DELETE FROM customer WHERE id IN(SELECT id FROM (SELECT MAX(id) AS id, COUNT(phone1) AS COUNT  FROM customer GROUP BY phone1 HAVING COUNT > 1 ORDER BY COUNT DESC) AS tab)";
		$num = $customer->execute($sql);
		return $num;

		/*
		$customer = new Model("customer");
		$total = $customer->count();
		$avg = ceil($total/5000);
		$sql = "SELECT COUNT(phone1) FROM customer GROUP BY phone1 HAVING COUNT(phone1) > 1";
		$re = $customer->query($sql);
		if(!$re){
			return false;
		}
		//$del_sql = "DELETE FROM sales_customer_".$id." WHERE id IN(SELECT id FROM (SELECT MAX(id) AS id, COUNT(phone1) AS COUNT  FROM sales_customer_".$id." GROUP BY phone1 HAVING COUNT > 1 ORDER BY COUNT DESC) AS tab)";
		//$num = $customer->execute($del_sql);

		//php删除重复数据的方法
		if($avg>1){
			for($i=1;$i<=$avg;$i++){
				$arrNum[$i] = $this->removeDuplicateData();
			}
			$num = array_sum($arrNum);
		}else{
			$num = $this->removeDuplicateData();
		}
		return $num;
		*/
	}


	function removeDuplicateData(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$customer = new Model("customer");
		$cmData = $customer->field("id,phone1")->select();
		foreach($cmData as $val){
			$tt[] = $val["phone1"];
		}
		$yy = array_unique($tt);
		$kk = $this->FetchRepeatMemberInArray($tt);
		foreach($kk as $key=>$val){
			if($val){
				$phone[] = $val;
			}
		}
		//dump($phone);die;
		$tt = array_slice($phone,0,5000);
		$ss = "'".implode("','",$tt)."'";
		//dump(explode(",",$ss));die;
		$usersList = $customer->field("id,phone1")->where("phone1 in ($ss)")->select();
		$arr = $this->assoc_unique($usersList,"phone1");
		//dump($usersList);die;
		foreach($arr as $val){
			$ids[] = $val["id"];
		}
		$id = "'".implode("','",$ids)."'";
		$res = $customer->where("id in ($id)")->delete();
		//echo "1111111<br>";
		sleep(1);
		return $res;
	}


	function FetchRepeatMemberInArray($array) {
		$unique_arr = array_unique ( $array );
		$repeat = array_diff_assoc ( $array, $unique_arr );

		$repeat_arr = array_unique($repeat);
		return $repeat_arr;
	}

	function assoc_unique($arr, $key){
	   $tmp_arr = array();
	   foreach($arr as $k => $v){
		 if(in_array($v[$key], $tmp_arr)){
		   unset($arr[$k]);
		}else{
		  $tmp_arr[] = $v[$key];
		}
	  }
	  sort($arr);
	  return $arr;
	}

	/*
	//导入客户资料
	function doImportCustomerData(){
		if( empty($_FILES["customer_name"]["tmp_name"]) ){
			//goBack("请选择文件!","");
			echo json_encode(array('msg'=>'请选择文件!'));
			exit;
		}

		$time = time();
		$tmp_f = $_FILES["customer_name"]["tmp_name"];  // /tmp/phptBkWXb
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));
		//dump($suffix);die;

		$customer = new Model("customer");
		$sql = "insert into customer(name, company, phone2, phone1, email, fax, zipcode, groups, grade, address,  description,createuser,createtime) values ";
		$value = "";

		if($suffix == "xls"){
			$dst_f = TMP_UPLOAD ."customer_name_".$time.".xls";
			//echo $dst_f;die;  // /tmp/IPPBX_Tmp_Upload/customer_name_1364281330.xls
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrCustomer = $data->sheets[0];
			array_shift($arrCustomer["cells"]);
			$arrCustomer = $arrCustomer["cells"];
			//dump($arrCustomer);die;
			foreach( $arrCustomer AS $row ){
				$str = "(";
				$str .= "'" .$row[1]. "',";  	 //客户姓名
				$str .= "'" .$row[2]. "',";		//公司名称
				$str .= "'" .$row[3]. "',";		//电话
				$str .= "'" .$row[4]. "',";		//手机
				$str .= "'" .$row[5]. "',";		//邮件地址
				$str .= "'" .$row[6]. "',";		//传真
				$str .= "'" .$row[7]. "',";		//邮编
				$str .= "'" .$row[8]. "',";		//群组
				$str .= "'" .$row[9]. "',";		//等级
				$str .= "'" .$row[10]. "',";	//客户地址
				$str .= "'" .$row[11]. "',";	//描述
				$str .= "'" .$_SESSION['user_info']['username']. "',";		//创建人
				$str .= "'" .date("Y-m-d H:i:s"). "'";			//创建时间//----------------最后一个不需要","

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
		}else{
			//goBack("文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!","");
			echo json_encode(array('msg'=>'文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!'));
			exit;
		}

		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $customer->execute($sql);
		}
		//判断导入结果
		if( $result ){
			echo json_encode(array('success'=>true,'msg'=>'成功导入 '.$result.' 条客户资料'));
		}else{
			echo json_encode(array('msg'=>'导入资料时出现未知错误'));
		}
	}
	*/

	//类库导Excel  将导出的资料作为客户下载的模板
	function downloadCustomer(){
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
		}
		//array_unshift($field,"createtime");
		//array_unshift($title,"创建时间");
		$count = count($field);
		$fields = "`".implode("`,`",$field)."`";
		$customer = new Model("customer");
		$cmlist = $customer->order("createtime desc")->field($fields)->select();

		$row = getSelectCache();
		//dump($row);
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}

		vendor("PHPExcel176.PHPExcel");
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($cmlist as $val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				if($start_row<5){
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
				}
			}
			$start_row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$name = "客户资料";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

	}





	//类库导Excel  将导出的资料作为客户下载的模板
	function downloadCustomer2(){
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
		}
		//array_unshift($field,"createtime");
		//array_unshift($title,"创建时间");
		$count = count($field);
		$fields = "`".implode("`,`",$field)."`";
		$customer = new Model("customer");
		$cmlist = $customer->order("createtime desc")->field($fields)->limit('10')->select();

		$row = getSelectCache();
		//dump($row);
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}

		vendor("PHPExcel176.PHPExcel");
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($cmlist as $val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				if($start_row<5){
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
				}
			}
			$start_row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$name = "客户资料";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

	}



	//下载客户资料模板
	function DownloadTemplate(){
		$file = $_GET['file'];
		$path = "/var/www/html/BGCC/Tpl/Public/download/";
		if($file){
			$realfile = $path.$file;
		}else{
			$realfile = $path."import_coustomer_XLS.xls";
		}
		if(!file_exists($realfile)){
			$this->error("File $realfile is not exist!");
		}
        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}

	/*
	//类库导Excel  将导出的资料作为客户下载的模板
	function downloadCustomer(){
		$username = $_SESSION["user_info"]["username"];
		$customer = new Model("customer");
		if($username == "admin"){
			$cmlist = $customer->order("createtime desc")->select();
		}else{
			$cmlist = $customer->order("createtime desc")->where("createuser = '$username'")->select();
		}
		$counts = count($cmlist);
		if($counts){

			vendor("PHPExcel176.PHPExcel");
			$objPHPExcel = new PHPExcel();

			// Set properties
			$objPHPExcel->getProperties()->setCreator("ctos")
				->setLastModifiedBy("ctos")
				->setTitle("Office 2007 XLSX Test Document")
				->setSubject("Office 2007 XLSX Test Document")
				->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
				->setKeywords("office 2007 openxml php")
				->setCategory("Test result file");

			//设置单元格（列）的宽度 水平居中
			for($j='A';$j<='M';$j++){
				$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(20);
				$objPHPExcel->getActiveSheet()->getStyle($j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}

		//合并单元格
		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

			//设置表 标题内容
			$objPHPExcel->setActiveSheetIndex()
				->setCellValue('A1', '客户姓名 [必填]')
				->setCellValue('B1', '公司名称')
				->setCellValue('C1', '电话')
				->setCellValue('D1', '手机 [必填]')
				->setCellValue('E1', '邮件地址')
				->setCellValue('F1', '传真')
				->setCellValue('G1', '邮编')
				->setCellValue('H1', '群组')
				->setCellValue('I1', '等级')
				->setCellValue('J1', '客户地址')
				->setCellValue('K1', '业务员')
				->setCellValue('L1', '描述');
			$i = 2;
			foreach($cmlist as $val){
				if($i < 5){
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $val['name']);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $val['company']);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $val['phone2']);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val['phone1']);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val['email']);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val['fax']);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['zipcode']);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val['groups']);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val['grade']);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val['address']);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $val['createuser']);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $val['description']);
				}
				$i++;
			}

			$objPHPExcel->setActiveSheetIndex(0);

			$name = "客户资料模板";
			$filename = iconv("utf-8","gb2312",$name);
			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		}else{
			$this->DownloadTemplate();
		}
	}
	*/

	//分配客户资料
	function allocationCustomerData(){
		$id = $_GET["id"];
		$level = $_GET["level"];
		$level_dept = explode(",",$level);
		//dump($level_dept);
		$arrId = explode(",",$id);
		$count = count($arrId);
		$customer = new Model("customer");
		if($level_dept[0] == "department"){
			$arrData = array(
				"share_deptid" => $level_dept[1],
			);

		}else{
			$arrData = array(
				//"createuser" => $_REQUEST["deptuser"],
				"dealuser" => $_REQUEST["deptuser"],
			);
		}
		//dump($arrData);die;
		for($i=0;$i<$count;$i++){
			$result[$i] = $customer->data($arrData)->where("id = $arrId[$i]")->save();
		}
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'客户资料分配成功'));
		} else {
			echo json_encode(array('msg'=>'出现未知错误！'));
		}
	}

	//取消共享资料
	function unsharingData(){
		$id = $_REQUEST["id"];
		//dump($id);die;
		$customer = new Model("customer");
		$arrData = array(
			"share_deptid" => "",
			"dealuser" => "",
		);
		$result = $customer->data($arrData)->where("id in ($id)")->save();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'取消共享资料成功'));
		} else {
			echo json_encode(array('msg'=>'出现未知错误！'));
		}
	}

	function getTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
					$v['children'] = $this->getTree($data, $v['tid']);
					if ( empty($v["children"])  )  unset($v['children']) ;
					/*
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['children'] =  array(array());
					*/
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['state'] =  'open';
				 $tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	function userProcess(){
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();

		$users = new Model("users");
		$userList = $users->field("username as text,d_id as pid,cn_name")->select();

		$j = 0;
		foreach($dept as $vm){
			$dept[$j]['iconCls'] = "door";
			$dept[$j]['state'] = "closed";
			$dept[$j]['attributes'] = "department".",".$vm['tid'];
			$j++;
		}
		unset($j);

		$i = 0;
		foreach($userList as $v){
			$userList[$i]['iconCls'] = "icon-user_chat";
			$userList[$i]['attributes'] = "user".",".$v['cn_name'];
			$i++;
		}
		unset($i);

		foreach($userList as $val){
			$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}
        $arrTree = $this->getTree($dept,0);
		//dump($arrTree);die;
		$strJSON = json_encode($arrTree);
		echo ($strJSON);

	}


	/*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}


	//导入客户资料
	function doImportCustomerData5(){
		$username = $_SESSION["user_info"]["username"];
		if( empty($_FILES["customer_name"]["tmp_name"]) ){
			//goBack("请选择文件!","");
			echo json_encode(array('msg'=>'请选择文件!'));
			exit;
		}
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
			/*
			if($val["text_type"] == "2"){
				$selectField[] = $val["en_name"];
			}
			*/
			//==========================选择框开始===============================
			if($val["text_type"] == "2" && $val['list_enabled'] == 'Y'){
				$ttField[] = $val["en_name"];
			}
			//==========================选择框结束===============================
		}
		//dump($selectField);die;
		//array_unshift($field,"createtime");
		//array_unshift($title,"创建时间");
		$count = count($field)+2;
		$fields = "`".implode("`,`",$field)."`".",`createtime`,`createuser`,`dept_id`";
		$time = time();
		$tmp_f = $_FILES["customer_name"]["tmp_name"];  // /tmp/phptBkWXb
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		$field_key = array_flip($field);
		//dump($field_key);die;
		//dump($count);die;

		$customer = new Model("customer");

		//-------------------去重
		$customerData = $customer->field($fields)->select();
		foreach($customerData as $val){
			$phone[] = $val["phone1"];
			$qq[] = $val["qq_number"];
		}
		foreach($qq as $v){
			if($v){
				$qq2[] = $v;
			}
		}
		//-------------------去重
		//dump($phone);
		$sql = "insert into customer($fields) values ";
		$value = "";
		//dump($sql);die;
		if($suffix == "xls"){
			$dst_f = TMP_UPLOAD ."customer_name_".$time.".xls";
			//echo $dst_f;die;  // /tmp/IPPBX_Tmp_Upload/customer_name_1364281330.xls
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrCustomer = $data->sheets[0];
			array_shift($arrCustomer["cells"]);
			$arrCustomer = $arrCustomer["cells"];

			$userArr = readU();
			$deptId_user = $userArr["deptId_user"];
			if($_REQUEST['workname']){
				$workname = $_REQUEST['workname'];
				$dept_id = $deptId_user[$workname];
			}else{
				$workname = $_SESSION['user_info']['username'];
				$dept_id = $_SESSION['user_info']['d_id'];
			}
			//dump($arrCustomer);die;
			$total_num = count($arrCustomer);



			//==========================选择框开始===============================
			$selectValue = require 'BGCC/Conf/selectCache2.php';
			foreach($selectValue as $key=>$val){
				$selectKey[$key] = array_flip($val);
			}

			foreach($selectKey as $key=>$val){
				if(in_array($key,$ttField)){
					$arrK[$key] = $val;
				}
			}
			$selectKey = $arrK;
			//dump($arrK);die;
			//dump($selectKey);die;
			//$selectF = array_flip($selectField);
			$i = 0;
			foreach($selectKey as $key=>$val){
				$selectKey[$key]['index'] = $key;
				$i++;
			}
			foreach($selectKey as &$val){
				$val['index'] = $field_key[$val['index']]+1;
				$selectKey2[$val['index']] = $val;
			}
			/*
			foreach($selectKey2 as $val){
				$index[] = $val['index'];
			}
			*/

			foreach($arrK as $k=>$vm){
				$cm_select[] = $field_key[$k]+1;
			}


			foreach($cm_select as $vm){
				foreach($arrCustomer as &$val){
					$tt = $selectKey2[$vm][$val[$vm]];
					$val[$vm] = $tt;
				}
			}
			//==========================选择框结束=================================================
			//dump($dd);
			dump($arrCustomer);die;


			foreach($arrCustomer as $val){
				if( !in_array($val[$field_key["phone1"]+1],$phone) ){
					$ph_unique[] = $val;
				}
			}

			foreach($ph_unique as $val){
				if( !in_array($val[$field_key["qq_number"]+1],$qq2) ){
					$qq_unique[] = $val;
				}
			}
			$num = count($qq_unique);
			$nouse_num = $total_num-$num;
			$arrCustomer = $qq_unique;
			/*
			$selectValue = require 'BGCC/Conf/selectCache2.php';
			foreach($selectValue as $key=>$val){
				$selectKey[$key] = array_flip($val);
			}

			$selectF = array_flip($selectField);
			foreach($selectKey as $key=>$val){
				$selectKey2[$selectF[$key]+1] = $val;
			}

			foreach($selectF as $vm){
				foreach($arrCustomer as &$val){
					$status = $selectKey2[$val[$vm+1]];
					$val[$vm+1] = $status;
				}
			}
			//dump($field_key);
			dump($arrCustomer);die;
			dump($selectF);
			dump($selectKey);die;*/
			//dump($arrCustomer);die;

			foreach( $arrCustomer AS $row ){
				$str = "(";
				for($i=1;$i<=($count-2);$i++){
					$str .= "'" .$row[$i]. "',";
				}
				$str .= "'" .date("Y-m-d H:i:s"). "',";
				$str .= "'" .$workname. "',";
				$str .= "'" .$dept_id. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
		}else{
			//goBack("文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!","");
			echo json_encode(array('msg'=>'文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!'));
			exit;
		}

		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			//dump($sql);die;
			$result = $customer->execute($sql);
		}
		//判断导入结果
		if( $result ){
			echo json_encode(array('success'=>true,'msg'=>'成功导入 '.$result.' 条客户资料，有'.$nouse_num.'条为号码已存在或QQ号已存在的!'));
		}else{
			echo json_encode(array('msg'=>'导入资料时出现未知错误！手机号或QQ号已存在！'));
		}
	}

	function aftermarketData(){
		$servicerecords = new Model("servicerecords");
		$customer_id = $_REQUEST["customer_id"];
		$where = "s.customer_id = '$customer_id'";
		$count = $servicerecords->table("servicerecords s")->field("s.id,s.createtime,s.seat,s.status,s.recipientseat,s.phone,s.content,s.content,s.customer_id,s.servicetype_id,s.recording,s.modifytime,st.servicename")->join("servicetype st on s.servicetype_id = st.id")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $servicerecords->order("s.createtime desc")->table("servicerecords s")->field("s.id,s.createtime,s.seat,s.status,s.recipientseat,s.phone,s.content,s.content,s.customer_id,s.servicetype_id,s.recording,s.modifytime,st.servicename")->join("servicetype st on s.servicetype_id = st.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		//echo $servicerecords->getLastSql();die;
		foreach($arrData as &$val){
			$arrTmp = explode('.',$val["recording"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["recording"].".WAV";

			if(file_exists($WAVfile) ){
				$val["recording"] = trim($val["recording"]);
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["recording"])."'".")\" >播放</a>";
			}else{
				$val['operating'] = "";
			}
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);

	}

	function insertAftermarket(){
		$time = date("Y-m-d H:i:s");
		$username = $_SESSION["user_info"]["username"];
		$d_id = $_SESSION["user_info"]["d_id"];

		$service = new Model("servicerecords");
		$arrData = array(
			"createtime"=>$time,
			"servicetype_id"=>$_REQUEST["servicetype_id"],
			"status"=>$_REQUEST["status"],
			"phone"=>$_REQUEST["phone"],
			"content"=>$_REQUEST["content"],
			"seat"=>$username,
			"dept_id"=>$d_id,
			"customer_id"=>$_REQUEST["id"],
		);
		//dump($arrData);die;
		$result = $service->data($arrData)->add();
		//echo $service->getLastSql();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateAftermarket(){
		if($_REQUEST['message_phone'] == "ms"){
			$phone = $_REQUEST['mess_phone'];
		}else{
			$phone = $_REQUEST['phone'];
		}
		$id = $_REQUEST['id'];
		$time = date("Y-m-d H:i:s");
		$service = new Model("servicerecords");
		$arrData = array(
			"modifytime"=>$time,
			"servicetype_id"=>$_REQUEST["servicetype_id"],
			"status"=>$_REQUEST["status"],
			//"phone"=>$phone,
			"content"=>$_REQUEST["content"],
		);
		//dump($arrData);die;
		$result = $service->data($arrData)->where("id = $id")->save();
		//echo $service->getLastSql();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function fileContentData(){
		$customerid = $_REQUEST['customerid'];
		$where = "f.customerid = '$customerid'";
		$filecontent = new Model("filecontent");
		$count = $filecontent->table("filecontent f")->field("f.id,f.createtime,f.filetype_id,f.customerid,f.filecontentname,f.filepath,f.description,f.createuser,ft.filename")->join("filetype ft on f.filetype_id=ft.id")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $filecontent->order("f.createtime desc")->table("filecontent f")->field("f.id,f.createtime,f.filetype_id,f.customerid,f.filecontentname,f.filepath,f.description,f.createuser,f.contract_amount,f.repayment_installments,f.has_also_amount,f.contract_number,f.repay_plan_date,f.createuser,f.repayment_status,ft.filename")->join("filetype ft on f.filetype_id=ft.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();


		foreach($arrData as &$val){
			$val["operating"] = "<a href='agent.php?m=CustomerData&a=downloadFile&path=".$val["filepath"]."&filename=".$val["filecontentname"]."' >下载</a> &nbsp;&nbsp;&nbsp;";

			if($val["filetype_id"] == "1"){
				$status = $val["contract_amount"]-$val["has_also_amount"];
				/*
				if($status == $val["contract_amount"]){
					$val["repayment_status"] = "未付款";
				}elseif($status == "0"){
					$val["repayment_status"] = "付款完成";
				}else{
					$val["repayment_status"] = "部分已付款";
				}
				*/
				$val["operating"] .=  "<a href='javascript:void(0);' onclick=\"payInfo("."'".$val["id"]."','".$val["customerid"]."','".$val["contract_number"]."'".")\" >付款信息</a>";
			}
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function transferData(){
		$id = $_REQUEST["customerid"];
		$process = new Model("process");
		import('ORG.Util.Page');
		$count = $process->where("customerid = $id")->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $process->order("p.createtime desc")->field("c.name,p.createtime,p.recipient,p.forwardedname,p.noticetype,p.modifycontent")->table("`process` p")->join("customer c on p.customerid = c.id")->join("servicerecords s on p.service_id = s.id ")->limit($page->firstRow.','.$page->listRows)->where("customerid = $id")->select();

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	function customerTansferData(){
		$customerid = $_REQUEST['customerid'];

		$where = "customer_id = '$customerid' ";
		//echo $where;die;

		$customer_transferees = new Model("customer_transferees");
		$count = $customer_transferees->table("customer_transferees t")->join("customer c on t.customer_id = c.id")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$transData = $customer_transferees->order("t.createtime desc")->table("customer_transferees t")->field("t.forwardedname, t.recipient, t.customer_id,t.createtime,t.transferType,c.name")->join("customer c on t.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		//echo $customer_transferees->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$row = array("in"=>"呼入平台转交","out"=>"外呼平台转交");
		foreach($transData as &$val){
			$transferType = $row[$val["transferType"]];
			$val["transferType"] = $transferType;

			$val['forwardedname_cn_name'] = $cnName[$val["forwardedname"]];
			$val['recipient_cn_name'] = $cnName[$val["recipient"]];
		}



		$rowsList = count($transData) ? $transData : false;
		$arrTrans["total"] = $count;
		$arrTrans["rows"] = $rowsList;

		echo json_encode($arrTrans);
	}

	//下一条
	function nextCustomer(){
		$customer = new Model("customer");
		//$sears = base64_decode($_GET["sears"]);
		$sears = array_shift(explode("LIMIT",base64_decode(str_replace(" ","+",$_GET[sears]))));
		//dump($sears);die;
		$id = $_GET["id"];
		$cmData = $customer->query($sears);

		foreach($cmData as $val){
			$cmId[] = $val["id"];
		}
		$srId = array_flip($cmId);
		$afterId = $cmId[$srId[$id]+1];
		$arr = $customer->field("id,phone1,name")->where("id = '$afterId'")->find();
		$title = $arr["name"];

		echo json_encode(array('id'=>$afterId,'title'=>$title));
	}

	function lastCustomer(){
		$customer = new Model("customer");
		//$sears = base64_decode($_GET["sears"]);
		$sears = array_shift(explode("LIMIT",base64_decode(str_replace(" ","+",$_GET[sears]))));
		$id = $_GET["id"];
		$cmData = $customer->query($sears);

		foreach($cmData as $val){
			$cmId[] = $val["id"];
		}

		$srId = array_flip($cmId);
		$afterId = $cmId[$srId[$id]-1];
		$arr = $customer->field("id,phone1,name")->where("id = '$afterId'")->find();
		$title = $arr["name"];

		echo json_encode(array('id'=>$afterId,'title'=>$title));
	}

}


?>
