<?php
class WaitMatterAction extends Action{
	function WaitMatterList(){
		checkLogin();
		$wait = new Model("waitmatter");
		$list = $wait->select();
		$this->assign("wlist",$list);

		$users = new Model("users");
		$ulist = $users->select();
		$this->assign("ulist",$ulist);
		$waitType = new Model("waitmattertype");
		$waitTypeList = $waitType->where('id <> 3')->select();
		$this->assign('waitTypeList',$waitTypeList);

		//分配增删改的权限
		$menuname = "Wait Matter";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function waitMatterData(){
		$wait = new Model("waitmatter");
		$count = $wait->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$waitList = $wait->order("createtime desc")->table("waitmatter w")->field("w.id,w.createtime,w.content,w.matterstype,w.username,w.status,w.status as tabstatus,w.remindertime,w.title,w.url, t.waitmattername")->join("waitmattertype t on w.matterstype = t.id")->limit($page->firstRow.','.$page->listRows)->select();
		$status_row = array('N'=>'未办','Y'=>'已办');
		foreach($waitList as &$val){
			$status = $status_row[$val['tabstatus']];
			$val['tabstatus'] = $status;
		}
		$rowsList = count($waitList) ? $waitList :false;
		$arrWaitData["total"] = $count;
		$arrWaitData["rows"] = $rowsList;
		echo json_encode($arrWaitData);
	}

	function insertWaitMatter(){
		$wait = new Model("waitmatter");
		$arrData = array(
			"createtime"=>date("Y-m-d H:i:s"),
			"username"=>$_POST["username"],
			"matterstype"=>$_POST["matterstype"],
			"status"=>$_POST["status"],
			"remindertime"=>$_POST["remindertime"],
			"content"=>$_POST["content"],
			"title"=>$_POST["title"],
			"url"=>$_POST["url"],
		);
		//dump($arrData);die;
		$result = $wait->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'事项添加成功！'));
		} else {
			echo json_encode(array('msg'=>'事项添加失败！'));
		}
	}

	function updateWaitMatter(){
		$id = $_REQUEST["id"];
		$wait = new Model("waitmatter");
		$arrData = array(
			"username"=>$_POST["username"],
			"matterstype"=>$_POST["matterstype"],
			"status"=>$_POST["status"],
			"remindertime"=>$_POST["remindertime"],
			"content"=>$_POST["content"],
			"title"=>$_POST["title"],
			"url"=>$_POST["url"],
		);
		$result = $wait->where("id = $id")->data($arrData)->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteWaitMatter(){
		$id = $_REQUEST["id"];
		$wait = new Model("waitmatter");
		$result = $wait->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}
}


?>
