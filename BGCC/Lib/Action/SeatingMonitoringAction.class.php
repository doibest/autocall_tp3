<?php
class SeatingMonitoringAction extends Action{
	function loadAGI(){
			global $asm;
			if (!isset($asm)) {
							if( $this->soap == "yes" ){ //soap调用
					include_once ("/var/lib/asterisk/agi-bin/phpagi-asmanager.php");

				}else{
					include_once ("/var/lib/asterisk/agi-bin/phpagi-asmanager.php");
				}

				$this->asm = new AGI_AsteriskManager();
				if(!$this->asm->connect(C('PBX_HOST'), C('PBX_USER'),C('PBX_PWD')))
				{
					echo "无法连接管理接口，退出.";
					return NULL;
				}
			}
	}

	function show_aststats2() {

		include_once ("/var/www/html/admin/functions.inc.php");
		include_once ("/var/www/html/admin/modules/dashboard/class.astinfo.php");
		import('ORG.Pbx.bmi');
		include_once ("/var/lib/asterisk/agi-bin/phpagi-asmanager.php");
		$asm = new AGI_AsteriskManager();
		if(!$asm->connect(C('PBX_HOST'), C('PBX_USER'),C('PBX_PWD')))
		{
			echo "无法连接管理接口，退出.";
			return NULL;
		}



		$response = $asm->send_request('Command',array('Command'=>"core show channels"));
		$astout = explode("\n",$response['data']);

		$external_calls = 0;
		$internal_calls = 0;
		$total_calls = 0;
		$total_channels = 0;

		foreach ($astout as $line) {
			if (preg_match('/s@macro-dialout/', $line)) {
				$external_calls++;
			} else if (preg_match('/s@macro-dial:/', $line)) {
				$internal_calls++;
			} else if (preg_match('/^(\d+) active channel/i', $line, $matches)) {
				$total_channels = $matches[1];
			} else if (preg_match('/^(\d+) active call/i', $line, $matches)) {
				$total_calls = $matches[1];
			}
		}
		$channels2 = array(
			'external_calls'=>$external_calls,
			'internal_calls'=>$internal_calls,
			'total_calls'=>$total_calls,
			'total_channels'=>$total_channels,
		);



		$astinfo = new astinfo($asm);
		$channels = $astinfo->get_channel_totals();
		//dump($channels);die;
		$devices = new Model("asterisk.devices");
		$res = $devices->field("id")->where(" `tech` IN ('sip', 'iax2') ")->select();
		foreach($res as $val){
			$tid[] = $val["id"];
		}
		$extenid = array_flip($tid);

		$conns = $astinfo->get_connections($extenid);
		$arrConns = array();
		$arrConns["users_online"] = $conns["users_online"];  //在线的IP电话
		$arrConns["trunks_online"] = $conns["trunks_online"];  //在线的IP中继
		$arrTmp = array_merge($channels2,$arrConns);
		//dump($arrTmp);
		return $arrTmp;
		//print_r($channels);die;
	}



	function cdrRate(){
		//dump( $_SESSION["user_info"]);die;
		$userArr = readU();
		$agent_type = $_SESSION["user_info"]['agent_type'];
		$where = "1 ";
		if($agent_type == 'sell'){
			$selluser = $userArr["sell_user"];
			$workno = "'".implode("','",$selluser)."'";
			$where .= " AND workno in ($workno)";
		}elseif($agent_type == 'service'){
			$serviceuser = $userArr["service_user"];
			$workno = "'".implode("','",$serviceuser)."'";
			$where .= " AND workno in ($workno)";
		}elseif($agent_type == 'admin'){
			$where .= " AND 1";
		}

		$arrDep = $this->getDepTreeArray();
		$d_id = $_SESSION["user_info"]["d_id"];
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$dept_id = rtrim($deptst,",");
		$where .= " AND dept_id in ($dept_id)";
		//dump($workno);
		//dump($userArr);die;

		$cdr = new Model("asteriskcdrdb.cdr");
		$date = date("Y-m-d");
		$callTotal = $cdr->where("$where AND Date(calldate) = '$date' AND calltype = 'IN' AND lastapp='Dial' ")->count();
		$answeredTotal = $cdr->where("$where AND Date(calldate) = '$date' AND disposition = 'ANSWERED' AND calltype = 'IN' AND lastapp='Dial'")->count();
		//echo $cdr->getLastSql();die;
		$answerRate = ceil(($answeredTotal/$callTotal)*100);
		$rateArr = array();
		$rateArr["total"] = $callTotal;
		$rateArr["answer"] = $answeredTotal;
		$rateArr["answerrate"] = $answerRate;
		//dump($rateArr);
		return $rateArr;
	}

	function SeatingMonitoring(){
		checkLogin();
		header("Content-Type:text/html; charset=utf-8");
		$u_extension = $_SESSION["user_info"]["extension"];

		//$en_name = $_REQUEST['en_name'];
		//$username = $_REQUEST['username'];
		$cn_name = $_REQUEST['cn_name'];
		$extension = $_REQUEST['extension'];
		$dept_id = $_REQUEST['dept_id'];//部门
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";

		$where = "1";
		$arrDep = $this->getDepTreeArray();
		$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$searchDeptId = rtrim($deptSet,",");
		$d_id = $_SESSION["user_info"]["d_id"];
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$dept_name_Set = rtrim($deptst,",");

		if($_SESSION['user_info']['username'] != 'admin'){
			if( !$dept_id ){
				$where .= " AND u.d_id IN ($dept_name_Set)";
			}
		}
		//$where = " AND u.extension <> ''";
		if( $searchmethod == "equal" ){ //精确查询，条件语句用等号
            if( $cn_name ) $where .= " AND u.cn_name='$cn_name' ";
            if( $extension ) $where .= " AND u.extension='$extension' ";
            //if( $username ) $where .= " AND u.username='$username' ";
            //if( $en_name ) $where .= " AND u.en_name='$en_name' ";
        }else{
            if( $cn_name ) $where .= " AND u.cn_name like '%$cn_name%' ";
            if( $extension ) $where .= " AND u.extension like '%$extension%' ";
            //if( $username ) $where .= " AND u.username like '%$username%' ";
            //if( $en_name ) $where .= " AND u.en_name like '%$en_name%' ";
        }
		if( $dept_id ){
			$where .= " AND u.d_id IN ($searchDeptId)";
		}

		//部门相关信息
		$sonStr = "0";
		$arrDep = $this->getDepTreeArray();
		//dump($arrDep);die;
		foreach( $arrDep AS $k=>$v){
			if($v['pid'] == 0)
				$sonStr .= "," . $v['id'];
		}
		$arrDep['0'] = Array("id"=>"0","pid" => "-1","name" => "所有部门","meAndSonId" => $sonStr );
		//dump($depTreeHTML);die;
		$depTreeHTML = $this->getDepList($arrDep,0,0);
		$this->assign('depTreeHTML',$depTreeHTML);
		if( $_REQUEST['dept'] ){
			//获取子部门字符集合
			//$deptSet = $arrDep[$d_id]['meAndSonId'];
			$dept_id = $_REQUEST['dept'];
			$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
			//dump($deptSet);die;
			//$where .= " AND dept IN ('财务部','IT部')";//部门编号
			$where .= " AND d.d_name IN (".$deptSet."'所有部门')";
			//dump($where);die;
		}

        import('ORG.Util.Page');
		import('ORG.Pbx.bmi');

		$bmi = new bmi();
		$extension_status = $bmi->getHints();   //获取分机状态
		//dump($extension_status);die;
		//$DND_status = $bmi->DBGet('DND','801');       //示忙状态
		//dump($DND_status);die;
		$cf_status = $bmi->getCFAll("cf");	 //转接
		$DND_status = $bmi->getDNDAll();
		//dump($DND_status);

		$channels = $bmi->getAllChannelsInfo("CallerID");
		//dump($channels);exit;

        $users=new Model("Users");
        $count=$users->table('users u')->order('u.username')->field('u.username,u.cn_name,r.r_name,d.d_name,u.extension,u.fax,u.email,u.en_name,u.extension_type')->join('department d on u.d_id=d.d_id ')->join('role r on u.r_id=r.r_id')->limit($page->firstRow.','.$page->listRows)->where("$where AND u.extension <> ' '")->count();

		$para_sys = readS();   //从缓存中读出系统参数
		if($_GET["pageNum"]){
			$pageNum = $_GET["pageNum"];
			$this->assign('pageNum',$pageNum);
		}else{
			$pageNum = $para_sys["page_rows"];
			$this->assign('pageNum',$pageNum);
		}
        $page = new Page($count,$pageNum);
		$page->parameter = "&";

		//if($username)$page->parameter .= "username=". urlencode($username) ."&";
		//if($en_name)$page->parameter .= "en_name=".urlencode($en_name)."&";
		if($cn_name)$page->parameter .= "cn_nam=".urlencode($cn_name)."&";
		if($extension)$page->parameter .= "extension=".urlencode($extension)."&";
		$page->parameter .= "dept=". urlencode($dept) ."&"; //这个地方的参数必须要的哦。
        $page->parameter .= "searchmethod=". urlencode($searchmethod) ."&";

		$page->setConfig("header",L("Records"));
		$page->setConfig("prev",L("Prev"));
		$page->setConfig("next",L("Next"));
		$page->setConfig("first",L("First"));
		$page->setConfig("last",L("Last"));
		$page->setConfig('theme','<span>  %totalRow%%header% &nbsp;  %nowPage%/%totalPage%</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');
        $show=$page->show();
        $this->assign('page',$show);

        $list = $users->table('users u')->order('u.extension')->field('u.username,u.cn_name,r.r_name,d.d_name,u.extension,u.fax,u.email,u.en_name,u.extension_type')->join('department d on u.d_id=d.d_id ')->join('role r on u.r_id=r.r_id')->limit($page->firstRow.','.$page->listRows)->where("$where AND u.extension <> ' '")->select();
		//echo $users->getLastSql();
		$i = 0;
		foreach( $list As &$val ){
			$ext = $val["extension"];
			$list[$i]['stat'] = $extension_status[$ext]['stat'];
			$list[$i]['Billsec'] = $channels[$ext]['Duration'];
			/*
			$DND = $bmi->DBGet('DND',$ext);//多次链接ami，负载比较大
			//dump($DND);
			if($DND){
				$list[$i]['stat'] = 'DND';
			}*/
			$dn = $DND_status[$ext];
			if($dn){
				if($val["stat"] == "State:Unavailable" || $val["stat"] == "State:InUse"){ // || $val["stat"] == "State:InUse"
					$list[$i]['stat'] = $val["stat"];
				}else{
					$list[$i]['stat'] = 'DND';
				}
			}
			$cf_tpl = $cf_status[$ext];
			if($cf_tpl){
				$list[$i]['stat'] = 'cf';
			}
			//dump($dn);
			//$list[$i]["cf"] = $cf_status[$ext];
			//$list[$i]["DND"] = $DND_status[$ext];
			$i++;

		}
		//dump($list);die;

        $this->assign('ulist',$list);

		//$this->assign("username",$username);
		//$this->assign("en_name",$en_name);
		$this->assign("cn_name",$cn_name);
		$this->assign("dept",$dept_id);
		$this->assign("extension",$extension);
		$this->assign("u_extension",$u_extension);
		$this->assign("searchmethod",$searchmethod);

        $role=new Model('Role');
        $list=$role->select();
        $this->assign('rlist',$list);

        $department=new Model("Department");
        $list=$department->select();
        $this->assign('dlist',$list);

		//分配增删改的权限
		$menuname = "Seating monitoring";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$aststats = $this->show_aststats2();
		$this->assign("aststats",$aststats);

		$answerRate = $this->cdrRate();
		$this->assign("answerRate",$answerRate);

        $this->display("","utf-8");
	}

	function monitorList(){
		checkLogin();
		header("Content-Type:text/html; charset=utf-8");
        import('ORG.Util.Page');
		import('ORG.Pbx.bmi');

		$bmi = new bmi();
		$extension_status = $bmi->getHints();   //获取分机状态

		$cf_status = $bmi->getCFAll("cf");	 //转接
		$DND_status = $bmi->getDNDAll();
		//dump($DND_status);

		$channels = $bmi->getAllChannelsInfo("CallerID");
		//dump($channels);exit;

		$arrQueue = $bmi->getAllQueueMember();  //获取队列中的分机
		$task_id = $_REQUEST["task_id"];
		$arrE = $arrQueue["AutoCall_".$task_id]["membername"];
		//dump($arrE);


		$deptId = $_REQUEST["deptId"];
        $users=new Model("Users");
		$where = "extension <> ' ' AND d_id='$deptId'";
        $count=$users->where($where)->count();

        $list = $users->order('extension')->field('username,cn_name,extension,fax,email,en_name,extension_type')->where($where)->select();
		//echo $users->getLastSql();
		//dump($list);die;
		$i = 0;
		foreach( $list As &$val ){
			$ext = $val["extension"];
			$list[$i]['stat'] = $extension_status[$ext]['stat'];
			$list[$i]['Billsec'] = $channels[$ext]['Duration'];
			$dn = $DND_status[$ext];
			if($dn){
				if($val["stat"] == "State:Unavailable" || $val["stat"] == "State:InUse"){ // || $val["stat"] == "State:InUse"
					$list[$i]['stat'] = $val["stat"];
				}else{
					$list[$i]['stat'] = 'DND';
				}
			}
			$cf_tpl = $cf_status[$ext];
			if($cf_tpl){
				$list[$i]['stat'] = 'cf';
			}
			$val["time"] = sprintf("%02d",intval($val["Billsec"]/3600)) .":". sprintf("%02d",intval(($val["Billsec"]%3600)/60)) .":" .sprintf("%02d",intval((($val["Billsec"]%3600)%60)));
			$i++;

		}
		unset($i);
		foreach($list as &$val){
			if($val["stat"] == "State:Unavailable"){
				$val["color"] = "#ADADAD";
				$Unavailable[] = $val;
			}elseif($val["stat"] == "State:Idle"){
				$val["color"] = "#66C010";
				$Idle[] = $val;
			}elseif($val["stat"] == "State:Ringing"){
				$val["color"] = "#FC9401";
				$Ringing[] = $val;
			}elseif($val["stat"] == "State:InUse"){
				$val["color"] = "#FC9401";
				$InUse[] = $val;
			}elseif($val["stat"] == "cf"){
				$val["color"] = "#0082DA";
				$cf[] = $val;
			}elseif($val["stat"] == "DND"){
				$val["color"] = "red";
				$DND[] = $val;
			}else{
				$val["color"] = "#ADADAD";
				$tt[] = $val;
			}

			if( in_array($val["extension"],$arrE) ){
				$queueExten[] = $val;
			}
		}

		$countStatus = array(
			"Unavailable" => count($Unavailable),
			"Idle" => count($Idle),
			"Ringing" => count($Ringing),
			"InUse" => count($InUse),
			"cf" => count($cf),
			"DND" => count($DND),
			"total" => count($list),
			"queueExten" => count($queueExten),
		);

		$this->assign("countStatus",$countStatus);
		$this->assign("Unavailable",$Unavailable);
		$this->assign("Idle",$Idle);
		$this->assign("Ringing",$Ringing);
		$this->assign("InUse",$InUse);
		$this->assign("cf",$cf);
		$this->assign("DND",$DND);
		$this->assign("queueExten",$queueExten);

		//dump($Unavailable);
		//dump($list);die;

		$this->display();
	}

	function getDepList($arrDep,$id,$i){
        $selectedD_id = $_REQUEST['dept'];//表单提交的部门id
        $html ="<option value='{$arrDep[$id]["id"] }' ";
        if($selectedD_id==$id){//是否选择了当前部门
            $html .= "selected";
        }
        $html .= ">". str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;",$i)."|-" . $arrDep[$id]["name"] ."</option>";
		//dump($html);die;
        //先检测自己是否有子节点
        $hasSonNode = false;
        foreach($arrDep AS $key=>$v){
            if( $v['pid'] == $id ){
                $hasSonNode = true;break;
            }
        }
        //如果有子节点
        if($hasSonNode){
            //取出子节点id
            $sonID = explode(',',$arrDep[$id]['meAndSonId']);
            array_shift($sonID);
            foreach ( $sonID AS $val ) {
                $html .= $this->getDepList($arrDep,$val,$i+1);
            }
            return $html;
        }else{//如果没有子节点
            return $html;
        }
    }

	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}


    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }

	//示忙
	function DNDoperating(){
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$extension = $_GET['extension'];
		$arrExt = explode(',',$extension);
		foreach($arrExt as $val){
			$result = $bmi->putDND($val);
		}
		//无条件相信bmi执行成功
		echo "ok";
	}

	//示闲
	function DelDNDoperating(){
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$extension = $_GET['extension'];
		$arrExt = explode(',',$extension);
		foreach($arrExt as $val){
			$result = $bmi->delDND($val);
		}
		//无条件相信bmi执行成功
		echo "ok";
	}

	//呼叫
	function calls(){
		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$src_exten = $_SESSION["user_info"]["extension"];    //主叫
		$dst_exten = $_POST['extension'];                //被叫
		$result = $bmi->call($src_exten,$dst_exten,src_exten);
		echo "ok";

	}

	//监听
	function monitors(){
		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$passage = $bmi->getDevices();
		$monitor_exten = $_GET['extension'];
		$dial = $passage['dial'][$monitor_exten];
		$result = $bmi->ajax_monitor($monitor_exten,$dial);
		echo $result;
	}

	//强插
	function chanspys(){
		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$passage = $bmi->getDevices();
		$monitor_exten = $_GET['extension'];
		$dial = $passage['dial'][$monitor_exten];
		$result = $bmi->ajax_chanspy($monitor_exten,$dial);
		//echo "ok";
		echo $result;
	}

	//强拆
	function hangups(){
		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$extension = $_REQUEST['extension'];
		$arrExt = explode(',',$extension);
		foreach($arrExt as $val){
			$result = $bmi->hangup($val);
		}
		echo "ok";
	}

	//强挂----用于当监控monitor挂死的时候
	function forceHangup(){
		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$extension = $_GET['extension'];
		$result = $bmi->forceHangup($extension);
		echo "ok";
	}


	//接口说明的坐席监控
	function interfaceTM(){
		//checkLogin();
		header("Content-Type:text/html; charset=utf-8");
		$u_extension = $_SESSION["user_info"]["extension"];

		//$en_name = $_REQUEST['en_name'];
		//$username = $_REQUEST['username'];
		$cn_name = $_REQUEST['cn_name'];
		$extension = $_REQUEST['extension'];
		$dept = $_REQUEST['dept'];//部门
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";

		$where = "1";
		//$where = " AND u.extension <> ''";
		if( $searchmethod == "equal" ){ //精确查询，条件语句用等号
            if( $cn_name ) $where .= " AND u.cn_name='$cn_name' ";
            if( $extension ) $where .= " AND u.extension='$extension' ";
            //if( $username ) $where .= " AND u.username='$username' ";
            //if( $en_name ) $where .= " AND u.en_name='$en_name' ";
        }else{
            if( $cn_name ) $where .= " AND u.cn_name like '%$cn_name%' ";
            if( $extension ) $where .= " AND u.extension like '%$extension%' ";
            //if( $username ) $where .= " AND u.username like '%$username%' ";
            //if( $en_name ) $where .= " AND u.en_name like '%$en_name%' ";
        }

		//部门相关信息
		$sonStr = "0";
		$arrDep = $this->getDepTreeArray();
		//dump($arrDep);die;
		foreach( $arrDep AS $k=>$v){
			if($v['pid'] == 0)
				$sonStr .= "," . $v['id'];
		}
		$arrDep['0'] = Array("id"=>"0","pid" => "-1","name" => "所有部门","meAndSonId" => $sonStr );
		//dump($depTreeHTML);die;
		$depTreeHTML = $this->getDepList($arrDep,0,0);
		$this->assign('depTreeHTML',$depTreeHTML);
		if( $_REQUEST['dept'] ){
			//获取子部门字符集合
			//$deptSet = $arrDep[$d_id]['meAndSonId'];
			$dept_id = $_REQUEST['dept'];
			$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
			//dump($deptSet);die;
			//$where .= " AND dept IN ('财务部','IT部')";//部门编号
			$where .= " AND d.d_name IN (".$deptSet."'所有部门')";
			//dump($where);die;
		}

        import('ORG.Util.Page');
		import('ORG.Pbx.bmi');

		$bmi = new bmi();
		$extension_status = $bmi->getHints();   //获取分机状态
		//dump($extension_status);die;
		//$DND_status = $bmi->DBGet('DND','801');       //示忙状态
		//dump($DND_status);die;
		$cf_status = $bmi->getCFAll("cf");	 //转接
		$DND_status = $bmi->getDNDAll();
		//dump($DND_status);

		$channels = $bmi->getAllChannelsInfo("CallerID");
		//dump($channels);exit;

        $users=new Model("Users");
        $count=$users->table('users u')->order('u.username')->field('u.username,u.cn_name,r.r_name,d.d_name,u.extension,u.fax,u.email,u.en_name,u.extension_type')->join('department d on u.d_id=d.d_id ')->join('role r on u.r_id=r.r_id')->limit($page->firstRow.','.$page->listRows)->where("$where AND u.extension <> ' '")->count();
		$para_sys = readS();   //从缓存中读出系统参数
        $page = new Page($count,$para_sys["page_rows"]);
		$page->parameter = "&";

		//if($username)$page->parameter .= "username=". urlencode($username) ."&";
		//if($en_name)$page->parameter .= "en_name=".urlencode($en_name)."&";
		if($cn_name)$page->parameter .= "cn_nam=".urlencode($cn_name)."&";
		if($extension)$page->parameter .= "extension=".urlencode($extension)."&";
		$page->parameter .= "dept=". urlencode($dept) ."&"; //这个地方的参数必须要的哦。
        $page->parameter .= "searchmethod=". urlencode($searchmethod) ."&";

		$page->setConfig("header",L("Records"));
		$page->setConfig("prev",L("Prev"));
		$page->setConfig("next",L("Next"));
		$page->setConfig("first",L("First"));
		$page->setConfig("last",L("Last"));
		$page->setConfig('theme','<span>  %totalRow%%header% &nbsp;  %nowPage%/%totalPage%</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');
        $show=$page->show();
        $this->assign('page',$show);

        $list = $users->table('users u')->order('u.extension')->field('u.username,u.cn_name,r.r_name,d.d_name,u.extension,u.fax,u.email,u.en_name,u.extension_type')->join('department d on u.d_id=d.d_id ')->join('role r on u.r_id=r.r_id')->limit($page->firstRow.','.$page->listRows)->where("$where AND u.extension <> ' '")->select();
		//echo $users->getLastSql();
		$i = 0;
		foreach( $list As $val ){
			$ext = $val["extension"];
			$list[$i]['stat'] = $extension_status[$ext]['stat'];
			$list[$i]['Billsec'] = $channels[$ext]['Duration'];
			/*
			$DND = $bmi->DBGet('DND',$ext);//多次链接ami，负载比较大
			//dump($DND);
			if($DND){
				$list[$i]['stat'] = 'DND';
			}*/
			$dn = $DND_status[$ext];
			if($dn){
				$list[$i]['stat'] = 'DND';
			}
			$cf_tpl = $cf_status[$ext];
			if($cf_tpl){
				$list[$i]['stat'] = 'cf';
			}
			//dump($dn);
			//$list[$i]["cf"] = $cf_status[$ext];
			//$list[$i]["DND"] = $DND_status[$ext];
			$i++;

		}
		//dump($list);die;

        $this->assign('ulist',$list);

		//$this->assign("username",$username);
		//$this->assign("en_name",$en_name);
		$this->assign("cn_name",$cn_name);
		$this->assign("dept",$dept);
		$this->assign("extension",$extension);
		$this->assign("u_extension",$u_extension);

        $role=new Model('Role');
        $list=$role->select();
        $this->assign('rlist',$list);

        $department=new Model("Department");
        $list=$department->select();
        $this->assign('dlist',$list);
        $this->display("","utf-8");
	}
}
?>
