<?php
class ComboboxAction extends Action{
	//用户搜索
	function userData(){
		$users = new Model("users");
		$name = $_REQUEST['name'];

		$where = "1 ";
		$where .= empty($name)?"":" AND ( username like '%$name%' OR cn_name like '%$name%'  OR extension like '%$name%' )";

		$count = $users->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$userData = $users->field("username,extension,d_id,r_id,cn_name,en_name,email,fax,extension_type,extension_mac,phone,out_pin,routeid")->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		foreach($userData as &$val){
			$val["cn_name"] = $val["username"]."/".$val["cn_name"];
		}

		$rowsList = count($userData) ? $userData : false;
		$arrU["total"] = $count;
		$arrU["rows"] = $rowsList;

		echo json_encode($arrU);
	}

	//获取外呼任务
	function getTaskBox(){
		$sales_task = new Model("sales_task");
		$d_id = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$where = "enable='Y'";
		$where .= " AND dept_id IN ($deptSet)";
		if($username == "admin"){
			$taskList = $sales_task->field("id,name")->order("createtime")->where("enable='Y'")->select();
			//echo $sales_task->getLastSql();die;
		}else{
			$taskList = $sales_task->field("id,name")->order("createtime")->where($where)->select();
		}

		echo json_encode($taskList);
	}

	//获取外呼任务--外呼资料
	function getTaskBox2(){
		$sales_task = new Model("sales_task");
		$d_id = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$where = "enable='Y'";
		$where .= " AND dept_id IN ($deptSet)";
		if($username == "admin"){
			$taskList = $sales_task->field("id,name,dept_id")->order("createtime")->where("enable='Y'")->select();
			//echo $sales_task->getLastSql();die;
		}else{
			$taskList = $sales_task->field("id,name,dept_id")->order("createtime")->where($where)->select();
		}

		foreach($taskList as &$val){
			$val["id"] = $val["id"]."-".$val["dept_id"];
		}

		echo json_encode($taskList);
	}

	//循环月份
	function getMonth(){
		$thisyear = date('Y');
		$currentYearMonth = date('Y-m');
		$yearmonth = Array(
			$thisyear .'-01',
			$thisyear .'-02',
			$thisyear .'-03',
			$thisyear .'-04',
			$thisyear .'-05',
			$thisyear .'-06',
			$thisyear .'-07',
			$thisyear .'-08',
			$thisyear .'-09',
			$thisyear .'-10',
			$thisyear .'-11',
			$thisyear .'-12',
		);
		/*
		$tpl_yearmonth = Array();
		for($i=0;$i<=12;$i++){
			$tpl_yearmonth[$i] = Array(
				'value' => $yearmonth[$i],
				'selected' => $yearmonth[$i]==$currentYearMonth?'selected="selected"':'',
				'display' => $yearmonth[$i],
			);
		}
		*/
		$monthList = Array();
		for($i=0;$i<=12;$i++){
			$monthList[$i] = Array(
				'id' => $yearmonth[$i],
				'month' => $yearmonth[$i],
			);
		}
		//dump($monthList);die;
		echo json_encode($monthList);
	}

	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }


	function userProcess(){
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();

		$users = new Model("users");
		$userList = $users->field("username as text,extension as id,d_id as pid,cn_name")->select();

		$j = 0;
		foreach($dept as $vm){
			$dept[$j]['iconCls'] = "door";
			$dept[$j]['state'] = "closed";
			$dept[$j]['attributes'] = "department".",".$vm['tid'];
			$j++;
		}
		unset($j);

		$i = 0;
		foreach($userList as &$v){
			$userList[$i]['text'] = $v['text']."/".$v['id'];
			$userList[$i]['iconCls'] = "icon-user_chat";
			$userList[$i]['attributes'] = "user".",".$v['cn_name'];
			$i++;
		}
		unset($i);

		foreach($userList as $val){
			$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}
        $arrTree = $this->getExtenTree($dept,0);
		//dump($arrTree);die;
		$strJSON = json_encode($arrTree);
		echo ($strJSON);

	}


	function getExtenTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
					$v['children'] = $this->getExtenTree($data, $v['tid']);
					if ( empty($v["children"])  )  unset($v['children']) ;
					/*
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['children'] =  array(array());
					*/
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['state'] =  'open';
				 $tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }


	//获取队列
	function getQueue(){
		$queues_config = new Model("asterisk.queues_config");
		$arrQueue = $queues_config->field("extension AS id,descr as name")->select();
		if($arrQueue){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrQueue,$arr);
		}else{
			$arrQueue = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrQueue);
	}

	//获取中继---用户
	function getTrunks(){
		$trunks = new Model("asterisk.trunks");
		$arrTrunk = $trunks->order("name asc")->field("trunkid AS id,name")->select();
		if($arrTrunk){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrTrunk,$arr);
		}else{
			$arrTrunk = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrTrunk);
	}

	//获取时间组
	function getTimegroup(){
		$timegroup = new Model("asterisk.timegroups_groups");
		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$arrTimegroup = $timegroup->field("id,description as name")->select();
		//echo $timegroup->getLastSql();die;
		if($arrTimegroup){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrTimegroup,$arr);
		}else{
			$arrTimegroup = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrTimegroup);die;
		echo json_encode($arrTimegroup);
	}
	//获取系统录音
	function getRecordings(){
		$d_id = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$where = "1 ";
		if($username != "admin"){
			//$where .= " AND dept_id IN ($deptSet)";
			$where .= " AND (dept_id in ($deptSet) OR dept_id = '-1')";
		}
		$recordings = new Model("asterisk.recordings");
		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$arrRecordings = $recordings->field("displayname as id ,description as name")->where($where)->select();
		//echo $timegroup->getLastSql();die;
		if($arrRecordings){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrRecordings,$arr);
		}else{
			$arrRecordings = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrRecordings);die;
		echo json_encode($arrRecordings);
	}

	//获取中继---外呼
	function getTaskTrunks(){
		$trunks = new Model("asterisk.trunks");

		$d_id = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$where = "1 ";
		if($username != "admin"){
			//$where .= " AND (deptId in ($deptSet) OR deptId is null OR deptId = '')";
			$where .= " AND (deptId in ($deptSet) OR deptId = '-1')";
		}

		$arrTrunk = $trunks->order("name asc")->field("trunkid AS id,name,channelid,tech")->where($where)->select();

		foreach( $arrTrunk AS &$val ){
			if($val['tech']=='zap' or $val['tech']=='dahdi'){
				$val["name"] = "DAHDI/".$val['channelid'];
			}elseif($val['tech']=='sip'){
				$val["name"] = "SIP/".$val['channelid'];
			}elseif($val['tech']=='iax2'){
				$val["name"] = "IAX2/".$val['channelid'];
			}else{
				;
			}
		}
		//dump($arrTrunk);die;
		if($arrTrunk){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrTrunk,$arr);
		}else{
			$arrTrunk = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrTrunk);
	}

	//获取会议号码
	function getMeetng(){
		$meeting = new Model("meeting");
		$arrData = $meeting->field("id,exten as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}


	//可以看到本部门及子部门的员工  和员工姓名  id为工号
	function getDeptUserArr2(){
		$username = $_SESSION['user_info']['username'];
		$ptlid = $_SESSION["user_info"]["d_id"];
		$deptId = $_REQUEST["deptId"];
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();
		if(!$_REQUEST["deptId"]){
			$pname = $department->field("d_name as text,d_id as id,d_pid")->where("d_id = $ptlid")->find();
		}else{
			$pname = $department->field("d_name as text,d_id as id,d_pid")->where("d_id = $deptId")->find();
		}
		$users = new Model("users");
		$userList = $users->field("username as text,username as id,d_id as pid,cn_name")->select();
		//dump($userList);

		foreach($userList as &$val){
			$val['text'] = $val['text']."/".$val["cn_name"];
		}

		$j = 0;
		foreach($dept as $vm){
			//$types[$j]['iconCls'] = "icon-file";
			$dept[$j]['state'] = "closed";
			$j++;
		}
		//echo var_export($dept);echo "=====</br>";echo var_export($userList);echo "====</br>";

		foreach($userList as &$val){
			//$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}
        //$arrTree = $this->getTree($dept,0);

		if(!$_REQUEST["deptId"]){
			if($username == "admin"){
				$arrTree = $this->getDeptTree($dept,0);
			}else{
				$arrTree = $this->getDeptTree($dept,$ptlid);
			}
		}else{
			$arrTree = $this->getDeptTree($dept,$_REQUEST["deptId"]);
		}
		$pname["children"] = $arrTree;

		$arrTreeData[0] =  $pname;

		//$strJSON = json_encode($arrTreeData);
		if(!$_REQUEST["deptId"]){
			if($username == "admin"){
				$strJSON = json_encode($arrTree);
			}else{
				$strJSON = json_encode($arrTreeData);
			}
		}else{
			$strJSON = json_encode($arrTreeData);
		}
		echo ($strJSON);
    }

	function getDeptTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
					$v['children'] = $this->getDeptTree($data, $v['tid']);
					if ( empty($v["children"])  )  unset($v['children']) ;
					/*
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['children'] =  array(array());
					*/
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['state'] =  'open';
				 $tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	//只能看到本部门的员工
	function getDeptUserArr(){
		header("Content-Type:text/html; charset=utf-8");
		$deptId = $_REQUEST["deptId"];

		$users = new Model("users");
		$userList = $users->field("username as text,username as id,d_id as pid,cn_name")->where("d_id = '$deptId'")->select();

		$pname["children"] = $userList;
		$userArr = readU();
		$deptId_name = $userArr["deptId_name"];

		//$arrTreeData[0] =  $pname;
		$arrTreeData[0] = array(
			"text"=>$deptId_name[$deptId],
			"id"=>$deptId,
			"children"=>$userList,
		);

		//dump($arrTreeData);die;
		$strJSON = json_encode($arrTreeData);
		echo ($strJSON);
    }


	//可以看到所有部门的员工  和员工姓名  id为工号
	function getDeptAllUser(){
		$username = $_SESSION['user_info']['username'];
		$ptlid = $_SESSION["user_info"]["d_id"];
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();
		$pname = $department->field("d_name as text,d_id as id,d_pid")->find();

		$users = new Model("users");
		$userList = $users->field("username as text,username as id,d_id as pid,cn_name")->select();
		//dump($userList);

		foreach($userList as &$val){
			$val['text'] = $val['text']."/".$val["cn_name"];
		}

		$j = 0;
		foreach($dept as $vm){
			//$types[$j]['iconCls'] = "icon-file";
			$dept[$j]['state'] = "closed";
			$j++;
		}
		//echo var_export($dept);echo "=====</br>";echo var_export($userList);echo "====</br>";

		foreach($userList as &$val){
			//$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}

		$arrTree = $this->getDeptTree($dept,0);
		$strJSON = json_encode($arrTree);

		echo ($strJSON);
    }


	//获取服务类型
	function getServiceData(){
		$servicetype = new Model("servicetype");
		$arrData = $servicetype->field("id,servicename as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}

	//获取文件类型
	function getFileData(){
		$filetype = new Model("filetype");
		$arrData = $filetype->field("id,filename as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}


	//获取角色
	function getRoleData(){
		$role=new Model('role');
		$arrData = $role->order("r_id desc")->field("r_id as id,r_name as name,interface")->select();
		foreach($arrData as &$val){
			$val["id"] = $val["id"]."_".$val["interface"];
		}
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}

	//获取角色
	function getRole(){
		$role=new Model('role');
		$arrData = $role->field("r_id as id,r_name as name,interface")->select();

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}



	//获取微信号
	function getWeChat(){
		$username = $_SESSION['user_info']['username'];
		$wx_users = new Model("wx_users");
		$where = "1 ";
		if($username != "admin"){
			$where .= " AND answer_num='$username'";
		}
		$arrData = $wx_users->field("nickname as name,openid as id")->where($where)->select();
		echo json_encode($arrData);
	}


	//用户搜索
	function weChatData(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$wx_users = new Model("wx_users");
		$name = $_REQUEST['name'];
		$openid = $_REQUEST['openid'];

		$where = "1 ";
		/*
		if($username != "admin"){
			$where .= " AND answer_num='$username'";
		}
		*/
		$where .= empty($name)?"":" AND ( openid like '%$name%' OR nickname like '%$name%'  OR country like '%$name%'  OR city like '%$name%'  OR province like '%$name%' )";

		$count = $wx_users->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wx_users->field("openid,nickname,sex,country,city,province,answer_num")->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		$row = array('1'=>'男','2'=>'女','0'=>'未知');
		foreach($arrData as &$val){
			$sex = $row[$val['sex']];
			$val['sex'] = $sex;
		}

		if($openid){
			foreach($arrData as &$val){
				if($val["openid"] == $openid){
					$arrT = $val;
				}else{
					$arr[] = $val;
				}
			}
			array_unshift($arr,$arrT);
			$arrData = $arr;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function getAtocalltMark(){
		header("Content-Type:text/html; charset=utf-8");
		$mark = new Model("autocall_mark");
		$arrData = $mark->field("key_value as id,key_description as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			$arr1 = array("id"=>"all","name"=>"全部");
			$arr2 = array("id"=>"not","name"=>"无");
			array_unshift($arrData,$arr2);
			array_unshift($arrData,$arr1);
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	function getVisitContent(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$vis = new Model("visit_content");
		$where = "1";
		if($username != "admin"){
			$where .= " AND createname = '$username'";
		}

		$arrData = $vis->field("title,content,description")->where($where)->select();
		foreach($arrData as &$val){
			$val["id"] = $val["title"];
			$val["name"] = $val["title"];
		}

		//dump($arrData);die;
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}


	//获取服务类型
	function getServiceTypeData(){
		$servicetype = new Model('servicetype');
		$arrData = $servicetype->field("id,servicename as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}













	//获取班次
	function getShiftData(){
		$shift_definition = new Model('pb_shift_definition');
		$arrData = $shift_definition->field("id,shift_name as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}

	//获取课件
	function getCourseware(){
		$ks_courseware = new Model("ks_courseware");
		$arrData = $ks_courseware->field("id,courseware_name as name,curriculum_id")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...","curriculum_id"=>"");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择...","curriculum_id"=>""));
		}
		echo json_encode($arrData);
	}

	//获取课程
	function getCurriculum(){
		$ks_curriculum = new Model("ks_curriculum");
		$arrData = $ks_curriculum->field("id,curriculum_name as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}


	//培训计划
	function getTrain(){
		$username = $_SESSION['user_info']['username'];
		$ks_train = new Model("ks_train");

		$name = $_REQUEST['name'];

		$where = "1 ";
		$where .= empty($name)?"":" AND ( t.training_task_name like '%$name%' OR t.training_address like '%$name%'  OR c.courseware_name like '%$name%' OR kc.curriculum_name like '%$name%' )";

		$fields = "t.id,t.create_time,t.create_user,t.dept_id,t.curriculum_id,t.courseware_id,t.training_task_name,t.training_address,t.training_task_description,t.training_date,t.training_start_time,t.training_end_time,t.whether_exam,t.ks_id,t.participants,c.courseware_name,kc.curriculum_name";


		$count = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$whether_exam_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$whether_exam = $whether_exam_row[$val['whether_exam']];
			$val['whether_exam2'] = $whether_exam;

			$val["training_time"] = $val["training_start_time"]."~".$val["training_end_time"];
			$val["participants"] = json_decode($val["participants"],true);
		}
		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//获取题库管理
	function getPaperData(){
		$username = $_SESSION['user_info']['username'];
		$ks_question_bank = new Model("ks_question_bank");
		$fields = "q.id,q.create_time,q.create_user,q.dept_id,q.curriculum_id,q.question_title,q.topic_answers,q.type_questions,q.question_content,q.term,q.start_validity_time,q.end_validity_time,q.easy_type,q.examination_scores,c.curriculum_name";

		$curriculum_id = $_REQUEST["curriculum_id"];
		$question_title = $_REQUEST["question_title"];
		$type_questions = $_REQUEST["type_questions"];
		$easy_type = $_REQUEST["easy_type"];

		$now_time = date("Y-m-d H:i:s");
		$where = "1 ";
		$where .= " AND (q.term='N' OR (q.start_validity_time<='$now_time' AND q.end_validity_time>='$now_time'))";
		$where .= empty($curriculum_id) ? "" : " AND q.curriculum_id = '$curriculum_id'";
		$where .= empty($question_title) ? "" : " AND q.question_title like '%$question_title%'";
		$where .= empty($type_questions) ? "" : " AND q.type_questions = '$type_questions'";
		$where .= empty($easy_type) ? "" : " AND q.easy_type = '$easy_type'";

		$count = $ks_question_bank->table("ks_question_bank q")->field($fields)->join("ks_curriculum c on (q.curriculum_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_question_bank->table("ks_question_bank q")->field($fields)->join("ks_curriculum c on (q.curriculum_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $ks_question_bank->getLastSql();die;
		$term_row = array('Y'=>'是','N'=>'否');
		$type_questions_row = array('1'=>'单选题','2'=>'多选题','3'=>'判断题','4'=>'填空题','5'=>'问答题');
		$easy_type_row =  array('1'=>'容易','2'=>'中等','3'=>'较难');
		foreach($arrData as &$val){
			$term = $term_row[$val['term']];
			$val['term'] = $term;

			$type_questions = $type_questions_row[$val['type_questions']];
			$val['type_questions'] = $type_questions;

			$easy_type = $easy_type_row[$val['easy_type']];
			$val['easy_type'] = $easy_type;

			if($val["start_validity_time"] == "0000-00-00 00:00:00"){
				$val["start_validity_time"] = "";
			}
			if($val["end_validity_time"] == "0000-00-00 00:00:00"){
				$val["end_validity_time"] = "";
			}
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	//获取试题策略配置
	function getPolicy(){
		$ks_policy_configuration = new Model("ks_policy_configuration");
		$arrData = $ks_policy_configuration->field("id,policy_name as name,policy_score")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...","policy_score"=>"");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择...","policy_score"=>""));
		}
		echo json_encode($arrData);
	}

	//获取试卷
	function getPapers(){
		$curriculum_id = $_REQUEST["curriculum_id"];
		$ks_papers = new Model("ks_papers");

		$where = "1 ";
		$where .= empty($curriculum_id) ? "" : " AND curriculum_id = '$curriculum_id'";

		$arrData = $ks_papers->field("id,papers_name as name")->where($where)->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}


	//获取考试计划
	function getExamProgram(){
		$curriculum_id = $_REQUEST["curriculum_id"];
		$training_date = $_REQUEST["training_date"];
		$training_start_time = $_REQUEST["training_start_time"];
		$training_end_time = $_REQUEST["training_end_time"];

		$ks_examination_program = new Model("ks_examination_program");

		$where = "1 ";
		$where .= empty($curriculum_id) ? "" : " AND curriculum_id = '$curriculum_id'";
		$where .= empty($training_date) ? "" : " AND exam_date = '$training_date'";
		$where .= empty($training_end_time) ? "" : " AND start_exam_time >= '$training_end_time'";

		$arrData = $ks_examination_program->field("id,exam_plan_name as name")->where($where)->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}


	//获取考试计划
	function getExamPlanData(){
		$username = $_SESSION['user_info']['username'];
		$ks_examination_program = new Model("ks_examination_program");

		$fields = "e.id,e.create_time,e.create_user,e.dept_id,e.paper_id,e.curriculum_id,e.exam_plan_name,e.exam_plan_description,e.exam_plan_address,e.exam_date,e.start_exam_time,e.end_exam_time,e.exam_type,e.are_published,e.examiners,e.reviwers,e.passing_score,e.examination_staff,p.papers_name,c.curriculum_name";


		$curriculum_id = $_REQUEST["curriculum_id"];
		$training_date = $_REQUEST["training_date"];
		$training_start_time = $_REQUEST["training_start_time"];
		$training_end_time = $_REQUEST["training_end_time"];

		$where = "1 ";
		$where .= empty($curriculum_id) ? "" : " AND e.curriculum_id = '$curriculum_id'";
		$where .= empty($training_date) ? "" : " AND e.exam_date = '$training_date'";
		$where .= empty($training_end_time) ? "" : " AND e.start_exam_time >= '$training_end_time'";


		$count = $ks_examination_program->order("e.create_time desc")->table("ks_examination_program e")->field($fields)->join("ks_papers p on (e.paper_id = p.id)")->join("ks_curriculum c on (e.curriculum_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_examination_program->order("e.create_time desc")->table("ks_examination_program e")->field($fields)->join("ks_papers p on (e.paper_id = p.id)")->join("ks_curriculum c on (e.curriculum_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $ks_examination_program->getLastSql();die;

		$exam_type_row = array('Y'=>'开卷','N'=>'闭卷');
		$are_published_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$exam_type = $exam_type_row[$val['exam_type']];
			$val['exam_type2'] = $exam_type;

			$are_published = $are_published_row[$val['are_published']];
			$val['are_published2'] = $are_published;


			$val["exam_time"] = $val["start_exam_time"]."~".$val["end_exam_time"];
			$val["examination_staff"] = json_decode($val["examination_staff"],true);
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//获取问卷题型
	function getQuestionnaireTopic(){
		$wj_questionnaire_topic = new Model("wj_questionnaire_topic");
		$arrData = $wj_questionnaire_topic->field("id,topic_name as name,topic_type")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...","topic_type"=>"");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择...","topic_type"=>""));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取问卷类型
	function getQuestionnaireType(){
		$wj_questionnaire_type = new Model("wj_questionnaire_type");
		$arrData = $wj_questionnaire_type->field("id,questionnaire_type_name as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}


	//获取配送方式
	function getDelivery(){
		$order_delivery = new Model("order_delivery");
		$arrData = $order_delivery->order("kd_order desc")->field("id,delivery as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取有阅卷权限的工号
	function getRefer(){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		if(file_exists("BGCC/Conf/crm/$db_name/assessmentPapers.php")){
			$arrPriv = require "BGCC/Conf/crm/$db_name/assessmentPapers.php";
			$r_id = implode(",",$arrPriv["refer"]);
			$where = "r_id in ($r_id)";
		}else{
			$where = "1 ";
		}
		$users = new Model("users");
		$arrData = $users->field("username,cn_name")->where($where)->select();
		foreach($arrData as &$val){
			$val["name"] = $val["username"]." / ".$val["cn_name"];
		}

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取有评卷权限的工号
	function getEvaluation(){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		if(file_exists("BGCC/Conf/crm/$db_name/assessmentPapers.php")){
			$arrPriv = require "BGCC/Conf/crm/$db_name/assessmentPapers.php";
			$r_id = implode(",",$arrPriv["evaluation"]);
			$where = "r_id in ($r_id)";
		}else{
			$where = "1 ";
		}
		$users = new Model("users");
		$arrData = $users->field("username,cn_name")->where($where)->select();
		foreach($arrData as &$val){
			$val["name"] = $val["username"]." / ".$val["cn_name"];
		}

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}


	//获取问卷列表
	function getQuestionnaireList(){
		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
		$arrData = $wj_questionnaire_generation->field("id,questionnaire_name as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取工作流程
	function getSysParam_leave_process_id(){
		$work_process_design = new Model("work_process_design");
		$arrData = $work_process_design->field("id,process_name as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取 录音文件存储时间
	function getSysParam_record_storage_time(){
		$sys_month = new Model("sys_month");
		$arrData = $sys_month->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取顶级菜单
	function getMemuModule(){
		//header("Content-Type:text/html; charset=utf-8");
		$menu = M("menu");
		$arrData = $menu->order("m.order asc")->table("menu m")->field("m.id,m.name,md.modulename")->join("module md on (m.moduleclass = md.modulename)")->where("md.enabled = 'Y' AND m.pid = '0'")->select();
		foreach($arrData as &$val){
			$val["name"] = L($val["name"]);
		}
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			//$arr2 = array("id"=>"0","name"=>"顶级菜单");
			//array_unshift($arrData,$arr2);
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
			//$arr2 = array("id"=>"0","name"=>"顶级菜单");
			//array_unshift($arrData,$arr2);
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取模块
	function getModule(){
		//header("Content-Type:text/html; charset=utf-8");
		$module = M("module");
		$arrData = $module->order("id asc")->field("modulename as id,description as name")->where("enabled = 'Y'")->select();

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			//$arr2 = array("id"=>"0","name"=>"顶级菜单");
			//array_unshift($arrData,$arr2);
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
			//$arr2 = array("id"=>"0","name"=>"顶级菜单");
			//array_unshift($arrData,$arr2);
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}


	//获取商品分类
	function getShopCategory(){
		//header("Content-Type:text/html; charset=utf-8");
		$shop_category = M("shop_category");
		$arrData = $shop_category->order("id asc")->field("id,cat_name as name")->select();

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取物流方式
	function getLogisticsMode(){
		//header("Content-Type:text/html; charset=utf-8");
		$logistics_mode = M("logistics_mode");
		$arrData = $logistics_mode->order("id asc")->field("id,logistics_name as name,com_code")->select();

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取会员等级
	function getMemberLevel(){
		$customer_fields = new Model("customer_fields");
		$fieldData = $customer_fields->order("field_order asc")->where("en_name = 'member_Level'")->find();
		$selectTpl = json_decode($fieldData["field_values"] ,true);

		$arrData = array();
		foreach($selectTpl as $vm){
			$arrData[] = array(
				"id"=>$vm[0],
				"name"=>$vm[1],
			);
		}
		$arrF = array("id"=>"failure","name"=>"不成单");
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arrF);
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
			array_unshift($arrData,$arrF);
		}

		echo json_encode($arrData);
	}

	//城市街道
	function provinceCombox(){
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$region2_table = "crm_public.region2";
		}else{
			$region2_table = "region2";
		}

		$region = new Model("$region2_table");
		$code = $_REQUEST['code'];
		$region_id = $_REQUEST['region_id'];
		if($region_id){
			$arrF = $region->where("region_id = '$region_id'")->find();
			$code = $arrF["code"];
		}
		if($code || $code == '0'){
			$regData = $region->where("parent_id = '$code'")->select();
			//echo $region->getLastSql();
			//dump($regData);die;
			$allCate = array(
				"region_id"=>"0",
				"parent_id"=>"-1",
				"region_name"=>"请选择...",
				"region_type"=>"0",
				"code"=>"0",
			);
			array_unshift($regData,$allCate);
			echo json_encode($regData);
		}else{
			$allCate[0] = array(
				"region_id"=>"0",
				"parent_id"=>"01",
				"region_name"=>"请选择...",
				"region_type"=>"0",
				"code"=>"0",
			);
			echo json_encode($allCate);
		}
	}


	//获取中继--表格
	function getTrunksGrid(){
		$name = $_REQUEST["name"];
		$where = "1 ";
		$where .= empty($name) ? "" : " AND name like '%$name%'";
		$trunks = new Model("asterisk.trunks");
		$count = $trunks->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $trunks->order("name asc")->field("trunkid AS id,name")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//下载文件
	function downloadFile(){
		$file = $_GET['file'];
		$path = "BGCC/Tpl/Public/download/";
		$realfile = $path.$file;
		if(!file_exists($realfile)){
			$this->error("File $realfile is not exist!");
		}
        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}




	//获取媒体来源
	function getMediaSources(){
		//header("Content-Type:text/html; charset=utf-8");
		$hotline_media_sources = M("hotline_media_sources");
		$arrData = $hotline_media_sources->order("id asc")->field("id,source_name as name")->select();

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取租户权限
	function getTenantRole(){
		//header("Content-Type:text/html; charset=utf-8");
		$tenants_role = M("tenants_role");
		$arrData = $tenants_role->order("r_id asc")->field("r_id,r_name")->select();

		if($arrData){
			$arr = array("r_id"=>"","r_name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("r_id"=>"","r_name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取工单类型
	function getWorkType(){
		//header("Content-Type:text/html; charset=utf-8");
		$tenants_role = M("work_order_type");
		$arrData = $tenants_role->order("id asc")->field("id,type_name as name")->where("enabled = 'Y'")->select();

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取工单状态
	function getWorkStatus(){
		//header("Content-Type:text/html; charset=utf-8");
		$tenants_role = M("work_order_status");
		$arrData = $tenants_role->order("id asc")->field("id,status_name as name")->where("enabled = 'Y'")->select();

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

}

?>
