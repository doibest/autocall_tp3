<?php
class SystemReportAction extends Action
{
    function listCDR(){
		checkLogin();
        //分配增删改的权限
        $menuname = "CDR List";
        $p_menuname = $_SESSION['menu'][$menuname]; //父菜单
        $priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

        $this->assign("excel",$priv['excel']);
        $this->assign("Play",$priv['Play']);
        $this->assign("priv",$priv);

        $phone = $_REQUEST['phone'];
        $this->assign("phone",$phone);

        $this->display("","utf-8");
    }


	function cdrDataList(){
		getArrCache();//必须包含缓存文件
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $billsec = $_REQUEST['billsec'];
        $dept_id = $_REQUEST['dept_id'];
        $dept = $_REQUEST['dept'];//部门
        $workno = $_REQUEST['workno'];//工号
        $username = $_REQUEST['username'];//姓名
		$calltype = $_REQUEST['calltype'];
        $disposition = $_REQUEST['disposition'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
        $phone = $_REQUEST['phone'];
        $answered_way = $_REQUEST['answered_way'];
        $customer_info = $_REQUEST['customer_info'];
		$search_type = $_REQUEST["search_type"];

        $where = "1 ";
		$where .= " AND dcontext!='meeting-play'";
		if($answered_way == "Dial"){
			$where .= " AND lastapp = 'Dial'";
		}elseif($answered_way == "Queue"){
			$where .= " AND lastapp = 'Queue'";
		}elseif($answered_way == "Playback"){
			$where .= " AND lastapp = 'Playback'";
		}else{
			$where .= empty($answered_way) ? "" : " AND lastapp != 'Dial' AND lastapp != 'Queue' AND lastapp != 'Playback'";
		}
		if($customer_info == "Y"){
			$where .= " AND customer_info is not null";
		}else{
			$where .= empty($customer_info) ? "" : " AND (customer_info is null OR customer_info = '')";
		}
		//$where .= " AND dst not like '%*%'";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";
        $where .= empty($billsec)?"":" AND billsec >'$billsec'";

		$arrDep = $this->getDepTreeArray();
		$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$searchDeptId = rtrim($deptSet,",");
		$d_id = $_SESSION["user_info"]["d_id"];
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$dept_name_Set = rtrim($deptst,",");

		$user_name = $_SESSION['user_info']['username'];

		if($user_name != 'admin'){
			if( !$dept_id ){
				$where .= " AND dept_id IN ($dept_name_Set)";
			}
		}
        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND (src ='$src' OR (clid = '$src' AND dst = 'toqueue') )";
            $where .= empty($dst)?"":" AND dst ='$dst'  OR (dstchannel like '%$dst%' AND dst = 'toqueue') ";
            $where .= empty($workno)?"":" AND workno ='$workno'";
            $where .= empty($dept_id)?"":" AND dept_id ='$dept_id'";

        }else{
            $where .= empty($src)?"":" AND (src like '%$src%' OR (clid like '%$src%' AND dst = 'toqueue')) ";
            $where .= empty($dst)?"":" AND (dst like '%$dst%' OR (dstchannel like '%$dst%' AND dst = 'toqueue'))";
            $where .= empty($workno)?"":" AND workno like '%$workno%'";

			$where .=  empty($dept_id) ? "" : " AND dept_id IN ($searchDeptId)";
        }

		if($user_name != 'admin'){
			$where .= " AND dept_id IN ($dept_name_Set)";
		}
		//dump($dept_id);die;
        $where .= empty($calltype)?"":" AND calltype = '$calltype'";
        $where .= empty($phone)?"":" AND (src = '$phone' OR dst = '$phone')";

		//$disposition除了有ANSWERED和"NO ANSWER"两个值外，原来还有个BUSY等，所以只能用下面的方法了
		if(! empty($disposition) ){
			if($disposition == "ANSWERED"){
				$where .= " AND disposition = 'ANSWERED'";
			}else{
				$where .= " AND disposition != 'ANSWERED'";
			}
		}
		$pbxcdr = new Model('asteriskcdrdb.Cdr');
		import('ORG.Util.Page');
		$count = $pbxcdr->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$fields = "calldate,src,dst,lastapp,outnum,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield,customer_info,customer_service_info,clid,dstchannel";
		if($search_type == "xls"){
			$pbxCallData = $pbxcdr->field($fields)->order("calldate desc")->where($where)->select();
		}else{
			$pbxCallData = $pbxcdr->field($fields)->order("calldate desc")->limit($page->firstRow.",".$page->listRows)->where($where)->select();
		}

		//echo $pbxcdr->getLastSql();die;
		$totalBill = $pbxcdr->where($where)->sum("billsec");
		//dump($pbxCallData);

		$menuname = "CDR List";
        $p_menuname = $_SESSION['menu'][$menuname]; //父菜单
        $priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$in_answer = "<img src='Agent/Tpl/public/images/calltype/IN_ANSWER.png'>";
		$out_answer = "<img src='Agent/Tpl/public/images/calltype/OUT_ANSWER.png'>";
		$cdr_status_row = array('ANSWERED'=>'已接听','NO ANSWER'=>'未接听');
		//$cdr_calltype = array('IN'=>$in_answer,'OUT'=>$out_answer);
		$cdr_calltype = array('IN'=>"呼入",'OUT'=>"呼出","AUTOCALL"=>"自动外呼呼出","PREVIEW"=>"预览式外呼","PREDICTION"=>"预测式外呼","LOCAL"=>"分机互打");
		$cdr_calltype2 = array('IN'=>"呼入",'OUT'=>"呼出","AUTOCALL"=>"自动外呼呼出","PREVIEW"=>"预览式外呼","PREDICTION"=>"预测式外呼","LOCAL"=>"分机互打");
		$lastapp_row = array("Dial"=>"坐席","Queue"=>"队列","Playback"=>"语音","Busy"=>"忙","Congestion"=>"忙","Hangup"=>"挂机");
		$i = 0;
		$para_sys = readS();
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptId_name = $userArr["deptId_name"];
		foreach($pbxCallData as &$val){
			if($search_type == "xls"){
				$val["calltype2"] = $cdr_calltype2[$val["calltype"]];
			}
			$val["lastapp2"] = $val["lastapp"];
			$val["lastapp"] = $lastapp_row[$val["lastapp"]];
			if(!$val["lastapp"]){
				$val["lastapp"] = $val["lastapp2"];
			}

			if($val["dst"] == "toqueue"){
				$val["src"] = trim(array_shift(explode("\" <",$val["clid"])),"\"");
				$val["dst"] = array_pop(explode("/",array_shift(explode("@",$val["dstchannel"]))));
			}


			$val["srcP"] = trim($val["src"]);
			$val["dstP"] = trim($val["dst"]);
			if($val["calltype"] == "IN"){
				$val["call_phone"] = $val["srcP"];
			}else{
				$val["call_phone"] = $val["dstP"];
			}

			if($val["customer_service_info"]){
				$val["customer_service_info2"] = "鼠标移到这里查看售后信息";
			}

			if($_SESSION['user_info']['username'] != 'admin'){
				if( $para_sys["callrecords_hide"] == "pbx" || $para_sys["callrecords_hide"] == "all" ){
					if($val["calltype"] == "IN"){
						if(strlen($val["src"])>5){
							$val["src"] = substr($val["src"],0,3)."***".substr($val["src"],-4);
						}
						$val["call_phone"] = $val["srcP"];
					}else{
						if(strlen($val["dst"])>5){
							$val["dst"] = substr($val["dst"],0,3)."***".substr($val["dst"],-4);
						}
						$val["call_phone"] = $val["dstP"];
					}
				}
			}

			$val['selectCDR'] = "<input type='checkbox' name='selectlist' value='${val['userfield']}'></input>";
			$status = $cdr_status_row[$val["disposition"]];
			$val["disposition"] = $status;
			$Incalltype = $cdr_calltype[$val["calltype"]];
			$val["calltype"] = $Incalltype;

			$arrBill[] = $val["billsec"];

			$val['username'] = $cnName[$val['workno']];
			$val['dept_name'] = $deptId_name[$val['dept_id']];

			$val["uniqueid"] = trim($val["uniqueid"]);
			//$pbxCallData[$i]['operations'] = "";
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";

			if(file_exists($WAVfile) ){
				$pbxCallData[$i]['operations'] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["uniqueid"])."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .trim($val["uniqueid"]) ."&src=" .trim($val["src"]) ."&dst=" .trim($val["dst"]) ."'> 下载 </a>" ;
				$val["WAVfile"] = $WAVfile;
			}else{
				$val["WAVfile"] = "on";
			}
			if($priv['delete'] == "Y" || $user_name == 'admin'){
				$pbxCallData[$i]['operations'] .= "  <a href='#' onclick=\"deleteRecording('" .trim($val["uniqueid"]) ."');\"> 删除 </a>";
			}
			$pbxCallData[$i]["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));


			$i++;
		}

		$tpl_billsec_page = array_sum($arrBill);
		$tpl_billsec = sprintf("%02d",intval($totalBill/3600)).":".sprintf("%02d",intval(($totalBill%3600)/60)).":".sprintf("%02d",intval((($totalBill%3600)%60)));;
		$tmp_mon = array(array("billsec"=>"总时长：".$tpl_billsec));


		if($search_type == "xls"){
			$arrField = array ('calldate','src','dst','billsec','workno','username','dept_name','calltype2','disposition');
			$arrTitle = array ('通话时间','主叫','被叫','通话时长','工号','姓名','所属部门','呼叫类型','接听状态');
			$xls_count = count($arrField);
			$excelTiele = "PBX通话记录".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$pbxCallData,$excelTiele);
			die;
		}

		//dump($pbxCallData);die;
		$rowsList = count($pbxCallData) ? $pbxCallData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["footer"] = $tmp_mon;

		echo json_encode($ary);
	}

    function getDepList($arrDep,$id,$i){
        $selectedD_id = $_REQUEST['dept_id'];//表单提交的部门id
        $html ="<option value='{$arrDep[$id]["id"] }' ";
        if($selectedD_id==$id){//是否选择了当前部门
            $html .= "selected";
        }
        $html .= ">". str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;",$i)."|-" . $arrDep[$id]["name"] ."</option>";
		//dump($html);die;
        //先检测自己是否有子节点
        $hasSonNode = false;
        foreach($arrDep AS $key=>$v){
            if( $v['pid'] == $id ){
                $hasSonNode = true;break;
            }
        }
        //如果有子节点
        if($hasSonNode){
            //取出子节点id
            $sonID = explode(',',$arrDep[$id]['meAndSonId']);
            array_shift($sonID);
            foreach ( $sonID AS $val ) {
                $html .= $this->getDepList($arrDep,$val,$i+1);
            }
            return $html;
        }else{//如果没有子节点
            return $html;
        }
    }

	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }


    /*
     * 播放录音
     */
    function playCDR(){
		checkLogin();
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = str_replace(".wav",".mp3",$_GET['userfield']);//lame把.wav的文件转化成了.mp3文件
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		//http://192.168.1.69/agent.php?m=TasksCallRecords&a=playCDR&uniqueid=1363344674.456
		$AudioURL = "agent.php?m=PbxCallRecords&a=downloadCDR&clientphone=${clientphone}&uniqueid=${uniqueid}" ."&userfield=".$userfield;
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->assign("clientphone",$clientphone);



        $this->display("","utf-8");

    }

	function downloadCDR(){
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		$WAVfile = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp) .'/'. $userfield;

		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=" .str_replace(".wav", ".mp3", basename($WAVfile))."");
        readfile($WAVfile);

	}

	//批量下载录音
	function batchDownloadRecording(){
		$arrUF = $_POST['selectlist'];
		foreach($arrUF as $val){
			if($val != "on"){
				$arrData[] = $val;
			}
		}

		$monitorDir = '/var/spool/asterisk/monitor/downloadRecording/';
		$this->mkdirs($monitorDir);
		foreach($arrData as $val){
			$cmd = "cp ".$val." ".$monitorDir;
			$res = exec($cmd);
		}

		$cmds = "cd $monitorDir;tar -zcf downloadRecording.tar.gz *";
		exec($cmds);
		$filePath = $monitorDir."downloadRecording.tar.gz";
		$this->downloadFile($filePath);
	}


	function downloadFile($filePath){
		//dump($filePath);die;
		if(!file_exists($filePath)){
			echo "<script>alert('File $filePath is not exist!');</script>";
		}
        header('HTTP/1.1 200 OK');
        //header('Accept-Ranges: bytes');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($filePath)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($filePath))."");
        readfile($filePath);
		$cmd = " cd /var/spool/asterisk/monitor/; rm -rf downloadRecording";
		exec($cmd);
	}

	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}
	//批量下载录音--老版本-不管用
	function batchDownloadRecordingBak(){
		$arrUF = $_POST['selectlist'];
		//dump($arrUF);die;
		$monitorDir = '/var/spool/asterisk/monitor';
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Transfer-Encoding: Binary");
		header("Content-Disposition: attachment;filename=Recording-" .Date('Ymd-His') .".tar.gz");
		$cmd = "cd $monitorDir;tar -zcf - ";
		foreach($arrUF AS $uf){
			$arr = explode("_",$uf);
			$arr = explode(".",$arr[2]);
			$timestamp = $arr[0];
			//要转化成mp3文件
			$wavFile = Date('Y-m',$timestamp) ."/" .Date('d',$timestamp) ."/" .str_replace(".wav",".mp3",$uf);
			//echo $wavFile;die;
			if(file_exists($monitorDir ."/" .$wavFile)){
				$cmd .= $wavFile ." ";
			}
		}
		//echo $cmd;die;
		system($cmd);
	}
	/*
	function exportCdrToExcel(){
		getArrCache();//必须包含缓存文件
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dept_id = $_REQUEST['dept_id'];
        $dept = $_REQUEST['dept'];//部门
        $workno = $_REQUEST['workno'];//工号
        $username = $_REQUEST['username'];//姓名
		$calltype = $_REQUEST['calltype'];
        $disposition = $_REQUEST['disposition'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";

        $where = "1 ";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";

		$arrDep = $this->getDepTreeArray();
		$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$searchDeptId = rtrim($deptSet,",");
		$d_id = $_SESSION["user_info"]["d_id"];
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$dept_name_Set = rtrim($deptst,",");
		if($_SESSION['user_info']['username'] != 'admin'){
			if( !$dept_id ){
				$where .= " AND dept_id IN ($dept_name_Set)";
			}
		}
        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND src ='$src'";
            $where .= empty($workno)?"":" AND workno ='$workno'";
            $where .= empty($dept_id)?"":" AND dept_id ='$dept_id'";
            if(empty($username)){
                $where .= " ";
            }else{
                //dump($arrCacheWorkNo);dump($username);die;
                $arrWorkNo = Array();
                foreach($arrCacheWorkNo AS $k=>$v){
                    if($v['cn_name'] == $username){
                        $arrWorkNo[] = $k;
                    }
                }
                $where .= empty($arrWorkNo)?" AND 0":" AND workno IN " .gen_SQL_IN($arrWorkNo,"string");//构造查询字符串
            }
        }else{
            $where .= empty($src)?"":" AND src like '%$src%'";
            $where .= empty($workno)?"":" AND workno like '%$workno%'";
            $arrWorkNo = Array();
            foreach($arrCacheWorkNo AS $k=>$v){
                if(false !== strpos($v['cn_name'],$username)){
                    $arrWorkNo[] = $k;
                }
            }
            $where .= empty($arrWorkNo)?"":" AND workno IN " .gen_SQL_IN($arrWorkNo,"string");//构造查询字符串
            unset($arrWorkNo);
			if($_SESSION['user_info']['username'] != 'admin'){
				if( $dept_id ){
					$where .= " AND dept_id IN ($searchDeptId)";
				}else{
					$where .= " AND dept_id IN ($dept_name_Set)";
				}
			}
        }
		//dump($where);die;
        $where .= empty($calltype)?"":" AND calltype ='$calltype'";
		//$disposition除了有ANSWERED和"NO ANSWER"两个值外，原来还有个BUSY等，所以只能用下面的方法了
		if(! empty($disposition) ){
			if($disposition == "ANSWERED"){
				$where .= " AND disposition = 'ANSWERED'";
			}else{
				$where .= " AND disposition != 'ANSWERED'";
			}
		}
		$pbxcdr = new Model('asteriskcdrdb.Cdr');
		import('ORG.Util.Page');
		$count = $pbxcdr->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		if($_SESSION['user_info']['username'] == 'admin'){
			$pbxCallData = $pbxcdr->field("calldate,src,dst,outnum,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield")->order("calldate desc")->where($where)->select();
        }else{
			$pbxCallData = $pbxcdr->field("calldate,src,dst,outnum,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield")->order("calldate desc")->where($where)->select();
        }



		$content = "通话时间,主叫号码,被叫号码,出/入线路号码,通话时长/秒,工号,姓名,直属部门,上级部门,呼叫类型,接听状态\r\n";

		foreach( $pbxCallData AS $row ){
			if( $row['calltype']=='IN'){
				$row['calltype']="呼入";
			}else{
				$row['calltype']="呼出";
			}
			if( $row['disposition']=='ANSWERED'){
				$row['disposition']="已接";
			}else{
				$row['disposition']="未接";
			}
			$record = $row['calldate'].",".
					$row['src'].",".
					$row['dst'].",".
					$row['outnum'].",".
					$row['billsec'].",".
					$row['workno'].",".
					$row['username'].",".
					$row['dept'].",".
					$row['p_dept'].",".
					$row['calltype'].",".
					$row['disposition']."\r\n";
			$content .= $record;

		}
		//dump($content);die;
		$content = iconv("UTF-8","GB2312//IGNORE",$content);
		$d = date("D M j G:i:s T Y");
        header('HTTP/1.1 200 OK');
        header('Date: ' . $d);
        header('Last-Modified: ' . $d);
        header("Content-Type: application/force-download");
        header("Content-Length: " . strlen($content));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".time().".csv");
        echo $content;

    }
	*/


	function exportCdrToExcel(){
		getArrCache();//必须包含缓存文件
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dept_id = $_REQUEST['dept_id'];
        $dept = $_REQUEST['dept'];//部门
        $workno = $_REQUEST['workno'];//工号
        $username = $_REQUEST['username'];//姓名
		$calltype = $_REQUEST['calltype'];
        $disposition = $_REQUEST['disposition'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";

        $where = "1 ";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";

		$arrDep = $this->getDepTreeArray();
		$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$searchDeptId = rtrim($deptSet,",");
		$d_id = $_SESSION["user_info"]["d_id"];
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$dept_name_Set = rtrim($deptst,",");
		if($_SESSION['user_info']['username'] != 'admin'){
			if( !$dept_id ){
				$where .= " AND dept_id IN ($dept_name_Set)";
			}
		}
        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND src ='$src'";
            $where .= empty($workno)?"":" AND workno ='$workno'";
            $where .= empty($dept_id)?"":" AND dept_id ='$dept_id'";
            if(empty($username)){
                $where .= " ";
            }else{
                //dump($arrCacheWorkNo);dump($username);die;
                $arrWorkNo = Array();
                foreach($arrCacheWorkNo AS $k=>$v){
                    if($v['cn_name'] == $username){
                        $arrWorkNo[] = $k;
                    }
                }
                $where .= empty($arrWorkNo)?" AND 0":" AND workno IN " .gen_SQL_IN($arrWorkNo,"string");//构造查询字符串
            }
        }else{
            $where .= empty($src)?"":" AND src like '%$src%'";
            $where .= empty($workno)?"":" AND workno like '%$workno%'";
            $arrWorkNo = Array();
            foreach($arrCacheWorkNo AS $k=>$v){
                if(false !== strpos($v['cn_name'],$username)){
                    $arrWorkNo[] = $k;
                }
            }
            $where .= empty($arrWorkNo)?"":" AND workno IN " .gen_SQL_IN($arrWorkNo,"string");//构造查询字符串
            unset($arrWorkNo);
			if($_SESSION['user_info']['username'] != 'admin'){
				if( $dept_id ){
					$where .= " AND dept_id IN ($searchDeptId)";
				}else{
					$where .= " AND dept_id IN ($dept_name_Set)";
				}
			}
        }
		//dump($where);die;
        $where .= empty($calltype)?"":" AND calltype ='$calltype'";
		//$disposition除了有ANSWERED和"NO ANSWER"两个值外，原来还有个BUSY等，所以只能用下面的方法了
		if(! empty($disposition) ){
			if($disposition == "ANSWERED"){
				$where .= " AND disposition = 'ANSWERED'";
			}else{
				$where .= " AND disposition != 'ANSWERED'";
			}
		}


		$pbxcdr = new Model('asteriskcdrdb.Cdr');
		import('ORG.Util.Page');
		$count = $pbxcdr->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		if($_SESSION['user_info']['username'] == 'admin'){
			$pbxCallData = $pbxcdr->field("calldate,src,dst,outnum,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield")->order("calldate desc")->where($where)->select();
        }else{
			$pbxCallData = $pbxcdr->field("calldate,src,dst,outnum,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield")->order("calldate desc")->where($where)->select();
        }

		foreach($pbxCallData as &$val){
			$val['username'] = $arrCacheWorkNo[$val['workno']]['cn_name'];
			$val['dept_name'] = $arrCacheDept[$val['dept_id']]['dept_name'];
			$val['dept_pname'] = $arrCacheDept[$val['dept_id']]['dept_pname'];
		}

		//dump($pbxCallData);die;

		vendor("PHPExcel176.PHPExcel");
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);

		//设置行高度
		$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(22);

		$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);

		//设置字体大小加粗
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		//设置水平居中
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('E')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('F')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('G')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('I')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->getActiveSheet()->mergeCells('A1:K1');

		//设置表 标题内容
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue('A1', 'PBX通话记录  时间:'.date('Y-m-d H:i:s'))
			->setCellValue('A2', '通话时间')
			->setCellValue('B2', '主叫号码')
			->setCellValue('C2', '被叫号码')
			->setCellValue('D2', '出/入线路号码')
			->setCellValue('E2', '通话时长/秒')
			->setCellValue('F2', '工号')
			->setCellValue('G2', '姓名')
			->setCellValue('H2', '直属部门')
			->setCellValue('I2', '上级部门')
			->setCellValue('J2', '呼叫类型')
			->setCellValue('K2', '接听状态');
		$i = 3;
		foreach($pbxCallData as $val){
			if( $val['calltype']=='IN'){
				$val['calltype']="呼入";
			}else{
				$val['calltype']="呼出";
			}
			if( $val['disposition']=='ANSWERED'){
				$val['disposition']="已接";
			}else{
				$val['disposition']="未接";
			}
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $val['calldate']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $val['src']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $val['dst']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val['outnum']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val['billsec']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val['workno']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['username']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val['dept_name']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val['dept_pname']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val['calltype']);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $val['disposition']);
			$i++;
		}
		// Rename sheet
		//$objPHPExcel->getActiveSheet()->setTitle('通话记录');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$name = "通话记录";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

    }

	function exportCdrToCsv(){
		getArrCache();//必须包含缓存文件
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $billsec = $_REQUEST['billsec'];
        $dept_id = $_REQUEST['dept_id'];
        $dept = $_REQUEST['dept'];//部门
        $workno = $_REQUEST['workno'];//工号
        $username = $_REQUEST['username'];//姓名
		$calltype = $_REQUEST['calltype'];
        $disposition = $_REQUEST['disposition'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";

        $where = "1 ";
		$where .= " AND dcontext!='meeting-play'";
		//$where .= " AND dst not like '%*%'";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";
        $where .= empty($billsec)?"":" AND billsec >'$billsec'";

		$arrDep = $this->getDepTreeArray();
		$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$searchDeptId = rtrim($deptSet,",");
		$d_id = $_SESSION["user_info"]["d_id"];
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$dept_name_Set = rtrim($deptst,",");
		if($_SESSION['user_info']['username'] != 'admin'){
			if( !$dept_id ){
				$where .= " AND dept_id IN ($dept_name_Set)";
			}
		}
        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND src ='$src'";
            $where .= empty($dst)?"":" AND dst ='$dst'";
            $where .= empty($workno)?"":" AND workno ='$workno'";
            $where .= empty($dept_id)?"":" AND dept_id ='$dept_id'";
            if(empty($username)){
                $where .= " ";
            }else{
                //dump($arrCacheWorkNo);dump($username);die;
                $arrWorkNo = Array();
                foreach($arrCacheWorkNo AS $k=>$v){
                    if($v['cn_name'] == $username){
                        $arrWorkNo[] = $k;
                    }
                }
                $where .= empty($arrWorkNo)?" AND 0":" AND workno IN " .gen_SQL_IN($arrWorkNo,"string");//构造查询字符串
            }
        }else{
            $where .= empty($src)?"":" AND src like '%$src%'";
            $where .= empty($dst)?"":" AND dst like '%$dst%'";
            $where .= empty($workno)?"":" AND workno like '%$workno%'";
            $arrWorkNo = Array();
            foreach($arrCacheWorkNo AS $k=>$v){
                if(false !== strpos($v['cn_name'],$username)){
                    $arrWorkNo[] = $k;
                }
            }
            $where .= empty($arrWorkNo)?"":" AND workno IN " .gen_SQL_IN($arrWorkNo,"string");//构造查询字符串
            unset($arrWorkNo);
			$where .=  empty($dept_id) ? "" : " AND dept_id IN ($searchDeptId)";
        }

		if($_SESSION['user_info']['username'] != 'admin'){
			$where .= " AND dept_id IN ($dept_name_Set)";
		}
		//dump($dept_id);die;
        $where .= empty($calltype)?"":" AND calltype ='$calltype'";
		//$disposition除了有ANSWERED和"NO ANSWER"两个值外，原来还有个BUSY等，所以只能用下面的方法了
		if(! empty($disposition) ){
			if($disposition == "ANSWERED"){
				$where .= " AND disposition = 'ANSWERED'";
			}else{
				$where .= " AND disposition != 'ANSWERED'";
			}
		}
		$pbxcdr = new Model('asteriskcdrdb.Cdr');
		import('ORG.Util.Page');
		$count = $pbxcdr->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		if($_SESSION['user_info']['username'] == 'admin'){
			$pbxCallData = $pbxcdr->field("calldate,src,dst,lastapp,outnum,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield")->order("calldate desc")->limit($page->firstRow.",".$page->listRows)->where($where)->select();
        }else{
			$pbxCallData = $pbxcdr->field("calldate,src,dst,lastapp,outnum,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield")->order("calldate desc")->limit($page->firstRow.",".$page->listRows)->where($where)->select();
        }

		foreach($pbxCallData as &$val){
			$val['username'] = $arrCacheWorkNo[$val['workno']]['cn_name'];
			$val['dept_name'] = $arrCacheDept[$val['dept_id']]['dept_name'];
			$val['dept_pname'] = $arrCacheDept[$val['dept_id']]['dept_pname'];
		}
		//dump($pbxCallData);die;
		$content = "通话时间,主叫号码,被叫号码,出/入线路号码,通话时长/秒,工号,姓名,直属部门,上级部门,呼叫类型,接听状态\r\n";
		foreach($pbxCallData as $val){
			$rows = $val['calldate'].",".
					$val['src'].",".
					$val['dst'].",".
					$val['outnum'].",".
					$val['billsec'].",".
					$val['workno'].",".
					$val['username'].",".
					$val['dept_name'].",".
					$val['dept_pname'].",".
					$val['calltype'].",".
					$val['disposition']."\r\n";
			$content .= $rows;
		}
		//dump($content);die;
		$name = "通话记录".date("Ymd");
		$filename = iconv("utf-8","gb2312",$name);
		$this->export_csv($filename,$content);
    }

	function export_csv($filename,$data){
		$content = iconv("utf-8","gb2312",$data);
		$d = date("D M j G:i:s T Y");
        header('HTTP/1.1 200 OK');
        header('Date: ' . $d);
        header('Last-Modified: ' . $d);
        header("Content-Type: application/force-download");
        header("Content-Length: " . strlen($data));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".$filename.".csv");
        echo $data;
	}

	//队列未接来电列表
	function queueMissedCalls(){
		checkLogin();
		$Q = M("asterisk.queues_config");
		$arrQueue = $Q->field("extension AS queue,descr as name")->select();

		//只取最近一周的未接来电
		$queuecall = M("asteriskcdrdb.queue");
		$arrMissedCalls = $queuecall->field("queue,src,calltime")->where("status='NO ANSWER' AND TIMESTAMPDIFF(DAY,calltime,NOW())<7")->order("queue ASC,calltime DESC")->select();
		//echo $queuecall->getLastSql();die;
		$A = Array();
		foreach($arrMissedCalls AS $v){
			$A[$v['queue']][] = $v;
		}
		foreach($arrQueue AS &$v){
			$v['missedCalls'] = $A[$v['queue']];
		}
		//dump($arrQueue);die;

		$this->assign("arrQueue",$arrQueue);
		$this->display();
	}

	function queueMissedCallsData2(){
		$calltime_start = $_REQUEST["calltime_start"];
		$calltime_end = $_REQUEST["calltime_end"];
		$src = $_REQUEST["src"];
		$id = $_REQUEST["id"];

		$where = "q.status='NO ANSWER' AND q.queue = '$id'";
		$where .= empty($calltime_start) ? " AND TIMESTAMPDIFF(DAY,q.calltime,NOW())<7" : " AND calltime > '$calltime_start'";
		$where .= empty($calltime_end) ? "" : " AND q.calltime < '$calltime_end'";
		$where .= empty($src) ? "" : " AND q.src like '%$src%'";

		$queue = new Model("asteriskcdrdb.queue");
		$count = $queue->table("asteriskcdrdb.queue q")->join("asterisk.queues_config c on (q.queue = c.extension)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrMissedCalls = $queue->table("asteriskcdrdb.queue q")->field("q.queue,q.src,q.calltime,c.descr as name")->join("asterisk.queues_config c on (q.queue = c.extension)")->order("queue ASC,calltime DESC")->limit($page->firstRow.','.$page->listRows)->where($where)->select();


		$rowsList = count($arrMissedCalls) ? $arrMissedCalls : false;
		$arrQue["total"] = $count;
		$arrQue["rows"] = $rowsList;
		//dump($arrmail);die;
		echo json_encode($arrQue);
	}


	//队列未接来电列表
	function queueMissedCalls2(){
		checkLogin();
		$this->display();
	}

	function queueMissedCallsData(){
		$calltime_start = $_REQUEST["calltime_start"];
		$calltime_end = $_REQUEST["calltime_end"];
		$src = $_REQUEST["src"];

		$where = "status='NO ANSWER'";
		$where .= empty($calltime_start) ? " AND TIMESTAMPDIFF(DAY,calltime,NOW())<7" : " AND calltime > '$calltime_start'";
		$where .= empty($calltime_end) ? "" : " AND calltime < '$calltime_end'";
		$where .= empty($src) ? "" : " AND src like '%$src%'";

		$queues_config = new Model("asterisk.queues_config");
		$arrQueue = $queues_config->field("extension AS queue,descr as name")->select();

		$queue = new Model("asteriskcdrdb.queue");
		$arrMissedCalls = $queue->field("queue,src,calltime")->order("queue ASC,calltime DESC")->where($where)->select();
		$countQueue = count($arrMissedCalls);
		foreach($arrMissedCalls AS &$val){
			$val["id"] = $i+1;
			$val['iconCls'] =  'icon-queue';
			$arr[$val['queue']][] = $val;
			$i++;
		}

		foreach($arrQueue AS &$val){
			$val["id"] = $countQueue+1;
			$val['iconCls'] =  'door';
			$val['children'] = $arr[$val['queue']];
			if( !empty($val['children']) ){
				$val['state'] =  'closed';
			}else{
				$val['state'] =  'open';
			}
			$val["queue"] = $val["queue"]." (<span style='color:red;'>".count($val['children'])."</span>)";
			$countQueue++;
		}
		//dump($arrQueue);die;


		$rowsList = count($arrQueue) ? $arrQueue : false;
		$arrQue["total"] = count($arrQueue);
		$arrQue["rows"] = $rowsList;
		//dump($arrmail);die;
		echo json_encode($arrQue);
	}

	//删除当录音
	function deleteRecording(){
		$uniqueid = $_REQUEST['uniqueid'];
		$cdr = M('asteriskcdrdb.cdr');
		$result = $cdr->where("uniqueid='$uniqueid'")->delete();
		if(FALSE !== $result){
			echo "删除成功!";
		}else{
			echo "删除失败!";
		}
	}

	//批量上传录音
	function batchDeleteRecording(){
		$str_uniqueid = $_REQUEST['str_uniqueid'];
		$arr = explode(",",$str_uniqueid);

		$cdr = M('asteriskcdrdb.cdr');
		$i = 0;
		foreach($arr AS $uniqueid){
			if($uniqueid){
				$result = $cdr->where("uniqueid='$uniqueid'")->delete();
				$i++;
			}
		}
		echo "成功删除${i}条记录!";
	}

	function transCustomer(){
		$name = $_REQUEST["name"];
		$phone = $_REQUEST["phone"];

		$arrF = explode(",",$phone);
		$total = count($arrF);
		foreach($arrF as $val){
			if( is_numeric($val) && strlen($val)>4 ){
				$arrT[] = $val;
			}
		}
		$arrData = array_unique($arrT);
		$customer = new Model("customer");
		$arrTmp = $customer->field('phone1')->select();
		$arrExist = Array(); //把所有号码放入关联数组，hash
		foreach( $arrTmp AS $row){
			$arrExist[$row['phone1']] = 'Y';
		}

		$sql = "insert into customer(`phone1`,`createuser`,`createtime`) values";
		$value = "";
		$repeat = 0;
		foreach($arrData as $val){
			if( $arrExist[$val] == 'Y'){
				$repeat++;
				continue;
			}else{
				$str = "('".$val."','".$name."','".date("Y-m-d H:i:s")."')";
				$value .= empty($value)?$str:",$str";
			}
		}
		if( $value ){
			$sql .= $value;
			$result = $customer->execute($sql);
		}
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>"成功添加${result}条记录！"));
		} else {
			echo json_encode(array('msg'=>'添加失败！，号码已存在！'));
		}
	}


	function transOutboundCustomer(){
		$name = $_REQUEST["name"];
		$phone = $_REQUEST["phone"];
		$task_id = $_REQUEST["task_id"];

		$arrF = explode(",",$phone);
		$total = count($arrF);
		foreach($arrF as $val){
			if( is_numeric($val) && strlen($val)>4 ){
				$arrT[] = $val;
			}
		}
		$arrData = array_unique($arrT);
		$table = "sales_source_".$task_id;
		$customer = new Model("sales_source_".$task_id);
		$arrTmp = $customer->field('phone1')->select();
		$arrExist = Array(); //把所有号码放入关联数组，hash
		foreach( $arrTmp AS $row){
			$arrExist[$row['phone1']] = 'Y';
		}

		$sql = "insert into $table(`phone1`,`dealuser`,`createtime`,`locked`,`dealresult_id`) values";
		$value = "";
		$repeat = 0;
		foreach($arrData as $val){
			if( $arrExist[$val] == 'Y'){
				$repeat++;
				continue;
			}else{
				$str = "('".$val."','".$name."','".date("Y-m-d H:i:s")."','Y','0')";
				$value .= empty($value)?$str:",$str";
			}
		}
		if( $value ){
			$sql .= $value;
			$result = $customer->execute($sql);
		}
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>"成功添加${result}条记录！"));
		} else {
			echo json_encode(array('msg'=>'添加失败！，号码已存在！'));
		}
	}

	function viewCustomer(){
		$row = getSelectCache();
		$cmFields = getFieldCache();

		$phone = $_REQUEST["phone"];
		$arrF = explode(",",$phone);
		$total = count($arrF);
		foreach($arrF as $val){
			if( is_numeric($val) && strlen($val)>4 ){
				$arrT[] = $val;
			}
		}
		$arrP = array_unique($arrT);
		$str_phone = "'".implode("','",$arrP)."'";
		$customer = M("customer");
		$cdr = M("asteriskcdrdb.cdr");
		$arrData = $customer->field("phone1,phone2,name,sex,company,address")->where("phone1 in ($str_phone) OR phone2 in ($str_phone)")->select();
		//echo $customer->getLastSql();

		$arrSR = $this->getServiceRecords($str_phone);
		//dump($arrSR);die;
		foreach($arrData as &$val){
			$val["customer_info"] = "";
			foreach($cmFields as $vm){
				if($vm['list_enabled'] == 'Y'){
					if($vm["text_type"] == "2"){
						$val[$vm["en_name"]] = $row[$vm["en_name"]][$val[$vm["en_name"]]];
					}

					if($val[$vm["en_name"]]){
						$val["customer_info"] .= $vm["cn_name"]."：".$val[$vm["en_name"]]."  ";
					}
				}
			}
			$val["customer_service_info"] = implode("<br>",$arrSR[$val["phone1"]]);

			$result[] = $cdr->where("src = '".$val["phone1"]."' OR src = '".$val["phone2"]."' OR dst = '".$val["phone1"]."' OR dst = '".$val["phone2"]."'")->save(array("customer_info"=>$val["customer_info"],"customer_service_info"=>$val["customer_service_info"]));
		}
		//dump($arrData);die;
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'同步成功！'));
		} else {
			echo json_encode(array('msg'=>'找不到对应的资料！'));
		}

	}

	function getServiceRecords($str_phone){
		$servicerecords = M("servicerecords");
		$arrS = $servicerecords->where("phone in ($str_phone)")->select();
		$arrD = $this->groupBy($arrS,"phone");
		//dump($arrD);die;
		return $arrD;
	}

	function groupBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row;
		}
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$arrTY = $this->getServiceType();
		$arrF = "";
		foreach($ret as $key=>$val){
			foreach($ret[$key] as $k=>$vm){
				$arrF[$key][] = "处理坐席：".$vm["seat"]." / ".$cnName[$vm["seat"]]."   <br>服务号码：".$vm["phone"]."  <br>服务类型：".$arrTY[$vm["servicetype_id"]]."  <br>服务状态：".$vm["status"]."  <br>服务内容：".$vm["content"]."<br>";
			}
		}
		//dump($arrF);die;
		return $arrF;
	}

	function getServiceType(){
		$servicetype = M("servicetype");
		$arrST = $servicetype->select();
		$arrResult = array();
		foreach($arrST as $key=>$val){
			$arrResult[$val["id"]] = $val["servicename"];
		}
		return $arrResult;
	}

	function viewCustomer3(){
		$cmd_tgz = "/var/lib/asterisk/agi-bin/autocall/callRecordCustomerInfo.php";
		$res = exec($cmd_tgz,$retstr1,$retno1);

		if( $retno1 == 0 ){
			echo json_encode(array('success'=>true,'msg'=>'同步成功！'));
		}else{
			echo json_encode(array('msg'=>'找不到对应的资料！'));
		}
	}

	function viewCustomer2(){
		//header("Content-Type:text/html; charset=utf-8");
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $billsec = $_REQUEST['billsec'];
        $dept_id = $_REQUEST['dept_id'];
        $dept = $_REQUEST['dept'];//部门
        $workno = $_REQUEST['workno'];//工号
        $username = $_REQUEST['username'];//姓名
		$calltype = $_REQUEST['calltype'];
        $disposition = $_REQUEST['disposition'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
        $phone = $_REQUEST['phone'];
        $answered_way = $_REQUEST['answered_way'];
        $customer_info = $_REQUEST['customer_info'];

        $where = "1 ";
		$where .= " AND dcontext!='meeting-play'";
		if($answered_way == "Dial"){
			$where .= " AND lastapp = 'Dial'";
		}elseif($answered_way == "Queue"){
			$where .= " AND lastapp = 'Queue'";
		}else{
			$where .= empty($answered_way) ? "" : " AND lastapp != 'Dial' AND lastapp != 'Queue'";
		}
		if($customer_info == "Y"){
			$where .= " AND customer_info is not null";
		}else{
			$where .= empty($customer_info) ? "" : " AND (customer_info is null OR customer_info = '')";
		}
		//$where .= " AND dst not like '%*%'";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";
        $where .= empty($billsec)?"":" AND billsec >'$billsec'";
		$user_name = $_SESSION['user_info']['username'];

		$where .= empty($src)?"":" AND src like '%$src%'";
		$where .= empty($dst)?"":" AND dst like '%$dst%'";
		$where .= empty($workno)?"":" AND workno ='$workno'";

		//dump($dept_id);die;
        $where .= empty($calltype)?"":" AND calltype = '$calltype'";
        $where .= empty($phone)?"":" AND (src = '$phone' OR dst = '$phone')";

		//$disposition除了有ANSWERED和"NO ANSWER"两个值外，原来还有个BUSY等，所以只能用下面的方法了
		if(! empty($disposition) ){
			if($disposition == "ANSWERED"){
				$where .= " AND disposition = 'ANSWERED'";
			}else{
				$where .= " AND disposition != 'ANSWERED'";
			}
		}

		$row = getSelectCache();
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){    //&& $val['text_type'] != '3'
				$arrField[] = $val['en_name'];
			}
		}
		$fields = implode(",",$arrField);

		$cdr = M("asteriskcdrdb.cdr");
		$where = "1 ";
		$arrCD = $cdr->Distinct(true)->field("src,dst")->where($where)->select();
		foreach($arrCD as $val){
			if(is_numeric($val["src"]) && strlen($val["src"])>4){
				$arrT[] = $val["src"];
			}
			if(is_numeric($val["dst"]) && strlen($val["dst"])>4){
				$arrT[] = $val["dst"];
			}
		}
		//dump($fields);die;

		$arrP = array_unique($arrT);
		$str_phone = "'".implode("','",$arrP)."'";
		$customer = M("customer");
		$arrData = $customer->field($fields)->where("phone1 in ($str_phone) OR phone2 in ($str_phone)")->select();
		//echo $customer->getLastSql();

		$arrSR = $this->getServiceRecords($str_phone);
		foreach($arrData as &$val){
			//$val["sex"] = $row["sex"][$val["sex"]];
			$val["customer_info"] = "";
			foreach($cmFields as $vm){
				if($vm['list_enabled'] == 'Y'){
					if($vm["text_type"] == "2"){
						$val[$vm["en_name"]] = $row[$vm["en_name"]][$val[$vm["en_name"]]];
					}

					if($val[$vm["en_name"]]){
						$val["customer_info"] .= $vm["cn_name"]."：".$val[$vm["en_name"]]."  ";
					}
				}
			}
			$val["customer_service_info"] = implode("<br>",$arrSR[$val["phone1"]]);
			/*
			$arrP2[] = $val["phone1"];
			if($val["phone2"]){
				$arrP2[] = $val["phone2"];
			}
			*/
			$result[] = $cdr->where("src = '".$val["phone1"]."' OR src = '".$val["phone2"]."' OR dst = '".$val["phone1"]."' OR dst = '".$val["phone2"]."'")->save(array("customer_info"=>$val["customer_info"],"customer_service_info"=>$val["customer_service_info"]));
		}
		//dump($arrData);die;

		/*
		foreach($arrP as $val){
			if(!in_array($val,$arrP2)){
				$result[] = $cdr->where("src = '".$val."' OR dst = '".$val."' ")->save(array("customer_info"=>"没有找到对应资料！"));
			}
		}
		*/
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'同步成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}

	}

}
?>

