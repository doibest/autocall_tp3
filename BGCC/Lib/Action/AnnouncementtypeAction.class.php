<?php
class AnnouncementtypeAction extends Action{
	function announcementList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Announcement Type";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		//dump($priv);
		$this->display();
	}

	function announcementDataList(){
		$announcement = new Model("announcementtype");
		import('ORG.Util.Page');
		$count = $announcement->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$announcementData = $announcement->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($announcementData) ? $announcementData : false;
		$arrannouncement["total"] = $count;
		$arrannouncement["rows"] = $rowsList;

		echo json_encode($arrannouncement);
	}

	function insertannouncement(){
		$announcement = new Model("announcementtype");
		$arrData = array(
			"announcementname"=>$_REQUEST["announcementname"],
		);
		$result = $announcement->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function updateannouncement(){
		$id = $_REQUEST["id"];
		$announcement = new Model("announcementtype");
		$arrData = array(
			"announcementname"=>$_REQUEST["announcementname"],
		);
		$result = $announcement->data($arrData)->where("id = $id")->save();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function deleteannouncement(){
		$id = $_REQUEST["id"];
		$announcement = new Model("announcementtype");
		$result = $announcement->where("id = $id")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}
}

?>
