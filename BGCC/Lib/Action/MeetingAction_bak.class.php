<?php
class MeetingAction extends Action{
	function listMeeting(){
		checkLogin();

		$menuname = "List Meeting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function mettingData(){
		$m = M("meeting");
		$count = $m->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$tmp_Rows = $m->order("starttime DESC")->select();
		$rowsList = count($tmp_Rows) ? $tmp_Rows : false;
		$arrMetting["total"] = $count;
		$arrMetting["rows"] = $rowsList;

		echo json_encode($arrMetting);
	}

	function checkMeetingExist(){
		$exten = $_GET['exten'];
		$m = M('meeting');
		$num = $m->where("exten='$exten'")->count();
		//dump($m->getLastSql());die;
		if( $num > 0){
			echo "YES";
		}else{
			echo "NO";
		}
	}

	function addMeetingDo(){
		$arrMeet = Array(
			"exten"=>$_POST['exten'],
			"starttime"=>$_POST['starttime'],
			"userpin"=>$_POST['userpin'],
			"adminpin"=>$_POST['adminpin'],
			"description"=>$_POST['description'],
		);
		$m = M('meeting');
		$res = $m->add($arrMeet);
		if ($res){
			echo json_encode(array('success'=>true,'msg'=>'会议室添加成功!'));
		} else {
			echo json_encode(array('msg'=>'会议室添加失败!'));
		}
	}

	function editMeetingDo(){
		$exten = $_REQUEST['exten'];
		$meeting = new Model("meeting");
		$arrMeet = Array(
			"starttime"=>$_POST['starttime'],
			"userpin"=>$_POST['userpin'],
			"adminpin"=>$_POST['adminpin'],
			"description"=>$_POST['description'],
		);
		$res = $meeting->where("exten='$exten'")->save($arrMeet);
		if ($res !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteMeeting(){
		$exten = $_REQUEST["exten"];
		$meeting = new Model("meeting");
		$res = $meeting->where("exten='$exten'")->delete();
		if ($res){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function loginMeeting(){
		//dump($_SESSION['meeting']);die;
		if($_SESSION['meeting']['exten']){
			header("Location: index.php?m=Meeting&a=startMeeting");
		}else{
			$this->display();
		}

	}

	function checkMeetingLogin(){
		$adminpin = $_POST['adminpin'];
		$exten = $_POST['exten'];

		$m = M('meeting');
		$res = $m->where("exten='$exten' AND adminpin='$adminpin'")->find();
		//dump($m->getLastSql());die;
		if( $res ){ //可以查到会议
			$_SESSION['meeting'] = $res; //设置会议室号码
			header("Location: index.php?m=Meeting&a=startMeeting");
		}else{
			//$this->error("登陆失败，用户名或者密码错误!");
			goback("登录失败！");
		}
	}

	/*
	function startMeeting(){
		//dump($_SESSION);die;
		//dump($_SESSION['meeting']);die;
		if(!$_SESSION['meeting']){
			header("Location: index.php?m=Meeting&a=loginMeeting");
		}
		$extension = $_SESSION['user_info']["extension"];
		$this->assign("extension",$extension);//管理员分机号
		$exten = $_SESSION['meeting']["exten"];
		$this->assign("exten",$exten);//会议室号码
		$userpin = $_SESSION['meeting']["userpin"];
		$this->assign("userpin",$userpin);
		//中继
		$ast = M("asterisk.trunks");
		$trunks = $ast->field("CONCAT(tech,'/',channelid) AS trunk")->select();
		$this->assign("trunks",$trunks);//中继

		$arrInfo = Array();
		$_SESSION['meeting']['members'] = isset($_SESSION['meeting']['members'])?$_SESSION['meeting']['members']:Array();
		//dump($_SESSION['meeting']['members'] );die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		getArrCache();
		$i = 0;
		foreach($_SESSION['meeting']['members'] AS $v){
			$arrInfo[$i] = Array();
			$arrInfo[$i]['num'] = $v;//电话号码

			$arrInfo[$i]['cn_name'] = '';
			foreach( $arrCacheWorkNo AS $val ){
				if( $val['extension'] == $v ){
					$arrInfo[$i]['cn_name'] = $val['cn_name']; //姓名
					break;
				}
			}
			$arrHint = $bmi->getHint($v);//状态
			if( $arrHint['stat'] == "Idle" ){
				$arrInfo[$i]['stat'] = '空闲';
			}elseif( $arrHint['stat'] == "Unavailable" ){
				$arrInfo[$i]['stat'] = '未注册';
			}else{
				$arrInfo[$i]['stat'] = '忙';
			}
			$i++;
		}
		unset($i);
		//dump($exten);die;
		//dump($bmi->listMeeting2($exten) );die;
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if($v['state']=='Muted'){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$this->assign('count',count($arrMeeting));
		$this->assign('arrMeeting',$arrMeeting);

		//dump($arrInfo);die;
		$this->assign("arrInfo","(" .json_encode($arrInfo) .")");
		$this->display();
	}
	*/

	function startMeeting(){
		//dump($_SESSION);die;
		if(!$_SESSION['meeting']){
			header("Location: index.php?m=Meeting&a=loginMeeting");
		}
		$extension = $_SESSION['user_info']["extension"];
		$this->assign("extension",$extension);//发起会议的分机号
		//会议室号码和登陆密码
		$exten = $_SESSION['meeting']["exten"];
		$this->assign("exten",$exten);//会议室号码
		$userpin = $_SESSION['meeting']["userpin"];
		$this->assign("userpin",$userpin);
		//中继
		$ast = M("asterisk.trunks");
		$trunks = $ast->field("CONCAT(tech,'/',channelid) AS trunk")->select();
		$this->assign("trunks",$trunks);//中继

		$arrInfo = Array();
		$_SESSION['meeting']['members'] = isset($_SESSION['meeting']['members'])?$_SESSION['meeting']['members']:Array();
		//dump($_SESSION['meeting']['members'] );die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		getArrCache();
		$i = 0;
		foreach($_SESSION['meeting']['members'] AS $v){
			$arrInfo[$i] = Array();
			$arrInfo[$i]['num'] = $v;//电话号码
			$arrInfo[$i]['cn_name'] = '';
			foreach( $arrCacheWorkNo AS $val ){
				if( $val['extension'] == $v ){
					$arrInfo[$i]['cn_name'] = $val['cn_name']; //姓名
					break;
				}
			}
			$arrHint = $bmi->getHint($v);//状态
			if( $arrHint['stat'] == "Idle" ){
				$arrInfo[$i]['stat'] = '空闲';
			}elseif( $arrHint['stat'] == "Unavailable" ){
				$arrInfo[$i]['stat'] = '未注册';
			}elseif( $arrHint['stat'] == NULL ){
				$arrInfo[$i]['stat'] = '空闲';
			}else{
				$arrInfo[$i]['stat'] = '忙';
			}
			$i++;
		}
		unset($i);
		//dump($exten);die;
		//dump($bmi->listMeeting2($exten) );die;
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if(false!== strpos($v['state'],'Muted')){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$this->assign('count',count($arrMeeting));
		$rowsList = count($arrMeeting) ? $arrMeeting : false;
		$arrMeet["total"] = count($arrMeeting);
		$arrMeet["rows"] = $rowsList;
		$strTmp = json_encode($arrMeet);
		$this->assign("strTmp",$strTmp);
		//dump($arrInfo);die;
		$this->assign("arrInfo","(" .json_encode($arrInfo) .")");
		$this->display();
	}


	//这个函数非常重要，当页面离开后是否保存发起呼叫的号码就是靠这个函数
	function checkMeetingMember(){
		//header("Content-Type:text/html;charset=UTF-8");
		$arrInfo = Array();
		$_SESSION['meeting']['members'] = isset($_SESSION['meeting']['members'])?$_SESSION['meeting']['members']:Array();
		$arrNum = explode(',',$_GET['numStr']);//号码用逗号隔开
		foreach($arrNum AS $v){//先进行一下号码过滤
			if( strlen($v)>=3 && is_numeric($v) ){
				if( in_array($v,$_SESSION['meeting']['members']) ){
					continue;
				}else{
					array_push($_SESSION['meeting']['members'],$v);
				}
			}
		}
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		getArrCache();

		$i = 0;
		foreach($_SESSION['meeting']['members'] AS $v){
			$arrInfo[$i] = Array();
			$arrInfo[$i]['num'] = $v;//电话号码

			$arrInfo[$i]['cn_name'] = '';
			foreach( $arrCacheWorkNo AS $val ){
				if( $val['extension'] == $v ){
					$arrInfo[$i]['cn_name'] = $val['cn_name']; //姓名
					break;
				}
			}

			$arrHint = $bmi->getHint($v);//状态
			if( $arrHint['stat'] == "Idle" ){
				$arrInfo[$i]['stat'] = '空闲';
			}elseif( $arrHint['stat'] == "Unavailable" ){
				$arrInfo[$i]['stat'] = '未注册';
			}elseif( $arrHint['stat'] == NULL ){
				$arrInfo[$i]['stat'] = '空闲';
			}else{
				$arrInfo[$i]['stat'] = '忙';
			}
			$i++;
		}
		unset($i);
		echo "(" .json_encode($arrInfo) .")";
	}


	function inviteMeeting(){
		header("Content-Type:text/html;charset=UTF-8");
		$trunk = $_REQUEST['trunk'];
		$userpw = $_REQUEST['userpw'];
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		if( isset($_GET['numStr']) ){
			$arr = explode(',',$_GET['numStr']);//号码用逗号隔开
			import('ORG.Pbx.bmi');
			$bmi = new bmi();
			$bmi->loadAGI();
			foreach( $arr AS $v){
				$request_parameters = Array(
					//"Channel"       =>      "SIP/$v",
					"Channel"       =>      strlen($v)>5?"$trunk/$v":"SIP/$v",
					"Exten"         =>      "888888",
					"Context"       =>      'bangian-meetme',
					"Priority"      =>      "1",
					"Timeout"       =>      "45000",
					"CallerID"      =>      "\"888888\" <888888>",
					"Async"			=>      "true",
					"Variable"		=>		"__exten=$exten,__ComingID=$v,__userpw=$userpw",
				);
				//dump($request_parameters);
				$bmi->asm->send_request('Originate',$request_parameters);
			}
		}
		echo "ok";
	}

	//删掉参会号码
	function removePhone(){
		$arrInfo = Array();
		$_SESSION['meeting']['members'] = isset($_SESSION['meeting']['members'])?$_SESSION['meeting']['members']:Array();
		$arrNum = explode(',',$_GET['numStr']);//号码用逗号隔开
		//dump($arrNum);die;
		//求差集
		$_SESSION['meeting']['members'] = array_diff($_SESSION['meeting']['members'],$arrNum);
		/*
		$i = 0;
		$arrTmp = Array();
		foreach($_SESSION['meeting']['members'] AS $v){
			if( in_array($v,$arrNum) ){
				break;
			}else{
				$arrTmp[] = $v;
			}
			$i++;
		}
		unset($_SESSION['meeting']['members']);
		$_SESSION['meeting']['members'] = $arrTmp;
		//dump($_SESSION['meeting']['members'] );die;
		*/
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		getArrCache();

		$i = 0;
		//dump($_SESSION['meeting']['members'] );
		//dump($arrInfo);die;
		//dump( count($_SESSION['meeting']['members']) );die;
		foreach($_SESSION['meeting']['members'] AS $v){
			$arrInfo[$i] = Array();
			$arrInfo[$i]['num'] = $v;//电话号码
			//dump($_SESSION['meeting']['members'] );
			$arrInfo[$i]['cn_name'] = '';
			foreach( $arrCacheWorkNo AS $val ){
				if( $val['extension'] == $v ){
					$arrInfo[$i]['cn_name'] = $val['cn_name']; //姓名
					break;
				}
			}

			$arrHint = $bmi->getHint($v);//状态
			if( $arrHint['stat'] == "Idle" ){
				$arrInfo[$i]['stat'] = '空闲';
			}elseif( $arrHint['stat'] == "Unavailable" ){
				$arrInfo[$i]['stat'] = '未注册';
			}else{
				$arrInfo[$i]['stat'] = '忙';
			}
			$i++;
		}
		//dump($i);die;
		//unset($i);

		//dump($arrInfo);die;
		echo "(" .json_encode($arrInfo) .")";
	}


	function muteSelected(){
		//header("Content-Type:text/html;charset=UTF-8");
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		$arrNum = explode(',',$_GET['numStr']);//号码用逗号隔开
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		foreach($arrNum AS $v){
			$bmi->loadAGI();
			$bmi->asm->Command("meetme mute {$exten} {$v}");
			//echo "{$exten} ";
		}
		//获取会议室号码状态
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			//dump($arrTemp);die;
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if(false!== strpos($v['state'],'Muted')){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$rowsList = count($arrMeeting) ? $arrMeeting : false;
		$arrMeet["total"] = count($arrMeeting);
		$arrMeet["rows"] = $rowsList;
		$return = Array();
		$return['res'] = 'ok';
		$return['data'] = $arrMeet;
		//dump($return);die;
		echo json_encode($return);
	}

	function unmuteSelected(){

		//header("Content-Type:text/html;charset=UTF-8");
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		$arrNum = explode(',',$_GET['numStr']);//号码用逗号隔开
		//dump($arrNum);die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		foreach($arrNum AS $v){
			$bmi->loadAGI();
			$bmi->asm->Command("meetme unmute {$exten} {$v}");
		}
		//获取会议室号码状态
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			//dump($arrTemp);die;
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if(false!== strpos($v['state'],'Muted')){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$rowsList = count($arrMeeting) ? $arrMeeting : false;
		$arrMeet["total"] = count($arrMeeting);
		$arrMeet["rows"] = $rowsList;
		$return = Array();
		$return['res'] = 'ok';
		$return['data'] = $arrMeet;
		//dump($return);die;
		echo json_encode($return);
	}

	function kickOne(){
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		$arrNum = explode(',',$_GET['numStr']);//号码用逗号隔开
		//dump($arrNum);die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		foreach($arrNum AS $v){
			$bmi->loadAGI();
			$bmi->asm->Command("meetme kick {$exten} {$v}");
		}
		sleep(3);
		//获取会议室号码状态
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			//dump($arrTemp);die;
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if(false!== strpos($v['state'],'Muted')){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$rowsList = count($arrMeeting) ? $arrMeeting : false;
		$arrMeet["total"] = count($arrMeeting);
		$arrMeet["rows"] = $rowsList;
		$return = Array();
		$return['res'] = 'ok';
		$return['data'] = $arrMeet;
		//dump($return);die;
		echo json_encode($return);
	}


	function kickAll(){
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		unset($_SESSION['meeting']);
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$bmi->asm->Command("meetme kick {$exten} all");

		echo "YES";

	}

	function meetingLogout(){
		unset($_SESSION['meeting']);
		goBack("您已登出会议室！","index.php?m=Meeting&a=loginMeeting");
	}

}

?>

