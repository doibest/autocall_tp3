<?php
class ExpressPrintAction extends Action{

    /**
 * 获取程序版本信息
 *
 * @access  private
 * @param
 * @return  array
 */

function start_prc(){


	$funlist = array(
                 'get_version',           //获取程序版本信息
                 'server_info',           //获取服务器信息
                 'get_goods_list',        //获取商品列表
                 'get_orders_list',       //获取订单列表
                 'get_order_info',        //获取订单信息
                 'pos_order_info',        //提交订单信息
                 'pos_order_shipping',    //变更订单发货状态
                 'shipping_list',         //获取已安装的配送方式
                 'get_order_print_info',  //获取订单打印信息
                 'admin_signin',          //管理员登陆验证
                 'get_table_count',       //获取数据表记录数
                 );
	import("ORG.phprpc.phprpc_server");
	$server = new PHPRPC_Server();
	$server->add($funlist);
	$server->start();
}

function get_orders_list($page='0', $pagesize='10', $where='order_status=1 and shipping_status=3')
{
    /*
	if (!check_priviege())
    {
        exit;
    }*/

    $order = '';
    if ($where =='order_status=1 and shipping_status=1')
    {
        $today      = strtotime(local_date('Y-m-d'));
        $start_date = $today;
        $end_date   = $today + 86399;
        $where .= " and (shipping_time >= '$start_date' and shipping_time <= '$end_date')";
        $order = "createtime DESC, shopping_name, createtime DESC";
    }
	else
	{
		$order = "id ASC, createtime DESC, shopping_name ";
	}
    /*
    $sql = "SELECT order_id, order_sn, consignee, address, invoice_no, shipping_name, pay_name, pay_status, shipping_status, add_time ".
           " FROM ". $GLOBALS['ecs']->table('order_info') .
           " WHERE $where and ( address<>'' ) " .
           " ORDER BY $order  LIMIT " . $page * $pagesize . ",$pagesize";*/
	$orderinfo = M("order_info");

    //$res = $GLOBALS['db']->getAll($sql);
	$res = $orderinfo->field("id AS `order_id`,order_num AS `order_sn`,`consignee`,delivery_address AS `address`,logistics_account AS `invoice_no`,shopping_name AS `shipping_name`,payment AS `pay_name`,`pay_status`,`shipping_status`,createtime AS `add_time`")->where("$where and ( delivery_address<>'' ) ")->order($order)->limit($page * $pagesize.",$pagesize")->select();
	//echo $orderinfo->getlastsql();


	//SELECT  FROM `order_info` WHERE 1 AND ( delivery_address<>'' )  LIMIT 0,1

    foreach($res as $key => $val)
    {
        $res[$key]['order_id']      = $val['order_id'];
        $res[$key]['order_sn']      = $val['order_sn'];
        $res[$key]['consignee']     = $val['consignee'];
        $res[$key]['address']       = $val['address'];
        $res[$key]['goods_amount']  = $val['goods_amount'];
        $res[$key]['shipping_name'] = $val['shipping_name'];
        $res[$key]['pay_name']      = $val['pay_name'];
        $res[$key]['shipping_status'] = $GLOBALS['_LANG']['ps'][$val['pay_status']].','.$GLOBALS['_LANG']['ss'][$val['shipping_status']];
        $res[$key]['add_time']      =  $val['add_time'];
    }
	//print_r($res);
    return $res;
}


function get_order_print_info($order_id="3")
{
	$para_sys = readS();
    if (!check_priviege())
    {
        exit;
    }
    /*
    $sql = "SELECT consignee, address, tel, mobile, order_amount, shipping_id, shipping_name, pay_name, zipcode, to_buyer ".
           " FROM ". $GLOBALS['ecs']->table('order_info') .
           " WHERE order_id = '$order_id' LIMIT 1 ";
    $res = $GLOBALS['db']->getRow($sql);*/

	$orderinfo = M("order_info");

    //$res = $GLOBALS['db']->getAll($sql);
	$res = $orderinfo->field("cope_money as order_amount, telephone as tel,phone as mobile,`consignee`,delivery_address AS `address`,shopping_name AS `shipping_name`,payment AS `pay_name`,zipcode,id as to_buyer")->where("id = '$order_id' ")->find();

    $shipping_id = $res['shipping_name'];
    $res['consignee'] = str_replace("[", "['[", $res['consignee']);
    $res['consignee'] = str_replace("]", "]']", $res['consignee']);


	$goods = $orderinfo->table("order_goods og,shop_goods as g")->field("g.good_name, og.goods_num")->where("og.order_id = '$order_id' AND og.goods_id = g.goods_id")->select();

    /*
	$sql = "SELECT g.goods_brief, og.goods_number FROM " . $GLOBALS['ecs']->table('order_goods') . " AS og, ".
           $GLOBALS['ecs']->table('goods') . " AS g ".
           " WHERE og.order_id = '$order_id' AND og.goods_id = g.goods_id ";
    $goods = $GLOBALS['db']->getAll($sql);
	*/
	$sql = $orderinfo->getlastsql();
    $contentname = '';
    $CRLF = '';
    foreach($goods AS $key => $value)
    {
        if ($key > 0) $CRLF= "，";
        $contentname .= $CRLF.$goods[$key]['good_name']. ($goods[$key]['goods_number']>1 ? "(数量:". $goods[$key]['goods_number'] .")" : '');
    }
    /*
    $sql = "SELECT shipping_code FROM ". $GLOBALS['ecs']->table('shipping').
           " WHERE shipping_id = '$shipping_id' ";
    $shipping_code = $GLOBALS['db']->getOne($sql);*/
    $row_shopping_name = array("1"=>"顺丰快递","2"=>"圆通速递","3"=>"邮政快递包裹","4"=>"市内快递","5"=>"申通快递","6"=>"邮局平邮","7"=>"城际快递");
    $shipping_code = $row_shopping_name[$res['shipping_name']];
	$arr = array();

    //寄件人信息
	/*
    $arr['sendername']       = '邦建通讯';
    $arr['senderaddress']    = '深圳市宝安区华美居Ｄ区1号楼913#';
    $arr['senderphone']      = '4008-888-888';
    $arr['senderzipcode']    = '510000';
	*/
    $arr['sendername']       = $para_sys["sendername"];
    $arr['senderaddress']    = $para_sys["senderaddress"];
    $arr['senderphone']      = $para_sys["senderphone"];
    $arr['senderzipcode']    = $para_sys["senderzipcode"];


    //收件人信息
    $arr['recipientname']    = $res['consignee'];
    $arr['recipientaddress'] = $res['address'];
    $arr['recipientzipcode'] = $res['zipcode'];
    $arr['recipientphone']   = $res['tel'];
    $arr['recipientmobile']  = $res['tel'] == $res['mobile'] ? '': $res['mobile'];
    $arr['payamount_small']  = $res['order_amount'];
    $arr['payamount_big']    = $res['order_amount'];
    $arr['contentname']      = $contentname;
    $arr['payment']          = '√月结';
    $arr['shipping_code']    = $shipping_code;
    $arr['to_buyer']         = $res['to_buyer'];
	print_r($$sql);
    return $sql;
}
}

?>
