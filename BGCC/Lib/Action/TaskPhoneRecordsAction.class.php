<?php
class TaskPhoneRecordsAction extends Action{
	function tprList(){
		checkLogin();
		$sales_task = new Model("sales_task");
		$d_id = $_SESSION["user_info"]["d_id"];	
		$username = $_SESSION["user_info"]["username"];	
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$where = "enable='Y'";
		$where .= " AND dept_id IN ($deptSet)";

		if($username == "admin"){
			$taskList = $sales_task->field("id as task_id,name")->order("createtime")->where("enable='Y'")->select();
		}else{
			$taskList = $sales_task->field("id as task_id,name")->order("createtime")->where($where)->select();
		}
		
		$this->assign("taskList",$taskList);
		
		
		//分配增删改的权限
        $menuname = "CDR List";
        $p_menuname = $_SESSION['menu'][$menuname]; //父菜单
        $priv = $_SESSION["user_priv"][$p_menuname][$menuname];		
        
		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}
		
        $this->assign("excel",$priv['excel']);
        $this->assign("Play",$priv['Play']);
		
		//dump( $_SESSION["user_info"]);die;
		$this->display();
	}
	
	
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;
		
	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }
	
	
	function taskRecordsList(){
		$AM = M('autocall_mark');
		$arrTag = $AM->order("key_value ASC")->select();
		$tag = Array();
		foreach($arrTag AS $row){
			$tag[$row['key_value'] ] = $row['key_description'];
		}
		$username = $_SESSION["user_info"]["username"];
		$task_id = $_REQUEST['task_id'];
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $billsec = $_REQUEST['billsec'];
        $success_enable = $_REQUEST['success_enable'];
		$recordtag = $_REQUEST['recordtag'];
		$disposition = $_REQUEST['disposition'];
		$workno = $_REQUEST['workno'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
        $search_type = $_REQUEST['search_type'];
		
		$where = "1 ";
        $where .= empty($billsec)?"":" AND billsec >'$billsec'";
        $where .= empty($success_enable)?"":" AND success_enable = '$success_enable'";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";
        //$where .= empty($recordtag)?"":" AND recordtag = $recordtag";
		if($recordtag == "-1"){
			$where .= " AND recordtag is not null AND recordtag !=''";
		}else{
			$where .= empty($recordtag)?"":" AND recordtag = $recordtag";
		}
		
        $where .= empty($disposition)?"":" AND disposition = '$disposition'";
        $where .= empty($workno)?"":" AND workno = '$workno'";
        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND src ='$src'";
            $where .= empty($dst)?"":" AND dst ='$dst'";
        }else{
            $where .= empty($src)?"":" AND src like '%$src%'";
            $where .= empty($dst)?"":" AND dst like '%$dst%'";
        }
		/*
		if($username != "admin"){
			$where .= " AND workno = '$username'";
		}
		*/
		if($_GET["roles"] == "agnet"){
			$where .= " AND workno = '$username'";
		}
		import('ORG.Util.Page');
		$sales_cdr = new Model("sales_cdr_".$task_id);
		$count = $sales_cdr->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}  
		$page = new Page($count,$page_rows);
		
		if($search_type == "search"){
			$sales_cdrList = $sales_cdr->order("calldate desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();	
		}else{
			set_time_limit(0);
			ini_set('memory_limit', '-1');
			$content = "通话时间,主叫号码,被叫号码,通道,通话时长,接听者,接听状态,通话类型,录音标记\r\n";
			
			$sales_cdrList = $sales_cdr->order("calldate desc")->where($where)->select();
		}
		//echo $sales_cdr->getLastSql();die;

		//$cdr_status_row = array('ANSWERED'=>'转人工已接','AUTOCALL'=>'外呼失败','NO ANSWER'=>'转人工未接','PLAYIVR'=>"未转接",'BUSY'=>'忙',"FAILED"=>"外呼失败");
		$cdr_status_row = array('ANSWERED'=>'转人工已接','AUTOCALL'=>'外呼失败','NO ANSWER'=>'未转接','PLAYIVR'=>"未转接",'BUSY'=>'忙',"FAILED"=>"外呼失败");
		$cdr_calltype_row = array('preview'=>'预览式外呼','autocall'=>'自动外呼');
		$success_row = array('Y'=>'成功单录音','N'=>'非成功单录音');
		$i = 0;
		$para_sys = readS();
		foreach($sales_cdrList as &$val){
			$status = $cdr_status_row[$val["disposition"]];
			$val["disposition"] = $status;
			$calltypes = $cdr_calltype_row[$val["calltype"]];
			$val["calltype"] = $calltypes;
			$success_enable = $success_row[$val["success_enable"]];
			$val["success_enable"] = $success_enable;
			
			//wjj--add
			$val["recordtag"] = $tag[$val["recordtag"]];
			
			$val["uniqueid"] = trim($val["uniqueid"]);
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";
			
			$sales_cdrList[$i]["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));
			$val["massPhone"] = $val["src"];
			$val["massPhone2"] = $val["dst"];
			if($username != "admin"){
				if( $para_sys["callrecords_hide"] == "outbound" || $para_sys["callrecords_hide"] == "all" ){
					$val["src"] = substr($val["src"],0,3)."***".substr($val["src"],-4);
				}
			}
			if($val["disposition"] == "转人工已接"){
				if(file_exists($WAVfile)){
					$sales_cdrList[$i]['operating'] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["uniqueid"])."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .trim($val["uniqueid"]) ."&src=" .trim($val["src"]) ."&dst=" .trim($val["dst"]) ."'> 下载 </a>" ;
				}
			}
			
			if($search_type == "csv"){
				$record = $val['calldate'].",".
					$val['src'].",".
					$val['dst'].",".
					$val['dstchannel'].",".
					$val['billsec'].",".
					$val['username'].",".
					$val['disposition'].",".
					$val['calltype'].",".
					$val['recordtag']."\r\n";
				$content .= $record;
			}
			
			$i++;
		}
		//dump($sales_cdrList);die;
		
		$count_sc = $sales_cdr->where($where)->sum("billsec");
		$total_billsec = sprintf("%02d",intval($count_sc/3600)).":".sprintf("%02d",intval(($count_sc%3600)/60)).":".sprintf("%02d",intval((($count_sc%3600)%60)));
		//dump($total_billsec);
		$arrFooter = array(array(
			"dstchannel" => "总时长:",
			"billsec" => $total_billsec
		));
		
		$arr = array(
			"dstchannel" => "总时长:",
			"billsec" => $total_billsec
		);
		if($search_type == "xls"){
			array_push($sales_cdrList,$arrFooter[0]);
			
			$field = array("calldate","src","dst","dstchannel","billsec","workno","disposition","calltype","recordtag");
			$title = array("通话时间","主叫号码","被叫号码","通道","通话时长","接听者","接听状态","通话类型","录音标记");
			$count = count($field);
			$excelTiele = "通话记录".date("Y-m-d");
			
			$this->exportDataFunction($count,$field,$title,$sales_cdrList,$excelTiele);
			die;
		}elseif($search_type == "csv"){
			$content .= ",,,总时长:,".
					$total_billsec.",,,,\r\n";
			$excelTiele = "通话记录".date("Y-m-d");
			
			$filename = iconv("utf-8","gb2312",$excelTiele);
			$this->export_csv($filename,$content);
			die;
		}
		
		$rowsList = count($sales_cdrList) ? $sales_cdrList : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["footer"] = $arrFooter;
		
		echo json_encode($ary);	
	}
	
	
	
	function exportDataFunction($count,$field,$title,$arrData,$excelTiele){
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type:text/html;charset=UTF-8");
		$filename = iconv("utf-8","gb2312",$excelTiele);
        header("Content-Disposition: attachment;filename=$excelTiele.xls ");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();
        $start_row	=	0;

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
			xlsWriteLabel($start_row,$i,utf2gb($title[$i]));
		}
        
        $start_row++;
		foreach($arrData as &$val){
			for($j=0;$j<$count;$j++){
				 xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
			}
			$start_row++;
		}
        xlsEOF();
	}
	
	
	//播放cdr
	function playCDR(){
		checkLogin();
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		//$userfield = str_replace(".wav",".mp3",$_GET['userfield']);//lame把.wav的文件转化成了.mp3文件
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		//http://192.168.1.69/agent.php?m=TasksCallRecords&a=playCDR&uniqueid=1363344674.456
		$AudioURL = "index.php?m=TaskPhoneRecords&a=downloadCDR&clientphone=${clientphone}&uniqueid=${uniqueid}". "&userfield=".$userfield;
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->assign("clientphone",$clientphone);
		$this->display();
	}
	
	//download CDR
	function downloadCDR(){
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		$WAVfile = "/var/spool/asterisk/monitor/" . Date("Y-m",$timestamp). "/" .Date("d",$timestamp)."/".$userfield;
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(".wav",".mp3", basename($WAVfile))."");
        readfile($WAVfile);
	
	}
	
	function exportTaskCdrToExcel2(){
		set_time_limit(0);
		ini_set('memory_limit', '-1');
		$username = $_SESSION["user_info"]["username"];
		$task_id = $_REQUEST['task_id'];
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
		$disposition = $_REQUEST['disposition'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
        $billsec = $_REQUEST['billsec'];
        $success_enable = $_REQUEST['success_enable'];
        $recordtag = $_REQUEST['recordtag'];
        $workno = $_REQUEST['workno'];
		
		$where = "1 ";
        $where .= empty($billsec)?"":" AND billsec >'$billsec'";
        $where .= empty($success_enable)?"":" AND success_enable = '$success_enable'";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";
        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND src ='$src'";
            $where .= empty($dst)?"":" AND dst ='$dst'";
        }else{
            $where .= empty($src)?"":" AND src like '%$src%'";
            $where .= empty($dst)?"":" AND dst like '%$dst%'";
        }
		$where .= empty($disposition)?"":" AND disposition = '$disposition'";
		$where .= empty($workno)?"":" AND workno = '$workno'";
		if($recordtag == "-1"){
			$where .= " AND recordtag is not null AND recordtag !=''";
		}else{
			$where .= empty($recordtag)?"":" AND recordtag = $recordtag";
		}
		
		import('ORG.Util.Page');
		$sales_cdr = new Model("sales_cdr_".$task_id);		
		$arrTaskCdr = $sales_cdr->order("calldate desc")->where($where)->select();
		$content = "通话时间,主叫号码,被叫号码,通道,通话时长,接听者,接听状态,通话类型,录音标记\r\n";
		
		$mark = getMark();
		
		foreach( $arrTaskCdr AS $row ){
			if( $row['disposition']=='ANSWERED'){
				$row['disposition']="已接";
			}else{
				$row['disposition']="未接";
			}
			if( $row['calltype']=='preview'){
				$row['calltype']="预览式外呼";
			}else{
				$row['calltype']="自动外呼";
			}
			$recordtag = $mark[$row["recordtag"]];
			$row["recordtag"] = $recordtag;
			
			$row['billsec'] = sprintf("%02d",intval($row["billsec"]/3600)).":".sprintf("%02d",intval(($row["billsec"]%3600)/60)).":".sprintf("%02d",intval((($row[billsec]%3600)%60)));
			$record = $row['calldate'].",".
					$row['src'].",".
					$row['dst'].",".
					$row['dstchannel'].",".
					$row['billsec'].",".
					$row['username'].",".
					$row['disposition'].",".
					$row['calltype'].",".
					$row['recordtag']."\r\n";
			$content .= $record;
										
		}
		//dump($content);die;
		/*
		$content = iconv("UTF-8","GB2312//IGNORE",$content);
		$d = date("D M j G:i:s T Y");
        header('HTTP/1.1 200 OK');
        header('Date: ' . $d);
        header('Last-Modified: ' . $d);
        header("Content-Type: application/force-download"); 
        header("Content-Length: " . strlen($content));
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-Disposition: attachment;filename=".time().".csv");
        echo $content;
		*/
		$name = "外呼通话记录".date("YmdHis");
		$filename = iconv("utf-8","gb2312",$name);
		$this->export_csv($filename,$content);
	}
	
	
	function export_csv($filename,$data){ 
		$content = iconv("utf-8","gb2312",$data);
		$d = date("D M j G:i:s T Y");
        header('HTTP/1.1 200 OK');
        header('Date: ' . $d);
        header('Last-Modified: ' . $d);
        header("Content-Type: application/force-download"); 
        header("Content-Length: " . strlen($data));
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-Disposition: attachment;filename=".$filename.".csv");
        echo $data;
	} 
	
	
	function exportTaskCdrToExcel(){
		set_time_limit(0);
		ini_set('memory_limit', '-1');
		$username = $_SESSION["user_info"]["username"];
		$task_id = $_REQUEST['task_id'];
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
		$disposition = $_REQUEST['disposition'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
        $billsec = $_REQUEST['billsec'];
        $success_enable = $_REQUEST['success_enable'];
        $recordtag = $_REQUEST['recordtag'];
        $workno = $_REQUEST['workno'];
		
		$where = "1 ";
        $where .= empty($billsec)?"":" AND billsec >'$billsec'";
        $where .= empty($success_enable)?"":" AND success_enable = '$success_enable'";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";
        $where .= empty($disposition)?"":" AND disposition = '$disposition'";
        $where .= empty($workno)?"":" AND workno = '$workno'";
        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND src ='$src'";
            $where .= empty($dst)?"":" AND dst ='$dst'";
        }else{
            $where .= empty($src)?"":" AND src like '%$src%'";
            $where .= empty($dst)?"":" AND dst like '%$dst%'";
        }
		if($recordtag == "-1"){
			$where .= " AND recordtag is not null AND recordtag !=''";
		}else{
			$where .= empty($recordtag)?"":" AND recordtag = $recordtag";
		}
		
		if($username != "admin"){
			$where .= " AND workno = '$username'";
		}
		$sales_cdr = new Model("sales_cdr_".$task_id);
		
		$sales_cdrList = $sales_cdr->order("calldate desc")->where($where)->select();	
		//echo $sales_cdr->getLastSql();

		$cdr_status_row = array('ANSWERED'=>'转人工已接','AUTOCALL'=>'外呼失败','NO ANSWER'=>'转人工未接','PLAYIVR'=>"未转接");
		$cdr_calltype_row = array('preview'=>'预览式外呼','autocall'=>'自动外呼');
		$mark = getMark();
		$i = 0;
		foreach($sales_cdrList as &$val){
			$status = $cdr_status_row[$val["disposition"]];
			$val["disposition"] = $status;
			$calltypes = $cdr_calltype_row[$val["calltype"]];
			$val["calltype"] = $calltypes;
			$recordtag = $mark[$val["recordtag"]];
			$val["recordtag"] = $recordtag;
			
			$sales_cdrList[$i]["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));
			$i++;
		}
		//dump($sales_cdrList);die;
		
		vendor("PHPExcel176.PHPExcel");
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB'); 
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings); 
		$objPHPExcel = new PHPExcel();

		// Set properties  
		$objPHPExcel->getProperties()->setCreator("ctos")  
			->setLastModifiedBy("ctos")  
			->setTitle("Office 2007 XLSX Test Document")  
			->setSubject("Office 2007 XLSX Test Document")  
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")  
			->setKeywords("office 2007 openxml php")  
			->setCategory("Test result file");  
  
		//设置行高度  
		//$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(22); 
		//$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);  
  
		//设置字体大小加粗
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);  
		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);  
		
		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);  
		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);  
  
		//设置水平居中  
		for($j='A';$j<='I';$j++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(20);  //设置单元格（列）的宽度
			$objPHPExcel->getActiveSheet()->getStyle($j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  
		}
  
		//合并单元格
		//$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');  
  
		//设置表 标题内容 通话时间,主叫号码,被叫号码,通道,通话时长,接听者,接听状态,通话类型
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue('A1', '通话时间')  
			->setCellValue('B1', '主叫号码')  
			->setCellValue('C1', '被叫号码')  
			->setCellValue('D1', '通道')  
			->setCellValue('E1', '通话时长')  
			->setCellValue('F1', '接听者')
			->setCellValue('G1', '接听状态') 
			->setCellValue('H1', '通话类型')
			->setCellValue('I1', '录音标记');  
		$i = 2; 
		foreach($sales_cdrList as &$val){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $val['calldate']); 
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $val['src']); 
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $val['dst']); 
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val['dstchannel']); 
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val['billsec']); 
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val['workno']); 
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['disposition']); 
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val['calltype']); 
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val['recordtag']); 
			$i++;
		}
		// Rename sheet  
		//$objPHPExcel->getActiveSheet()->setTitle('通话记录');  
  
  
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet  
		$objPHPExcel->setActiveSheetIndex(0);  
  
		$name = "通话记录";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)  
		ob_end_clean();   //清除缓冲区,避免乱码
		//header("Content-Type:text/html; charset=utf-8");
		header('Content-Type: application/vnd.ms-excel');  
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"'); 
		header('Cache-Control: max-age=0');  
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		$objWriter->save('php://output'); 
	}
	
	
	//pbx通话记录---跟外呼记录同步
	function pbxRecordesList(){
		$username = $_SESSION['user_info']['username'];
		getArrCache();//必须包含缓存文件
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $billsec = $_REQUEST['billsec'];
        $disposition = $_REQUEST['disposition'];
        $agent_type = $_REQUEST['agent_type'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		
        $where = "1 ";
        $where .= empty($billsec)?"":" AND billsec >'$billsec'";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";

        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND src ='$src'";
            $where .= empty($dst)?"":" AND dst ='$dst'";
        }else{
            $where .= empty($src)?"":" AND src like '%$src%'";
            $where .= empty($dst)?"":" AND dst like '%$dst%'";
			
        }
		if($username != 'admin'){
			if($agent_type && $agent_type == "roles"){
				$arrDep = $this->getDepTreeArray();
				$d_id = $_SESSION["user_info"]["d_id"];
				$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
				$dept_ids = rtrim($deptst,",");
				$where .= " AND dept_id IN ($dept_ids)";
			}else{
				$where .= " AND workno ='$username'";
			}
		}
		
		if(! empty($disposition) ){
			if($disposition == "ANSWERED"){
				$where .= " AND disposition = 'ANSWERED'";
			}else{
				$where .= " AND disposition != 'ANSWERED'";
			}
		}
		$pbxcdr = new Model('asteriskcdrdb.Cdr');
		import('ORG.Util.Page');
		$count = $pbxcdr->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		} 
		$page = new Page($count,$page_rows);
		
		$pbxCallData = $pbxcdr->field("calldate,src,dst,outnum,dstchannel,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield")->where($where)->order("calldate desc")->limit($page->firstRow.",".$page->listRows)->select();
        
		//echo $pbxcdr->getLastSql();die;
		$in_answer = "<img src='Agent/Tpl/public/images/calltype/IN_ANSWER.png'>";
		$out_answer = "<img src='Agent/Tpl/public/images/calltype/OUT_ANSWER.png'>";
		$cdr_status_row = array('ANSWERED'=>'已接听','NO ANSWER'=>'未接听');
		$cdr_calltype = array('IN'=>$in_answer,'OUT'=>$out_answer);
		$i = 0;
		foreach($pbxCallData as &$val){
			$status = $cdr_status_row[$val["disposition"]];
			$val["disposition"] = $status;
			$Incalltype = $cdr_calltype[$val["calltype"]];
			$val["calltype"] = $Incalltype;
			
			$val['username'] = $arrCacheWorkNo[$val['workno']]['cn_name'];
			$val['dept_name'] = $arrCacheDept[$val['dept_id']]['dept_name'];
			$val['dept_pname'] = $arrCacheDept[$val['dept_id']]['dept_pname'];
			
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";
			
			if($val["disposition"] == "已接听"  && !empty($val["uniqueid"]) ){
				if(file_exists($WAVfile)){
					$pbxCallData[$i]['operating'] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["uniqueid"])."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .trim($val["uniqueid"])."'> 下载 </a>" ;
				}
			}
			$pbxCallData[$i]["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));
			$i++;
		}
		//dump($pbxCallData);die;
		$rowsList = count($pbxCallData) ? $pbxCallData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		echo json_encode($ary);	
	}
	
	
	function testTask(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$sales_task = M('sales_task');
		$arrTaskId = $sales_task->select();
		$mod = M();
		
		
		for($i=21;$i<26;$i++){
		$optDate = "2014-05-0".$i;
		foreach($arrTaskId as $vm){
			$task_table = "sales_cdr_" .$vm["id"]; //任务表
			if($vm["calltype"] == "autocall"){
				$sql_call = "
				REPLACE INTO bgcrm.agent_report(calldate,workno,extension,calltype,callin_times,callin_billsec_total)(
				SELECT DATE(calldate) AS calldate,workno,dst AS extension,'IN',COUNT(*) AS callin_times,SUM(billsec) AS callin_billsec_total
				FROM $task_table
				WHERE dst IN (SELECT extension FROM bgcrm.users WHERE LENGTH(extension)>2 AND LENGTH(extension)<6) AND disposition='ANSWERED' AND dst IS NOT NULL
				GROUP BY DATE(calldate),workno
				HAVING workno IS NOT NULL AND DATE(calldate)='$optDate'
				ORDER BY calldate ASC,workno ASC
				)";
			}else{
				$sql_call = "
				REPLACE INTO bgcrm.agent_report(calldate,workno,extension,calltype,callout_times,callout_billsec_total)(
				SELECT DATE(calldate) AS calldate,workno,dst AS extension,'OUT',COUNT(*) AS callout_times,SUM(billsec) AS callout_billsec_total
				FROM $task_table
				WHERE dst IN (SELECT extension FROM bgcrm.users WHERE LENGTH(extension)>2 AND LENGTH(extension)<6) AND disposition='ANSWERED' AND dst IS NOT NULL
				GROUP BY DATE(calldate),workno
				HAVING workno IS NOT NULL AND DATE(calldate)='$optDate'
				ORDER BY calldate ASC,workno ASC
				)";
			}
			$mod->execute($sql_call);
		}
		}
	}
	
	function downloadAllRecording(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$task_id = $_REQUEST["task_id"];
		$success_enable = $_REQUEST["success_enable"];
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $billsec = $_REQUEST['billsec'];
		$disposition = $_REQUEST['disposition'];
		$recordtag = $_REQUEST['recordtag'];
		$workno = $_REQUEST['workno'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		
		$where = "uniqueid IS NOT NULL AND uniqueid !='' ";
        $where .= empty($billsec)?"":" AND billsec >'$billsec'";
        $where .= empty($success_enable)? "" : " AND success_enable = '$success_enable'";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";
        $where .= empty($disposition)?"":" AND disposition = '$disposition'";
        $where .= empty($recordtag)?"":" AND recordtag = '$recordtag'";
        $where .= empty($workno)?"":" AND workno = '$workno'";
        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND src ='$src'";
            $where .= empty($dst)?"":" AND dst ='$dst'";
        }else{
            $where .= empty($src)?"":" AND src like '%$src%'";
            $where .= empty($dst)?"":" AND dst like '%$dst%'";
        }
		
		
		$sales_cdr = new Model("sales_cdr_".$task_id);
		//$arrData = $sales_cdr->where("success_enable='Y' AND uniqueid IS NOT NULL AND uniqueid !=''")->select();
		$arrData = $sales_cdr->where($where)->select();
		
		$monitorDir = '/var/spool/asterisk/monitor/successFile/';
		$this->mkdirs($monitorDir);
		foreach($arrData as &$val){
			$val["uniqueid"] = trim($val["uniqueid"]);
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";
			if(file_exists($WAVfile)){
				$filename = $val["dst"]."-".$val["src"]."-".date('Ymdhis',$val["uniqueid"]).".WAV";
				$v = $dirPath ."/".$val["uniqueid"].".WAV";
				$cmd = "cp ".$v." ".$monitorDir.$filename;
				$res = exec($cmd);		
			}
		}
		
		
		/*
		foreach($WAVfile as $val){
			$filename = ""
			$cmd = "cp ".$val." ".$monitorDir;
			$res = exec($cmd);
		}*/
		
		$cmds = "cd $monitorDir;tar -zcf successFile.tar.gz *";
		exec($cmds);
		$filePath = $monitorDir."successFile.tar.gz";
		$this->downloadFile($filePath);
		
	}
	
	
	function downloadFile($filePath){
		//dump($filePath);die;
		
		if(!file_exists($filePath)){
			echo "<script>alert('File $filePath is not exist!');</script>";
		}
        header('HTTP/1.1 200 OK');
        //header('Accept-Ranges: bytes');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($filePath)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($filePath))."");
        readfile($filePath);
		$cmd = " cd /var/spool/asterisk/monitor/; rm -rf successFile";
		exec($cmd);
	}
	
	
	function downloadAllRecordingBak(){
		$task_id = $_REQUEST["task_id"];
		$success_enable = $_REQUEST["success_enable"];
		$sales_cdr = new Model("sales_cdr_".$task_id);
		$arrData = $sales_cdr->where("success_enable='Y' AND uniqueid IS NOT NULL AND uniqueid !=''")->select();
		foreach($arrData as &$val){
			$val["uniqueid"] = trim($val["uniqueid"]);
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile[] = $dirPath ."/".$val["uniqueid"].".WAV";
		}
		
		$monitorDir = '/var/spool/asterisk/monitor/successFile/';
		$this->mkdirs($monitorDir);
		foreach($WAVfile as $val){
			$cmd = "cp ".$val." ".$monitorDir;
			$res = exec($cmd);
		}
		
		$cmds = "cd $monitorDir;tar -zcf successFile.tar.gz *";
		exec($cmds);
		$filePath = $monitorDir."successFile.tar.gz";
		$this->downloadFile($filePath);
		
	}
	
	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			} 
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}
	
}

?>
