<?php
class DocumentCenterAction extends Action{
	//查看文档
	function viewDocuments(){
		checkLogin();
		$web_type = empty($_REQUEST["web_type"]) ? "my" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);
		$this->display();
	}

	function documentsTree(){
		$faq=new Model("faq_type");
		$types = $faq->field("name as text,id,pid")->select();
        $arrTree = $this->getTree($types,0);
		$strJSON = json_encode($arrTree);
		echo ($strJSON);
	}


    function getTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
				$v['children'] = $this->getTree($data, $v['id']);
				if ( empty($v["children"])  )  unset($v['children']) ;
				if ( empty($v["children"]) && $v['state'] =='closed')  $v['state'] =  'open';
				$tree[] = $v;
            }
        }
        return $tree;
    }


	function documentsList(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$web_type = $_REQUEST["web_type"];
		$type_id = $_REQUEST["type_id"];
		$keyword = $_REQUEST["keyword"];

		$where = "1 ";
		$where .= empty($type_id) ? "" : " AND type_id = '$type_id'";
		$where .= empty($keyword) ? "" : " AND (title like '%$keyword%' or content like '%$keyword%') ";
		$arrAdmin = getAdministratorNum();
		if( !in_array($username,$arrAdmin) ){
			if($web_type == "my"){
				$where .= " AND (create_user = '$username' OR  find_in_set('$username',sharing_personnel) OR  find_in_set('$d_id',shared_department))";
			}elseif($web_type == "collection"){
				//收藏
				$where .= " AND find_in_set('$username',collection_user)";
			}elseif($web_type == "share"){
				//分享
				$where .= " AND (find_in_set('$username',sharing_personnel) OR  find_in_set('$d_id',shared_department))";
			}elseif($web_type == "all"){

			}
		}

		$mod = M("faq_content");
		$count = $mod->where($where)->count();
		$arrData = $mod->order("createtime desc")->where($where)->select();

		$strcolor = "<span style=\'color:#000;background:#FBEC88;font-weight:bold;padding:3px 5px;\'>".$keyword."</span>";
		foreach($arrData as &$val){
			$val["content"] = str_ireplace($keyword,$strcolor,$val['content']);
			$val["title"] = str_ireplace($keyword,$strcolor,$val['title']);

			$val["createtime"] = date("Y-m-d",strtotime($val["createtime"]));
		}

		$this->assign("count",$count);
		$this->assign("arrData",$arrData);
		$this->assign("keyword",$keyword);


		$this->display();
	}

	function documentContentView(){
		checkLogin();
		$id = $_GET['id'];
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$faq = new Model("faq_content");
		if($id){
			$this->assign("id",$id);
			$faqList = $faq->where("id = $id")->find();
			//$faqList["edit_content"] = strip_tags($faqList["content"]);
			$faqList['cn_name'] = $cnName[$faqList["create_user"]];
			//dump($faqList);die;
			$faqList["file_path_name"] = $faqList["file_path"].$faqList["faq_file"];
			$arrF = explode(".",$faqList["faq_file"]);
			$filename = $arrF[0];
			$suffix = $arrF[1];
			$suffix_old = $suffix;
			$arrPdf = array("doc","docx","xls","xlsx","ppt","pptx");
			if($suffix == "txt"){
				$faqList["file_content"] = file_get_contents($faqList["file_path_name"]);
				$faqList["file_content"] = str_replace("\r\n","<br/>",$faqList["file_content"]);
				if( mb_detect_encoding($faqList["file_content"],"UTF-8, ISO-8859-1, GBK") != "UTF-8" ){
					$faqList["file_content"] = gb2utf($faqList["file_content"]);
				}

			}else{
				$faqList["file_content"] = "";
			}

			if( in_array($suffix,$arrPdf) ){
				//$suffix = "pdf";
				$suffix = "word";
				//$faqList["file_path_name"] = $faqList["file_path"].$filename.".pdf";
			}

			$browser = browserType();
			if( $browser =="fire" ){
				$keyword = $_GET['keyword'];
			}
			if( $browser == "ie360" ){
				$keyword2 = $_GET['keyword'];
				$keyword = gb2utf($keyword2);
			}

			$strcolor = "<span class='faqcolor'>".$keyword."</span>";
			$faqList["content"] = str_ireplace($keyword,$strcolor,$faqList['content']);
			$faqList["title"] = str_ireplace($keyword,$strcolor,$faqList['title']);


			if(file_exists($faqList["file_path_name"])){
				$this->assign("annex","Y");
			}else{
				$this->assign("annex","N");
			}

			//视频
			$arrVideo = array("mp4","webm","ogv");
			if( in_array($suffix,$arrVideo) ){
				$suffix = "video";
			}
			//音频
			$arrAudio = array("mp3","wav");
			if( in_array($suffix,$arrAudio) ){
				$suffix = "audio";
			}

			//图片
			$arrPicture = array('jpg', 'gif', 'png', 'jpeg');
			if( in_array($suffix,$arrPicture) ){
				$suffix = "picture";
			}

			if($suffix_old == "mp4"){
				$media_type =  "m4v";
			}elseif($suffix_old == "webm"){
				$media_type =  "webmv";
			}elseif($suffix_old == "ogv"){
				$media_type =  "ogv";
			}elseif($suffix_old == "mp3"){
				$media_type =  "mp3";
			}elseif($suffix_old == "wav"){
				$media_type =  "wav";
			}else{
				$media_type =  "";
			}

			if(!$faqList["collection_num"]){
				$faqList["collection_num"] = "0";
			}

			if(!$faqList["review_num"]){
				$faqList["review_num"] = "0";
			}

			if(!$faqList["thumb_up_num"]){
				$faqList["thumb_up_num"] = "0";
			}

			if(!$faqList["annex_num"]){
				$faqList["annex_num"] = "0";
			}

			if(!$faqList["browse_num"]){
				$faqList["browse_num"] = "0";
			}

			$this->assign("faqList",$faqList);
			$this->assign("suffix",$suffix);
			$this->assign("suffix_old",$suffix_old);
			$this->assign("media_type",$media_type);
			$CTI_IP = $_SERVER["SERVER_ADDR"];
			//$CTI_IP = "lch.cc";
			$this->assign("CTI_IP",$CTI_IP);


			//dump($arrF);
			//dump($suffix);
			//dump($faqList);die;
			$click_num = $faqList["click_num"]+1;
			$faq->where("id = $id")->save(array("click_num"=>$click_num));
		}
		//dump($faqList);die;

		//浏览
		$browse_num = $faqList["browse_num"]+1;
		$faq->where("id = $id")->save(array("browse_num"=>$browse_num));

		$username = $_SESSION["user_info"]["username"];
		$faq_operating_record = M("faq_operating_record");
		$collection_num = $faq_operating_record->where("faq_id = '$id' AND operation_type = '1' AND create_user = '$username'")->count();   //收藏
		if($collection_num > 0){
			$this->assign("collection_num","Y");
		}else{
			$this->assign("collection_num","N");
		}
		$thumb_up_num = $faq_operating_record->where("faq_id = '$id' AND operation_type = '2' AND create_user = '$username'")->count();  //点赞
		if($thumb_up_num > 0){
			$this->assign("thumb_up_num","Y");
		}else{
			$this->assign("thumb_up_num","N");
		}

		//评论
		$arrReviewData = $faq_operating_record->order("create_time desc")->where("faq_id = '$id' AND operation_type = '3'")->select();
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrReviewData as &$val){
			$val['cn_name'] = $cnName[$val["create_user"]];
		}
		$arrReview = $this->getReviewTree($arrReviewData,"0","pid","id");
		$this->assign("arrReview",$arrReview);
		//dump($arrReview);die;

		//分享
		if($username == $faqList["create_user"]){
			$this->assign("operation","Y");
		}else{
			$this->assign("operation","N");
		}
		$users = M("users");
		$arrU = $users->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$department = M("department");
		$arrDept = $department->select();
		$this->assign("arrDept",$arrDept);

		//分配增删改的权限
		$menuname = "My Document";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);


		$this->display();
	}

	/*
	$data： 数组
	$pId： 顶级父id的值【一般为0】
	$field_pid：父id字段【如：d_pid】
	$field_id： id字段【如d_id】
	*/
	function getReviewTree($data, $pId,$field_pid,$field_id) {
		$tree = '';
		foreach($data as $k =>$v) {
			if($v[$field_pid] == $pId)    {
				$v['children'] = $this->getReviewTree($data, $v[$field_id],$field_pid,$field_id);
				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				$tree[] = $v;     //unset($data[$k]);
			}
		}
		return $tree;
	}

	function saveCollection(){
		$username = $_SESSION["user_info"]["username"];
		$id = $_REQUEST["id"];
		$operation_type = $_REQUEST["operation_type"];
		$mod = M("faq_operating_record");
		$arrData = array(
			"create_time"=>date("Y-m-d H:i:s"),
			"create_user"=>$username,
			"faq_id"=>$id,
			"operation_type"=>$operation_type,
		);
		if($operation_type == "1"){
			$msg = '收藏成功！';
		}elseif($operation_type == "2"){
			$msg = '点赞成功！';
		}elseif($operation_type == "3"){
			$msg = '评论成功！';
			$arrData["comment_content"] = $_REQUEST["comment_content"];
		}else{
			$msg = '...';
		}
		$result = $mod->data($arrData)->add();
		if ($result){
			$arrF = $mod->where("faq_id = '$id' AND operation_type = '$operation_type'")->select();
			$count = $mod->where("faq_id = '$id' AND operation_type = '$operation_type'")->count();
			$arrT = array();
			foreach($arrF as $val){
				$arrT[] = $val["create_user"];
			}
			$arrT2 = array_unique($arrT);
			$collection_user = implode(",",$arrT2);
			$faq_content = M("faq_content");
			if($operation_type == "1"){
				$arr = array(
					"collection_user"=>$collection_user,
					"collection_num"=>$count,
				);
			}elseif($operation_type == "2"){
				$arr = array(
					"thumb_up_user"=>$collection_user,
					"thumb_up_num"=>$count,
				);
			}elseif($operation_type == "3"){
				$arr = array(
					"review_user"=>$collection_user,
					"review_num"=>$count,
				);
			}else{
				$msg = '...';
			}
			$faq_content->data($arr)->where("id = '$id'")->save();
			echo json_encode(array('success'=>true,'msg'=>$msg));
		} else {
			echo json_encode(array('msg'=>'收藏失败！'));
		}
	}

	function saveReview(){
		$username = $_SESSION["user_info"]["username"];
		$id = $_REQUEST["id"];
		$pid = $_REQUEST["pid"];
		$faq_id = $_REQUEST["faq_id"];
		$comment_content = $_REQUEST["comment_content"];
		$operation_type = $_REQUEST["operation_type"];

		$mod = M("faq_operating_record");
		$arrData = array(
			"create_time"=>date("Y-m-d H:i:s"),
			"create_user"=>$username,
			"faq_id"=>$faq_id,
			"pid"=>$id,
			"operation_type"=>$operation_type,
			"comment_content"=>$comment_content,
		);
		$result = $mod->data($arrData)->add();
		if ($result){
			$arrF = $mod->where("faq_id = '$faq_id' AND operation_type = '$operation_type'")->select();
			$count = $mod->where("faq_id = '$faq_id' AND operation_type = '$operation_type'")->count();
			$arrT = array();
			foreach($arrF as $val){
				$arrT[] = $val["create_user"];
			}
			$arrT2 = array_unique($arrT);
			$collection_user = implode(",",$arrT2);
			$faq_content = M("faq_content");
			if($operation_type == "1"){
				$arr = array(
					"collection_user"=>$collection_user,
					"collection_num"=>$count,
				);
			}elseif($operation_type == "2"){
				$arr = array(
					"thumb_up_user"=>$collection_user,
					"thumb_up_num"=>$count,
				);
			}elseif($operation_type == "3"){
				$arr = array(
					"review_user"=>$collection_user,
					"review_num"=>$count,
				);
				$msg = '回复成功！';
			}else{
				$msg = '...';
			}
			$faq_content->data($arr)->where("id = '$faq_id'")->save();
			//$sql = $faq_content->getLastSql();
			echo json_encode(array('success'=>true,'msg'=>$msg));
		} else {
			echo json_encode(array('msg'=>'收藏失败！'));
		}
	}

	function saveAnnex(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$file_path = "include/data/faq/";
		mkdirs($file_path);
		$upload->savePath= $file_path;  //上传路径

		$upload->saveRule="uniqid";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖

		$username = $_SESSION['user_info']['username'];
		$faq_id = $_REQUEST["faq_id"];
		$faq_annex=new Model('faq_annex');

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData = array(
				"createtime" =>date("Y-m-d H:i:s"),
				"create_user" =>$username,
				"faq_id" =>$faq_id,
				"file_path_name" =>$info[0]["savepath"].$info[0]["savename"],
				"file_name" =>$info[0]["name"],
			);
			$result = $faq_annex->data($arrData)->add();
			if ($result){
				$faq_content = M("faq_content");
				$count = $faq_annex->where("faq_id = '$faq_id'")->count();
				$arr = array(
					"annex_num"=>$count,
				);
				$faq_content->data($arr)->where("id = '$faq_id'")->save();
				echo json_encode(array('success'=>true,'msg'=>'附件上传成功！'));
			} else {
				echo json_encode(array('msg'=>'附件上传失败！'));
			}
		}

	}

	function annexData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$faq_id = $_REQUEST["faq_id"];
		$where = "1 ";
		$where .= empty($faq_id) ? "" : " AND faq_id = '$faq_id'";

		$mod = M("faq_annex");
		$count = $mod->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $mod->getLastSql

		$arrPdf = array("doc","docx","xls","xlsx","ppt","pptx");
		$arrVideo = array("mp4","webm","ogv");
		$arrAudio = array("mp3","wav");
		$arrPicture = array('jpg', 'gif', 'png', 'jpeg');
		foreach($arrData as &$val){
			$val["suffix"] = array_pop(explode(".",$val["file_path_name"]));
			$val["suffix_old"] = array_pop(explode(".",$val["file_path_name"]));
			if($val["suffix"] == "txt"){
				$val["file_content"] = file_get_contents($val["file_path_name"]);
				$val["file_content"] = str_replace("\r\n","<br/>",$val["file_content"]);
				if( mb_detect_encoding($val["file_content"],"UTF-8, ISO-8859-1, GBK") != "UTF-8" ){
					$val["file_content"] = gb2utf($val["file_content"]);
				}

			}else{
				$val["file_content"] = "";
			}

			if( in_array($val["suffix"],$arrPdf) ){
				$val["suffix"] = "word";
			}

			//视频
			if( in_array($val["suffix"],$arrVideo) ){
				$val["suffix"] = "video";
			}
			//音频
			if( in_array($val["suffix"],$arrAudio) ){
				$val["suffix"] = "audio";
			}

			//图片
			if( in_array($val["suffix"],$arrPicture) ){
				$val["suffix"] = "picture";
			}

			if($val["suffix_old"] == "mp4"){
				$val["media_type"] =  "m4v";
			}elseif($val["suffix_old"] == "webm"){
				$val["media_type"] =  "webmv";
			}elseif($val["suffix_old"] == "ogv"){
				$val["media_type"] =  "ogv";
			}elseif($val["suffix_old"] == "mp3"){
				$val["media_type"] =  "mp3";
			}elseif($val["suffix_old"] == "wav"){
				$val["media_type"] =  "wav";
			}else{
				$val["media_type"] =  "";
			}

			$val["operating"] = "<a href='index.php?m=DocumentCenter&a=DownloadAnnex&filename=".$val['file_path_name']."'>下载</a>  &nbsp;&nbsp;&nbsp; <a href='javascript:void(0);' onclick=\"deleteAnnex("."'".$val["id"]."'".")\" >删除</a>  &nbsp;&nbsp;&nbsp; <a href='javascript:void(0);' onclick=\"viewAnnex("."'".$val["suffix"]."','".$val["suffix_old"]."','".$val["media_type"]."','".$val["file_path_name"]."','".$val["file_content"]."'".")\" >查看</a>";
		}
		//dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	//下载文件
	function DownloadAnnex(){
		$realfile = $_REQUEST["filename"];
		//echo $realfile;die;
		if(!file_exists($realfile)){
			echo goback("没有找到 $realfile 文件");
		}
        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}

	function deleteAnnex(){
		$id = $_REQUEST["id"];
		$mod = M("faq_annex");
		$arrData = $mod->where("id = '$id'")->find();
		$result = $mod->where("id in ($id)")->delete();
		if ($result){
			unlink($arrData["file_path_name"]);
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function saveShare(){
		$id = $_REQUEST['id'];
		$mod = M("faq_content");
		$arrData = array(
			'sharing_personnel'=>$_REQUEST['sharing_personnel'],
			'shared_department'=>$_REQUEST['shared_department'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"分享成功！"));
		} else {
			echo json_encode(array('msg'=>'分享失败！'));
		}
	}

	function saveDocument(){
		$id = $_REQUEST['id'];
		$title = $_REQUEST['title'];
		$content = $_REQUEST['content'];
		$mod = M("faq_content");
		$arrData = array(
			'title'=>$_REQUEST['title'],
			'content'=>$_REQUEST['content'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"分享成功！"));
		} else {
			echo json_encode(array('msg'=>'分享失败！'));
		}
	}

	function addDocument(){
		checkLogin();
		$this->display();
	}

	function deleteDocument(){
		$id = $_REQUEST["id"];
		$mod = M("faq_content");
		$faq_annex = M("faq_annex");
		$arrData = $faq_annex->where("faq_id = '$id'")->select();
		$result = $mod->where("id in ($id)")->delete();
		$res = $faq_annex->where("faq_id in ($id)")->delete();
		if ($result){
			if($arrData){
				foreach($arrData as $val){
					unlink($val["file_path_name"]);
				}
			}
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}
}

?>
