<?php
class FaqContentAction extends Action{
	function faqContentList(){
		checkLogin();
		$users = new Model("users");
		$ulist = $users->select();
		$this->assign('ulist',$ulist);

		//分配增删改的权限
		$menuname = "Faq Content";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}
		$this->assign("priv",$priv);

		$users = M("users");
		$arrU = $users->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$department = M("department");
		$arrDept = $department->select();
		$this->assign("arrDept",$arrDept);


		$this->display();
	}

	function faqContentData(){
		$title = $_REQUEST['title'];
		$content = $_REQUEST['content'];
		$faq_type = $_REQUEST['faq_type'];

		$arrFaq = $this->getFaqTreeArray();
		$deptst = $this->getMeAndSubFaqID($arrFaq,$faq_type);
		$faq_id = rtrim($deptst,",");
		//dump($faq_id);die;
		$where = "1 ";
		$where .= empty($title)?"":" AND fc.title like '%$title%'";
		$where .= empty($content)?"":" AND fc.content like '%$content%'";
		$where .= empty($faq_type)?"":" AND ft.id in ($faq_id)";

		$faq=new Model("faq_content");
		$fields = "ft.name,ft.id as id,ft.pid as pid,fc.title,fc.type_id,fc.content,fc.faq_file,fc.create_user,fc.createtime,fc.id as fc_id,fc.share_enabled,fc.sharing_personnel,fc.shared_department,fc.collection_user,fc.review_user,fc.thumb_up_user,fc.browse_num,fc.collection_num,fc.review_num,fc.thumb_up_num,fc.annex_num";
		$count=$faq->table("faq_content fc")->order("fc.createtime desc")->field("ft.name,ft.id as id,ft.pid as pid,fc.title,fc.type_id,fc.content,fc.faq_file,fc.create_user,fc.createtime,fc.id as fc_id")->join("faq_type ft on (fc.type_id = ft.id)")->where($where)->count();

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$faqList = $faq->table("faq_content fc")->order("fc.createtime desc")->field($fields)->join("faq_type ft on (fc.type_id = ft.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		foreach($faqList as &$val){
			$val["operating"] = "<a href='#' onclick='viewFAQContent(" .$val['fc_id'] .")'>查看内容</a>" ;
			if($val["faq_file"]){
				$val["operating"] .= " | " ."<a href='index.php?m=FaqContent&a=DownloadFj&name=".$val['faq_file']."'>下载附件</a>";
			}
		}

		$rowsList = count($faqList) ? $faqList : false;
		$arrFaqCon["total"] = $count;
		$arrFaqCon["rows"] = $rowsList;

		echo json_encode($arrFaqCon);
	}

	//下载文件
	function DownloadFj(){
		$file = $_GET['name'];
		$path = "include/data/faq/";
		$realfile = $path.$file;
		if(!file_exists($realfile)){
			echo goback("没有找到 $realfile 文件","index.php?m=FaqContent&a=faqContentList");
		}
        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}

	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}

	function insertFaqContent(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$file_path = "include/data/faq/";
		$this->mkdirs($file_path);
		$upload->savePath= $file_path;  //上传路径

		$upload->saveRule="uniqid";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		//$upload->allowExts=array('al');     //准许上传的文件后缀

		$username = $_SESSION['user_info']['username'];
		$content = str_replace("\\","",$_POST["content"]);
		$faq=new Model('faq_content');

		$sharing_personnel = implode(",",$_REQUEST["sharing_personnel"]);
		$shared_department = implode(",",$_REQUEST["shared_department"]);
		//dump($shared_department);die;

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = array(
					"createtime" =>date("Y-m-d H:i:s"),
					//"create_user" =>$_POST["create_user"],
					"create_user" =>$username,
					"title" =>$_POST["title"],
					"content" =>$content,
					"type_id" =>$_POST["type_id"],
					"share_enabled" =>$_POST["share_enabled"],
					"sharing_personnel" =>$sharing_personnel,
					"shared_department" =>$shared_department,
				);
				$result = $faq->data($arrData)->add();
				if ($result){
					echo json_encode(array('success'=>true,'msg'=>'faq内容添加成功！'));
				} else {
					echo json_encode(array('msg'=>'faq内容添加失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			//$content = $content."<br/><br/><br/>点击<a href='index.php?m=FaqContent&a=DownloadFj&name=".$info[0]["savename"]."'>下载附件</a>";
			//dump(content);die;
			$arrData = array(
				"createtime" =>date("Y-m-d H:i:s"),
				//"create_user" =>$_POST["create_user"],
				"create_user" =>$username,
				"title" =>$_POST["title"],
				"content" =>$content,
				"type_id" =>$_POST["type_id"],
				"faq_file" =>$info[0]["savename"],
				"file_path" =>$info[0]["savepath"],
				"share_enabled" =>$_POST["share_enabled"],
				"sharing_personnel" =>$sharing_personnel,
				"shared_department" =>$shared_department,
			);
			$result = $faq->data($arrData)->add();
			if ($result){
				$faq_annex=new Model('faq_annex');
				$arrDataA = array(
					"createtime" =>date("Y-m-d H:i:s"),
					"create_user" =>$username,
					"faq_id" =>$result,
					"file_path_name" =>$info[0]["savepath"].$info[0]["savename"],
					"file_name" =>$info[0]["name"],
				);
				$res = $faq_annex->data($arrDataA)->add();
				if($res){
					$count = $faq_annex->where("faq_id = '$result'")->count();
					$arr = array(
						"annex_num"=>$count,
					);
					$faq->data($arr)->where("id = '$result'")->save();
				}
				echo json_encode(array('success'=>true,'msg'=>'faq内容添加成功！'));
			} else {
				echo json_encode(array('msg'=>'faq内容添加失败！'));
			}
		}



	}

	function updateFaqContent(){
		//dump($_REQUEST);die;
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$file_path = "include/data/faq/";
		$this->mkdirs($file_path);
		$upload->savePath= $file_path;  //上传路径

		$upload->saveRule="uniqid";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		//$upload->allowExts=array('al');     //准许上传的文件后缀

		$id = $_REQUEST["id"];
		$sharing_personnel = implode(",",$_REQUEST["sharing_personnel"]);
		$shared_department = implode(",",$_REQUEST["shared_department"]);
		$content = str_replace("\\","",$_POST["content"]);
		$faq=new Model('faq_content');
		$faqD = $faq->where("id = '$id'")->find();

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = array(
					//"create_user" =>$_POST["create_user"],
					"title" =>$_POST["title"],
					"content" =>$content,
					"type_id" =>$_POST["type_id"],
					"share_enabled" =>$_POST["share_enabled"],
					"sharing_personnel" =>$sharing_personnel,
					"shared_department" =>$shared_department,
				);
				$result = $faq->where("id = $id")->save($arrData);
				if ($result !== false){
					echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
				} else {
					echo json_encode(array('msg'=>'更新失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			/*
			$arrC = explode("点击",$content);
			$downloadFile = array_pop($arrC);
			if( strpos($downloadFile,"DownloadFj") && strpos($downloadFile,"FaqContent")){
				if($faqD['faq_file'] !== $info[0]["savename"]){
					$content = str_replace($faqD['faq_file'],$info[0]["savename"],$content);
				}else{
					$content = $content;
				}
			}else{
				$content = $content."<br/><br/><br/>点击<a href='index.php?m=FaqContent&a=DownloadFj&name=".$info[0]["savename"]."'>下载附件</a>";
			}
			*/
			//dump($content);die;
			$arrData = array(
				//"create_user" =>$_POST["create_user"],
				"title" =>$_POST["title"],
				"content" =>$content,
				"type_id" =>$_POST["type_id"],
				"faq_file" =>$info[0]["savename"],
				"file_path" =>$info[0]["savepath"],
				"share_enabled" =>$_POST["share_enabled"],
				"sharing_personnel" =>$sharing_personnel,
				"shared_department" =>$shared_department,
			);
			$result = $faq->where("id = $id")->save($arrData);
			if ($result !== false){
				$file_path_name = $faqD["file_path"].$faqD["faq_file"];
				unlink($file_path_name);
				$faq_annex=new Model('faq_annex');
				$arrDataA = array(
					"createtime" =>date("Y-m-d H:i:s"),
					"create_user" =>$username,
					"file_path_name" =>$info[0]["savepath"].$info[0]["savename"],
					"file_name" =>$info[0]["name"],
				);
				$res = $faq_annex->data($arrDataA)->where("file_path_name = '$file_path_name' AND faq_id = '$id'")->save();


				echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}

	}

	function deleteFaqContent(){
		$id = $_REQUEST["id"];
		$faq=new Model('faq_content');
		$faqD = $faq->where("id = '$id'")->find();
		$result = $faq->where("id in ($id)")->delete();
		$faq_annex = M("faq_annex");
		$arrData = $faq_annex->where("faq_id = '$id'")->select();
		$result = $mod->where("id in ($id)")->delete();
		if ($result){
			if($arrData){
				foreach($arrData as $val){
					unlink($val["file_path_name"]);
				}
			}
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	/*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getFaqTreeArray(){
        $DepTree = array();//一维数组
        $faq = M('faq_type');
        $arr = $faq->select();
        foreach($arr AS $v){
            $currentId = $v['id'];
            $arrSonId = $faq->field('id')->where("pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['id'],
                "pid" => $v['pid'],
                "name"=> $v['name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }

	//取下级部门
	function getMeAndSubFaqID($arrFaq,$dept_id){
		$arrId = explode(',',$arrFaq[$dept_id]['meAndSonId']);
		//$str =  $arrFaq[$dept_id]['id'] . ",";
		$str = "'" . $arrFaq[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubFaqID($arrFaq,$id);
			}
		}
		return $str;

	}
}

?>
