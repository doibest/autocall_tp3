<?php
class OutboundRoutesAction extends Action{
	function routingList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Outbound Routes";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function routingData(){
		$name = $_REQUEST["name"];

		$where = "1 ";
		$where .= empty($name) ? "" : " AND `name` like '%$name%'";

		$outroutes = new Model("asterisk.outbound_routes");
		$count = $outroutes->where($where)->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$outroutesData = $outroutes->where($where)->limit($page->firstRow.",".$page->listRows)->select();

		$listRows = count($outroutesData) ? $outroutesData : false;
		$arrOutroutes["total"] = $count;
		$arrOutroutes["rows"] = $listRows;

		echo json_encode($arrOutroutes);
	}

	function addRouteout(){
		checkLogin();
		$trunks = new Model("asterisk.trunks");
		$trunksList = $trunks->field("trunkid,name")->select();
		$this->assign("trunksList",$trunksList);
		$this->display();
	}

	function editRouteout(){
		checkLogin();
		$route_id = $_GET["route_id"];
		$outroutes = new Model("asterisk.outbound_routes");
		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$outroutesList = $outroutes->where("route_id = '$route_id'")->find();
		//dump($route_id);die;
		if($outroutesList['outcid_mode']){
			$this->assign("display","checked");
		}
		$this->assign("outroutesList",$outroutesList);

		$trunks = new Model("asterisk.trunks");
		$trunksList = $trunks->field("trunkid,name")->select();
		$this->assign("trunksList",$trunksList);

		$route_patterns = new Model("asterisk.outbound_route_patterns");
		$pattData = $route_patterns->where("route_id = '$route_id'")->select();
		$i = 0;
		foreach($pattData as $v){
			$pattData[$i]["seq"] = $i;
			$i++;
		}
		unset($i);
		//dump($pattData);
		$num = count($pattData);
		//$num = $pattcount+1;
		$this->assign("pattData",$pattData);
		$this->assign("num",$num);

		$route_trunks = new Model("asterisk.outbound_route_trunks");
		$route_trunksData = $outroutes->table(" asterisk.outbound_route_trunks t")->order("t.seq")->join("asterisk.outbound_routes r on t.route_id=r.route_id")->where("t.route_id = '$route_id'")->select();
		$rcount = count($route_trunksData);
		$count = $rcount+1;
		//dump($count);die;
		//echo $outroutes->getLastSql();die;
		$this->assign("route_trunksData",$route_trunksData);
		$this->assign("rcount",$rcount);
		$this->display();
	}
}
?>

