<?php
class WaitmattertypeAction extends Action{
	function waitmatterList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Waitmatter Type";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function waitmatterDataList(){
		$waitmatter = new Model("waitmattertype");
		import('ORG.Util.Page');
		$count = $waitmatter->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$waitmatterData = $waitmatter->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($waitmatterData) ? $waitmatterData : false;
		$arrwaitmatter["total"] = $count;
		$arrwaitmatter["rows"] = $rowsList;

		echo json_encode($arrwaitmatter);
	}

	function insertwaitmatter(){
		$waitmatter = new Model("waitmattertype");
		$arrData = array(
			"waitmattername"=>$_REQUEST["waitmattername"],
		);
		$result = $waitmatter->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function updatewaitmatter(){
		$id = $_REQUEST["id"];
		$waitmatter = new Model("waitmattertype");
		$arrData = array(
			"waitmattername"=>$_REQUEST["waitmattername"],
		);
		$result = $waitmatter->data($arrData)->where("id = $id")->save();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function deletewaitmatter(){
		$id = $_REQUEST["id"];
		$waitmatter = new Model("waitmattertype");
		$result = $waitmatter->where("id = $id")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}
}

?>
