<?php
class WeChatManagementAction extends Action{

	function index(){
		header("Content-Type:text/html; charset=utf-8");

		$arrP = getWeChatSet();

		if($arrP["url_check"] && $arrP["url_check"] == "N"){
			require 'BGCC/Conf/wx.php';
			die;
		}
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		//$arrF = require "BGCC/Conf/chatTest.php";

		$type = $weObj->getRev()->getRevType();
		$arrF = $weObj->getRev()->getRevData();  //获取微信服务器发来的信息
		$MsgId = $arrF["MsgId"];


		$wx_users = new Model("wx_users");
		$openid = $arrF["FromUserName"];

		if($type == "voice"){
			unset($arrF["Recognition"]);
		}elseif($type == "location"){
			if( gettype($arrF["Label"]) == "object"){
				$arrF["Label"] = "地址不详细,位置大概在纬度为： ".$arrF["Location_X"]." 经度为：".$arrF["Location_Y"];
			}
		}elseif($type == "event"){
			if( gettype($arrF["EventKey"]) == "object"){
				$arrF["EventKey"] = "未知事件类型";
			}
			if( $arrF["Event"] == "unsubscribe" ){
				$wx_users->where("openid = '$openid'")->delete();
			}elseif( $arrF["Event"] == "subscribe" ){
				$arrSubInfo = $weObj->getUserInfo($arrF["FromUserName"]);
				$arrSubInfo["subscribe_time"] = date("Y-m-d H:i:s",$arrSubInfo["subscribe_time"]);
				$wx_users->data($arrSubInfo)->add();
			}
		}

		foreach($arrF as $key=>&$val){
			if( gettype($arrF[$key]) == "object" ){
				$val = "对象类型数据！";
			}
		}


		$arrUser = $weObj->getUserInfo($arrF["FromUserName"]);
		$arrF["nickname"] = $arrUser["nickname"];
		$arrF["from_nickname"] = $arrUser["nickname"];
		$arrF["sendType"] = "other";

		if($arrF["MsgType"] == "text"){
			$arr = require "Agent/Tpl/public/js/qqFace/faceImage.php";
			foreach($arr as $key=>$val){
				$arrKey[] = $key;
				$arrVal[] = $val;
			}
			$arrF["Content"] = str_replace($arrKey,$arrVal,$arrF["Content"]);
		}elseif($arrF["MsgType"] == "image"){
			$getFile = $weObj->saveMedia($arrF["MediaId"],"include/data/weChat/images/");
			$arrF["Content"] = '<img src="'.$getFile.'" onload="javascript:DrawImage(this,100,100);" class="bigImage" >';
		}elseif($arrF["MsgType"] == "voice"){
			$getFile = $weObj->saveMedia($arrF["MediaId"],"include/data/weChat/voice/");
			$file = realpath($getFile);
			$name = basename($file);
			$dirpath = dirname($file);
			$arrName = explode(".",$name);
			if($arrName[1] == "amr"){
				//$cmd = "cd ".$dirpath."; ffmpeg -i ".$name." ".$arrName[0].".mp3";
				$cmd = " sudo /usr/local/bin/ffmpeg -i ".$file." ".$dirpath."/".$arrName[0].".mp3";
				$res = exec($cmd,$retstr,$retno);
				$cmd2 = "cd ".$dirpath."; mv ".$name." ".$dirpath."/amr/".$name;
				$res2 = exec($cmd2,$retstr2,$retno2);
				$arrF["Content"] = "include/data/weChat/voice/".$arrName[0].".mp3";
			}else{
				$arrF["Content"] = $getFile;
			}
		}elseif($arrF["MsgType"] == "video"){
			$getFile = $weObj->saveMedia($arrF["MediaId"],"include/data/weChat/video/");
			$arrF["Content"] = $getFile;
		}elseif($arrF["MsgType"] == "location"){
			$arrF["Content"] = $arrF["Label"];
		}elseif($arrF["MsgType"] == "event"){
			$arrF["Content"] = $arrF["EventKey"];
		}
		$arrF["CreateTime"] = date("Y-m-d H:i:s",$arrF["CreateTime"]);

		//$arrN = require "BGCC/Conf/weChatAnswer.php";
		$arrN = getWeChatAnswer();
		$answerU = $arrN[$arrF["FromUserName"]];
		$arrAns = getLoginUser();
		if($answerU){
			//已经登录的坐席
			/*
			if( file_exists("BGCC/Conf/loginUser.php") ){
				$arrAns = require "BGCC/Conf/loginUser.php";
				if( in_array($answerU,$arrAns) ){
					$agentLogin = $answerU;
				}else{
					$rand = array_rand($arrAns);
					$agentLogin = $arrAns[$rand];
				}
			}else{
				$agentLogin = $answerU;
				$arrAns = array($answerU);
			}
			*/
			if($arrAns){
				if($arrAns[$answerU]){
					$where = $answerU;
				}else{
					$rand = array_rand($arrAns);
					$agentLogin = $arrAns[$rand];
				}
			}else{
				$agentLogin = $answerU;
				$arrAns = array($answerU);
			}

		}else{
			//没有分配的微信账户
			/*
			if( file_exists("BGCC/Conf/loginUser.php") ){
				$arrAns = require "BGCC/Conf/loginUser.php";
				$rand = array_rand($arrAns);
				$agentLogin = $arrAns[$rand];
			}else{
				$agentLogin = "admin";
			}
			*/
			if($arrAns){
				$rand = array_rand($arrAns);
				$agentLogin = $arrAns[$rand];
			}else{
				$agentLogin = "admin";
			}
		}
		//dump($agentLogin);die;

		$db_name = "bgcrm";
		//比较空闲的坐席
		if( in_array($answerU,$arrAns) ){
			$answerAgent = $answerU;
		}else{
			if( file_exists("BGCC/Conf/crm/$db_name/weChatRecord.php") ){
				$arrRe = require "BGCC/Conf/crm/$db_name/weChatRecord.php";
				$time = date('Y-m-d H:i:s',strtotime("-".$arrP["minutes"]." minutes"));
				foreach($arrRe as $val){
					if($val["CreateTime"] >= $time){
						$arrMR[] = array(
							"openid" => $val["FromUserName"],
							"CreateTime" => $val["CreateTime"],
							"answer_seats" => $val["answer_seats"],
							"nickname" => $val["nickname"],
						);
					}
				}
				if($arrMR){
					$arrG = $this->groupCountBy($arrMR,"answer_seats");
					asort($arrG);
					foreach($arrG as $key=>$val){
						//判断坐席聊天客户数跟坐席登录
						if( $val <= $arrP["max_chat"] && in_array($key,$arrAns) ){
							$arrIdle[] = $key;
						}
					}
					if( $arrG[$agentLogin] > $arrP["max_chat"] ){
						$rand2 = array_rand($arrIdle);
						$answerAgent = $arrIdle[$rand2];
					}else{
						$answerAgent = $agentLogin;
					}
				}else{
					$answerAgent = $agentLogin;
				}
			}else{
				$answerAgent = $agentLogin;
			}
		}
		//dump($answerAgent);die;

		$arrF["answer_seats"] = $answerAgent;    //应答坐席
		$arrF["MsgId"] = $MsgId;    //应答坐席
		$this->weChatRecordCaChe($arrF);

		//dump($arrF);die;
		$arrF["type"] = "wechat";
		$arrT[0] = $arrF;
		$this->phpPush(json_encode($arrT),$answerAgent);

		$arrData = $wx_users->where("openid = '$openid'")->save(array("lastchat_time"=>date("Y-m-d H:i:s")));

		//快捷回复
		$wx_quick_reply = new Model("wx_quick_reply");
		$arrQR = $wx_quick_reply->where("answer_number = '$answerAgent' AND reply_enable='Y' AND reply_type='quick'")->find();
		$arrKeyQr = $wx_quick_reply->where("answer_number = '$answerAgent' AND reply_enable='Y' AND reply_type='keyword'")->select();
		foreach($arrKeyQr as $val){
			if( strstr($arrF["Content"],$val["keyword"]) ){
				$msg_content = $val["reply_content"];  //关键字回复
			}
		}
		if($msg_content){
			$quick_content = $msg_content;
		}else{
			$quick_content = $arrQR["reply_content"];
		}
		//dump($quick_content);die;
		if($quick_content){
			$weObj->text($quick_content)->reply();
			$arrFRy = array (
			  'ToUserName' => $arrF["FromUserName"],  //接收方帐号
			  'FromUserName' => $arrP["openid"],   //发送方帐号
			  'CreateTime' => date("Y-m-d H:i:s"),
			  'MsgType' => $arrF["MsgType"],
			  'Content' => $quick_content,
			  'nickname' => $arrF["nickname"],
			  'sendType' => "my",
			  'answer_seats' => $answerAgent,   //应答坐席
			  'MsgId' => '',
			);
			$this->weChatRecordCaChe($arrFRy);
			$arrFRy["type"] = "wechat";
			$arrTRy[0] = $arrFRy;
			$this->phpPush(json_encode($arrTRy),$answerAgent);
		}

		require 'BGCC/Conf/wx.php';
		//dump($arrT);die;
		//$this->display();
	}

	function groupBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row;
		}
		return $ret;
	}


	function groupCountBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row;
		}
		foreach($ret as $key=>$val){
			$arrF[$key] = count($ret[$key]);
		}
		return $arrF;
	}

	function phpPush($post_string,$answerAgent){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost/msgInterface/pub?user=U".$answerAgent);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "curl");
		$strReturn = curl_exec($ch);
		curl_close($ch);
		//echo $strReturn;
	}

	//分配微信账号的应答坐席
	function answerSeatAllocation(){
		$id = $_REQUEST["id"];
		//$recipient = $_REQUEST["recipient"];
		$wx_users = new Model("wx_users");
		$arrData = array(
			"answer_num" => $_REQUEST['recipient'],
		);
		$result = $wx_users->data($arrData)->where("id in ($id)")->save();
		$arrU = $wx_users->field("answer_num,openid")->select();
		foreach($arrU as $key=>$val){
			$tmp[$val["openid"]] = $val["answer_num"];
		}
		$db_name = "bgcrm";
		F('weChatAnswer',$tmp,"BGCC/Conf/crm/$db_name/");
		//F('weChatAnswer',$tmp,"BGCC/Conf/");
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"分配成功！"));
		} else {
			echo json_encode(array('msg'=>'分配失败！'));
		}
	}

	//微信菜单
	function weChatMemuList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "WeChat Menu";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	//微信菜单数据
	function weChatData(){
		$wx_menu = new Model("wx_menu");
		import('ORG.Util.Page');
		$count = $wx_menu->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wx_menu->order("wx_order desc")->field("id,name as text,pid,wx_order,action_type,event_value")->limit($page->firstRow.','.$page->listRows)->select();


		$i = 0;
		$row = array('click'=>'点击事件','view'=>'链接');
		foreach($arrData as &$val){
			$type = $row[$val['action_type']];
			$val['action_type2'] = $type;
			$arrData[$i]['state'] = "closed";
			$i++;
		}
		unset($i);

		$arrTree = $this->getTree($arrData,0);

		$j = 0;
		foreach($arrTree as $v){
			//if( $arrTree[$j]["cat_pid"] == "0" && !$arrTree[$j]["children"]){
			if( $arrTree[$j]["cat_pid"] == "0"){
				$arrTree[$j]['iconCls'] = "shop";  //顶级分类的图标，叶子节点的图标在getTree()方法中设置。
			}
			$j++;
		}
		unset($j);

		$rowsList = count($arrTree) ? $arrTree : false;
		$arrCat["total"] = $count;
		$arrCat["rows"] = $rowsList;
		//dump($arrTree);die;
		echo json_encode($arrCat);
	}

	function insertWeChatMenu(){
		$wx_menu = new Model("wx_menu");
		if($_POST['pid']){
			$pid = $_POST['pid'];
			$sub_count = $wx_menu->where("pid = '$pid'")->count();
			$arrF = $wx_menu->where("id = '$pid'")->find();
			if($sub_count >= 5){
				echo json_encode(array('msg'=>'每个顶级菜单最多有5个子菜单！'));
				die;
			}
			if($arrF["pid"] != "0"){
				echo json_encode(array('msg'=>'不能有三级菜单！'));
				die;
			}
		}else{
			$top_count = $wx_menu->where("pid = 0")->count();
			if($top_count >= 3){
				echo json_encode(array('msg'=>'顶级菜单菜单最多为3个！'));
				die;
			}
		}
		$arrData = Array(
			'name'=>$_POST['text'],
			'pid'=>$_POST['pid'],
			'wx_order'=>$_POST['wx_order'],
			'action_type'=>$_POST['action_type'],
			'event_value'=>$_POST['event_value'],
		);
		$result = $wx_menu->data($arrData)->add();
		if ($result){
			$this->createWeChatMenu("Y");
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateWeChatMenu(){
		$id = $_REQUEST['id'];
		$wx_menu = new Model("wx_menu");
		$arrData = Array(
			'name'=>$_POST['text'],
			'pid'=>$_POST['pid'],
			'wx_order'=>$_POST['wx_order'],
			'action_type'=>$_POST['action_type'],
			'event_value'=>$_POST['event_value'],
		);
		$result = $wx_menu->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			$this->createWeChatMenu("Y");
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function deleteChatMenu(){
		$id = $_REQUEST["id"];
		$wx_menu = new Model("wx_menu");
		$result = $wx_menu->where("id in ($id)")->delete();
		if ($result){
			$this->createWeChatMenu("Y");
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//在微信平台中添加菜单
	function createWeChatMenu($msg){
		header("Content-Type:text/html; charset=utf-8");
		$wx_menu = new Model("wx_menu");
		$arrData = $wx_menu->order("wx_order asc")->field("id,name,pid,action_type as type,event_value")->select();
		if(!$arrData){
			$this->deleteWeChatMenu();
			die;
		}
		foreach($arrData as &$val){
			if($val["type"] == "click"){
				$val["key"] = $val["event_value"];
			}
			if($val["type"] == "view"){
				$val["url"] = $val["event_value"];
			}
		}
		$arrTree = $this->getMenuTree($arrData,0);
		foreach($arrTree as $val){
			unset($val['id']) ;
			unset($val['pid']) ;
			unset($val['event_value']);
			if( !empty($val["sub_button"]) ){
				foreach($val["sub_button"] as $v){
					unset($v['id']) ;
					unset($v['pid']) ;
					unset($v['event_value']);
					$arrSub[] = $v;
				}
				unset($val['type']) ;
				$val["sub_button"] = $arrSub;
			}
			$arrF[]  = $val;
		}
		$arrMenu["button"] = $arrF;
		$menu_json = json_encode($arrMenu);

		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);
		//$menu = $weObj->getMenu(); //获取菜单
		//dump($menu);die;
		$resultMenu = $weObj->createMenu($arrMenu);   //创建菜单
		if($msg){
			return true;
		}else{
			if($resultMenu === true){
				echo json_encode(array('success'=>true,'msg'=>"微信菜单创建成功！"));
			} else {
				echo json_encode(array('msg'=>'微信菜单创建失败！'));
			}
		}
	}

	//在微信平台中删除菜单
	function deleteWeChatMenu(){
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);
		$resultMenu = $weObj->deleteMenu();   //删除菜单
		if($resultMenu === true){
			echo json_encode(array('success'=>true,'msg'=>"微信菜单删除成功！"));
		} else {
			echo json_encode(array('msg'=>'微信菜单删除失败，您的微信账号中添没有菜单！'));
		}
	}


	function getMenuTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
				$v['sub_button'] = $this->getMenuTree($data, $v['id']);
				if ( empty($v["sub_button"])  ) {
					unset($v['sub_button']) ;
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	function getTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
				$v['children'] = $this->getTree($data, $v['id']);

				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				if ( empty($v["children"])){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}else{
					$v['iconCls'] =  'wechat';  //不是顶级分类，但有叶子节点的分类的图标
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	function weChatTree(){
		$wx_menu = new Model("wx_menu");

		$arrData = $wx_menu->order("wx_order desc")->field("id,name as text,pid")->select();
		$nodeStatus = $_GET['nodeStatus'];
		if($nodeStatus == 'colse'){
			$i = 0;
			foreach($arrData as $v){
				$arrData[$i]['state'] = "closed";
				$i++;
			}
			unset($i);
		}

		$arrTree = $this->getTree($arrData,0);
		if( $nodeStatus != 'other' && $nodeStatus !== "fitting"){
			$allCate = array(
				"text"=>"顶级菜单",
				"id"=>"0",
				"pid"=>"0",
				"iconCls"=>"wechat",
			);
			array_unshift($arrTree,$allCate);
		}

		if($nodeStatus == "open"){
			$j = 0;
			foreach($arrTree as $v){
				//if( $arrTree[$j]["pid"] == "0" && !$arrTree[$j]["children"]){
				if( $arrTree[$j]["pid"] == "0"){
					$arrTree[$j]['iconCls'] = "wechat";
				}
				$j++;
			}
			unset($j);
		}
		//dump($arrData);
		echo  json_encode($arrTree);
	}




	//微信参数设置
	function weChatSetList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "WeChat settings";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function weChatSetData(){
		$wx_set = new Model("wx_set");
		$count = $wx_set->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wx_set->order("id asc")->limit($page->firstRow.','.$page->listRows)->select();

		foreach($arrData as &$val){
			if( $val["name"] == "password" ){
				$val["password"] = $val["wx_value"];
				if($val["wx_value"]){
					$val["wx_value"] = substr($val["wx_value"],0,3)."***".substr($val["wx_value"],-4);
				}
			}

			if($val["name"] == "url_check"){
				$val["radio_able"] = "Y";
				$val["radio_value"] = "Y,已验证|N,未验证";
				$val["radioText"] = explode("|",$val["radio_value"]);
				foreach($val["radioText"] as &$vm){
					$vm = explode(",",$vm);
					if($vm[0] == $val["wx_value"]){
						$vm["checked"] = "checked";
						$vm["radio"] = "<input type='radio' name='".$val["name"]."' value='".$vm[0]."' checked onclick=\"updateSys("."'".$val["id"]."','".$vm[0]."'".");\" >".$vm[1]."&nbsp;&nbsp;";
					}else{
						$vm["radio"] = "<input type='radio' name='".$val["name"]."' value='".$vm[0]."' onclick=\"updateSys("."'".$val["id"]."','".$vm[0]."'".");\" >".$vm[1]."&nbsp;&nbsp;";
					}
					$val["radioValue"][] = $vm["radio"];
					$val["radioMsg"] = implode("&nbsp;",$val["radioValue"]);
				}
			}else{
				$val["radio_able"] = "N";

			}
		}

		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertWeChatSet(){
		$wx_set = new Model("wx_set");
		$arrData = Array(
			'name'=>$_REQUEST['name'],
			'description'=>$_REQUEST['description'],
			'wx_value'=>$_REQUEST['wx_value'],
		);
		$result = $wx_set->data($arrData)->add();
		if ($result){
			$this->weChatSetCache();
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateWeChatSet(){
		$id = $_REQUEST['id'];
		$wx_set = new Model("wx_set");
		if( $_REQUEST['name'] == "password" ){
			$wx_value = $_REQUEST['pwd'];
		}else{
			$wx_value = $_REQUEST['wx_value'];
		}
		$arrData = Array(
			//'name'=>$_REQUEST['name'],
			'description'=>$_REQUEST['description'],
			'wx_value'=>$wx_value,
		);
		//dump($arrData);die;
		$result = $wx_set->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			$this->weChatSetCache();
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function updateWeChatSet2(){
		$id = $_REQUEST["id"];
		$value = $_REQUEST["value"];
		$mod = M("wx_set");

		$result = $mod->where("id='$id'")->save(array("wx_value"=>$value));
		//echo $mod->getLastSql();die;
		if ($result !== false){
			$this->weChatSetCache();
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteWeChatSet(){
		$id = $_REQUEST["id"];
		$wx_set = new Model("wx_set");
		$result = $wx_set->where("id in ($id)")->delete();
		if ($result){
			$this->weChatSetCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//设置缓存
	function weChatSetCache(){
		$wx_set = new Model("wx_set");
		$arr = $wx_set->select();

		foreach($arr as $key => $val){
			$tmp[$val["name"]]	= $val["wx_value"];
		}
		$db_name = "bgcrm";
		F('weChatSet',$tmp,"BGCC/Conf/crm/$db_name/");
	}


	//微信用户
	function weChatUsersList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "WeChat Users";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function weChatUsersData(){
		$username = $_SESSION['user_info']['username'];
		$starttime = $_REQUEST["starttime"];
		$endtime = $_REQUEST["endtime"];
		$nickname = $_REQUEST["nickname"];
		$sex = $_REQUEST["sex"];
		$province = $_REQUEST["province"];
		$city = $_REQUEST["city"];
		$user = $_REQUEST["user"];

		$where = "1 ";
		if($username != "admin"){
			$where .= " AND answer_num='$username'";
		}
		$where .= empty($starttime) ? "" : " AND lastchat_time >= '$starttime'";
		$where .= empty($endtime) ? "" : " AND lastchat_time <= '$endtime'";
		$where .= empty($nickname) ? "" : " AND nickname like '%$nickname%'";
		$where .= empty($sex) ? "" : " AND sex = '$sex'";
		$where .= empty($province) ? "" : " AND province like '%$province%'";
		$where .= empty($city) ? "" : " AND city like '%$city%'";
		$where .= empty($user) ? "" : " AND answer_num = '$user'";


		$wx_users = new Model("wx_users");
		$count = $wx_users->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wx_users->order("lastchat_time desc,subscribe_time desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$row = array('1'=>'男','2'=>'女','0'=>'未知');
		foreach($arrData as &$val){
			$sex = $row[$val['sex']];
			$val['sex'] = $sex;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function getWeChatUsers(){
		header("Content-Type:text/html; charset=utf-8");
		set_time_limit(0);
		ini_set('memory_limit','-1');

		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		$users = $weObj->getUserList(); //批量获取关注用户列表
		$arrUser = $users["data"]["openid"];
		foreach($arrUser as $val){
			$arrU[] = $weObj->getUserInfo($val);
		}

		/*
		import('ORG.WeChat.Wechatext');
		$options = array(
			'account' => $arrP["openid"],    //这三个值都行  登录邮箱、原始ID、微信号
			'password' => $arrP["password"],
			'debug' => false,
		);
		$wechat = new Wechatext($options);
		if ($wechat->checkValid()) {
			$userList = $wechat->getUserlist(0,10);
			foreach($userList as $val){
				$userdata[] = $wechat->getInfo($val['id']);
			}
		}else{
			echo "login error";
		}
		*/
		$wx_users = new Model("wx_users");
		$arrData = $wx_users->field("nickname,openid")->select();
		foreach($arrData as $val){
			$arrOpenid[] = $val["openid"];
		}
		//$wx_users->execute("TRUNCATE TABLE wx_users");
		$sql = "insert into wx_users(subscribe_time,nickname,openid,subscribe,sex,country,city,province,language,headimgurl,remark,unionid) values";

		foreach($arrU as &$val){
			$val["subscribe_time"] = date("Y-m-d H:i:s",$val["subscribe_time"]);
			if( !in_array($val["openid"],$arrOpenid) ){
				$str = "('" .$val["subscribe_time"]."','".$val["nickname"]."','".$val["openid"]."','".$val["subscribe"]."','".$val["sex"]."','".$val["country"]."','".$val["city"]."','".$val["province"]."','".$val["language"]."','".$val["headimgurl"]."','".$val["remark"]."','".$val["unionid"]."')";
				$value .= empty($value)?$str:",$str";
			}else{
				$openid2 = $val["openid"];
				$res = $wx_users->data($val)->where("openid ='$openid2' ")->save();
			}
		}
		$sql .= $value;
		$result = $wx_users ->execute($sql);
		if ($result || $res !== false){
			echo json_encode(array('success'=>true,'msg'=>'获取用户信息成功！'));
		} else {
			echo json_encode(array('msg'=>'获取用户信息失败！'));
		}
	}




	//微信记录
	function weChatRecordList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "WeChat Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}


	function customService(){
		header("Content-Type:text/html; charset=utf-8");
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		$arr = array(
			"starttime" => "1407726703",
			"endtime" => "1407752228",
			//"openid" => "gh_34ff33e9c158",
			"openid" => "oAVUOuFznCr5KVQJP-pwxb0JKQHk",
			"pagesize" => "1000",
			"pageindex" => "1",
		);
		$users = $weObj->getCustomServiceMessage($arr); //批量获取关注用户列表
		echo time();

		dump($users);die;
	}

	function sendMessage(){
		checkLogin();
		//分配增删改的权限
		$menuname = "WeChat Customer Service";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$username = $_SESSION['user_info']['username']."/".$_SESSION['user_info']['cn_name'];
		$this->assign("answer_seats",$username);

		$this->display();
	}

	//测试替换
	function faceImg(){
		$arr = require "Agent/Tpl/public/js/qqFace/faceImage.php";
		$str = "Hi /::) this /::~/::B test /::$";
		foreach($arr as $key=>$val){
			$arrKey[] = $key;
			$arrVal[] = $val;
		}
		$tt2 = str_replace($arrKey,$arrVal,$str);

		dump($arrKey);
		dump($arrVal);
		dump($tt2);
		die;
	}

	//可以删除
	function sendWeChatTest(){
		$this->display();
	}
	//微信发送消息测试
	function sendWeChatTestData(){
		header("Content-Type:text/html; charset=utf-8");
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);
		//dump($access_token);
		$msgtype = $_REQUEST["type"];
		$touser = "oAVUOuFznCr5KVQJP-pwxb0JKQHk";

		if($msgtype == "text"){
			$arr = array(
				"touser" => $touser,
				"msgtype" => $msgtype,
				"text" => array("content"=>$_REQUEST["content"]),
			);
		}elseif($msgtype == "image"){  //图片
			$file = realpath('include/data/weChat/images/1407998906367.jpeg'); //要上传的文件
			$fields = array("media"=>'@'.$file);
			$arrData = $weObj->uploadMedia2($fields,$msgtype);
			$arrD = json_decode($arrData,true);
			$arr = array(
				"touser" => $touser,
				"msgtype" => $msgtype,
				"image"=>array("media_id"=>$arrD["media_id"]),
			);
		}elseif($msgtype == "voice"){    //语音
			$file = realpath('include/data/weChat/voice/hpqy.mp3'); //要上传的文件
			$fields = array("media"=>'@'.$file);
			$arrData = $weObj->uploadMedia2($fields,$msgtype);
			$arrD = json_decode($arrData,true);
			$arr = array(
				"touser" => $touser,
				"msgtype" => $msgtype,
				"voice"=>array("media_id"=>$arrD["media_id"]),
			);
		}elseif($msgtype == "video"){   //视频
			$file = realpath('include/data/weChat/video/meipai_20140811124528.mp4'); //要上传的文件
			$fields = array("media"=>'@'.$file);
			$arrData = $weObj->uploadMedia2($fields,$msgtype);
			$arrD = json_decode($arrData,true);

			$file2 = realpath('include/data/weChat/images/121026112458_1.jpg'); //要上传的文件
			$fields2 = array("media"=>'@'.$file2);
			$arrData2 = $weObj->uploadMedia2($fields2,"thumb");
			$arrD2 = json_decode($arrData2,true);

			$arr = array(
				"touser" => $touser,
				"msgtype" => $msgtype,
				"video"=>array(
					"media_id " => $arrD["media_id"],
					"thumb_media_id" => $arrD2["thumb_media_id"],
					"title" => "视频标题",
					"description" => "视频描述",
				),
			);
			//dump($arrD);
			//dump($arrD2);die;
			file_put_contents("BGCC/Conf/send2.txt",var_export($arr,TRUE));
		}elseif($msgtype == "music"){   //音乐
			$file = realpath('include/data/weChat/images/121026112458_1.jpg'); //要上传的文件
			$fields = array("media"=>'@'.$file);
			$arrData = $weObj->uploadMedia2($fields,"thumb");
			$arrD = json_decode($arrData,true);
			$arr = array(
				"touser" => $touser,
				"msgtype" => $msgtype,
				"music"=>array(
					"musicurl " => "http://play.baidu.com/?__m=mboxCtrl.playSong&__a=265436&__o=/artist/1396||songIcon&fr=-1||www.baidu.com#loaded",
					"hqmusicurl" => "http://play.baidu.com/?__m=mboxCtrl.playSong&__a=265436&__o=/song/265436||playBtn&fr=-1||www.baidu.com#loaded",
					"thumb_media_id" => $arrD["thumb_media_id"],
					"title" => "音乐标题",
					"description" => "音乐描述",
				),
			);
			//dump($arrD);die;
		}elseif($msgtype == "news"){   //图文
			$arr = array (
				'touser' => $touser,
				'msgtype' => 'news',
				'news' => array (
					'articles' => array (
						0 => array (
							'title' => '反腐',
							'description' => '中纪委1天内公布10名厅官被查消息 反腐再刷纪录',
							'url' => 'http://news.qq.com/a/20140814/000942.htm',
							'picurl' => '',
						),
						1 => array (
							'title' => '国事',
							'description' => '习近平将于8月21日至22日对蒙古国进行国事访问',
							'url' => 'http://news.qq.com/a/20140814/059637.htm?tu_biz=1.114.1.0',
							'picurl' => 'http://www.baidu.com/img/bdlogo.png',
						),
					),
				),
			);
		}
		$send = $weObj->sendCustomMessage($arr);
		if($send["errmsg"] == "ok"){
			echo json_encode(array('success'=>true,'msg'=>"发送成功！"));
		} else {
			echo json_encode(array('msg'=>'发送失败！'));
		}
	}
	//发送图片
	function sendImage(){
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);
		$file = realpath('include/data/53eb34dbbd524.jpg'); //要上传的文件
		$fields = array("media"=>'@'.$file);
		$arrData = $weObj->uploadMedia2($fields,"image");
		$arrD = json_decode($arrData,true);
		//$id = "6-AAZqI7Ahh5cVXhN0gWPpx0igK1sLGiaqoUSeovXE0bSgX4rsQ1T5amtAaaiUnu";
		//$get = $weObj->saveMedia($id,"include/data/weChat/video/");  //下载文件
		//dump($access_token);
		//dump($get);die;
		$arr = array(
			"touser"=>"oAVUOuFznCr5KVQJP-pwxb0JKQHk",
			"msgtype"=>"image",
			"image"=>array("media_id"=>$arrD["media_id"]),
		);
		$send = $weObj->sendCustomMessage($arr);
		if($send["errmsg"] == "ok"){
			echo json_encode(array('success'=>true,'msg'=>"发送成功！"));
		} else {
			echo json_encode(array('msg'=>'发送失败！'));
		}
	}

	function uplodeMediaFile(){
		//dump($_REQUEST);die;
		$filetype = $_REQUEST['filetype'];
		$filename = $_REQUEST['filename'];
		$name = $_REQUEST['savename'];
		if($filetype == "image"){
			$path = "include/data/weChat/images/";
			$arrExt = array('jpg','jpeg','png');
		}elseif($filetype == "voice"){
			$path = "include/data/weChat/voice/";
			$arrExt = array('AMR','amr','MP3','mp3');
			//$arrExt = array('MP3','mp3');
		}
		$suffix = array_pop(explode(".",$filename));
		if( !in_array($suffix,$arrExt)){
			echo json_encode(array('msg'=>"上传文件类型不允许！"));
			die;
		}

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize ='1000000';
		$upload->savePath= $path;  //上传路径
		//dump($suffix);die;

		$upload->saveRule=uniqid;    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			echo json_encode(array('msg'=>$mess."<br>  只支持jpg、png格式的图片!"));
		}else{
			$info=$upload->getUploadFileInfo();
			$filename = $info[0]["savepath"].$info[0]["savename"];
			$result = $this->sendMediaWeChat($filetype,$name,$filename);
			//dump($filename);die;
			if($filetype == "image"){
				$mess = $filename;
			}elseif($filetype == "voice"){
				$mess = $filename;
			}
			if($result){
				echo json_encode(array('success'=>true,'message'=>$mess,'time'=>date("Y-m-d H:i:s")));
			}else{
				echo json_encode(array('msg'=>"发送失败！不正确的文件格式！"));
			}
		}
	}

	function sendMediaWeChat($msgtype,$touser,$filename){
		header("Content-Type:text/html; charset=utf-8");
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		if($msgtype == "image"){  //图片
			$Content = '<img src="'.$filename.'" onload="javascript:DrawImage(this,100,100);" class="bigImage" >';
			$file = realpath($filename); //要上传的文件
			$fields = array("media"=>'@'.$file);
			$arrData = $weObj->uploadMedia2($fields,$msgtype);
			$arrD = json_decode($arrData,true);
			$arr = array(
				"touser" => $touser,
				"msgtype" => $msgtype,
				"image"=>array("media_id"=>$arrD["media_id"]),
			);
		}elseif($msgtype == "voice"){    //语音
			$Content = $filename;
			$file = realpath($filename); //要上传的文件
			$fields = array("media"=>'@'.$file);
			$arrData = $weObj->uploadMedia2($fields,$msgtype);
			$arrD = json_decode($arrData,true);
			$arr = array(
				"touser" => $touser,
				"msgtype" => $msgtype,
				"voice"=>array("media_id"=>$arrD["media_id"]),
			);
		}
		$send = $weObj->sendCustomMessage($arr);

		$arrUser = $weObj->getUserInfo($touser);



		$arrF = array (
		  'ToUserName' => $touser,  //接收方帐号
		  'FromUserName' => $arrP["openid"],    //发送方帐号
		  'CreateTime' => date("Y-m-d H:i:s"),
		  'MsgType' => $msgtype,
		  'Content' => $Content,
		  'nickname' => $arrUser["nickname"],
		  'sendType' => "my",
		  'answer_seats' => $_SESSION['user_info']['username'],   //应答坐席
		  'MsgId' => '',
		);
		if($send["errmsg"] == "ok"){
			$this->weChatRecordCaChe($arrF);
			return true;
		} else {
			return false;
		}
	}

	function sendWeChat(){
		//dump($_REQUEST);die;
		header("Content-Type:text/html; charset=utf-8");
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);


		$arr = array(
			"touser" => $_REQUEST["savename"],
			"msgtype" => $_REQUEST["msgtype"],
			"text" => array("content"=>$_REQUEST["content"]),
		);
		//dump($_REQUEST);
		//dump($arr);die;
		$send = $weObj->sendCustomMessage($arr);

		$arrF = array (
		  'ToUserName' => $_REQUEST["savename"],  //接收方帐号
		  'FromUserName' => $arrP["openid"],   //发送方帐号
		  'CreateTime' => date("Y-m-d H:i:s"),
		  'MsgType' => $_REQUEST["msgtype"],
		  'Content' => $_REQUEST["message"],
		  'nickname' => $_REQUEST["nickname"],
		  'sendType' => "my",
		  'answer_seats' => $_SESSION['user_info']['username'],   //应答坐席
		  'MsgId' => '',
		);
		if($send["errmsg"] == "ok"){
			$this->weChatRecordCaChe($arrF);
			echo json_encode(array('success'=>true,'msg'=>"发送成功！"));
		} else {
			echo json_encode(array('msg'=>'发送失败！可能是您跟该用户24小时内没有联系或者该用户没有关注您的微信号！'));
		}
	}

	//发送图文消息
	function sendWeChatNews(){
		header("Content-Type:text/html; charset=utf-8");
		$newsid = $_REQUEST["newsid"];
		$openid = $_REQUEST["openid"];
		$wx_news = new Model("wx_news");
		$arrData = $wx_news->where("id = '$newsid'")->find();
		$sendMsg = json_decode($arrData["sendMsg"],true);
		$ip = "http://".$_SERVER['SERVER_ADDR']."/";

		foreach($sendMsg as &$val){
			$arrM[] = $val;
			unset($val["content"]);
			$val["picurl"] = $ip.$val["picurl"];
			$val["url"] = $ip.$val["url"];
		}

		//dump($arrM);

		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		$arr = array(
			"touser" => $_REQUEST["openid"],
			"msgtype" => "news",
			"news" => array('articles'=>$sendMsg),
		);
		//dump($_REQUEST);
		//dump($arr);die;
		$send = $weObj->sendCustomMessage($arr);

		$arrF = array (
		  'ToUserName' => $_REQUEST["openid"],  //接收方帐号
		  'FromUserName' => $arrP["openid"],   //发送方帐号
		  'CreateTime' => date("Y-m-d H:i:s"),
		  'MsgType' => "news",
		  'Content' => $arrData["sendMsg"],
		  'nickname' => $_REQUEST["nickname"],
		  'sendType' => "my",
		  'answer_seats' => $_SESSION['user_info']['username'],   //应答坐席
		  'MsgId' => '',
		);
		if($send["errmsg"] == "ok"){
			$this->weChatRecordCaChe($arrF);
			echo json_encode(array('success'=>true,'msg'=>"发送成功！"));
		} else {
			echo json_encode(array('msg'=>'发送失败！可能是您跟该用户24小时内没有联系或者该用户没有关注您的微信号！'));
		}

	}

	//将消息写到文件中
	function weChatRecordCaChe($arr){
		$db_name = "bgcrm";
		$arrData = array($arr);
		if( file_exists("BGCC/Conf/crm/$db_name/weChatRecord.php") ){
			$arrF = require "BGCC/Conf/crm/$db_name/weChatRecord.php";
		}
		if($arrF){
			$arrT = array_merge($arrF,$arrData);
		}else{
			$arrT = $arrData;
		}

		//dump($arrT);die;
		$content = "<?php\nreturn " .var_export($arrT,true) .";\n ?>";
		file_put_contents("BGCC/Conf/crm/$db_name/weChatRecord.php",$content);
	}


	//微信记录【用于推送记录】
	function weChatRecord(){
		$arr = $_REQUEST["data"];
		$openid = $arr["FromUserName"];
		$wx_users = new Model("wx_users");
		$arrData = $wx_users->where("openid = '$openid'")->find();
		$username = $_SESSION['user_info']['username'];
		$where = "1 ";
		if($username != "admin"){
			$where .= " AND answer_seats='$username'";
		}
		$where .= empty($openid) ? "" : " AND (ToUserName='$openid' OR FromUserName='$openid')";
		$wx_message_record = new Model("wx_message_record");
		$arrData2 = $wx_message_record->where($where)->select();
		if(!$arrData2){
			$arrData2 = array();
		}

		$db_name = "bgcrm";
		$filename = "BGCC/Conf/crm/$db_name/weChatRecord.php";
		if(file_exists($filename)){
			$arrF = require "BGCC/Conf/crm/$db_name/weChatRecord.php";
			foreach($arrF as $val){
				if($openid){
					if($val["FromUserName"] == $openid || $val["ToUserName"] == $openid){
						$arrFr[] = $val;
					}
				}
			}
			$arr2 = empty($openid) ? $arrF : $arrFr;
			$arrR = array_merge($arrData2,$arr2);
		}else{
			$arrR = $arrData2;
		}

		//$arrR = require "BGCC/Conf/weChatRecord.php";
		foreach($arrR as &$val){
			if($val["sendType"] == "my"){
				$val["nickname"] = $val["answer_seats"];
			}
			if($val["MsgType"] == "voice"){
				$val["Content"] = '<embed src="include/ui/niftyplayer.swf?file='.$val["Content"].'&as=0" quality=high bgcolor=#FFFFFF width="165" height="37" name="niftyPlayer1" align="" type="application/x-shockwave-flash" swLiveConnect="true" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>';  //可以控制声音
			}
			if($val["FromUserName"] == $openid || $val["ToUserName"] == $openid){
				$arrT[] = $val;
			}
		}

		$arrF = array(
			"userMsg" => $arrData,
			"weChatData" => array_slice($arrT,-6,6),
		);

		echo json_encode($arrF);
	}


	//微信记录【用于双击用户的】
	function weChatRecord2(){
		$arr = $_REQUEST["data"];
		$openid = $arr["openid"];
		$wx_users = new Model("wx_users");
		$arrData = $wx_users->where("openid = '$openid'")->find();
		$username = $_SESSION['user_info']['username'];
		$where = "1 ";
		if($username != "admin"){
			$where .= " AND answer_seats='$username'";
		}
		$where .= empty($openid) ? "" : " AND (ToUserName='$openid' OR FromUserName='$openid')";

		$wx_message_record = new Model("wx_message_record");
		$arrData2 = $wx_message_record->where($where)->select();
		if(!$arrData2){
			$arrData2 = array();
		}

		$db_name = "bgcrm";
		$filename = "BGCC/Conf/crm/$db_name/weChatRecord.php";
		if(file_exists($filename)){
			$arrF = require "BGCC/Conf/crm/$db_name/weChatRecord.php";
			foreach($arrF as $val){
				if($openid){
					if($val["FromUserName"] == $openid || $val["ToUserName"] == $openid){
						$arrFr[] = $val;
					}
				}
			}
			$arr2 = empty($openid) ? $arrF : $arrFr;
			$arrR = array_merge($arrData2,$arr2);
		}else{
			$arrR = $arrData2;
		}

		//$arrR = require "BGCC/Conf/weChatRecord.php";
		foreach($arrR as &$val){
			if($val["sendType"] == "my"){
				$val["nickname"] = $val["answer_seats"];
			}
			if($val["MsgType"] == "voice"){
				$val["Content"] = '<embed src="include/ui/niftyplayer.swf?file='.$val["Content"].'&as=0" quality=high bgcolor=#FFFFFF width="165" height="37" name="niftyPlayer1" align="" type="application/x-shockwave-flash" swLiveConnect="true" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>';  //可以控制声音
				//$val["Content"] = '<embed src="include/ui/singlemp3player.swf?file='.$val["Content"].'&as=0" quality=high bgcolor=#FFFFFF width="165" height="37" name="niftyPlayer1" align="" type="application/x-shockwave-flash" swLiveConnect="true" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>';     //不能控制声音
			}
			if($val["FromUserName"] == $openid || $val["ToUserName"] == $openid){
				$arrT[] = $val;
			}
		}

		//dump($arrT);die;
		$arrF = array(
			"userMsg" => $arrData,
			"weChatData" => array_slice($arrT,-6,6),
		);

		echo json_encode($arrF);
	}

	//历史聊天记录
	function weChatSearch(){
		checkLogin();
		//分配增删改的权限
		$menuname = "WeChat Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function weChatSearchData(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$openid = $_REQUEST["openid"];
		$nickname = $_REQUEST["nickname"];
		$where = "1 ";
		if($username != "admin"){
			$where .= " AND answer_seats='$username'";
		}
		$where .= empty($openid) ? "" : " AND (ToUserName='$openid' OR FromUserName='$openid')";

		$range = $_REQUEST["range"];
		$content = $_REQUEST["content"];
		$where .= empty($content) ? "" : " AND Content like '%$content%'";
		$now = date("Y-m-d H:i:s");
		$date1 = date("Y-m-d");
		if($range){
			if($range == "recent_week"){
				$time = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-7,date("Y")))." 00:00:00";
				$where .= " AND CreateTime > '$time' AND CreateTime < '$now'";
			}elseif($range == "recent_month"){
				$time = date('Y-m-d',strtotime("$date1 -1 month"))." 00:00:00";
				$where .= " AND CreateTime > '$time' AND CreateTime < '$now'";
			}elseif($range == "recent_year"){
				$time = date('Y-m-d',strtotime("$date1 -1 year"))." 00:00:00";
				$where .= " AND CreateTime > '$time' AND CreateTime < '$now'";
			}else{
				$where .= " AND 1 ";
			}
		}


		$wx_message_record = new Model("wx_message_record");
		$arrData = $wx_message_record->order("CreateTime desc")->where($where)->select();

		//echo $wx_message_record->getLastSql();die;
		if(!$arrData){
			$arrData = array();
		}

		$db_name = "bgcrm";
		$filename = "BGCC/Conf/crm/$db_name/weChatRecord.php";
		if(file_exists($filename)){
			$arrF = require "BGCC/Conf/crm/$db_name/weChatRecord.php";
			foreach($arrF as $val){
				if($openid){
					if($val["FromUserName"] == $openid || $val["ToUserName"] == $openid){
						$arrFr[] = $val;
					}
				}
			}
			$arr = empty($openid) ? $arrF : $arrFr;
			if($content){
				foreach($arr as $val){
					if( strpos($val["Content"],$content)!== false ){
						$arr2[] = $val;
					}
				}
			}
			$arr3 = empty($content) ? $arr : $arr2;
			//dump($arr3);die;
			$arrRData = array_merge($arrData,$arr3);
		}else{
			$arrRData = $arrData;
		}

		foreach($arrRData as &$val){
			if($val["MsgType"] == "voice"){
				$val["Content"] = '<embed src="include/ui/niftyplayer.swf?file='.$val["Content"].'&as=0" quality=high bgcolor=#FFFFFF width="165" height="37" name="niftyPlayer1" align="" type="application/x-shockwave-flash" swLiveConnect="true" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>';  //可以控制声音
				//$val["Content"] = '<embed src="include/ui/singlemp3player.swf?file='.$val["Content"].'&as=0" quality=high bgcolor=#FFFFFF width="165" height="37" name="niftyPlayer1" align="" type="application/x-shockwave-flash" swLiveConnect="true" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>';     //不能控制声音
			}

			if($val["sendType"] == "my"){
				$val["msg"] = '<span style="color:#5ae214;" class="mycolor">'.$val["answer_seats"]."  ".$val["CreateTime"]."</span><br><span style='margin-left:20px;'>".$val["Content"].'</span>';
			}else{
				$val["nickname"] = $val["from_nickname"];
				$val["msg"] = '<span style="color:red;" class="othercolor">'.$val["nickname"]."  ".$val["CreateTime"]."</span><br><span style='margin-left:20px;'>".$val["Content"].'</span>';
			}
			//$val["order"] = strtotime($val["CreateTime"]);
		}
		$arrRD = $this->array_sort($arrRData,"CreateTime","desc","no"); //排序

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrRD);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		$arrRecordData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrRD AS &$v){
			if($i >= $start && $j < $length){
				$arrRecordData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		//dump($arrRecordData);die;
		$rowsList = count($arrRecordData) ? $arrRecordData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}


	//快捷回复信息
	function quickReplyData(){
		$username = $_SESSION['user_info']['username'];
		$quick_reply = new Model("wx_quick_reply");
		$where = "1 ";
		$where .= " AND reply_type ='quick'";
		if($username != "admin"){
			$where .= " AND answer_number='$username'";
		}

		$count = $quick_reply->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $quick_reply->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$row = array('Y'=>'启用','N'=>'禁用');
		foreach($arrData as &$val){
			$reply_enable = $row[$val['reply_enable']];
			$val['reply_enable2'] = $reply_enable;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertQuickReply(){
		$username = $_SESSION['user_info']['username'];
		$quick_reply = new Model("wx_quick_reply");
		$reply_enable = empty($_REQUEST['reply_enable']) ? "N" : $_REQUEST['reply_enable'];
		$arrData = Array(
			'answer_number' => $username,
			'reply_content' => $_REQUEST['reply_content'],
			'reply_enable' => $reply_enable,
		);
		$result = $quick_reply->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateQuickReply(){
		$id = $_REQUEST['id'];
		$answer_number = $_REQUEST['answer_number'];
		$quick_reply = new Model("wx_quick_reply");
		$reply_enable = empty($_REQUEST['reply_enable']) ? "N" : $_REQUEST['reply_enable'];
		$arrData = Array(
			'reply_content' => $_REQUEST['reply_content'],
			'reply_enable' => $reply_enable,
		);
		$result = $quick_reply->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			if($reply_enable == "Y" ){
				$quick_reply->data($arrData)->where("answer_number='$answer_number' AND id!=$id")->save(array("reply_enable"=>"N"));
			}
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteQuickReply(){
		$id = $_REQUEST["id"];
		$quick_reply = new Model("wx_quick_reply");
		$result = $quick_reply->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//关键字回复信息
	function keywordReplyData(){
		$username = $_SESSION['user_info']['username'];
		$quick_reply = new Model("wx_quick_reply");
		$where = "1 ";
		$where .= " AND reply_type ='keyword'";
		if($username != "admin"){
			$where .= " AND answer_number='$username'";
		}

		$count = $quick_reply->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $quick_reply->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$row = array('Y'=>'启用','N'=>'禁用');
		foreach($arrData as &$val){
			$reply_enable = $row[$val['reply_enable']];
			$val['reply_enable2'] = $reply_enable;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertKeyWordReply(){
		$username = $_SESSION['user_info']['username'];
		$quick_reply = new Model("wx_quick_reply");
		$reply_enable = empty($_REQUEST['reply_enable']) ? "N" : $_REQUEST['reply_enable'];
		$arrData = Array(
			'answer_number' => $username,
			'reply_type' => "keyword",
			'keyword' => $_REQUEST['keyword'],
			'reply_content' => $_REQUEST['reply_content'],
			'reply_enable' => $reply_enable,
		);
		$result = $quick_reply->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateKeyWordReply(){
		$id = $_REQUEST['id'];
		$answer_number = $_REQUEST['answer_number'];
		$quick_reply = new Model("wx_quick_reply");
		$reply_enable = empty($_REQUEST['reply_enable']) ? "N" : $_REQUEST['reply_enable'];
		$arrData = Array(
			'reply_content' => $_REQUEST['reply_content'],
			'keyword' => $_REQUEST['keyword'],
			'reply_enable' => $reply_enable,
		);
		$result = $quick_reply->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteKeyWordReply(){
		$id = $_REQUEST["id"];
		$quick_reply = new Model("wx_quick_reply");
		$result = $quick_reply->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//创建分组
	function createWeChatGroup(){
		header("Content-Type:text/html; charset=utf-8");
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		$group = $weObj->getGroup();  //获取用户分组
		//$result = $weObj->createGroup("AutoCall1");  //创建分组
		/*
		//$result = $weObj->createGroup("AutoCall1");返回一个数组
		array(1) {
		  ["group"] => array(2) {
			["id"] => int(101)
			["name"] => string(7) "AutoCall1"
		  }
		}
		*/
		//$modifyGroup = $weObj->updateGroup("100","AutoCall");  //修改分组名称
		/*
		$modifyGroup = $weObj->updateGroup("100","AutoCall1");  //修改分组名称 返回
		array(2) {
		  ["errcode"] => int(0)
		  ["errmsg"] => string(2) "ok"
		}
		*/

		//$moveUser = $weObj->updateGroupMembers("100","oAVUOuFznCr5KVQJP-pwxb0JKQHk");  //移动用户到分组

		dump($group);
		//dump($modifyGroup);
		die;

	}

	//生成带参数的二维码
	function createTicKet(){
		header("Content-Type:text/html; charset=utf-8");
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		$qrcode = $weObj->getQRCode("1");
		$arr = $weObj->getQRUrl($qrcode["ticket"]);
		dump($qrcode);
		dump($arr);
		die;
	}

	//群发发送图文消息
	function sendMassWeChatNews(){
		header("Content-Type:text/html; charset=utf-8");
		$newsid = $_REQUEST["newsid"];
		$openid = $_REQUEST["openid"];
		$wx_news = new Model("wx_news");
		$arrData = $wx_news->where("id = '$newsid'")->find();
		$sendMsg = json_decode($arrData["sendMsg"],true);
		$ip = "http://".$_SERVER['SERVER_ADDR']."/";

		foreach($sendMsg as &$val){
			$arrM[] = $val;
			unset($val["content"]);
			$val["picurl"] = $ip.$val["picurl"];
			$val["url"] = $ip.$val["url"];
		}

		//dump($arrM);

		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

	}

	//群发
	function bulkWeChatList(){
		$this->display();
	}
	//文本
	function bulkText(){
		header("Content-Type:text/html; charset=utf-8");
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		$id = $_REQUEST["id"];
		$content = $_REQUEST["content"];
		$message = $_REQUEST["message"];
		$msgtype = $_REQUEST["msgtype"];
		$bulk_obj = $_REQUEST["bulk_obj"];
		$id = $_REQUEST["id"];

		$wx_users = new Model("wx_users");
		if($bulk_obj == "checked"){
			$where = "id in ($id)";
		}else{
			$where = "1 ";
		}
		$arrU = $wx_users->field("id,openid,nickname")->where($where)->select();
		foreach($arrU as $val){
			$touser[] = $val["openid"];
		}
		$sendInfo = array(
			"touser" => $touser,
			"msgtype" => "text",
			"text" => array("content"=>$_REQUEST["content"]),
		);
		$result = $weObj->sendMassMessage($sendInfo);
		if($result["errmsg"] == "send job submission success"){

			$arrF = array (
			  'ToUserName' => "bulk",  //接收方帐号
			  'Description' => json_encode($touser),  //群发用户
			  'FromUserName' => $arrP["openid"],   //发送方帐号
			  'CreateTime' => date("Y-m-d H:i:s"),
			  'MsgType' => "text",  //群发
			  'Content' => $message,
			  'nickname' => $bulk_obj,
			  'sendType' => "my",
			  'answer_seats' => $_SESSION['user_info']['username'],   //应答坐席
			  'MsgId' => $result["msg_id"],
			);
			$this->weChatRecordCaChe($arrF);

			echo json_encode(array('success'=>true,'msg'=>'发送成功！'));
		}else{
			echo json_encode(array('msg'=>'发送失败！可能是您该月已经群发了4条信息！'));
		}
	}

	function bulkMedia(){
		//dump($_REQUEST);die;
		$filetype = $_REQUEST['filetype'];
		$filename = $_REQUEST['filename'];
		$bulk_obj = $_REQUEST['bulk_obj'];
		$id = $_REQUEST['id'];
		if($filetype == "image"){
			$path = "include/data/weChat/images/";
			$arrExt = array('jpg','jpeg','png');
		}elseif($filetype == "voice"){
			$path = "include/data/weChat/voice/";
			//$arrExt = array('AMR','amr','MP3','mp3');
			$arrExt = array('MP3','mp3');
		}
		$suffix = array_pop(explode(".",$filename));
		if( !in_array($suffix,$arrExt)){
			echo json_encode(array('msg'=>"上传文件类型不允许！"));
			die;
		}

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize ='1000000';
		$upload->savePath= $path;  //上传路径
		//dump($suffix);die;

		$upload->saveRule=uniqid;    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			echo json_encode(array('msg'=>$mess."<br>  只支持jpg、png格式的图片!"));
		}else{
			$info=$upload->getUploadFileInfo();
			$filename = $info[0]["savepath"].$info[0]["savename"];
			$result = $this->sendBulkMediaWeChat($filetype,$filename,$bulk_obj,$id);
			//dump($filename);die;
			if($filetype == "image"){
				$mess = $filename;
			}elseif($filetype == "voice"){
				$mess = $filename;
			}
			if($result){
				echo json_encode(array('success'=>true,'message'=>$mess,'time'=>date("Y-m-d H:i:s"),'msg'=>"发送成功！"));
			}else{
				echo json_encode(array('msg'=>"发送失败！不正确的文件格式！"));
			}
		}
	}

	function sendBulkMediaWeChat($msgtype,$filename,$bulk_obj,$id){
		header("Content-Type:text/html; charset=utf-8");
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		$wx_users = new Model("wx_users");
		if($bulk_obj == "checked"){
			$where = "id in ($id)";
		}else{
			$where = "1 ";
		}
		$arrU = $wx_users->field("id,openid,nickname")->where($where)->select();
		foreach($arrU as $val){
			$touser[] = $val["openid"];
		}

		if($msgtype == "image"){  //图片
			$Content = '<img src="'.$filename.'" onload="javascript:DrawImage(this,100,100);" class="bigImage" >';
			$file = realpath($filename); //要上传的文件
			$fields = array("media"=>'@'.$file);
			$arrData = $weObj->uploadMedia2($fields,$msgtype);
			$arrD = json_decode($arrData,true);
			$arr = array(
				"touser" => $touser,
				"msgtype" => $msgtype,
				"image"=>array("media_id"=>$arrD["media_id"]),
			);
		}elseif($msgtype == "voice"){    //语音
			$Content = $filename;
			$file = realpath($filename); //要上传的文件
			$fields = array("media"=>'@'.$file);
			$arrData = $weObj->uploadMedia2($fields,$msgtype);
			$arrD = json_decode($arrData,true);
			$arr = array(
				"touser" => $touser,
				"msgtype" => $msgtype,
				"voice"=>array("media_id"=>$arrD["media_id"]),
			);
		}
		//dump($arr);die;
		$result = $weObj->sendMassMessage($arr);
		if($result["errmsg"] == "send job submission success"){
			$arrF = array (
			  'ToUserName' => "bulk",  //接收方帐号
			  'Description' => json_encode($touser),  //群发用户
			  'FromUserName' => $arrP["openid"],    //发送方帐号
			  'CreateTime' => date("Y-m-d H:i:s"),
			  'MsgType' => $msgtype,
			  'Content' => $Content,
			  'nickname' => $bulk_obj,
			  'sendType' => "my",
			  'answer_seats' => $_SESSION['user_info']['username'],   //应答坐席
			  'MsgId' => $result["msg_id"],
			);
			$this->weChatRecordCaChe($arrF);
			return true;
		} else {
			return false;
		}
	}

	//根据群组id群发图文消息
	function massGraphicInfo(){
		header("Content-Type:text/html; charset=utf-8");
		$newsid = $_REQUEST["newsid"];
		//$openid = $_REQUEST["openid"];
		//$newsid = "4";
		//$openid = "oAVUOuFznCr5KVQJP-pwxb0JKQHk";
		$wx_news = new Model("wx_news");
		$arrData = $wx_news->where("id = '$newsid'")->find();
		$sendMsg = json_decode($arrData["sendMsg"],true);
		$ip = "http://".$_SERVER['SERVER_ADDR']."/";

		foreach($sendMsg as &$val){
			$arrM[] = $val;
			$val["url"] = $ip.$val["url"];
		}

		//dump($sendMsg);die;
		$arrP = getWeChatSet();
		import('ORG.Util.Wechat');
		$options = array(
			'token' => $arrP["token"], //填写你设定的key
			'appid' => $arrP["appid"], //填写高级调用功能的app id
			'appsecret' => $arrP["appsecret"], //填写高级调用功能的密钥
		);
		$weObj = new Wechat($options);
		$access_token = $weObj->checkAuth($arrP["appid"],$arrP["appsecret"]);

		foreach($sendMsg as &$val){
			$val["file"] = realpath($val["picurl"]);
			$fields = array("media"=>'@'.$val["file"]);
			$val["media"] = $weObj->uploadMedia2($fields,"thumb");  //缩略图
			//$val["media"] = $weObj->uploadMedia2($fields,"image");  //图片
			$val["media_id"] = json_decode($val["media"],true);
			$val["thumb_media_id"] = $val["media_id"]["thumb_media_id"];

			$arrMsg[] = array(
				"thumb_media_id" => $val["thumb_media_id"],    //缩略图
				//"thumb_media_id" => $val["media_id"]["media_id"],    //图片
				"author" => "",
				"title" => $val["title"],
				"content_source_url" => $val["url"],
				"content" => $val["description"],
				"digest" => $val["content"],
				"show_cover_pic" => "1",
			);
		}

		$news = array('articles'=>$arrMsg);
		$upNews = $weObj->uploadArticles($news);
		/*
		array(3) {
		  ["type"] => string(4) "news"
		  ["media_id"] => string(64) "EMRz70jfrStFayKOAztf8VE1rJsigmW4nKszVbB5iDvxvvpgF6DsuadeFMvovmrl"
		  ["created_at"] => int(1410859163)
		}
		*/
		$group = $weObj->getGroup();  //获取用户分组
		$arrG = array(
			"filter" => array("group_id"=>"0"),
			"mpnews" => array("media_id"=>$upNews["media_id"]),
			"msgtype" => "mpnews",
		);
		$sendGroup = $weObj->sendGroupMassMessage($arrG);
		/*
		array(3) {
		  ["errcode"] => int(0)
		  ["errmsg"] => string(27) "send job submission success"
		  ["msg_id"] => int(2348308690)
		}
		*/
		dump($sendGroup);
		dump($sendMsg);


		//dump($news);
		dump($arrData);die;

	}


}
?>
