<?php
class VoiceMailAction extends Action{
	/*语音信息*/
	function listVoiceMail(){
		checkLogin();
		$extension = $_SESSION["user_info"]["extension"];
		$archivos=array();
		if( $extension ){
			$path = "/var/spool/asterisk/voicemail/default";
			$folder = "INBOX";
			$voicemailPath = "$path/$extension/$folder/";
		}else{
			echo "分机不存在!";
		}

		$count = 0;
		$arrFile = array();
		if(file_exists($voicemailPath)) {
			if ($handle = opendir($voicemailPath)) {
				while (false !== ($file = readdir($handle) )) {
					if ($file!="." && $file!=".." && ereg("(.+)\.[txt|TXT]",$file,$regs))
					{
						$arrFile[] = $file;
						$count ++;
					}
				}
				closedir($handle);
			}
		}
		rsort($arrFile);
		//dump($arrFile);

		//分页
		import('ORG.Util.Page');
		$count = count($arrFile);

        $para_sys = readS();   //从缓存中读出系统参数
        $page = new Page($count,$para_sys["page_rows"]);
		$page->setConfig("header",L("Records"));
		$page->setConfig("prev",L("Prev"));
		$page->setConfig("next",L("Next"));
		$page->setConfig("first",L("First"));
		$page->setConfig("last",L("Last"));
		$page->setConfig('theme','<span>  %totalRow%%header% &nbsp;  %nowPage%/%totalPage%</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');
        $show = $page->show();
        $this->assign("page",$show);

		$arrData = array();
		$indexStart = $page->firstRow;
		$indexEnd = $indexStart + $page->listRows -1;
		if($indexEnd > $count -1 ){$indexEnd = $count -1;}//数组中最后一个
		//echo $indexStart;echo $indexEnd;die;
		for($i=$indexStart;$i<=$indexEnd;$i++){
			$file = $arrFile[$i];
			$arrTmp = explode(".",$file);
			$vmfile = $arrTmp['0'];
			$msgfile = $voicemailPath.$file;
			//echo $msgfile."<br>";
			$arrTmp = parse_ini_file($msgfile);
			$arrTmp['origtime'] = Date('Y-m-d H:i:s', $arrTmp['origtime']);
			$arrTmp['vmfile'] = $vmfile;
			$arrData[$vmfile] = $arrTmp;
		}

		$this->assign("Data",$arrData);
		$this->assign("vmbox",$extension);
		//dump($arrTmp);die;

		//分配增删改的权限
		$menuname = "VoiceMail";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("delete",$priv['delete']);
		$this->assign("Play",$priv['Play']);
		$this->assign("Download",$priv['Download']);

		$this->display();
	}

	function playVoiceMail(){
		checkLogin();
		$mvfile = $_GET['vmfile'];
		$mailbox = $_GET['mailbox'];
		$path = "/var/spool/asterisk/voicemail/default";
		$folder = "INBOX";
		$voicemailPath = "$path/$mailbox/$folder/";
		$msgfile = $voicemailPath.$mvfile.".txt";
		if(!file_exists($msgfile)){
			$this->error("File $msgfile is not exist!");
		}
		//echo $msgfile."<br>";
		$tpl_Msg = parse_ini_file($msgfile);
		$tpl_Msg['origtime'] = Date('Y-m-d H:i:s', $tpl_Msg['origtime']);

		$this->assign("Msg",$tpl_Msg);
		$this->assign("vmbox",$extension);

		$AudioURL = "index.php?m=VoiceMail&a=downloadVoiceMail&mailbox=$mailbox&vmfile=$mvfile";
		//echo urlencode($AudioURL);die;
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->display();
	}

	function downloadVoiceMail(){
		$mvfile = $_GET['vmfile'];
		$mailbox = $_GET['mailbox'];
		$path = "/var/spool/asterisk/voicemail/default";
		$folder = "INBOX";
		$voicemailPath = "$path/$mailbox/$folder/";
		$WAVfile = $voicemailPath.$mvfile.".wav";
		if(!file_exists($WAVfile)){
			$this->error("File $WAVfile is not exist!");
		}
        header('HTTP/1.1 200 OK');
        //header('Accept-Ranges: bytes');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        //header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        //header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($WAVfile))."");
		header("Content-Disposition: attachment;filename=" .str_replace(".wav",".mp3",basename($WAVfile)));
        //readfile($WAVfile);
		system('/usr/bin/lame -b 96 -t -F -m m --bitwidth 16 --quiet ' .$WAVfile ." -");
	}

	function deleteVoiceMail(){
		$mvfile = $_GET['vmfile'];
		$mailbox = $_GET['mailbox'];
		$path = "/var/spool/asterisk/voicemail/default";
		$folder = "INBOX";
		$voicemailPath = "$path/$mailbox/$folder/";

		$msgfile = $voicemailPath.$mvfile.".txt";
		$WAVfile = $voicemailPath.$mvfile.".WAV";
		//dump($msgfile);die;
		$return = false;
		if( file_exists( $msgfile ) || file_exists( $WAVfile ) ){
			$ret1 = unlink( $msgfile );
			$ret2 = unlink( $WAVfile);
			$return = $ret1 || $ret2;
		}dump($return);
		if( $return ){
			$this->success("Delete Successfully!","index.php?m=VoiceMail&a=listVoiceMail");
		}else{
			$this->error("Delete Field!");
		}
	}
}
?>

