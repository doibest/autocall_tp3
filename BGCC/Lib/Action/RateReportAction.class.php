<?php
class RateReportAction extends Action{
	function rateReportList(){
		checkLogin();

		$thisyear = date('Y');
		$currentYearMonth = date('Y-m');
		$yearmonth = Array(
			$thisyear .'-01',
			$thisyear .'-02',
			$thisyear .'-03',
			$thisyear .'-04',
			$thisyear .'-05',
			$thisyear .'-06',
			$thisyear .'-07',
			$thisyear .'-08',
			$thisyear .'-09',
			$thisyear .'-10',
			$thisyear .'-11',
			$thisyear .'-12',
		);
		$tpl_yearmonth = Array();
		for($i=0;$i<=12;$i++){
			$tpl_yearmonth[$i] = Array(
				'value' => $yearmonth[$i],
				'selected' => $yearmonth[$i]==$currentYearMonth?'selected="selected"':'',
				'display' => $yearmonth[$i],
			);
		}
		unset($i);
		$this->assign("yearmonth",$tpl_yearmonth);

		$month = date("Y-m");
		$this->assign("month",$month);
		//分配增删改的权限
		$menuname = "Rate Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function rateReportData(){
		$rate = new Model("rate");
		$createtime = $_REQUEST["createtime"];
		$dept_id = $_REQUEST["dept_id"];
		if($createtime){
			$year_month = $_REQUEST["createtime"];
		}else{
			$year_month = date("Y-m");
		}


		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arr = $_REQUEST;
		foreach($arr as $k=>$v){
			if($v && $k != "page"  && $k != "rows" && $k != "createtime"  && $k != "searchmethod" ){
				$arrS[$k] = $v;
			}
		}

		$arrDep = $this->getDepTreeArray();
		$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$searchDeptId = rtrim($deptSet,",");
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptId = rtrim($deptst,",");

		$where = empty($dept_id) ? "" : " AND c.dept_id IN ($searchDeptId)";
		if($username != "admin"){
			if(!$arrS){
				$where = " AND c.dept_id IN ($deptId)";
			}
		}
		$where .= " AND r.call_type='pbx'";
		$where .= " AND c.workno!='' AND c.workno is not null";

		$sql = "SELECT SUBSTR(DATE(c.calldate),1,7) as date,c.dept_id,c.workno,c.src,SUM(c.billsec) AS billsec,SUM(TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,2)) AS amoney FROM asteriskcdrdb.cdr c LEFT JOIN bgcrm.rate r ON (SUBSTR(c.dst,1,LENGTH(r.rate_prefix)) = r.rate_prefix) WHERE 1  AND r.rate_prefix IS NOT NULL AND LENGTH(dst)>4 AND r.rate_type=1  AND c.calldate LIKE '%$year_month%' $where GROUP BY SUBSTR(DATE(c.calldate),1,7),c.workno ORDER BY c.workno";

		$rateData = $rate->query($sql);
		//echo $rate->getLastSql();die;

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];
		$deptName = $userArr["deptId_name"];

		$i = 0;
		foreach($rateData as $vm){
			$rateData[$i]['cn_name'] = $cnName[$vm["workno"]];
			$rateData[$i]['dept_name'] = $deptName[$vm["dept_id"]];
			$money[] = $vm['amoney'];
			$i++;
		}

		$search_type = $_REQUEST["search_type"];
		if($search_type == "xls"){
			$arrField = array('workno','cn_name','dept_name','src','amoney');
			$arrTitle = array('工号','姓名','部门','分机号','通话费用(元)');
			$xls_count = count($arrField);
			$excelTiele = "话费账单".date("Y-m-d");
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$rateData,$excelTiele);
			die;
		}

		$tpl_money = array_sum($money);
		$count = count($rateData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$rateTplData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($rateData AS &$v){
			if($i >= $start && $j < $length){
				$rateTplData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}
		//dump($tpl_money);die;
		/*
		$i = 0;
		foreach($rateData as $val){
			$rateData[$i]["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));
			$i++;
		}
		unset($i);
		*/

		$en_amondy = L("Total cost of");
		$tmp_mon = array(array("amoney"=>$en_amondy.": ".$tpl_money));

		$rowsList = count($rateTplData) ? $rateTplData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["footer"] = $tmp_mon;

		echo json_encode($ary);
	}


	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }

	//外呼计费报表
	function outBoundRateReport(){
		$month = date("Y-m");
		$this->assign("month",$month);
		$this->display();
	}

	function outBoundRateReportTask(){
		$month = date("Y-m");
		$this->assign("month",$month);
		$task_id = $_REQUEST["task_id"];
		$this->assign("task_id",$task_id);
		$this->display();
	}

	function outBoundRateReportData(){
		$task_id = $_REQUEST["task_id"];
		$table = "sales_cdr_".$task_id;
		$sales_cdr = new Model("sales_cdr_".$task_id);

		$createtime = $_REQUEST["createtime"];
		if($createtime){
			$year_month = $_REQUEST["createtime"];
		}else{
			$year_month = date("Y-m");
		}

		$where = "1 ";
		$where .= " AND r.rate_prefix IS NOT NULL AND LENGTH(src)>4 AND r.rate_type=1  AND c.calldate LIKE '%$year_month%'";
		$where .= " AND r.call_type='outbound'";
		$where .= " AND c.disposition='ANSWERED'";
		$where .= " AND SUBSTRING_INDEX(SUBSTRING_INDEX(c.channel,'-',1),'/',-1) IS NOT NULL ";

		$arrT = $sales_cdr->order("c.calldate desc")->table("$table c")->field("DATE(c.calldate) as date,c.workno,c.src,c.dst,SUM(c.billsec) AS billsec,SUM(TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,2)) AS amoney")->join("rate r ON (SUBSTR(c.src,1,LENGTH(r.rate_prefix)) = r.rate_prefix AND SUBSTRING_INDEX(SUBSTRING_INDEX(c.channel,'-',1),'/',-1)= r.rate_trunk)")->where($where)->group("SUBSTR(DATE(c.calldate),1,7),c.workno")->select();
		$count = count($arrT);

		foreach($arrT as $val){
			$total_money[] = $val["amoney"];
		}
		$totalMoney = array_sum($total_money);
		//dump($totalMoney);
		//dump($arrT);die;
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		$arrData = $sales_cdr->order("c.calldate desc")->table("$table c")->field("DATE(c.calldate) as date,c.workno,c.src,c.dst,SUM(c.billsec) AS billsec,SUM(TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,2)) AS amoney")->join("rate r ON (SUBSTR(c.src,1,LENGTH(r.rate_prefix)) = r.rate_prefix AND  SUBSTRING_INDEX(SUBSTRING_INDEX(c.channel,'-',1),'/',-1)= r.rate_trunk)")->limit($page->firstRow.','.$page->listRows)->where($where)->group("SUBSTR(DATE(c.calldate),1,7),c.workno")->select();
		//echo $sales_cdr->getLastSql();
		//dump($arrData);die;

		$userArr = readU();
		$deptName = $userArr["deptId_name"];
		$deptUserName = $userArr["deptName_user"];
		$cn_user = $userArr["cn_user"];

		$i = 0;
		foreach($arrData as $val){
			$arrData[$i]["cn_name"] = $cn_user[$val["workno"]];
			$arrData[$i]["dept_name"] = $deptUserName[$val["workno"]];
			$arrData[$i]["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));
			$i++;
		}
		unset($i);


		$en_amondy = L("Total cost of");
		$tmp_mon = array(array("amoney"=>$en_amondy.": ".$totalMoney));

		$rowsList = count($arrData) ? $arrData : false;
		$arrR["total"] = $count;
		$arrR["rows"] = $rowsList;
		$arrR["footer"] = $tmp_mon;

		echo json_encode($arrR);
	}

}

?>
