<?php
class AutoTaskCallReportAction extends Action{
	function autoCallList(){
		checkLogin();
		$id = $_REQUEST["task_id"];
		$this->assign("id",$id);
		$month = date("Y-m");
		$this->assign("month",$month);
		$this->display();
	}

	function autoCallData(){
		$task_id = $_REQUEST["task_id"];
		$m = date("Y-m");
		$month = $_REQUEST["month"];
		if($month){
			$date = $month."-01";
			$now = $month."-31";
		}else{
			$date = date("Y-m")."-01";
			$now = date("Y-m-d");
		}
		$where = "DATE(calldate) >= '$date' AND DATE(calldate) <= '$now'";

		$task_cdr = new Model("sales_cdr_".$task_id);
		//$countData = $task_cdr->where($where)->group("DATE(calldate)")->select(); //->having($having)
		//$count = count($countData);
		//echo $task_cdr->getLastSql();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		//$cdrData = $task_cdr->field("DATE(calldate) as calldate,workno,SUM(billsec) as billsec,SUM(IF(disposition='ANSWERED' AND transferbillsec IS NOT NULL,`transferbillsec`,billsec)) AS duration,disposition,COUNT(*) AS answertotal,round(AVG(billsec)) AS averageduration,SUM(disposition='ANSWERED') as answeredCount,SUM(disposition='ANSWERED' OR disposition='PLAYIVR' OR disposition='NO ANSWER') as connectCount,SUM(IF(billsec>=1 AND billsec <=10,billsec,0)) as shortime,SUM(IF(billsec>10 AND billsec <=20,billsec,0)) as midtime,SUM(IF(billsec>20 ,billsec,0)) AS langtime")->limit($page->firstRow.','.$page->listRows)->where($where)->group("DATE(calldate)")->select();   //->having($having)
		$cdrData = $task_cdr->field("DATE(calldate) as calldate,workno,SUM(billsec) as billsec,SUM(IF(disposition='ANSWERED' AND transferbillsec IS NOT NULL,`transferbillsec`,billsec)) AS duration,disposition,COUNT(*) AS answertotal,round(AVG(billsec)) AS averageduration,SUM(disposition='ANSWERED') as answeredCount,SUM(disposition='ANSWERED' OR disposition='PLAYIVR' OR disposition='NO ANSWER') as connectCount,SUM(CASE WHEN billsec>=1 AND billsec <=10 AND disposition='ANSWERED' THEN 1 ELSE 0 END) as shortime,SUM(CASE WHEN billsec>10 AND billsec <=20 AND disposition='ANSWERED' THEN 1 ELSE 0 END) as midtime,SUM(CASE WHEN billsec>20 AND disposition='ANSWERED' THEN 1 ELSE 0 END) as langtime")->where($where)->group("DATE(calldate)")->select();   //->having($having)
		//->limit($page->firstRow.','.$page->listRows)
		//echo $task_cdr->getLastSql();

		foreach($cdrData as &$val){
			$val['throughRate'] = substr($val["answeredCount"]/$val["answertotal"]*100,0,5);
			$val['connectRate'] = substr($val["connectCount"]/$val["answertotal"]*100,0,5);
			$val["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));

			$val["averageduration"] = sprintf("%02d",intval($val["averageduration"]/3600)).":".sprintf("%02d",intval(($val["averageduration"]%3600)/60)).":".sprintf("%02d",intval((($val[averageduration]%3600)%60)));

			$val["duration"] = sprintf("%02d",intval($val["duration"]/3600)).":".sprintf("%02d",intval(($val["duration"]%3600)/60)).":".sprintf("%02d",intval((($val[duration]%3600)%60)));

			/*
			$val["shortime"] = sprintf("%02d",intval($val["shortime"]/3600)).":".sprintf("%02d",intval(($val["shortime"]%3600)/60)).":".sprintf("%02d",intval((($val[shortime]%3600)%60)));

			$val["midtime"] = sprintf("%02d",intval($val["midtime"]/3600)).":".sprintf("%02d",intval(($val["midtime"]%3600)/60)).":".sprintf("%02d",intval((($val[midtime]%3600)%60)));

			$val["langtime"] = sprintf("%02d",intval($val["langtime"]/3600)).":".sprintf("%02d",intval(($val["langtime"]%3600)/60)).":".sprintf("%02d",intval((($val[langtime]%3600)%60)));
			*/

			$arrData[$val["calldate"]] = $val;
		}

		$where1 = "DATE(dealtime) >= '$date' AND DATE(dealtime) <= '$now'";
		$history = new Model("sales_contact_history_".$task_id);
		$arrHis = $history->field("DATE(dealtime) as calldate,SUM(CASE WHEN dealresult=0 THEN 1 ELSE 0 END) AS untreated,SUM(CASE WHEN dealresult=1 THEN 1 ELSE 0 END) AS visit,SUM(CASE WHEN dealresult=2 THEN 1 ELSE 0 END) AS failure,SUM(CASE WHEN dealresult=3 THEN 1 ELSE 0 END) AS success")->group("DATE(dealtime)")->where($where1)->select();
		foreach($arrHis as $key=>&$val){
			$arrF[$val["calldate"]] = $val;
		}


		foreach($arrData as $key=>$val){
			foreach($val as $k=>&$v) {
			  $arrF[$key][$k] = $v;
			}
		}

		foreach($arrF as &$val){
			$val["time"] = strtotime($val["calldate"]);
			$arrT[] = $val;
		}
		$count = count($arrT);
		$arrTD = $this->array_sort($arrT,"time","asc","");
		//dump($arrT);
		//dump($arrF);die;

		$rowsList = count($arrTD) ? $arrTD : false;
		$arrCdr["total"] = $count;
		$arrCdr["rows"] = $rowsList;

		echo json_encode($arrCdr);
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	function exporTaskExcel(){
		$task_id = $_REQUEST["task_id"];
		$m = date("Y-m");
		$month = $_REQUEST["month"];
		if($month){
			$date = $month."-01";
			$now = $month."-31";
		}else{
			$date = date("Y-m")."-01";
			$now = date("Y-m-d");
		}
		$where = "DATE(calldate) >= '$date' AND DATE(calldate) <= '$now'";

		$task_cdr = new Model("sales_cdr_".$task_id);
		$countData = $task_cdr->where($where)->group("DATE(calldate)")->select(); //->having($having)
		$count = count($countData);

		$cdrData = $task_cdr->field("DATE(calldate) as calldate,workno,SUM(billsec) as billsec,SUM(IF(disposition='ANSWERED' AND transferbillsec IS NOT NULL,`transferbillsec`,billsec)) AS duration,disposition,COUNT(*) AS answertotal,round(AVG(billsec)) AS averageduration,SUM(disposition='ANSWERED') as answeredCount,SUM(disposition='ANSWERED' OR disposition='PLAYIVR' OR disposition='NO ANSWER') as connectCount,SUM(IF(billsec>=1 AND billsec <=10,billsec,0)) as shortime,SUM(IF(billsec>10 AND billsec <=20,billsec,0)) as midtime,SUM(IF(billsec>20 ,billsec,0)) AS langtime")->where($where)->group("DATE(calldate)")->select();   //->having($having)

		$i = 0;
		foreach($cdrData as &$val){
			$cdrData[$i]['throughRate'] = substr($val["answeredCount"]/$val["answertotal"]*100,0,5)."%";
			$cdrData[$i]['connectRate'] = substr($val["connectCount"]/$val["answertotal"]*100,0,5)."%";
			$val["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));

			$val["duration"] = sprintf("%02d",intval($val["duration"]/3600)).":".sprintf("%02d",intval(($val["duration"]%3600)/60)).":".sprintf("%02d",intval((($val[duration]%3600)%60)));

			$val["averageduration"] = sprintf("%02d",intval($val["averageduration"]/3600)).":".sprintf("%02d",intval(($val["averageduration"]%3600)/60)).":".sprintf("%02d",intval((($val[averageduration]%3600)%60)));

			$val["shortime"] = sprintf("%02d",intval($val["shortime"]/3600)).":".sprintf("%02d",intval(($val["shortime"]%3600)/60)).":".sprintf("%02d",intval((($val[shortime]%3600)%60)));

			$val["midtime"] = sprintf("%02d",intval($val["midtime"]/3600)).":".sprintf("%02d",intval(($val["midtime"]%3600)/60)).":".sprintf("%02d",intval((($val[midtime]%3600)%60)));

			$val["langtime"] = sprintf("%02d",intval($val["langtime"]/3600)).":".sprintf("%02d",intval(($val["langtime"]%3600)/60)).":".sprintf("%02d",intval((($val[langtime]%3600)%60)));

			$i++;
		}


		vendor("PHPExcel176.PHPExcel");
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置行高度
		//$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(22);
		//$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);

		//设置字体大小加粗
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		//设置水平居中
		for($j='A';$j<='L';$j++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(20);  //设置单元格（列）的宽度
			$objPHPExcel->getActiveSheet()->getStyle($j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//合并单元格
		//$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');

		//设置表 标题内容
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue('A1', '通话日期')
			->setCellValue('B1', '呼叫数量')
			->setCellValue('C1', '接通量')
			->setCellValue('D1', '有效通话次数')
			->setCellValue('E1', '接通率')
			->setCellValue('F1', '人工转接率')
			->setCellValue('G1', '接听总时长')
			->setCellValue('H1', '转人工时长')
			->setCellValue('I1', '平均接听时长')
			->setCellValue('J1', '1~10秒')
			->setCellValue('K1', '11~20秒')
			->setCellValue('L1', '大于20秒');
		$i = 2;
		foreach($cdrData as $val){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $val['calldate']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $val['answertotal']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $val['connectCount']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val['answeredCount']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val['connectRate']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val['throughRate']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['billsec']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val['duration']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val['averageduration']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val['shortime']);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $val['midtime']);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $val['langtime']);
			$i++;
		}
		// Rename sheet
		//$objPHPExcel->getActiveSheet()->setTitle('外乎报表');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$name = "外乎报表";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

	}

	function exporTaskExcel2(){
		$task_id = $_REQUEST["task_id"];
		$sales_task = new Model("sales_task");
		$taskData = $sales_task->where("id = $task_id")->find();

		$m = date("Y-m");
		$month = $_REQUEST["month"];
		if($month){
			$date = $month."-01";
			$now = $month."-31";
		}else{
			$date = date("Y-m")."-01";
			$now = date("Y-m-d");
		}
		$where = "DATE(calldate) >= '$date' AND DATE(calldate) <= '$now'";

		$task_cdr = new Model("sales_cdr_".$task_id);
		$countData = $task_cdr->where($where)->group("DATE(calldate)")->select(); //->having($having)
		$count = count($countData);

		$cdrData = $task_cdr->field("DATE(calldate) as calldate,workno,SUM(billsec) as billsec,SUM(IF(disposition='ANSWERED' AND transferbillsec IS NOT NULL,`transferbillsec`,billsec)) AS duration,disposition,COUNT(*) AS answertotal,round(AVG(billsec)) AS averageduration,SUM(disposition='ANSWERED') as answeredCount,SUM(disposition='ANSWERED' OR disposition='PLAYIVR' OR disposition='NO ANSWER') as connectCount,SUM(IF(billsec>=1 AND billsec <=10,billsec,0)) as shortime,SUM(IF(billsec>10 AND billsec <=20,billsec,0)) as midtime,SUM(IF(billsec>20 ,billsec,0)) AS langtime")->where($where)->group("DATE(calldate)")->select();   //->having($having)

		$i = 0;
		foreach($cdrData as &$val){
			$cdrData[$i]['throughRate'] = substr($val["answeredCount"]/$val["answertotal"]*100,0,5)."%";
			$cdrData[$i]['connectRate'] = substr($val["connectCount"]/$val["answertotal"]*100,0,5)."%";
			$val["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));

			$val["duration"] = sprintf("%02d",intval($val["duration"]/3600)).":".sprintf("%02d",intval(($val["duration"]%3600)/60)).":".sprintf("%02d",intval((($val[duration]%3600)%60)));

			$val["averageduration"] = sprintf("%02d",intval($val["averageduration"]/3600)).":".sprintf("%02d",intval(($val["averageduration"]%3600)/60)).":".sprintf("%02d",intval((($val[averageduration]%3600)%60)));

			$val["shortime"] = sprintf("%02d",intval($val["shortime"]/3600)).":".sprintf("%02d",intval(($val["shortime"]%3600)/60)).":".sprintf("%02d",intval((($val[shortime]%3600)%60)));

			$val["midtime"] = sprintf("%02d",intval($val["midtime"]/3600)).":".sprintf("%02d",intval(($val["midtime"]%3600)/60)).":".sprintf("%02d",intval((($val[midtime]%3600)%60)));

			$val["langtime"] = sprintf("%02d",intval($val["langtime"]/3600)).":".sprintf("%02d",intval(($val["langtime"]%3600)/60)).":".sprintf("%02d",intval((($val[langtime]%3600)%60)));

			$i++;
		}


		vendor("PHPExcel176.PHPExcel");
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置行高度
		//$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(22);
		//$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);

		//设置字体大小加粗
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(12);
		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		//设置水平居中
		for($j='A';$j<='L';$j++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(20);  //设置单元格（列）的宽度
			$objPHPExcel->getActiveSheet()->getStyle($j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//合并单元格
		for($n='A';$n<='I';$n++){
			$objPHPExcel->getActiveSheet()->mergeCells($n."1:".$n."2");
		}
		$objPHPExcel->getActiveSheet()->mergeCells("J1:L1");
		//$objPHPExcel->getActiveSheet()->mergeCells("B1:B2");

		//设置表 标题内容
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue('A1', '通话日期')
			->setCellValue('B1', '呼叫数量')
			->setCellValue('C1', '接通量')
			->setCellValue('D1', '有效通话次数')
			->setCellValue('E1', '接通率')
			->setCellValue('F1', '人工转接率')
			->setCellValue('G1', '接听总时长')
			->setCellValue('H1', '转人工时长')
			->setCellValue('I1', '平均接听时长')
			->setCellValue('J1', '接听时长')
			->setCellValue('J2', '1~10秒')
			->setCellValue('K2', '11~20秒')
			->setCellValue('L2', '大于20秒');
		$i = 3;
		foreach($cdrData as $val){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $val['calldate']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $val['answertotal']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $val['connectCount']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val['answeredCount']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val['connectRate']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val['throughRate']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['billsec']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val['duration']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val['averageduration']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val['shortime']);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $val['midtime']);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $val['langtime']);
			$i++;
		}
		// Rename sheet
		//$objPHPExcel->getActiveSheet()->setTitle('外乎报表');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$name = $taskData["name"]." 任务的外乎报表";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');

	}

	//外呼坐席报表
	function agentReportList(){
		checkLogin();
		$task_id = empty($_REQUEST["task_id"]) ? "Y" : $_REQUEST["task_id"];
		$this->assign("task_id",$task_id);
		$this->display();
	}

	function agentReportData(){
		$username = $_SESSION['user_info']['username'];
		$task_id = $_REQUEST["task_id"];
		$startime = $_REQUEST["startime"];
		$endtime = $_REQUEST["endtime"];

		$where = "workno is not null AND workno != ''";
		$where .= empty($startime) ? "" : " AND calldate >= '$startime'";
		$where .= empty($endtime) ? "" : " AND calldate <= '$endtime'";


		$task_cdr = new Model("sales_cdr_".$task_id);
		$countData = $task_cdr->where($where)->group("workno")->select(); //->having($having)
		$count = count($countData);
		//echo $task_cdr->getLastSql();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$cdrData = $task_cdr->field("workno,dst,SUM(billsec) as billsec,SUM(IF(disposition='ANSWERED' AND transferbillsec IS NOT NULL,`transferbillsec`,billsec)) AS duration,disposition,COUNT(*) AS answertotal,round(AVG(billsec)) AS averageduration,SUM(disposition='ANSWERED') as answeredCount,SUM(disposition='ANSWERED' OR disposition='PLAYIVR' OR disposition='NO ANSWER') as connectCount")->limit($page->firstRow.','.$page->listRows)->where($where)->group("workno")->select();
		//echo $task_cdr->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];


		foreach($cdrData as &$val){
			$val['throughRate'] = substr($val["answeredCount"]/$val["answertotal"]*100,0,5);
			$val['connectRate'] = substr($val["connectCount"]/$val["answertotal"]*100,0,5);
			$val["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));

			$val["averageduration"] = sprintf("%02d",intval($val["averageduration"]/3600)).":".sprintf("%02d",intval(($val["averageduration"]%3600)/60)).":".sprintf("%02d",intval((($val[averageduration]%3600)%60)));

			$val["duration"] = sprintf("%02d",intval($val["duration"]/3600)).":".sprintf("%02d",intval(($val["duration"]%3600)/60)).":".sprintf("%02d",intval((($val[duration]%3600)%60)));

			$val["cn_name"] = $cnName[$val["workno"]];
		}

		$rowsList = count($cdrData) ? $cdrData : false;
		$arrCdr["total"] = $count;
		$arrCdr["rows"] = $rowsList;

		echo json_encode($arrCdr);
	}


}
?>
