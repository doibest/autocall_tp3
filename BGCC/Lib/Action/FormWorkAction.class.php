<?php
class FormWorkAction extends Action{
    function basicSet(){
		checkLogin();
        $crmWT = new CrmWorkTablesModel();
        $count = $crmWT->count();
        $crmList = $crmWT->select();
        //dump($crmList);die;
        $this->assign('crmlist',$crmList);
        $this->display();
    }

    function insertBasic(){

        $crmWT = new CrmWorkTablesModel();
        $crmList = $crmWT->select();
        $this->assign('crmlist',$crmList);

        if($_POST['name']) {
            $index_id = $_POST["master_table"] != "crm_customer" ? $_POST["table_name"]."_id" : "customer_id";
            $sqlstr = !empty($_POST["master_table"]) ?  ",`". $index_id ."` INT( 10 ) NOT NULL " : "";
            $sqlindex = !empty($_POST["master_table"]) ? ",INDEX ( `". $index_id ."` )" :"";
            $sql = "CREATE TABLE `". $_POST["table_name"] ."` (
                    `". $_POST["table_name"] ."_id` INT( 10 ) NOT NULL AUTO_INCREMENT,
                     `uniqueid` char( 32 )
                    ". $sqlstr .",
                    PRIMARY KEY ( `". $_POST["table_name"] ."_id` )
                     ". $sqlindex ."
                    ) CHARACTER SET = utf8";
            $crmresult = $crmWT->query($sql);

            $arrData = Array(
              'work_name' => $_POST['name'],
                'table_name' => $_POST['table_name'],
                'table_type' => $_POST['table_type'],
                'master_table' => $_POST['master_table']
            );
            $crmresult = $crmWT->add($arrData);
            //dump($crmWT->getLastSql());

            //$crmresult = $crmWT->query($sql);
            //var_dump($crmresult);
            if($crmresult){
                //$this->success("添加成功");
                echo "添加成功";
                $this->workSetCache();
            }else{
                //$this->error('操作失败：'.$crmWT->getDbError());
                echo ('操作失败：'.$crmWT->getDbError());
            }
        }
    }

    //设置缓存
    function workSetCache(){
        $crmWT = new CrmWorkTablesModel();
        $tmp = $crmWT->select();
        F('workSet',$tmp,"BgCC/Conf/");
    }
    function addFields(){
		checkLogin();
        $tableName = $_GET["table_name"];
        //dump($tableName);die;
        $this->assign("table_name",$tableName);
        $this->display();
    }

    //字段增加保存
    function insertFields(){
        header("Content-Type:text/html; charset=utf-8");
        $crmFields = new CrmTablesFieldsModel();

        if($_POST['fldLabel']) {
            switch($_POST['type']){
                case 'Text':
                        $fieldtype = "varchar";
                        $fieldlen = "(200)";
                        break;
                case 'Number':
                        $fieldtype = "int";
                        $fieldlen = "(10)";
                        break;
                case 'Email':
                        $fieldtype = "varchar";
                        $fieldlen = "(100)";
                        break;
                case 'Phone':
                        $fieldtype = "varchar";
                        $fieldlen = "(15)";
                        break;
                case 'Picklist':
                        $fieldtype = "int";
                        $fieldlen = "(5)";
                        break;
                case 'files':
                        $fieldtype = "varchar";
                        $fieldlen = "(200)";
                        break;
            }

            $table_name = $_POST["table_name"] ? $_POST["table_name"] : "crm_test" ;
            //dump($table_name);die;
           $sql = "ALTER TABLE $table_name ADD `". $_POST["fldname"] ."` ". $fieldtype ."  ".$fieldlen." " ;
           $crmFTRs = $crmFields->query($sql);
            //echo $crmFields->getLastSql();die;
            $arrCrmData = array(
                "fields_name" => $_POST["fldLabel"],
                "fields_ename" => $_POST["fldname"],
                //"fields_len" => $_POST[""],
                "isbase" => $_POST["isbase"],
                "type" => $_POST["type"],
                "noempty" => $_POST["noempty"],
                "table_name" => $table_name
            );
            $crmFTRs = $crmFields->add($arrCrmData);

            //echo $crmFields->getLastSql();die;
            if($crmFTRs){
                echo "添加成功";
                $this->workSetCache();
            }else{
                echo ('操作失败：'.$crmFields->getDbError());
            }
        }
    }

    //工单字段列表
    function fieldsList(){
		checkLogin();
        import("ORG.Util.Page");
        $crmFields = new CrmTablesFieldsModel();
        //取表单值：1）post过来，非查询的值  2)实实在在查询，通get传过来。
        $table_name = isset($_REQUEST['table_name'])?$_REQUEST['table_name']:"crm_test";
        F('fields',$table_name,"BgCC/Conf/");
        $work_name = $_REQUEST['work_name'];
        //设置SQL查询条件
        $where = "1";
        if($table_name) $where.= " AND table_name='$table_name' ";
        //初始化分页对象
        $count = $crmFields->where($where)->count();
        $para_sys = readS();   //从缓存中读出系统参数
        $page = new Page($count,$para_sys["page_rows"]);
        $page->setConfig('theme','<span>共%totalRow%%header% &nbsp;  %nowPage%/%totalPage%页</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');

         //给分页类传入参数，给分页类的parameter属性赋值：
        $page->parameter = "&";
        if( $table_name ){$page->parameter .= "table_name=". urlencode($table_name) ."&";}
        $show= $page->show();
        $this->assign('page',$show);

        $crmFTRs = $crmFields->order('fields_order')->where($where)->select();
       // F('fields',$crmFTRs,"BgCC/Conf/");
        //echo $crmFields->getLastSql();die;
        //dump($crmFTRs);die;
        $this->assign('fields',$crmFTRs);


        $crmWT = new CrmWorkTablesModel();
        $crmList = $crmWT->select();
        //dump($crmList);die;
        $this->assign('crmlist',$crmList);
        $this->assign('table_name',$table_name);
        $this->assign('work_name',$work_name);

        $this->display();
    }

    //获取要 编辑工单字段列表  的数据
    function editFieldsList(){
		checkLogin();
        $tpl_fid = $_GET['fid'];
        $tableName = $_GET['table_name'];
        //dump($_GET);die;
        if(!empty($tpl_fid)){
            $this->assign('fid',$tpl_fid);
            $this->assign('fid',$tpl_fid);
            $crmFields = new CrmTablesFieldsModel();
            $data = $crmFields->where("fid=$tpl_fid")->find();
            //echo $crmFields->getLastSql();die;
            //dump($data);die;
            if($data){
                $this->assign('crmdata',$data);
                $this->display();
            }else{
                exit('编辑参数不存在！');
            }
        }else{
            exit('编辑项不存在！');
        }
    }

    //更新工单字段列表
    function updateFieldsList(){
        $id = $_POST['fid'];
        $tableName = $_GET['table_name'];
        //dump($_POST);die;
        $crmFields = new CrmTablesFieldsModel();

        $table_name = $_POST["table_name"] ? $_POST["table_name"] : "crm_test" ;
        $this->assign('table_name',$table_name);
        $getField = $crmFields->field('fields_ename,type')->where("fid=$id")->find();
        $field_old = $getField['fields_ename'];
        $field_new = $_POST['fields_ename'];
        //dump($field_new);die;
        $type = $getField['type'];
        //dump($type);die;
        switch($type){
            case 'Text':
                $fieldtype = "varchar";
                $fieldlen = "(200)";
                break;
            case 'Number':
                $fieldtype = "int";
                $fieldlen = "(10)";
                break;
            case 'Email':
                $fieldtype = "varchar";
                $fieldlen = "(100)";
                break;
            case 'Phone':
                $fieldtype = "varchar";
                $fieldlen = "(15)";
                break;
            case 'Picklist':
                $fieldtype = "int";
                $fieldlen = "(5)";
                break;
            case 'files':
                $fieldtype = "varchar";
                $fieldlen = "(200)";
                break;
        }
        //dump($field_old);die;
        $sql ="ALTER TABLE $table_name CHANGE $field_old $field_new $fieldtype $fieldlen NULL";
        $updField = $crmFields->query($sql);
        echo $crmFields->getLastSql();die;

        $arrData = array(
            'fields_name' => $_POST['fields_name'],
            'fields_ename' => $_POST['fields_ename'],
            'isbase' => $_POST['isbase'],
            'noempty' => $_POST['noempty'],
            'listview' => $_POST['listview']
        );
        $crmRes = $crmFields->where("fid=$id")->save($arrData);
        //echo $crmFields->getLastSql();die;
        if($crmRes){
            echo  "数据更新成功";
        }else{
            echo ('数据更新失败！');
        }
    }

    //删除工单字段
    function deleteFieldsList(){
       // dump($_POST);die;
        $del_ary = $_POST['del_ary'];
        $ary_del = explode(",",$del_ary);
        $crmFields = new CrmTablesFieldsModel();

        $tableList  = readWork();
        $this->assign('table_list',$tableList);
        foreach($tableList as $val){
            $tableName[] = $val['table_name'];
        }
        $table_name = $_GET["table_name"] ? $_GET["table_name"] : $tableName ;
        $this->assign('table_name',$table_name);
        $getField = $crmFields->field('fields_ename')->where("fid=$del_ary")->find();
        $field = $getField['fields_ename'];
        $sql ="ALTER TABLE $table_name[9] DROP COLUMN $field";
        $delField = $crmFields->query($sql);
        //echo $crmFields->getLastSql();die;



        $number = $crmFields->where("fid in ($del_ary)")->delete();
        if($number){
            echo "删除成功";
        }else{
            echo '操作失败：'.$crmFields->getDbError();
        }
    }
}
?>
