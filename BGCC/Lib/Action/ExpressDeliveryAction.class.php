<?php
/*
快递100接口推送
*/
class ExpressDeliveryAction extends Action{
	/*将物流号推送到快递100
	 *logistics_account：物流号， express_id：配送方式
	*/
	function pushExpress($logistics_account,$express_id){
		$para_sys = readS();
		$post_data = array();
		$post_data["schema"] = 'json' ;
		//$callbackurl="http://60.174.207.179:38180/index.php?m=ExpressDelivery&a=expressCallBack";
		$callbackurl="http://".$para_sys["public_network_ip"]."/index.php?m=ExpressDelivery&a=expressCallBack";

		$logistics_mode = M("logistics_mode");
		$arrM = $logistics_mode->select();
		foreach($arrM as $val){
			$row_shopping_name[$val["id"]] = $val["com_code"];
		}

		//你那边如果不确定出发地，目的地可以不填写，一旦填写了解析系统会以你填写的权重会很高"from":"广东上海", "to":"北京丰台"
		$company_name = $row_shopping_name[$express_id];
		$post_data["param"] = '{"company":"'. $company_name .'", "number":"'. $logistics_account .'","from":"", "to":"", "key":"'.$para_sys["express_key"].'", "parameters":{"callbackurl":"'. $callbackurl .'"}}';
		$url='http://www.kuaidi100.com/poll';
		$o="";
		foreach ($post_data as $k=>$v){
			$o.= "$k=".urlencode($v)."&";
		}

		$post_data=substr($o,0,-1);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		//dump($result);
		return $result;
	}

	function subscribeTo(){
		$order_info = M("order_info");
		//$arrData = $order_info->field("id,shopping_name,logistics_account")->where("logistics_state='4' AND (sign_back_number < '2' OR (TIMESTAMPDIFF(DAY,logistics_time,DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:%s')) > 2 AND TIMESTAMPDIFF(DAY,logistics_time,DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:%s')) <= 5))")->select();

		$id = $_REQUEST["id"];

		$where = "1 ";
		if($id){
			$where .= " AND id in ($id)";
		}else{
			$where .= "logistics_state='4' ";   //退签的
			$where .= " AND TIMESTAMPDIFF(DAY,logistics_time,DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:%s')) >= 2 AND TIMESTAMPDIFF(DAY,logistics_time,DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:%s')) < 5";
		}
		$arrData = $order_info->field("id,shopping_name,logistics_account")->where($where)->select();
		//echo $order_info->getLastSql();

		foreach($arrData as $val){
			//$arrF[] = $val["logistics_account"];
			$arrF[] = $this->pushExpress($val["logistics_account"],$val["shopping_name"]);
		}
		foreach($arrF as $vm){
			$row[] = json_decode($vm,true);
		}
		$success = $failure = 0;
		$failure_msg = "";
		foreach($row as $val){
			if($val["returnCode"] == "200"){
				$success++;
			}else{
				$failure++;
				$failure_msg = $val["returnCode"];
			}
		}
		//dump($row);die;
		$msg = "重新订阅成功 $success 条，失败： $failure 条，失败码： ".$failure_msg;
		echo json_encode(array('success'=>true,'msg'=>$msg));
		//dump($arrF);die;
	}

	function test(){
		header("Content-Type:text/html; charset=utf-8");
		$order_info = M("order_info");
		$arrData = $order_info->field("id,shopping_name,logistics_account")->where("logistics_account != '' AND logistics_account is not null AND shopping_name!='' and shipping_status='1'")->select();
		//echo $order_info->getLastSql();

		foreach($arrData as $val){
			$arrF[] = $this->pushExpress($val["logistics_account"],$val["shopping_name"]);
		}
		//$this->pushExpress("201998704635","1");
		dump($arrF);
		dump($arrData);
		die;
	}

	//快递100回调方法
	function expressCallBack(){
		$param=$_POST['param'];
		$arr = json_decode($param,'assoc');
		//echo $_POST['param'];
		//dump($arrData);die;
		$arrData = $arr["lastResult"];
		try{
			$logisticsAccount = $arrData["nu"];

			$state = $arrData["state"];
			$arrInfo = $arrData["data"];
			foreach($arrInfo as $val){
				//$arrF[] = "时间: ".$val["time"]." &nbsp;&nbsp;&nbsp;&nbsp;地点和跟踪进度:".$val["context"];
				$arrF[] = $val["time"]." &nbsp;&nbsp;|&nbsp;&nbsp;".$val["context"];
			}
			$info = implode("<br>",$arrF);
			//dump($arrData);die;
			if($info){
				$result = $this->updateOrderInfo($logisticsAccount,$state,$info);
				//echo $result;die;
				if ($result !== false){
					echo  '{"result":"true",	"returnCode":"200","message":"成功"}';
				} else {
					echo  '{"result":"false",	"returnCode":"500","message":"失败.$result"}';
				}
			} else {
				echo  '{"result":"false",	"returnCode":"500","message":"失败"}';
			}

		}catch(Exception $e){
			echo  '{"result":"false",	"returnCode":"500","message":"失败"}';
	    }
	}

	//改变订单信息
	function updateOrderInfo($logistics_account,$state,$info){
		$order_info = M("order_info");
		$arrF = $order_info->field("id,order_num,createname,logistics_account,cope_money,stored_values,customer_id,shopping_name,sign_back_number")->where("logistics_account = '$logistics_account'")->find();

		//推送坐席
		$row_logistics_state = array("0"=>"在途","1"=>"揽件","2"=>"疑难","3"=>"签收","4"=>"退签","5"=>"派件","6"=>"退回","7"=>"转投");
		$arrPustData = array(
			"type"=>"order",
			"order_num"=>$arrF["order_num"],
			"push_msg"=>$row_logistics_state[$state],
			"content"=>"您订单号为：".$arrF["order_num"]." 物流状态：".$row_logistics_state[$state]
		);
		$arrTRy[0] = $arrPustData;
		//echo "http://localhost/msgInterface/pub?user=U".$arrF["createname"].json_encode($arrTRy);die;

		$customer = M(customer);
		$customer_id = $arrF["customer_id"];
		$arrD = array(
			"logistics_state"=>$state,
			"logistics_Info"=>$info,
			"view_enable"=>"N",  //订单状态更变提醒
			"logistics_time"=>date("Y-m-d H:i:s"),  //物流推送时间
		);
		if($state == "3"){
			$arrD["order_status"] = "1";  //已确认
			$arrD["shipping_status"] = "2";  //收货确认
			$arrD["receipttime"] = date("Y-m-d H:i:s");

			//付款
			$arrD["pay_status"] = "Y";
			$arrD["payment"] = "3";     //付款方式--货到付款
			$arrD["paytime"] = date("Y-m-d H:i:s");

			//当签收的时候，将预存减去这个订单的预存，如减去后还有预存，则将预存是否冻结的状态改成否
			$arrC = $customer->field("id,stored_value,stored_frozen")->find();
			$stored_value = $arrC["stored_value"]-$arrF["stored_values"];
			if($stored_value > 0){
				$stored_frozen = "N";   //不冻结
			}else{
				$stored_frozen = "Y";   //冻结
			}
			$arrCD = array(
				"stored_value"=>$stored_value,
				"stored_frozen"=>$stored_frozen
			);

			$res = $customer->data($arrCD)->where("id = '$customer_id'")->save();
			//return $customer->getLastSql();die;
		}elseif($state == "4"){
			//state == "4":货物退回发货人并签收  state == "6":货物正处于返回发货人的途中
			$arrD["order_status"] = "4";  //退货
			$arrD["pay_status"] = "N";  //未付款
			$arrD["shipping_status"] = "0";  //未发货
			$arrD["canceltime"] = date("Y-m-d H:i:s"); //退货时间
			$arrD["description4"] = "快递100接口推送改变状态";  //操作备注
			$arrD["refund4"] = "";  //退款方式
			$arrD["explanation4"] = "快递100接口推送改变状态";  //退款说明

			$arrD["sign_back_number"] = $arrF["sign_back_number"]+1;  //退签次数


			//第一次退签
			if($arrF["sign_back_number"] == '0'){
				//当拒收是，积分减去这个订单的积分，
				$arrC = $customer->field("id,stored_value,stored_frozen,integral")->find();
				$integral = $arrC["integral"]-$arrF["cope_money"];
				$arrCD = array(
					"integral"=>$integral
				);
				$res = $customer->data($arrCD)->where("id = '$customer_id'")->save();

				//退签的要重新订阅一次
				$express_id = $arrF["shopping_name"];
				$this->pushExpress($logistics_account,$express_id);
			}
		}else{

		}

		$result = $order_info->data($arrD)->where("logistics_account = '$logistics_account'")->save();

		if ($result !== false){
			if($state == "3" || $state == "4"){
				//积分--如果有快递100推送的就改成在签收之后，否者就确定订单之后
				$this->saveIntegral($customer_id);
			}
			$this->phpPush(json_encode($arrTRy),$arrF["createname"]);
			return true;
		} else {
			return false;
		}

	}

	//用户积分
	function saveIntegral($customer_id){
		//$customer_id = "48";
		$order_info = new Model("order_info");
		$arrF = $order_info->field("sum(goods_amount) as goods_amount,id")->where("customer_id = '$customer_id' AND logistics_state = '3'")->find();
		$integral = $arrF["goods_amount"];
		if(!$integral || $integral == "0.00"){
			$integral = "0";
		}
		//echo $order_info->getLastSql();

		$membership_level = M("membership_level");
		$arrML = $membership_level->where(" start_num <= '$integral' AND end_num >= '$integral'")->find();
		$groups = $arrML["groups"];
		if(!$arrML){
			if(!$integral){
				$groups = "failure";
			}else{
				$arrML2 = $membership_level->where("end_num = '0'")->find();
				$groups = $arrML2["groups"];
			}
		}

		$customer = M("customer");
		$arrData = array(
			"integral"=>$integral,
			"member_Level"=>$groups,
		);
		$res = $customer->data($arrData)->where("id = '$customer_id'")->save();
		return true;
	}

	//推送给坐席
	function phpPush($post_string,$answerAgent){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost/msgInterface/pub?user=U".$answerAgent);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "curl");
		$strReturn = curl_exec($ch);
		curl_close($ch);
		//echo $strReturn;
		return true;
	}

}

?>
