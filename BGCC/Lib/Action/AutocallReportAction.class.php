<?php
class AutocallReportAction extends Action{
	function AutocallReportList(){
		checkLogin();
		$sales_task = new Model("sales_task");
		$d_id = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$where = "enable='Y'";
		$where .= " AND dept_id IN ($deptSet)";
		if($username == "admin"){
			$taskList = $sales_task->field("id as task_id,name")->order("createtime")->where("enable='Y'")->select();
			//echo $sales_task->getLastSql();die;
		}else{
			$taskList = $sales_task->field("id as task_id,name")->order("createtime")->where($where)->select();
		}
		//dump($taskList);die;
		$this->assign("taskList",$taskList);
		$this->display();
	}

	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }

	function AutocallReportData(){
		$task_id = $_REQUEST['task_id'];
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
		$mixtime = $_REQUEST['mixtime'];

		$task_table = "sales_cdr_".$task_id;

		$sql = "SELECT u.username AS username,dst AS agent,COUNT(*) AS answeredtimes,SUM(billsec>=$mixtime) AS a_answeredtimes,round(SUM(billsec)/60,2) AS callduration,ROUND(SUM((billsec>=$mixtime)*billsec)/60,2) AS a_callduration,round(AVG(billsec)/60,2) AS averageduration FROM $task_table sc left join users u on sc.dst=u.extension WHERE dst <> '' AND disposition='ANSWERED' AND calldate > '$calldate_start' ";

		if( $calldate_end ){
			$sql .= "AND calldate < '$calldate_end' ";
		}
		$sql .= "GROUP BY agent ORDER BY username ASC";
		//dump($sql);die;
		$sales_cdr = new Model($task_table);

		$arrRet = $sales_cdr->query($sql);
		foreach($arrRet as $k=>$v){
			$arrRes[$v['username']] = $v;
			$arrRes[$v['username']]['a_averageduration'] = number_format($v['a_callduration']/$v['a_answeredtimes'],2);//有效时长
		}
		/*
		$history_table = "sales_contact_history_".$task_id;
		$history = new Model($history_table);
		$sql2 = "SELECT
		  dealuser,
		  SUM(CASE WHEN dealresult=0 THEN 1 ELSE 0 END) AS untreated,
		  SUM(CASE WHEN dealresult=1 THEN 1 ELSE 0 END) AS visit,
		  SUM(CASE WHEN dealresult=2 THEN 1 ELSE 0 END) AS failure,
		  SUM(CASE WHEN dealresult=3 THEN 1 ELSE 0 END) AS success
		FROM $history_table
		GROUP BY dealuser ORDER BY dealuser asc";
		$arrHis = $history->query($sql2);
		//echo $history->getLastSql();die;
		*/
		$source_table = "sales_source_".$task_id;
		$source = new Model("sales_source_".$task_id);
		$sql3 = "SELECT
		  dealuser,
		  SUM(CASE WHEN dealresult_id=0 THEN 1 ELSE 0 END) AS untreated,
		  SUM(CASE WHEN dealresult_id=1 THEN 1 ELSE 0 END) AS visit,
		  SUM(CASE WHEN dealresult_id=2 THEN 1 ELSE 0 END) AS failure,
		  SUM(CASE WHEN dealresult_id=3 THEN 1 ELSE 0 END) AS success
		FROM $source_table WHERE dealuser IS NOT NULL AND locked='Y'
		GROUP BY dealuser ORDER BY dealuser asc";
		$arrHis = $source->query($sql3);
		//echo $source->getLastSql();die;


		foreach($arrHis as $key=>$vm){
			$arrHistory[$vm['dealuser']] = $vm;
		}

		foreach($arrHistory as $key=>$value) {
			foreach($value as $k=>$v) {
			  $arrRes[$key][$k] = $v;
			}
		}

		foreach($arrRes as $v){
			$arrResult[] = $v;
		}
		//dump($arrResult);
		//dump($arrHis);die;
		//dump($sales_cdr->getLastSql());die;
		//$rowsList = count($arrRet) ? $arrRet : false;
		$rowsList = count($arrResult) ? $arrResult : false;
		$ary["total"] = count($arrRet);
		$ary["rows"] = $rowsList;
		echo json_encode($ary);
	}


	//坐席话务量报表
	function agentTrafficReport(){
		checkLogin();
		$currentDate = Date('Y-m-d');
		$this->assign('currentDate',$currentDate);

		//分配增删改的权限
		$menuname = "Agent Traffic Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$this->display();
	}

	//取得坐席话务量报表数据
	//http://192.168.1.69/index.php?m=AutocallReport&a=agentReportList&date_start=2013-03-29
	function getAgentReportData(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_start = Date('Y-m-d',$_GET['ts_start']);
			$date_end = Date('Y-m-d',$_GET['ts_end']);
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1));
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}
		//echo $date_start,$date_end;die;
		if(! $date_end){$date_end = Date('Y-m-d');}
		if($date_start == Date('Y-m-d') && $date_end == Date('Y-m-d')){ //统计今天的报表，需要先生成以下
			exec("/var/lib/asterisk/agi-bin/autocall/generalAgentReport.php");
		}

		$username = $_SESSION['user_info']['username'];
		$workno = $_REQUEST["user_name"];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		//$deptUser = "'".implode("','",$userArr["deptIdUser"][$d_id])."'";
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";
		//echo $deptUser;die;
		$ar = M("agent_report");

		if($workno){
			$where = " AND workno = '$workno'";
		}else{
			if($username != "admin"){
				$where = " AND workno in ($deptUser)";
			}else{
				$where = " AND workno != '' AND workno is not null";
			}
		}

		$fields = "workno,extension,SUM(callin_times) AS callin_times,SUM(callin_billsec_total) AS callin_billsec_total,SUM(callin_billsec_total)/SUM(callin_times) AS avg_callin,SUM(callout_times) AS callout_times,SUM(callout_billsec_total) AS callout_billsec_total,SUM(callout_billsec_total)/SUM(callout_times) AS avg_callout, SUM(CASE WHEN calltype='OUT' AND task_id='0' THEN callout_times ELSE 0 END) AS pbx_callout_times, SUM(CASE WHEN  calltype='OUT' AND task_id='0' THEN callout_billsec_total ELSE 0 END) AS pbx_callout_billsec_total, SUM(CASE WHEN  calltype='OUT' AND task_id='0' THEN callout_billsec_total ELSE 0 END)/ SUM(CASE WHEN calltype='OUT' AND task_id='0' THEN callout_times ELSE 0 END) AS avg_pbx_callout, sum(case when calltype='OUT' AND task_id!='0' then callout_times else 0 end) as outbound_callout_times, sum(case when  calltype='OUT' AND task_id!='0' then callout_billsec_total else 0 end) as outbound_callout_billsec_total, sum(case when  calltype='OUT' AND task_id!='0' then callout_billsec_total else 0 end)/ sum(case when calltype='OUT' AND task_id!='0' then callout_times else 0 end) as avg_outbound_callout ";
		$sql_ar = "
		SELECT $fields
		FROM agent_report
		where calldate >= '$date_start' AND calldate <= '$date_end' AND workno != '' AND workno is not null $where
		GROUP BY workno
		ORDER BY workno asc";

		$result = $ar->query($sql_ar);


		$userArr = readU();
		$user_name = $userArr["service_user"];
		$cnName = $userArr["cn_user"];
		$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];
		$i = 0;
		foreach($result as $val){
			$result[$i]['cn_name'] = $cnName[$val["workno"]];
			$result[$i]['dept_id'] = $deptId_user[$val["workno"]];
			$result[$i]['dept_name'] = $deptName_user[$val["workno"]];
			$i++;
		}
		//dump($result);die;
		//echo $ar->getLastSql();die;



        $search_type = $_REQUEST['search_type'];
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($result);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$displayAgentReport = Array(); //转换成显示的
		$i = $j = 0;
		foreach($result AS &$v){
			$v['callin_billsec_total1'] = $v['callin_billsec_total'];
			$v['avg_callin1'] = $v['avg_callin'];
			$v['callout_billsec_total1'] = $v['callout_billsec_total'];
			$v['avg_callout1'] = $v['avg_callout'];
			$v['pbx_callout_billsec_total1'] = $v['pbx_callout_billsec_total'];
			$v['avg_pbx_callout1'] = $v['avg_pbx_callout'];
			$v['outbound_callout_billsec_total1'] = $v['outbound_callout_billsec_total'];
			$v['avg_outbound_callout1'] = $v['avg_outbound_callout'];


			$v['callin_billsec_total'] = ceil($v['callin_billsec_total']/60);
			$v['avg_callin'] = ceil($v['avg_callin']/60);
			$v['callout_billsec_total'] = ceil($v['callout_billsec_total']/60);
			$v['avg_callout'] = ceil($v['avg_callout']/60);
			$v['pbx_callout_billsec_total'] = ceil($v['pbx_callout_billsec_total']/60);
			$v['avg_pbx_callout'] = ceil($v['avg_pbx_callout']/60);
			$v['outbound_callout_billsec_total'] = ceil($v['outbound_callout_billsec_total']/60);
			$v['avg_outbound_callout'] = ceil($v['avg_outbound_callout']/60);


			$v['callin_billsec_total2'] = sprintf("%02d",intval($v["callin_billsec_total1"]/3600)).":".sprintf("%02d",intval(($v["callin_billsec_total1"]%3600)/60)).":".sprintf("%02d",intval((($v[callin_billsec_total1]%3600)%60)));

			$v['avg_callin2'] = sprintf("%02d",intval($v["avg_callin1"]/3600)).":".sprintf("%02d",intval(($v["avg_callin1"]%3600)/60)).":".sprintf("%02d",intval((($v[avg_callin1]%3600)%60)));

			$v['callout_billsec_total2'] = sprintf("%02d",intval($v["callout_billsec_total1"]/3600)).":".sprintf("%02d",intval(($v["callout_billsec_total1"]%3600)/60)).":".sprintf("%02d",intval((($v[callout_billsec_total1]%3600)%60)));

			$v['avg_callout2'] = sprintf("%02d",intval($v["avg_callout1"]/3600)).":".sprintf("%02d",intval(($v["avg_callout1"]%3600)/60)).":".sprintf("%02d",intval((($v[avg_callout1]%3600)%60)));

			$v['pbx_callout_billsec_total2'] = sprintf("%02d",intval($v["pbx_callout_billsec_total1"]/3600)).":".sprintf("%02d",intval(($v["pbx_callout_billsec_total1"]%3600)/60)).":".sprintf("%02d",intval((($v[pbx_callout_billsec_total1]%3600)%60)));

			$v['avg_pbx_callout2'] = sprintf("%02d",intval($v["avg_pbx_callout1"]/3600)).":".sprintf("%02d",intval(($v["avg_pbx_callout1"]%3600)/60)).":".sprintf("%02d",intval((($v[avg_pbx_callout1]%3600)%60)));

			$v['outbound_callout_billsec_total2'] = sprintf("%02d",intval($v["outbound_callout_billsec_total1"]/3600)).":".sprintf("%02d",intval(($v["outbound_callout_billsec_total1"]%3600)/60)).":".sprintf("%02d",intval((($v[outbound_callout_billsec_total1]%3600)%60)));

			$v['avg_outbound_callout2'] = sprintf("%02d",intval($v["avg_outbound_callout1"]/3600)).":".sprintf("%02d",intval(($v["avg_outbound_callout1"]%3600)/60)).":".sprintf("%02d",intval((($v[avg_outbound_callout1]%3600)%60)));

			if($search_type != "xls"){
				if($i >= $start && $j < $length){
					$displayAgentReport[$j] = $v;
					$j++;
				}
				if( $j >= $length) break;
				$i++;
			}
		}

		$arrFooter = $ar->order("workno asc")->field($fields)->where(" calldate >= '$date_start' AND calldate <= '$date_end' $where")->find();
		//echo $ar->getLastSql();die;

		$arrFooter['callin_billsec_total2'] = sprintf("%02d",intval($arrFooter["callin_billsec_total"]/3600)).":".sprintf("%02d",intval(($arrFooter["callin_billsec_total"]%3600)/60)).":".sprintf("%02d",intval((($arrFooter[callin_billsec_total]%3600)%60)));

		$arrFooter['avg_callin2'] = sprintf("%02d",intval($arrFooter["avg_callin"]/3600)).":".sprintf("%02d",intval(($arrFooter["avg_callin"]%3600)/60)).":".sprintf("%02d",intval((($arrFooter[avg_callin]%3600)%60)));

		$arrFooter['callout_billsec_total2'] = sprintf("%02d",intval($arrFooter["callout_billsec_total"]/3600)).":".sprintf("%02d",intval(($arrFooter["callout_billsec_total"]%3600)/60)).":".sprintf("%02d",intval((($arrFooter[callout_billsec_total]%3600)%60)));

		$arrFooter['avg_callout2'] = sprintf("%02d",intval($arrFooter["avg_callout"]/3600)).":".sprintf("%02d",intval(($arrFooter["avg_callout"]%3600)/60)).":".sprintf("%02d",intval((($arrFooter[avg_callout]%3600)%60)));

		$arrFooter['pbx_callout_billsec_total2'] = sprintf("%02d",intval($arrFooter["pbx_callout_billsec_total"]/3600)).":".sprintf("%02d",intval(($arrFooter["pbx_callout_billsec_total"]%3600)/60)).":".sprintf("%02d",intval((($arrFooter[pbx_callout_billsec_total]%3600)%60)));

		$arrFooter['avg_pbx_callout2'] = sprintf("%02d",intval($arrFooter["avg_pbx_callout"]/3600)).":".sprintf("%02d",intval(($arrFooter["avg_pbx_callout"]%3600)/60)).":".sprintf("%02d",intval((($arrFooter[avg_pbx_callout]%3600)%60)));

		$arrFooter['outbound_callout_billsec_total2'] = sprintf("%02d",intval($arrFooter["outbound_callout_billsec_total"]/3600)).":".sprintf("%02d",intval(($arrFooter["outbound_callout_billsec_total"]%3600)/60)).":".sprintf("%02d",intval((($arrFooter[outbound_callout_billsec_total]%3600)%60)));

		$arrFooter['avg_outbound_callout2'] = sprintf("%02d",intval($arrFooter["avg_outbound_callout"]/3600)).":".sprintf("%02d",intval(($arrFooter["avg_outbound_callout"]%3600)/60)).":".sprintf("%02d",intval((($arrFooter[avg_outbound_callout]%3600)%60)));

		$arrFooter["workno"] = "总计";
		$arrFooter["extension"] = "";
		$arrFooter2[0] = $arrFooter;


		if($search_type == "xls"){
			$field = array("workno","cn_name","extension","callin_times","callin_billsec_total2","avg_callin2","callout_times","callout_billsec_total2","avg_callout2","pbx_callout_times","pbx_callout_billsec_total2","avg_pbx_callout2","outbound_callout_times","outbound_callout_billsec_total2","avg_outbound_callout2");
			$title = array("工号","姓名","分机号","接听次数","接听总时长","接听平均时长","呼出次数","呼出总时长","呼出平均时长","pbx呼出次数","pbx呼出总时长","pbx呼出平均时长","外呼呼出次数","外呼呼出总时长","外呼呼出平均时长");

			$xls_count = count($field);
			$excelTiele = "坐席话务量报表";
			array_push($excelTiele,$arrFooter);
			//$this->exportData($field,$title,$xls_count,$excelTiele,$result);
			exportDataFunction($xls_count,$field,$title,$result,$excelTiele);
			die;
		}

		//dump($displayAgentReport);die;
		$rowsList = count($displayAgentReport) ? $displayAgentReport : false;

		$ary["total"] = count($result);
		$ary["rows"] = $rowsList;
		if($date_start){
			$ary["date_start"] = $date_start;
		}
		if($date_end){
			$ary["date_end"] = $date_end;
		}
		$ary["footer"] = $arrFooter2;
		echo json_encode($ary);
		//$this->assign('result',$result);
		//$this->display();
	}



	function exportData($field,$title,$count,$excelTiele,$arrOrderData){


		vendor("PHPExcel176.PHPExcel");
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			//$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setWrapText(true);    //自动换行
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($arrOrderData as &$val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
				//将单元格设为文本类型----------手机号码
				if($j == "1" || $j == "9"){
					$objPHPExcel->getActiveSheet()->setCellValueExplicit($letters2[$j].$start_row, $val[$field[$j]],PHPExcel_Cell_DataType::TYPE_STRING);
				}
				//$objPHPExcel->getActiveSheet()->getStyle($letters2[$j].$start_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			}
			$start_row++;
		}
		$objPHPExcel->setActiveSheetIndex(0);

		$filename = iconv("utf-8","gb2312",$excelTiele);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}



	//坐席示忙报表
	function agentDNDReport(){
		checkLogin();
		$currentDate = Date('Y-m-d H:i:s');
		$this->assign('currentDate',$currentDate);

		//分配增删改的权限
		$menuname = "Agent DND Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	//取得坐席示忙报表数据
	//index.php?m=AutocallReport&a=getDNDReportData&date_start=2013-03-22%2015:14:24&date_end=2013-04-01%2000:00:00
	function getDNDReportData(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_end = Date('Y-m-d',$_GET['ts_end']) ." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){//上个月的起始时间
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1)) ." 00:00:00";
			}else{
				$date_start = Date('Y-m-d',$_GET['ts_start']) ." 00:00:00";
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}

		if(! $date_end){
			$date_end = Date('Y-m-d H:i:s');
		}
		$search_type = $_REQUEST["search_type"];


		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";
		//echo $deptUser;die;

		if($username != "admin"){
			$where = " AND username in ($deptUser)";
		}else{
			$where = " ";
		}

		$ar = M("servicepause_reason");
		$sql_sr="
		SELECT username,reason,COUNT(*) AS times,SUM(during) AS during_total
		FROM servicepause_reason
		WHERE starttime >= '$date_start' AND starttime <= '$date_end' $where
		GROUP BY username,reason
		ORDER BY username ASC";
		//dump($sql_sr);die;
		$result = $ar->query($sql_sr);
		//dump($result);
		$arrData = Array();//索引数组
		$j = -1; //新数组下表
		foreach( $result AS $row ){
			if( $arrData[$j]['username'] != $row['username'] ){
				$j++;
				$arrData[$j] = Array();
				$arrData[$j]['username'] = $row['username'];
			}
			switch($row['reason']){
				case '小休':{
					$arrData[$j]['littlerest'] = '小休';
					$arrData[$j]['littlerest_times'] = $row['times'];
					$arrData[$j]['littlerest_total'] = secendHMS($row['during_total']);
					//$arrData[$j]['littlerest_total'] = ceil($row['during_total']/60);
					//$arrData[$j]['littlerest_total'] = round(($row['during_total']/3600),2);
					break;
				}
				case '话后':{
					$arrData[$j]['aftercall'] = '话后';
					$arrData[$j]['aftercall_times'] = $row['times'];
					$arrData[$j]['aftercall_total'] = secendHMS($row['during_total']);
					//$arrData[$j]['aftercall_total'] = ceil($row['during_total']/60);
					break;
				}
				case '午休':{
					$arrData[$j]['lunchbreak'] = '午休';
					$arrData[$j]['lunchbreak_times'] = $row['times'];
					$arrData[$j]['lunchbreak_total'] = secendHMS($row['during_total']);
					//$arrData[$j]['lunchbreak_total'] = ceil($row['during_total']/60);
					break;
				}
				case '会议':{
					$arrData[$j]['meeting'] = '会议';
					$arrData[$j]['meeting_times'] = $row['times'];
					$arrData[$j]['meeting_total'] = secendHMS($row['during_total']);
					//$arrData[$j]['meeting_total'] = ceil($row['during_total']/60);
					break;
				}
				case '培训':{
					$arrData[$j]['training'] = '培训';
					$arrData[$j]['training_times'] = $row['times'];
					$arrData[$j]['training_total'] = secendHMS($row['during_total']);
					//$arrData[$j]['training_total'] = ceil($row['during_total']/60);
					break;
				}
				case '话机示忙':{
					$arrData[$j]['exten_busy'] = '话机示忙';
					$arrData[$j]['exten_busy_times'] = $row['times'];
					$arrData[$j]['exten_busy_total'] = secendHMS($row['during_total']);
					//$arrData[$j]['training_total'] = ceil($row['during_total']/60);
					break;
				}
			}
		}
		unset($j);
		//dump($arrData);die;
		/*
		$max = 0;
		foreach($result as $k => $v){
			$v['during_total'] = ceil($v['during_total']/3600);
			if($v['during_total'] > $max)$max = $v['during_total'];
		}
		$max = $max+10;
		//dump($max);
		*/

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];
		$i = 0;
		foreach($arrData as $val){
			$arrData[$i]['cn_name'] = $cnName[$val["username"]];
			$arrData[$i]['dept_name'] = $deptName_user[$val["username"]];
			$i++;
		}


		if($search_type == "xls"){
			$arrField = array('username','cn_name','littlerest','littlerest_times','littlerest_total','lunchbreak','lunchbreak_times','lunchbreak_total','meeting','meeting_times','meeting_total','training','training_times','training_total');  //,'aftercall','aftercall_times','aftercall_total'
			$arrTitle = array('工号','姓名','小休','小休次数','小休总时长','午休','午休次数','午休总时长','会议','会议次数','会议总时长','培训','培训次数','培训总时长');  //,'话后','话后次数','话后总时长'
			$xls_count = count($arrField);
			$excelTiele = "坐席示忙报表".date("Y-m-d");
			//array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}


		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$dndData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrData AS &$v){
			if($i >= $start && $j < $length){
				$dndData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}

		$rowsList = count($dndData) ? $dndData : false;
		$ary["total"] = count($arrData);
		$ary["rows"] = $rowsList;
		//$ary["max"] = $max;
		if($date_start){
			$ary["date_start"] = $date_start;
		}
		if($date_end){
			$ary["date_end"] = $date_end;
		}
		echo json_encode($ary);
	}


	//系统报表
	function systemReport(){
		checkLogin();
		$currentDate = Date('Y-m-d');
		$this->assign('currentDate',$currentDate);

		//分配增删改的权限
		$menuname = "System Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function getsystemReportData(){
		$search_type= $_REQUEST["search_type"];
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_start = Date('Y-m-d',$_GET['ts_start']);
			$date_end = Date('Y-m-d',$_GET['ts_end']);
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1));
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}
		if(! $date_end){$date_end = Date('Y-m-d');}
		if($date_start == Date('Y-m-d') && $date_end == Date('Y-m-d')){ //统计今天的报表，需要先生成以下
			exec("/var/lib/asterisk/agi-bin/autocall/generalAgentReport.php");
		}
		$lastweek = $_GET['lastweek'];
		$year_month = substr($date_start,0,-3);
		$year_month_end = substr($date_end,0,-3);
		//dump($_GET);

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";

		$agent_report = new Model("agent_report");
		$where = "1  AND workno != '' AND workno is not null";
		$where .= empty($date_start) ? "" : " AND calldate >= '$date_start'";
		$where .= empty($date_end) ? "" : " AND calldate <= '$date_end'";

		if($username != "admin"){
			$where = " AND workno in ($deptUser)";
		}

		$arrData = $agent_report->field("calldate,SUM(callin_times) AS callin_times,SUM(callin_billsec_total) AS callin_billsec_total,SUM(callout_times) AS callout_times,SUM(callout_billsec_total) AS callout_billsec_total")->where($where)->group("calldate")->select();

		$arrTmp = Array();
		foreach($arrData as &$val){
			$val['callin_billsec_total2'] = secendHMS($val['callin_billsec_total']);
			$val['callout_billsec_total2'] = secendHMS($val['callout_billsec_total']);
			$val['billsec_total2'] = secendHMS($val['callin_billsec_total'] + $val['callout_billsec_total']); //累计呼叫时长

			$val['callin_billsec_total'] = ceil($val['callin_billsec_total']/60);
			$val['callout_billsec_total'] = ceil($val['callout_billsec_total']/60);

			$val['call_times'] = $val['callin_times'] + $val['callout_times']; //累计呼叫次数
			$val['billsec_total'] = ceil($val['callin_billsec_total'] + $val['callout_billsec_total']); //累计呼叫时长
			$arrTmp[$val['calldate']] = $val;
			$arrTmp[$val['calldate']]['day'] = substr($val['calldate'],-2);
		}

		$date = $date_start;
		for($date = $date_start;strcmp($date,$date_end)<=0;$date = date("Y-m-d",strtotime("$date +1 day"))){
			if(! isset($arrTmp[$date]) ){
				$arrTmp[$date] = Array(
					"day"=>substr($date,-2),
					"calldate"=>$date,
					"callin_times" => '0',
					"callin_billsec_total" => '0',
					"callin_billsec_total2" => '00:00:00',
					"callout_times" => '0',
					"callout_billsec_total" => '0',
					"callout_billsec_total2" => '00:00:00',
					"call_times" => '0',
					"billsec_total" => '0',
					"billsec_total2" => '00:00:00',
				);
			}
		}
		ksort($arrTmp);

		$arrD = array_values($arrTmp);


		if($search_type == "xls"){
			$arrField = array('calldate','callin_times','callin_billsec_total2','callout_times','callout_billsec_total2','call_times','billsec_total2');
			$arrTitle = array('呼叫时间','呼入次数','呼入应答累计时长','呼出次数','呼出应答累计时长','累计呼叫总次数','累计通话总时长');
			$xls_count = count($arrField);
			$excelTiele = "系统话务量统计".date("Y-m-d");
			//dump($arrD);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrD,$excelTiele);
			die;
		}

		//dump($arrD);die;
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$count = count($arrD);
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrF = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrD AS &$v){
			if($i >= $start && $j < $length){
				$arrF[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		//dump($arrF);die;

		$rowsList = count($arrF) ? $arrF : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		if($date_start){
			$ary["date_start"] = $date_start;
		}
		if($date_end){
			$ary["date_end"] = $date_end;
		}
		echo json_encode($ary);
	}

	//取得系统报表数据
	//http://192.168.1.69/index.php?m=AutocallReport&a=getsystemReportDate&date_start=2013-03-29
	function getsystemReportDataOld(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_start = Date('Y-m-d',$_GET['ts_start']);
			$date_end = Date('Y-m-d',$_GET['ts_end']);
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1));
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}
		$lastweek = $_GET['lastweek'];
		//dump($lastweek);die;
	/*	$date_start = "2013-04-29";
		$date_end = "2013-05-05";*/
		//echo $date_start,$date_end;die;
		if(! $date_end){$date_end = Date('Y-m-d');}
		$ar = M("agent_report");
		/*
		$sql_ar = "
		SELECT calldate,SUM(callin_times) AS callin_times,SUM(callin_billsec_total) AS callin_billsec_total,SUM(callout_times) AS callout_times,SUM(callout_billsec_total) AS callout_billsec_total
		FROM agent_report
		WHERE calldate >= '$date_start' AND calldate <= '$date_end'
		GROUP BY calldate ";
		*/
		//$year_month = date("Y-m");
		$year_month = substr($date_start,0,-3);
		$year_month_end = substr($date_end,0,-3);
		$sql_ar = "
		SELECT calldate,SUM(callin_times) AS callin_times,SUM(callin_billsec_total) AS callin_billsec_total,SUM(callout_times) AS callout_times,SUM(callout_billsec_total) AS callout_billsec_total
		FROM agent_report
		WHERE calldate >= '$date_start' AND calldate <= '$date_end'
		GROUP BY calldate ";

		$result = $ar->query($sql_ar); //其实只有一行
		/*
		for($i = strtotime($date_start); $i <= strtotime($date_end); $i += 86400){
			$ThisDate=date("Y-m-d",$i);
			$Thisd=date("d",$i);
			$arrLAST[$Thisd] = Array(
				'calldate' => $ThisDate,
				"callin_times" => '0',
				"callin_billsec_total" => '0',
				"callout_times" => '0',
				"callout_billsec_total" => '0',
				"call_times" => '0',
				"billsec_total" => '0',
			);
		}
		*/

		//dump($result);
		/*
		$timestamp = strtotime($year_month . "-10 00:00:00");
		$days = date('t',$timestamp);
		$arrLAST = Array();
		if($_GET['thisweek'] != 0 || $_GET['lastweek'] != 0){
			for($i=0;$i<$days;$i++){
				if( $i< 9){
					$day = "0" . ($i+1);
				}else{
					$day = $i+1;
				}
				$current_day = $year_month .'-' .$day;
				$arrLAST[$i] = Array(
					//'date' => $i+1,
					'calldate' => $current_day,
					"callin_times" => '0',
					"callin_billsec_total" => '0',
					"callout_times" => '0',
					"callout_billsec_total" => '0',
					"call_times" => '0',
					"billsec_total" => '0',
					//"billsec_total" => $i+1,
				);
			}
		}else{
			for($i = strtotime($date_start); $i <= strtotime($date_end); $i += 86400){
				$ThisDate=date("Y-m-d",$i);
				$arrLAST[] = Array(
					'calldate' => $ThisDate,
					"callin_times" => '0',
					"callin_billsec_total" => '0',
					"callout_times" => '0',
					"callout_billsec_total" => '0',
					"call_times" => '0',
					"billsec_total" => '0',
				);
			}
		}

		foreach( $result AS $v ){

			if( empty( $v['callin_times'] ) ) {$v['callin_times'] = 0;}
			if( empty( $v['callout_times'] ) ) {$v['callout_times'] = 0;}
			$v['callin_billsec_total'] = ceil($v['callin_billsec_total']/60);
			$v['callout_billsec_total'] = ceil($v['callout_billsec_total']/60);
			$v['call_times'] = $v['callin_times'] + $v['callout_times']; //累计呼叫次数
			$v['billsec_total'] = ceil($v['callin_billsec_total'] + $v['callout_billsec_total']); //累计呼叫时长

		}*/

		//转换成索引数组
		$arrTmp = Array();
		foreach( $result AS &$v ){
			if( empty( $v['callin_times'] ) ) {$v['callin_times'] = 0;}
			if( empty( $v['callout_times'] ) ) {$v['callout_times'] = 0;}
			$v['callin_billsec_total'] = ceil($v['callin_billsec_total']/60);
			$v['callout_billsec_total'] = ceil($v['callout_billsec_total']/60);
			$v['call_times'] = $v['callin_times'] + $v['callout_times']; //累计呼叫次数
			$v['billsec_total'] = ceil($v['callin_billsec_total'] + $v['callout_billsec_total']); //累计呼叫时长
			$arrTmp[$v['calldate']] = $v;
			$arrTmp[$v['calldate']]['day'] = substr($v['calldate'],-2);
		}

		//dump($arrTmp);die;
		//构造所有日期列表
		$date = $date_start;
		for($date = $date_start;strcmp($date,$date_end)<=0;$date = date("Y-m-d",strtotime("$date +1 day"))){
			if(! isset($arrTmp[$date]) ){
				$arrTmp[$date] = Array(
					"day"=>substr($date,-2),
					"calldate"=>$date,
					"callin_times" => '0',
					"callin_billsec_total" => '0',
					"callout_times" => '0',
					"callout_billsec_total" => '0',
					"call_times" => '0',
					"billsec_total" => '0',
				);
			}
		}
		ksort($arrTmp);

		$arrD = array_values($arrTmp);
		//dump($arrD);die;

		$rowsList = count($arrD) ? $arrD : false;
		$ary["total"] = count($arrD);
		$ary["rows"] = $rowsList;
		if($date_start){
			$ary["date_start"] = $date_start;
		}
		if($date_end){
			$ary["date_end"] = $date_end;
		}
		echo json_encode($ary);
	}


	//队列报表
	function queueReport(){
		checkLogin();
		$currentDate = Date('Y-m-d');
		$this->assign('currentDate',$currentDate);

		$qc = M("asterisk.queues_config");
		$arrQueue = $qc->field("extension,descr")->select();
		$this->assign('arrQueue',$arrQueue);


		//分配增删改的权限
		$menuname = "Queue Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	//取得系统报表数据
	//http://192.168.1.69/index.php?m=AutocallReport&a=getsystemReportDate&date_start=2013-03-29
	function getqueueReportData(){
		$queue = $_REQUEST["queue"];
		$search_type = $_REQUEST["search_type"];
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_start = Date('Y-m-d',$_GET['ts_start']);
			$date_end = Date('Y-m-d',$_GET['ts_end']);
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1));
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}
		$lastweek = $_GET['lastweek'];
		if(! $date_end){$date_end = Date('Y-m-d');}
		$year_month = substr($date_start,0,-3);
		$year_month_end = substr($date_end,0,-3);

		$Q = M("asteriskcdrdb.queue");
		$sql_q = "SELECT DATE(calltime) AS `calldate`,queue,COUNT(*) AS totalcalls,SUM(STATUS='ANSWERED') AS answeredtimes,SUM(STATUS='NO ANSWER')AS noansweredtimes,SUM(ringtimes) AS passedcalltimes,SUM(TIMESTAMPDIFF(SECOND,calltime,answertime)) AS waittotalsecond,SUM(TIMESTAMPDIFF(SECOND,answertime,hanguptime)) AS during
		FROM asteriskcdrdb.queue
		WHERE queue='$queue' AND DATE(calltime) >= '$date_start' AND DATE(calltime) <= '$date_end'
		GROUP BY DATE(calltime),queue
		ORDER BY DATE(calltime) DESC,queue ASC";

		$result = $Q->query($sql_q);
		//dump($Q->getLastSql());dump($result);die;

		//转换成索引数组
		$arrTmp = Array();
		foreach( $result AS &$v ){
			$v['failedrate'] = (string)(round(100*$v['noansweredtimes']/$v['totalcalls'],1)) ."%"; //失败率
			$v['passedrate'] = (string)(round(100*$v['passedcalltimes']/($v['answeredtimes']+$v['passedcalltimes']),1)) ."%"; //漏接率

			$v['avg_waitingtime'] = (string)(round($v['waittotalsecond']/$v['totalcalls'],1)); //平均等待时长
			//$v['avg_waitingtime'] = secendHMS($v['avg_waitingtime']); //平均等待时长

			$v['avg_duringtime'] = (string)(round($v['during']/$v['totalcalls'],1)); //平均通话时长
			//$v['avg_duringtime'] = secendHMS($v['avg_duringtime']); //平均通话时长

			$arrTmp[$v['calldate']] = $v;
			$arrTmp[$v['calldate']]['day'] = substr($v['calldate'],-2);
		}

		//dump($arrTmp);die;
		//构造所有日期列表
		$date = $date_start;
		for($date = $date_start;strcmp($date,$date_end)<=0;$date = date("Y-m-d",strtotime("$date +1 day"))){
			if(! isset($arrTmp[$date]) ){
				$arrTmp[$date] = Array(
					"day"=>substr($date,-2),
					"calldate"=>$date,
					//"queue" => "$queue",
					"totalcalls" => "0",
					"answeredtimes" => '0',
					"noansweredtimes" => '0',
					"passedcalltimes" => '0',
					"failedrate" => '0.0%',
					"passedrate" => '0.0%',
					"avg_waitingtime" => '0',
					"avg_duringtime" => '0',
				);
			}
		}
		ksort($arrTmp);

		$arrD = array_values($arrTmp);

		if($search_type == "xls"){
			$arrField = array('calldate','totalcalls','answeredtimes','noansweredtimes','passedcalltimes','failedrate','passedrate','avg_waitingtime','avg_duringtime');
			$arrTitle = array('呼叫时间','呼入总次数','接通次数','漏接次数','坐席轮询总次数','漏接率','应答不及时率','平均等待时长','平均通话时长');
			$xls_count = count($arrField);
			$excelTiele = "呼入队列报表".date("Y-m-d");
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrD,$excelTiele);
			die;
		}
		//dump($arrD);die;

		$rowsList = count($arrD) ? $arrD : false;
		$ary["total"] = count($arrD);
		$ary["rows"] = $rowsList;
		if($date_start){
			$ary["date_start"] = $date_start;
		}
		if($date_end){
			$ary["date_end"] = $date_end;
		}
		echo json_encode($ary);
	}



	function outboundCount(){
		checkLogin();
		$this->display();
	}

	function outBoundData(){
		$username = $_SESSION['user_info']['username'];
		$dept_id = $_SESSION['user_info']['d_id'];
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_start = Date('Y-m-d',$_GET['ts_start']);
			$date_end = Date('Y-m-d',$_GET['ts_end']);
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1));
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}
		//echo $date_start,$date_end;die;
		if(! $date_end){
			$date_end = Date('Y-m-d');
		}

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$deptSet = rtrim($deptst,",");

		$where = "1 ";
		$where .= empty($date_start)?"":" AND createtime >= '$date_start'";
        $where .= empty($date_end)?"":" AND createtime <='$date_end'";

		$outbound = new Model("outbound_statistics");
		if($username == "admin"){
			$count = $outbound->field("createname,sum(success_num) as success_num,sum(fail_num) as fail_num,sum(visit_num) as visit_num")->group("createname")->where($where)->select();
		}else{
			$count = $outbound->field("createname,sum(success_num) as success_num,sum(fail_num) as fail_num,sum(visit_num) as visit_num")->group("createname")->where("$where AND dept_id in ($deptSet)")->select();
		}
		$count = count($count);
		//dump($count);
		//echo $outbound->getLastSql();die;
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == "admin"){
			$outboundData = $outbound->field("createname,sum(success_num) as success_num,sum(fail_num) as fail_num,sum(visit_num) as visit_num,sum(untreated_num) as untreated_num")->group("createname")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}else{
			$outboundData = $outbound->field("createname,sum(success_num) as success_num,sum(fail_num) as fail_num,sum(visit_num) as visit_num,sum(untreated_num) as untreated_num")->group("createname")->limit($page->firstRow.','.$page->listRows)->where("$where AND dept_id in ($deptSet)")->select();
		}
		//echo $outbound->getLastSql();
		//dump($outboundData);die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];
		$i = 0;
		foreach($outboundData as $val){
			$outboundData[$i]['cn_name'] = $cnName[$val["createname"]];
			$outboundData[$i]['d_name'] = $deptName_user[$val["createname"]];
			$i++;
		}


		$rowsList = count($outboundData) ? $outboundData : false;
		$arrBound["total"] = $count;
		$arrBound["rows"] = $rowsList;
		if($date_start){
			$arrBound["date_start"] = $date_start;
		}
		if($date_end){
			$arrBound["date_end"] = $date_end;
		}
		echo json_encode($arrBound);
	}
}

?>

