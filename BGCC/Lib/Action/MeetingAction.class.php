<?php
class MeetingAction extends Action{
	function listMeeting(){
		checkLogin();
		$menuname = "List Meeting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$time = date("Y-m-d H:i");
		$this->assign("time",$time);

		$this->display();
	}

	function mettingData(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrMeeting = $bmi->listAllMeetings();
		//dump($arrM);die;

		$m = M("meeting");
		$where = "1 ";
		if($username != "admin"){
			$where .= " AND dept_id = '$d_id'";
		}
		$count = $m->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$arrM = $m->table("meeting m")->field("m.*,username,cn_name")->join("left join users u on m.createuser=u.username")->order("starttime DESC")->where($where)->select();
		//echo $m->getLastSql();die;

		$userArr = readU();
		$deptId_name = $userArr["deptId_name"];
		foreach($arrM AS &$row){
			$row['state'] = $arrMeeting[$row['exten']]?'开会中':'';
			$row['count'] = $arrMeeting[$row['exten']][1];
			$row['duration'] = $arrMeeting[$row['exten']][3];
			$row['opt'] = <<<EOF
<a href="index.php?m=Meeting&a=loginMeeting&exten=${row['exten']}">主持会议</a>
EOF;
			$row["deptName"] = $deptId_name[$row["dept_id"]];
		}
		$rowsList = count($arrM) ? $arrM : false;
		$arrMetting["total"] = $count;
		$arrMetting["rows"] = $rowsList;

		echo json_encode($arrMetting);
	}

	function checkMeetingExist(){
		checkLogin();
		$exten = $_GET['exten'];
		$m = M('meeting');
		$num = $m->where("exten='$exten'")->count();
		//dump($m->getLastSql());die;
		if( $num > 0){
			echo "YES";
		}else{
			echo "NO";
		}
	}

	function addMeetingDo(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$arrMeet = Array(
			"dept_id"=>$_POST['dept_id'],
			"exten"=>$_POST['exten'],
			"starttime"=>$_POST['starttime'],
			"userpin"=>$_POST['userpin'],
			"adminpin"=>$_POST['adminpin'],
			"users"=>$_POST['users'],
			"description"=>$_POST['description'],
			"createuser"=>$username,
		);
		$m = M('meeting');
		//检测会议室是否存在
		//$count = $m->where("exten='{$_POST['exten']}'")->count();
		$count = $m->where("exten='${_POST['exten']}'")->count();
		if($count > 0){
			goBack("会议室已经存在!");
		}
		$res = $m->add($arrMeet);
		if ($res){
			echo json_encode(array('success'=>true,'msg'=>'会议室添加成功!'));
		} else {
			echo json_encode(array('msg'=>'会议室添加失败!'));
		}
	}

	function editMeetingDo(){
		checkLogin();
		$id = $_REQUEST['id'];
		$meeting = new Model("meeting");
		$arrMeet = Array(
			"dept_id" => $_REQUEST['dept_id'],
			"exten" => $_REQUEST['exten'],
			"starttime"=>$_POST['starttime'],
			"userpin"=>$_POST['userpin'],
			"adminpin"=>$_POST['adminpin'],
			"users"=>$_POST['users'],
			"description"=>$_POST['description'],
		);
		$res = $meeting->where("id=$id")->save($arrMeet);
		if ($res !== false){
			echo json_encode(array('success'=>true,'msg'=>"修改成功！"));
		} else {
			echo json_encode(array('msg'=>'修改失败！'));
		}
	}

	function deleteMeeting(){
		checkLogin();
		$id = $_REQUEST["id"];
		$meeting = new Model("meeting");
		$res = $meeting->where("id=$id")->delete();
		if ($res){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function loginMeeting(){
		checkLogin();
		//dump($_SESSION['meeting']);die;
		if($_SESSION['meeting']['exten']){
			header("Location: index.php?m=Meeting&a=startMeeting");
		}else{
			$exten = $_REQUEST['exten']?$_REQUEST['exten']:"";
			$this->assign("exten",$exten);
			$this->display();
		}

	}

	function loginMeetingCheck(){
		$adminpin = $_POST['adminpin'];
		$exten = $_POST['exten'];

		$m = M('meeting');
		$res = $m->where("exten='$exten' AND adminpin='$adminpin'")->find();
		//dump($m->getLastSql());die;
		if( $res ){ //可以查到会议
			$_SESSION['meeting'] = $res; //设置会议室号码
			header("Location: index.php?m=Meeting&a=startMeeting");
		}else{
			//$this->error("登陆失败，用户名或者密码错误!");
			goback("登录失败！");
		}
	}

	/*
	function startMeeting(){
		//dump($_SESSION);die;
		//dump($_SESSION['meeting']);die;
		if(!$_SESSION['meeting']){
			header("Location: index.php?m=Meeting&a=loginMeeting");
		}
		$extension = $_SESSION['user_info']["extension"];
		$this->assign("extension",$extension);//管理员分机号
		$exten = $_SESSION['meeting']["exten"];
		$this->assign("exten",$exten);//会议室号码
		$userpin = $_SESSION['meeting']["userpin"];
		$this->assign("userpin",$userpin);
		//中继
		$ast = M("asterisk.trunks");
		$trunks = $ast->field("CONCAT(tech,'/',channelid) AS trunk")->select();
		$this->assign("trunks",$trunks);//中继

		$arrInfo = Array();
		$_SESSION['meeting']['members'] = isset($_SESSION['meeting']['members'])?$_SESSION['meeting']['members']:Array();
		//dump($_SESSION['meeting']['members'] );die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		getArrCache();
		$i = 0;
		foreach($_SESSION['meeting']['members'] AS $v){
			$arrInfo[$i] = Array();
			$arrInfo[$i]['num'] = $v;//电话号码

			$arrInfo[$i]['cn_name'] = '';
			foreach( $arrCacheWorkNo AS $val ){
				if( $val['extension'] == $v ){
					$arrInfo[$i]['cn_name'] = $val['cn_name']; //姓名
					break;
				}
			}
			$arrHint = $bmi->getHint($v);//状态
			if( $arrHint['stat'] == "Idle" ){
				$arrInfo[$i]['stat'] = '空闲';
			}elseif( $arrHint['stat'] == "Unavailable" ){
				$arrInfo[$i]['stat'] = '未注册';
			}else{
				$arrInfo[$i]['stat'] = '忙';
			}
			$i++;
		}
		unset($i);
		//dump($exten);die;
		//dump($bmi->listMeeting2($exten) );die;
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if($v['state']=='Muted'){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$this->assign('count',count($arrMeeting));
		$this->assign('arrMeeting',$arrMeeting);

		//dump($arrInfo);die;
		$this->assign("arrInfo","(" .json_encode($arrInfo) .")");
		$this->display();
	}
	*/

	function startMeeting(){
		checkLogin();
		//dump($_SESSION);die;
		if(!$_SESSION['meeting']){
			header("Location: index.php?m=Meeting&a=loginMeeting");
		}
		//我的分机号
		$extension = $_SESSION['user_info']["extension"];
		$this->assign("extension",$extension);//发起会议的分机号

		//会议室号码和登陆密码
		$exten = $_SESSION['meeting']["exten"];
		$this->assign("exten",$exten);//会议室号码

		$userpin = $_SESSION['meeting']["userpin"];
		$this->assign("userpin",$userpin);
		//中继
		$ast = M("asterisk.trunks");
		$trunks = $ast->field("CONCAT(tech,'/',channelid) AS trunk")->select();
		$this->assign("trunks",$trunks);//中继

		$arrInfo = Array();
		$_SESSION['meeting']['members'] = isset($_SESSION['meeting']['members'])?$_SESSION['meeting']['members']:Array();
		//dump($_SESSION['meeting']['members'] );die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		getArrCache();
		$i = 0;
		foreach($_SESSION['meeting']['members'] AS $v){
			$arrInfo[$i] = Array();
			$arrInfo[$i]['num'] = $v;//电话号码
			$arrInfo[$i]['cn_name'] = '';
			foreach( $arrCacheWorkNo AS $val ){
				if( $val['extension'] == $v ){
					$arrInfo[$i]['cn_name'] = $val['cn_name']; //姓名
					break;
				}
			}
			$arrHint = $bmi->getHint($v);//状态
			if( $arrHint['stat'] == "Idle" ){
				$arrInfo[$i]['stat'] = '空闲';
			}elseif( $arrHint['stat'] == "Unavailable" ){
				$arrInfo[$i]['stat'] = '未注册';
			}elseif( $arrHint['stat'] == NULL ){
				$arrInfo[$i]['stat'] = '空闲';
			}else{
				$arrInfo[$i]['stat'] = '忙';
			}
			$i++;
		}
		unset($i);
		//dump($exten);die;
		//dump($bmi->listMeeting2($exten) );die;
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if(false!== strpos($v['state'],'Muted')){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$this->assign('count',count($arrMeeting));
		$rowsList = count($arrMeeting) ? $arrMeeting : false;
		$arrMeet["total"] = count($arrMeeting);
		$arrMeet["rows"] = $rowsList;
		$strTmp = json_encode($arrMeet);
		$this->assign("strTmp",$strTmp);
		//dump($arrInfo);die;
		$this->assign("arrInfo","(" .json_encode($arrInfo) .")");
		$this->display();
	}

	//完全是用来刷新会议室，中间有暂停时间
	function reloadMeeting(){
		$exten = $_SESSION['meeting']["exten"];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		//getArrCache();
		sleep(1);
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if(false!== strpos($v['state'],'Muted')){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}

		$arrU = readU();
		$ename = $arrU["exten_cnName"];
		$pname = $arrU["phone_cnName"];

		foreach($arrMeeting as &$val){
			if( strlen($val["extension"]) > 6){
				$val["name"] = $pname[$val["extension"]];
			}else{
				$val["name"] = $ename[$val["extension"]];
			}
		}

		//dump($arrMeeting);die;
		$rowsList = count($arrMeeting) ? $arrMeeting : false;
		$arrMeet["total"] = count($arrMeeting);
		$arrMeet["rows"] = $rowsList;
		$strTmp = json_encode($arrMeet);
		echo $strTmp;
	}

	//这个函数非常重要，当页面离开后是否保存发起呼叫的号码就是靠这个函数
	function checkMeetingMember(){
		$_SESSION['meeting']['members'] = isset($_SESSION['meeting']['members'])?$_SESSION['meeting']['members']:Array();
		$arrNum = explode(',',$_GET['numStr'] .',' .$_GET['phoneStr']);//号码用逗号隔开
		//dump($arrNum);die;
		foreach($arrNum AS $v){//先进行一下号码过滤
			if( strlen($v)>2 && is_numeric($v) ){//过滤掉空号码
				if( in_array($v,$_SESSION['meeting']['members']) ){
					continue;
				}else{
					array_push($_SESSION['meeting']['members'],$v);
				}
			}
		}
		//dump($_SESSION['meeting']['members']);die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		getArrCache();

		$i = 0;$arrInfo = Array();
		foreach($_SESSION['meeting']['members'] AS $v){
			$arrInfo[$i] = Array();
			$arrInfo[$i]['num'] = $v;//电话号码
			$arrInfo[$i]['cn_name'] = '';
			foreach( $arrCacheWorkNo AS $val ){
				if( $val['extension'] == $v ){
					$arrInfo[$i]['cn_name'] = $val['cn_name']; //姓名
					break;
				}
			}

			$arrHint = $bmi->getHint($v);//状态
			if( $arrHint['stat'] == "Idle" ){
				$arrInfo[$i]['stat'] = '空闲';
			}elseif( $arrHint['stat'] == "Unavailable" ){
				$arrInfo[$i]['stat'] = '未注册';
			}elseif( $arrHint['stat'] == NULL ){
				$arrInfo[$i]['stat'] = '未知';
			}else{
				$arrInfo[$i]['stat'] = '忙';
			}
			$i++;
		}
		unset($i);

		$arrU = readU();
		$ename = $arrU["exten_cnName"];
		$pname = $arrU["phone_cnName"];

		foreach($arrInfo as &$val){
			if( !$val["cn_name"] ){
				$val["cn_name"] = $pname[$val["num"]];
			}
		}

		//dump($arrInfo);die;
		echo "(" .json_encode($arrInfo) .")";
	}


	//删掉参会号码
	function removePhone(){
		$_SESSION['meeting']['members'] = isset($_SESSION['meeting']['members'])?$_SESSION['meeting']['members']:Array();
		$arrNum = explode(',',$_GET['numStr']);//号码用逗号隔开
		//求差集
		$_SESSION['meeting']['members'] = array_diff($_SESSION['meeting']['members'],$arrNum);
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		getArrCache();

		$i = 0;$arrInfo = Array();
		foreach($_SESSION['meeting']['members'] AS $v){
			$arrInfo[$i] = Array();
			$arrInfo[$i]['num'] = $v;//电话号码
			$arrInfo[$i]['cn_name'] = '';
			foreach( $arrCacheWorkNo AS $val ){
				if( $val['extension'] == $v ){
					$arrInfo[$i]['cn_name'] = $val['cn_name']; //姓名
					break;
				}
			}
			$arrHint = $bmi->getHint($v);//状态
			if( $arrHint['stat'] == "Idle" ){
				$arrInfo[$i]['stat'] = '空闲';
			}elseif( $arrHint['stat'] == "Unavailable" ){
				$arrInfo[$i]['stat'] = '未注册';
			}elseif( $arrHint['stat'] == NULL ){
				$arrInfo[$i]['stat'] = '未知';
			}else{
				$arrInfo[$i]['stat'] = '忙';
			}
			$i++;
		}
		echo "(" .json_encode($arrInfo) .")";
	}

	function inviteMeeting(){
		header("Content-Type:text/html;charset=UTF-8");
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		if( isset($_GET['numStr']) ){
			$arr = explode(',',$_GET['numStr']);//号码用逗号隔开
			import('ORG.Pbx.bmi');
			$bmi = new bmi();
			$bmi->loadAGI();
			foreach( $arr AS $v){
				$request_parameters = Array(
					"Channel"       =>      "Local/$v@from-internal",
					"Exten"         =>      "888888",
					"Context"       =>      'bangian-meetme',
					"Priority"      =>      "1",
					"Timeout"       =>      "60000",
					"CallerID"      =>      "888888",
					"Async"			=>      "true",
					"Variable"		=>		"_exten=$exten,_ComingID=$v",
				);
				$bmi->asm->send_request('Originate',$request_parameters);
			}
		}
		echo "ok";
	}

	function muteSelected(){
		//header("Content-Type:text/html;charset=UTF-8");
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		$arrNum = explode(',',$_GET['numStr']);//号码用逗号隔开
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		foreach($arrNum AS $v){
			$bmi->loadAGI();
			$bmi->asm->Command("meetme mute {$exten} {$v}");
			//echo "{$exten} ";
		}
		//获取会议室号码状态
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			//dump($arrTemp);die;
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if(false!== strpos($v['state'],'Muted')){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$rowsList = count($arrMeeting) ? $arrMeeting : false;
		$arrMeet["total"] = count($arrMeeting);
		$arrMeet["rows"] = $rowsList;
		$return = Array();
		$return['res'] = 'ok';
		$return['data'] = $arrMeet;
		//dump($return);die;
		echo json_encode($return);
	}

	function unmuteSelected(){

		//header("Content-Type:text/html;charset=UTF-8");
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		$arrNum = explode(',',$_GET['numStr']);//号码用逗号隔开
		//dump($arrNum);die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		foreach($arrNum AS $v){
			$bmi->loadAGI();
			$bmi->asm->Command("meetme unmute {$exten} {$v}");
		}
		//获取会议室号码状态
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			//dump($arrTemp);die;
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if(false!== strpos($v['state'],'Muted')){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$rowsList = count($arrMeeting) ? $arrMeeting : false;
		$arrMeet["total"] = count($arrMeeting);
		$arrMeet["rows"] = $rowsList;
		$return = Array();
		$return['res'] = 'ok';
		$return['data'] = $arrMeet;
		//dump($return);die;
		echo json_encode($return);
	}

	function kickOne(){
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		$arrNum = explode(',',$_GET['numStr']);//号码用逗号隔开
		//dump($arrNum);die;
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		foreach($arrNum AS $v){
			$bmi->loadAGI();
			$bmi->asm->Command("meetme kick {$exten} {$v}");
		}
		sleep(3);
		//获取会议室号码状态
		$arrMeeting = Array();
		if( $arrTemp = $bmi->listMeeting2($exten) ){
			//dump($arrTemp);die;
			$arrMeeting = $arrTemp;
			foreach($arrMeeting AS &$v){
				if(false!== strpos($v['state'],'Muted')){
					$v['state'] = '静音';
				}else{
					$v['state'] = '正常';
				}
			}
		}
		$rowsList = count($arrMeeting) ? $arrMeeting : false;
		$arrMeet["total"] = count($arrMeeting);
		$arrMeet["rows"] = $rowsList;
		$return = Array();
		$return['res'] = 'ok';
		$return['data'] = $arrMeet;
		//dump($return);die;
		echo json_encode($return);
	}


	function kickAll(){
		$exten = $_SESSION['meeting']['exten']; //会议室号码
		unset($_SESSION['meeting']);
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$bmi->asm->Command("meetme kick {$exten} all");
		echo "YES";
	}

	function meetingLogout(){
		unset($_SESSION['meeting']);
		goBack("您已登出会议室！","index.php?m=Meeting&a=loginMeeting");
	}

	function meetingRecord(){
		checkLogin();
		$this->display();
	}

	function meetingRecordData(){
		$meeting_num = $_REQUEST["meeting_num"];
		$meeting_time = $_REQUEST["meeting_time"];

		$month = date("Y-m",strtotime($meeting_time));
		$day = date("d",strtotime($meeting_time));

		$path = "/var/spool/asterisk/meetme/".$month."/".$day."/".$meeting_num."/";
		$name = $path."*.WAV";
		$arrFile = glob($name);

		foreach($arrFile as $val){
			$uniqueid = trim(str_replace(".WAV","",array_pop(explode("/",$val))));
			$arrF[] = array(
				"path"=>$val,
				"exten"=>$meeting_num,
				"meeting_time"=>$meeting_time,
				"recording"=>"<a  href='javascript:void(0);' onclick=\"palyRecording("."'".$uniqueid."','".$month."','".$day."','".$meeting_num."'".")\" > 播放 </a>"."<a target='_blank' href='index.php?m=CDR&a=downloadMeeting&uniqueid=".$uniqueid."&month=".$month."&day=".$day."&meeting_num=" .$meeting_num."'> 下载 </a>",
			);
		}

		//dump($arrF);die;


		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrF);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrF AS &$v){
			if($i >= $start && $j < $length){
				$arrData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function listDir( $dir){
		 $return=array();
		 $d=dir($dir);
		 while(FALSE !== ($item = $d->read() )){
			if('.' ==$item || '..'==$item){
				continue;
			}

			if(is_file($dir . '/' .$item)){
				$return[] = $item;
			} elseif(is_dir($dir . '/' .$item)){
				$return[$item] =$this->listDir($dir . '/' .$item);
			}
		 }
		 $d->close();
		 return $return;
	}

	function meetingAllRecordData(){
		$meeting_num = $_REQUEST["meeting_num"];
		$meeting_time = $_REQUEST["meeting_time"];
		$meeting_time_end = $_REQUEST["meeting_time_end"];
		$type = $_REQUEST["type"];

		$directory = "/var/spool/asterisk/meetme/";
		$arrT = $this->listDir($directory);

		foreach($arrT as $key=>$val){
			foreach($val as $k=>$v){
				$arrD[] = array(
					"month"=>$key,
					"day"=>$k,
					"file"=>$v,
				);

			}
		}

		foreach($arrD as $val){
			foreach($val["file"] as $k=>$v){
				foreach($v as $vm){
					$uniqueid = trim(str_replace(".WAV","",$vm));
					$time = date("Y-m-d H:i:s",array_shift(explode(".",$vm)));
					$arrF[] = array(
						//"meeting_time"=>$val["month"]."-".$val["day"],
						"meeting_time"=>$time,
						"exten"=>$k,
						"uniqueid"=> trim(str_replace(".WAV","",$vm)),
						"recording"=>"<a  href='javascript:void(0);' onclick=\"palyRecording("."'".$uniqueid."','".$val["month"]."','".$val["day"]."','".$k."'".")\" > 播放 </a>"."<a target='_blank' href='index.php?m=CDR&a=downloadMeeting&uniqueid=".$uniqueid."&month=".$val["month"]."&day=".$val["day"]."&meeting_num=" .$k."'> 下载 </a>"."<a  href='javascript:void(0);' onclick=\"deleteRecording("."'".$uniqueid."','".$val["month"]."','".$val["day"]."','".$k."'".")\" > 删除 </a>",
					);
				}
			}
		}

		if($type == "search"){
			if($meeting_num != "请选择..."){
				foreach($arrF as $val){
					if( $val["exten"] == $meeting_num ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);

			if($meeting_time){
				foreach($arrF as $val){
					if( strtotime($val["meeting_time"]) >= strtotime($meeting_time) ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);

			if($meeting_time_end){
				foreach($arrF as $val){
					if( strtotime($val["meeting_time"]) <= strtotime($meeting_time_end) ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);

			if($meeting_time_end && $meeting_time && $meeting_num == "请选择..."){
				foreach($arrF as $val){
					if( strtotime($val["meeting_time"]) >= strtotime($meeting_time) && strtotime($val["meeting_time"]) <= strtotime($meeting_time_end) ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);

			if($meeting_time && $meeting_num != "请选择..." && !$meeting_time_end){
				foreach($arrF as $val){
					if( strtotime($val["meeting_time"]) >= strtotime($meeting_time) && $val["exten"] == $meeting_num ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);

			if(!$meeting_time && $meeting_num != "请选择..." && $meeting_time_end){
				foreach($arrF as $val){
					if( strtotime($val["meeting_time"]) <= strtotime($meeting_time_end) && $val["exten"] == $meeting_num ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);

			if( $meeting_time && $meeting_num != "请选择..." && $meeting_time_end ){
				foreach($arrF as $val){
					if( strtotime($val["meeting_time"]) >= strtotime($meeting_time) && strtotime($val["meeting_time"]) <= strtotime($meeting_time_end) && $val["exten"] == $meeting_num ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);


		}
		//dump($meeting_num);
		//dump($meeting_time);
		//dump($arrF);die;


		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrF);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrF AS &$v){
			if($i >= $start && $j < $length){
				$arrData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}

		//dump($arrT);
		//dump($arrData);die;

		$arrD = $this->array_sort($arrData,"meeting_time","desc","");

		$rowsList = count($arrD) ? $arrD : false;
		$arrM["total"] = $count;
		$arrM["rows"] = $rowsList;

		echo json_encode($arrM);
	}


	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	function meetingAllRecordDataBak(){
		$meeting_num = $_REQUEST["meeting_num"];
		$meeting_time = $_REQUEST["meeting_time"];
		$type = $_REQUEST["type"];

		$directory = "/var/spool/asterisk/meetme/";
		$arrT = $this->listDir($directory);

		foreach($arrT as $key=>$val){
			foreach($val as $k=>$v){
				$arrD[] = array(
					"month"=>$key,
					"day"=>$k,
					"file"=>$v,
				);

			}
		}

		foreach($arrD as $val){
			foreach($val["file"] as $k=>$v){
				foreach($v as $vm){
					$uniqueid = trim(str_replace(".WAV","",$vm));
					$arrF[] = array(
						"meeting_time"=>$val["month"]."-".$val["day"],
						"exten"=>$k,
						"uniqueid"=> trim(str_replace(".WAV","",$vm)),
						"recording"=>"<a  href='javascript:void(0);' onclick=\"palyRecording("."'".$uniqueid."','".$val["month"]."','".$val["day"]."','".$k."'".")\" > 播放 </a>"."<a target='_blank' href='index.php?m=CDR&a=downloadMeeting&uniqueid=".$uniqueid."&month=".$val["month"]."&day=".$val["day"]."&meeting_num=" .$k."'> 下载 </a>",
					);
				}
			}
		}

		if($type == "search"){
			if($meeting_num != "请选择..."){
				foreach($arrF as $val){
					if( $val["exten"] == $meeting_num ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);
			if($meeting_time){
				foreach($arrF as $val){
					if( $val["meeting_time"] == $meeting_time ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);
			if($meeting_time && $meeting_num != "请选择..."){
				foreach($arrF as $val){
					if( $val["meeting_time"] == $meeting_time && $val["exten"] == $meeting_num ){
						$arrF2[] = $val;
					}
				}
				$arrF = $arrF2;
			}
			unset($arrF2);
		}
		//dump($meeting_num);
		//dump($meeting_time);
		//dump($arrF);die;


		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrF);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrF AS &$v){
			if($i >= $start && $j < $length){
				$arrData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}

		//dump($arrT);
		//dump($arrData);die;


		$rowsList = count($arrData) ? $arrData : false;
		$arrM["total"] = $count;
		$arrM["rows"] = $rowsList;

		echo json_encode($arrM);
	}

	function deleteMeetingRecord(){
		$uniqueid = $_REQUEST['uniqueid'];
		$month = $_REQUEST['month'];
		$day = $_REQUEST['day'];
		$meeting_num = $_REQUEST["meeting_num"];
		$path = "/var/spool/asterisk/meetme/".$month."/".$day."/".$meeting_num."/";
		$WAVfile = $path.$uniqueid.".WAV";
		//echo $WAVfile;die;
		if(!file_exists($WAVfile)){
			//echo "录音文件不存在!!\n";die;
			echo json_encode(array('msg'=>"录音文件不存在"));die;
		}else{
			unlink($WAVfile);
			echo json_encode(array('success'=>true));
		}
	}

	function downloadAllRecording(){
		//dump($arrUF);die;
		$meeting_num = $_REQUEST["meeting_num"];
		$meeting_time = $_REQUEST["meeting_time"];

		$month = date("Y-m",strtotime($meeting_time));
		$day = date("d",strtotime($meeting_time));

		$path = "/var/spool/asterisk/meetme/".$month."/".$day."/".$meeting_num."/";
		$name = $path."*.WAV";
		$arrFile = glob($name);


		$cmd = "cd $path;tar -zcf meetingRecording.tar.gz * ";
		//dump($cmd);die;
		system($cmd);


		$monitorDir = '/var/spool/asterisk/monitor';
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Transfer-Encoding: Binary");
		header("Content-Disposition: attachment;filename=meetingRecording-" .$meeting_time.$meeting_num .".tar.gz");




	}

}

?>

