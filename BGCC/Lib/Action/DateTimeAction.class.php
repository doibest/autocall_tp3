<?php
class DateTimeAction extends Action{
	//日期/时间
	function DateTime(){
		checkLogin();
		$timezone = $_POST['timezone'];
		$datetime = $_POST['datetime'];
		if( $timezone ){
			//改时区
			$cmd1 = "sudo /opt/bin/dateconfig --timezone $timezone 2>&1";
			$output=$ret_val="";
			exec($cmd1,$output,$ret_val);
			if($ret_val == 0) {
				$this->assign('timezone_msg', L('System Timezone changed successfully!'));
			}else{
				$this->assign('timezone_msg', L('Failed to change timezone!')." - <br/>".implode('<br/>', $output));
			}
			//改时间
			if( $datetime ){
				//$datetime .= ":00";
				$cmd2 = "sudo /opt/bin/dateconfig --datetime '$datetime' 2>&1";
				$output=$ret_val="";
				exec($cmd2,$output,$ret_val);
				if ($ret_val == 0) {
					$this->assign('datetime_msg', L('System time changed successfully!'));
				}else{
					$this->assign('datetime_msg', L('System time can not be changed!')." - <br/>".implode('<br/>', $output));
				}
			}
		}
		$timeZone = "Asia/Shanghai"; //默认时区
		$fh = fopen('/etc/sysconfig/clock', 'r');
		if ($fh) {
			$infoZona = array();
			while (!feof($fh)) {
				$s = fgets($fh);
				$regs = NULL;
				if (ereg('^ZONE="(.*)"', $s, $regs))
					$timeZone = $regs[1];
				else $infoZona[] = $s;
			}
			fclose($fh);
		}

		$fh = fopen('/usr/share/zoneinfo/zone.tab', 'r');
		$tpl_arrZone = Array();
		if( $fh ){
			while ($row = fgetcsv($fh, 2048, "\t")) {
				if (count($row) >= 3 && $row[0]{0} != '#') $tpl_arrZone[]['name'] = $row[2];
			}
			fclose($fh);
			sort($tpl_arrZone);
		}
		$this->assign('arrZone',$tpl_arrZone);
		$this->assign('timeZone',$timeZone);


		date_default_timezone_set($timeZone);
		$this->assign('tpl_datetime',Date("Y-m-d H:i:s"));

		//分配增删改的权限
		$menuname = "Date/Time";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("edit",$priv['edit']);

		$this->display();
	}
}
?>
