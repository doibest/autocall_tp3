<?php
class UsersTrackRecordAction extends Action{
	//用户业绩列表
	function trackRecordList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$this->display();
	}

	//用户业绩列表
	function trackList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$this->display();
	}

	function trackRecordData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$user_name = $_REQUEST["user_name"];
		$name = $_REQUEST["name"];
		$company = $_REQUEST["company"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		$where .= empty($start_date) ? "" : " AND u.pay_date >= '$start_date'";
		$where .= empty($end_date) ? "" : " AND u.pay_date <= '$end_date'";
		$where .= empty($user_name) ? "" : " AND u.user_name = '$user_name'";
		$where .= empty($name) ? "" : " AND c.name like '%$name%'";
		$where .= empty($company) ? "" : " AND c.company like '%$company%'";

		$mod = M("users_track_record");
		$fields = "u.*,c.name,c.company";
		if($search_type != "xls"){
			$count = $mod->table("users_track_record u")->field($fields)->join("customer c on (u.customer_id = c.id)")->where($where)->count();
			import('ORG.Util.Page');
			$_GET["p"] = $_REQUEST["page"];
			if(!$_REQUEST["rows"]){
				$page_rows = $para_sys["page_rows"];
			}else{
				$page_rows = $_REQUEST["rows"];
			}
			$page = new Page($count,$page_rows);
		}

		if($search_type == "xls"){
			$arrData = $mod->order("pay_date desc")->table("users_track_record u")->field($fields)->join("customer c on (u.customer_id = c.id)")->where($where)->select();
		}else{
			$arrData = $mod->order("pay_date desc")->table("users_track_record u")->field($fields)->join("customer c on (u.customer_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["user_name"]];
		}

		//$arrFooter = $mod->field("sum(pay_amount) as pay_amount")->where($where)->find();
		$arrFooter = $mod->table("users_track_record u")->field("sum(u.pay_amount) as pay_amount")->join("customer c on (u.customer_id = c.id)")->where($where)->find();
		$arrFooter["user_name"] = "总计";
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('user_name','cn_name','pay_date','name','company','pay_amount','rate_cycle','rate_rate');
			$arrTitle = array('坐席工号','坐席姓名','交费日期','客户名称','公司名称','交费金额','计费周期','计费费率');
			$xls_count = count($arrField);
			$excelTiele = "用户业绩列表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter2;

		echo json_encode($arrT);
	}

	function insertTrackRecord(){
		$username = $_SESSION['user_info']['username'];
		$mod = M("users_track_record");
		$arrData = array(
			'create_user'=>$username,
			'create_time'=>date("Y-m-d H:i:s"),
			'user_name'=>$_REQUEST['user_name'],
			'customer_id'=>$_REQUEST['customer_id'],
			'pay_date'=>$_REQUEST['pay_date'],
			'pay_amount'=>$_REQUEST['pay_amount'],
			'rate_cycle'=>$_REQUEST['rate_cycle'],
			'rate_rate'=>$_REQUEST['rate_rate'],
		);
		$result = $mod->data($arrData)->add();
		if ($result){
			$up_sql = "UPDATE users_track_record u INNER JOIN customer c ON u.company=c.company SET u.customer_id=c.id,u.user_name=c.createuser WHERE u.user_name IS NULL OR u.user_name = ''";
			$mod->execute($up_sql);
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateTrackRecord(){
		$id = $_REQUEST['id'];
		$mod = M("users_track_record");
		$arrData = array(
			'user_name'=>$_REQUEST['user_name'],
			'customer_id'=>$_REQUEST['customer_id'],
			'pay_date'=>$_REQUEST['pay_date'],
			'pay_amount'=>$_REQUEST['pay_amount'],
			'rate_cycle'=>$_REQUEST['rate_cycle'],
			'rate_rate'=>$_REQUEST['rate_rate'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteTrackRecord(){
		$id = $_REQUEST["id"];
		$mod = M("users_track_record");
		$result = $mod->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function trackReportList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$start_date = date("Y-m")."-01";
		$end_date = date("Y-m-d");
		$this->assign("start_date",$start_date);
		$this->assign("end_date",$end_date);

		$this->display();
	}

	function trackReportData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		$where .= empty($start_date) ? "" : " AND DATE(pay_date) >= '$start_date'";
		$where .= empty($end_date) ? "" : " AND DATE(pay_date) <= '$end_date'";
		$mod = M("users_track_record");
		$fields = "company,DATE(pay_date) as pay_date,sum(pay_amount) as pay_amount";
		if($search_type != "xls"){
			$arrCount = $mod->Distinct(true)->field("DATE(pay_date)")->where($where)->select();
			$count = count($arrCount);
			import('ORG.Util.Page');
			$_GET["p"] = $_REQUEST["page"];
			if(!$_REQUEST["rows"]){
				$page_rows = $para_sys["page_rows"];
			}else{
				$page_rows = $_REQUEST["rows"];
			}
			$page = new Page($count,$page_rows);
		}

		if($search_type == "xls"){
			$arrData = $mod->order("DATE(pay_date) desc")->field($fields)->group("DATE(pay_date)")->where($where)->select();
		}else{
			$arrData = $mod->order("DATE(pay_date) desc")->field($fields)->limit($page->firstRow.','.$page->listRows)->group("DATE(pay_date)")->where($where)->select();
		}

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["user_name"]];
		}

		//$arrFooter = $mod->field("sum(pay_amount) as pay_amount")->where($where)->find();
		$arrFooter = $mod->table("users_track_record u")->field("sum(u.pay_amount) as pay_amount")->join("customer c on (u.customer_id = c.id)")->where($where)->find();
		$arrFooter["user_name"] = "总计";
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('pay_date','pay_amount');
			$arrTitle = array('交费日期','交费金额');
			$xls_count = count($arrField);
			$excelTiele = "用户业绩日报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter2;
		if($start_date){
			$arrT["start_date"] = $start_date;
		}
		if($end_date){
			$arrT["end_date"] = $end_date;
		}

		echo json_encode($arrT);
	}

	//坐席业绩报表
	function agentTrackList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$start_date = date("Y-m")."-01";
		$end_date = date("Y-m-d");
		$this->assign("start_date",$start_date);
		$this->assign("end_date",$end_date);

		$this->display();
	}

	function agentTrackData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		$where .= empty($start_date) ? "" : " AND DATE(pay_date) >= '$start_date'";
		$where .= empty($end_date) ? "" : " AND DATE(pay_date) <= '$end_date'";
		$mod = M("users_track_record");
		$fields = "company,user_name,sum(pay_amount) as pay_amount";
		if($search_type != "xls"){
			$arrCount = $mod->Distinct(true)->field("user_name")->where($where)->select();
			$count = count($arrCount);
			import('ORG.Util.Page');
			$_GET["p"] = $_REQUEST["page"];
			if(!$_REQUEST["rows"]){
				$page_rows = $para_sys["page_rows"];
			}else{
				$page_rows = $_REQUEST["rows"];
			}
			$page = new Page($count,$page_rows);
		}

		if($search_type == "xls"){
			$arrData = $mod->order("DATE(pay_date) desc")->field($fields)->group("user_name")->where($where)->select();
		}else{
			$arrData = $mod->order("DATE(pay_date) desc")->field($fields)->limit($page->firstRow.','.$page->listRows)->group("user_name")->where($where)->select();
		}

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["user_name"]];
		}

		//$arrFooter = $mod->field("sum(pay_amount) as pay_amount")->where($where)->find();
		$arrFooter = $mod->table("users_track_record u")->field("sum(u.pay_amount) as pay_amount")->join("customer c on (u.customer_id = c.id)")->where($where)->find();
		$arrFooter["user_name"] = "总计";
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('user_name','cn_name','pay_amount');
			$arrTitle = array('坐席工号','坐席姓名','交费金额');
			$xls_count = count($arrField);
			$excelTiele = "坐席业绩报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter2;
		if($start_date){
			$arrT["start_date"] = $start_date;
		}
		if($end_date){
			$arrT["end_date"] = $end_date;
		}

		echo json_encode($arrT);
	}

	//客户交费报表
	function customerPayList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$start_date = date("Y-m")."-01";
		$end_date = date("Y-m-d");
		$this->assign("start_date",$start_date);
		$this->assign("end_date",$end_date);

		$this->display();
	}

	function customerPayData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		$where .= empty($start_date) ? "" : " AND DATE(pay_date) >= '$start_date'";
		$where .= empty($end_date) ? "" : " AND DATE(pay_date) <= '$end_date'";
		$mod = M("users_track_record");
		$fields = "company,customer_id,user_name,sum(pay_amount) as pay_amount";
		if($search_type != "xls"){
			$arrCount = $mod->Distinct(true)->field("customer_id")->where($where)->select();
			$count = count($arrCount);
			import('ORG.Util.Page');
			$_GET["p"] = $_REQUEST["page"];
			if(!$_REQUEST["rows"]){
				$page_rows = $para_sys["page_rows"];
			}else{
				$page_rows = $_REQUEST["rows"];
			}
			$page = new Page($count,$page_rows);
		}

		if($search_type == "xls"){
			$arrData = $mod->order("sum(pay_amount) desc")->field($fields)->group("customer_id")->where($where)->select();
		}else{
			$arrData = $mod->order("sum(pay_amount) desc")->field($fields)->limit($page->firstRow.','.$page->listRows)->group("customer_id")->where($where)->select();
		}

		//$arrFooter = $mod->field("sum(pay_amount) as pay_amount")->where($where)->find();
		$arrFooter = $mod->table("users_track_record u")->field("sum(u.pay_amount) as pay_amount")->join("customer c on (u.customer_id = c.id)")->where($where)->find();
		$arrFooter["user_name"] = "总计";
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('company','pay_amount');
			$arrTitle = array('公司名称','交费金额');
			$xls_count = count($arrField);
			$excelTiele = "客户交费报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter2;
		if($start_date){
			$arrT["start_date"] = $start_date;
		}
		if($end_date){
			$arrT["end_date"] = $end_date;
		}

		echo json_encode($arrT);
	}


	//用户业绩列表
	function lineList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$this->display();
	}


	function lineData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$user_name = $_REQUEST["user_name"];
		$name = $_REQUEST["name"];
		$company = $_REQUEST["company"];
		$line_name = $_REQUEST["line_name"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		$where .= " AND u.company!=''";
		$where .= empty($user_name) ? "" : " AND u.user_name = '$user_name'";
		$where .= empty($name) ? "" : " AND c.name like '%$name%'";
		$where .= empty($company) ? "" : " AND c.company like '%$company%'";
		$where .= empty($line_name) ? "" : " AND u.line_name like '%$line_name%'";

		$mod = M("users_line_record");
		$fields = "u.*,c.name,c.company";
		if($search_type != "xls"){
			$count = $mod->table("users_line_record u")->field($fields)->join("customer c on (u.customer_id = c.id)")->where($where)->count();
			import('ORG.Util.Page');
			$_GET["p"] = $_REQUEST["page"];
			if(!$_REQUEST["rows"]){
				$page_rows = $para_sys["page_rows"];
			}else{
				$page_rows = $_REQUEST["rows"];
			}
			$page = new Page($count,$page_rows);
		}

		if($search_type == "xls"){
			$arrData = $mod->order("user_name desc")->table("users_line_record u")->field($fields)->join("customer c on (u.customer_id = c.id)")->where($where)->select();
		}else{
			$arrData = $mod->order("user_name desc")->table("users_line_record u")->field($fields)->join("customer c on (u.customer_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}
		//echo $mod->getLastSql();
		//dump($arrData);die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["user_name"]];
		}

		//$arrFooter = $mod->field("sum(pay_amount) as pay_amount")->where($where)->find();
		$arrFooter = $mod->table("users_line_record u")->field("sum(u.line_num) as line_num")->join("customer c on (u.customer_id = c.id)")->where($where)->find();
		$arrFooter["user_name"] = "总计";
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('user_name','cn_name','name','company','line_name','line_num');
			$arrTitle = array('坐席工号','坐席姓名','客户名称','公司名称','落地网关群组','线路数量');
			$xls_count = count($arrField);
			$excelTiele = "客户线路列表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter2;

		echo json_encode($arrT);
	}




	//客户利润报表
	function index(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$this->display();
	}

	//客户利润报表
	function profitReportList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$start_date = date("Y-m")."-01";
		$end_date = date("Y-m-d");
		$this->assign("start_date",$start_date);
		$this->assign("end_date",$end_date);

		$this->display();
	}

	function profitReportData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$company = $_REQUEST["company"];
		$user_name = $_REQUEST["user_name"];

		$where = "1 ";
		$where .= empty($start_date) ? "" : " AND u.settlement_date >= '$start_date'";
		$where .= empty($end_date) ? "" : " AND u.settlement_date <= '$end_date'";
		$where .= empty($user_name) ? "" : " AND u.user_name = '$user_name'";
		$where .= empty($company) ? "" : " AND c.company like '%$company%'";

		$mod = M("users_customer_profit");
		$fields = "u.*,c.name,c.company";
		if($search_type != "xls"){
			$count = $mod->table("users_customer_profit u")->field($fields)->join("customer c on (u.customer_id = c.id)")->where($where)->count();
			import('ORG.Util.Page');
			$_GET["p"] = $_REQUEST["page"];
			if(!$_REQUEST["rows"]){
				$page_rows = $para_sys["page_rows"];
			}else{
				$page_rows = $_REQUEST["rows"];
			}
			$page = new Page($count,$page_rows);
		}


		$sort = $_REQUEST["sort"];
		$order = $_REQUEST["order"];
		if($sort){
			$usort = "u.".$sort." ".$order;
		}else{
			$usort = "u.settlement_date desc";
		}


		if($search_type == "xls"){
			$arrData = $mod->order($usort)->table("users_customer_profit u")->field($fields)->join("customer c on (u.customer_id = c.id)")->where($where)->select();
		}else{
			$arrData = $mod->order($usort)->table("users_customer_profit u")->field($fields)->join("customer c on (u.customer_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}
		//echo $mod->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["user_name"]];
			$val["profit"] = $val["total_fee"]-$val["cost_money"];
			$val["rate_profit"] = sprintf("%.2f",($val["profit"]/$val["total_fee"])*100)."%";
		}

		$arrFooter = $mod->table("users_customer_profit u")->field("sum(u.total_fee) as total_fee,sum(cost_money) as cost_money,sum(total_fee-cost_money) as profit")->join("customer c on (u.customer_id = c.id)")->where($where)->find();
		$arrFooter["settlement_date"] = "总计";
		$arrFooter["rate_profit"] = sprintf("%.2f",($arrFooter["profit"]/$arrFooter["total_fee"])*100)."%";;
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('settlement_date','company','settlement_account','total_fee','cost_money','profit','rate_profit');
			$arrTitle = array('结算日期','公司名称','结算账户','费用总计','结算成本','利润','利润率');
			$xls_count = count($arrField);
			$excelTiele = "客户利润记录".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter2;
		if($start_date){
			$arrT["start_date"] = $start_date;
		}
		if($end_date){
			$arrT["end_date"] = $end_date;
		}

		echo json_encode($arrT);
	}

	function profitDateList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$start_date = date("Y-m")."-01";
		$end_date = date("Y-m-d");
		$this->assign("start_date",$start_date);
		$this->assign("end_date",$end_date);

		$this->display();
	}

	function profitDateData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		$where .= empty($start_date) ? "" : " AND DATE(settlement_date) >= '$start_date'";
		$where .= empty($end_date) ? "" : " AND DATE(settlement_date) <= '$end_date'";
		$mod = M("users_customer_profit");
		$fields = "company,DATE(settlement_date) as settlement_date,sum(total_fee) as total_fee,sum(cost_money) as cost_money";
		if($search_type != "xls"){
			$arrCount = $mod->Distinct(true)->field("DATE(settlement_date)")->where($where)->select();
			$count = count($arrCount);
			import('ORG.Util.Page');
			$_GET["p"] = $_REQUEST["page"];
			if(!$_REQUEST["rows"]){
				$page_rows = $para_sys["page_rows"];
			}else{
				$page_rows = $_REQUEST["rows"];
			}
			$page = new Page($count,$page_rows);
		}

		$sort = $_REQUEST["sort"];
		$order = $_REQUEST["order"];
		if($sort){
			if($sort == "rate_profit"){
				$usort = " SUM(total_fee-cost_money)/SUM(total_fee) ".$order;
			}else{
				$usort = $sort." ".$order;
			}
		}else{
			$usort = "settlement_date desc";
		}

		if($search_type == "xls"){
			$arrData = $mod->order($usort)->field($fields)->group("DATE(settlement_date)")->where($where)->select();
		}else{
			$arrData = $mod->order($usort)->field($fields)->limit($page->firstRow.','.$page->listRows)->group("DATE(settlement_date)")->where($where)->select();
		}
		//echo $mod->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["user_name"]];
			$val["profit"] = $val["total_fee"]-$val["cost_money"];
			$val["rate_profit"] = sprintf("%.2f",($val["profit"]/$val["total_fee"])*100)."%";
		}

		$arrFooter = $mod->field("sum(total_fee) as total_fee,sum(cost_money) as cost_money,sum(total_fee-cost_money) as profit")->where($where)->find();
		$arrFooter["settlement_date"] = "总计";
		$arrFooter["rate_profit"] = sprintf("%.2f",($arrFooter["profit"]/$arrFooter["total_fee"])*100)."%";;
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('settlement_date','total_fee','cost_money','profit','rate_profit');
			$arrTitle = array('结算日期','费用总计','结算成本','利润','利润率');
			$xls_count = count($arrField);
			$excelTiele = "客户利润日报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter2;
		if($start_date){
			$arrT["start_date"] = $start_date;
		}
		if($end_date){
			$arrT["end_date"] = $end_date;
		}

		echo json_encode($arrT);
	}

	function profitUserList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$start_date = date("Y-m")."-01";
		$end_date = date("Y-m-d");
		$this->assign("start_date",$start_date);
		$this->assign("end_date",$end_date);

		$this->display();
	}

	function profitUserData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		$where .= empty($start_date) ? "" : " AND DATE(settlement_date) >= '$start_date'";
		$where .= empty($end_date) ? "" : " AND DATE(settlement_date) <= '$end_date'";
		$mod = M("users_customer_profit");
		$fields = "company,settlement_date,sum(total_fee) as total_fee,sum(cost_money) as cost_money";
		if($search_type != "xls"){
			$arrCount = $mod->Distinct(true)->field("company")->where($where)->select();
			$count = count($arrCount);
			import('ORG.Util.Page');
			$_GET["p"] = $_REQUEST["page"];
			if(!$_REQUEST["rows"]){
				$page_rows = $para_sys["page_rows"];
			}else{
				$page_rows = $_REQUEST["rows"];
			}
			$page = new Page($count,$page_rows);
		}

		$sort = $_REQUEST["sort"];
		$order = $_REQUEST["order"];
		if($sort){
			if($sort == "rate_profit"){
				$usort = " SUM(total_fee-cost_money)/SUM(total_fee) ".$order;
			}else{
				$usort = $sort." ".$order;
			}
		}else{
			$usort = "total_fee desc";
		}

		if($search_type == "xls"){
			$arrData = $mod->order($usort)->field($fields)->group("company")->where($where)->select();
		}else{
			$arrData = $mod->order($usort)->field($fields)->limit($page->firstRow.','.$page->listRows)->group("company")->where($where)->select();
		}
		//echo $mod->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["user_name"]];
			$val["profit"] = $val["total_fee"]-$val["cost_money"];
			$val["rate_profit"] = sprintf("%.2f",($val["profit"]/$val["total_fee"])*100)."%";
		}

		$arrFooter = $mod->field("sum(total_fee) as total_fee,sum(cost_money) as cost_money,sum(total_fee-cost_money) as profit")->where($where)->find();
		$arrFooter["settlement_date"] = "总计";
		$arrFooter["rate_profit"] = sprintf("%.2f",($arrFooter["profit"]/$arrFooter["total_fee"])*100)."%";;
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('settlement_date','total_fee','cost_money','profit','rate_profit');
			$arrTitle = array('结算日期','费用总计','结算成本','利润','利润率');
			$xls_count = count($arrField);
			$excelTiele = "客户利润日报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter2;
		if($start_date){
			$arrT["start_date"] = $start_date;
		}
		if($end_date){
			$arrT["end_date"] = $end_date;
		}

		echo json_encode($arrT);
	}

	function profitLineList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Users Track Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$start_date = date("Y-m")."-01";
		$end_date = date("Y-m-d");
		$this->assign("start_date",$start_date);
		$this->assign("end_date",$end_date);

		$this->display();
	}

	function profitLineData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		$where .= empty($start_date) ? "" : " AND DATE(settlement_date) >= '$start_date'";
		$where .= empty($end_date) ? "" : " AND DATE(settlement_date) <= '$end_date'";
		$mod = M("users_customer_profit");
		$fields = "settlement_account,settlement_date,sum(total_fee) as total_fee,sum(cost_money) as cost_money";
		if($search_type != "xls"){
			$arrCount = $mod->Distinct(true)->field("settlement_account")->where($where)->select();
			$count = count($arrCount);
			import('ORG.Util.Page');
			$_GET["p"] = $_REQUEST["page"];
			if(!$_REQUEST["rows"]){
				$page_rows = $para_sys["page_rows"];
			}else{
				$page_rows = $_REQUEST["rows"];
			}
			$page = new Page($count,$page_rows);
		}

		$sort = $_REQUEST["sort"];
		$order = $_REQUEST["order"];
		if($sort){
			if($sort == "rate_profit"){
				$usort = " SUM(total_fee-cost_money)/SUM(total_fee) ".$order;
			}else{
				$usort = $sort." ".$order;
			}
		}else{
			$usort = "total_fee desc";
		}

		if($search_type == "xls"){
			$arrData = $mod->order($usort)->field($fields)->group("settlement_account")->where($where)->select();
		}else{
			$arrData = $mod->order($usort)->field($fields)->limit($page->firstRow.','.$page->listRows)->group("settlement_account")->where($where)->select();
		}
		//echo $mod->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["user_name"]];
			$val["profit"] = $val["total_fee"]-$val["cost_money"];
			$val["rate_profit"] = sprintf("%.2f",($val["profit"]/$val["total_fee"])*100)."%";
		}

		$arrFooter = $mod->field("sum(total_fee) as total_fee,sum(cost_money) as cost_money,sum(total_fee-cost_money) as profit")->where($where)->find();
		$arrFooter["settlement_date"] = "总计";
		$arrFooter["rate_profit"] = sprintf("%.2f",($arrFooter["profit"]/$arrFooter["total_fee"])*100)."%";;
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('settlement_date','total_fee','cost_money','profit','rate_profit');
			$arrTitle = array('结算日期','费用总计','结算成本','利润','利润率');
			$xls_count = count($arrField);
			$excelTiele = "线路利润报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["footer"] = $arrFooter2;
		if($start_date){
			$arrT["start_date"] = $start_date;
		}
		if($end_date){
			$arrT["end_date"] = $end_date;
		}

		echo json_encode($arrT);
	}


}
?>
