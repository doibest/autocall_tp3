<?php
class LevelSettingAction extends Action{
	function levelList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Level Type Setting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		$this->display();
	}

	function version(){
		$file = @file_get_contents("BGCC/Conf/filename.txt");
		//$file = "robotupdate-20140522-635-20140626-725.tar.gz";
		$version_num = array_pop( explode("-",array_shift( explode(".",$file) )) );
		$time = explode("-",array_shift( explode(".",$file) ));

		$sys = new Model("system_set");
		$num = $sys->where("name = 'version_num'")->save(array("svalue"=>$time[3]."-".$version_num));

		$filename = "/var/www/html/BGCC/Conf/sqlUpdate.php";
		if(file_exists($filename)){
			$conn = M();
			$config = require '/var/www/html/BGCC/Conf/sqlUpdate.php';

			foreach($config as $key=>$v){
				if($key > $time[2] && $key <= $time[4]){
					foreach(explode(";",$v) as $vm){
						$conn->query($vm);
					}
				}
			}
			unlink('/var/www/html/BGCC/Conf/sqlUpdate.php');
		}

		//导入sql文件=====开始===================
		$files = scandir("/var/www/html/BGCC/Conf/");
		array_shift($files);
		array_shift($files);
		for($i = 0;$i < count($files);$i++){
			if(preg_match('/(.*)(\.)sql$/i',$files[$i])){
				$filenames[] = $files[$i];
			}
			//echo $files[$i]."<br>";
		}
		if($filenames){
			foreach($filenames as $val){
				$arrFile[] = explode(".",$val);
			}
			if($arrFile){
				import('ORG.Util.DBManager');
				$dbM = new DBManager(C("DB_HOST"),C("DB_USER"),C("DB_PWD"),C("DB_NAME"));
				foreach($arrFile as $val){
					$dbM->createFromFile('/var/www/html/BGCC/Conf/'.$val[0].'.sql',null,'');
					unlink('/var/www/html/BGCC/Conf/'.$val[0].'.sql');
				}
			}
		}
		//导入sql文件=====结束===================

		//导入第三方类库====开始=================
		if(file_exists("/var/www/html/BGCC/Conf/VersionFunction.class.php")){
			$version = require '/var/www/html/BGCC/Conf/VersionFunction.class.php';
			$arr =  new VersionFunction();
			$aryFunc = get_class_methods($arr);   //返回由类的方法名组成的数组
			$aryAttr = get_class_vars(get_class($arr));  ///返回由类的属性名组成的数组
			/*
			$obj = new VersionFunction();
			$ref = new ReflectionObject($obj);
			$aa = $ref->getProperties();
			$tt = $ref->getMethods();
			*/

			foreach($aryFunc as $val){
				$func[] = explode("_",$val);
			}

			foreach($func as &$val){
				$val['name'] = $val[0]."_".$val[1];
				if($val[1] > $time[2] && $val[1] <= $time[4]){
					$arr->$val['name']();
				}
			}
			unlink('/var/www/html/BGCC/Conf/VersionFunction.class.php');
		}
		//导入第三方类库====结束=================

		if(file_exists("/var/www/html/other.tar.gz")){
			exec("sudo /bin/tar  --overwrite -zxf /var/www/html/other.tar.gz -C /");
			unlink('/var/www/html/other.tar.gz');
		}

		unlink("/var/www/html/version.php");
		unlink("/var/www/html/version_msg.php");
		$arrConf = require '/var/www/html/BGCC/Conf/system.php';
		$arrConf['version_num'] = $time[3]."-".$time[4];
		$content = "<?php\nreturn " .var_export($arrConf,TRUE) .";\n ?>";
		file_put_contents('/var/www/html/BGCC/Conf/system.php',$content);
	}

	function smsTest(){
		require_once('include/nusoap/lib/nusoap.php');
		  $client   =   new   soapclient('http://122.193.249.107:8050/ws/SmsWS.asmx?WSDL',   true);
		  //$client   =   new   nusoap_client('http://122.193.249.107:8050/ws/SmsWS.asmx?WSDL',   true);
		  $err   =   $client->getError();
		  if   ($err)   {
			echo   '<h2>Constructor   error</h2><pre>'   .   $err   .   '</pre>';
		  }


		   $params   =   array('operatorid'   =>   '0007', 'password'   =>   'CE1790C6C9B12B5B0035D86E5E4896BF', 'paradestaddr'   =>   '18807194375,', 'paracontent'   =>   'test1111', 'sendtime'   =>   '2014-05-13 15:20:00', 'paraserviceid'   =>   'EIE', 'remark1'   =>   'null', 'remark2'   =>   'null');
		  $result   =   $client->call('SMSSend',   array('parameters'   =>   $params),   '',   '',   false,   true,'document','encoded');

		  var_dump($result);

	}
	/*
	function smsTest(){
		$date = date("Y-m-d H:i:s");
		header("Content-type:text/html;charset=utf-8");
		require_once('ThinkPHP/Extend/Library/ORG/Nusoap/nusoap/nusoap.php');
		$nusoap_client = new nusoap_client("http://SMS.Uhuocn.com:8050/ws/SmsWs.asmx",false);
		$nusoap_client->soap_defencoding = 'utf-8';
		$nusoap_client->decode_utf8 = false;
		$err = $nusoap_client->getError();
		if ($err) {
			echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
			echo '<h2>Debug</h2><pre>' . htmlspecialchars($nusoap_client->getDebug(), ENT_QUOTES) . '</pre>';
			exit();
		}

		$params = array("0007","CE1790C6C9B12B5B0035D86E5E4896BF","13417529054","test111",$date,"EIE","","");
		$result = $nusoap_client->call('SMSSend',$params);
		echo '<p/>';
		echo 'Request:';
		echo '<pre>',htmlspecialchars($nusoap_client->request,ENT_QUOTES),'</pre>';
		echo 'Response:';
		echo '<pre>',htmlspecialchars($nusoap_client->response,ENT_QUOTES ),'</pre>';
		dump($result);die;

	}
	*/

	/*
	function smsTest(){
		include("ThinkPHP/Extend/Library/ORG/Nusoap/SoapDiscovery.class.php");
		//include "include/nusoap/nusoap.php";
		require_once("include/nusoap/nusoap.php");
		$date = date("Y-m-d H:i:s");
		$url = "http://SMS.Uhuocn.com:8050/ws/SmsWs.asmx";
		$soap = new soapclient($url);
		$tt = $soap->SMSMultiSend("0007","CE1790C6C9B12B5B0035D86E5E4896BF","13417529054","test111",$date,"EIE","","");

		dump($tt);die;
	}
	*/

	function test(){
		header("Content-Type:text/html; charset=utf-8");
		set_time_limit(0);
		$arrData = @file_get_contents("http://al.robot365.com/Index/Conf/warrant.php");
		$arrF = json_decode($arrData,true);
		$checkRole = getSysinfo();
		foreach($arrF as &$val){
			$val["mac_addr1"] = str_replace(":","",$val['mac_addr1']);
			$val["mac_addr2"] = str_replace(":","",$val['mac_addr2']);

			if( ($checkRole[9] == $val["mac_addr1"] || $checkRole[9] == $val["mac_addr2"]) && $val["warrant_status"] == "N" ){
				$warrantArr[] = $val;
			}
		}
		$savePath = '/var/www/html/BGCC/Conf';
		//  保存一天
		$lifeTime = 24 * 3600;
		session_save_path($savePath);
		session_set_cookie_params($lifeTime);
		session_start();
		if(count($warrantArr)>0){
			echo "N";
			$_SESSION["warrant_role"] = "N";
		}else{
			echo "Y";
			$_SESSION["warrant_role"] = "Y";
		}
		//b63df5bf28a6
		//dump($checkRole);
		dump($_SESSION);
		//dump($arrF);
	}

	function levelDataList(){
		$level = new Model("leveltype");
		import('ORG.Util.Page');
		$count = $level->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$levelData = $level->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($levelData) ? $levelData : false;
		$arrLevel["total"] = $count;
		$arrLevel["rows"] = $rowsList;

		echo json_encode($arrLevel);
	}

	function insertlevel(){
		$level = new Model("leveltype");
		$arrData = array(
			"levelname"=>$_REQUEST["levelname"],
		);
		$result = $level->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function updatelevel(){
		$id = $_REQUEST["id"];
		$level = new Model("leveltype");
		$arrData = array(
			"levelname"=>$_REQUEST["levelname"],
		);
		$result = $level->data($arrData)->where("id = $id")->save();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function deletelevel(){
		$id = $_REQUEST["id"];
		$level = new Model("leveltype");
		$result = $level->where("id = $id")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}


	function backupData(){
		import('ORG.Backup2.DbBak');
		import('ORG.Backup2.TableBak');

		$connectid = mysql_connect(C("DB_HOST"),C("DB_USER"),C("DB_PWD"));
		mysql_query("set names utf8"); //声明字符集
		$backupDir = 'include/backup';
		$DbBak = new DbBak($connectid,$backupDir);
		//$DbBak->backupDb('bgcrm',array('faq_content','faq_type','region'));
		$DbBak->backupDb('asterisk');
	}

	function restoreData(){
		import('ORG.Backup2.DbBak');
		import('ORG.Backup2.TableBak');

		$connectid = mysql_connect(C("DB_HOST"),C("DB_USER"),C("DB_PWD"));
		mysql_query("set names utf8"); //声明字符集
		$backupDir = 'include/backup';
		$DbBak = new DbBak($connectid,$backupDir);
		$DbBak->restoreDb('bgcrm',array('faq_content','faq_type','region','classtype'));
		//$DbBak->restoreDb('bgcrm','classtype');
	}



	function clearTable(){
		$sales_task = M('sales_task');
		$arrTaskId = $sales_task->select();
		$mod = M();
		foreach($arrTaskId as $vm){
			$sql = "drop table sales_source_".$vm['id'];
			$add = $mod->execute($sql);

			$sql2 = "drop table sales_contact_history_".$vm['id'];
			$add2 = $mod->execute($sql2);

			$sql3 = "drop table sales_cdr_".$vm['id'];
			$add3 = $mod->execute($sql3);

			$sql4 = "drop table sales_proposal_record_".$vm['id'];
			$add4 = $mod->execute($sql4);

			$sql5 = "drop table task_result_status_".$vm['id'];
			$add5 = $mod->execute($sql5);

		}
	}


	function xmltest(){
		$xml = "<xml><ToUserName><![CDATA[gh_34ff33e9c158]]></ToUserName>
<FromUserName><![CDATA[oAVUOuFznCr5KVQJP-pwxb0JKQHk]]></FromUserName>
<CreateTime>1406791858</CreateTime>
<MsgType><![CDATA[voice]]></MsgType>
<MediaId><![CDATA[m4SklOzbRQZDnAjKaX7QRflCHcncTnKGXsQrB_K0jnWQke_8p5DWNs77duKzLM-r]]></MediaId>
<Format><![CDATA[amr]]></Format>
<MsgId>6042125022589712580</MsgId>
<Recognition><![CDATA[]]></Recognition>
</xml>";
		$xml2 = file_get_contents("/var/www/html/BGCC/Conf/changelog.xml");
		$dom = new DOMDocument();
		$dom->loadXML($xml);
		$aa = array();
		$aa = $this->getArray($dom->documentElement);

		$tt = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
		$arr = $this->objectToArray($tt);
		dump($arr);
		dump($aa);
		die;
	}


	//将对象转换成数组
	function objectToArray($obj){
		$_arr = is_object($obj) ? get_object_vars($obj) :$obj;
		//dump($_arr);die;
		foreach ($_arr as $key=>$val){
			$val = (is_array($val) || is_object($val)) ? $this->objectToArray($val):$val;
			$arr[$key] = $val;
		}
		return $arr;
	}

	//解析xml文件
	function getArray($node){
		$array = false;

		if ($node->hasAttributes()) {
			foreach ($node->attributes as $attr) {
				$array[$attr->nodeName] = $attr->nodeValue;
			}
		}

		if ($node->hasChildNodes()) {
			if ($node->childNodes->length == 1) {
				$array[$node->firstChild->nodeName] = $this->getArray($node->firstChild);
			} else {
				foreach ($node->childNodes as $childNode) {
					if ($childNode->nodeType != XML_TEXT_NODE) {
						$array[$childNode->nodeName][] = $this->getArray($childNode);
					}
				}
			}
		} else {
			return $node->nodeValue;
		}
		return $array;
	}

	function getxml(){
		$this->display();
	}

	function getxmlData(){
		set_time_limit(0);
		ini_set('memory_limit','400M');


		vendor("PHPExcel176.PHPExcel");
		//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array();
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($_FILES["customer_name"]["tmp_name"]);

		$indata = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组

		foreach($indata as $val){
			if(is_numeric($val[0]) && strlen($val[0]) > 5){
				$arrF[] = $val[0]."@qq.com";
			}
		}
		$str = implode("<br>",$arrF);
		echo json_encode(array('success'=>true,'msg'=>$str));



	}

	function importSql(){
		if(file_exists("/var/www/html/BGCC/Conf/chat.sql")){
			import('ORG.Util.DBManager');
			$dbM = new DBManager(C("DB_HOST"),C("DB_USER"),C("DB_PWD"),C("DB_NAME"));
			$dbM->createFromFile('/var/www/html/BGCC/Conf/chat.sql',null,'');
			//unlink('/var/www/html/BGCC/Conf/news1.sql');
		}
	}
}

?>

