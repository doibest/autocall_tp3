<?php
class BillingAction extends Action{
	//运营商、代理商、用户商 计费
    function operatorsRate(){
		checkLogin();

        if($_GET['type']==1){
            $title = "运营商费率";
            $this->assign('title',$title);
        }elseif($_GET['type']==2){
            $title = "代理商费率";
            $this->assign('title',$title);
        } else{
            $title = "用户费率";
            $this->assign('title',$title);
        }
		//$this->("Lang_From",Lang("From") );
		//$this->("Lang_To", Lang("To") );

		//分配增删改的权限
		$menuname = "Call Bill";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		//dump($priv['Explode Excel']);
		$this->assign("excel",$priv['excel']);

        $this->display();
    }

	function operatorsRateData(){
		$type = $_REQUEST["type"] ? $_REQUEST["type"] : "1" ;
        $start_time = $_REQUEST['start_time'];
        $end_time = $_REQUEST['end_time'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $dept_id = $_REQUEST['dept_id'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arr = $_REQUEST;
		foreach($arr as $k=>$v){
			if($v && $k != "page"  && $k != "rows" && $k != "type"  && $k != "searchmethod" ){
				$arrS[$k] = $v;
			}
		}

		$arrDep = $this->getDepTreeArray();
		$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$searchDeptId = rtrim($deptSet,",");
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptId = rtrim($deptst,",");

        $where = "1 ";
		$where .= " AND r.call_type='pbx'";
        if( $start_time )$where .= "AND calldate > '$start_time:00' ";
        if( $end_time )$where .= "AND calldate < '$end_time:00' ";
        if( $searchmethod == "equal" ){ //精确查询，条件语句用等号
            if( $src )$where .= "AND src='$src' ";
            if( $dst )$where .= "AND dst='$dst' ";
        }else{          //模糊查询，条件语句用like
            if( $src )$where .= "AND src like '%$src%' ";
            if( $dst )$where .= "AND dst like '%$dst%' ";
        }
		$where .= empty($dept_id) ? "" :" AND c.dept_id IN ($searchDeptId)";
		if($username != "admin"){
			if(!$arrS){
				$where .= " AND c.dept_id IN ($deptId)";
			}
		}
		//dump($where);
		//dump($arr);die;
        $where .= " AND r.rate_prefix IS NOT NULL AND LENGTH(dst)>4 AND r.rate_type=$type";
		$where .= " AND c.disposition='ANSWERED'";
		$where .= " AND c.calltype='OUT'";

		$cdr=new Model('asteriskcdrdb.cdr');
        $totalOpRete = $cdr->table("asteriskcdrdb.cdr c")->order('calldate desc')->field("c.calldate AS calldate,c.dept_id,c.src AS src,c.dst AS dst,c.dstchannel AS dstchannel,c.billsec AS billsec,r.rate_prefix,r.rate_money AS rate_money,TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,2) AS amoney")->join("bgcrm.rate r ON (SUBSTR(c.dst,1,LENGTH(r.rate_prefix)) = r.rate_prefix)" )->where($where)->select();
		$count = count($totalOpRete);

		foreach($totalOpRete as $val){
			$total_money[] = $val["amoney"];
		}

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$resultOpRate=$cdr->table("asteriskcdrdb.cdr c")->order('calldate desc')->field("c.calldate AS calldate,c.dept_id,c.workno,c.src AS src,c.dst AS dst,c.dstchannel AS dstchannel,c.billsec AS billsec,r.rate_prefix,r.rate_money AS rate_money,TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,3) AS amoney")->join("bgcrm.rate r ON (SUBSTR(c.dst,1,LENGTH(r.rate_prefix)) = r.rate_prefix)" )->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		//echo $cdr->getLastSql();die;
		$userArr = readU();
		$deptName = $userArr["deptId_name"];
		$deptUserName = $userArr["deptName_user"];
		$cn_user = $userArr["cn_user"];

		$i = 0;
		foreach($resultOpRate as $vm){
			$resultOpRate[$i]["billsec"] = sprintf("%02d",intval($vm["billsec"]/3600)).":".sprintf("%02d",intval(($vm["billsec"]%3600)/60)).":".sprintf("%02d",intval((($vm[billsec]%3600)%60)));
			$resultOpRate[$i]["dept_name"] = $deptName[$vm["dept_id"]];
			//$resultOpRate[$i]["dept_name"] = $deptUserName[$vm["workno"]];
			$resultOpRate[$i]["cn_name"] = $cn_user[$vm["workno"]];
			$money[] = $vm["amoney"];
			$i++;
		}
		unset($i);
		//dump($userArr);die;

		$pageMoney = array_sum($money);  //单页费用
		$totalMoney = array_sum($total_money); //总费用
		$en_amondy = L("Total cost of");
		//$tmp_mon = array(array("amoney"=>$en_amondy.": ".$totalMoney."  该页费用：".$pageMoney));
		$tmp_mon = array(array("amoney"=>$en_amondy.": ".$totalMoney));

		$rowsList = count($resultOpRate) ? $resultOpRate : false;
		$arrBilling["total"] = $count;
		$arrBilling["rows"] = $rowsList;
		$arrBilling["footer"] = $tmp_mon;

		echo json_encode($arrBilling);
	}

    //导出运营商、代理商、用户商 计费的Excel
    function  timeRateDbToExcel(){

        $cdr=new Model('asteriskcdrdb.cdr');
        $type = $_REQUEST["type"] ? $_REQUEST["type"] : "1" ;
        $start_time = $_REQUEST['start_time'];
        $end_time = $_REQUEST['end_time'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";

        $where = "1 ";
        if( $start_time )$where .= "AND calldate > '$start_time:00' ";
        if( $end_time )$where .= "AND calldate < '$end_time:00' ";
        if( $searchmethod == "equal" ){ //精确查询，条件语句用等号
            if( $src )$where .= "AND src='$src' ";
            if( $dst )$where .= "AND dst='$dst' ";
        }else{          //模糊查询，条件语句用like
            if( $src )$where .= "AND src like '%$src%' ";
            if( $dst )$where .= "AND dst like '%$dst%' ";
        }
        $where .= " AND r.rate_prefix IS NOT NULL AND LENGTH(dst)>4 AND r.rate_type=$type";
        $rs = $cdr->table("asteriskcdrdb.cdr c")->order('calldate desc')->field("c.calldate AS calldate,c.src AS src,c.dst AS dst,c.dstchannel AS dstchannel,c.billsec AS billsec,r.rate_prefix,r.rate_money AS rate_money,TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,2) AS amoney")->join("bgcrm.rate r ON (SUBSTR(c.dst,1,LENGTH(r.rate_prefix)) = r.rate_prefix)" )->where($where)->select();

        if($_REQUEST['type']==1){
            $filename ="运营商费率";
        }elseif($_REQUEST['type']==2){
            $filename ="代理商费率";
        } else{
            $filename ="用户费率";
        }
		//dump($rs);die;
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type:text/html;charset=UTF-8");
		$filename = iconv("utf-8","gb2312",$filename);
        header("Content-Disposition: attachment;filename=$filename.xls");
        header("Content-Transfer-Encoding: binary");

        xlsBOF();
        $start_row	=	0;

        xlsWriteLabel($start_row,0,utf2gb("通话时间"));
        xlsWriteLabel($start_row,1,utf2gb("主叫号码"));
        xlsWriteLabel($start_row,2,utf2gb("被叫号码"));
        xlsWriteLabel($start_row,3,utf2gb("通道"));
        xlsWriteLabel($start_row,4,utf2gb("通话时长"));
        xlsWriteLabel($start_row,5,utf2gb("前缀"));
        xlsWriteLabel($start_row,6,utf2gb("费率"));
        xlsWriteLabel($start_row,7,utf2gb("费用"));

        $start_row++;

        foreach($rs as $val)  {
            $val['billsec'] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));
            xlsWriteLabel($start_row,0,utf2gb($val["calldate"]));
            xlsWriteLabel($start_row,1,utf2gb($val["src"]));
            xlsWriteLabel($start_row,2,utf2gb($val["dst"]));
            xlsWriteLabel($start_row,3,utf2gb($val["dstchannel"]));
            xlsWriteLabel($start_row,4,utf2gb($val["billsec"]));
            xlsWriteLabel($start_row,5,utf2gb($val["rate_prefix"]));
            xlsWriteLabel($start_row,6,utf2gb($val["rate_money"]));
            xlsWriteNumber($start_row,7,utf2gb($val["amoney"]));
            $start_row++;
        }
        xlsEOF();
    }



    //费率报表
    function rateStatements(){
		checkLogin();
        import("ORG.Util.Page");
        $cdr=new Model('asteriskcdrdb.cdr');
        $start_time = $_REQUEST['start_time'];
        $end_time = $_REQUEST['end_time'];
        $cn_name = $_REQUEST['cn_name'];


        $where = "1 ";
        if( $start_time )$where .= "AND calldate > '$start_time:00' ";
        if( $end_time )$where .= "AND calldate < '$end_time:00' ";
        if( $cn_name )$where .= "AND u.cn_name='$cn_name' ";
        $where .= " AND r.rate_prefix IS NOT NULL AND LENGTH(dst)>4 AND LENGTH(src)<5 AND r.rate_type=1";

        $count = $cdr->table("asteriskcdrdb.cdr c")->order('c.calldate desc')->field('u.cn_name AS cn_name,d.d_name AS d_name,c.src AS src,SUM(c.billsec) AS sumbillsec,r.rate_money AS rate_money,SUM(TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,2)) AS smoney')->join("bgcrm.rate r ON (SUBSTR(c.dst,1,LENGTH(r.rate_prefix)) = r.rate_prefix)" )->join('bgcrm.department d on c.d_id=d.d_id')->join('bgcrm.users u on c.username=u.username')->where($where)->group("src")->select();
        $count = count($count);
        $para_sys = readS();   //从缓存中读出系统参数
        $page = new Page($count,$para_sys["page_rows"]);
        $page->setConfig("header",L("Records"));
		$page->setConfig("prev",L("Prev"));
		$page->setConfig("next",L("Next"));
		$page->setConfig("first",L("First"));
		$page->setConfig("last",L("Last"));
		$page->setConfig('theme','<span>  %totalRow%%header% &nbsp;  %nowPage%/%totalPage%</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');

        $page->parameter = "&";
        if( $start_time ){$page->parameter .= "start_time=". urlencode($start_time) ."&";}
        if( $end_time ){$page->parameter .= "end_time=". urlencode($end_time) ."&";}
        if( $cn_name ){$page->parameter .= "cn_name=". urlencode($cn_name) ."&";}
        $show= $page->show();
        $this->assign('page',$show);
        $resultOpRate =  $cdr->table("asteriskcdrdb.cdr c")->order('c.calldate desc')->field('count(*) as callcount,u.cn_name AS cn_name,d.d_name AS d_name,c.src AS src,SUM(c.billsec) AS sumbillsec,r.rate_money AS rate_money,SUM(TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,2)) AS smoney')->join("bgcrm.rate r ON (SUBSTR(c.dst,1,LENGTH(r.rate_prefix)) = r.rate_prefix)" )->join('bgcrm.department d on c.d_id=d.d_id')->join('bgcrm.users u on c.username=u.username')->limit($page->firstRow.','.$page->listRows)->where($where)->group('src')->select();
        $sql =  $cdr -> getLastSql();
        $this->assign('statements',$resultOpRate);

        $rate = new RateModel();
        $resultRate = $rate->select();

        $users = new UsersModel();
        $resultuser = $users->select();
        $this->assign('userlist',$resultuser);
        $this->assign('ratelist',$resultRate);
        $this->assign("start_time",$start_time);
        $this->assign("end_time",$end_time);
        $this->assign("cn_name",$cn_name);
        $this->display();
    }


    //导出费率报表的Excel
    function  rateStatementsDbToExcel(){
        import("ORG.Util.Page");
        $cdr=new Model('asteriskcdrdb.cdr');
        $start_time = $_REQUEST['start_time'];
        $end_time = $_REQUEST['end_time'];
        $cn_name = $_REQUEST['cn_name'];

        $where = "1 ";
        if( $start_time )$where .= "AND calldate > '$start_time:00' ";
        if( $end_time )$where .= "AND calldate < '$end_time:00' ";
        if( $cn_name )$where .= "AND u.cn_name='$cn_name' ";
        $where .= " AND r.rate_prefix IS NOT NULL AND LENGTH(dst)>4 AND LENGTH(src)<5 AND r.rate_type=1";
        $rs =  $cdr->table("asteriskcdrdb.cdr c")->order('c.calldate desc')->field('count(*) as callcount,u.cn_name AS cn_name,d.d_name AS d_name,c.src AS src,SUM(c.billsec) AS sumbillsec,r.rate_money AS rate_money,SUM(TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,2)) AS smoney')->join("bgcrm.rate r ON (SUBSTR(c.dst,1,LENGTH(r.rate_prefix)) = r.rate_prefix)" )->join('bgcrm.department d on c.d_id=d.d_id')->join('bgcrm.users u on c.username=u.username')->where($where)->group('src')->select();
        $filename ="费率报表";
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type:text/html;charset=UTF-8");
		$filename = iconv("utf-8","gb2312",$filename);
        header("Content-Disposition: attachment;filename=$filename.xls ");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();
        $start_row	=	0;

        xlsWriteLabel($start_row,0,utf2gb("姓名"));
        xlsWriteLabel($start_row,1,utf2gb("部门"));
        xlsWriteLabel($start_row,2,utf2gb("主叫号码"));
        xlsWriteLabel($start_row,3,utf2gb("通话次数"));
        xlsWriteLabel($start_row,4,utf2gb("通话总时长"));
        xlsWriteLabel($start_row,5,utf2gb("费率(元/分)"));
        xlsWriteLabel($start_row,6,utf2gb("总费用(元)"));

        $start_row++;
        foreach($rs as $val)  {
            $val['sumbillsec'] = sprintf("%02d",intval($val["sumbillsec"]/3600)).":".sprintf("%02d",intval(($val["sumbillsec"]%3600)/60)).":".sprintf("%02d",intval((($val[sumbillsec]%3600)%60)));
            xlsWriteLabel($start_row,0,utf2gb($val["cn_name"]));
            xlsWriteLabel($start_row,1,utf2gb($val["d_name"]));
            xlsWriteLabel($start_row,2,utf2gb($val["src"]));
            xlsWriteLabel($start_row,3,utf2gb($val["callcount"]));
            xlsWriteLabel($start_row,4,utf2gb($val["sumbillsec"]));
            xlsWriteLabel($start_row,5,utf2gb($val["rate_money"]));
            xlsWriteNumber($start_row,6,utf2gb($val["smoney"]));
            $start_row++;
        }
        xlsEOF();
    }


	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }


	//外呼计费
	function outBoundBillingList(){
		$task_id = empty($_REQUEST["task_id"]) ? "Y" : $_REQUEST["task_id"];
		$this->assign("task_id",$task_id);
		//dump($task_id);
		$this->display();
	}

	function outBoundBillingData(){
		$task_id = $_REQUEST["task_id"];
		$table = "sales_cdr_".$task_id;
		$sales_cdr = new Model("sales_cdr_".$task_id);

		$startime = $_REQUEST["startime"];
		$endtime = $_REQUEST["endtime"];
		$dst = $_REQUEST["dst"];   //主叫
		$src = $_REQUEST["src"];   //被叫

		$where = "1 ";
		$where .= " AND r.call_type='outbound' AND r.rate_prefix IS NOT NULL AND LENGTH(src)>4 AND r.rate_type=1";
		$where .= " AND SUBSTRING_INDEX(SUBSTRING_INDEX(c.channel,'-',1),'/',-1) IS NOT NULL ";
		$where .= " AND c.disposition='ANSWERED'";
		//$where .= " AND SUBSTRING_INDEX(SUBSTRING_INDEX(c.channel,'-',1),'/',-1)= r.rate_trunk";
		$where .= empty($startime) ? "" : " AND c.calldate>='$startime'";
		$where .= empty($endtime) ? "" : " AND c.calldate<='$endtime'";
		$where .= empty($dst) ? "" : " AND c.dst like '%$dst%'";
		$where .= empty($src) ? "" : " AND c.src like '%$src%'";

		$arrT = $sales_cdr->order("c.calldate desc")->table("$table c")->field("c.calldate,c.channel,c.src,c.dst,c.workno,c.billsec,r.rate_prefix,r.rate_money AS rate_money,TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,3) AS amoney")->join("rate r on (SUBSTR(c.src,1,LENGTH(r.rate_prefix)) = r.rate_prefix AND SUBSTRING_INDEX(SUBSTRING_INDEX(c.channel,'-',1),'/',-1)= r.rate_trunk  )")->where($where)->select();
		$count = count($arrT);

		foreach($arrT as $val){
			$total_money[] = $val["amoney"];
		}
		//$totalMoney = array_sum($total_money);
		$totalMoney1 = array_sum($total_money);
		$totalMoney = sprintf("%.3f", $totalMoney1);

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $sales_cdr->order("c.calldate desc")->table("$table c")->field("c.calldate,c.channel,c.src,c.dst,c.workno,c.billsec,r.rate_prefix,r.rate_money AS rate_money,TRUNCATE(CEILING(c.billsec/r.rate_cycle)* r.rate_money,3) AS amoney")->join("rate r on (SUBSTR(c.src,1,LENGTH(r.rate_prefix)) = r.rate_prefix AND  SUBSTRING_INDEX(SUBSTRING_INDEX(c.channel,'-',1),'/',-1)= r.rate_trunk)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$userArr = readU();
		$deptName = $userArr["deptId_name"];
		$deptUserName = $userArr["deptName_user"];
		$cn_user = $userArr["cn_user"];

		$i = 0;
		foreach($arrData as $val){
			$arrData[$i]["cn_name"] = $cn_user[$val["workno"]];
			$arrData[$i]["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));
			$i++;
		}
		unset($i);
		//echo $sales_cdr->getLastSql();
		//dump($arrData);die;

		$en_amondy = L("Total cost of");
		$tmp_mon = array(array("amoney"=>$en_amondy.": ".$totalMoney));

		$rowsList = count($arrData) ? $arrData : false;
		$arrR["total"] = $count;
		$arrR["rows"] = $rowsList;
		$arrR["footer"] = $tmp_mon;

		echo json_encode($arrR);
	}

}
?>
