<?php
class WeChatNewsAction extends Action{
	//图文
	function weChatNewsList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Graphic information";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$openid = empty($_REQUEST["openid"]) ? "Y" : $_REQUEST["openid"];
		$this->assign("openid",$openid);

		$this->display();
	}

	function weChatNewsData(){
		header("Content-Type:text/html; charset=utf-8");
		$wx_news = new Model("wx_news");
		$count = $wx_news->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wx_news->order("newtype asc")->limit($page->firstRow.','.$page->listRows)->select();
		foreach($arrData as &$val){
			$val["sendMsg"] = json_decode($val["sendMsg"],true);
			foreach($val["sendMsg"] as &$v){
				//$v["content"] = mb_substr($v["description"],0,50,'utf-8');
				$v["id"] = $val["id"];
				$v["newtype"] = $val["newtype"];
			}
		}

		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function batchWeChatNews(){
		$this->display();
	}

	function insertWeChatNews(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize = "1024";  //文件上传的最大文件大小（以字节为单位）默认为-1 不限大小
		$upload->savePath= "include/data/weChat/newsImg/";  //上传路径

		$upload->saveRule=uniqid;
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('jpg','jpeg','png');    //准许上传的文件后缀
		$upload->allowTypes=array('image/png','image/jpg','image/pjpeg','image/jpeg');  //检测mime类型


		$newMsg = $_REQUEST["newMsg"];
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			//dump($mess);die;
			echo json_encode(array('msg'=>$mess));
		}else{
			$info=$upload->getUploadFileInfo();
			$img_id = $_REQUEST["img_id"];
			$result = $this->insertNews($img_id,$info,$newMsg);
			if ($result){
				echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
			} else {
				echo json_encode(array('msg'=>'添加失败！'));
			}
		}
	}

	function insertNews($img_id,$info,$newMsg){
		$arrId = explode(",",$img_id);
		foreach($info as $val){
			$arr[] = $val["savepath"].$val["savename"];
		}
		$arr2 = array_combine($arrId,$arr);
		$i = 0;
		foreach($newMsg as $key=>&$val){
			$val[1] = $arr2[$key];
			$val["order"] = $i;
			if(!$val[2]){
				$val[2] = mb_substr($val[3],0,50,'utf-8')."...";
			}
			$i++;
		}
		$count = count($newMsg);
		if($count > 1){
			$newtype = "2";
		}else{
			$newtype = "1";
		}
		//dump($newMsg);die;
		$username = $_SESSION['user_info']['username'];
		$wx_news = new Model("wx_news");
		$arrData = array(
			"createname" => $username,
			"createtime" => date("Y-m-d H:i:s"),
			"newtype" => $newtype,
		);
		$result = $wx_news->data($arrData)->add();
		if ($result){
			$sql = "insert into wx_news_content(`new_id`,`order`,`title`,`content`,`description`,`img_path`,`news_url`) values";
			$value = "";
			foreach($newMsg as &$val){
				$val["url"] = "index.php?m=WeChatNews&a=viewNews&id=".$result."&order=".$val["order"];
				$arrM[] = array(
					"title" => $val[0],
					"description" => $val[3],
					"content" => $val[2],
					"url" => $val["url"],
					"picurl" => $val[1],
				);
				$str = "(";
				$str .= "'" .$result. "',";
				$str .= "'" .$val["order"]. "',";
				$str .= "'" .$val[0]. "',";
				$str .= "'" .$val[2]. "',";
				$str .= "'" .$val[3]. "',";
				$str .= "'" .$val[1]. "',";
				$str .= "'" .$val["url"]. "'";
				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
			$re = $wx_news->where("id = '$result'")->save(array("sendMsg" => json_encode($arrM)));
			//dump($newMsg);die;
			if( $value ){
				$sql .= $value;
				$res = $wx_news->execute($sql);
			}
			if($res){
				return true;
			}
		} else {
			return false;
		}
	}

	function editWechatNews(){
		$id = $_REQUEST["id"];
		$wx_news_content = new Model("wx_news_content");
		$arrData = $wx_news_content->where("new_id = '$id'")->select();
		$this->assign("newsData",$arrData);
		$max = $wx_news_content->where("new_id = '$id'")->max("order");
		$this->assign("max",$max);
		$this->assign("id",$id);

		$this->display();
	}

	function updateWeChatNews(){
		$new_id = $_REQUEST["new_id"];
		$order = $_REQUEST["orderid"];
		$description = "description_".$new_id."_".$order;
		$wx_news_content = new Model("wx_news_content");

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize = "1000000";
		$upload->savePath= "include/data/weChat/newsImg/";  //上传路径

		$upload->saveRule=uniqid;
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('jpg','jpeg','png');    //准许上传的文件后缀
		$upload->allowTypes=array('image/png','image/jpg','image/pjpeg','image/jpeg');  //检测mime类型


		$newMsg = $_REQUEST["newMsg"];
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = array(
					"title" => $_REQUEST["title"],
					"content" => $_REQUEST["content"],
					"description" => $_REQUEST[$description],
				);
				$result = $wx_news_content->data($arrData)->where("new_id = '$new_id' AND `order` = '$order'")->save();
				//echo $wx_news_content->getLastSql();die;
				if ($result !== false){
					$this->updateNewsMsg($new_id);
					echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
				} else {
					echo json_encode(array('msg'=>'更新失败！'.$mess));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrF = $wx_news_content->where("new_id = '$new_id' AND `order` = '$order'")->find();
			$arrData = array(
				"title" => $_REQUEST["title"],
				"content" => $_REQUEST["content"],
				"img_path" => $info[0]["savepath"].$info[0]["savename"],
				"description" => $_REQUEST[$description],
			);
			$result = $wx_news_content->data($arrData)->where("new_id = '$new_id' AND `order` = '$order'")->save();
			//echo $wx_news_content->getLastSql();die;
			if ($result !== false){
				unlink($arrF["img_path"]);
				$this->updateNewsMsg($new_id);
				echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
			} else {
				echo json_encode(array('msg'=>'更新失败！'.$mess));
			}
		}
	}

	function updateNewsMsg($new_id){
		$wx_news_content = new Model("wx_news_content");
		$newsData = $wx_news_content->where("new_id = '$new_id'")->select();
		foreach($newsData as $val){
			$arrM[] = array(
				"title" => $val["title"],
				"description" => $val["description"],
				"content" => $val["content"],
				"url" => $val["news_url"],
				"picurl" => $val["img_path"],
			);
		}
		$wx_news = new Model("wx_news");
		$re = $wx_news->where("id = '$new_id'")->save(array("sendMsg" => json_encode($arrM)));
	}

	function insertUpdateWeChatNews(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize = "1000000";
		$upload->savePath= "include/data/weChat/newsImg/";  //上传路径

		$upload->saveRule=uniqid;
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('jpg','jpeg','png');    //准许上传的文件后缀
		$upload->allowTypes=array('image/png','image/jpg','image/pjpeg','image/jpeg');  //检测mime类型


		$newMsg = $_REQUEST["newMsg"];
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			//dump($mess);die;
			echo json_encode(array('msg'=>$mess));
		}else{
			$info=$upload->getUploadFileInfo();
			$newid = $_REQUEST["newid"];
			$img_id = $_REQUEST["img_id"];
			$result = $this->insertEditNews($img_id,$info,$newMsg,$newid);
			if ($result){
				echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
			} else {
				echo json_encode(array('msg'=>'添加失败！'));
			}
		}
	}


	function insertEditNews($img_id,$info,$newMsg,$newid){
		$arrId = explode(",",$img_id);
		foreach($info as $val){
			$arr[] = $val["savepath"].$val["savename"];
		}
		$arr2 = array_combine($arrId,$arr);

		foreach($newMsg as $key=>&$val){
			$val[1] = $arr2[$key];
			$val["order"] = $key;
			if(!$val[2]){
				$val[2] = mb_substr($val[3],0,50,'utf-8')."...";
			}
		}
		//dump($newMsg);die;
		$username = $_SESSION['user_info']['username'];
		$wx_news = new Model("wx_news");
		$sql = "insert into wx_news_content(`new_id`,`order`,`title`,`content`,`description`,`img_path`,`news_url`) values";
		$value = "";
		foreach($newMsg as &$val){
			$val["url"] = "index.php?m=WeChatNews&a=viewNews&id=".$newid."&order=".$val["order"];
			$str = "(";
			$str .= "'" .$newid. "',";
			$str .= "'" .$val["order"]. "',";
			$str .= "'" .$val[0]. "',";
			$str .= "'" .$val[2]. "',";
			$str .= "'" .$val[3]. "',";
			$str .= "'" .$val[1]. "',";
			$str .= "'" .$val["url"]. "'";
			$str .= ")";
			$value .= empty($value)?"$str":",$str";
		}
		//dump($value);die;
		if( $value ){
			$sql .= $value;
			$res = $wx_news->execute($sql);
			//echo $wx_news->getLastSql();die;
		}
		if($res){
			$this->updateNewsMsg($newid);
			$wx_news->where("id = '$newid'")->save(array("newtype"=>"2"));
			return true;
		} else {
			return false;
		}
	}

	function viewNews(){
		header("Content-Type:text/html; charset=utf-8");
		$id = $_REQUEST["id"];
		$order = $_REQUEST["order"];
		$wx_news_content = new Model("wx_news_content");
		$arrData = $wx_news_content->where("new_id = '$id' AND `order`='$order'")->find();
		$this->assign("news",$arrData);

		$this->display();
	}

	function deleteWeChatNews(){
		$id = $_REQUEST["id"];
		$wx_news = new Model("wx_news");
		$result = $wx_news->where("id in ($id)")->delete();
		if ($result){
			$wx_news_content = new Model("wx_news_content");
			$arrData = $wx_news_content->where("new_id = '$id'")->select();
			foreach($arrData as $val){
				unlink($val["img_path"]);
			}
			$wx_news_content->where("new_id = '$id'")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function test1(){
		header("Content-Type:text/html; charset=utf-8");
		$id = $_REQUEST["id"];
		$order = $_REQUEST["order"];
		$wx_news_content = new Model("wx_news_content");
		$arrData = $wx_news_content->where("new_id = '$id' AND `order`='$order'")->find();
		$this->assign("news",$arrData);

		$this->display();
	}

	function test(){
		$this->display();
	}
	function testData(){
		$wx_news_content = new Model("wx_news_content");
		$count = $wx_news_content->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wx_news_content->limit($page->firstRow.','.$page->listRows)->select();
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}
}
?>
