<?php
class BuildVersionAction extends Action{
	function versionList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Build Version";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function versionDataList(){
		$version = new Model("version");
		import('ORG.Util.Page');
		$count = $version->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$versionData = $version->order("id desc")->limit($page->firstRow.','.$page->listRows)->select();
		$i = 0;
		foreach($versionData as $vm){
			$versionData[$i]['operating'] = "<a href='index.php?m=BuildVersion&a=downloadFile&path=".$vm['path']."&filename=".$vm['version_name']."'>下载</a>";
			$i++;
		}

		$rowsList = count($versionData) ? $versionData : false;
		$arrversion["total"] = $count;
		$arrversion["rows"] = $rowsList;

		echo json_encode($arrversion);
	}

	function downloadFile(){
		$path = $_GET["path"];
		$filename = urlencode($_GET["filename"]);
		$filePath = "$path/$filename";
		if(!file_exists($filePath)){
			echo "<script>alert('File $filePath is not exist!');</script>";
		}
        header('HTTP/1.1 200 OK');
        //header('Accept-Ranges: bytes');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($filePath)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($filePath))."");
        readfile($filePath);
	}

	function addVersion(){
		//svn最新版本号
		exec("svn info svn://localhost/svn/html --xml > BGCC/Conf/versionnum.xml");
		$xml = file_get_contents("BGCC/Conf/versionnum.xml");
		$dom = new DOMDocument();
		//dump(gettype($dom));die;
		$dom->loadXML($xml);
		$aa = array();
		$aa = $this->getArray($dom->documentElement);
		$version_num = $aa["entry"][0]["revision"];
		$tm = $aa["entry"][0]["commit"][0];
		$time = str_replace("-","",substr($tm["date"][0]["#text"],0,10));
		$version = $time."-".$version_num;
		$this->assign("version_num",$version_num);
		$this->assign("version",$version);
		$this->display();
	}

	function buildVersionData(){
		set_time_limit(0);
		ini_set('memory_limit','-1');

		$version_start = $_REQUEST["version_start"];
		$version_end = $_REQUEST["version_end"];
		$file_type = $_REQUEST["file_type"];
		$vpath = "/version";
		//将两个版本号之间的日志信息用xml文件形式导出
		$cmd = "svn log -r $version_start:$version_end  -v svn://localhost/svn/html --xml > BGCC/Conf/changelog.xml";
		exec($cmd,$retstr,$retno);
		//dump($retno);die;
		$xml = file_get_contents("BGCC/Conf/changelog.xml");
		$dom = new DOMDocument();
		//dump(gettype($dom));die;
		$dom->loadXML($xml);
		$aa = array();
		$aa = $this->getArray($dom->documentElement);
		foreach($aa['logentry'] as $v){
			$bb[] = $v['paths'];
			foreach($v['paths'] as $vm){
				$cc[] = $vm["path"];
				foreach($vm["path"] as $val){
					$dd[] = $val["#text"];
					$tt[] = $vpath."/versionv".$version_end.dirname($val["#text"]);
				}
			}
			foreach($v["date"] as $dv){
				$tm[] = str_replace("-","",substr($dv["#text"],0,10));
			}
			foreach($v["msg"] as $dv){
				$ms[] = $dv["#text"];
			}

			/*
			$mess[] = array(
				"version"=>$v['revision'],
				//substr($v["date"][0]["#text"],0,10)=>$v["msg"][0]["#text"],
				"msgv"=>"[".substr($v["date"][0]["#text"],0,10)."] ".$v["msg"][0]["#text"],
				"path"=>$v["paths"][0]["path"],
			);
			$mess[] = array(
				str_replace("-","",substr($v["date"][0]["#text"],0,10)),
				$v["msg"][0]["#text"],
				$v["paths"][0]["path"],
			);*/
			if($v["msg"][0]["#text"]){
				$mess[] = "[".substr($v["date"][0]["#text"],0,10)."]  修改：".$v["msg"][0]["#text"];
			}
		}


		//修改信息
		//$msge = array_unique($ms);  //或
		$msge = array_unique($mess);
		//$mage = implode("\n",$msge);
		$mage = implode("<br>",$msge);
		F('version_msg',$mage,"$vpath/versionv$version_end/");
		sort($tm);
		$count = count($tm)-1;
		foreach($tt as $tv){
			$this->mkdirs($tv);
		}
		$kk = array_unique($dd);
		F('version',$kk,"$vpath/versionv$version_end/");

		//将修改了的文件导入到一个文件夹中
		foreach($dd as $vf){
			exec("svn cat -r $version_end svn://localhost/svn/html$vf> $vpath/versionv$version_end$vf ");
		}

		$vfn = "$vpath/versionv$version_end/BGCC/Lib/Action/BuildVersionAction.class.php";
		if(file_exists($vfn)){
			unlink($vfn);
		}
		$vfntpl = "$vpath/versionv$version_end/BGCC/Tpl/BuildVersion";
		if(file_exists($vfntpl)){
			exec("cd $vpath/versionv$version_end/BGCC/Tpl;rm -rf BuildVersion");
		}

		//将文件打包
		$tgz_file = "/robotupdate-$tm[0]-$version_start-$tm[$count]-$version_end.tar.gz";
		if($file_type == "1"){
			//密文
			$cmd_tgz = "cd $vpath/versionv$version_end; find . -name '*.php' |egrep 'Action|bmi' |xargs -i screw {};find . -name '*.screw' |xargs rm -rf;  tar zcfp /version/$tgz_file * 2>/dev/null";
		}elseif($file_type == "2"){
			//明文
			$cmd_tgz = "cd $vpath/versionv$version_end; tar zcfp /version/$tgz_file * 2>/dev/null";
		}elseif($file_type == "3"){
			//pbx密文
			//$cmd_tgz = "cd $vpath/versionv$version_end; find . -name '*.php' |egrep 'Action|bmi' |xargs -i screw {};find . -name '*.screw' |xargs rm -rf;    tar zcf other.tar.gz    /etc/asterisk/extensions_custom.conf  			 /var/lib/asterisk/agi-bin/autocall/autocall.pl   /var/lib/asterisk/agi-bin/writePreviewCDR.php 			/var/lib/asterisk/agi-bin/autocall/callRecordCustomerInfo.php; chmod 777 /var/lib/asterisk/agi-bin/autocall/callRecordCustomerInfo.php;  chmod 777 /var/lib/asterisk/agi-bin/autocall/autocall.pl;    tar zcfp /version/$tgz_file * 2>/dev/null";


			$cmd_tgz = "cd $vpath/versionv$version_end; find . -name '*.php' |egrep 'Action|bmi' |xargs -i screw {};find . -name '*.screw' |xargs rm -rf;    tar zcf other.tar.gz    /etc/asterisk/extensions_custom.conf  			 /var/lib/asterisk/agi-bin/autocall/autocall.pl   /var/lib/asterisk/agi-bin/autocall/tenantRate.php   /var/lib/asterisk/agi-bin/autocall/warrant.php /var/lib/asterisk/agi-bin/autocall/tmpRate.php   /var/lib/asterisk/agi-bin/writePreviewCDR.php 			/var/lib/asterisk/agi-bin/autocall/callRecordCustomerInfo.php; chmod 777 /var/lib/asterisk/agi-bin/autocall/callRecordCustomerInfo.php;  chmod 777 /var/lib/asterisk/agi-bin/autocall/autocall.pl;  chmod 777 /var/lib/asterisk/agi-bin/autocall/tenantRate.php; chmod 777 /var/lib/asterisk/agi-bin/autocall/warrant.php; chmod 777 /var/lib/asterisk/agi-bin/autocall/tmpRate.php;   tar zcfp /version/$tgz_file * 2>/dev/null";
		}

		//$cmd_tgz = "cd $vpath/versionv$version_end; tar zcfp /version/$tgz_file * 2>/dev/null";
		//$cmd_tgz = "cd $vpath/versionv$version_end; find . -name '*.php' |egrep 'Action|bmi' |xargs -i screw {};find . -name '*.screw' |xargs rm -rf;  tar zcfp /version/$tgz_file * 2>/dev/null";
		//$cmd_tgz = "cd $vpath/versionv$version_end; find . -name '*.php' |egrep 'Action|bmi' |xargs -i screw {};find . -name '*.screw' |xargs rm -rf;    tar zcf other.tar.gz /var/lib/asterisk/agi-bin/autocall/callRecordCustomerInfo.php; chmod 777 /var/lib/asterisk/agi-bin/autocall/callRecordCustomerInfo.php;     tar zcfp /version/$tgz_file * 2>/dev/null";
		//$cmd_tgz = "cd $vpath/versionv$version_end; find . -name '*.php' |egrep 'Action|bmi|class' |xargs -i screw {}; find . -name '*.screw' |xargs rm -rf;  tar zcf other.tar.gz /etc/asterisk/extensions_custom.conf   /etc/asterisk/extensions_override_asterplus.conf  /var/lib/asterisk/agi-bin/autocall/  /var/lib/asterisk/agi-bin/writePreviewCDR.php    /var/lib/asterisk/agi-bin/writeCDR.php   /opt/bin/asterplus-update.php  /opt/bin/forced_upgrade.php    /var/lib/asterisk/agi-bin/bgcheck.php    /etc/asterisk/pp1  /etc/asterisk/pp2  /etc/asterisk/pp3  /etc/asterisk/pp4   ;  chmod 777 /opt/bin/asterplus-update.php;  chmod 777 /opt/bin/forced_upgrade.php ;  chmod 777 /var/lib/asterisk/agi-bin/writePreviewCDR.php; chmod 777 /var/lib/asterisk/agi-bin/writeCDR.php;  chmod 777 /var/lib/asterisk/agi-bin/bgcheck.php;  tar zcfp /version/$tgz_file * 2>/dev/null";

		//$cmd_tgz = "cd $vpath/versionv$version_end; find . -name '*.php' |egrep 'Action|bmi|class' |xargs -i screw {}; find . -name '*.screw' |xargs rm -rf;  tar zcf other.tar.gz /etc/asterisk/extensions_custom.conf   /etc/asterisk/extensions_override_asterplus.conf /etc/asterisk/features_applicationmap_custom.conf  /etc/asterisk/to-meeting.conf /var/lib/asterisk/agi-bin/autocall/ /var/lib/asterisk/sounds/custom/add-queue-success.wav /var/lib/asterisk/sounds/custom/dingdan.wav /var/lib/asterisk/sounds/custom/input-task-id.wav /var/lib/asterisk/sounds/custom/recordTag.wav /var/lib/asterisk/sounds/custom/tag-success.wav /var/lib/asterisk/sounds/custom/remove-queue-success.wav  /var/lib/asterisk/agi-bin/writePreviewCDR.php    /var/lib/asterisk/agi-bin/writeCDR.php  /var/lib/asterisk/agi-bin/bgcheck.php     /etc/asterisk/p1 /etc/asterisk/p2 /etc/asterisk/p3 /etc/asterisk/p4   /etc/asterisk/pp1  /etc/asterisk/pp2  /etc/asterisk/pp3  /etc/asterisk/pp4 /var/lib/asterisk/sounds/meeting   ;   chmod 777 /var/lib/asterisk/agi-bin/writePreviewCDR.php; chmod 777 /var/lib/asterisk/agi-bin/writeCDR.php;  chmod 777 /var/lib/asterisk/agi-bin/bgcheck.php;  tar zcfp /version/$tgz_file * 2>/dev/null";

		$res = exec($cmd_tgz,$retstr1,$retno1);
		if( $retno1 == 0 ){
			exec("cd $vpath;rm -rf versionv$version_end");
			unlink("/var/www/html/BGCC/Conf/changelog.xml");
			unlink("/var/www/html/BGCC/Conf/changelog2.xml");
			unlink("/var/www/html/BGCC/Conf/versionnum.xml");
			$versions = new Model("version");
			$arrData = array(
				"version_name"=>"robotupdate-$tm[0]-$version_start-$tm[$count]-$version_end.tar.gz",
				"description"=>$_REQUEST["description"],
				"path"=>$vpath,
			);
			$result = $versions->data($arrData)->add();
			if( $result ){
				echo json_encode(array('success'=>true,'msg'=>'新版本文件生成成功！'));
			}else{
				echo json_encode(array('msg'=>'新版本文件生成失败！'));
			}
		}
		sort($dd);
	}

	function deleteVersion(){
		$id = $_REQUEST["id"];
		$path = $_REQUEST["path"];
		$filename = $_REQUEST["filename"];
		$res = exec("cd $path;rm -rf $filename");
		if( $res == 0 ){
			$version = new Model("version");
			$result = $version->where("id in ($id)")->delete();
			if ($result){
				echo json_encode(array('success'=>true));
			} else {
				echo json_encode(array('msg'=>'删除失败'));
			}
		}
	}

	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}

	//解析xml文件
	function getArray($node){
		$array = false;

		if ($node->hasAttributes()) {
			foreach ($node->attributes as $attr) {
				$array[$attr->nodeName] = $attr->nodeValue;
			}
		}

		if ($node->hasChildNodes()) {
			if ($node->childNodes->length == 1) {
				$array[$node->firstChild->nodeName] = $this->getArray($node->firstChild);
			} else {
				foreach ($node->childNodes as $childNode) {
					if ($childNode->nodeType != XML_TEXT_NODE) {
						$array[$childNode->nodeName][] = $this->getArray($childNode);
					}
				}
			}
		} else {
			return $node->nodeValue;
		}
		return $array;
	}


	function installVersion(){
		$sys = new Model("system_set");
		$ver = $sys->where("name = 'version_num'")->find();
		$version = explode("-",$ver["svalue"]);
		$this->assign("version",$version[1]);
		//dump($version);
		$this->display();
	}

	function DoInstallVersion(){
		$install = $_REQUEST["install"];
		$vpath_tmp = "/var/www/html/data/attechment/version_tmp/";
		$vpath_install = "data/attechment/install_version/";
		$this->mkdirs($vpath_tmp);
		$this->mkdirs($vpath_install);
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$upload->savePath= $vpath_tmp;  //上传路径

		//$upload->saveRule = uniqid;
		//如果存在同名文件是否进行覆盖
		$upload->uploadReplace=true;
		$upload->allowExts=array('gz'); //准许上传的文件后缀

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			echo json_encode(array('msg'=>$mess."<br>  只允许上传.tar.gz格式的文件!"));
		}else{
			if($install){
				$info=$upload->getUploadFileInfo();
				$file = $info[0]["savename"];
				$version_num = array_pop( explode("-",array_shift( explode(".",$file) )) );
				$time = explode("-",array_shift( explode(".",$file) ));
				//dump($time[3]);die;
				//dump($version_num);die;
				$cmd = "/bin/tar --overwrite -zxf $vpath_tmp$file -C /var/www/html";
				$res = exec($cmd);
				$sys = new Model("system_set");
				$num = $sys->where("name = 'version_num'")->save(array("svalue"=>$time[3]."-".$version_num));
				if( $res == 0 ){
					exec("cd $vpath_tmp;rm -rf $file");
					echo json_encode(array('success'=>true,'msg'=>'升级成功！'));
				}
			}else{
				echo json_encode(array('success'=>true,'msg'=>'文件上传成功！'));
			}
		}
	}
}


?>
