<?php
class QualityNormAction extends Action{
	function normList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Quality control indicators";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function normDataList(){
		$norm = new Model("normtype");
		import('ORG.Util.Page');
		$count = $norm->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$normData = $norm->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($normData) ? $normData : false;
		$arrnorm["total"] = $count;
		$arrnorm["rows"] = $rowsList;

		echo json_encode($arrnorm);
	}

	function insertnorm(){
		$norm = new Model("normtype");
		$now_sum = $norm->sum("highestscore");
		$lave_num = 100-$now_sum;
		$max = $now_sum+$_REQUEST["highestscore"];
		if( $max > 100 ){
			$message = "最高分总和不能超过100，您当前最高分总和为：".$now_sum." 还剩 ".$lave_num."分";
			echo json_encode(array('msg'=>$message));
			die;
		}
		//dump($max);die;
		$arrData = array(
			"cn_normname"=>$_REQUEST["cn_normname"],
			//"en_normname"=>'score_',
			"highestscore"=>$_REQUEST["highestscore"],
		);
		$result = $norm->data($arrData)->add();
		$arrD = array(
			"en_normname"=>'score_'.$result,
		);
		$num = $norm->data($arrD)->where("id = $result")->save();
		$qualityscore = new Model("qualityscore");
		$sql = "ALTER TABLE `qualityscore`  ADD `score_$result` int(4) COMMENT '详细指标分数' ";
		$addField = $qualityscore->query($sql);
		//dump($addField);die;
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'出现未知错误！'));
		}
	}

	function updatenorm(){
		$id = $_REQUEST["id"];
		$norm = new Model("normtype");
		$now_sum = $norm->where("id <> $id")->sum("highestscore");
		//dump($now_sum);die;
		$lave_num = 100-$now_sum;
		$max = $now_sum+$_REQUEST["highestscore"];
		if( $max > 100 ){
			$message = "最高分总和不能超过100，您当前除此项之外的最高分总和为：".$now_sum." 还剩 ".$lave_num."分";
			echo json_encode(array('msg'=>$message));
			die;
		}
		$arrData = array(
			"cn_normname"=>$_REQUEST["cn_normname"],
			//"en_normname"=>$_REQUEST["en_normname"],
			"highestscore"=>$_REQUEST["highestscore"],
		);
		$result = $norm->data($arrData)->where("id = $id")->save();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function deletenorm(){
		$id = $_REQUEST["id"];
		$norm = new Model("normtype");
		$result = $norm->where("id = $id")->delete();
		$qualityscore = new Model("qualityscore");
		$sql = "ALTER TABLE `qualityscore`  DROP COLUMN `score_$id` ";
		$addField = $qualityscore->query($sql);
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}





	//标记
	function autocallMarkList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Recording Mark";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function MarkData(){
		$mark = new Model("autocall_mark");
		import('ORG.Util.Page');
		$count = $mark->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mark->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertMark(){
		$mark = new Model("autocall_mark");
		$key_value = $_REQUEST["key_value"];
		$count = $mark->where("key_value = '$key_value'")->count();
		if($count>0){
			echo json_encode(array('msg'=>'此按键值已存在！'));
			die;
		}
		$arrData = array(
			"key_value"=>$_REQUEST["key_value"],
			"key_description"=>$_REQUEST["key_description"],
		);
		$result = $mark->data($arrData)->add();
		//dump($addField);die;
		if ($result){
			$this->markCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'出现未知错误！'));
		}
	}

	function updateMark(){
		$id = $_REQUEST["id"];
		$mark = new Model("autocall_mark");
		$arrData = array(
			"key_value"=>$_REQUEST["key_value"],
			"key_description"=>$_REQUEST["key_description"],
		);
		$result = $mark->data($arrData)->where("id = $id")->save();
		if ($result !== false){
			$this->markCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function deleteMark(){
		$id = $_REQUEST["id"];
		$mark = new Model("autocall_mark");
		$result = $mark->where("id = $id")->delete();
		if ($result){
			$this->markCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function markCache(){
		$mark = new Model("autocall_mark");
		$arrData = $mark->select();
		foreach($arrData as $key => $val){
			$arrM[$val["key_value"]] = $val["key_description"];
		}

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F('mark',$arrM,"BGCC/Conf/crm/$db_name/");
		//F('mark',$arrM,"BGCC/Conf/");
	}

}

?>
