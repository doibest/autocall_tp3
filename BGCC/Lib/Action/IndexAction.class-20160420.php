<?php

class IndexAction extends Action {
    public function index(){
		checkLogin();
		if($_GET["l"]){
			setcookie("lang",$_GET["l"]);
			setcookie("think_language",$_GET["l"]);
		}else{
			//dump($_COOKIE);die;
			setcookie("lang",'zh_CN');
			setcookie("think_language",'zh_CN');
		}

		$para_sys = readS();
		setcookie("think_language",$para_sys["language"]);

		if($_REQUEST['agent_username']){
			//if(!$_SESSION["user_info"]){
			if(!$_SESSION["user_info"] || $_SESSION['user_info']['username']!=$_REQUEST['agent_username']){
				header("Location:index.php?m=Index&a=Login");
			}
		}else{
			if(!$_SESSION["user_info"]){
				header("Location:index.php?m=Index&a=Login");
			}
			if($_SESSION["user_info"]["interface"] == "pbx"){
				goback("pbx用户不能登录！");
			}
			if($_SESSION["user_info"]["interface"] == "dimission"){
				goback("离职员工不能登录！");
			}
			if($_SESSION["user_info"]["interface"] == "agent"){
				$checkRole = getSysinfo();
				if( strstr($checkRole[2],"wechat") ){
					$checkRole[2] = trim(str_replace(",wechat","",$checkRole[2]));
				}
				if( $checkRole[2] == "IPPBX" ){
					goback("您装的IPPBX系统，没有坐席界面！");
				}

				if( $checkRole[2] == "interface" ){
					goback("您装的使用接口系统，除admin外没有可使用界面！");
				}

				$username = $_SESSION["user_info"]["username"];
				$users = new Model("users");
				$users->where("username='$username'")->save(array("login_status"=>"Y"));
				$this->loginUserCaChe();

				//dump($_SESSION);die;
				//判断是否已经登录了
				$report = M('report_agent_login');
				$arrTmp = $report->where("username='$username' AND status='登录中'")->find();

				//if( $arrTmp ){
				if( 0 ){
					$logintime	=	$arrTmp['logintime'];
					goBack("您于 {$logintime} 已经在其它坐席端登录,请先注销后再登录或者联系管理员!","");
				}else{
					//记录登录报表到数据表report_agent_login
					$logintime = Date('Y-m-d H:i:s');
					$arr = Array(
						'username'	=> $_SESSION["user_info"]["username"],
						'logintime'	=>	$logintime,
						'lastheartbeat'	=>	$logintime,
						'status'	=>	'登录中',
						'during'	=>	0
					);
					$report->data($arr)->add();
					header("Location:agent.php");
				}
			}
		}
		if( check_reload()){
			$this->assign("reload_need","");
		}else{
			$this->assign("reload_need","display:none;");
		}
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model('menu');
		$mainMenu = $menu->table("menu me")->field("me.id AS id,name,me.icon,moduleclass,order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=0 AND enabled='Y'")->order("`order` ASC")->select();
		//dump($mainMenu);die;

		$_SESSION['menu'] = Array();//存放子菜单的父菜单，为以后判断子菜单的权限的方便
		foreach( $mainMenu AS $row ){
			$mainId = $row['id'];
			//$subMenu = $menu->where("pid=$mainId")->order("`order` ASC")->select();
			$subMenu = $menu->table("menu me")->field("me.id AS id,me.icon as iconCls,name,moduleclass, order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			if( 'admin'==$_SESSION["user_info"]['username'] ){//如果是管理员，默认显示所有模块加载的菜单
				/*
				$tpl_menu[$row['name']]['thisMenu'] = $row['name'];
				$tpl_menu[$row['name']]['iconCls'] = $row['icon'];
				$tpl_menu[$row['name']]['subMenu'] = $subMenu;
				*/
				//下面这样做，是为了让管理员过期后只显示一个菜单
				if( array_key_exists($row['name'],$_SESSION["user_priv"]) ){
					$tpl_menu[$row['name']]['thisMenu'] = $row['name'];//一级菜单应该显示
					$tpl_menu[$row['name']]['iconCls'] = $row['icon'];//一级菜单应该显示
					foreach( $subMenu AS $arrRow ){
						if( array_key_exists($arrRow['name'],$_SESSION["user_priv"][$row['name']]) ){
							$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
							$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
						}
					}
				}

			}else{//非管理员
				//$_SESSION["user_priv"]是权限数组
				//dump($_SESSION["user_priv"]);die;

				if( array_key_exists($row['name'],$_SESSION["user_priv"]) ){
					$tpl_menu[$row['name']]['thisMenu'] = $row['name'];//一级菜单应该显示
					$tpl_menu[$row['name']]['iconCls'] = $row['icon'];//一级菜单应该显示
					//下面查询一级菜单下面的二级菜单有哪些应该显示

					foreach( $subMenu AS $arrRow ){
						if( array_key_exists($arrRow['name'],$_SESSION["user_priv"][$row['name']]) ){
							$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
							$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
						}
					}
				}
			}
		}
		//$para_sys = readS();
		//dump($tpl_menu);die;

		$logo_replace = new Model("logo_replace");
		$logo = $logo_replace->where("id = '2'")->find();
		$this->assign("logo",$logo['logoname']);
		//dump($_SESSION["user_info"]);die;
        $this->assign("system",$para_sys);
		$this->assign("userinfo",$_SESSION["user_info"]);
		$this->assign("menu",$tpl_menu);

		$paraSys = readS();
		$version = $paraSys["version_num"];
		$name = "/var/www/html/data/version/robotupdate-".$version."-*.tar.gz";
		//$arrFile = glob("/var/www/html/data/version/*.tar.gz");
		$arrFile = glob($name);
		$fileName = array_pop(explode("/",$arrFile[0]));
		$arrV = explode(".",$fileName);
		$versionTmp = explode("-",$arrV[0]);
		$next_version = $versionTmp[3]."-".$versionTmp[4];
		//dump($version);
		//dump($arrFile);die;

		//用户选择性升级
		if($paraSys["Secondary_development"] != "Y"){   //没做过二次开发的可以升级
			if($paraSys["automaticUpgrades"] == "N"){    //升级提醒
				$this->assign("automaticUpgrades","N");
				if($arrFile){
					$this->assign("upgrade","Y");
					$this->assign("fileName",$fileName);
					$this->assign("next_version",$next_version);
					$this->assign("version",$version);
				}else{
					$this->assign("upgrade","N");
				}
			}else{
				if($arrFile){
					$this->assign("automaticUpgrades","Y");  //自动升级
					$this->assign("fileName",$fileName);
				}else{
					$this->assign("upgrade","N");
				}
			}
		}

		/*
		//广告、新闻等弹窗
		$arrMsg = $this->versionNewsData();
		if($arrMsg){
			$i = 0;
			foreach($arrMsg as $val){
				if( mb_strlen($val["content"]) < 50){
					$arrMsg[$i]["keyword"] = $val["content"];
				}else{
					$arrMsg[$i]["keyword"] = mb_substr($val["content"],0,50,'utf-8')."...";
				}
				$arrMsg[$i]["index"] = $i;
				$i++;
			}
			unset($i);
			if(count($arrMsg)>0){
				$this->assign("arrMsg",$arrMsg);
				$this->assign("sysMessage","Y");
			}else{
				$this->assign("sysMessage","N");
			}
		}

		//强制升级------做过二次开发的也要强制升级
		$version2 = file_get_contents("/var/www/html/BGCC/Conf/forcedUp.txt");
		$name2 = "/var/www/html/data/forcedUpgrade/robotforced-".$version2."-*.tar.gz";
		$arrFile2 = glob($name2);
		$fileName2 = array_pop(explode("/",$arrFile2[0]));
		if($arrFile2){
			$this->assign("forcedUpgrades","Y");  //强制升级
			$this->assign("fileName2",$fileName2);
		}else{
			$this->assign("forcedUpgrades","N");


			//强制升级------做过二次开发的不给强制升级
			$name3 = "/var/www/html/data/forcedUpgrade/robotupdate-".$version."-*.tar.gz";
			$arrFile3 = glob($name3);
			$fileName3 = array_pop(explode("/",$arrFile3[0]));
			if($arrFile3){
				if($paraSys["Secondary_development"] != "Y"){  //做过二次开发的不给强制升级
					$this->assign("forcedUpgrades_web","Y");  //强制升级
					$this->assign("fileName3",$fileName3);
				}else{
					$this->assign("forcedUpgrades_web","N");
				}
			}else{
				$this->assign("forcedUpgrades_web","N");
			}
		}


		//dump($arrH);
		//dump($arrMsg);die;
		*/


		$menu_count = count($tpl_menu);
		if($menu_count>13){
			$this->assign("height","90px;");
			$this->assign("img_name","toolbar_bg_93");
		}else{
			$this->assign("height","63px;");
			$this->assign("img_name","toolbar_bg");
		}


		$this->display();
    }

	function versionNewsData(){
		set_time_limit(0);
		$ctx = stream_context_create(array(
			'http' => array(
				'timeout' => 10    //设置一个超时时间，单位为秒
			)
		));
		$arrData = @file_get_contents("http://al.robot365.com/Index/Conf/verisonNews.php",0,$ctx);
		$arrF = json_decode($arrData,true);
		$nowtime = date("Y-m-d H:i:s");
		foreach($arrF as $val){
			if( strtotime($val["starttime"]) <= strtotime($nowtime) && strtotime($val["endtime"]) >= strtotime($nowtime) && $val["status"] == "Y" ){
				$arrT[] = $val;
			}
		}

		//去掉用户已经查看了的新闻
		$user_name = $_SESSION["user_info"]["username"];
		$arrDT = require "BGCC/Conf/newsMessage.php";
		$arrTD = $arrDT["markNews_".$user_name];

		foreach($arrTD as $val){
			$arrID[] = array_shift(explode(",",$val));
		}
		//dump($arrID);die;
		if($arrID){
			foreach($arrT as $val){
				if( !in_array($val["id"],$arrID) ){
					$arrH[] = $val;
				}
			}
			return $arrH;
		}else{
			return $arrT;
		}
	}

	function viewNewsData(){
		set_time_limit(0);
		$ctx = stream_context_create(array(
			'http' => array(
				'timeout' => 10    //设置一个超时时间，单位为秒
			)
		));
		$arrData = @file_get_contents("http://al.robot365.com/Index/Conf/verisonNews.php",0,$ctx);
		$arrF = json_decode($arrData,true);
		$nowtime = date("Y-m-d H:i:s");
		foreach($arrF as $val){
			if( $val["starttime"] <= $nowtime && $val["endtime"] >= $nowtime && $val["status"] == "Y" ){
				$arrT[] = $val;
			}
		}
		return $arrT;
	}

	function markNews(){
		$id = $_REQUEST["id"];
		$endtime = $_REQUEST["endtime"];
		$username = $_SESSION["user_info"]["username"];
		$msg = $id.",".$endtime;
		//$username = "test806";
		//$arrData = array("markNews_".$username => $id);
		$arrData = array("markNews_".$username => array($msg));
		$arrT = require "BGCC/Conf/newsMessage.php";
		$keyname = "markNews_".$username;

		if(is_array($arrT)){
			foreach($arrT[$keyname] as $val){
				$arrID[] = array_shift(explode(",",$val));
			}

			if(array_key_exists($keyname,$arrT)){
				if( !in_array($id,$arrID) ){
					array_push($arrT[$keyname],$msg);
				}
			}else{
				//array_push($arrT,$arrData);
				foreach($arrData as $key=>$value) {
					foreach($value as $k=>$v) {
					  $arrT[$key][$k] = $v;
					}
				}
			}
		}else{
			$arrT = $arrData;
		}


		//已经过期的数据 删除掉
		$nowtime = date("Y-m-d H:i:s");
		foreach($arrT[$keyname] as $val){
			if( strtotime(substr($val,-19)) < strtotime($nowtime) ){
				unset($val);
			}else{
				$tmp_arr[] = $val;
			}
		}
		$arrT[$keyname] = $tmp_arr;


		$content = "<?php\nreturn " .var_export($arrT,true) .";\n ?>";
		file_put_contents("BGCC/Conf/newsMessage.php",$content);
	}


	function seeMoreNews(){
		$type = $_REQUEST["type"];
		/*
		$arrMsg = $this->viewNewsData();
		if($type == "one"){
			$id = $_REQUEST["id"];
			foreach($arrMsg as $val){
				if($val["id"] == $id){
					$arrM[] = $val;
				}
			}
			$this->assign("arrMsg",$arrM);
		}else{
			$this->assign("arrMsg",$arrMsg);
		}
		//dump($arrM);die;
		*/
		$this->display();
	}

	public function controlPanel(){
		checkLogin();

		$this->display();
	}
	public function pbx(){
		checkLogin();
		$this->display();
		$pbx->addextension();
	}


    function Login(){
        header("Content-Type:text/html; charset=utf-8");
		$logo_replace = new Model("logo_replace");
		$logo = $logo_replace->where("id = '1'")->find();
		$this->assign("logo",$logo['logoname']);
		$background = $logo_replace->where("id = '3'")->find();
		$this->assign("background",$background['logoname']);
		/*
		$para_sys = readS();
		$this->assign("url",$para_sys["official_url"]);
		*/

		$system_set = new Model("system_set");
		$arr = $system_set->where("`name`='official_url'")->find();
		$this->assign("url",$arr["svalue"]);


		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("inbound",$arrAL) ){
			$wj_in = "Y";
		}else{
			$wj_in = "N";
		}
		if( in_array("outbound",$arrAL) ){
			$wj_out = "Y";
		}else{
			$wj_out = "N";
		}
		if( in_array("IPPBX",$arrAL) ){
			$wj_IPPBX = "Y";
		}else{
			$wj_IPPBXt = "N";
		}
		if($wj_in == "Y" && $wj_out == "Y"){
			$content = @file_get_contents("data/system/admin-all.php");
		}elseif($wj_in == "Y"){
			$content = @file_get_contents("data/system/admin-inbound.php");
		}elseif($wj_out == "Y"){
			$content = @file_get_contents("data/system/admin-outbound.php");
		}elseif($wj_IPPBX == "Y"){
			$content = @file_get_contents("data/system/admin-ippbx.php");
		}
		/*
		if( strstr($checkRole[2],"wechat") ){
			$checkRole[2] = trim(str_replace(",wechat","",$checkRole[2]));
		}
		//dump($checkRole);die;
		if( $checkRole[2] == "inbound" ){
			$content = @file_get_contents("data/system/admin-inbound.php");
		}elseif( $checkRole[2] == "outbound" ){
			$content = @file_get_contents("data/system/admin-outbound.php");
		}elseif( $checkRole[2] == "outbound,inbound" ){
			$content = @file_get_contents("data/system/admin-all.php");
		}elseif( $checkRole[2] == "IPPBX" || $checkRole[2] == "interface"){
			$content = @file_get_contents("data/system/admin-ippbx.php");
		}
		*/

		$role = new Model("role");
		$roleData = $role->where("r_id=1")->find();
		$systype = explode("-",$roleData["system_type"]);

		$arrData = array(
			"action_list"=>$content,
			"system_type"=>"Y-".$checkRole[2],
		);
		if( $systype[0] == "N" ){
			$role->data($arrData)->where("r_id = 1")->save();
		}else{
			if($checkRole[2] != $systype[1]){
				$role->data($arrData)->where("r_id = 1")->save();
			}
		}
		//dump($arrData);die;


        $this->display("","utf-8");
    }


    function userLogin(){
		//$warrant_role = $_SESSION["warrant_role"];
		$warrant_role = empty($_SESSION["warrant_role"]) ? file_get_contents("data/system/warrant.php") : $_SESSION["warrant_role"];
		//dump($warrant_role);die;
        if(empty($_POST['username'])) {
            goBack(L("The username must be filled")."！","");
        }elseif (empty($_POST['password'])){
            goBack("密码不能为空！","");
        }
		$isExpired = false; //专门为管理员设置的标记
		//$date = date("Ymd");
		$date = strtotime(date("Y-m-d"));
		//dump($date);die;
		//$date = "20141205";
		$checkRole = getSysinfo();
		/*
		if( strstr($checkRole[2],"wechat") ){
			$checkRole[2] = trim(str_replace(",wechat","",$checkRole[2]));
		}
		*/
		if( $_POST['username'] != "admin" ){
			$expireDate = strtotime($checkRole[1]);
			if( $date > $expireDate || $warrant_role == "N" ){
				goback("该系统已经过试用期或者授权已收回，请联系管理员","index.php?m=Index&a=Login");
			}
			$lic = $checkRole[3];
			if( $lic != '1'){
				goback("授权文件错误！","index.php?m=Index&a=Login");
			}
			if( $checkRole[2] == "interface" ){
				goback("您装的使用接口系统，除admin外没有可使用界面！");
			}
		}else{
			$expireDate = strtotime($checkRole[1]);
			if( $date > $expireDate ){
				$isExpired = true; //专门为管理员设置的标记
			}

		}
		//dump($date);
		//dump($expireDate);die;
		//dump($_SESSION);die;

        $sql = "SELECT * FROM users u LEFT JOIN role r ON u.r_id=r.r_id  WHERE username='". $_POST["username"] ."'";
        $user = M("users");
        $arrUser = $user->query($sql);
        $arrUser = $arrUser[0];

        if( !$arrUser["username"] ){
            goBack(L("Without this user"),"");
        }elseif($arrUser["password"] != $_POST["password"]){
            goBack(L("Wrong password"),"");
        }else{
             $_SESSION["user_info"] = $arrUser;
			 $_SESSION["user_priv"] = json_decode( $arrUser['action_list'],true);//权限
			 //dump($_SESSION["user_priv"]);die;
			 //下面专门让admin过期后登陆只有 上载授权文件这一个菜单页面
			 if( ($_POST['username'] == "admin" && $isExpired) || $warrant_role == "N"  ){
				$_SESSION["user_priv"] = Array(
					"Information Settings"	=> Array(
													"System Infomation"	=> Array("view"=>'Y'),
												)
				);
			 }
			 $_SESSION["db_name"] = "bgcrm";
			 cookie('user_info',$arrUser);
             header("Location:index.php");
        }
    }

    function userLogout(){
        header("Content-Type:text/html; charset=utf-8");

		//dump($_SESSION);die;
		//更新登录报表状态到数据表report_agent_login
		if( $_SESSION["user_info"]["interface"]=="agent" ){
			$logouttime = Date('Y-m-d H:i:s');
			$username = $_SESSION["user_info"]["username"];

			$users = new Model("users");
			$users->where("username='$username'")->save(array("login_status"=>"N"));
			$this->loginUserCaChe();

			$task_id = isset($_SESSION['outbound_current_task']['id'])?$_SESSION['outbound_current_task']['id']:NULL;
			$report = M('report_agent_login');
			$arrTmp = $report->where("username='$username' AND status='登录中'")->find();
			$logintime	=	$arrTmp['logintime'];
			$arr = Array(
				'lastheartbeat'	=>	$logouttime,
				'status'	=>	'正常注销',
				'last_task_id'	=>	$task_id,
				'during'	=>	strtotime($logouttime) - strtotime($logintime),
			);
			$report->data($arr)->where("username='$username' AND logintime='$logintime'")->save();
			//dump($report->getLastSql());die;
			//正常注销的时候还要把这个成员从asterisk项目队列中移动出来
			if( $task_id ){
				import('ORG.Pbx.bmi');
				$bmi = new bmi();
				$bmi->loadAGI();
				$queuename = "AutoCall_". $task_id;
				$ext = $_SESSION['user_info']['extension'];
				$interface	= "Local/$ext@BG-QueueAgent";
				$cliCmd = "queue remove member $interface from $queuename";
				$bmi->asm->command($cliCmd);
			}
		}

        unset($_SESSION['user_info']);
        unset($_SESSION['user_priv']);
        unset($_SESSION['db_name']);
        unset($_SESSION['menu']);
		unset($_SESSION['outbound_current_task']);

        goBack(L("You have been logout"),"index.php");
    }

    function homeLogin(){
        if(empty($_POST['username'])) {
            goBack("帐号必须填写！","");
        }elseif (empty($_POST['password'])){
            goBack("密码必须填写！","");
        }

        $sql = "SELECT * FROM users u LEFT JOIN role r ON u.r_id=r.r_id  WHERE username='". $_POST["username"] ."'";
        $user = M("users");
        $arrUser = $user->query($sql);
        $arrUser = $arrUser[0];

        if( !$arrUser["username"] ){
            goBack("没有这个用户！","");
        }elseif($arrUser["password"] != $_POST["password"]){
            goBack("密码不正确！","");
        }else{
            $_SESSION["user_info"] = $arrUser;
            header("Location:home.php");
        }
    }


	//验证码
    function  verify(){
        import("ORG.Util.Image");
        Image::buildImageVerify(4,1,"png",40,28);
    }

    //检测验证码
    function checkVerify(){
        if( $_SESSION['verify'] == md5($_POST['verify']) ) return true;
        else return false;
    }



	function test(){
		header("Content-Type:text/html; charset=utf-8");
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model('menu');
		$mainMenu = $menu->table("menu me")->field("me.id AS id,name,me.icon,moduleclass,order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=0 AND enabled='Y'")->order("`order` ASC")->select();
		//dump($mainMenu);die;

		$_SESSION['menu'] = Array();//存放子菜单的父菜单，为以后判断子菜单的权限的方便
		foreach( $mainMenu AS $row ){
			$mainId = $row['id'];
			//$subMenu = $menu->where("pid=$mainId")->order("`order` ASC")->select();
			//$subMenu = $menu->table("menu me")->field("me.id AS id,me.icon as iconCls,name,moduleclass, order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			$subMenu = $menu->table("menu me")->field("me.id AS id,name,url")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			if( 'admin'==$_SESSION["user_info"]['username'] ){
				//下面这样做，是为了让管理员过期后只显示一个菜单
				if( array_key_exists($row['name'],$_SESSION["user_priv"]) ){
					$tpl_menu[$row['name']]['thisMenu'] = L($row['name']);//一级菜单应该显示
					$tpl_menu[$row['name']]['id'] = $row['id'];//一级菜单应该显示
					//下面查询一级菜单下面的二级菜单有哪些应该显示

					foreach( $subMenu AS &$arrRow ){
						$arrRow["name"] = L($arrRow["name"]);
						$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
					}
				}

			}
		}

		$arr_all = array('分机','中继','时间组','等待音乐','系统录音','呼出路由','基本设置','编码','坐席监控','硬件检测','备份/恢复','网络','VPN 连接','日期/时间','重启/关机','版本升级','系统参数','系统信息','修改logo','部门','角色','用户','通话记录');  //三者相同的菜单
		$arr_all2 = array('PBX设置','系统管理','信息设置','组织结构','通话记录');  //三者相同的菜单

		$arr_outbound = array('外呼CID','外呼报表','外呼质检','外呼统计报表','外呼任务列表','外呼黑名单','质检评分');   //外呼独有的的菜单
		$arr_outbound2 = array('外呼平台');   //外呼独有的的菜单

		$arr_inbound = array('面板设置','传真邮件模版','事项类型','待办事项', '邮件模板','短信模板','短信查询', '费率','通话详单','队列漏接来电','工单报表','回访质检','话费账单', '服务报表','订单报表','客户资料','资料转交','服务类型','服务记录','文件类别','转交历史', '回访记录','回访内容','今日统计','商品列表','商品分类','商品品牌','商品类型','商品属性','订单管理','货单列表','队列报表');   //呼入独有的的菜单
		$arr_inbound2 = array('呼入平台','商品管理');   //呼入独有的的菜单


		$arr_in_out_bound = array('公告类型','公告','Faq类型','Faq内容管理','接口说明','质检类型','客户字段','坐席报表','示忙报表','系统报表');   //外呼与呼入共有的菜单
		$arr_in_out_bound2 = array('知识库','报表统计');   //外呼与呼入共有的菜单

		$arr_in_pbx = array('队列','自动语音应答','语音公告','时间条件','号码池','密码池','呼入路由','功能代码','呼入黑名单','会议室目录','开会');  //IPPBX与呼入共有的菜单
		$arr_in_pbx2 = array('会议');  //IPPBX与呼入共有的菜单

		$count = count($arr_all)+count($arr_outbound)+count($arr_inbound)+count($arr_in_out_bound)+count($arr_in_pbx);
		$arr_me = array_merge($arr_all,$arr_outbound,$arr_inbound,$arr_in_out_bound,$arr_in_pbx);


		//dump($count);
		foreach($tpl_menu as $key=>$val){
			$pid_name[] = $val["thisMenu"];
			if( in_array($val["thisMenu"],$arr_all2) ){
				$allId[] = $val["id"];   //三者相同的菜单
			}
			if( in_array($val["thisMenu"],$arr_outbound2) ){
				$all_outboundid[] = $val["id"];   //外呼独有的的菜单
			}
			if( in_array($val["thisMenu"],$arr_inbound2) ){
				$all_inboundid[] = $val["id"];     //呼入独有的的菜单
			}
			if( in_array($val["thisMenu"],$arr_in_out_bound2) ){
				$in_out_pid[] = $val["id"];     //外呼与呼入共有的菜单
			}
			if( in_array($val["thisMenu"],$arr_in_pbx2) ){
				$in_pbx_pid[] = $val["id"];     //IPPBX与呼入共有的菜单
			}
			foreach($val["subMenu"] as $vm){
				if( in_array($vm["name"],$arr_all) ){
					$all_sub[] = $vm['id'];	   //三者相同的菜单
				}
				if( in_array($vm["name"],$arr_outbound) ){
					$outbound_sub[] = $vm['id'];    //外呼独有的的菜单
				}
				if( in_array($vm["name"],$arr_inbound) ){
					$inbound_sub[] = $vm['id'];    //呼入独有的的菜单
				}
				if( in_array($vm["name"],$arr_in_out_bound) ){
					$in_out_sub[] = $vm['id'];    //外呼与呼入共有的菜单
				}
				if( in_array($vm["name"],$arr_in_pbx) ){
					$in_pbx_sub[] = $vm['id'];    //IPPBX与呼入共有的菜单
				}
				$sub_name[] = $vm["name"];
			}
		}
		//$arr_u = array_unique($arr_me,$sub_name);

		$str_pid = "'".implode("','",$allId)."'";
		$sub_id = "'".implode("','",$all_sub)."'";
		$all_id = $str_pid.",".$sub_id;    //三者相同的菜单id
		$res_all = $menu->where("id in ($all_id)")->save(array("sys_type"=>'{"outbound":"Y","inbound":"Y","IPPBX":"Y"}'));

		$outbound_id = "'".implode("','",$outbound_sub)."'";
		$outbound_id2 = "'".implode("','",$all_outboundid)."'";
		$outbound_all_id = $outbound_id.",".$outbound_id2;    //外呼独有的的菜单id
		$res_outbound = $menu->where("id in ($outbound_id)")->save(array("sys_type"=>'{"outbound":"Y","inbound":"N","IPPBX":"N"}'));

		$inbound_id = "'".implode("','",$inbound_sub)."'";
		$inbound_id2 = "'".implode("','",$all_inboundid)."'";
		$inbound_all_id = $inbound_id.",".$inbound_id2;     //呼入独有的的菜单id
		$res_inbound = $menu->where("id in ($inbound_all_id)")->save(array("sys_type"=>'{"outbound":"N","inbound":"Y","IPPBX":"N"}'));


		$in_out_id = "'".implode("','",$in_out_sub)."'";
		$in_out_id2 = "'".implode("','",$in_out_pid)."'";
		$in_out_all_id = $in_out_id.",".$in_out_id2;    //外呼与呼入共有的菜单
		$res_inbound = $menu->where("id in ($in_out_all_id)")->save(array("sys_type"=>'{"outbound":"Y","inbound":"Y","IPPBX":"N"}'));


		$in_pbx_id = "'".implode("','",$in_pbx_sub)."'";
		$in_pbx_id2 = "'".implode("','",$in_pbx_pid)."'";
		$in_pbx_all_id = $in_pbx_id.",".$in_pbx_id2;   //IPPBX与呼入共有的菜单
		$res_inbound = $menu->where("id in ($in_pbx_all_id)")->save(array("sys_type"=>'{"outbound":"N","inbound":"Y","IPPBX":"Y"}'));


		foreach($sub_name as $val){
			if( !in_array($val,$arr_me) ){
				$tt[] = $val;    //剩余那些菜单没写上的
			}
		}

		//dump($pid_name);
		//dump(count($sub_name));die;
		//dump($tpl_menu);die;
	}

	function loginUserCaChe(){
		$users = new Model("users");
		$arrData = $users->field("username,login_status")->where("login_status='Y'")->select();
		foreach($arrData as $val){
			$tmp[] = $val["username"];
		}
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F('loginUser',$tmp,"BGCC/Conf/crm/$db_name/");
		//F('loginUser',$tmp,"BGCC/Conf/");
	}

}

