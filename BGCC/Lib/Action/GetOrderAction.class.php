<?php
//获取订单
class GetOrderAction extends Action{
	function singleTrialList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Single Trial";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}


	function orderData(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION["user_info"]["username"];
		$menuname = "Order Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$calldate_start = $_REQUEST['calldate_start'];
		$calldate_end = $_REQUEST['calldate_end'];
        $order_num = $_REQUEST['order_num'];
        $consignee = $_REQUEST['consignee'];
        $email = $_REQUEST['email'];
        $phone = $_REQUEST['phone'];
        $telephone = $_REQUEST['telephone'];
        //$delivery_address = $_REQUEST['delivery_address'];
        $zipcode = $_REQUEST['zipcode'];
        //$country = $_REQUEST['country'];
        $province = $_REQUEST['province'];
        $city = $_REQUEST['city'];
        $district = $_REQUEST['district'];
        $order_status = $_REQUEST['order_status'];
        $pay_status = $_REQUEST['pay_status'];
        $shipping_status = $_REQUEST['shipping_status'];
        $createname = $_REQUEST['createname'];
        $logistics_account = $_REQUEST['logistics_account'];
        $shopping_name = $_REQUEST['shopping_name'];
        $search_type = $_REQUEST['search_type'];
        $logistics_state = $_REQUEST['logistics_state'];


		$where = "1 ";
        $where .= empty($calldate_start)?"":" AND createtime >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND createtime <'$calldate_end'";
		$where .= empty($order_num)?"":" AND order_num like '%$order_num%'";
		$where .= empty($consignee)?"":" AND consignee like '%$consignee%'";
		$where .= empty($email)?"":" AND email like '%$email%'";
		$where .= empty($phone)?"":" AND phone like '%$phone%'";
		$where .= empty($telephone)?"":" AND telephone like '%$telephone%'";
		$where .= empty($createname)?"":" AND createname like '%$createname%'";
		$where .= empty($logistics_account)?"":" AND logistics_account like '%$logistics_account%'";
		$where .= empty($zipcode)?"":" AND zipcode like '%$zipcode%'";
		//$where .= empty($country)?"":" AND country = '$country'";
		$where .= empty($province)?"":" AND province = '$province'";
		$where .= empty($city)?"":" AND city = '$city'";
		$where .= empty($district)?"":" AND district = '$district'";
		$where .= empty($shopping_name)?"":" AND shopping_name = '$shopping_name'";
		if($order_status == "0"){
			$where .= " AND order_status = '0'";
		}else{
			$where .= empty($order_status)?"":" AND order_status = '$order_status'";
		}
		$where .= empty($pay_status)?"":" AND pay_status = '$pay_status'";
		if($shipping_status == "0"){
			$where .= " AND shipping_status = '0'";
		}else{
			$where .= empty($shipping_status)?"":" AND shipping_status = '$shipping_status'";
		}
		if($logistics_state == "0"){
			$where .= " AND logistics_state = '$logistics_state'";
		}else{
			$where .= empty($logistics_state)?"":" AND logistics_state = '$logistics_state'";
		}

        $order_type = $_REQUEST['order_type'];
        $approval_enable = $_REQUEST['approval_enable'];
		if($order_type == "hd"){
			//审核订单
			$where .= " AND get_order = 'Y'";
			if($username != "admin"){
				$where .= " AND members_bomb = '$username'";
			}

		}elseif($order_type == "sp"){
			//审批预存
			$where .= empty($approval_enable) ? "" : " AND approval_enable='$approval_enable'";
			$where .= " AND order_status='3' AND stored_value_enable='Y'";
		}



		$r_id = $_SESSION["user_info"]["r_id"];
		$role = new Model("role");
		$rData = $role->where("r_id = '$r_id'")->find();
		$order_info = new Model("order_info");
		//if( ($username == "admin" || $priv['finance'] == "Y" || $priv['confirm_order'] == "Y" || $priv['package'] == "Y") && $priv['members_bomb'] == "Y" ){
		if(  $username == "admin" || $priv['members_bomb'] == "Y"  ){
			$count = $order_info->where($where)->count();
		}else{
			$count = $order_info->where("$where AND createname ='$username'")->count();
		}
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if(  $username == "admin" || $priv['members_bomb'] == "Y"  ){
			if($search_type == "xls"){
				$arrOrderData = $order_info->order("createtime desc")->where($where)->select();
			}else{
				$arrOrderData = $order_info->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
			}
		}else{
			if($search_type == "xls"){
				$arrOrderData = $order_info->order("createtime desc")->where("$where AND createname ='$username'")->select();
			}else{
				$arrOrderData = $order_info->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where("$where AND createname ='$username'")->select();
			}
		}
		//echo $order_info->getLastSql();die;
		//->field("order_num,consignee,phone,shopping_name,logistics_account,createname,createtime,goods_amount,shipping_fee,cope_money,delivery_address,payment,order_status,pay_status,shipping_status")

		$i = 0;
		//$row_order_status = array('0'=>'未确认','1'=>'已确认','2'=>'<span style="color:#FF6600;">取消</span>','3'=>'<span style="color:#FF6600;">无效</span>','4'=>'<span style="color:red;font-weight:bold;">退货</span>','5'=>'已分单','6'=>'部分分单','7'=>"未通过",'8'=>"疑似单");
		$row_order_status = array('1'=>'已通过','2'=>'<span style="color:#FF6600;">取消</span>','3'=>'<span style="color:#FF6600;">待审核</span>','4'=>'<span style="color:red;font-weight:bold;">退货</span>','5'=>'已分单','6'=>'部分分单','7'=>"未通过",'8'=>"问题单");

		$row_pay_status = array('N'=>'未付款','M'=>'付款中','Y'=>'<span style="color:#9933CC;font-weight:bold;">已付款</span>');
		$row_shipping_status = array('0'=>'<span style="color:#FF9933;font-weight:bold;">未发货</sapn>','1'=>'已发货','2'=>'<span style="color:#0000FF;font-weight:bold;">收货确认</span>','3'=>'配货中','4'=>'已发货(部分商品)','5'=>'发货中');
		//$row_shopping_name = array("1"=>"顺丰快递","2"=>"圆通速递","3"=>"邮政快递包裹","4"=>"市内快递","5"=>"申通快递","6"=>"邮局平邮","7"=>"城际快递");
		$row_shopping_name = logisticsMode();
		$row_refund = array("1"=>"生成退款申请","2"=>"不处理，错误操作时选此项","3"=>"退回用户余额");

		$row_payment = array("1"=>"余额支付","2"=>"银行汇款/转帐","3"=>"货到付款");
		$row_approval_enable = array("Y"=>"是","N"=>"否");
		$row_stored_value_enable = array("Y"=>"是","N"=>"否");

		$userArr = readU();
		$cnName = $userArr["cn_user"];

		$row_logistics_state = array("0"=>"在途","1"=>"揽件","2"=>"疑难","3"=>"签收","4"=>"退签","5"=>"派件","6"=>"退回","7"=>"转投");

		foreach($arrOrderData as &$val){
			$val["phone_source"] = $val["phone"];
			if($username != "admin"){
				if($para_sys["hide_phone"] =="yes"){
					$val["phone"] = substr($val["phone"],0,3)."***".substr($val["phone"],-4);
				}
			}

			$val['cn_name'] = $cnName[$val["createname"]];
			$val["approval_enable"] = $row_approval_enable[$val["approval_enable"]];
			$val["stored_value_enable"] = $row_approval_enable[$val["stored_value_enable"]];

			//$val["order_goods_num"] = explode(",",$val["order_goods_num"]);
			$order_status = $row_order_status[$val['order_status']];
			$val['order_status'] = $order_status;

			$pay_status = $row_pay_status[$val['pay_status']];
			$val['pay_status'] = $pay_status;

			$shipping_status = $row_shipping_status[$val['shipping_status']];
			$val['shipping_status'] = $shipping_status;

			$shopping_name = $row_shopping_name[$val['shopping_name']];
			$val['shopping_name'] = $shopping_name;

			$payment = $row_payment[$val['payment']];
			$val['payment'] = $payment;

			$val['status'] = $val['order_status']."  ".$val['pay_status']."   ".$val['shipping_status'];
			$val['status2'] = strip_tags($val['status']);

			$val["logistics_state"] = $row_logistics_state[$val["logistics_state"]];
			if($val["logistics_account"] && $val["logistics_state"]){
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"viewLogisticsInfo("."'".$val["logistics_account"]."','".$val["logistics_state"]."','".$val["logistics_Info"]."'".")\" >查看物流</a>";
			}else{
				$val["operating"] = "";
			}




			/*
			$refund1 = $row_refund[$val['refund1']];
			$val['refund1'] = $refund1;
			$refund4 = $row_refund[$val['refund4']];
			$val['refund4'] = $refund4;
			$refund3 = $row_refund[$val['refund3']];
			$val['refund3'] = $payment;

			if($val["explanation1"]){
				$val["pay_operating"] = "操作备注：".$val["description1"]."<br> 退款说明：".$val["explanation1"]."<br> 退款方式：".$val["refund1"]."<br> ";		//订单操作----设置为未付款--信息
			}else{
				$val["pay_operating"] = "";
			}
			if($val["explanation4"]){
				$val["reset_operating"] = "操作备注：".$val["description4"]."<br> 退款说明：".$val["explanation4"]."<br> 退款方式：".$val["refund4"]."<br> ";		//订单操作----退货--信息
			}else{
				$val["reset_operating"] = "";
			}
			if($val["explanation3"]){
				$val["cancel_operating"] = "操作备注：".$val["description3"]."<br> 退款说明：".$val["explanation3"]."<br> 退款方式：".$val["refund3"]."<br> ";		//订单操作----取消--信息
			}else{
				$val["cancel_operating"] = "";
			}

			if($val["pay_operating"]){
				$val["operating_order"] = "订单操作为设置为未付款的信息：<br>".$val["pay_operating"]."<br>";
			}
			if($val["reset_operating"]){
				$val["operating_order"] .= "订单操作为退货的信息：<br>".$val["reset_operating"]."<br>";
			}
			if($val["cancel_operating"]){
				$val["operating_order"] .= "订单操作为取消的信息：<br>".$val["cancel_operating"];
			}
			if($val["operating_order"]){
				$val["operating2"] = "鼠标移到这里查看取消、退货等信息";
			}else{
				$val["operating2"] = "";
			}

			$val["operations"] = "<a href='javascript:void(0);' onclick=\"exportGoods("."'".$val["id"]."','".$val["goods_id"]."'".")\" >导出</a>";
			*/
			$i++;
		}
		unset($i);

		if($search_type == "xls"){
			$field = array("consignee","phone","createname","createtime","goods_amount","shipping_fee","cope_money","status2","delivery_address","payment","shopping_name","logistics_account","order_goods_num");
			$title = array("收货人","收货人号码","创建者","下单时间","总金额","优惠金额","应付金额","状态","地址","支付方式","配送方式","物流号","商品信息");
			$count = count($field);
			$excelTiele = "订单列表";
			//echo $order_info->getLastSql();die;
			//dump($arrOrderData);die;
			$this->exportData($field,$title,$count,$excelTiele,$arrOrderData);
			die;
		}

		//dump($arrOrderData);die;
		//echo $order_info->getLastSql();die;

		if($order_type == "hd"){
			$start_time = date("Y-m-d")." 00:00:00";
			$now = date("Y-m-d H:i:s");
			$where_hd = "1 ";
			//$where_hd .= " AND ((stored_value_enable = 'Y' AND stored_sk = 'Y') OR stored_value_enable='N')";  //预存审批的
			$arrF = $order_info->field("count(*) as order_total,sum(case when order_status='1' then 1 else 0 end) as order_status_1_num,sum(case when order_status='3' AND get_order = 'Y' then 1 else 0 end) as order_status_3_num,sum(case when order_status='7' then 1 else 0 end) as order_status_7_num,sum(case when order_status='8' then 1 else 0 end) as order_status_8_num,sum(case when get_order='N' then 1 else 0 end) as order_no_get")->where($where_hd)->find();   //->where("createtime >='$start_time' AND createtime<='$now'")
			//echo $order_info->getLastSql();die;
		}else{
			$arrF = array();
		}
		//dump($arrF);die;

		$rowsList = count($arrOrderData) ? $arrOrderData : false;
		$arrOrder["total"] = $count;
		$arrOrder["rows"] = $rowsList;
		$arrOrder["today_info"] = $arrF;

		echo json_encode($arrOrder);
	}

	function getOrders(){
		$username = $_SESSION["user_info"]["username"];
		$get_num = $_REQUEST["get_num"];
		$order_info = M("order_info");
		$where_hd = "get_order = 'N' ";
		//$where_hd .= " AND ((stored_value_enable = 'Y' AND stored_sk = 'Y') OR stored_value_enable='N')";  //预存相关的
		$arrData = $order_info->order("createtime asc")->field("id,createname")->limit($get_num)->where($where_hd)->select();
		foreach($arrData as $val){
			$arrId[] = $val["id"];
		}
		$str_id = implode(",",$arrId);
		//dump($arrData);die;
		$arr = array(
			"get_order"=>"Y",
			"members_bomb"=>$username
		);
		$result = $order_info->data($arr)->where("id in ($str_id)")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"获取成功！"));
		} else {
			echo json_encode(array('msg'=>'获取失败！'));
		}
	}

	//预存审批列表
	function storedList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Approval Stored";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	//发货订单列表
	function shipList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Ship List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}


	//匹配单号
	function matchingNumber(){
		$order_status = $_REQUEST["order_status"];
		$shopping_name = $_REQUEST["shopping_name"];
		$shipping_status = $_REQUEST["shipping_status"];
		$start_order_num = $_REQUEST["start_order_num"];
		$end_order_num = $_REQUEST["end_order_num"];
		$com_code = $_REQUEST["com_code"];

		if($com_code == "shunfeng"){
			//顺丰快递
			$arr = $this->sfOrderNum($start_order_num,$end_order_num);
		}elseif($com_code == "ems"){
			//邮政
			$arr = $this->emsnum($start_order_num,$end_order_num);
		}elseif($com_code == "yuantong"){
			//圆通快递
			$arr = $this->yuantongNum($start_order_num,$end_order_num);
		}else{
			$arr = array();
			echo json_encode(array('msg'=>'只有顺丰快递、EMS跟圆通快递的能够匹配订单号!'));
			die;
		}
		//dump($arr);die;

		$where = "order_status = '$order_status' AND shopping_name = '$shopping_name' AND shipping_status = '$shipping_status' AND (logistics_account is null OR logistics_account='')";
		$order_info = M("order_info");
		$arrData = $order_info->order("createtime asc")->field("id,logistics_account")->where($where)->limit($end_order_num)->select();
		//echo $order_info->getLastSql();die;

		$i = 0;
		foreach($arrData as &$val){
			$val["logistics_account"] = $arr[$i];

			$result[] = $order_info->where("id = '".$val["id"]."'")->save(array("logistics_account"=>$val["logistics_account"]));
			$i++;
		}

		//dump($arr);
		//dump($arrData);die;
		if ($result !== false){
			//$this->getOrderIDUpdateProfuct($arr);  //在打印物流单号后在执行这个
			echo json_encode(array('success'=>true,'msg'=>"匹配成功！"));
		} else {
			echo json_encode(array('msg'=>'匹配失败！'));
		}

	}

	function getOrderIDUpdateProfuct($arr){
		$logistics_account = "'".implode("','",$arr)."'";
		$order_info = M("order_info");
		$arrD = $order_info->field("id,logistics_account")->where("logistics_account in ($logistics_account)")->select();
		foreach($arrD as $val){
			$this->productUpdates($val["id"]);
		}
		return true;
	}


	//发货后对应的商品库存减少
	function productUpdates($id){
		$order_goods = new Model("order_goods");
		$orgData = $order_goods->order("goods_id asc")->field("goods_id,id,order_id,goods_num")->where("order_id = '$id'")->select();

		foreach($orgData as $v){
			$gid[] = $v["goods_id"];
		}
		$good_id = implode(",",$gid);
		$shop_goods = new Model("shop_goods");
		$goodData = $shop_goods->order("id asc")->field("id as g_id,good_Inventory,good_name")->where("id in ($good_id)")->select();


		foreach($goodData as $key=>$value) {
			foreach($value as $k=>$v) {
			  $orgData[$key][$k] = $v;
			}
		}

		foreach($orgData as $val){
			$shop_goods->where("id = ".$val['goods_id'])->save( array("good_Inventory"=>$val['good_Inventory']-$val['goods_num']) );
		}
		return true;
	}

	function yuantongNum($yuantong,$num){
		$fri=substr($yuantong,1);
		$head = substr($yuantong,0,1);
		$yuantongRes[0] = $yuantong;
		for($i=1;$i<$num;$i++){
			$fri = $fri+1;
			$num2 = sprintf("%011d",$fri);
			$yuantongRes[$i]=$head.$num2;
		}
		//dump($yuantongRes);die;
		return $yuantongRes;
	}

	//邮政单号
	function emsnum($ems,$num){
		$fri=substr($ems,2,8);
		$head = substr($ems,0,2);
		$tail = substr($ems,-2);

		for($i=1;$i<=$num;$i++){
			$num3=substr($fri,0,1);
			$num4=substr($fri,1,1);
			$num5=substr($fri,2,1);
			$num6=substr($fri,3,1);
			$num7=substr($fri,4,1);
			$num8=substr($fri,5,1);
			$num9=substr($fri,6,1);
			$num0=substr($fri,7,1);
			$mid=8*$num3+6*$num4+4*$num5+2*$num6+3*$num7+5*$num8+9*$num9+7*$num0;
			$res=11-($mid%11);
			if($res==10){
				$res=0;
			}
			if($res==11){
				$res=5;
			}
			$emsres[$i-1]=$head.$fri.$res.$tail;
			$fri+=1;
			$len=strlen($fri);
			if($len<8){
				for($j=$len;$j<8;$j++){
					$fri="0".$fri;
				}
			}
		}
		return $emsres;
	}


	function set_num12($ems){
		//echo "ems=".$ems;
		//echo "<br>";
		$num1=substr($ems,0,1);
		$num2=substr($ems,1,1);
		$num3=substr($ems,2,1);
		$num4=substr($ems,3,1);
		$num5=substr($ems,4,1);
		$num6=substr($ems,5,1);
		$num7=substr($ems,6,1);
		$num8=substr($ems,7,1);
		$num9=substr($ems,8,1);
		$num10=substr($ems,9,1);
		$num11=substr($ems,10,1);
		$num12=substr($ems,11,1);
			switch($num11){
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
					$num12+=9;
					$num12=$num12%10;
					//echo "num11=".$num11."<br>";
					break;
				case 9:
					switch($num10){
						case 0:
						case 1:
						case 2:
						case 4:
						case 5:
						case 7:
						case 8:
							$num12+=6;
							$num12=$num12%10;
							break;
						case 3:
						case 6:
							$num12+=5;
							$num12=$num12%10;
							break;
						case 9:
							switch($num9){
								case 0:
								case 2:
								case 4:
								case 6:
								case 8:
									$num12+=3;
									$num12=$num12%10;
									break;
								case 1:
								case 3:
								case 5:
								case 7:
									$num12+=2;
									$num12=$num12%10;
									break;
								case 9:
									switch($num8){
										case 0:
										case 3:
										case 6:
											$num12+=0;
											$num12=$num12%10;
											break;
										case 1:
										case 2:
										case 4:
										case 5:
										case 7:
										case 8:
											$num12+=9;
											$num12=$num12%10;
											break;
										case 9:
											switch($num7){
												case 0:
													$num12+=7;
													$num12=$num12%10;
													break;
												case 1:
												case 2:
												case 3:
												case 4:
												case 5:
												case 6:
												case 7:
												case 8:
													$num12+=6;
													$num12=$num12%10;
													break;
												case 9:
													switch($num6){
														case 0:
														case 1:
														case 2:
														case 3:
														case 4:
														case 5:
														case 6:
														case 7:
														case 8:
															$num12+=3;
															$num12=$num12%10;
															break;
														case 9:
															 switch($num5){
																case 0:
																case 1:
																case 2:
																case 4:
																case 5:
																case 7:
																case 8:
																	$num12+=9;
																	$num12=$num12%10;
																	break;
																case 3:
																case 6:
																	$num12+=8;
																	$num12=$num12%10;
																	break;
																case 9:
																	switch($num4){
																		case 0:
																		case 2:
																		case 4:
																		case 6:
																		case 8:
																			$num12+=5;
																			$num12=$num12%10;
																			break;
																		case 1:
																		case 3:
																		case 5:
																		case 7:
																			$num12+=4;
																			$num12=$num12%10;
																			break;
																	}
																	break;
															 }
															 break;
													}
													break;
											}
											break;

									}
									break;
							}
							break;
					}
					break;
			}
			return $num12;
	}

	function sfOrderNum($ems,$num){
		$num1=substr($ems,0,1);
		$num12=substr($ems,11,1);
		if($num1==0){
			$sf_dh=substr($ems,1,10);
		}else{
			$sf_dh=substr($ems,0,11);
		}
		$ok=$ems;
		$sfNum = array();
		for($i=0;$i<$num;$i++){
			if($num1==0){
				$ok='0'.$sf_dh.$num12;
			}
			else{
				$ok=$sf_dh.$num12;
			}
			$num12=$this->set_num12($ok);
			$sf_dh++;
			$sfNum[] = $ok;
		}
		return $sfNum;
	}


}

?>
