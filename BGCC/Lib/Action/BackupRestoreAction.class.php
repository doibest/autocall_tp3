<?php
class BackupRestoreAction extends Action{
	//备份/恢复
	function listBackupRestore(){
		checkLogin();
		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";


		//读取备份文件并显示
		$backupFiles = Array();
		if ($handle = opendir($tar_dir)){
			while (false !== ($file = readdir($handle) )) {
				//echo "xx";die;
				if ($file!="." && $file!=".." && ereg("\.tar$",$file,$regs))
				{
					$tmpArr = explode('.',$file);
					$name = $tmpArr[0];
					if(file_exists( $ini_dir .'/' .$name .".ini")){//配置文件存在才说明备份有效
						//echo $ini_dir."/".$name.'.ini';die;
						$arrBackup = BG_read_from_ini_format($ini_dir."/".$name.'.ini');
						//dump($arrBackup);die;
						$backupTime = $arrBackup['Global']['time'];
						$size_M = ceil( filesize($tar_dir.'/'.$name.".tar") /(1024*1024) );
						//echo $size_M;
						$backupFiles[$name] = Array(
							'name' => $name,
							'time' => $backupTime,
							'size'	=> $size_M,
						);
					}

				}
			}
			krsort($backupFiles);
			closedir($handle);
		}
		//dump($backupFiles);die;

		//分页
		$count = count($backupFiles);
		//dump($count);die;
		$para_sys = readS();
		$size = $para_sys["page_rows"];
		$page = BG_Page($count,$size);//这个地方要修改
		$show = $page->show();
		$this->assign("page",$show);

		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$displayFiles = Array(); //转换成显示的
		$i = $j = 0;
		foreach($backupFiles AS $v){
			if($i >= $start && $j < $length){
				$displayFiles[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}
		//dump($displayFiles);die;

		//$this->assign("backupFiles",$backupFiles);
		$this->assign("displayFiles",$displayFiles);

		//分配增删改的权限
		$menuname = "Backup/Restore";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("add",$priv['add']);
		$this->assign("delete",$priv['delete']);
		$this->assign("Download",$priv['Download']);
		$this->assign("Restore",$priv['Restore']);

		$this->display();
	}

	function BackupRestoreData(){
		//分配增删改的权限
		$menuname = "Backup/Restore";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$username = $_SESSION['user_info']['username'];

		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";


		//读取备份文件并显示
		$backupFiles = Array();
		if ($handle = opendir($tar_dir)){
			while (false !== ($file = readdir($handle) )) {
				//echo "xx";die;
				if ($file!="." && $file!=".." && ereg("\.tar$",$file,$regs))
				{
					$tmpArr = explode('.',$file);
					$name = $tmpArr[0];
					if(file_exists( $ini_dir .'/' .$name .".ini")){//配置文件存在才说明备份有效
						//echo $ini_dir."/".$name.'.ini';die;
						$arrBackup = BG_read_from_ini_format($ini_dir."/".$name.'.ini');
						//dump($arrBackup);die;
						$backupTime = $arrBackup['Global']['time'];
						$size_M = ceil( filesize($tar_dir.'/'.$name.".tar") /(1024*1024) );
						//echo $size_M;
						$backupFiles[$name] = Array(
							'name' => $name,
							'time' => $backupTime,
							'size'	=> $size_M,
						);
					}

				}
			}
			krsort($backupFiles);
			closedir($handle);
		}
		//dump($backupFiles);die;


		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($backupFiles);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$displayFiles = Array(); //转换成显示的
		$i = $j = 0;
		foreach($backupFiles AS &$v){
			if($i >= $start && $j < $length){
				$displayFiles[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		foreach($displayFiles as &$val){
			$val["operating"] = '<a href="index.php?m=BackupRestore&a=viewBackup&name='.$val["name"].'">查看详情</a>&nbsp;&nbsp;';
			if($username == "admin" || $priv["Download"] == "Y"){
				$val["operating"] .= '<a href="index.php?m=BackupRestore&a=downloadBackup&name='.$val["name"].'">下载</a>&nbsp;&nbsp;';
			}
			/*
			if($username == "admin" || $priv["delete"] == "Y"){
				//$val["operating"] .= '<a href="index.php?m=BackupRestore&a=deleteBackup&name='.$val["name"].'">删除</a>&nbsp;&nbsp;';
				$val["operating"] .= "<a href='javascript:void(0);' onclick=\"removeUser("."'".$val["name"]."'".")\" >删除</a>";
			}
			*/
			$val["name2"] = $val["name"];
			$val["name"] = $val["name"].".tar";
			$val["size"] = $val["size"]." MB";
		}

		$rowsList = count($displayFiles) ? $displayFiles : false;
		$arrBack["total"] = $count;
		$arrBack["rows"] = $rowsList;

		echo json_encode($arrBack);
	}

	//查看备份文件详情
	function viewBackup(){
		checkLogin();
		//echo "viewBackup";die;
		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";

		$name = $_GET['name'];
		$arrBackup = BG_read_from_ini_format($ini_dir."/".$name.'.ini');
		//dump($arrBackup);die;
		$this->assign("name",$name);
		$this->assign("backUp",$arrBackup);
		$this->display();
	}

	//下载备份文件
	function downloadBackup(){
		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";

		$name = $_GET['name'];
		$tarFile = $tar_dir ."/".$name.".tar";

		//echo $tarFile;die;
        header('HTTP/1.1 200 OK');
        //header('Accept-Ranges: bytes');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($tarFile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($tarFile))."");
        readfile($tarFile);

	}

	//删除备份文件
	function deleteBackup(){
		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";

		$name = $_REQUEST['name'];
		//dump($name);die;
		$tarFile = $tar_dir ."/".$name.".tar";
		$iniFile = $ini_dir ."/".$name.".ini";
		if(unlink($tarFile) && unlink($iniFile)){
			echo json_encode(array('success'=>true));
			//goBack(L("删除成功"),"index.php?m=BackupRestore&a=listBackupRestore","");
		}else{
			//goBack(L("删除失败"),"index.php?m=BackupRestore&a=listBackupRestore","");
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//新建备份
	function newBackup(){
		checkLogin();
		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";
		$para_sys = readS();
		$this->assign("system",$para_sys);

		$this->display();
	}

	//点击备份按钮的实际动作
	function doNewBackup(){
		set_time_limit(0);
		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";

		$defaultBackup = array(
			'Global'	=>array(
				'OS_bit'	=> 'x86_64',//默认是64位的备份
				'time'	=>	'2013-01-01 00:00:00',

			),
			'Asterisk'	=>array(
				'astdb'	=>	'Asterisk Databases',
				'config'	=>	'Configuration Files',
				'monitors'	=>	'Recording Files',
				'vm'	=>	'Voicemails Sounds Files',
				'sound'	=>	'Custom Sounds Files,IVR and Announce',
				'moh'	=>	'Music on Hold',
				'dahdi'	=>	'DAHDI Configuration',
				'pbx'	=>	'PBX web GUI configration Tool',

			),
			'Fax'	=>array(
				'faxpdf'	=>'Fax PDF Files',

			),
			'Others'	=>array(
				'mysqldb'	=>'Mysql databases',

			),
		);
		$db_pass = C("DB_PWD");

		$timestamp = time();
		$datetime = Date('Y-m-d H:i:s',$timestamp);//备份时间
		$filename = "backup_".Date('Y-m-d_His',$timestamp);//备份文件名
		$OS_bit = exec("uname -i");
		$arrBackup = Array('Global'=>array(),'Asterisk'=>array(),'Fax'=>array(),'Others'=>array(),);
		$arrBackup['Global']['time'] = $datetime;
		$arrBackup['Global']['OS_bit'] = $OS_bit;
		$args = "";
		if( $_POST['Asterisk'] ){
			//echo "xxxx";die;
			//dump($_POST['Asterisk']);die;
			foreach( $_POST['Asterisk'] AS $v ){
				$arrBackup['Asterisk'][$v] = $defaultBackup['Asterisk'][$v];
				$args .= $this->getCmdArgs($v,$db_pass);
			}

		}

		if( $_POST['Fax'] ){
			foreach( $_POST['Fax'] AS $v ){
				$arrBackup['Fax'][$v] = $defaultBackup['Fax'][$v];
				$args .= $this->getCmdArgs($v,$db_pass);
			}

		}

		if( $_POST['Others'] ){
			foreach( $_POST['Others'] AS $v ){
				$arrBackup['Others'][$v] = $defaultBackup['Others'][$v];
				$args .= $this->getCmdArgs($v,$db_pass);
			}

		}

		//dump($arrBackup);die;
		//dump($args);die;
		$tgz_file = $backup_tmp_dir."/".$filename.".tgz";
		$cmd = "tar zcfp $tgz_file " . $args ." 2>/dev/null";
		//echo $cmd;die;
		exec($cmd);

		//$strJson = json_encode($arrBackup);
		$ini_file = $ini_dir."/".$filename.".ini";
		//echo $ini_file;die;
		//$ret = file_put_contents($ini_file,$strJson);
		BG_write_to_ini_format($ini_file,$arrBackup);

		//最后备份成tar包
		$tar_file = $tar_dir."/".$filename.".tar";
		$cmd = "tar cf $tar_file $tgz_file $ini_file 2>/dev/null";
		exec($cmd);
		$cmd = "rm -rf /var/www/backup/backup_tmp/*";
		exec($cmd);

		//上载到ftp服务器
		if( $_POST['ftpupload'] == "upload_to_ftp_server" ){
			import('ORG.Pbx.Ftp');
			$Ftp = new Ftp();
			$Ftp_conf = array("hostname"=>$_POST["ftp_ip"],"username"=>$_POST["ftp_username"],"password"=>$_POST["ftp_password"],"port"=>$_POST["ftp_port"]);
			//dump($Ftp_conf);die;
			$ftp_dir = $_POST["ftp_dir"];
			//echo "xxx";die;
			if( $Ftp->connect($Ftp_conf) ){
				//dump($ini_file);dump($tar_file);die;
				$Ftp->chgdir($ftp_dir);
				//$result1 = $Ftp->upload($ini_file,$filename.".ini");
				$result2 = $Ftp->upload($tar_file,$filename.".tar");
			}else{
				echo "can not connect to ftp server!";die;
			}

		}
		goBack(L("备份成功"),"index.php?m=BackupRestore&a=listBackupRestore","");
	}

	//取得到命令行参数
	function getCmdArgs($option,$db_pass){
		$path = "";
		switch($option){

			case 'astdb':
				//echo "---";die;
				$path .= " /var/lib/asterisk/astdb";
				break;

			case 'config':
				$path .= " /etc/asterisk/";
				break;

			//需要备份asteriskcdrdb数据库
			case 'monitors':
				$cmd = "/usr/bin/mysqldump -uroot -p{$db_pass} asteriskcdrdb > /var/www/backup/backup_tmp/mysqldb_asteriskcdrdb.sql";
				exec($cmd);
				$path .= " /var/spool/asterisk/monitor/ /var/www/backup/backup_tmp/mysqldb_asteriskcdrdb.sql";
				break;

			case 'vm':
				$path .= " /var/spool/asterisk/voicemail/";
				break;

			case 'sound':
				$path .= " /var/lib/asterisk/sounds/custom/";
				break;

			case 'moh':
				$path .= " /var/lib/asterisk/moh/";
				break;

			case 'dahdi':
				$path .= " /etc/dahdi/";
				break;

			//需要备份asterisk和bgcrm两个数据库
			case 'pbx':
				$cmd1 = "/usr/bin/mysqldump -uroot -p{$db_pass}  asterisk > /var/www/backup/backup_tmp/mysqldb_asterisk.sql";
				$cmd2 = "/usr/bin/mysqldump -uroot -p{$db_pass}  bgcrm > /var/www/backup/backup_tmp/mysqldb_bgcrm.sql";
				exec($cmd1);
				exec($cmd2);
				$path .= " /var/www/html/  /var/www/backup/backup_tmp/mysqldb_asterisk.sql  /var/www/backup/backup_tmp/mysqldb_bgcrm.sql";
				break;

			case 'faxpdf':
				$path .= " /var/www/faxes/";
				break;

			//需要备份mysql数据库
			case 'mysqldb':
				//$cmd = "/usr/bin/mysqldump -uroot -p{$db_pass}  mysql > /var/www/backup/backup_tmp/mysqldb_mysql.sql";
				$cmd1 = "/usr/bin/mysqldump -uroot -p{$db_pass}  bgcrm > /var/www/backup/backup_tmp/mysqldb_bgcrm.sql";
				$cmd2 = "/usr/bin/mysqldump -uroot -p{$db_pass}  asterisk > /var/www/backup/backup_tmp/mysqldb_asterisk.sql";
				exec($cmd1);
				exec($cmd2);
				$path .= " /var/www/backup/backup_tmp/mysqldb_bgcrm.sql /var/www/backup/backup_tmp/mysqldb_asterisk.sql";
				break;

			default:
			;
		}
		return $path;
	}

	//恢复备份文件的动作
	function restoreBackup(){
		set_time_limit(0);
		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";

		$cmd = "rm -rf $backup_tmp_dir/*";
		exec($cmd);

		$db_pass = C("DB_PWD");
		$name = $_GET['name'];
		$tar_file = $tar_dir.'/'.$name.".tar";
		$cmd = "tar xf $tar_file -C /";
		exec($cmd);

		$tgz_file = $backup_tmp_dir.'/'.$name.".tgz";
		$cmd = "tar zxf $tgz_file -C /";
		exec($cmd);//还原网页文件
		unlink($tgz_file);

		//下面如果有数据库的话数据库也导入进去
		//$ini_file = $ini_dir.'/'.$name.".ini";
		$sqlFiles = Array();
		if ($handle = opendir($backup_tmp_dir)){
			while (false !== ($file = readdir($handle) )) {
				if ($file!="." && $file!=".." && ereg("\.sql$",$file,$regs))
				{
					$tmpArr = explode('.',$file);
					$tmpArr = explode('_',$tmpArr[0]);
					$dbname = $tmpArr[1];
					if(! is_dir("/var/lib/mysql/$dbname")){//数据库不存在要先创建数据库
						exec("/usr/bin/mysqladmin -uroot -p{$db_pass} create $dbname");
					}
					$cmd = "/usr/bin/mysql -uroot -p{$db_pass}  $dbname < $backup_tmp_dir/$file";
					//echo $cmd;die;
					exec($cmd);
				}
			}
			closedir($handle);
		}
		goBack(L("还原成功!"),"index.php?m=BackupRestore&a=listBackupRestore","");
	}

	//列出ftp备份文件
	function listFtp(){
		checkLogin();
		$para_sys = readS();
		//dump($para_sys);die;
		import('ORG.Pbx.Ftp');
		$Ftp = new Ftp();
		$Ftp_conf = array("hostname"=>$para_sys["ftp_ip"],"username"=>$para_sys["ftp_username"],"password"=>$para_sys["ftp_password"],"port"=>$para_sys["ftp_port"]);
		$ftp_dir = $para_sys["ftp_dir"];
		$res = Array();
		if( $Ftp->connect($Ftp_conf) ){
			//dump($ini_file);dump($tar_file);die;
			$res = $Ftp->filelist($ftp_dir);
		}else{
			echo "can not connect to ftp server!";die;
		}
		$arrFiles = Array();
		foreach($res AS $v){
			if(substr($v,-4)=='.tar'){
				$arrFiles[] = $v;
			}
		}
		//dump($arrFiles);die;
		$this->assign("arrFiles",$arrFiles);
		$this->display();
	}

	//下载ftp备份文件并还原
	function getFileFromFtpAndRestore(){
		set_time_limit(0);
		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";

		$db_pass = C("DB_PWD");
		$remotefile = $_GET['file'];
		$para_sys = readS();
		//dump($para_sys);die;
		import('ORG.Pbx.Ftp');
		$Ftp = new Ftp();
		$Ftp_conf = array("hostname"=>$para_sys["ftp_ip"],"username"=>$para_sys["ftp_username"],"password"=>$para_sys["ftp_password"],"port"=>$para_sys["ftp_port"]);
		$ftp_dir = $para_sys["ftp_dir"];
		$tar_dir = "/var/www/backup/backup_tar";
		if(! $Ftp->connect($Ftp_conf) ){
			echo "can not connect to ftp server!";die;
		}
		//echo $tar_dir;die;
		$name = substr(basename($remotefile),0,-4);
		$tar_file = $tar_dir.'/'.$name. ".tar";
		if( $Ftp->download($remotefile, $tar_file) ){
			$cmd = "sudo /bin/tar xf $tar_file -C /";
			exec($cmd);

			//确保系统有权限解压或者新建文件夹，visudo 中加入 /bin/tar 权限
			$tgz_file = $backup_tmp_dir.'/'.$name.".tgz";
			$cmd = "sudo /bin/tar zxf $tgz_file -C /";
			exec($cmd);//还原网页文件
			unlink($tgz_file);

			//下面如果有数据库的话数据库也导入进去
			//$ini_file = $ini_dir.'/'.$name.".ini";
			$sqlFiles = Array();
			if ($handle = opendir($backup_tmp_dir)){
				while (false !== ($file = readdir($handle) )) {
					if ($file!="." && $file!=".." && ereg("\.sql$",$file,$regs))
					{
						$tmpArr = explode('.',$file);
						$tmpArr = explode('_',$tmpArr[0]);
						$dbname = $tmpArr[1];
						if(! is_dir("/var/lib/mysql/$dbname")){//数据库不存在要先创建数据库
							exec("/usr/bin/mysqladmin -uroot -p{$db_pass} create $dbname");
						}
						$cmd = "/usr/bin/mysql -uroot -p{$db_pass}  $dbname < $backup_tmp_dir/$file";
						//echo $cmd;die;
						exec($cmd);
					}
				}
				closedir($handle);
			}
			goBack(L("还原成功!"),"index.php?m=BackupRestore&a=listBackupRestore","");

		}else{
			echo "下载远程文件{$remotefile}失败!";die;
		}



	}

	//上次本地备份文件并欢迎
	function restoreWithUploadfile(){
		checkLogin();

		$this->display();
	}

	//恢复
	function restoreTarFile(){
		set_time_limit(0);
		$base_dir = "/var/www/backup";
		$conf_ini = $base_dir . "/backup_conf.ini";
		$tar_dir = $base_dir . "/backup_tar";
		$ini_dir = $base_dir . "/backup_ini";
		$backup_tmp_dir = $base_dir . "/backup_tmp";
		$restore_tmp_dir = $base_dir . "/restore_tmp";


		$tar_file = $tar_dir. "/". $_FILES['downloadfile']['name'];
		if( $_FILES['downloadfile']['tmp_name'] ){
			if(move_uploaded_file($_FILES['downloadfile']['tmp_name'], $tar_file)) {
				$cmd = "sudo /bin/tar xf $tar_file -C /";
				exec($cmd, $output, $return_var);//还原网页文件

				//$name不知道如何来的。
				//$tgz_file = $backup_tmp_dir.'/'.$name.".tgz";
				//用 trim过滤掉回车符
				$tgz_file = trim( shell_exec("ls $backup_tmp_dir/*.tgz |head -1") );
				//echo $tgz_file;die;
				$cmd = "sudo /bin/tar zxf $tgz_file -C /";
				//echo $cmd;die;
				exec($cmd, $output, $return_var);//还原网页文件
				//var_dump($output);echo $return_var;die;
				unlink($tgz_file);

				//下面如果有数据库的话数据库也导入进去
				$sqlFiles = Array();
				$db_pass = C("DB_PWD");
				if ($handle = opendir($backup_tmp_dir)){
					while (false !== ($file = readdir($handle) )) {
						if ($file!="." && $file!=".." && ereg("\.sql$",$file,$regs))
						{
							$tmpArr = explode('.',$file);
							$tmpArr = explode('_',$tmpArr[0]);
							$dbname = $tmpArr[1];
							if(! is_dir("/var/lib/mysql/$dbname")){//数据库不存在要先创建数据库
								exec("/usr/bin/mysqladmin -uroot -p{$db_pass} create $dbname");
							}
							$cmd = "/usr/bin/mysql -uroot -p{$db_pass}  $dbname < $backup_tmp_dir/$file";
							//echo $cmd;die;
							exec($cmd);
						}
					}
					closedir($handle);
				}
				goBack(L("还原成功!"),"index.php?m=BackupRestore&a=listBackupRestore","");
			}

		}else{
			echo "上载文件失败!";die;

		}

	}




}
?>
