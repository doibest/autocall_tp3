<?php
class AnnouncementAction extends Action{
	function announcementList(){
		checkLogin();

		$ggtype = new Model("announcementtype");
		$ggtypeList = $ggtype->select();
		$this->assign('ggtypeList',$ggtypeList);


		//dump($ggList);die;
		$menuname = "Announce";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		//dump($priv);
		$this->display();
	}

	function announcementData(){
		$user = $_SESSION["user_info"]["username"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$r_id = $_SESSION["user_info"]["r_id"];

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";

		$where = "1 ";
		if($user != admin){
			//$where .= " AND find_in_set('$d_id',a.department_id) ";
			$where .= " AND a.name in ($deptUser)";
		}
		$gg = new Model("announcement");
		$count = $gg->table("announcement a")->field("a.id,a.createtime,a.starttime,a.endtime,a.department_id as dept_id,a.content,a.an_enabeld,a.name,a.noticetype,a.title, t.announcementname")->join("announcementtype t on a.noticetype = t.id")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$ggList = $gg->order("createtime desc")->table("announcement a")->field("a.id,a.createtime,a.starttime,a.endtime,a.department_id as dept_id,a.content,a.an_enabeld,a.name,a.noticetype,a.title, t.announcementname")->join("announcementtype t on a.noticetype = t.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$i = 0;
		$rows = array("Y"=>"是","N"=>"否");
		foreach($ggList as &$val){
			$an_enabeld = $rows[$val["an_enabeld"]];
			$ggList[$i]['an_enabeld2'] = $an_enabeld;
			$i++;
		}
		//dump($ggList);die;
		$rowsList = count($ggList) ? $ggList : false;
		$arrgg["total"] = $count;
		$arrgg["rows"] = $rowsList;

		echo json_encode($arrgg);
	}

	function announcementDataBAK(){
		$user = $_SESSION["user_info"]["username"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$r_id = $_SESSION["user_info"]["r_id"];

		$gg = new Model("announcement");
		import('ORG.Util.Page');
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		//dump($_SESSION["user_info"]);die;
		if($user == admin){
			$count = $gg->count();
			$ggList = $gg->order("createtime desc")->table("announcement a")->field("a.id,a.createtime,a.starttime,a.endtime,a.department_id as dept_id,a.content,a.an_enabeld,a.name,a.noticetype,a.title, t.announcementname")->join("announcementtype t on a.noticetype = t.id")->limit($page->firstRow.','.$page->listRows)->select();
		}else{
			$arrDep = $this->getDepTreeArray();
			$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
			$deptSet = rtrim($deptst,",");
			$where = "department_id IN ($deptSet)";
			$where .= " OR reader = $r_id";
			//dump($deptSet);die;

			$count = $gg->where($where)->count();
			$ggList = $gg->order("createtime desc")->table("announcement a")->field("a.id,a.createtime,a.starttime,a.endtime,a.content,a.an_enabeld,a.department_id as dept_id,a.name,a.noticetype,a.title, t.announcementname")->join("announcementtype t on a.noticetype = t.id")->limit($page->firstRow.','.$page->listRows)->where("$where OR department_id = 0")->select();
			//echo $gg->getLastSql();die;
		}

		$i = 0;
		$rows = array("Y"=>"是","N"=>"否");
		foreach($ggList as &$val){
			$an_enabeld = $rows[$val["an_enabeld"]];
			$ggList[$i]['an_enabeld2'] = $an_enabeld;
			$i++;
		}
		//dump($ggList);die;
		$rowsList = count($ggList) ? $ggList : false;
		$arrgg["total"] = $count;
		$arrgg["rows"] = $rowsList;

		echo json_encode($arrgg);
	}


	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }

	/*
    function getDepartmentTree( $l )
	{
		import("ORG.Util.DepartmentTree");
        $department=new Model('Department');
		$Tree = new Tree();

		$sql ="SELECT * FROM department ORDER BY d_orderid,d_id ASC";
		$rows = $department->query($sql);
		//dump($rows);die;
		foreach($rows as $val){
			$Tree->setNode($val["d_id"],$val["d_pid"] , $val["d_name"]);
			$pid[$val["d_id"]] = $val["d_pid"];
		}
		$category = $Tree->getChilds();

		foreach ($category as $key=>$id)
		{
			$list[$id]["id"] = $id;
			$list[$id]["layer"] = $Tree->getLayer($id, $l);
			$list[$id]["d_pid"] = $pid[$id];
			$list[$id]["v"] = $Tree->getValue($id);
			//dump($list);die;
		}
		return $list;

	}
*/

	function addAnnouncement(){
		checkLogin();
		$ggtype = new Model("announcementtype");
		$ggtypeList = $ggtype->select();
		$this->assign('ggtypeList',$ggtypeList);

		$this->display();
	}

	function insertAnnouncement(){
		$username = $_SESSION["user_info"]["username"];
		$gg = new Model("announcement");
		//dump($_POST);die;
		$arrData = array(
			//"name"=>$_POST["name"],
			"name"=>$username,
			"noticetype"=>$_POST["noticetype"],
			"content"=>$_POST["content"],
			"createtime"=>date("Y-m-d H:i:s"),
			"reader"=>$_POST["reader"],
			"title"=>$_POST["title"],
			"an_enabeld"=>$_REQUEST["an_enabeld"],
			"endtime"=>$_POST["endtime"]." 23:59:59",
			//"starttime"=>date("Y-m-d H:i:s"),
			"starttime"=>$_POST["starttime"]." 00:00:00",
			//"department_id"=>$_POST["dept_id"],
			"department_id"=>$_REQUEST['deptid'],
		);
		//dump($arrData);die;
		//dump($sql);die;
		$result = $gg->data($arrData)->add();
		if ($result){
			$dept = $_REQUEST['deptid'];
			$deptId = explode(",",$dept);
			$sql = "insert into announcement_dept(gg_id,dept_id) values";
			foreach($deptId as $val){
				$str = "('$result','$val')";
				$value .= empty($value) ? $str : ",".$str;
			}
			$sql .= $value;
			$gg->execute($sql);
			$users = M("users");
			$arr = $users->field("username,d_id")->where("d_id in ($dept)")->select();
			echo json_encode(array('success'=>true,'msg'=>'公告添加成功！','user_name'=>$arr));
		} else {
			echo json_encode(array('msg'=>'公告添加失败！'));
		}

	}

	function editAnnouncement(){
		checkLogin();
		$id = $_REQUEST['id'];
		$gg = new Model("announcement");
		$ggData = $gg->where("id = '$id'")->find();
		$ggData["starttime"] = substr($ggData["starttime"],0,10);
		$ggData["endtime"] = substr($ggData["endtime"],0,10);
		$this->assign("ggData",$ggData);
		$this->assign("id",$id);

		$ggtype = new Model("announcementtype");
		$ggtypeList = $ggtype->select();
		$this->assign('ggtypeList',$ggtypeList);

		$this->display();
	}

	function updateAnnouncement(){
		$id = $_REQUEST["id"];
		$gg = new Model("announcement");
		//dump($_POST);die;
		$arrData = array(
			//"name"=>$_POST["name"],
			"noticetype"=>$_POST["noticetype"],
			"content"=>$_POST["content"],
			"reader"=>$_POST["reader"],
			"title"=>$_POST["title"],
			"an_enabeld"=>$_REQUEST["an_enabeld"],
			"endtime"=>$_POST["endtime"]." 23:59:59",
			"starttime"=>$_POST["starttime"]." 00:00:00",
			//"department_id"=>$_POST["dept_id"],
			"department_id"=>$_REQUEST['deptid'],
		);
		$result = $gg->where("id = $id")->save($arrData);
		if ($result !== false){
			$announcement_dept = new Model("announcement_dept");
			$res = $announcement_dept->where("gg_id = '$id'")->delete();

			$dept = $_REQUEST['deptid'];
			$deptId = explode(",",$dept);
			$sql = "insert into announcement_dept(gg_id,dept_id) values";
			foreach($deptId as $val){
				$str = "('$id','$val')";
				$value .= empty($value) ? $str : ",".$str;
			}
			$sql .= $value;
			$announcement_dept->execute($sql);

			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteAnnouncement(){
		$id = $_REQUEST["id"];
		//dump($id);die;
		$gg = new Model("announcement");
		$result = $gg->where("id in ($id)")->delete();
		if ($result){
			$announcement_dept = new Model("announcement_dept");
			$res = $announcement_dept->where("gg_id in ($id)")->delete();

			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

}
?>
