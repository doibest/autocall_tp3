<?php
class LogoReplaceAction extends Action{
	function logoList(){
		checkLogin();
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		if($db_name == "bgcrm"){
			$this->assign("enterprise_type","Y");
		}else{
			$this->assign("enterprise_type","N");
		}
		$this->display();
	}

	function saveLogo(){
		//$file_uplode_name = $_FILES["logoname"]["name"];  //获取上传文件的名称
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$enterprise_number = $_SESSION['user_info']['enterprise_number'];
		$logopath = "include/images/logo/$db_name/";
		$this->mkdirs($logopath);
		$id = $_REQUEST['logo_type'];
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize ='1000000';
		$upload->savePath= $logopath;  //上传路径

		//$upload->saveRule=uniqid;    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		if($id == '1'){
			$upload->saveRule="login_logo";  //登录logo
		}
		if($id == '2'){
			$upload->saveRule="jm_logo";    //界面logo
		}
		if($id == '3'){
			$upload->saveRule="background";    //登录背景图
		}
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('jpg','jpeg','png','gif');    //准许上传的文件后缀
		$upload->allowTypes=array('image/png','image/jpg','image/pjpeg','image/gif','image/jpeg');  //检测mime类型

		$checkRole = getSysinfo();
		$days=round( (strtotime($checkRole[1]) - strtotime($checkRole[0]) )/3600/24);
		if($days > 16){
			if(!$upload->upload()){ // 上传错误提示错误信息
				$mess = $upload->getErrorMsg();
				echo json_encode(array('msg'=>$mess."<br>  只支持jpg、png、gif格式的图片!"));
			}else{
				$info=$upload->getUploadFileInfo();
				//dump($info);
				$logo_replace = new Model("logo_replace");
				$arrData = Array(
				  'logopath' => $info[0]["savepath"],
				  'logoname' => $info[0]["savename"],
				  'logo_type' => $_REQUEST['logo_type'],
				);
				$result = $logo_replace->data($arrData)->where("id = '$id'")->save();
				if ($result !== false){
					if($db_name == "bgcrm"){
						$this->logoCache();
					}
					echo json_encode(array('success'=>true,'msg'=>'logo图片修改成功!'));
				} else {
					$mess = $upload->getErrorMsg();
					echo json_encode(array('msg'=>$mess));
				}
			}
		}else{
			echo json_encode(array('msg'=>"您安装的是试用版，不能修改图片，若您希望修改图片，请您安装正式版的！"));
		}
	}

	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}

	function logoCache(){
		$logo_replace = new Model("logo_replace");
		$arrData = $logo_replace->select();
		foreach($arrData as &$val){
			$val["logo_filename"] = $val["logopath"].$val["logoname"];
			$arrF[$val["id"]] = $val["logo_filename"];
		}
		//dump($arrF);die;
		F("logo_img",$arrF,"BGCC/Conf/");
	}

	function test(){
		$checkRole = getSysinfo();
		dump($checkRole);die;
	}

	function warrant(){
		checkLogin();
		$this->display();
	}
	function uplodeWarrant(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$upload->savePath= "data/system/";  //上传路径

		$upload->saveRule="robot";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('al');     //准许上传的文件后缀

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			echo json_encode(array('msg'=>$mess."<br>  只允许上传.al格式的文件!"));
		}else{
			$info=$upload->getUploadFileInfo();

			exec("/var/lib/asterisk/agi-bin/autocall/warrant.php -t local");

			//dump($info);
			echo json_encode(array('success'=>true,'msg'=>'授权文件上传成功！'));
		}
	}
}

?>
