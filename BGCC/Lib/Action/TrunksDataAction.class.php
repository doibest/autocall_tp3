<?php
class TrunksDataAction extends Action{
	function trunksList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Trunks";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}
		$this->assign("priv",$priv);

		$this->display();
	}

	function trunksDataList(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");

		$name = $_REQUEST["name"];
		$tech = $_REQUEST["tech"];

		$where = "1 ";
		if($username != "admin"){
			//$where .= " AND deptId in ($deptSet)";
			$where .= " AND (deptId in ($deptSet) OR deptId = '-1')";
		}
		$where .= empty($name) ? "" : " AND `name` like '%$name%'";
		$where .= empty($tech) ? "" : " AND `tech` = '$tech'";

		$trunk = new Model("asterisk.trunks");
		$count = $trunk->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$trunkData = $trunk->order("name asc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$status = $this->getTrunkstatus();
		$sipStatus = $status["sip"];
		$iaxStatus = $status["iax"];

		$userArr = readU();
		$deptName = $userArr["deptId_name"];
		$deptName["-1"] = "所有部门";

		$tech_row = array('iax2'=>'iax');
		$i =0;
		foreach($trunkData as &$val){
			if($val['tech'] == "iax2"){
				$tech = $tech_row[$val["tech"]];
				$val["tech"] = $tech;
			}

			if($val['tech'] == 'sip'){
				if( in_array($val["trunkid"],$sipStatus["registering"]) ){
					$val["trunkStatus"] ="正在注册";
				}elseif( in_array($val["trunkid"],$sipStatus["unregistered"]) ){
					$val["trunkStatus"] ="未注册";
				}elseif( in_array($val["trunkid"],$sipStatus["registered"]) ){
					$val["trunkStatus"] ="<span style='color:red'>已注册</span>";
				}else{
					$val["trunkStatus"] ="未知";
				}
			}elseif($val['tech'] == 'iax'){
				if( in_array($val["trunkid"],$iaxStatus["registering"]) ){
					$val["trunkStatus"] ="正在注册";
				}elseif( in_array($val["trunkid"],$iaxStatus["unregistered"]) ){
					$val["trunkStatus"] ="未注册";
				}elseif( in_array($val["trunkid"],$iaxStatus["registered"]) ){
					$val["trunkStatus"] ="<span style='color:red'>已注册</span>";
				}else{
					$val["trunkStatus"] ="未知";
				}
			}else{
				$val["trunkStatus"] ="未知";
			}
			$val["dept_name"] = $deptName[$val["deptId"]];
			$i++;
		}
		//dump($iaxStatus);die;

		$rowsList = count($trunkData) ? $trunkData : false;
		$arrTrunks["total"] = $count;
		$arrTrunks["rows"] = $rowsList;

		echo json_encode($arrTrunks);
	}

	function getTrunkstatus(){
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$sipTrunkStatus = $bmi->getSipTrunkStatus();
		foreach($sipTrunkStatus as $val){
			if($val[4] == "Request"){
				$sipRegistering[] = $val[2];   //正在注册
			}

			if($val[4] == "Unregistered"){
				$sipUnregistered[] = $val[2];  //未注册
			}

			if($val[4] == "Registered"){
				$sipRegistered[] = $val[2];  //已注册
			}
			$name[] = $val[2];
		}
		$sipStr = "'".implode("','",$name)."'";
		$sip = new Model("asterisk.sip");
		$sipData = $sip->where("data in ($sipStr) AND keyword = 'username'")->select();
		foreach($sipData as $val){
			$sipId[] = array_pop(explode("-",$val["id"]));
			if( in_array($val['data'],$sipRegistering) ){
				$sipId1[] = array_pop(explode("-",$val["id"]));    //正在注册
			}
			if( in_array($val['data'],$sipUnregistered) ){
				$sipId2[] = array_pop(explode("-",$val["id"]));    //未注册
			}
			if( in_array($val['data'],$sipRegistered) ){
				$sipId3[] = array_pop(explode("-",$val["id"]));    //已注册
			}
		}


		$iaxTrunkStatus = $bmi->getIaxTrunkStatus();
		foreach($iaxTrunkStatus as $val){
			if($val[5] == "Request"){
				$iaxRegistering[] = $val[2];   //正在注册
			}

			if($val[5] == "Unregistered"){
				$iaxUnregistered[] = $val[2];  //未注册
			}

			if($val[5] == "Registered"){
				$iaxRegistered[] = $val[2];  //已注册
			}
			$iaxname[] = $val[2];
		}
		$iaxStr = "'".implode("','",$iaxname)."'";
		$iax = new Model("asterisk.iax");
		$iaxData = $iax->where("data in ($iaxStr) AND keyword = 'username'")->select();
		//echo $iax->getLastSql();
		//dump($iaxData);die;
		foreach($iaxData as &$val){
			$iaxId[] = array_pop(explode("-",$val["id"]));
			if( in_array($val['data'],$iaxRegistering) ){
				$iaxId1[] = array_pop(explode("-",$val["id"]));    //正在注册
			}
			if( in_array($val['data'],$iaxUnregistered) ){
				$iaxId2[] = array_pop(explode("-",$val["id"]));    //未注册
			}
			if( in_array($val['data'],$iaxRegistered) ){
				$iaxId3[] = array_pop(explode("-",$val["id"]));    //已注册
			}
		}

		$trunkStatus = array(
						"sip"=>array(
							"registering"=>$sipId1,     //正在注册
							"unregistered"=>$sipId2,    //未注册
							"registered"=>$sipId3,      //已注册
							),
						"iax"=>array(
							"registering"=>$iaxId1,     //正在注册
							"unregistered"=>$iaxId2,    //未注册
							"registered"=>$iaxId3,      //已注册
							),
						);
		//dump($iaxRegistering);
		//dump($iaxRegistered);
		//dump($iaxData);
		//dump($iaxId2);
		//dump($sipTrunkStatus);
		//dump($trunkStatus);die;
		return $trunkStatus;
	}

	//添加SIP中继
	function addTrunks(){
		checkLogin();
		include_once ("/var/www/html/admin.php");
		$content = getContent($this ,"exteinsion",false);


		$peerdetails = "host=***provider ip address***\nusername=***userid***\nsecret=***password***\ntype=peer";
		/*
		$usercontext = "";
		$userconfig = "secret=***password***\ntype=user\ncontext=from-trunk";
		$register = "";

		$localpattern = "NXXXXXX";
		$lddialprefix = "1";
		$areacode = "";

		$upper_tech = strtoupper($tech);
		//echo "<h2>".sprintf(_("Add %s Trunk"),$upper_tech).($upper_tech == 'ZAP' && ast_with_dahdi()?" ("._("DAHDI compatibility mode").")":"")."</h2>";
		*/
		$this->assign("peerdetails",htmlspecialchars($peerdetails));

		$this->display();
	}


	function editTrunks(){
		checkLogin();
		$extdisplay = $_GET['extdisplay'];
		$trunk_type = $_REQUEST['trunk_type'];
		$trunkid = substr($extdisplay,4);
		//dump($trunkid);die;
		$trunk = new Model("asterisk.trunks");
		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$trunkList = $trunk->where("trunkid = '$trunkid'")->find();

		$trunkTypes = strtoupper($trunkList['tech']);

		$this->assign("trunkList",$trunkList);
		$this->assign("trunkid",$trunkid);
		$this->assign("trunkTypes",$trunkTypes);
		//if($trunkList['tech'] == 'dahdi'){
			//$this->assign("dahdis","ms");
		//}

		$trunk_dialpatterns = new Model("asterisk.trunk_dialpatterns");
		$dialData = $trunk_dialpatterns->order("seq")->where("trunkid = '$trunkid'")->select();
		$max = $trunk_dialpatterns->max("seq");
		$num = $max+1;
		$this->assign("dialData",$dialData);
		$this->assign("num",$num);

		if( $trunk_type == "sip"){
			$sip = new Model("asterisk.sip");
			$id = "tr-peer-".$trunkid;
			$sipData = $sip->order("flags")->where("id = '$id'")->select();
			foreach($sipData as $vm){
				$account[] = $vm['keyword'];
				$tmp_sip[] = $vm['keyword']." = ".$vm['data'];
			}
			if($account[0] == 'account'){
				array_shift($tmp_sip);
			}
			$tpl_sip = implode("\n",$tmp_sip);
			$this->assign("tpl_sip",$tpl_sip);

			$sid = "tr-reg-".$trunkid;
			$sData = $sip->order("flags")->where("id = '$sid'")->find();
			$this->assign("sData",$sData['data']);
			//dump($sData['data']);
		}
		if( $trunk_type == "iax" ){
			$iax2 = new Model("asterisk.iax");
			$id = "tr-peer-".$trunkid;
			$iaxData = $iax2->order("flags")->where("id = '$id'")->select();
			foreach($iaxData as $val){
				$iax_account[] = $val['keyword'];
				$tmp_iax[] = $val['keyword']." = ".$val['data'];
			}
			if($iax_account[0] == 'account'){
				array_shift($tmp_iax);
			}
			$tpl_iax = implode("\n",$tmp_iax);
			$this->assign("tpl_sip",$tpl_iax);

			$iaxid = "tr-reg-".$trunkid;
			$iData = $iax2->order("flags")->where("id = '$iaxid'")->find();
			$this->assign("sData",$iData['data']);
			//dump($iData);
		}


		//dump($tpl_sip);die;
		$this->assign("sipData",$sipData);

		$this->display();
	}

	//下载导入号码资源的excel的模板文件
	function DownloadTemplate(){
		$file = $_GET['file'];
		$path = "BGCC/Tpl/Public/download/";
		$realfile = $path.$file;
		if(!file_exists($realfile)){
			$this->error("File $realfile is not exist!");
		}
        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}

	//导入中继
	function importTrunk(){
		checkLogin();
		include_once ("/var/www/html/admin.php");
		$content = getContent($this ,"exteinsion",false);

		$peerdetails = "host=***provider ip address***\nusername=***userid***\nsecret=***password***\ntype=peer";
		$this->assign("peerdetails",htmlspecialchars($peerdetails));

		$this->display();
	}

	function doTmportTrunkData(){
		if( empty($_FILES["trunk_name"]["tmp_name"]) ){
			//goBack("请选择文件!","");
			echo json_encode(array('msg'=>'请选择文件!'));
			exit;
		}

		$time = time();
		$tmp_f = $_FILES["trunk_name"]["tmp_name"];  // /tmp/phptBkWXb
		$tmpArr = explode(".",$_FILES["trunk_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		//$trunks = new Model();
		$trunks = new Model("asterisk.trunks");
		$sqlId = "SELECT MAX(trunkid) AS id,name FROM `asterisk.trunks`";
		$maxid = $trunks->max('trunkid');
		//echo $trunks->getLastSql();die;
		$sql = "INSERT INTO asterisk.trunks(trunkid,`name`,tech,keepcid,addzero,channelid,disabled) VALUES";
		$sql2 = "INSERT INTO asterisk.sip(id,keyword,data,flags) VALUES";
		$value2 = "";

		if($suffix == "xls"){
			$dst_f = TMP_UPLOAD ."customer_name_".$time.".xls";
			//echo $dst_f;die;  // /tmp/IPPBX_Tmp_Upload/customer_name_1364281330.xls
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrTrunks = $data->sheets[0];
			array_shift($arrTrunks["cells"]);
			$arrTrunks = $arrTrunks["cells"];

			$i = $maxid+1;
			$keywordArr = array(
						"2"=>"account",
						"3"=>"host",
						"4"=>"username",
						"5"=>"secret",
						"6"=>"type",
						"7"=>"dtmfmode",
						"8"=>"fromuser",
						"9"=>"fromdomain",
					);
			$row = array("10"=>"nat");
			if($_REQUEST['tech'] == "vos"){
				array_push($keywordArr,"nat");
			}
			//dump($keywordArr);die;
			foreach($arrTrunks as &$val){
				$val['tt'] = explode('@',$val[2]);
				$str = "(";
				$str .= "'" .$i. "',";
				$str .= "'" .$val[1]. "',";
				$str .= "'sip',";
				$str .= "'off',";
				$str .= "'Y',";
				$str .= "'" .$val[1]. "',";
				$str .= "'off'";
				$str .= ")";
				$value .= empty($value) ? $str : ",".$str;

				$val['account'] = $val[1];
				$val['host'] = $val[4];
				$val['username'] = $val[2];
				$val['secret'] = $val[3];
				$val['type'] = "peer";
				if($_REQUEST['tech'] == "ims"){
					$n = "9";
					$val['dtmfmode'] = "inband";
					$val['fromuser'] = array_shift(explode('@',$val[2]));
					$val['fromdomain'] = array_pop(explode('@',$val[2]));
					$val['register'] = $val[2].":".$val[3]."@".$val[4].":5060";
				}else{
					$n = "10";
					$val['dtmfmode'] = "rfc2833";
					$val['fromuser'] = $val[2];
					$val['fromdomain'] = $val[4];
					$val['nat'] = "yes";
					$val['register'] = $val[2].":".$val[3]."@".$val[4];
				}
				$strSip = "";
				for($j=2;$j<=$n;$j++){
					$strSip .= "(";
					$strSip .= "'tr-peer-".$i."',";
					$strSip .= "'".$keywordArr[$j]."',";
					$strSip .= "'".$val[$keywordArr[$j]]."',";
					$strSip .= "'".$j."'";
					$strSip .= "),";
				}
				$strSip .= "(";
				$strSip .= "'tr-reg-".$i."',";
				$strSip .= "'register',";
				$strSip .= "'".$val["register"]."',";
				$strSip .= "'0'";
				$strSip .= "),";

				$value2 .= empty($value) ? $strSip : $strSip;

				$i++;
			}
			unset($i);
		}else{
			echo json_encode(array('msg'=>'文件类型错误!系统只支持处理.xls格式的Microsoft Excel文件!'));
			exit;
		}

		$value2 = rtrim($value2,",");
		//导入动作，执行SQL语句
		//$result = false;
		if( $value ){
			$sql .= $value;
			//dump($sql);die;
			$result = $trunks->execute($sql);
		}

		if($value2){
			$sql2 .= $value2;
			//dump($sql2);die;
			$res = $trunks->execute($sql2);
		}

		//判断导入结果
		if( $result && $res){
			for($i = $maxid+1;$i<=$result;$i++){

			}
			echo json_encode(array('success'=>true,'msg'=>'成功导入 '.$result.' 条中继'));
		}else{
			echo json_encode(array('msg'=>'导入中继时出现未知错误'));
		}
	}


    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}




	function updateTrunksSip(){
		$trunks = M("asterisk.trunks");
		$arrData = $trunks->order("trunkid asc")->field("trunkid,name")->select();

		$sql = "INSERT INTO asterisk.sip(id,keyword,data,flags) VALUES";
		$value2 = "";
		$value = "";

		//dump($arrData);die;
		foreach($arrData as $val){
			$str = "(";
			$str .= "'tr-peer-" .$val["trunkid"]. "',";
			$str .= "'context',";
			$str .= "'from-trunk-sip-ims',";
			$str .= "'0'";
			$str .= ")";
			$value .= empty($value)?"$str":",$str";


		}
		//dump($value);die;
		if($value){
			$sql .= $value;
			//dump($sql);die;
			$res = $trunks->execute($sql);
		}
		dump($res);die;

	}

	/*
UPDATE sip
SET `data` = CONCAT(CONCAT(SUBSTRING_INDEX(`data`,'@',2),'@ims.ha.chinamobile.com@sbc.chinamobile.com/'),SUBSTRING_INDEX(`data`,'@',1))
WHERE id LIKE '%tr-reg-%'
    AND id NOT IN('tr-reg-2','tr-reg-1')


UPDATE sip SET  `data` =  CONCAT(CONCAT(CONCAT(CONCAT(SUBSTRING_INDEX(`data`,'@',2),':'),SUBSTRING_INDEX(`data`,'@',1)),'@'),SUBSTRING_INDEX(`data`,'@',-2))
WHERE id LIKE '%tr-reg-%'
    AND id NOT IN('tr-reg-2','tr-reg-1')




UPDATE sip SET `data`= CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(SUBSTRING_INDEX(`data`,'@',2),":"),SUBSTRING_INDEX(`data`,'@',1)),"@ims.ha.chinamobile.com@"),SUBSTRING_INDEX(SUBSTRING_INDEX(`data`,'@',-1),':',1)),'/'),SUBSTRING_INDEX(`data`,'@',1))
 WHERE id LIKE 'tr-reg-%'
 AND id!='tr-reg-160'


UPDATE sip SET `data`= CONCAT(CONCAT(CONCAT(SUBSTRING_INDEX(SUBSTRING_INDEX(`data`,':',1),'@',1),'@'),'218.206.229.232:'),SUBSTRING_INDEX(`data`,':',-2))
 WHERE id LIKE 'tr-reg-%'


UPDATE sip SET `data`= CONCAT(CONCAT(SUBSTRING_INDEX(`data`,'@',3),'@218.206.229.248/'),SUBSTRING_INDEX(`data`,'/',-1))
 WHERE id LIKE 'tr-reg-%'


UPDATE sip SET `data`= '218.206.229.248'
WHERE id LIKE '%tr-peer-%' AND keyword='host'


	*/

}
?>
