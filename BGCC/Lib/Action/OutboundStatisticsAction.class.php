<?php
class OutboundStatisticsAction extends Action{

	function outBoundCountList(){
		checkLogin();
		$task_id = empty($_REQUEST["task_id"]) ? "Y" : $_REQUEST["task_id"];
		$trunk = M('sales_task')->where(array('id'=>$task_id))->getField('trunk');

		$numberStatus = M('asterisk.trunks')->where(array('name'=>explode('/',$trunk)[1],'tech'=>strtolower(explode('/',$trunk)[0])))->getField('numberStatus');

		$numberState = !empty($numberStatus)?$numberStatus:'';
		$this->assign('numberState',$numberState);
		$this->assign("task_id",$task_id);

		if($_REQUEST["calltype"]){
			$this->assign("calltype",$_REQUEST["calltype"]);
		}

		$this->display();
	}

	function OutboundStatisticsData(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的

			$endtime = Date('Y-m-d',$_GET['ts_end'])." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$startime = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1))." 00:00:00";
			}else{
				$startime = Date('Y-m-d',$_GET['ts_start'])." 00:00:00";
			}
		}else{
			$startime = $_REQUEST["startime"];
			$endtime = $_REQUEST["endtime"];
		}

		$task_id = $_REQUEST["task_id"];
        $search_type = $_REQUEST['search_type'];



		$source = new Model("sales_source_".$task_id);

		//dump($startime);
		//dump($endtime);die;

		$where = "dealuser is not null ";
		$where .= empty($startime) ? "" : " AND modifytime >= '$startime'";
		$where .= empty($endtime) ? "" : " AND modifytime <= '$endtime'";

		$totalNum = $source->count();  //没获取的数据
		$noGetData = $source->where("dealuser is null")->count();  //没获取的数据
		//接通率没有加
		$arrOutBoundData = $source->field("dealuser,COUNT(*) AS total,SUM(CASE WHEN calledflag=1 THEN 1 ELSE 0 END) AS nocall,SUM(CASE WHEN calledflag=2 THEN 1 ELSE 0 END) AS noAnswer,SUM(CASE WHEN calledflag=3 THEN 1 ELSE 0 END) AS answered,SUM(CASE WHEN dealresult_id=0 THEN 1 ELSE 0 END) AS untreated_source,SUM(CASE WHEN dealresult_id=1 THEN 1 ELSE 0 END) AS visit_source,SUM(CASE WHEN dealresult_id=2 THEN 1 ELSE 0 END) AS failure_source,SUM(CASE WHEN dealresult_id=3 THEN 1 ELSE 0 END) AS success_source,SUM(IF(call_status='ANSWERED',billsec,0)) as answerTime,SUM(billsec) as totalTime,AVG(billsec) as avgTime,SUM(transfer_status=2 or transfer_status=3) AS turnToArtificialNumber,SUM(transfer_status=3) AS manualConnectionHasBeenMade,SUM(transfer_status=2) AS turnManualNotConnected,SUM(hangup_disposition =1) as customerHangUpRate,SUM(calledflag=3) AS numberOfNumbersConnected,SUM(is_click=1) AS keyStrokes,SUM(quantity_obtained=1) AS acquiredQuantity,SUM(case when key_value is not null  AND key_value!='' then 1 else 0 end) as keyValue,SUM(case when calledflag !=1 then 1 else 0 end) as callNum,SUM(aigrade=1)as aClassCount,SUM(aigrade=2) as bClassCount,SUM(aigrade=3) as cClassCount,SUM(aigrade=4) as dClassCount,SUM(aigrade!='') as totalClassCount")->group("dealuser")->where($where)->select();
		foreach($arrOutBoundData as $key=>$val){
			if( $arrOutBoundData[0]['aigrade'] !='' || $arrOutBoundData[0]['aigrade']!=null){

				$arrOutBoundData[0]['aClass'] = $arrOutBoundData[0]['aClassCount'].' /  '.round($arrOutBoundData[0]['aClassCount']/$arrOutBoundData[0]['totalClassCount'], 2)*100;
				$arrOutBoundData[0]['bClass'] = $arrOutBoundData[0]['bClassCount'].' /  '.round($arrOutBoundData[0]['bClassCount']/$arrOutBoundData[0]['totalClassCount'], 2)*100;
				$arrOutBoundData[0]['cClass'] = $arrOutBoundData[0]['cClassCount'].' /  '.round($arrOutBoundData[0]['cClassCount']/$arrOutBoundData[0]['totalClassCount'], 2)*100;
				$arrOutBoundData[0]['dClass'] = $arrOutBoundData[0]['dClassCount'].' /  '.round($arrOutBoundData[0]['dClassCount']/$arrOutBoundData[0]['totalClassCount'], 2)*100;
			}else{
				$arrOutBoundData[0]['aClass'] = '0';
				$arrOutBoundData[0]['bClass'] = '0';
				$arrOutBoundData[0]['cClass'] = '0';
				$arrOutBoundData[0]['dClass'] = '0';
			}
			if($val["dealuser"]){
				$arrD[$val["dealuser"]] = $val;
			}
		}

		$where1 = "1 ";
		$where1 .= empty($startime) ? "" : " AND dealtime >= '$startime'";
		$where1 .= empty($endtime) ? "" : " AND dealtime <= '$endtime'";
		$history = new Model("sales_contact_history_".$task_id);
		$arrHis = $history->field("dealuser,SUM(CASE WHEN dealresult=0 THEN 1 ELSE 0 END) AS untreated,SUM(CASE WHEN dealresult=1 THEN 1 ELSE 0 END) AS visit,SUM(CASE WHEN dealresult=2 THEN 1 ELSE 0 END) AS failure,SUM(CASE WHEN dealresult=3 THEN 1 ELSE 0 END) AS success")->group("dealuser")->where($where1)->select();
		foreach($arrHis as $key=>$val){
			if($val["dealuser"]){
				$arrF[$val["dealuser"]] = $val;
			}
		}
		//echo $source->getLastSql();die;


		$task_cdr = new Model("sales_cdr_".$task_id);
		$where2 = "workno is not null AND workno != ''";
		$where2 .= empty($startime) ? "" : " AND calldate >= '$startime'";
		$where2 .= empty($endtime) ? "" : " AND calldate <= '$endtime'";
		$cdrData = $task_cdr->field("workno as dealuser,dst,SUM(billsec) as billsec,SUM(IF(disposition='ANSWERED' AND transferbillsec IS NOT NULL,`transferbillsec`,billsec)) AS duration,disposition,COUNT(*) AS answertotal,round(AVG(billsec)) AS averageduration,SUM(disposition='ANSWERED') as answeredCount,SUM(disposition='ANSWERED' OR disposition='PLAYIVR' OR disposition='NO ANSWER') as connectCount, SUM(duration>0 AND duration<5) as 5Duration,SUM(duration>5 AND duration<10) as 510Duration,SUM(disposition='ANSWERED') as answeredCount,SUM(duration>0) AS AllDuration,SUM(duration) as totalDuration,SUM(duration>10 AND duration<20) as 1020Duration,SUM(duration>20 AND duration<30) as 2030Duration,SUM(duration>30 AND duration<60) as 3060Duration,SUM(duration>60) as 60Duration")->where($where2)->group("workno")->select();
		foreach($cdrData as $key=>$val){
			$arrC[$val["dealuser"]] = $val;
		}

		// dump($cdrData);die;
		if($arrD){
			foreach($arrD as $key=>$val){
				foreach($val as $k=>&$v){
					$arrF[$key][$k] = $v;
				}
			}
		}
		if($arrF){
			foreach($arrF as $key=>$val){
				foreach($val as $k=>&$v){
					$arrC[$key][$k] = $v;
				}
			}
		}
		foreach($arrC as $val){
			$arrT[] = $val;
		}

		$arrOutBoundData = $arrT;
		// dump($arrOutBoundData);die();

		$userArr = readU();
		$cn_user = $userArr["cn_user"];
		$user_exten = $userArr["exten_user"];

		foreach($arrOutBoundData as &$val){

			$val['aClass'] = empty($val["aClassCount"])?'0 / 0':$val["aClassCount"].' / '.$val["aClassCount"]/$val['totalClassCount'];
			$val["bClass"] = empty($val["bClassCount"])?'0 / 0':$val["bClassCount"].' / '.$val["bClassCount"]/$val['totalClassCount'];
			$val["cClass"] = empty($val["cClassCount"])?'0 / 0':$val["cClassCount"].' / '.$val["cClassCount"]/$val['totalClassCount'];
			$val["dClass"] = empty($val["totalClassCount"])?'0 / 0':$val["dClassCount"].' / '.$val["dClassCount"]/$val['totalClassCount'];
			##转人工数
			$val["turnToArtificialNumber"] = empty($val["turnToArtificialNumber"])?'0 / 0':$val["turnToArtificialNumber"] .' / '.$val["turnToArtificialNumber"]/$val['total'];
			##转人工已接数
			$val["manualConnectionHasBeenMade"] = empty($val["manualConnectionHasBeenMade"])?'0 /0 ':$val["manualConnectionHasBeenMade"] .' / '.$val["manualConnectionHasBeenMade"]/$val['total'];
			##转人工未接数
			$val["turnManualNotConnected"] = empty($val["turnManualNotConnected"])?'0 /0 ':$val["turnManualNotConnected"] .' / '.$val["turnManualNotConnected"]/$val['total'];
			##客户挂机率
			$val["customerHangUpRate"] = empty($val["customerHangUpRate"])?'0 /0 ':$val["customerHangUpRate"].' / '.$val["customerHangUpRate"]/$val["numberOfNumbersConnected"];
			##接通号码数量
			$val["numberOfNumbersConnected"] = empty($val["numberOfNumbersConnected"])?'0 /0 ':$val["numberOfNumbersConnected"];
			##5秒通话
			$val["5Duration"] = empty($val["5Duration"])?'0 /0 ':$val["5Duration"].' / '.$val["5Duration"]/$val['totalDuration'];
			##5-10秒通话
			$val["510Duration"] = empty($val["510Duration"])?'0 /0 ':$val["510Duration"].' / '.$val["510Duration"]/$val['totalDuration'];
			##10-20秒通话
			$val["1020Duration"] = empty($val["1020Duration"])?'0 /0 ':$val["1020Duration"].' / '.$val["1020Duration"]/$val['totalDuration'];
			##20-30秒通话
			$val["2030Duration"] = empty($val["2030Duration"])?'0 /0 ':$val["2030Duration"].' / '.$val["2030Duration"]/$val['totalDuration'];
			##30-60秒通话
			$val["3060Duration"] = empty($val["3060Duration"])?'0 /0 ':$val["3060Duration"] .' / '.$val["3060Duration"]/$val['totalDuration'];
			##60秒+秒通话
			$val["60Duration"] = empty($val["60Duration"])?'0 /0 ':$val["60Duration"].' /' .$val['60Duration']/$val['totalDuration'];
			#有效通话次数
			$val["numberOfActiveCalls"] = empty($val["answeredCount"])?'0':$val["answeredCount"];
			##总的通话时长
			$val["totalDuration"] = empty($val["totalDuration"])?'0 /0 ':$val["totalDuration"];
			##平均接听时长
			$val["averageAnsweringTime"] = empty($val["averageAnsweringTime"])?'0 /0 ':$val["averageAnsweringTime"] .' / '.$val["averageAnsweringTime"]/$val["totalDuration"];
			##已获取的数量
			$val["acquiredQuantity"] = empty($val["acquiredQuantity"])?'0':$val["acquiredQuantity"];
			##接通率
			$val["callCompletingRate"] = empty($val["callCompletingRate"])?'0':$val['callCompletingRate']/$val['total'];
			##按键值
			$val["keyValue"] = empty($val["keyValue"])?'0':$val["keyValue"];
			##按键率
			$val["keyRate"] = empty($val["keyValue"])?'0':$val["keyValue"]/$val["callNum"];
			$val["cn_name"] = $cn_user[$val["dealuser"]];
			$val["exten"] = $user_exten[$val["dealuser"]];
			##a类客户
			$aClassTotal[] = $val['aClassCount'];
			##b类客户
			$bClassTotal[] = $val['bClassCount'];
			##c类客户
			$cClassTotal[] = $val['cClassCount'];
			##d类客户
			$dClassTotal[] = $val['dClassCount'];
			##所有的分类客户
			$totalClassTotal[] = $val['totalClassCount'];
			##转人工总数
			$turnToArtificialNumberTotal[] = $val['turnToArtificialNumberTotal'];
			##转人工已接
			$manualConnectionHasBeenMadeTotal[] = $val['manualConnectionHasBeenMade'];
			##转人工未接
			$turnManualNotConnectedTotal[] = $val['turnManualNotConnected'];
			##所有的通话分诶
			$turnToArtificialTotal[] = $val['total'];
			##客户挂机率
			$customerHangUpRateTotal[] = $val["customerHangUpRate"];
			##所有已经连接的客户
			$numberOfNumbersConnectedTotal[] = $val["numberOfNumbersConnected"];
			##所有接通的号码
			$numberOfNumbersConnectedTotal[] = $val["numberOfNumbersConnected"];
			##5s通话时长
			$_5DurationTotal[] = $val["5Duration"];
			##10s通话时长
			$_510DurationTotal[] = $val["510Duration"];
			##20s通话时长
			$_1020DurationTotal[] = $val["1020Duration"];
			##30s通话时长
			$_2030DurationTotal[] = $val["2030Duration"];
			##60s通话时长
			$_3060DurationTotal[] = $val["3060Duration"];
			##大于60s通话时长
			$_60DurationTotal[] = $val["60Duration"];
			$DurationTotal[] = $val["totalDuration"];
			$Total[] = $val["total"];
			##总的有效通话次数
			$numberOfActiveCallsTotal[] = $val["answeredCount"];
			##总的接听总时长
			$totalDurationTotal[] = $val["totalDuration"];
			##总的已平均接听时长
			$averageAnsweringTimeTotal[] = $val["averageAnsweringTime"];
			##总的已获取数量
			$acquiredQuantityTotal[] = $val["acquiredQuantity"];
			##总的接通率
			$callCompletingRateTotal[] = $val["callCompletingRate"];
			##总的按键次数
			$keyValueTotal[] = $val["keyValue"];
			##总的按键
			$callNumTotal[] = $val["callNum"];


			$arrNoCall[] = $val["nocall"];
			$arrNoAnswer[] = $val["noAnswer"];
			$arrAnswered[] = $val["answered"];
			$arrUntreated[] = $val["untreated"];
			$arrVisit[] = $val["visit"];
			$arrFailure[] = $val["failure"];
			$arrSuccess[] = $val["success"];
			$arrUntreated_source[] = $val["untreated_source"];
			$arrVisit_source[] = $val["visit_source"];
			$arrFailure_source[] = $val["failure_source"];
			$arrSuccess_source[] = $val["success_source"];

			$val["totalTime"] = sprintf("%02d",intval($val["totalTime"]/3600)).":".sprintf("%02d",intval(($val["totalTime"]%3600)/60)).":".sprintf("%02d",intval((($val[totalTime]%3600)%60)));

			$val["avgTime"] = sprintf("%02d",intval($val["avgTime"]/3600)).":".sprintf("%02d",intval(($val["avgTime"]%3600)/60)).":".sprintf("%02d",intval((($val[avgTime]%3600)%60)));

			$val['throughRate'] = substr($val["answeredCount"]/$val["answertotal"]*100,0,5);
			$val['connectRate'] = substr($val["connectCount"]/$val["answertotal"]*100,0,5);
			$val["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));

			$val["averageduration"] = sprintf("%02d",intval($val["averageduration"]/3600)).":".sprintf("%02d",intval(($val["averageduration"]%3600)/60)).":".sprintf("%02d",intval((($val[averageduration]%3600)%60)));

			$val["duration"] = sprintf("%02d",intval($val["duration"]/3600)).":".sprintf("%02d",intval(($val["duration"]%3600)/60)).":".sprintf("%02d",intval((($val[duration]%3600)%60)));


		}

		if($search_type == "xls"){
			$calltype = $_REQUEST['calltype'];
			if($calltype == "autocall"){
				$field = array("dealuser","cn_name","answered","answertotal","answeredCount","billsec","averageduration","failure","success","visit","failure_source","success_source","visit_source");
				$title = array("工号","姓名","已分配资料数量","呼叫总次数","有效通话次数","接听总时长","平均接听时长","历史失败单","历史成功单","历史回访单","实际失败单","实际成功单","实际回访单");
			}else{
				$field = array("dealuser","cn_name","total","nocall","noAnswer","answered","answertotal","answeredCount","billsec","averageduration","failure","success","visit","failure_source","success_source","visit_source");
				$title = array("工号","姓名","号码总数量","未呼叫号码数量","未接听号码数量","已分配资料数量","呼叫总次数","有效通话次数","接听总时长","平均接听时长","历史失败单","历史成功单","历史回访单","实际失败单","实际成功单","实际回访单");
			}
			$count = count($field);
			$excelTiele = "外呼统计";
			$this->exportData($field,$title,$count,$excelTiele,$arrOutBoundData);
			die;
		}

		$count = count($arrOutBoundData);
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrOutBoundData AS &$v){
			if($i >= $start && $j < $length){
				$arrData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}

		//dump($noGetData);
		//dump($arrData);die;

		$tmp_mon = array(
			array(
				"dealuser"=>"总数",
				"total"=>array_sum($arrTotal),
				"nocall"=>array_sum($arrNoCall),
				"noAnswer"=>array_sum($arrNoAnswer),
				"answered"=>array_sum($arrAnswered),
				"untreated"=>array_sum($arrUntreated),
				// "visit"=>array_sum($arrVisit),
				"failure"=>array_sum($arrFailure),
				"success"=>array_sum($arrSuccess),
				"untreated_source"=>array_sum($arrUntreated_source),
				"visit_source"=>array_sum($arrVisit_source),
				"failure_source"=>array_sum($arrFailure_source),
				"success_source"=>array_sum($arrSuccess_source),
				"aClass"=>empty(array_sum($aClassTotal))?'0 / 0':array_sum($aClassTotal).' / '.array_sum($aClassTotal)/array_sum($totalClassTotal),
				"bClass"=>empty(array_sum($bClassTotal))?'0 / 0':array_sum($bClassTotal).' / '.array_sum($bClassTotal)/array_sum($totalClassTotal),
				"cClass"=>empty(array_sum($cClassTotal))?'0 / 0':array_sum($cClassTotal).' / '.array_sum($cClassTotal)/array_sum($totalClassTotal),
				"dClass"=>empty(array_sum($dClassTotal))?'0 / 0':array_sum($dClassTotal).' / '.array_sum($dClassTotal)/array_sum($totalClassTotal),
				"turnToArtificialNumber"=>empty(array_sum($turnToArtificialNumberTotal))?'0 / 0':array_sum($turnToArtificialNumberTotal).' / '.array_sum($turnToArtificialNumberTotal)/array_sum($turnToArtificialTotal),
				"manualConnectionHasBeenMade"=>empty(array_sum($manualConnectionHasBeenMadeTotal))?'0 / 0':array_sum($manualConnectionHasBeenMadeTotal).' / '.array_sum($manualConnectionHasBeenMadeTotal)/array_sum($turnToArtificialTotal),
				"turnManualNotConnected"=>empty(array_sum($turnManualNotConnectedTotal))?'0 / 0':array_sum($turnManualNotConnectedTotal).' / '.array_sum($turnManualNotConnectedTotal)/array_sum($turnToArtificialTotal),
				"customerHangUpRate"=>empty(array_sum($customerHangUpRate))?'0 / 0':array_sum($customerHangUpRate).' / '.array_sum($customerHangUpRate)/array_sum($numberOfNumbersConnectedTotal),
				"5Duration"=>empty(array_sum($_5DurationTotal))?'0 / 0':array_sum($_5DurationTotal).' / '.array_sum($_5DurationTotal)/array_sum($DurationTotal),
				"510Duration"=>empty(array_sum($_510DurationTotal))?'0 / 0':array_sum($_510DurationTotal).' / '.array_sum($_510DurationTotal)/array_sum($DurationTotal),
				"1020Duration"=>empty(array_sum($_1020DurationTotal))?'0 / 0':array_sum($_1020DurationTotal).' / '.array_sum($_1020DurationTotal)/array_sum($DurationTotal),
				"2030Duration"=>empty(array_sum($_2030DurationTotal))?'0 / 0':array_sum($_2030DurationTotal).' / '.array_sum($_2030DurationTotal)/array_sum($DurationTotal),
				"3060Duration"=>empty(array_sum($_3060DurationTotal))?'0 / 0':array_sum($_3060DurationTotal).' / '.array_sum($_3060DurationTotal)/array_sum($DurationTotal),
				"60Duration"=>empty(array_sum($_60DurationTotal))?'0 / 0':array_sum($_60DurationTotal).' / '.array_sum($_60DurationTotal)/array_sum($DurationTotal),
				"numberOfActiveCalls"=>empty(array_sum($numberOfActiveCalls))?'0':array_sum($numberOfActiveCalls),
				"averageAnsweringTime"=>empty(array_sum($averageAnsweringTime))?'0':array_sum($averageAnsweringTime).' / '.array_sum($averageAnsweringTime)/array_sum($totalDuration),
				"acquiredQuantity"=>empty(array_sum($acquiredQuantity))?'0':array_sum($acquiredQuantity),
				"callCompletingRate"=>empty(array_sum($callCompletingRate))?'0':array_sum($callCompletingRate)/array_sum($Total),
				"keyValue"=>empty(array_sum($keyValueTotal))?'0':array_sum($keyValueTotal),
				"keyRate"=>empty(array_sum($keyValueTotal))?'0':array_sum($keyValueTotal)/array_sum($callNumTotal),


			)
		);
		// dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrR["total"] = $count;
		$arrR["rows"] = $rowsList;
		$arrR["noGetData"] = $noGetData;
		//$arrR["getData"] = array_sum($arrTotal);
		$arrR["getData"] = empty($arrTotal) ? "0" : array_sum($arrTotal);
		$arrR["totalNum"] = $totalNum;
		$arrR["footer"] = $tmp_mon;
		//$_GET['ts_start'] && $_GET['ts_end']
		if($_GET['ts_start']){
			$arrR["date_start"] = $startime;
		}
		if($_GET['ts_end']){
			$arrR["date_end"] = $endtime;
		}

		echo json_encode($arrR);
	}



	function exportData($field,$title,$count,$excelTiele,$arrOrderData){


		vendor("PHPExcel176.PHPExcel");
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			//$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setWrapText(true);    //自动换行
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($arrOrderData as &$val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
				//将单元格设为文本类型----------手机号码
				if($j == "1" || $j == "9"){
					$objPHPExcel->getActiveSheet()->setCellValueExplicit($letters2[$j].$start_row, $val[$field[$j]],PHPExcel_Cell_DataType::TYPE_STRING);
				}
				//$objPHPExcel->getActiveSheet()->getStyle($letters2[$j].$start_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			}
			$start_row++;
		}
		$objPHPExcel->setActiveSheetIndex(0);

		$filename = iconv("utf-8","gb2312",$excelTiele);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}


}
?>
