<?php
class OrderManagementAction extends Action{
	function orderList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Order Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function orderData(){
		header("Content-Type:text/html; charset=utf-8");
		$menuname = "Order Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$calldate_start = $_REQUEST['calldate_start'];
		$calldate_end = $_REQUEST['calldate_end'];
        $order_num = $_REQUEST['order_num'];
        $consignee = $_REQUEST['consignee'];
        $email = $_REQUEST['email'];
        $phone = $_REQUEST['phone'];
        $telephone = $_REQUEST['telephone'];
        //$delivery_address = $_REQUEST['delivery_address'];
        $zipcode = $_REQUEST['zipcode'];
        //$country = $_REQUEST['country'];
        $province = $_REQUEST['province'];
        $city = $_REQUEST['city'];
        $district = $_REQUEST['district'];
        $street = $_REQUEST['street'];
        $order_status = $_REQUEST['order_status'];
        $pay_status = $_REQUEST['pay_status'];
        $shipping_status = $_REQUEST['shipping_status'];
        $createname = $_REQUEST['createname'];
        $logistics_account = $_REQUEST['logistics_account'];
        $logistics_state = $_REQUEST['logistics_state'];
        $shopping_name = $_REQUEST['shopping_name'];
        $start_receipttime = $_REQUEST['start_receipttime'];
        $end_receipttime = $_REQUEST['end_receipttime'];
        $create_user = $_REQUEST['create_user'];
        $search_type = $_REQUEST['search_type'];


		$where = "1 ";
        $where .= empty($calldate_start)?"":" AND createtime >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND createtime <'$calldate_end'";
		$where .= empty($order_num)?"":" AND order_num like '%$order_num%'";
		$where .= empty($consignee)?"":" AND consignee like '%$consignee%'";
		$where .= empty($email)?"":" AND email like '%$email%'";
		$where .= empty($phone)?"":" AND phone like '%$phone%'";
		$where .= empty($telephone)?"":" AND telephone like '%$telephone%'";
		$where .= empty($createname)?"":" AND createname like '%$createname%'";
		$where .= empty($logistics_account)?"":" AND logistics_account like '%$logistics_account%'";
		$where .= empty($zipcode)?"":" AND zipcode like '%$zipcode%'";
		//$where .= empty($country)?"":" AND country = '$country'";
		$where .= empty($province)?"":" AND province = '$province'";
		$where .= empty($city)?"":" AND city = '$city'";
		$where .= empty($district)?"":" AND district = '$district'";
		$where .= empty($street)?"":" AND street = '$street'";
		if($order_status == "0"){
			$where .= " AND order_status = '0'";
		}else{
			$where .= empty($order_status)?"":" AND order_status = '$order_status'";
		}
		$where .= empty($pay_status)?"":" AND pay_status = '$pay_status'";
		if($shipping_status == "0"){
			$where .= " AND shipping_status = '0'";
		}else{
			$where .= empty($shipping_status)?"":" AND shipping_status = '$shipping_status'";
		}
		if($logistics_state == "0"){
			$where .= " AND logistics_state = '$logistics_state'";
		}elseif($logistics_state == "all"){
			$where .= " AND logistics_state != '' AND logistics_state is not null";
		}else{
			$where .= empty($logistics_state)?"":" AND logistics_state = '$logistics_state'";
		}
		$where .= empty($shopping_name)?"":" AND shopping_name = '$shopping_name'";
        $where .= empty($start_receipttime)?"":" AND logistics_time >= '$start_receipttime'";
        $where .= empty($end_receipttime)?"":" AND logistics_time <= '$end_receipttime'";
		$where .= empty($create_user)?"":" AND createname = '$create_user'";



		$username = $_SESSION["user_info"]["username"];
		$r_id = $_SESSION["user_info"]["r_id"];
		$role = new Model("role");
		$rData = $role->where("r_id = '$r_id'")->find();
		$order_info = new Model("order_info");
		//if( ($username == "admin" || $priv['finance'] == "Y" || $priv['confirm_order'] == "Y" || $priv['package'] == "Y") && $priv['members_bomb'] == "Y" ){
		if(  $username == "admin" || $priv['members_bomb'] == "Y"  ){
			$count = $order_info->where($where)->count();
		}else{
			$count = $order_info->where("$where AND createname ='$username'")->count();
		}
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if(  $username == "admin" || $priv['members_bomb'] == "Y"  ){
			if($search_type == "xls"){
				$arrOrderData = $order_info->order("createtime desc")->where($where)->select();
			}else{
				$arrOrderData = $order_info->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
			}
		}else{
			if($search_type == "xls"){
				$arrOrderData = $order_info->order("createtime desc")->where("$where AND createname ='$username'")->select();
			}else{
				$arrOrderData = $order_info->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where("$where AND createname ='$username'")->select();
			}
		}

		$i = 0;
		$row_order_status = array('0'=>'未确认','1'=>'已确认','2'=>'<span style="color:#FF6600;">取消</span>','3'=>'<span style="color:#FF6600;">待审核</span>','4'=>'<span style="color:red;font-weight:bold;">退货</span>','5'=>'已分单','6'=>'部分分单','7'=>"未通过",'8'=>"问题单");
		$row_pay_status = array('N'=>'未付款','M'=>'付款中','Y'=>'<span style="color:#9933CC;font-weight:bold;">已付款</span>');
		$row_shipping_status = array('0'=>'<span style="color:#FF9933;font-weight:bold;">未发货</sapn>','1'=>'已发货','2'=>'<span style="color:#0000FF;font-weight:bold;">收货确认</span>','3'=>'配货中','4'=>'已发货(部分商品)','5'=>'发货中');
		//$row_shopping_name = array("1"=>"顺丰快递","2"=>"圆通速递","3"=>"邮政快递包裹","4"=>"市内快递","5"=>"申通快递","6"=>"邮局平邮","7"=>"城际快递");
		$row_shopping_name = logisticsMode();
		$row_refund = array("1"=>"生成退款申请","2"=>"不处理，错误操作时选此项","3"=>"退回用户余额");

		$row_payment = array("1"=>"余额支付","2"=>"银行汇款/转帐","3"=>"货到付款");

		$row_logistics_state = array("0"=>"在途","1"=>"揽件","2"=>"疑难","3"=>"签收","4"=>"退签","5"=>"派件","6"=>"退回");

		$userArr = readU();
		$cnName = $userArr["cn_user"];

		foreach($arrOrderData as &$val){
			$val["phone_source"] = $val["phone"];
			if($username != "admin"){
				if($para_sys["hide_phone"] =="yes"){
					$val["phone"] = substr($val["phone"],0,3)."***".substr($val["phone"],-4);
				}
			}
			$val['cn_name'] = $cnName[$val["createname"]];
			//$val["order_goods_num"] = explode(",",$val["order_goods_num"]);
			$order_status = $row_order_status[$val['order_status']];
			$val['order_status'] = $order_status;

			$pay_status = $row_pay_status[$val['pay_status']];
			$val['pay_status'] = $pay_status;

			$shipping_status = $row_shipping_status[$val['shipping_status']];
			$val['shipping_status'] = $shipping_status;

			$shopping_name = $row_shopping_name[$val['shopping_name']];
			$val['shopping_name'] = $shopping_name;

			$payment = $row_payment[$val['payment']];
			$val['payment'] = $payment;

			$val["logistics_state2"] = $row_logistics_state[$val["logistics_state"]];

			$val['status'] = $val['order_status']."  ".$val['pay_status']."   ".$val['shipping_status'];
			$val['status2'] = strip_tags($val['status']);

			$refund1 = $row_refund[$val['refund1']];
			$val['refund1'] = $refund1;
			$refund4 = $row_refund[$val['refund4']];
			$val['refund4'] = $refund4;
			$refund3 = $row_refund[$val['refund3']];
			$val['refund3'] = $payment;

			if($val["explanation1"]){
				$val["pay_operating"] = "操作备注：".$val["description1"]."<br> 退款说明：".$val["explanation1"]."<br> 退款方式：".$val["refund1"]."<br> ";		//订单操作----设置为未付款--信息
			}else{
				$val["pay_operating"] = "";
			}
			if($val["explanation4"]){
				$val["reset_operating"] = "操作备注：".$val["description4"]."<br> 退款说明：".$val["explanation4"]."<br> 退款方式：".$val["refund4"]."<br> ";		//订单操作----退货--信息
			}else{
				$val["reset_operating"] = "";
			}
			if($val["explanation3"]){
				$val["cancel_operating"] = "操作备注：".$val["description3"]."<br> 退款说明：".$val["explanation3"]."<br> 退款方式：".$val["refund3"]."<br> ";		//订单操作----取消--信息
			}else{
				$val["cancel_operating"] = "";
			}

			if($val["pay_operating"]){
				$val["operating_order"] = "订单操作为设置为未付款的信息：<br>".$val["pay_operating"]."<br>";
			}
			if($val["reset_operating"]){
				$val["operating_order"] .= "订单操作为退货的信息：<br>".$val["reset_operating"]."<br>";
			}
			if($val["cancel_operating"]){
				$val["operating_order"] .= "订单操作为取消的信息：<br>".$val["cancel_operating"];
			}
			if($val["operating_order"]){
				$val["operating2"] = "鼠标移到这里查看取消、退货等信息";
			}else{
				$val["operating2"] = "";
			}

			$val["operations"] = "<a href='javascript:void(0);' onclick=\"exportGoods("."'".$val["id"]."','".$val["goods_id"]."'".")\" >导出</a>";

			if($val["logistics_state"] || $val["logistics_state"] == "0"){
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"viewLogisticsInfo("."'".$val["logistics_account"]."','".$val["logistics_state"]."','".$val["logistics_Info"]."'".")\" >查看物流</a>";
			}else{
				$val["operating"] = "";
			}

			$i++;
		}
		unset($i);

		$arrFooter = $order_info->order("createname asc")->field("sum(cope_money) as cope_money")->where($where)->find();
		$arrFooter["createname"] = "总计";
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$field = array("consignee","phone","createname","cn_name","createtime","goods_amount","shipping_fee","cope_money","status2","delivery_address","payment","shopping_name","logistics_account","order_goods_num");
			$title = array("收货人","收货人号码","下单坐席","坐席姓名","下单时间","总金额","优惠金额","应付金额","状态","地址","支付方式","配送方式","物流号","商品信息");
			$count = count($field);
			$excelTiele = "订单列表";
			//echo $order_info->getLastSql();die;
			//dump($arrOrderData);die;
			array_push($arrOrderData,$arrFooter);
			$this->exportData($field,$title,$count,$excelTiele,$arrOrderData);
			die;
		}

		//dump($arrOrderData);die;
		//echo $order_info->getLastSql();die;
		$rowsList = count($arrOrderData) ? $arrOrderData : false;
		$arrOrder["total"] = $count;
		$arrOrder["rows"] = $rowsList;
		$arrOrder["footer"] = $arrFooter2;

		echo json_encode($arrOrder);
	}

	function addOrder(){
		checkLogin();
		$times = date("Y-m-d H:i:s");
		$this->assign("times",$times);

		$this->display();
	}

	function insertOrder(){
		//dump($_REQUEST);die;
		$username = $_SESSION["user_info"]["username"];
		$order_num = date("YmdHis").$username;
		$info = explode(",",$_REQUEST['info']);

		foreach($info as $vm){
			$goodInfo[] = explode("-",$vm);
		}

		foreach($goodInfo as $v){
			$g_id[] = $v[0];
			$g_name[] = $v[4]."-".$v[2];
			$price[] = $v[3];
			$nums[] = $v[1];
			//$g_num[] = $v[0]."-".$v[4]."-".$v[1]."-".$v[5];  //商品id-商品名称-商品数量-商品货号
			$g_num[] = $v[4]."[".$v[5]."]*".$v[1];  //商品名称[商品货号]*商品数量
		}
		//dump($goodInfo);die;
		$goods_total = array_sum($nums);
		$goods_id = implode(",",$g_id);
		$goods_name = implode(",",$g_name);
		$goods_amount = array_sum($price);
		$order_goods_num = implode(",",$g_num);

		$shipping_fee = isset($_REQUEST['shipping_fee']) ? $_REQUEST['shipping_fee'] : "0.00";

		$order_type = $_REQUEST["order_type"];
		$cope_money = $goods_amount - $shipping_fee;
		$order_info = new Model("order_info");
		$arrData = array(
			"order_num"=>$order_num,
			"goods_id"=>$goods_id,
			"goods_name"=>$goods_name,
			"customer_id"=>$_REQUEST['customer_id'],
			"consignee"=>$_REQUEST['consignee'],
			"createname"=>$username,
			"createtime"=>$_REQUEST['createtime'],
			"goods_amount"=>$goods_amount,
			"goods_total"=>$goods_total,
			"cope_money"=>$cope_money,
			"shipping_fee"=>$shipping_fee,
			"order_status"=>'3',             //状态
			"pay_status"=>'N',
			"shipping_status"=>'0',
			"country"=>$_REQUEST['country'],
			"province"=>$_REQUEST['province'],
			"city"=>$_REQUEST['city'],
			"district"=>$_REQUEST['district'],
			"street"=>$_REQUEST['street'],
			"delivery_address"=>$_REQUEST['delivery_address'],
			"zipcode"=>$_REQUEST['zipcode'],
			"telephone"=>$_REQUEST['telephone'],
			"phone"=>$_REQUEST['phone'],
			"email"=>$_REQUEST['email'],
			"description"=>$_REQUEST['description2'],
			"purchase_time"=>$_REQUEST['purchase_time'],
			"order_type"=>$order_type,
			"order_goods_num"=>$order_goods_num
		);
		//dump($arrData);die;
		$result = $order_info->data($arrData)->add();
		if($result){
			$order_goods = new Model("order_goods");
			$sql = "insert into order_goods(goods_id,goods_num,goods_price,goods_money,order_id) values";
			$value = "";
			foreach($goodInfo as $row){
				$str = "(";
				$str .= "'" .$row[0]. "',";  	 //商品id
				$str .= "'" .$row[1]. "',";  	 //商品数量
				$str .= "'" .$row[2]. "',";  	 //商品价格
				$str .= "'" .$row[3]. "',";  	 //商品总价格
				$str .= "'" .$result. "'";	 //订单id---最后一个不需要","

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
			if( $value ){
				$sql .= $value;
				//dump($sql);die;
				$res = $order_goods->execute($sql);
			}
		}
		//echo $order_info->getLastSql();die;
		if ($result && $res){
			echo json_encode(array('success'=>true,'msg'=>'订单添加成功！'));
		} else {
			echo json_encode(array('msg'=>'订单添加失败！'));
		}
	}

	function editOrder(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Order Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$id = $_REQUEST['id'];
		$order_info = new Model("order_info");
		$sql = "SHOW COLUMNS FROM order_info";
		$arrF = $order_info->query($sql);
		foreach($arrF as $val){
			$arrT[] = "o.".$val["Field"];
		}
		$fields = implode(",",$arrT).",c.name";

		$orlist = $order_info->table("order_info o")->field($fields)->join("customer c on (o.customer_id=c.id)")->where("o.id = '$id'")->find();

		$para_sys = readS();
		if($para_sys["editOrder_hide"] =="phone" || $para_sys["editOrder_hide"] == "all" ){
			$orlist["phone_ms"] = $orlist["phone"];
			$orlist["telephone_ms"] = $orlist["telephone"];
			$orlist["phone"] = substr($orlist["phone"],0,3)."***".substr($orlist["phone"],-4);
			if($orlist["telephone"]){
				$orlist["telephone"] = substr($orlist["telephone"],0,3)."***".substr($orlist["telephone"],-3);
			}
			$this->assign("message_phone","ms");
			$this->assign("message_telephone","ms");
		}else{
			$orlist["phone_ms"] = $orlist["phone"];
			$orlist["telephone_ms"] = $orlist["telephone"];
			$this->assign("message_phone","show_phone");
			$this->assign("message_telephone","show_phone");
		}

		if($orlist["email"]){
			if($para_sys["editOrder_hide"] =="email" || $para_sys["editOrder_hide"] == "all" ){
				$orlist["email_ms"] = $orlist["email"];
				$orlist["email"] = "***".strstr($orlist["email"],"@");
				$this->assign("message_email","ms");
			}else{
				$orlist["email_ms"] = $orlist["email"];
				$this->assign("message_email","show_email");
			}
		}

		if($orlist["purchase_time"] == "0000-00-00 00:00:00"){
			$orlist["purchase_time"] = "";
		}
		$this->assign('orlist',$orlist);
		$this->assign('id',$id);

		$order_type = empty($_REQUEST["order_type"]) ? "list" : $_REQUEST["order_type"];
		$this->assign('order_type',$order_type);

		$this->display();
	}

	function updateOrder(){
		$id = $_REQUEST['id'];
		$username = $_SESSION["user_info"]["username"];
		/*
		$goods_id = $_REQUEST['goods_id'];
		$goods_name = $_REQUEST['goods_name'];
		$name = explode(",",$goods_name);
		foreach($name as $v){
			$price[] = explode("-",$v);
		}
		foreach($price as $vm){
			$prices[] = $vm[1];
		}
		$goods_amount = array_sum($prices);


		$cope_money = $goods_amount - $shipping_fee;
		*/

		if($_REQUEST['message_phone'] == "ms"){
			$phone = $_REQUEST['mess_phone'];
		}else{
			$phone = $_REQUEST['phone'];
		}
		if($_REQUEST['message_telephone'] == "ms"){
			$telephone = $_REQUEST['mess_telephone'];
		}else{
			$telephone = $_REQUEST['telephone'];
		}
		if($_REQUEST['message_email'] == "ms"){
			$email = $_REQUEST['mess_email'];
		}else{
			$email = $_REQUEST['email'];
		}

		$shipping_fee = isset($_REQUEST['shipping_fee']) ? $_REQUEST['shipping_fee'] : "0.00";
		$order_type = $_REQUEST["order_type"];
		//dump($cope_money);
		//dump($name);die;
		$order_info = new Model("order_info");
		$arrData = array(
			//"customer_id"=>$_REQUEST['customer_id'],
			//"consignee"=>$_REQUEST['consignee'],
			"shipping_fee"=>$shipping_fee,
			"country"=>$_REQUEST['country'],
			"province"=>$_REQUEST['province'],
			"city"=>$_REQUEST['city'],
			"district"=>$_REQUEST['district'],
			"street"=>$_REQUEST['street'],
			"delivery_address"=>$_REQUEST['delivery_address'],
			"zipcode"=>$_REQUEST['zipcode'],
			"telephone"=>$telephone,
			"phone"=>$phone,
			"email"=>$email,
			"description"=>$_REQUEST['description2'],
			"logistics_account"=>$_REQUEST['logistics_account2'],   //物流号
			"purchase_time"=>$_REQUEST['purchase_time'],
			"order_type"=>$order_type,
		);
		//dump($arrData);die;
		$result = $order_info->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}


	function editLogistics(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Order Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$id = $_REQUEST['id'];
		$order_info = new Model("order_info");
		$sql = "SHOW COLUMNS FROM order_info";
		$arrF = $order_info->query($sql);
		foreach($arrF as $val){
			$arrT[] = "o.".$val["Field"];
		}
		$fields = implode(",",$arrT).",c.name";

		$orlist = $order_info->table("order_info o")->field($fields)->join("customer c on (o.customer_id=c.id)")->where("o.id = '$id'")->find();

		if($orlist["purchase_time"] == "0000-00-00 00:00:00"){
			$orlist["purchase_time"] = "";
		}

		$this->assign('orlist',$orlist);
		$this->assign('id',$id);

		$this->display();
	}

	function updateLogistics(){
		$id = $_REQUEST['id'];
		$order_info = M("order_info");
		$arrData = array(
			'shopping_name'=>$_REQUEST['shopping_name'],
			'logistics_account'=>$_REQUEST['logistics_account'],
			'shipping_status'=>$_REQUEST['shipping_status'],
		);
		$result = $order_info->data($arrData)->where("id in ($id)")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}


	//编辑订单时保存商品
	function saveShip(){
		$order_id = $_REQUEST['order_id'];
		$data = $_REQUEST['data'];
		$info = explode(",",$_REQUEST['info']);

		foreach($info as $vm){
			$goodInfo[] = explode("-",$vm);
		}

		foreach($goodInfo as $v){
			$g_id[] = $v[0]; //商品id
			$nums[] = $v[1];   //商品数量
			$goods_money[] = $v[3]; //总价格
			//$g_name[] = $v[4]."-".$v[2];  //商品名称-商品单价
			//$g_name[] = $v[0]."`*`".$v[1]."`*`".$v[2]."`*`".$v[3]."`*`".$v[4]."`*`".$v[5]."`*`".$v[6]."`*`".$v[7];  //商品id-商品数量-商品单价-商品总价格-商品名称-商品货号-商品分类-商品品牌
			$g_name[] = array(
				"id"=>$v[0],			//商品id
				"goods_num"=>$v[1],		//商品数量
				"price"=>$v[2],			//商品单价
				"goods_money"=>$v[3],	//商品总价格
				"good_name"=>$v[4],		//商品名称
				"good_num"=>$v[5],		//商品货号
				"cat_name"=>$v[6],		//商品分类
				"brand_name"=>$v[7],	//商品品牌
			);
			$g_num[] = $v[4]."[".$v[5]."]*".$v[1];  //商品名称[商品货号]*商品数量
		}
		//dump($goodInfo);die;
		$goods_total = array_sum($nums);
		$goods_id = implode(",",$g_id);
		//$goods_name = implode(",",$g_name);
		$goods_name = json_encode($g_name);
		$goods_amount = array_sum($goods_money);
		$order_goods_num = implode(",",$g_num);

		$order_info = new Model("order_info");
		$cope = $order_info->where("id = $order_id")->find();
		$cope_money = $goods_amount-$cope['shipping_fee'];
		$arrData = array(
			"goods_id"=>$goods_id,
			"goods_name"=>$goods_name,
			"goods_amount"=>$goods_amount,
			"cope_money"=>$cope_money,
			"goods_total"=>$goods_total,
			"order_goods_num"=>$order_goods_num,
		);
		//dump($cope_money);
		//dump($arrData);die;
		$res = $order_info->data($arrData)->where("id = '$order_id'")->save();

		$order_goods = new Model("order_goods");
		$goods_del = $order_goods->where("order_id = '$order_id'")->delete();
		$sql = "insert into order_goods(goods_id,goods_num,goods_price,goods_money,order_id) values";
		$value = "";
		foreach($goodInfo as $row){
			$str = "(";
			$str .= "'" .$row[0]. "',";  	 //商品id
			$str .= "'" .$row[1]. "',";  	 //商品数量
			$str .= "'" .$row[2]. "',";  	 //商品价格
			$str .= "'" .$row[3]. "',";  	 //商品总价格
			$str .= "'" .$order_id. "'";	 //订单id---最后一个不需要","

			$str .= ")";
			$value .= empty($value)?"$str":",$str";
		}
		if( $value ){
			$sql .= $value;
			//dump($sql);die;
			$result = $order_goods->execute($sql);
		}
		if ($result && $res !== false){
			echo json_encode(array('success'=>true,'msg'=>'商品保存成功！'));
		} else {
			echo json_encode(array('msg'=>'商品保存失败！'));
		}
	}


	function deleteOrder(){
		$id = $_REQUEST["id"];
		$order_info = new Model("order_info");
		$result = $order_info->where("id in ($id)")->delete();
		$order_goods = new Model("order_goods");
		$res = $order_goods->where("order_id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}

	}

	//编辑时商品列表
	function goodsData(){
		$goods_id = $_REQUEST['goods_id'];
		$order_id = $_REQUEST['order_id'];
		$goods = new Model("shop_goods");
		$count = $goods->where("id in ($goods_id)")->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$goodsData = $goods->table("shop_goods g")->order("g.good_order desc")->field("g.id,g.good_name,g.good_num,g.good_Inventory,g.price,g.cate_id,g.brand_id,g.simple_description,g.good_description,g.good_order,g.good_enabled,g.original_img,g.thumb_path,g.good_img,g.img_path,c.cat_name,b.brand_name,og.goods_num,og.goods_money")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->join("order_goods og on g.id = og.goods_id")->join("order_info o on o.id = og.order_id")->limit($page->firstRow.','.$page->listRows)->where("g.id in ($goods_id) AND og.order_id = $order_id")->select();

		$i = 0;
		foreach($goodsData as $vm){
			$goodsData[$i]['operating'] = '<a href="javascript:void(0);" onclick="removePd();">移除</a>';

			//$goodsData[$i]['operating'] = "<a href='javascript:void(0);' onclick=\"remove("."'".$vm["id"]."'".")\" >". 移除 ."</a>";
			//$goodsData[$i]['amoney'] = $vm["price"]*$vm["goods_num"];
			$amoney[] = $vm['goods_money'];
			$i++;
		}
		$tpl_money = array_sum($amoney);
		//dump($goodsData);die;
		//$tmp_mon = array(array("good_name"=>"总计","goods_money"=>$tpl_money));
		$tmp_mon = array(array("goods_money"=>"总计: ".$tpl_money));

		//echo $goods->getLastSql();die;
		$rowsList = count($goodsData) ? $goodsData : false;
		$arrProduct["total"] = $count;
		$arrProduct["rows"] = $rowsList;
		$arrProduct["footer"] = $tmp_mon;
		//dump($arrProduct);die;
		echo json_encode($arrProduct);
	}

	//保存状态
	function updateStatus(){
		$order_info = new Model("order_info");

		$username = $_SESSION["user_info"]["username"];
		$para_sys = readS();
		$id = $_REQUEST['id'];
		$customer_id = $_REQUEST['customer_id2'];
		$createname = $_REQUEST['createname'];
		$order_num = $_REQUEST['order_num2'];  //订单号
		$status = $_REQUEST['status'];
		$order = strpos($status,"order");
		$pay = strpos($status,"pay");
		$ship = strpos($status,"ship");
		$push_user = "";   //是否推送
		$push_msg = "";   //推送的订单状态
		if($order){
			if($status == "confirm_order"){   //确认订单
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"order_status"=>"1",   //已确认
				);
				$push_user .= "Y";
				$push_msg .= "已确认";
			}elseif($status == "confirm_todelivery_order"){   //确认订单-直接发货
				$arrData = array(
					"order_status"=>"1",
					"payment"=>"3",
					"pay_status"=>"N",   //已付款
					"paytime"=>date("Y-m-d H:i:s"),
					"shipping_status"=>'3',   //配货中
					"delivery_order"=>"00".date("YmdHis"),
					"shopping_name"=>$_REQUEST['shopping_name'],  //配送方式
					"examine_time"=> date("Y-m-d H:i:s")
				);
				$push_user .= "Y";
				$push_msg .= "已确认";
			}elseif($status == "not_order"){   //待审核
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"order_status"=>"3",   //待审核
				);
				$push_user .= "Y";
				$push_msg .= "待审核";
			}elseif($status == "cancel_order"){    //取消
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"order_status"=>"2",   //取消
					"pay_status"=>"N",     //未付款
				);
				$push_user .= "Y";
				$push_msg .= "取消";
			}elseif($status == "reset_order"){   //退货
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"order_status"=>"4",   //退货
					"pay_status"=>"N",  //未付款
					"shipping_status"=>"0",  //未发货
				);
				$push_user .= "Y";
				$push_msg .= "退货";
				$this->productUpdates2($id);
			}elseif($status == "confirm_notby_order"){   //未通过
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"order_status"=>"7",   //未通过
				);
				$push_user .= "Y";
				$push_msg .= "未通过";
			}elseif($status == "confirm_suspected_order"){   //问题单
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"order_status"=>"8",   //问题单
				);
				$push_user .= "Y";
				$push_msg .= "问题单";
			}else{
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"order_status"=>"0",
				);
			}
			$result = $order_info->data($arrData)->where("id = '$id'")->save();
			if ($result !== false){
				$this->saveTrack($id,$_REQUEST['description']);
				//积分--如果有快递100推送的就改成在签收之后，否者就确定订单之后
				if(!$para_sys["express_key"]){
					$this->saveIntegral($customer_id);
				}
				echo json_encode(array('success'=>true,'msg'=>'订单状态已改变！','user_name'=>$createname,'push_user'=>$push_user,'order_num'=>$order_num,'push_msg'=>$push_msg));
			}else{
				echo json_encode(array('msg'=>'订单状态改变失败！'));
			}
		}
		if($pay){
			if($status == "confirm_pay"){    //付款
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"payment"=>$_REQUEST['payment'],
					"pay_status"=>"Y",   //已付款
					"paytime"=>date("Y-m-d H:i:s"),
				);
			}elseif($status == "confirm_pay_mod"){    //付款方式
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"payment"=>$_REQUEST['payment'],
					"pay_status"=>"N",   //已付款
					"paytime"=>date("Y-m-d H:i:s"),
				);
			}elseif($status == "not_pay"){   //设为未付款
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"pay_status"=>"N",     //未付款
					"order_status"=>"1",   //已确认
					"shipping_status"=>"0",  //未发货
					"payment"=>"",  //付款方式
				);
			}else{
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"pay_status"=>"M",
				);
			}
			$result = $order_info->data($arrData)->where("id = '$id'")->save();
			if ($result !== false){
				$this->saveTrack($id,$_REQUEST['description']);
				echo json_encode(array('success'=>true,'msg'=>'付款状态已改变！','user_name'=>$createname,'push_user'=>$push_user,'order_num'=>$order_num,'push_msg'=>$push_msg));
			}else{
				echo json_encode(array('msg'=>'付款状态改变失败！'));
			}
		}
		if($ship){
			if($status == "picking_ship"){    //配货
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"shipping_status"=>'3',    //配货中
				);
			}elseif($status == "delivery_ship"){   //生成发货单
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"shipping_status"=>'3',   //配货中
					"delivery_order"=>"00".date("YmdHis"),
				);
			}elseif($status == "confirm_ship"){   //发货
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"shopping_name"=>$_REQUEST['shopping_name'],
					"logistics_account"=>$_REQUEST['logistics_account'],   //物流号
					"shipping_status"=>'1',   //已发货
					"deliverytime"=>date("Y-m-d H:i:s"),
					"enbaled"=>"Y",
				);
				$this->productUpdates($id);
			}elseif($status == "receipt_ship"){   //已收货
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"shipping_status"=>'2',   //收货确认
					"receipttime"=>date("Y-m-d H:i:s"),
				);
			}elseif($status == "not_ship"){   //未发货
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"shipping_status"=>'0',   //未发货
				);
			}else{
				$arrData = array(
					//"description"=>$_REQUEST['description'],
					"shipping_status"=>'0',  //未发货
				);
			}
			$result = $order_info->data($arrData)->where("id = '$id'")->save();

			if ($result !== false){
				$this->saveTrack($id,$_REQUEST['description']);
				//echo $order_action->getLastSql();die;

				if($status == "confirm_ship" && $para_sys["express_key"]){
					$arrData = $order_info->field("id,shopping_name,logistics_account")->where("id = '$id'")->find();
					$this->pushExpress($arrData["logistics_account"],$arrData["shopping_name"]);
				}

				echo json_encode(array('success'=>true,'msg'=>'发货状态已改变！','user_name'=>$createname,'push_user'=>$push_user,'order_num'=>$order_num,'push_msg'=>$push_msg));
			}else{
				echo json_encode(array('msg'=>'发货状态改变失败！'));
			}
		}

	}


	/*将物流号推送到快递100
	 *logistics_account：物流号， express_id：配送方式
	*/
	function pushExpress($logistics_account,$express_id){
		$para_sys = readS();
		//var_dump($para_sys);die;
		$post_data = array();
		$post_data["schema"] = 'json' ;
		//$callbackurl="http://60.174.207.179:38180/index.php?m=ExpressDelivery&a=expressCallBack";
		$callbackurl="http://".$para_sys["public_network_ip"]."/index.php?m=ExpressDelivery&a=expressCallBack";

		$logistics_mode = M("logistics_mode");
		$arrM = $logistics_mode->select();
		foreach($arrM as $val){
			$row_shopping_name[$val["id"]] = $val["com_code"];
		}

		$company_name = $row_shopping_name[$express_id];
		$post_data["param"] = '{"company":"'. $company_name .'", "number":"'. $logistics_account .'","from":"", "to":"", "key":"'.$para_sys["express_key"].'", "parameters":{"callbackurl":"'. $callbackurl .'"}}';
		$url='http://www.kuaidi100.com/poll';
		$o="";
		foreach ($post_data as $k=>$v){
			$o.= "$k=".urlencode($v)."&";
		}

		$post_data=substr($o,0,-1);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		//dump($result);
		return $result;
	}


	//用户积分
	function saveIntegral($customer_id){
		//$customer_id = "48";
		$order_info = new Model("order_info");
		$arrF = $order_info->field("sum(goods_amount) as goods_amount,id")->where("customer_id = '$customer_id' AND order_status = '1'")->find();
		$integral = $arrF["goods_amount"];
		if(!$integral || $integral == "0.00"){
			$integral = "0";
		}
		//echo $order_info->getLastSql();

		$membership_level = M("membership_level");
		$arrML = $membership_level->where(" start_num <= '$integral' AND end_num >= '$integral'")->find();
		$groups = $arrML["groups"];
		if(!$arrML){
			if(!$integral){
				$groups = "failure";
			}else{
				$arrML2 = $membership_level->where("end_num = '0'")->find();
				$groups = $arrML2["groups"];
			}
		}

		$customer = M("customer");
		$arrData = array(
			"integral"=>$integral,
			"member_Level"=>$groups,
			"order_status2"=>"Y",
		);
		$res = $customer->data($arrData)->where("id = '$customer_id'")->save();
		return true;
	}

	function saveTrack($id,$description){
		$username = $_SESSION["user_info"]["username"];
		$order_info = new Model("order_info");
		$arrD = $order_info->where("id = '$id'")->find();
		$customer_id = $arrD["customer_id"];
		$order_action = new Model("order_action");
		$arrF = array(
			"order_id" => $id,
			"action_time" => date("Y-m-d H:i:s"),
			"action_user" => $username,
			"order_status" => $arrD["order_status"],
			"pay_status" => $arrD["pay_status"],
			"shipping_status" => $arrD["shipping_status"],
			"action_description" => $description,
			"uniqueid"=>$_SESSION[$username."_order"][$customer_id],
		);
		//dump($arrF);
		$res = $order_action->data($arrF)->add();
	}

	//审批预存--更改审批状态
	function updateApproval(){
		$id = $_REQUEST['id'];
		$customer_id = $_REQUEST['customer_id'];
		$stored_sk = $_REQUEST['stored_sk'];
		$approval_description = $_REQUEST["approval_description"];

		$order_info = new Model("order_info");
		$arrOrderData = $order_info->field("id,cope_money,stored_values,order_num,createname,approval_money")->where("id='$id'")->find();
		$user_name = $arrOrderData["createname"];
		$arrData = array(
			'stored_sk'=>$stored_sk,
			'approval_enable'=>"Y"
		);
		//dump($customer_id);die;
		$result = $order_info->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			$this->saveTrack($id,$approval_description);
			$customer = M("customer");
			if($stored_sk == "Y"){
				//$this->updateCustomer($id,$customer_id);
				$approval_money = $arrOrderData["approval_money"];
				$stored_values = $arrOrderData["stored_values"];
				if($stored_values > $approval_money){
					//预存金额大于应付金额
					$save_cope_money = "0";
					$stored_value = $stored_values-$approval_money;
				}else{
					$stored_value = '';
					$save_cope_money = $approval_money-$stored_values;
				}
				//dump($stored_value);die;
				$res = $customer->where("id = '$customer_id'")->save(array("stored_value"=>$stored_value));
				$res2 = $order_info->where("id = '$id'")->save(array("approval_money"=>$save_cope_money));
			}else{
				$customer->where("id = '$customer_id'")->save(array("stored_value"=>""));
			}
			echo json_encode(array('success'=>true,'msg'=>"更新成功！",'user_name'=>$user_name,'push_msg'=>'预存审批通过','order_num'=>$arrOrderData["order_num"]));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	//审批预存--更改订单信息、客户资料
	function updateCustomer($id,$customer_id){
		$order_info = M("order_info");
		$arrOrderData = $order_info->field("id,cope_money,stored_values")->where("id='$id'")->find();
		$cope_money = $arrOrderData["cope_money"];
		$stored_values = $arrOrderData["stored_values"];
		if($stored_values > $cope_money){
			//预存金额大于应付金额
			$save_cope_money = "0";
			$stored_value = $stored_values-$cope_money;
			$customer = M("customer");
			$res = $customer->where("id = '$customer_id'")->save(array("stored_value"=>$stored_value));
		}else{
			$save_cope_money = $cope_money-$stored_values;
		}
		$result = $order_info->where("id = '$id'")->save(array("cope_money"=>$save_cope_money));
		return true;
	}

	//发货后对应的商品库存减少
	function productUpdates($id){
		$order_goods = new Model("order_goods");
		$orgData = $order_goods->order("goods_id asc")->field("goods_id,id,order_id,goods_num")->where("order_id = '$id'")->select();

		foreach($orgData as $v){
			$gid[] = $v["goods_id"];
		}
		$good_id = implode(",",$gid);
		$shop_goods = new Model("shop_goods");
		$goodData = $shop_goods->order("id asc")->field("id as g_id,good_Inventory,good_name")->where("id in ($good_id)")->select();


		foreach($goodData as $key=>$value) {
			foreach($value as $k=>$v) {
			  $orgData[$key][$k] = $v;
			}
		}

		foreach($orgData as $val){
			$shop_goods->where("id = ".$val['goods_id'])->save( array("good_Inventory"=>$val['good_Inventory']-$val['goods_num']) );
		}
		//echo $shop_goods->getLastSql();
		//dump($orgData);
	}

	//全部退货后对应的商品库存增加
	function productUpdates2($id){
		$order_goods = new Model("order_goods");
		$orgData = $order_goods->order("goods_id asc")->field("goods_id,id,order_id,goods_num")->where("order_id = '$id'")->select();

		foreach($orgData as $v){
			$gid[] = $v["goods_id"];
		}
		$good_id = implode(",",$gid);
		$shop_goods = new Model("shop_goods");
		$goodData = $shop_goods->order("id asc")->field("id as g_id,good_Inventory,good_name")->where("id in ($good_id)")->select();


		foreach($goodData as $key=>$value) {
			foreach($value as $k=>$v) {
			  $orgData[$key][$k] = $v;
			}
		}

		foreach($orgData as $val){
			$res.$val['goods_id'] = $shop_goods->where("id = ".$val['goods_id'])->save( array("good_Inventory"=>$val['good_Inventory']+$val['goods_num']) );
		}
		//echo $shop_goods->getLastSql();
		//dump($orgData);
	}

	//订单操作----设置为未付款
	function updatePayStatus(){
		$username = $_SESSION["user_info"]["username"];
		$id = $_REQUEST['id'];
		$order_info = new Model("order_info");
		$arrData = array(
			"description1"=>$_REQUEST['pay_desp'],	//操作备注
			"payment"=>"",     //付款方式
			"pay_status"=>"N",     //未付款
			"order_status"=>"1",   //已确认
			"shipping_status"=>"0",  //未发货
			"refund1"=>$_REQUEST['pay_refund'],		//退款方式
			"explanation1"=>$_REQUEST['pay_explanation'],	 //退款说明
			"delivery_order"=>"not",
		);
		$result = $order_info->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'付款状态已改变！'));
		}else{
			echo json_encode(array('msg'=>'付款状态改变失败！'));
		}
	}

	//订单操作----退货
	function updateReset(){
		$username = $_SESSION["user_info"]["username"];
		$id = $_REQUEST['id'];
		$order_info = new Model("order_info");
		$arrData = array(
			"description4"=>$_REQUEST['reset_desp'],		//操作备注
			"order_status"=>"4",   //退货
			"pay_status"=>"N",  //未付款
			"shipping_status"=>"0",  //未发货
			"refund4"=>$_REQUEST['reset_refund'],		//退款方式
			"explanation4"=>$_REQUEST['reset_explanation'],		 //退款说明
			"canceltime"=>date("Y-m-d H:i:s"),		//退货时间
		);
		//dump($arrData);die;
		$result = $order_info->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			$this->productUpdates2($id);
			echo json_encode(array('success'=>true,'msg'=>'订单状态已改变！'));
		}else{
			echo json_encode(array('msg'=>'订单状态改变失败！'));
		}
	}

	//订单操作----取消
	function updateCancelStatus(){
		$username = $_SESSION["user_info"]["username"];
		$id = $_REQUEST['id'];
		$order_info = new Model("order_info");
		$arrData = array(
			"description3"=>$_REQUEST['cancel_desp'],    //操作备注
			"order_status"=>"2",   //取消
			"pay_status"=>"N",     //未付款
			"shipping_status"=>"0",  //未发货
			"refund3"=>$_REQUEST['cancel_refund'],    //退款方式
			"explanation3"=>$_REQUEST['cancel_explanation'],     //退款说明
			"cancel"=>$_REQUEST['cancel'],    //取消原因
			"delivery_order"=>"not",
		);
		//dump($arrData);die;
		$result = $order_info->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'订单状态已改变！'));
		}else{
			echo json_encode(array('msg'=>'订单状态改变失败！'));
		}
	}

	function customerCombox(){
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];

		$customer = new Model("customer");
		if($username == "admin"){
			$custData = $customer->order("createtime desc")->field("id,name as text,phone1")->select();
		}else{
			$custData = $customer->order("createtime desc")->field("id,name as text,phone1")->where("createuser = '$username' OR dept_id = '$dept_id' OR dealuser = '$username' ")->select();
		}
		echo json_encode($custData);
	}

	function shoppingCombox(){
		$goods = new Model("shop_goods");
		$goodsData = $goods->field("id,good_name,price")->select();
		$i = 0;
		foreach($goodsData as $val){
			$goodsData[$i]['text'] = $val['good_name']."-".$val['price'];
			$i++;
		}
		unset($i);
		echo json_encode($goodsData);
	}

	//添加订单时的商品列表
	function goodsNumData(){
		$goods_id = $_REQUEST['goods_id'];
		$goods = new Model("shop_goods");
		$count = $goods->where("id in ($goods_id)")->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$goodsData = $goods->order("good_order desc")->field("id,good_name,good_num,price")->where("id in ($goods_id)")->select();
		$i = 0;
		foreach($goodsData as $vm){
			$goodsData[$i][num] = "1";
			$goodsData[$i][money] = $vm['price'];
			$amoney[] = $vm['money'];
			$i++;
		}
		unset($i);
		foreach($goodsData as $v){
			$amoney[] = $v['money'];
		}
		$tpl_money = array_sum($amoney);
		$tmp_mon = array(array("money"=>"总计: ".$tpl_money));
		//dump($amoney);
		//dump($goodsData);die;
		$rowsList = count($goodsData) ? $goodsData : false;
		$arrProduct["total"] = $count;
		$arrProduct["rows"] = $rowsList;
		$arrProduct["footer"] = $tmp_mon;
		//dump($arrProduct);die;
		echo json_encode($arrProduct);
	}

	//设置商品信息
	function goodsEditData(){
		$goods_id = $_REQUEST['goods_id'];
		$name = $_REQUEST['name'];

		$where = "1 ";
		$where .= empty($name)?"":" AND ( g.good_name like '%$name%' OR g.good_num like '%$name%' OR c.cat_name like '%$name%'  OR b.brand_name like '%$name%' )";

		$goods = new Model("shop_goods");
		$count = $goods->table("shop_goods g")->order("g.good_order desc")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->where($where)->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$goodsData = $goods->table("shop_goods g")->order("g.good_order desc")->field("g.id,g.good_name,g.good_num,g.good_Inventory,g.price,g.cate_id,g.brand_id,g.simple_description,g.good_description,g.good_order,g.good_enabled,c.cat_name,b.brand_name")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$i = 0;
		foreach($goodsData as $v){
			$goodsData[$i]["good_ids"] = str_replace("-","",$v['good_name'])."-".str_replace("-","",$v['good_num'])."-".$v['price']."-".str_replace("-","",$v['cat_name'])."-".str_replace("-","",$v['brand_name'])."-".str_replace("-","",$v['simple_description'])."-".$v['id'];
			$i++;
		}
		unset($i);
		//echo $goods->getLastSql();die;
		$rowsList = count($goodsData) ? $goodsData : false;
		$arrProduct["total"] = $count;
		$arrProduct["rows"] = $rowsList;
		//dump($goodsData);die;
		echo json_encode($arrProduct);
	}

	//客户资料下拉表格
	function customerComboGrid(){
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];$menuname = "Order Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$name = $_REQUEST['name'];
		$customer_id = $_REQUEST['customer_id'];
		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];

		$where = "1 ";
		$where .= empty($customer_id) ? "" : " AND c.id!='$customer_id'";
		$where .= empty($name)?"":" AND ( name like '%$name%' OR phone1 like '%$name%' OR company like '%$name%' )";
		$where .= " AND recycle = 'N'";

		if($username != "admin" && $priv['members_bomb'] != "Y" && $web_type != "back"){
			$where .= "  AND (createuser = '$username' OR dealuser = '$username')";
		}
		$customer = new Model("customer");
		import('ORG.Util.Page');
		$count = $customer->table("customer c")->join('department d on c.dept_id = d.d_id ')->where($where)->count();
		$para_sys = readS();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$parameter = $_GET['parameter'];
		if($parameter != "edit"){
			$cmlist = $customer->table("customer c")->order("createtime desc")->join('department d on c.dept_id = d.d_id ')->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}else{
			$cmlist = $customer->table("customer c")->order("createtime desc")->join('department d on c.dept_id = d.d_id ')->where($where)->select();
		}
		//echo $customer->getLastSql();die;



		$cmFields = getFieldCache();

		$row = getSelectCache();
		//dump($row);die;
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$vm["phone_mess"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
					if($vm["phone2"]){
						$vm["telephone_mess"] = substr($vm["phone2"],0,3)."***".substr($vm["phone2"],-4);
					}else{
						$vm["telephone_mess"] = "";
					}
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
					$vm['customer_info'] = $vm['id']."--".$vm['phone1']."--".$vm['phone2']."--".$vm['email']."--".$vm['zipcode'];

				}
			}
		}


		if($customer_id){
			$arrFD = $customer->where("id = '$customer_id'")->find();
			array_unshift($cmlist,$arrFD);
		}
		//dump($customer->getLastSql());die;
		$rowsList = count($cmlist) ? $cmlist : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
	}


	function countryCombox(){
		$region = new Model("region");
		$regData = $region->where("parent_id = 0")->select();
		$allCate = array(
			"region_id"=>"0",
			"parent_id"=>"-1",
			"region_name"=>"请选择...",
			"region_type"=>"0",
			"agency_id"=>"0",
		);
		array_unshift($regData,$allCate);
		echo json_encode($regData);
	}

	function provinceCombox(){
		$parent_id = $_REQUEST['parent_id'];
		if($parent_id){
			$region = new Model("region");
			$regData = $region->where("parent_id = '$parent_id'")->select();
			$allCate = array(
				"region_id"=>"0",
				"parent_id"=>"-1",
				"region_name"=>"请选择...",
				"region_type"=>"0",
				"agency_id"=>"0",
			);
			array_unshift($regData,$allCate);
			echo json_encode($regData);
		}else{
			$allCate[0] = array(
				"region_id"=>"0",
				"parent_id"=>"01",
				"region_name"=>"请选择...",
				"region_type"=>"0",
				"agency_id"=>"0",
			);
			echo json_encode($allCate);
		}
	}

	function trackData(){
		$order_id = $_REQUEST["order_id"];
		$order_action = new Model("order_action");

		$where = "order_id = '$order_id'";

		$count = $order_action->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $order_action->order("action_time desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$row_order_status = array('0'=>'未确认','1'=>'已确认','2'=>'取消','3'=>'待审核','4'=>'退货','5'=>'已分单','6'=>'部分分单','7'=>'未通过','8'=>'问题单');
		$row_pay_status = array('N'=>'未付款','M'=>'付款中','Y'=>'已付款');
		$row_shipping_status = array('0'=>'未发货','1'=>'已发货','2'=>'收货确认','3'=>'配货中','4'=>'已发货(部分商品)','5'=>'发货中');

		foreach($arrData as &$val){
			$order_status = $row_order_status[$val['order_status']];
			$val['order_status'] = $order_status;

			$pay_status = $row_pay_status[$val['pay_status']];
			$val['pay_status'] = $pay_status;

			$shipping_status = $row_shipping_status[$val['shipping_status']];
			$val['shipping_status'] = $shipping_status;

			$val["uniqueid"] = trim($val["uniqueid"]);
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";

			if(file_exists($WAVfile) ){
				$val['operations'] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["uniqueid"])."'".")\" > 播放 </a> ";
				//."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .trim($val["uniqueid"]) ."'> 下载 </a>"
				$val["WAVfile"] = $WAVfile;
			}else{
				$val["operations"] = "";
				$val["WAVfile"] = "on";
			}

		}
		//dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertTrack(){
		$order_id = $_REQUEST['order_id'];
		$customer_id = $_REQUEST['customer_id'];
		$username = $_SESSION["user_info"]["username"];
		$uniqueid = $_SESSION[$username."_order"][$customer_id];
		//dump($uniqueid);die;

		$order_info = new Model("order_info");
		$arrD = $order_info->where("id = '$order_id'")->find();
		$order_action = new Model("order_action");
		$arrF = array(
			"order_id" => $order_id,
			"action_time" => date("Y-m-d H:i:s"),
			"action_user" => $username,
			"order_status" => $arrD["order_status"],
			"pay_status" => $arrD["pay_status"],
			"shipping_status" => $arrD["shipping_status"],
			"action_description" => $_REQUEST['action_description'],
			"uniqueid"=>$uniqueid,
		);
		//dump($arrF);die;
		$result = $order_action->data($arrF)->add();
		if ($result){
			//订单确认后计数订单跟踪次数
			if($username == $arrD["createname"] && $arrD["order_status"] == "1" && $uniqueid){
				$visit_num = $arrD["visit_num"]+1;
				$order_info->where("id = '$order_id'")->save(array("visit_num"=>$visit_num));
			}
			unset($_SESSION[$username."_order"][$customer_id]);
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	//分配订单
	function transOrder(){
		$id = $_REQUEST['id'];
		$deptuser = $_REQUEST['deptuser'];
		$order_info = new Model("order_info");
		$arrData = array(
			"createname" => $_REQUEST['recipient'],
		);
		$result = $order_info->data($arrData)->where("id in ($id)")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"分配成功！"));
		} else {
			echo json_encode(array('msg'=>'分配失败！'));
		}
	}

	/*

$arrData = array(
			//"order_num"=>$_REQUEST['order_num'],
			"order_num"=>$order_num,
			"goods_id"=>$_REQUEST['goods_id'],
			"goods_name"=>$_REQUEST['goods_name'],
			"customer_id"=>$_REQUEST['customer_id'],
			"consignee"=>$_REQUEST['consignee'],
			"createname"=>$username,
			"createtime"=>$_REQUEST['createtime'],
			//"paytime"=>$_REQUEST['paytime'],
			//"deliverytime"=>$_REQUEST['deliverytime'],
			//"receipttime"=>$_REQUEST['receipttime'],
			//"canceltime"=>$_REQUEST['canceltime'],
			"goods_amount"=>$goods_amount,
			"cope_money"=>$cope_money,
			"shipping_fee"=>$shipping_fee,
			"order_status"=>'3',             //状态
			"pay_status"=>'N',
			"shipping_status"=>'0',
			"country"=>$_REQUEST['country'],
			"province"=>$_REQUEST['province'],
			"city"=>$_REQUEST['city'],
			"district"=>$_REQUEST['district'],
			"delivery_address"=>$_REQUEST['delivery_address'],
			"zipcode"=>$_REQUEST['zipcode'],
			"telephone"=>$_REQUEST['telephone'],
			"phone"=>$_REQUEST['phone'],
			"email"=>$_REQUEST['email'],
			//"shopping_name"=>$_REQUEST['shopping_name'],
			//"payment"=>$_REQUEST['payment'],
		);

*/


	function exportOrderData(){
		$menuname = "Order Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$calldate_start = $_REQUEST['calldate_start'];
		$calldate_end = $_REQUEST['calldate_end'];
        $order_num = $_REQUEST['order_num'];
        $consignee = $_REQUEST['consignee'];
        $email = $_REQUEST['email'];
        $phone = $_REQUEST['phone'];
        $telephone = $_REQUEST['telephone'];
        //$delivery_address = $_REQUEST['delivery_address'];
        $zipcode = $_REQUEST['zipcode'];
        //$country = $_REQUEST['country'];
        $province = $_REQUEST['province'];
        $city = $_REQUEST['city'];
        $district = $_REQUEST['district'];
        $order_status = $_REQUEST['order_status'];
        $pay_status = $_REQUEST['pay_status'];
        $shipping_status = $_REQUEST['shipping_status'];
        $createname = $_REQUEST['createname'];
        $logistics_account = $_REQUEST['logistics_account'];


		$where = "1 ";
        $where .= empty($calldate_start)?"":" AND createtime >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND createtime <'$calldate_end'";
		$where .= empty($order_num)?"":" AND order_num like '%$order_num%'";
		$where .= empty($consignee)?"":" AND consignee like '%$consignee%'";
		$where .= empty($email)?"":" AND email like '%$email%'";
		$where .= empty($phone)?"":" AND phone like '%$phone%'";
		$where .= empty($telephone)?"":" AND telephone like '%$telephone%'";
		$where .= empty($createname)?"":" AND createname like '%$createname%'";
		$where .= empty($logistics_account)?"":" AND logistics_account like '%$logistics_account%'";
		$where .= empty($zipcode)?"":" AND zipcode like '%$zipcode%'";
		//$where .= empty($country)?"":" AND country = '$country'";
		$where .= empty($province)?"":" AND province = '$province'";
		$where .= empty($city)?"":" AND city = '$city'";
		$where .= empty($district)?"":" AND district = '$district'";
		if($order_status == "0"){
			$where .= " AND order_status = '0'";
		}else{
			$where .= empty($order_status)?"":" AND order_status = '$order_status'";
		}
		$where .= empty($pay_status)?"":" AND pay_status = '$pay_status'";
		if($shipping_status == "0"){
			$where .= " AND shipping_status = '0'";
		}else{
			$where .= empty($shipping_status)?"":" AND shipping_status = '$shipping_status'";
		}


		$username = $_SESSION["user_info"]["username"];
		$r_id = $_SESSION["user_info"]["r_id"];
		$role = new Model("role");
		$rData = $role->where("r_id = '$r_id'")->find();
		$order_info = new Model("order_info");

		if(  $username == "admin" || $priv['members_bomb'] == "Y"  ){
			$arrOrderData = $order_info->order("createtime desc")->where($where)->select();
		}else{
			$arrOrderData = $order_info->order("createtime desc")->where("$where AND createname ='$username'")->select();
		}

		$i = 0;
		$row_order_status = array('0'=>'未确认','1'=>'已确认','2'=>'取消','3'=>'待审核','4'=>'退货','5'=>'已分单','6'=>'部分分单','7'=>"未通过",'8'=>"问题单");
		$row_pay_status = array('N'=>'未付款','M'=>'付款中','Y'=>'已付款');
		$row_shipping_status = array('0'=>'未发货','1'=>'已发货','2'=>'收货确认','3'=>'配货中','4'=>'已发货(部分商品)','5'=>'发货中');
		$row_shopping_name = array("1"=>"顺丰快递","2"=>"圆通速递","3"=>"邮政快递包裹","4"=>"市内快递","5"=>"申通快递","6"=>"邮局平邮","7"=>"城际快递");

		$row_payment = array("1"=>"余额支付","2"=>"银行汇款/转帐","3"=>"货到付款");
		foreach($arrOrderData as &$val){
			$order_status = $row_order_status[$val['order_status']];
			$val['order_status'] = $order_status;

			$pay_status = $row_pay_status[$val['pay_status']];
			$val['pay_status'] = $pay_status;

			$shipping_status = $row_shipping_status[$val['shipping_status']];
			$val['shipping_status'] = $shipping_status;

			$shopping_name = $row_shopping_name[$val['shopping_name']];
			$val['shopping_name'] = $shopping_name;

			$payment = $row_payment[$val['payment']];
			$val['payment'] = $payment;


			$arrOrderData[$i]['status'] = $val['order_status']."  ".$val['pay_status']."   ".$val['shipping_status'];
			$i++;
		}
		unset($i);

		$field = array("consignee","phone","createname","createtime","goods_amount","shipping_fee","cope_money","status","delivery_address","payment","shopping_name","logistics_account");   //,"goods_name"
		$title = array("收货人","收货人号码","创建者","下单时间","总金额","优惠金额","应付金额","状态","地址","支付方式","配送方式","物流号");   //,"商品跟价格"
		$count = count($field);
		$excelTiele = "订单列表";

		//dump($arrOrderData);die;
		$this->exportData($field,$title,$count,$excelTiele,$arrOrderData);
	}

	function exportData($field,$title,$count,$excelTiele,$arrOrderData){


		vendor("PHPExcel176.PHPExcel");
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			//$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setWrapText(true);    //自动换行
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($arrOrderData as &$val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
				//将单元格设为文本类型----------手机号码
				if($j == "1" || $j == "9"){
					$objPHPExcel->getActiveSheet()->setCellValueExplicit($letters2[$j].$start_row, $val[$field[$j]],PHPExcel_Cell_DataType::TYPE_STRING);
				}
				//$objPHPExcel->getActiveSheet()->getStyle($letters2[$j].$start_row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			}
			$start_row++;
		}
		$objPHPExcel->setActiveSheetIndex(0);

		$filename = iconv("utf-8","gb2312",$excelTiele);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}


	function exportGoods(){
		//header("Content-Type:text/html; charset=utf-8");
		$order_id = $_REQUEST["id"];
		$goods_id = $_REQUEST["goods_id"];

		$goods = new Model("shop_goods");
		$arrData = $goods->table("shop_goods g")->order("g.good_order desc")->field("g.id,g.good_name,g.good_num,g.good_Inventory,g.price,g.cate_id,g.brand_id,g.simple_description,g.good_description,g.good_order,g.good_enabled,g.original_img,g.thumb_path,g.good_img,g.img_path,c.cat_name,b.brand_name,og.goods_num,og.goods_money,o.consignee,o.phone,o.createname,o.createtime,o.goods_amount,o.shipping_fee,o.cope_money,o.order_status,o.delivery_address,o.payment,o.shopping_name,o.logistics_account")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->join("order_goods og on g.id = og.goods_id")->join("order_info o on o.id = og.order_id")->where("g.id in ($goods_id) AND og.order_id = $order_id")->select();
		//echo $goods->getLastSql();die;

		foreach($arrData as &$val){
			$val["good_description"] = strip_tags($val["good_description"]);
		}

		$field = array("consignee","phone","createname","createtime","goods_amount","shipping_fee","cope_money","delivery_address","good_name","good_num","price","cat_name","brand_name","goods_num","goods_money","good_description");
		$title = array("收货人","收货人号码","创建者","下单时间","总金额","优惠金额","应付金额","地址","商品名称","商品货号","商品价格","商品分类","商品品牌","商品数量","商品总价格","商品描述");
		$count = count($field);
		$excelTiele = "订单商品";

		//dump($arrOrderData);die;
		$this->exportData($field,$title,$count,$excelTiele,$arrData);

	}

	function groupBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row["order_goods_num"];
		}
		return $ret;
	}

	function testo(){
		header("Content-Type:text/html; charset=utf-8");
		$shop_goods = new Model("shop_goods");
		$arr = $shop_goods->field("id,good_name,good_num")->select();
		foreach($arr as $val){
			$arrIDName[$val["id"]] = $val["good_name"];
			$arrIDNum[$val["id"]] = $val["good_num"];
		}
		$order_goods = new Model("order_goods");
		$arrF = $order_goods->select();
		foreach($arrF as &$val){
			$val["good_name"] = $arrIDName[$val["goods_id"]];
			$val["good_num2"] = $arrIDNum[$val["goods_id"]];

			//$val["order_goods_num"] = $val["goods_id"]."-".$val["good_name"]."-".$val["goods_num"]."-".$val["good_num2"]; //商品id-商品名称-商品数量-商品货号
			$val["order_goods_num"] = $val["good_name"]."[".$val["good_num2"]."]*".$val["goods_num"]; //商品名称[商品货号]*商品数量
		}
		$arrT = $this->groupBy($arrF,"order_id");
		foreach($arrT as $key=>&$val){
			foreach($arrT[$key] as $k=>$v){
				$arrD[$key] = array(
					"id" => $key,
					"name" => implode(",",$val),
				);
			}
		}

		foreach($arrD as $val){
			$arrData[] = $val;
		}

		$order_info = new Model("order_info");

		foreach($arrData as $val){
			$order_info->where("id = '".$val["id"]."'")->save(array("order_goods_num"=>$val["name"]));
		}
	}

	//打印货单
	function printInvoiceList(){
		//checkLogin();  //不能打开这个 打印的会不行
		$para_sys = readS();
		header("Content-Type:text/html; charset=utf-8");

		$calldate_start = $_REQUEST['calldate_start'];
		$calldate_end = $_REQUEST['calldate_end'];
        $order_num = $_REQUEST['order_num'];
        $consignee = $_REQUEST['consignee'];
        $email = $_REQUEST['email'];
        $phone = $_REQUEST['phone'];
        $telephone = $_REQUEST['telephone'];
        //$delivery_address = $_REQUEST['delivery_address'];
        $zipcode = $_REQUEST['zipcode'];
        //$country = $_REQUEST['country'];
        $province = $_REQUEST['province'];
        $city = $_REQUEST['city'];
        $district = $_REQUEST['district'];
        $street = $_REQUEST['street'];
        $order_status = $_REQUEST['order_status'];
        $pay_status = $_REQUEST['pay_status'];
        $shipping_status = $_REQUEST['shipping_status'];
        $createname = $_REQUEST['createname'];
        $logistics_account = $_REQUEST['logistics_account'];
        $logistics_state = $_REQUEST['logistics_state'];
        $shopping_name = $_REQUEST['shopping_name'];
        $start_receipttime = $_REQUEST['start_receipttime'];
        $end_receipttime = $_REQUEST['end_receipttime'];
        $id = $_REQUEST['id'];
        $create_user = $_REQUEST['create_user'];
        $search_type = $_REQUEST['search_type'];


		$where = "1 ";
        $where .= empty($calldate_start)?"":" AND createtime >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND createtime <'$calldate_end'";
		$where .= empty($order_num)?"":" AND order_num like '%$order_num%'";
		$where .= empty($consignee)?"":" AND consignee like '%$consignee%'";
		$where .= empty($email)?"":" AND email like '%$email%'";
		$where .= empty($phone)?"":" AND phone like '%$phone%'";
		$where .= empty($telephone)?"":" AND telephone like '%$telephone%'";
		$where .= empty($createname)?"":" AND createname like '%$createname%'";
		$where .= empty($logistics_account)?"":" AND logistics_account like '%$logistics_account%'";
		$where .= empty($zipcode)?"":" AND zipcode like '%$zipcode%'";
		//$where .= empty($country)?"":" AND country = '$country'";
		$where .= empty($province)?"":" AND province = '$province'";
		$where .= empty($city)?"":" AND city = '$city'";
		$where .= empty($district)?"":" AND district = '$district'";
		$where .= empty($street)?"":" AND street = '$street'";
		if($order_status == "0"){
			$where .= " AND order_status = '0'";
		}else{
			$where .= empty($order_status)?"":" AND order_status = '$order_status'";
		}
		$where .= empty($pay_status)?"":" AND pay_status = '$pay_status'";
		if($shipping_status == "0"){
			$where .= " AND shipping_status = '0'";
		}else{
			$where .= empty($shipping_status)?"":" AND shipping_status = '$shipping_status'";
		}
		if($logistics_state == "0"){
			$where .= " AND logistics_state = '$logistics_state'";
		}elseif($logistics_state == "all"){
			$where .= " AND logistics_state != '' AND logistics_state is not null";
		}else{
			$where .= empty($logistics_state)?"":" AND logistics_state = '$logistics_state'";
		}
		$where .= empty($shopping_name)?"":" AND shopping_name = '$shopping_name'";
        $where .= empty($start_receipttime)?"":" AND logistics_time >= '$start_receipttime'";
        $where .= empty($end_receipttime)?"":" AND logistics_time <= '$end_receipttime'";
		$where .= empty($id)?"":" AND id in ($id)";
		$where .= empty($create_user)?"":" AND createname = '$create_user'";

		$order_info = M("order_info");
		$arrData = $order_info->order("createtime asc")->where($where)->select();

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$row_logistics_state = array("0"=>"在途","1"=>"揽件","2"=>"疑难","3"=>"签收","4"=>"退签","5"=>"派件","6"=>"退回","7"=>"转投");
		$i = 1;
		foreach($arrData as &$val){
			$val["logistics_state"] = $row_logistics_state[$val["logistics_state"]];
			$val['cn_name'] = $cnName[$val["createname"]];
			if($val["cn_name"]){
				$val["createname"] = $val["createname"]."/".$val["cn_name"];
			}
			$val["goods_name"] = json_decode($val["goods_name"],true);
			$val["shop_num"] = count($val["goods_name"]);

			$val["mod"] = $i%2;
			$i++;
		}

		//dump($arrData);die;
		$max = count($arrData);
		$this->assign("arrData",$arrData);
		$this->assign("max",$max);
		$this->assign("copyright",$para_sys["copyright"]);

		$this->display();
	}

	function getGoodsName(){
		header("Content-Type:text/html; charset=utf-8");
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$goods = new Model("shop_goods");
		$arrF = $goods->table("shop_goods g")->field("g.id,g.good_name,g.good_num,g.price,c.cat_name,b.brand_name")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->select();
		foreach($arrF as $val){
			$arrG[$val["id"]] = $val;
		}


		$order_goods = M("order_goods");
		$arrData = $order_goods->select();
		foreach($arrData as &$val){
			$val["good_name"] = $arrG[$val["goods_id"]]["good_name"];
			$val["good_num"] = $arrG[$val["goods_id"]]["good_num"];
			$val["cat_name"] = $arrG[$val["goods_id"]]["cat_name"];
			$val["brand_name"] = $arrG[$val["goods_id"]]["brand_name"];
		}
		$arrT = $this->groupByww($arrData,"order_id");
		//sort($arrT);

		$order_info = M("order_info");
		foreach($arrT as $key=>&$val){
			$arr[] = $order_info->where("id='".$key."'")->save(array("goods_name"=>json_encode($arrT[$key])));
		}
		echo $order_info->getLastSql();
		dump($arr);
		dump($arrT);
		die;
	}

	function groupByww($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row;
		}

		foreach($ret as $key=>$v){
			foreach($ret[$key] as $val){
				$arrF[$key][] = array(
					"id"=>$val["order_id"],			//商品id
					"goods_num"=>$val["goods_num"],		//商品数量
					"price"=>$val["goods_price"],			//商品单价
					"goods_money"=>$val["goods_money"],	//商品总价格
					"good_name"=>$val["good_name"],		//商品名称
					"good_num"=>$val["good_num"],		//商品货号
					"cat_name"=>$val["cat_name"],		//商品分类
					"brand_name"=>$val["brand_name"],	//商品品牌
				);
			}
		}
		//dump($arrF);die;
		return $arrF;
	}
}

?>
