<?php

class VpnAction extends Action {
    function index(){
		checkLogin();
		$para_sys = readS();
		//dump($para_sys);die;
		$vpn_crt = isset($para_sys['vpn_crt'])?$para_sys['vpn_crt']:"请先在【系统参数】菜单中设置供应商分配的<font color='red'>VPN证书名</font>参数";

		$vpn_ipaddr = "";
		exec("/sbin/ifconfig tun0 |grep 'inet addr'",$out,$status);
		//dump($out);die;
		if($status == 0){
			$arrTemp = explode(":",$out[0]);
			//dump($arrTemp);die;
			$arrtemp = explode('  ',$arrTemp[1]);
			$vpn_ipaddr = $arrtemp[0];
		}else{

		}
		$this->assign("vpn_crt",$vpn_crt);
		$this->assign("vpn_ipaddr",$vpn_ipaddr);
        $this->display();
    }


	//下载证书 并 拨号连接vpn服务器
	function connect(){
		$para_sys = readS();
		//dump($para_sys);die;
		if(! isset($para_sys['vpn_crt'])){
			//$this->error("请先在【系统参数】菜单中设置供应商分配的VPN证书名参数");
			echo json_encode(array('msg'=>'请先在【系统参数】菜单中设置供应商分配的VPN证书名参数'));
			die;
		}

		$crt_name = $para_sys['vpn_crt'];
        $vpn_home = "/etc/openvpn";

		//如果IP地址已经存在，禁止连接
		$vpn_ipaddr = "";
		exec("/sbin/ifconfig tun0 |grep 'inet addr'",$out,$status);
		if($status == 0){
			$arrTemp = explode(":",$out[0]);
			//dump($arrTemp);die;
			$arrtemp = explode('  ',$arrTemp[1]);
			$vpn_ipaddr = $arrtemp[0];
		}else{

		}
		if(! empty($vpn_ipaddr) ){
			//$this->error("VPN已经连接，请先断开后再连接!");
			echo json_encode(array('msg'=>'VPN已经连接，请先断开后再连接!'));
			die;
		}

		$crt_file = $vpn_home ."/" .$crt_name .".crt";
		if(! file_exists($crt_file) ){ //如果证书不存在，需要从服务器上下载证书
			$url = "http://al.robot365.com/index.php?m=ComputerData&a=downloadCrt&crt_name=" .$crt_name;
			//echo $url;die;
			system("wget -q '$url'  -O -|tar -zx -C /etc/openvpn",$status);
			if( $status == 0 ){
				system("cp /etc/openvpn/$crt_name/* /etc/openvpn/");
			}else{
				//$this->error("从服务器下载证书失败!");
				echo json_encode(array('msg'=>'从服务器下载证书失败!'));
				die;
			}

		}

		//开始vpn拨号,这个地方一定要很注意，如果用sudo，必须cd /etc/openvpn，否则asterisk用户拨号会失败。
		set_time_limit(60);
		system("cd /etc/openvpn;/usr/bin/sudo /usr/sbin/openvpn /etc/openvpn/client.conf 1>/dev/null 2>&1 &",$status);

		$i = 1;
		do{
			sleep(2);
			exec("/sbin/ifconfig tun0",$arr,$status);
			$i ++;
		}while(0!=$status && $i< 10);

		//header("Location: index.php?m=Vpn&a=index");
		echo json_encode(array('success'=>true,'msg'=>'连接成功!'));

	}

	//断开vpn服务器的连接
	function disconnect(){
		system("/usr/bin/sudo /usr/bin/pkill openvpn &",$status);
		sleep(4);
		echo json_encode(array('success'=>true,'msg'=>'操作成功!'));
		//header("Location: index.php?m=Vpn&a=index");
	}

}
