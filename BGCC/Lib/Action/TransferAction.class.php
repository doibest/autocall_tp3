<?php

class TransferAction extends Action{
	function transferList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Transfer";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function transferData(){
		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$arrCF = $bmi->getCFAll("CF");
		$arrCFU = $bmi->getCFAll("CFU");
		$arrCFB = $bmi->getCFAll("CFB");

		$arrCFData = array();
		foreach($arrCF as $key=>$val){
			$arrCFData[] = array(
				"exten"=>$key,
				"phone"=>$val,
				"transfer_type"=>"CF",
				"transfer_type2"=>"<span style='color:blue;'>无条件转移</span>",
			);
		}

		$arrCFUData = array();
		foreach($arrCFU as $key=>$val){
			$arrCFUData[] = array(
				"exten"=>$key,
				"phone"=>$val,
				"transfer_type"=>"CFU",
				"transfer_type2"=>"超时转移",
			);
		}

		$arrCFBData = array();
		foreach($arrCFB as $key=>$val){
			$arrCFBData[] = array(
				"exten"=>$key,
				"phone"=>$val,
				"transfer_type"=>"CFB",
				"transfer_type2"=>"<span style='color:red;'>遇忙转移<span>",
			);
		}

		$arrF = array_merge($arrCFData,$arrCFUData,$arrCFBData);
		$arrData = array_sort($arrF,"exten","asc","no");


		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = count($arrData);
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertTransfer(){
		$exten = $_REQUEST["exten"];
		$phone = $_REQUEST["phone"];
		$transfer_type = $_REQUEST["transfer_type"];

		import("ORG.Pbx.bmi");
		$bmi = new bmi();
		$bmi->loadAGI();

		$bmi->setCF($exten,$phone,$transfer_type);

		$msg = L("Set successfully");
		echo json_encode(array('success'=>true,'msg'=>$msg));
	}


	function listTransfer(){
		checkLogin();
		$extension = $_SESSION['user_info']['extension'];
		if( $extension ){
			import("ORG.Pbx.bmi");
			$bmi = new bmi();
			$cf = $bmi->getCF($extension);
			$cfb = $bmi->getCF($extension,"CFB");
			$cfu = $bmi->getCF($extension,"CFU");

			$this->assign("extension",$extension);
			$this->assign("cf",$cf);
			$this->assign("cfb",$cfb);
			$this->assign("cfu",$cfu);
			$this->assign("extension",$extension);

			$users = new Model("users");
			$arrU = $users->field("username,extension")->where("extension is not null AND extension != ''")->select();
			$this->assign("exten",$arrU);
			//dump($arrU);
			//分配增删改的权限
			$menuname = "Transfer";
			$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
			$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

			$user_name2 = $_SESSION['user_info']['username'];
			$arrAdmin = getAdministratorNum();
			if( in_array($user_name2,$arrAdmin) ){
				$this->assign("username","admin");
			}else{
				$this->assign("username",$user_name2);
			}

			$this->assign("edit",$priv['edit']);

			$this->display();

		}else{
			echo "Extension is not exist!";
		}

	}

	function saveTransfer(){
		$exten = $_SESSION['user_info']['extension'];
		$extension = empty($_REQUEST["exten"]) ? $exten : $_REQUEST["exten"];
		$cf = $_POST['cf'];
		$cfb = $_POST['cfb'];
		$cfu = $_POST['cfu'];
		if( $extension ){
			import("ORG.Pbx.bmi");
			$bmi = new bmi();
			$bmi->loadAGI();

			$bmi->setCF($extension,$cf,"CF");

			$bmi->setCF($extension,$cfb,"CFB");

			$bmi->setCF($extension,$cfu,"CFU");

			$msg = L("Set successfully");
			goBack($msg,"index.php?m=Transfer&a=listTransfer","");
			//$this->success("设置成功!","index.php?m=Transfer&a=listTransfer");
		}else{
			echo "Extension is not exist!";
		}

	}




}

?>
