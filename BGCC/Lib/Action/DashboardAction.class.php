<?php

class DashboardAction extends Action{
	function panelDisplay(){
		checkLogin();
		$para_sys = readS();
		$no_visit = $para_sys["no_visit"];
		$this->assign("no_visit",$no_visit);
		$this->assign("para_sys",$para_sys);

		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$username = $_SESSION["user_info"]["username"];
		$extension = $_SESSION["user_info"]["extension"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$r_id = $_SESSION["user_info"]["r_id"];
		//dump($_SESSION["user_info"]);die;

		$date = date("Y-m-d H:i:s");
		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$now = date("Y-m-d H:i:s");

		//条件
		$where_wait = "status ='N' ";
		$where_visit = "visit_time<='$end_time' AND visit_time>='$start_time' AND visit_status = 'N' AND visit_type='in' ";
		$where_visit2 = "visit_time<='$end_time' AND visit_time>='$start_time' AND visit_status = 'N' AND visit_type='out' ";
		$where_gg = "endtime >='$start_time'  AND starttime <= '$end_time' AND an_enabeld = 'Y' AND starttime < '$now'";
		$where_noVisit = "1 ";
		$where_noVisit .= " AND (TIMESTAMPDIFF(DAY,'$date',recently_visittime) >= '$no_visit' OR TIMESTAMPDIFF(DAY,recently_visittime,'$date') >= '$no_visit' ) ";
		$where_purchase = "TIMESTAMPDIFF(DAY,'$date',o.purchase_time) <= '$no_visit' AND o.purchase_time > '$date'";
		$where_order_status = "view_enable = 'N' AND order_status in (1,2,4,7,8)";
		$where_hotline = "1 ";

		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$d_id = $_SESSION['user_info']['d_id'];
				$arrDep = $this->getDepTreeArray();
				$deptst = $this->getMeAndSubDeptName12($arrDep,$d_id);
				$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
				$userArr = readU();
				$deptUser2 = "";
				foreach($deptSet as $val){
					$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
				}
				$deptUser = rtrim($deptUser2,",'',")."'";

				$where_wait .= " AND username in ($deptUser)" ;
				$where_visit .= " AND visit_name in ($deptUser) ";
				$where_visit2 .= " AND visit_name in ($deptUser) ";
				$where_gg .= " AND find_in_set('$d_id',department_id) ";
				$where_noVisit .= " AND createuser in ($deptUser) ";
				$where_purchase .= " AND o.createname in ($deptUser) ";
				$where_order_status .= " AND createname in ($deptUser) ";
			}else{
				$where_wait .= " AND username = '$username'";
				$where_visit .= " AND visit_name = '$username'";
				$where_visit2 .= " AND visit_name = '$username'";
				$where_gg .= " AND find_in_set('$d_id',department_id) ";
				$where_noVisit .= " AND createuser = '$username' ";
				$where_purchase .= " AND o.createname = '$username' ";
				$where_order_status .= " AND createname = '$username' ";
				$where_hotline .= " AND create_user = '$username' AND visit_status = 'N'";
			}

		}
		//dump($deptSet);die;

		//待办事项
		$wait = new Model("waitmatter");
		$waitcount = $wait->where($where_wait)->count();
		$arrWait = $wait->field("id,title,remindertime")->where($where_wait)->select();
		//echo $wait->getLastSql();die;
		$this->assign("waitcount",$waitcount);
		$this->assign("arrWait",$arrWait);
		//dump($arrWait);die;

		//今日回访任务
		$visit_customer = new Model("visit_customer");
		$visit_count = $visit_customer->where($where_visit)->count();
		$this->assign("visit_count",$visit_count);

		//今日外呼回访任务
		$visit_outbound_count = $visit_customer->where($where_visit2)->count();
		//echo $visit_customer->getLastSql();die;
		$this->assign("visit_outbound_count",$visit_outbound_count);

		//公告
		$gg = new Model("announcement");
		$ggcount = $gg->where($where_gg)->count();
		$arrGG = $gg->where($where_gg)->select();
		//echo $gg->getLastSql();die;
		$this->assign("ggcount",$ggcount);
		$this->assign("arrGG",$arrGG);


		//未接来电
		$cdr = new Model('asteriskcdrdb.Cdr');
		if($username != "admin"){
			$cdrcount = $cdr->where("dst='$extension' AND disposition !='ANSWERED'")->count();
		}else{
			$cdrcount = $cdr->where("disposition !='ANSWERED'")->count();
		}
		$this->assign("cdrcount",$cdrcount);

		//最近未联系客户
		$visit_record = new Model("customer");
		$novisitCount = $visit_record->where($where_noVisit)->count();
		//echo $visit_record->getLastSql();die;
		$this->assign("novisitCount",$novisitCount);

		//最近可能需要复购的资料
		$order_info = new Model("order_info");
		$purchaseCount = $order_info->table("order_info o")->field("o.id,o.purchase_time,o.order_num,o.customer_id,c.phone1,c.name")->join("customer c on (o.customer_id = c.id)")->where($where_purchase)->count();
		//echo $order_info->getLastSql();
		$this->assign("purchaseCount",$purchaseCount);


		//订单状态更变提醒
		$count_order_status = $order_info->where($where_order_status)->count();
		$this->assign("count_order_status",$count_order_status);

		//热线资源
		$hotline_record = M("hotline_record");
		$hotline_count = $hotline_record->where($where_hotline)->count();
		$this->assign("hotline_count",$hotline_count);



		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("inbound",$arrAL) ){
			$wj_in = "Y";
		}else{
			$wj_in = "N";
		}
		if( in_array("outbound",$arrAL) ){
			$wj_out = "Y";
		}else{
			$wj_out = "N";
		}
		if( in_array("IPPBX",$arrAL) ){
			$wj_IPPBX = "Y";
		}else{
			$wj_IPPBXt = "N";
		}
		if($wj_in == "Y" && $wj_out == "Y"){
			$sys_type = "all";
		}elseif($wj_in == "Y"){
			$sys_type = "inbound";
		}elseif($wj_out == "Y"){
			$sys_type = "outbound";
		}elseif($wj_IPPBX == "Y"){
			$sys_type = "IPPBX";
		}
		$this->assign("sys_type",$sys_type);


		import('ORG.Util.DashboardCheck');
		$sys = new Dashboard();
		$serverSituation = $sys->obtener_info_de_sistema();
		//dump($serverSituation);
		//硬盘
		$num_bloques_total = $serverSituation['particiones'][0]['num_bloques_total'];
		$num_bloques_usados = $serverSituation['particiones'][0]['num_bloques_usados'];
		$num_bloques_disponibles = $serverSituation['particiones'][0]['num_bloques_disponibles'];
		$uso_porcentaje = $serverSituation['particiones'][0]['uso_porcentaje'];
		$hard_used = intval(substr($uso_porcentaje,0,2));  //硬盘已使用的概率
		$hard_lv = intval(100-$hard_used); //硬盘可用率
		$this->assign("hard_used",$hard_used); //硬盘 使用率
		$this->assign("hard_lv",$hard_lv); //硬盘可用率
		$CpuUsage = substr($serverSituation['CpuUsage']*100,0,4);  //CPU使用率
		$this->assign("CpuUsage",$CpuUsage);  //cpu使用率
		//RAM 内存
		$MemTotal = $serverSituation['MemTotal'];
		//$MemFree = $serverSituation['MemFree'];
		$MemFree = $serverSituation['MemFree']+$serverSituation['Cached']; //wjj--edit
		$Mem_used = ceil(($MemTotal-$MemFree)/1024)." M";  //使用的内存
		$Mem_lv = substr( (($MemTotal-$MemFree)/$MemTotal)*100,0,4);
		$this->assign("Mem_lv",$Mem_lv); //RAM 内存 使用率

		$SwapTotal = $serverSituation['SwapTotal'];
		$SwapFree = $serverSituation['SwapFree'];  //可用的虚拟空间
		$Swap_uselv = ceil(($SwapTotal-$SwapFree)/1024)." M"; //已用的虚拟内存（单位为M）
		$Swap_lv = ($SwapFree/$SwapTotal)*100; //swap可用率
		$Swap_used = substr(100-$Swap_lv,0,4);
		$this->assign("Swap_used",$Swap_used); //swap 使用率


		//销售额
		$date1 = date("Y-m-d");
		//$last_year = date('Y-m-d',strtotime("$date1 -1 year"))." 00:00:00";
		$last_year = date('Y-m-d',strtotime("$date1 -9 month"))." 00:00:00";
		$where_order = "order_status = '1' AND createtime >= '$last_year'";
		$arrOD = $order_info->field("SUM(cope_money) as cope_money,DATE_FORMAT(createtime,'%Y-%m') AS create_month")->where($where_order)->group("DATE_FORMAT(createtime,'%Y-%m')")->select();
		$month_money = "";
		foreach($arrOD as $val){
			$str = "[";
			$str .= "'" .$val["create_month"]. "',";
			$str .= "'" .$val["cope_money"]. "'";
			$str .= "]";
			$month_money .= empty($month_money)?"$str":",$str";
		}
		$this->assign("month_money",$month_money);

		$where_cm = "createtime >= '$last_year' ";
		$arrCDM = $visit_record->field("count(*) as total_num,DATE_FORMAT(createtime,'%Y-%m') AS create_month")->where($where_cm)->group("DATE_FORMAT(createtime,'%Y-%m')")->select();
		//echo $visit_record->getLastSql();die;
		$customer_num = "";
		foreach($arrCDM as $val){
			$str = "[";
			$str .= "'" .$val["create_month"]. "',";
			$str .= "'" .$val["total_num"]. "'";
			$str .= "]";
			$customer_num .= empty($customer_num)?"$str":",$str";
		}
		$this->assign("customer_num",$customer_num);


		//代办工单
		$work_order_list = M("work_order_list");
		if($username != "admin"){
			$work_order_count = $work_order_list->where("find_in_set('$username',operators) AND work_order_status != '3'")->count();
		}else{
			$work_order_count = $work_order_list->where("work_order_status != '3'")->count();
		}
		$this->assign("work_order_count",$work_order_count);


		//dump($month_money);die;

		$this->display();
	}
	function panelDisplayOLD(){
		checkLogin();
		$para_sys = readS();
		$no_visit = $para_sys["no_visit"];
		$this->assign("no_visit",$no_visit);

		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$username = $_SESSION["user_info"]["username"];
		$extension = $_SESSION["user_info"]["extension"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$r_id = $_SESSION["user_info"]["r_id"];
		//dump($_SESSION["user_info"]);die;

		$date = date("Y-m-d H:i:s");
		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$now = date("Y-m-d H:i:s");

		//条件
		$where_wait = "status ='N' ";
		$where_visit = "visit_time<='$end_time' AND visit_time>='$start_time' AND visit_status = 'N' AND visit_type='in' ";
		$where_visit2 = "visit_time<='$end_time' AND visit_time>='$start_time' AND visit_status = 'N' AND visit_type='out' ";
		$where_gg = "endtime >='$start_time'  AND starttime <= '$end_time' AND an_enabeld = 'Y' AND starttime < '$now'";
		$where_noVisit = "1 ";
		$where_noVisit .= " AND (TIMESTAMPDIFF(DAY,'$date',recently_visittime) >= '$no_visit' OR TIMESTAMPDIFF(DAY,recently_visittime,'$date') >= '$no_visit' ) ";
		$where_purchase = "TIMESTAMPDIFF(DAY,'$date',o.purchase_time) <= '$no_visit' AND o.purchase_time > '$date'";
		$where_order_status = "view_enable = 'N' AND order_status in (1,2,4,7,8)";
		$where_hotline = "1 ";

		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$d_id = $_SESSION['user_info']['d_id'];
				$arrDep = $this->getDepTreeArray();
				$deptst = $this->getMeAndSubDeptName12($arrDep,$d_id);
				$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
				$userArr = readU();
				$deptUser2 = "";
				foreach($deptSet as $val){
					$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
				}
				$deptUser = rtrim($deptUser2,",'',")."'";

				$where_wait .= " AND username in ($deptUser)" ;
				$where_visit .= " AND visit_name in ($deptUser) ";
				$where_visit2 .= " AND visit_name in ($deptUser) ";
				$where_gg .= " AND find_in_set('$d_id',department_id) ";
				$where_noVisit .= " AND createuser in ($deptUser) ";
				$where_purchase .= " AND o.createname in ($deptUser) ";
				$where_order_status .= " AND createname in ($deptUser) ";
			}else{
				$where_wait .= " AND username = '$username'";
				$where_visit .= " AND visit_name = '$username'";
				$where_visit2 .= " AND visit_name = '$username'";
				$where_gg .= " AND find_in_set('$d_id',department_id) ";
				$where_noVisit .= " AND createuser = '$username' ";
				$where_purchase .= " AND o.createname = '$username' ";
				$where_order_status .= " AND createname = '$username' ";
				$where_hotline .= " AND create_user = '$username' AND visit_status = 'N'";
			}

		}
		//dump($deptSet);die;

		//待办事项
		$wait = new Model("waitmatter");
		$waitcount = $wait->where($where_wait)->count();
		//echo $wait->getLastSql();die;
		$this->assign("waitcount",$waitcount);


		//今日回访任务
		$visit_customer = new Model("visit_customer");
		$visit_count = $visit_customer->where($where_visit)->count();
		$this->assign("visit_count",$visit_count);

		//今日外呼回访任务
		$visit_outbound_count = $visit_customer->where($where_visit2)->count();
		//echo $visit_customer->getLastSql();die;
		$this->assign("visit_outbound_count",$visit_outbound_count);

		//公告
		$gg = new Model("announcement");
		$ggcount = $gg->where($where_gg)->count();
		//echo $gg->getLastSql();die;
		$this->assign("ggcount",$ggcount);

		//未接来电
		$cdr = new Model('asteriskcdrdb.Cdr');
		if($username != "admin"){
			$cdrcount = $cdr->where("dst='$extension' AND disposition !='ANSWERED'")->count();
		}else{
			$cdrcount = $cdr->where("disposition !='ANSWERED'")->count();
		}
		$this->assign("cdrcount",$cdrcount);

		//最近未联系客户
		$visit_record = new Model("customer");
		$novisitCount = $visit_record->where($where_noVisit)->count();
		//echo $visit_record->getLastSql();die;
		$this->assign("novisitCount",$novisitCount);

		//最近可能需要复购的资料
		$order_info = new Model("order_info");
		$purchaseCount = $order_info->table("order_info o")->field("o.id,o.purchase_time,o.order_num,o.customer_id,c.phone1,c.name")->join("customer c on (o.customer_id = c.id)")->where($where_purchase)->count();
		//echo $order_info->getLastSql();
		$this->assign("purchaseCount",$purchaseCount);


		//订单状态更变提醒
		$count_order_status = $order_info->where($where_order_status)->count();
		$this->assign("count_order_status",$count_order_status);

		//热线资源
		$hotline_record = M("hotline_record");
		$hotline_count = $hotline_record->where($where_hotline)->count();
		$this->assign("hotline_count",$hotline_count);



		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("inbound",$arrAL) ){
			$wj_in = "Y";
		}else{
			$wj_in = "N";
		}
		if( in_array("outbound",$arrAL) ){
			$wj_out = "Y";
		}else{
			$wj_out = "N";
		}
		if( in_array("IPPBX",$arrAL) ){
			$wj_IPPBX = "Y";
		}else{
			$wj_IPPBXt = "N";
		}
		if($wj_in == "Y" && $wj_out == "Y"){
			$sys_type = "all";
		}elseif($wj_in == "Y"){
			$sys_type = "inbound";
		}elseif($wj_out == "Y"){
			$sys_type = "outbound";
		}elseif($wj_IPPBX == "Y"){
			$sys_type = "IPPBX";
		}
		$this->assign("sys_type",$sys_type);


		import('ORG.Util.DashboardCheck');
		$sys = new Dashboard();
		$serverSituation = $sys->obtener_info_de_sistema();
		//dump($serverSituation);
		//硬盘
		$num_bloques_total = $serverSituation['particiones'][0]['num_bloques_total'];
		$num_bloques_usados = $serverSituation['particiones'][0]['num_bloques_usados'];
		$num_bloques_disponibles = $serverSituation['particiones'][0]['num_bloques_disponibles'];
		$uso_porcentaje = $serverSituation['particiones'][0]['uso_porcentaje'];
		$hard_used = intval(substr($uso_porcentaje,0,2));  //硬盘已使用的概率
		$hard_lv = intval(100-$hard_used); //硬盘可用率
		$this->assign("hard_used",$hard_used); //硬盘 使用率
		$this->assign("hard_lv",$hard_lv); //硬盘可用率



		//dump($web_type);die;

		$this->display();
	}

	function panelPushData(){
		$table_type = $_REQUEST["table_type"];
		$para_sys = readS();
		$no_visit = $para_sys["no_visit"];
		$this->assign("no_visit",$no_visit);

		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$username = $_SESSION["user_info"]["username"];
		$extension = $_SESSION["user_info"]["extension"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$r_id = $_SESSION["user_info"]["r_id"];
		//dump($_SESSION["user_info"]);die;

		$date = date("Y-m-d H:i:s");
		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$now = date("Y-m-d H:i:s");

		//条件
		$where_wait = "status ='N' ";
		$where_visit = "visit_time<='$end_time' AND visit_time>='$start_time' AND visit_status = 'N' AND visit_type='in' ";
		$where_visit2 = "visit_time<='$end_time' AND visit_time>='$start_time' AND visit_status = 'N' AND visit_type='out' ";
		$where_gg = "endtime >='$start_time'  AND starttime <= '$end_time' AND an_enabeld = 'Y' AND starttime < '$now'";
		$where_noVisit = "1 ";
		$where_noVisit .= " AND (TIMESTAMPDIFF(DAY,'$date',recently_visittime) >= '$no_visit' OR TIMESTAMPDIFF(DAY,recently_visittime,'$date') >= '$no_visit' ) ";
		$where_purchase = "TIMESTAMPDIFF(DAY,'$date',o.purchase_time) <= '$no_visit' AND o.purchase_time > '$date'";
		$where_order_status = "view_enable = 'N' AND order_status in (1,2,4,7,8)";
		$where_hotline = "1 ";

		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$d_id = $_SESSION['user_info']['d_id'];
				$arrDep = $this->getDepTreeArray();
				$deptst = $this->getMeAndSubDeptName12($arrDep,$d_id);
				$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
				$userArr = readU();
				$deptUser2 = "";
				foreach($deptSet as $val){
					$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
				}
				$deptUser = rtrim($deptUser2,",'',")."'";

				$where_wait .= " AND username in ($deptUser)" ;
				$where_visit .= " AND visit_name in ($deptUser) ";
				$where_visit2 .= " AND visit_name in ($deptUser) ";
				$where_gg .= " AND find_in_set('$d_id',department_id) ";
				$where_noVisit .= " AND createuser in ($deptUser) ";
				$where_purchase .= " AND o.createname in ($deptUser) ";
				$where_order_status .= " AND createname in ($deptUser) ";
			}else{
				$where_wait .= " AND username = '$username'";
				$where_visit .= " AND visit_name = '$username'";
				$where_visit2 .= " AND visit_name = '$username'";
				$where_gg .= " AND find_in_set('$d_id',department_id) ";
				$where_noVisit .= " AND createuser = '$username' ";
				$where_purchase .= " AND o.createname = '$username' ";
				$where_order_status .= " AND createname = '$username' ";
				$where_hotline .= " AND create_user = '$username' AND visit_status = 'N'";
			}

		}
		//dump($deptSet);die;

		$visit_customer = new Model("visit_customer");
		$order_info = new Model("order_info");
		$count = "0";
		//待办事项
		if($table_type == "wait"){
			$wait = new Model("waitmatter");
			$count = $wait->where($where_wait)->count();
		}elseif($table_type == "visit" || $table_type == "open"){
			//今日回访任务
			$count = $visit_customer->where($where_visit)->count();
		}elseif($table_type == "out_visit"){
			//今日外呼回访任务
			$count = $visit_customer->where($where_visit2)->count();
		}elseif($table_type == "notice"){
			//公告
			$gg = new Model("announcement");
			$count = $gg->where($where_gg)->count();
		}elseif($table_type == "missedCall"){
			//未接来电
			$cdr = new Model('asteriskcdrdb.cdr');
			if($username != "admin"){
				$count = $cdr->where("dst='$extension' AND disposition !='ANSWERED'")->count();
			}else{
				$count = $cdr->where("disposition !='ANSWERED'")->count();
			}
		}elseif($table_type == "novisit"){
			//最近未联系客户
			$visit_record = new Model("customer");
			$count = $visit_record->where($where_noVisit)->count();
		}elseif($table_type == "purchase"){
			//最近可能需要复购的资料
			$count = $order_info->table("order_info o")->field("o.id,o.purchase_time,o.order_num,o.customer_id,c.phone1,c.name")->join("customer c on (o.customer_id = c.id)")->where($where_purchase)->count();
		}elseif($table_type == "order"){
			//订单状态更变提醒
			$count = $order_info->where($where_order_status)->count();
		}elseif($table_type == "hotline"){
			//热线资源
			$hotline_record = M("hotline_record");
			$count = $hotline_record->where($where_hotline)->count();
		}

		echo json_encode(array('success'=>true,'msg'=>$count));
	}

	function index(){
		checkLogin();
		$dashboard = new Model("Dashboard");
		$arrList = $dashboard->order("`order` ASC")->select();
		$this->assign("panelList",$arrList);
		$this->display();

		set_time_limit(0);
		$ctx = stream_context_create(array(
			'http' => array(
				'timeout' => 10    //设置一个超时时间，单位为秒
			)
		));
		$arrData = @file_get_contents("http://al.robot365.com/Index/Conf/warrant.php",0,$ctx);
		$arrF = json_decode($arrData,true);
		$checkRole = getSysinfo();
		/*
		if( strstr($checkRole[2],"wechat") ){
			$checkRole[2] = trim(str_replace(",wechat","",$checkRole[2]));
		}
		*/
		//warrant_status为N：表示已经收回授权
		foreach($arrF as &$val){
			$val["mac_addr1"] = str_replace(":","",$val['mac_addr1']);
			$val["mac_addr2"] = str_replace(":","",$val['mac_addr2']);
			if( ($checkRole[9] == strtolower($val["mac_addr1"]) || $checkRole[9] == strtolower($val["mac_addr2"]) ) && $val["warrant_status"] == "N" ){
				$warrantArr[] = $val;
			}
			if( $checkRole[9] == strtolower($val["mac_addr1"]) || $checkRole[9] == strtolower($val["mac_addr2"]) ){
				$cmArr[] = $val;
			}
		}
		/*
		$lifeTime = 24 * 3600;
		session_set_cookie_params($lifeTime);
		session_start();

		if(count($warrantArr)>0){
			$_SESSION["warrant_role"] = "N";
		}else{
			$_SESSION["warrant_role"] = "Y";
		}
		*/
		if($cmArr){
			if(count($warrantArr)>0){
				$_SESSION["warrant_role"] = "N";
				file_put_contents("data/system/warrant.php","N");
			}else{
				$_SESSION["warrant_role"] = "Y";
				file_put_contents("data/system/warrant.php","Y");
			}
		}

	}

	function listPanelSetParam(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Dashboard Setting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function panelData(){
		$dashboard = new Model("dashboard");
		import('ORG.Util.Page');
		$count = $dashboard->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$arrpanel = $dashboard->select();
		$rowsList = count($arrpanel) ? $arrpanel : false;
		$arrPan["total"] = $count;
		$arrPan["rows"] = $rowsList;

		echo json_encode($arrPan);
	}

	function insertPanelSet(){
		$dashboard = new Model("Dashboard");
		$arrData = array(
			'name'=>$_REQUEST['name'],
			'url'=>$_REQUEST['url'],
			'order'=>$_REQUEST['order'],
		);
		$result = $dashboard->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'面板添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updatePanelSet(){
		$id = $_REQUEST["id"];
		$dashboard = new Model("dashboard");
		$arrData = array(
			'name'=>$_POST['name'],
			'url'=>$_POST['url'],
			'order'=>$_POST['order'],
		);
		$result = $dashboard->data($arrData)->where("id = $id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deletePanelSet(){
		$id = $_REQUEST["id"];
		//dump($id);die;
		$dashboard = new Model("dashboard");
		$result = $dashboard->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function hardDiskState(){
		checkLogin();
		import('ORG.Util.DashboardCheck');
		$sys = new Dashboard();

		$serverSituation = $sys->obtener_info_de_sistema();
		//dump($serverSituation);
		//硬盘
		$num_bloques_total = $serverSituation['particiones'][0]['num_bloques_total'];
		$num_bloques_usados = $serverSituation['particiones'][0]['num_bloques_usados'];
		$num_bloques_disponibles = $serverSituation['particiones'][0]['num_bloques_disponibles'];
		$uso_porcentaje = $serverSituation['particiones'][0]['uso_porcentaje'];
		$hard_used = intval(substr($uso_porcentaje,0,2));  //硬盘已使用的概率
		$hard_lv = intval(100-$hard_used); //硬盘可用率

		$this->assign("hard_used",$hard_used); //硬盘 使用率
		$this->assign("hard_lv",$hard_lv); //硬盘可用率

		$this->display();
	}

	function systemResources(){
		checkLogin();
		import('ORG.Util.DashboardCheck');
		$sys = new Dashboard();
		$serverSituation = $sys->obtener_info_de_sistema();
		//dump($serverSituation);

		//硬盘
		$num_bloques_total = $serverSituation['particiones'][0]['num_bloques_total'];
		$num_bloques_usados = $serverSituation['particiones'][0]['num_bloques_usados'];
		$num_bloques_disponibles = $serverSituation['particiones'][0]['num_bloques_disponibles'];
		$uso_porcentaje = $serverSituation['particiones'][0]['uso_porcentaje'];

		$hard_used = substr($uso_porcentaje,0,2);  //硬盘已使用的概率
		$hard_lv = 100-$hard_used; //硬盘可用率

		//CPU
		$CpuUsage = substr($serverSituation['CpuUsage']*100,0,4);  //CPU使用率
		$SysUptime = $serverSituation['SysUptime'];
		$CpuMHz = $serverSituation['CpuMHz']." MHz";
		$CpuModel = $serverSituation['CpuModel'];
		$CpuVendor = $serverSituation['CpuVendor'];
		//dump($CpuUsage);die;

		//RAM 内存
		$MemTotal = $serverSituation['MemTotal'];
		//$MemFree = $serverSituation['MemFree'];
		$MemFree = $serverSituation['MemFree']+$serverSituation['Cached']; //wjj--edit
		$Mem_used = ceil(($MemTotal-$MemFree)/1024)." M";  //使用的内存
		$Mem_lv = substr( (($MemTotal-$MemFree)/$MemTotal)*100,0,4);

		$SwapTotal = $serverSituation['SwapTotal'];
		$SwapFree = $serverSituation['SwapFree'];  //可用的虚拟空间
		$Swap_uselv = ceil(($SwapTotal-$SwapFree)/1024)." M"; //已用的虚拟内存（单位为M）
		$Swap_lv = ($SwapFree/$SwapTotal)*100; //swap可用率
		$Swap_used = substr(100-$Swap_lv,0,4);
		//dump($Mem_used);die;

		$this->assign("Swap_uselv",$Swap_uselv);  //已用的虚拟内存（单位为M）
		$this->assign("Mem_used",$Mem_used);  //已用的内存
		$this->assign("CpuMHz",$CpuMHz);  //cpu Speed
		$this->assign("SysUptime",$SysUptime);  //使用中
		$this->assign("CpuModel",$CpuModel);  //cpu信息
		$this->assign("CpuUsage",$CpuUsage);  //cpu使用率
		$this->assign("Swap_lv",$Swap_lv);    //swap可用率
		$this->assign("Swap_used",$Swap_used); //swap 使用率
		$this->assign("Mem_lv",$Mem_lv); //RAM 内存 使用率
		$this->assign("hard_used",$hard_used); //硬盘 使用率
		$this->assign("hard_lv",$hard_lv); //硬盘可用率

		$this->display();
	}

	function CpuCallsMem(){
		checkLogin();
		import('ORG.Pbx.sampler');
		import('ORG.Db.bgDB');

		$oSampler = new Sampler();
		$a["cpu"] = $oSampler->getSamplesByLineId("2");
		$a["calls"] = $oSampler->getSamplesByLineId("1");
		$a["mem"] = $oSampler->getSamplesByLineId("3");

		//dump($a["calls"]);die;

		foreach($a["cpu"] as $val){
			//$cpuTime[] = date("Y-m-d",$val['timestamp']);
			$cpuTime[] = date("Y-m-d H:i",$val['timestamp']);
			$cpuvalue[] = $val['value'];
			$tt[] = date("Y-m-d H:i",$val['timestamp'])."----".$val['value'];

		}
		//$cpu_tplTime = implode(",",$cpuTime);
		//$aa = array_splice($cpuvalue,0,289);
		/*
		$aa = array_splice($cpuvalue,21);
		$cpu_tplval = implode(",",$aa);

		$cupT = str_replace("-",",",date('Y-m-d-H-i',strtotime("$cpuTime[21] -1 month")));
		*/
		if(count($cpuvalue) < 50){
			$cupT = str_replace("-",",",date('Y-m-d-H-i',strtotime("$cpuTime[1] -1 month")));
			$cpu_tplval = implode(",",$cpuvalue);
		}else{
			$cupT = str_replace("-",",",date('Y-m-d-H-i',strtotime("$cpuTime[21] -1 month")));
			$aa = array_splice($cpuvalue,21);
			$cpu_tplval = implode(",",$aa);
		}


		foreach($a['calls'] as $val){
			$callsTime[] = date("Y,m,d,H,i",$val['timestamp']);
			$callsvalue[] =$val['value'];
		}
		//$calls_tplTime = implode(",",$callsTime);
		$call_tplval = implode(",",$callsvalue);
		foreach($a['mem'] as $val){
			$memTime[] = date("Y,m,d,H,i",$val['timestamp']);
			$memvalue[] = $val['value'];

		}

		//dump($tt);die;
		//exit;
		$mem_tplval = implode(",",$memvalue);
		//$cpu_tplval = "15.6,10.8,".$cpu_tplval;
		//dump($cpu_tplval);die;
		//$this->assign("cpuTime",$cpuTime[0]);
		$this->assign("cpuTime",$cupT);
		$this->assign("cpuvalue",$cpu_tplval);
		$this->assign("callsTime",$callsTime[0]);
		$this->assign("callsvalue",$call_tplval);
		$this->assign("memTime",$memTime[0]);
		$this->assign("memvalue",$mem_tplval);
		$this->display();


	}



	function CpuCallsMem1(){
		checkLogin();
		import('ORG.Pbx.sampler');
		import('ORG.Db.bgDB');

		$oSampler = new Sampler();
		$a["cpu"] = $oSampler->getSamplesByLineId("2");
		$a["calls"] = $oSampler->getSamplesByLineId("1");
		$a["mem"] = $oSampler->getSamplesByLineId("3");

		//dump($a["calls"]);die;

		foreach($a["cpu"] as $val){
			$cpuTime[] = date("H:i",$val['timestamp']);
			$cpuvalue[] = $val['value'];
			$qq[] = date("Y-m-d H:i:s",$val['timestamp'])."   ".$val['value'];

		}
		$aaa = array_slice($cpuTime,0,20);
		$cpuTime1 = "'".implode("','",$aaa)."'";
		$bbb = array_slice($cpuvalue,0,20);
		$cpu_tplval = implode(",",$bbb);
		foreach($a['calls'] as $val){
			$callsTime[] = date("Y,m,d,H,i",$val['timestamp']);
			$callsvalue[] =$val['value'];
		}
		//$calls_tplTime = implode(",",$callsTime);
		$call_tplval = implode(",",$callsvalue);
		foreach($a['mem'] as $val){
			$memTime[] = date("Y,m,d,H,i",$val['timestamp']);
			$memvalue[] = $val['value'];

		}

		$cupT = str_replace("-",",",date('Y-m-d',strtotime("$cpuTime[0] -1 month")));
		//dump($cpuTime);
		//exit;
		$mem_tplval = implode(",",$memvalue);

		//$this->assign("cpuTime",$cpuTime[0]);
		$this->assign("cpuTime",$cupT);
		$this->assign("cpuTime1",$cpuTime1);
		$this->assign("cpuvalue",$cpu_tplval);
		$this->assign("callsTime",$callsTime[0]);
		$this->assign("callsvalue",$call_tplval);
		$this->assign("memTime",$memTime[0]);
		$this->assign("memvalue",$mem_tplval);
		$this->display();


	}



	function mem(){
		$mem = $this->CpuCallsMem();
		//$this -> display();
	}

	function memGraphic(){
		checkLogin();
		import('ORG.Pbx.sampler');
		import('ORG.Db.bgDB');

		$oSampler = new Sampler();
		$a["mem"] = $oSampler->getSamplesByLineId("3");
		foreach($a['mem'] as $val){
			//$memTime[] = date("Y-m-d",$val['timestamp']);
			$memTime[] = date("Y-m-d H:i",$val['timestamp']);
			$memvalue[] = $val['value'];
			$tt[] = date("Y-m-d H:i",$val['timestamp'])."----".$val['value'];

		}
		if(count($memvalue) < 50){
			$memT = str_replace("-",",",date('Y-m-d-H-i',strtotime("$memTime[1] -1 month")));
			$mem_tplval = implode(",",$memvalue);
		}else{
			$memT = str_replace("-",",",date('Y-m-d-H-i',strtotime("$memTime[21] -1 month")));
			$aa = array_splice($memvalue,21);
			$mem_tplval = implode(",",$aa);
		}

		//dump($mem_tplval);
		//dump($tt);die;
		//$this->assign("memTime",$memTime[0]);
		$this->assign("memTime",$memT);
		$this->assign("memvalue",$mem_tplval);
		$this -> display();
	}


	function callsGraphic(){
		checkLogin();
		import('ORG.Pbx.sampler');
		import('ORG.Db.bgDB');

		$oSampler = new Sampler();
		$a["calls"] = $oSampler->getSamplesByLineId("1");
		foreach($a['calls'] as $val){
			//$callsTime[] = date("Y-m-d",$val['timestamp']);
			$callsTime[] = date("Y-m-d H:i",$val['timestamp']);
			$callsvalue[] =$val['value'];
			$tt[] = date("Y-m-d H:i",$val['timestamp'])."----".$val['value'];
		}

		if(count($callsvalue) < 50){
			$callT = str_replace("-",",",date('Y-m-d-H-i',strtotime("$callsTime[1] -1 month")));
			$call_tplval = implode(",",$callsvalue);
		}else{
			$callT = str_replace("-",",",date('Y-m-d-H-i',strtotime("$callsTime[21] -1 month")));
			//$calls_tplTime = implode(",",$callsTime);
			//$aa = array_splice($callsvalue,0,289);
			$aa = array_splice($callsvalue,21);
			$call_tplval = implode(",",$aa);
		}
		//$this->assign("callsTime",$callsTime[0]);
		$this->assign("callsTime",$callT);
		$this->assign("callsvalue",$call_tplval);
		$this -> display();
	}





}

?>

