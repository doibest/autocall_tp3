<?php
//工单管理
class WorkOrderManagementAction extends Action{
	//工单类型
	function workOrderType(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order Type";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$this->display();
	}

	function workOrderTypeData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$mod = M("work_order_type");
		$count = $mod->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->limit($page->firstRow.','.$page->listRows)->select();
		$enabled_row = array('Y'=>'启用','N'=>'禁用');
		foreach($arrData as &$val){
			$val['enabled2'] = $enabled_row[$val['enabled']];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertOrderType(){
		$username = $_SESSION['user_info']['username'];
		$mod = M("work_order_type");
		$arrData = array(
			'type_name'=>$_REQUEST['type_name'],
			'enabled'=>$_REQUEST['enabled'],
		);
		$result = $mod->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateOrderType(){
		$id = $_REQUEST['id'];
		$mod = M("work_order_type");
		$arrData = array(
			'type_name'=>$_REQUEST['type_name'],
			'enabled'=>$_REQUEST['enabled'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function deleteOrderType(){
		$id = $_REQUEST["id"];
		$mod = M("work_order_type");
		$result = $mod->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//工单状态
	function workOrderStatus(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order Status";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$this->display();
	}

	function workOrderStatusData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$mod = M("work_order_status");
		$count = $mod->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->limit($page->firstRow.','.$page->listRows)->select();
		$enabled_row = array('Y'=>'启用','N'=>'禁用');
		foreach($arrData as &$val){
			$val['enabled2'] = $enabled_row[$val['enabled']];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertOrderStatus(){
		$username = $_SESSION['user_info']['username'];
		$mod = M("work_order_status");
		$arrData = array(
			'status_name'=>$_REQUEST['status_name'],
			'enabled'=>$_REQUEST['enabled'],
		);
		$result = $mod->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateOrderStatus(){
		$id = $_REQUEST['id'];
		$mod = M("work_order_status");
		$arrData = array(
			'status_name'=>$_REQUEST['status_name'],
			'enabled'=>$_REQUEST['enabled'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function deleteOrderStatus(){
		$id = $_REQUEST["id"];
		$mod = M("work_order_status");
		$result = $mod->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}



	//工单记录
	function workOrderList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$users = M("users");
		$arrU = $users->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$customer_id = $_REQUEST["customer_id"];
		$this->assign("customer_id",$customer_id);

		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		import("ORG.Util.AjaxPage");// 导入分页类 注意导入的是自己写的AjaxPage类
		$mod = M('work_order_list');
		$count = $mod->count(); //计算记录数
		$limitRows =$para_sys["page_rows"]; // 设置每页记录数
		$page = new AjaxPage($count, $limitRows,"AjaxPageSearch"); //第三个参数是你需要调用换页的ajax函数名
		$arrData = $mod->order('priority asc')->limit($page->firstRow . "," . $page->listRows)->select(); // 查询数据

		$arrType = $this->getType();
		$arrStatus = $this->getStatus();
		$arrPriority = array("1"=>"<span style='color:red;'>紧急</span>","2"=>"高","3"=>"中","4"=>"低");
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];
		$deptId_name = $userArr["deptId_name"];
		foreach($arrData as &$val){
			$val["work_order_type2"] = $arrType[$val["work_order_type"]];
			$val["work_order_status2"] = $arrStatus[$val["work_order_status"]];
			$val["priority2"] = $arrPriority[$val["priority"]];
			$val["cn_name"] = $cnName[$val["create_user"]];
			$val["dept_name"] = $deptId_name[$val["dept_id"]];

			$val["uniqueid"] = trim($val["recording"]);
			$val['operations'] = "";
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";

			if(file_exists($WAVfile) ){
				$val['operations'] .= "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".$val["uniqueid"]."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .$val["uniqueid"]."'> 下载 </a> &nbsp;&nbsp; <a  href='javascript:void(0);' onclick=\"transferWorkOrders("."'".$val["id"]."'".")\" > 转交 </a>" ;
			}
		}

		$show = $page->show();
		$this->assign('arrData',$arrData);
		$this->assign('page',$show);

		$this->display();
	}

	function myWorkOrder(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		import("ORG.Util.AjaxPage");// 导入分页类 注意导入的是自己写的AjaxPage类
		$mod = M('work_order_list');
		$count = $mod->count(); //计算记录数
		$limitRows =$para_sys["page_rows"]; // 设置每页记录数
		$page = new AjaxPage($count, $limitRows,"AjaxPageSearch"); //第三个参数是你需要调用换页的ajax函数名
		$arrData = $mod->order('priority asc')->limit($page->firstRow . "," . $page->listRows)->select(); // 查询数据

		$arrType = $this->getType();
		$arrStatus = $this->getStatus();
		$arrPriority = array("1"=>"<span style='color:red;'>紧急</span>","2"=>"高","3"=>"中","4"=>"低");
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];
		$deptId_name = $userArr["deptId_name"];
		foreach($arrData as &$val){
			$val["work_order_type2"] = $arrType[$val["work_order_type"]];
			$val["work_order_status2"] = $arrStatus[$val["work_order_status"]];
			$val["priority2"] = $arrPriority[$val["priority"]];
			$val["cn_name"] = $cnName[$val["create_user"]];
			$val["dept_name"] = $deptId_name[$val["dept_id"]];

			$val["uniqueid"] = trim($val["recording"]);
			$val['operations'] = "";
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";

			if(file_exists($WAVfile) ){
				$val['operations'] .= "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".$val["uniqueid"]."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .$val["uniqueid"]."'> 下载 </a> &nbsp;&nbsp; <a  href='javascript:void(0);' onclick=\"transferWorkOrders("."'".$val["id"]."'".")\" > 转交 </a>" ;
			}
		}

		$show = $page->show();
		$this->assign('arrData',$arrData);
		$this->assign('page',$show);
		$this->display();


	}


	function workOrderRecord(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$users = M("users");
		$arrU = $users->order("username asc")->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$customer_id = $_REQUEST["customer_id"];
		$this->assign("customer_id",$customer_id);

		$this->display();
	}

	function agentWorkOrderList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$users = M("users");
		$arrU = $users->order("username asc")->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$customer_id =$_REQUEST["customer_id"];
		$this->assign("customer_id",$customer_id);
		$web_type =$_REQUEST["web_type"];     //my：我的工单 done:经办过的
		$this->assign("web_type",$web_type);
		$work_status =$_REQUEST["work_status"];     //wait：代办工单
		$this->assign("work_status",$work_status);

		$this->display();
	}

	function workOrderRecordData(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$para_sys = readS();

		$web_type = $_REQUEST["web_type"];
		$work_status = $_REQUEST["work_status"];
		$customer_id = $_REQUEST["customer_id"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$where = "1 ";
		$where .= empty($customer_id) ? "" : " AND customer_id = '$customer_id'";
		if($username != "admin"){
			if($web_type == "my"){
				$where .= " AND create_user = '$username'";
			}elseif($web_type == "back"){
				$where .= " AND dept_id in ($deptSet)";
			}elseif($web_type == "done"){
				$where .= " AND find_in_set('$username',history_operators)";
			}
			if($work_status == "wait"){
				$where .= " AND find_in_set('$username',operators) AND work_order_status != '3'";
			}
		}else{
			if($work_status == "wait"){
				$where .= " AND work_order_status != '3'";
			}
		}

		$mod = M("work_order_list");
		$count = $mod->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->order("priority asc,work_order_status asc,create_time desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $mod->getLastSql();die;

		$arrType = $this->getType();
		$arrStatus = $this->getStatus();
		$arrPriority = array("1"=>"<span style='color:red;'>紧急</span>","2"=>"高","3"=>"中","4"=>"低");
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];
		$deptId_name = $userArr["deptId_name"];
		foreach($arrData as &$val){
			$val["work_order_type2"] = $arrType[$val["work_order_type"]];
			$val["work_order_status2"] = $arrStatus[$val["work_order_status"]];
			$val["priority2"] = $arrPriority[$val["priority"]];
			$val["cn_name"] = $cnName[$val["create_user"]];
			$val["dept_name"] = $deptId_name[$val["dept_id"]];
			$val["work_order_type_status"] = $val["work_order_type2"]." / ".$val["work_order_status2"];

			/*
			$val["uniqueid"] = trim($val["recording"]);
			$val['operations'] = "";
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";

			if(file_exists($WAVfile) ){
				$val['operations'] .= "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".$val["uniqueid"]."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .$val["uniqueid"]."'> 下载 </a> " ;
			}
			*/
			$val['operations'] .= " <a  href='javascript:void(0);' onclick=\"transferWorkOrders("."'".$val["id"]."'".")\" > 转交 </a>" ;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function getType(){
		$mod = M("work_order_type");
		$arrData = $mod->select();
		$arrF = array();
		foreach($arrData as $val){
			$arrF[$val["id"]] = $val["type_name"];
		}
		return $arrF;
	}

	function getStatus(){
		$mod = M("work_order_status");
		$arrData = $mod->select();
		$arrF = array();
		foreach($arrData as $val){
			$arrF[$val["id"]] = $val["status_name"];
		}
		return $arrF;
	}

	function insertWOrkOrderProblem(){
		$username = $_SESSION['user_info']['username'];
		$dept_id = $_SESSION['user_info']['d_id'];
		$customer_id = $_REQUEST["customer_id"];

		$customer = M("customer");
		$arrCD = $customer->field("name,phone1")->where("id = '$customer_id'")->find();
		$transfer_enabled = $_REQUEST["transfer_enabled"];
		if($transfer_enabled == "Y"){
			$operators = $_REQUEST["sharing_personnel"];
		}else{
			$operators = "";
		}
		$mod = M("work_order_list");
		$arrData = array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'history_operators'=>$username,
			'dept_id'=>$dept_id,
			'customer_id'=>$_REQUEST['customer_id'],
			'customer_name'=>$arrCD['name'],
			'phone'=>$arrCD['phone1'],
			'work_order_title'=>$_REQUEST['work_order_title'],
			'work_order_type'=>$_REQUEST['work_order_type'],
			'work_order_status'=>$_REQUEST['work_order_status'],
			'priority'=>$_REQUEST['priority'],
			'transfer_enabled'=>$transfer_enabled,
			'recording'=>$_SESSION[$username."_work_order"][$_REQUEST["customer_id"]],
			'operators'=>$operators,
		);
		$result = $mod->data($arrData)->add();
		if ($result){
			$mod2 = M("work_order_content");
			$arrD = array(
				'create_time'=>date("Y-m-d H:i:s"),
				'create_user'=>$username,
				'dept_id'=>$dept_id,
				'work_order_id'=>$result,
				'customer_id'=>$_REQUEST['customer_id'],
				'work_order_problem'=>$_REQUEST['work_order_title'],
				'work_order_status'=>$_REQUEST['work_order_status'],
				'work_order_node'=>"1",
				'recording'=>$_SESSION[$username."_work_order"][$_REQUEST["customer_id"]],
			);
			$result2 = $mod2->data($arrD)->add();
			unset($_SESSION[$username."_work_order"][$_REQUEST["customer_id"]]);
			echo json_encode(array('success'=>true,'msg'=>'添加成功！','work_order_id'=>$result));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateWOrkOrderProblem(){
		$id = $_REQUEST['id'];
		$mod = M("work_order_list");
		$arrData = array(
			'work_order_title'=>$_REQUEST['work_order_title'],
			'work_order_type'=>$_REQUEST['work_order_type'],
			'work_order_status'=>$_REQUEST['work_order_status'],
			'priority'=>$_REQUEST['priority'],
			'transfer_enabled'=>$_REQUEST['transfer_enabled'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function saveTransferWorkOrders(){
		$username = $_SESSION['user_info']['username'];
		$dept_id = $_SESSION['user_info']['d_id'];
		$id = $_REQUEST['id'];
		$mod = M("work_order_list");
		$arrData = array(
			'work_order_title'=>$_REQUEST['work_order_title2'],
			'operators'=>$_REQUEST['operators'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			$arr = $mod->where("id = '$id'")->find();
			$mod2 = M("work_order_content");
			$count = $mod2->where("work_order_id = '$id' AND work_order_node='1'")->count();
			//echo $mod2->getLastSql();
			//dump($count);die;
			if($count > 0){
				$arrD = array(
					'work_order_problem'=>$_REQUEST['work_order_title2'],
					'work_order_status'=>$arr['work_order_status'],
				);
				$result2 = $mod2->data($arrD)->where("work_order_id = '$id' AND work_order_node='1'")->save();
			}else{
				$arrD = array(
					'create_time'=>date("Y-m-d H:i:s"),
					'create_user'=>$username,
					'dept_id'=>$dept_id,
					'work_order_id'=>$id,
					'customer_id'=>$arr['customer_id'],
					'work_order_problem'=>$_REQUEST['work_order_title2'],
					'work_order_status'=>$arr['work_order_status'],
					'work_order_node'=>"1",
					'recording'=>$_SESSION[$username."_work_order"][$_REQUEST["customer_id"]],
				);
				$result2 = $mod2->data($arrD)->add();
			}

			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function workOrderContentList(){
		checkLogin();
		$work_order_id = $_REQUEST["work_order_id"];
		$this->assign("work_order_id",$work_order_id);

		$this->display();
	}

	function workOrderContentData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$work_order_id = $_REQUEST["work_order_id"];
		$where = "work_order_id = '$work_order_id'";
		$mod = M("work_order_content");
		$count = $mod->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->order("work_order_node desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$arrStatus = $this->getStatus();
		foreach($arrData as &$val){
			$val["work_order_status2"] = $arrStatus[$val["work_order_status"]];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addWorkOrderContent(){
		$work_order_id = $_REQUEST["work_order_id"];
		$this->assign("work_order_id",$work_order_id);

		$where = "work_order_id = '$work_order_id'";

		$mod = M("work_order_content");
		$arrData = $mod->order("work_order_node asc")->where($where)->select();
		//echo $mod->getLastSql();die;
		foreach($arrData as &$val){
			$val["uniqueid"] = trim($val["recording"]);
			$val['operations'] = "";
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";

			if(file_exists($WAVfile) ){
				$val['operations'] .= "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".$val["uniqueid"]."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .$val["uniqueid"]."'> 下载 </a>" ;
			}
		}

		$this->assign("arrData",$arrData);
		$max_node = count($arrData);
		$LastNode =  $mod->order("work_order_node desc")->where($where)->find();
		$LastNode["uniqueid"] = trim($LastNode["recording"]);
		$LastNode['operations'] = "";
		$arrTmp = explode('.',$LastNode["uniqueid"]);
		$timestamp = $arrTmp[0];
		$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
		$WAVfile = $dirPath ."/".$LastNode["uniqueid"].".WAV";

		if(file_exists($WAVfile) ){
			$LastNode['operations'] .= "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".$LastNode["uniqueid"]."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .$LastNode["uniqueid"]."'> 下载 </a>" ;
		}
		$this->assign("max_node",$max_node);
		$this->assign("LastNode",$LastNode);


		$users = M("users");
		$arrU = $users->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$customer_id = $_REQUEST["customer_id"];
		$customer = M("customer");
		$arrCD = $customer->where("id = '$customer_id'")->find();
		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');
		$para_sys = readS();

		foreach($fielddTpl2 as $val){
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields" && $val["en_name"] != "sms_cust"){
					$fielddTpl[] = $val;
				}
			}else{
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
					$fielddTpl[] = $val;
				}
			}
		}
		$fields = "`".implode("`,`",$arrF)."`,`dealresult_id`";
		$this->assign("fielddTpl",$fielddTpl);
		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				$arrCD[$val['en_name']] = $row[$val['en_name']][$arrCD[$val['en_name']]];
			}
		}

		$this->assign("arrCD",$arrCD);

		$work_order_list = M("work_order_list");
		$arrWD = $work_order_list->where("id = '$work_order_id'")->find();
		$arrType = $this->getType();
		$arrStatus = $this->getStatus();
		$arrPriority = array("1"=>"<span style='color:red;'>紧急</span>","2"=>"高","3"=>"中","4"=>"低");
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$arrWD["work_order_type2"] = $arrType[$arrWD["work_order_type"]];
		$arrWD["work_order_status2"] = $arrStatus[$arrWD["work_order_status"]];
		$arrWD["priority2"] = $arrPriority[$arrWD["priority"]];
		$arrWD["cn_name"] = $cnName[$arrWD["create_user"]];
		$arrWD["dept_name"] = $deptId_name[$arrWD["dept_id"]];

		$arrWD["uniqueid"] = trim($arrWD["recording"]);
		$arrWD['operations'] = "";
		$arrTmp = explode('.',$arrWD["uniqueid"]);
		$timestamp = $arrTmp[0];
		$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
		$WAVfile = $dirPath ."/".$arrWD["uniqueid"].".WAV";

		if(file_exists($WAVfile) ){
			$arrWD['operations'] .= "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".$arrWD["uniqueid"]."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .$arrWD["uniqueid"]."'> 下载 </a> " ;
		}
		$this->assign("arrWD",$arrWD);

		//dump($arrData);die;

		$this->display();
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	function saveSolution(){
		$username = $_SESSION['user_info']['username'];
		$dept_id = $_SESSION['user_info']['d_id'];
		$id = $_REQUEST["id"];
		$work_order_id = $_REQUEST["work_order_id"];
		$customer_id = $_REQUEST["customer_id"];
		$mod = M("work_order_content");

		$arrData = array(
			'work_order_solution'=>$_REQUEST['work_order_solution'],
			'solve_problem_user'=>$username,
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			$mod2 = M("work_order_list");
			$arrWOD = $mod2->where("id = '$work_order_id'")->find();
			$history_operators = $arrWOD["history_operators"].",".$username;
			$arrD = array(
				"work_order_status"=>$_REQUEST["work_order_status"],
				"history_operators"=>$history_operators,
			);
			if($_REQUEST["transfer_enabled"] == "Y"){
				$arr = $mod->where("id = '$id'")->find();
				$arrContentData = array(
					'create_time'=>date("Y-m-d H:i:s"),
					'create_user'=>$username,
					'dept_id'=>$dept_id,
					'work_order_id'=>$work_order_id,
					'customer_id'=>$_REQUEST['customer_id'],
					'work_order_problem'=>$_REQUEST['work_order_title'],
					'work_order_status'=>$_REQUEST['work_order_status'],
					'work_order_node'=>$arr[work_order_node]+1,
					'recording'=>$_SESSION[$username."_work_order"][$_REQUEST["customer_id"]],
				);
				$res3 = $mod->data($arrContentData)->add();

				$arrD["operators"] = $_REQUEST["operators"];

			}
			$result2 = $mod2->data($arrD)->where("id = '$work_order_id'")->save();
			unset($_SESSION[$username."_work_order"][$_REQUEST["customer_id"]]);

			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function addWorkOrder(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$users = M("users");
		$arrU = $users->order("username asc")->field("username,cn_name")->select();
		$this->assign("arrU",$arrU);

		$customer_id = $_REQUEST["customer_id"];
		$this->assign("customer_id",$customer_id);

		$this->display();
	}

    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }

	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
}
?>
