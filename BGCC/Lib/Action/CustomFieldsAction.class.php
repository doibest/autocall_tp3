<?php
class CustomFieldsAction extends Action{
	function feildsList(){
		checkLogin();
		//分配增删改的权限
		//$menuname = "Custom Fields";
		$menuname = "Customer Fields";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$form_id = $_REQUEST["form_id"];
		$this->assign("form_id",$form_id);
		if($form_id){
			$this->assign("form_have","Y");
		}else{
			$this->assign("form_have","N");
		}
		//dump($form_id);die;

		$this->display();
	}

	function formTable(){
		$work_form_design = new Model("work_form_design");
		$arrData = $work_form_design->field("id,form_name")->select();
		foreach($arrData as $key=>$val){
			$arrF["work_form_".$val["id"]] = $val["form_name"];
		}
		$arrT = array('order_info'=>'订单表','servicerecords'=>'售后服务');
		if($arrF){
			$arr = array_merge($arrF,$arrT);
		}else{
			$arr = $arrT;
		}
		//dump($arr);die;
		return $arr;
	}

	function customerFieldsData(){
		$custom_fields = new Model("custom_fields");
		$table_name = $_REQUEST["table_name"];

		$form_id = $_REQUEST["form_id"];

		$where = "1 ";
		$where .= empty($table_name) ? "" : " AND table_name = '$table_name'";
		$where .= empty($form_id) ? " AND table_name not like 'work_form_%'" : " AND table_name = 'work_form_$form_id'";
		$count = $custom_fields->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		//$fieldData = $custom_fields->order("field_order asc")->limit($page->firstRow.','.$page->listRows)->select();
		$fieldData = $custom_fields->order("field_enabled desc,field_order asc")->where($where)->select();


		//dump($fieldData);die;
		$status_row = array('Y'=>'是','N'=>'否');
		//$table_name_row = array('order_info'=>'订单表','servicerecords'=>'工单/售后服务');
		$table_name_row = $this->formTable();
		$list_enabled_row = array('Y'=>'是','N'=>'否');
		$text_type_row = array('1'=>'单行文本框','2'=>'选择框','3'=>'多行文本框','4'=>'日期时间','5'=>'日期');
		foreach($fieldData as &$val){
			$status = $status_row[$val['field_enabled']];
			$val['field_enabled'] = $status;

			$list_enabled = $list_enabled_row[$val['list_enabled']];
			$val['list_enabled'] = $list_enabled;

			$text_type = $text_type_row[$val['text_type']];
			$val['text_type'] = $text_type;

			$table_name = $table_name_row[$val['table_name']];
			$val['table_name'] = $table_name;

			$val['field_order1'] = $val['field_order'];
			$val['field_order'] = "<a href=\"javascript:updateOrder('". $val['id'] ."','". $val['field_order'] ."','up');\"><img src='include/images/arrow-up-16.png' title='上'></a><span style='padding-right:10px;'></span><a href=\"javascript:updateOrder('". $val['id'] ."','". $val['field_order'] ."','down');\"><img src='include/images/arrow-down-16.png' title='下'></a>";
		}

		$rowsList = count($fieldData) ? $fieldData : false;
		$arrfield["total"] = $count;
		$arrfield["rows"] = $rowsList;
		//dump($fieldData);die;
		echo json_encode($arrfield);
	}

	/*
	function upOrder(){
		$custom_fields = new Model("custom_fields");
		$arr_data = array("field_order"=>$_POST["current_order"]);
		$res = $custom_fields->where("field_order='". ($_POST["current_order"]-1) ."'")->data($arr_data)->save();

		$arr_data = array( "field_order" => ($_POST["current_order"]-1) );
		$res = $custom_fields->where("id='". $_POST["id"] ."'")->data($arr_data)->save();
		echo json_encode(array('success'=>true));
		//$this->fieldCache();
	}

	function downOrder(){
		$custom_fields = new Model("custom_fields");
		$arr_data = array("field_order"=>$_POST["current_order"]);
		$res = $custom_fields->where("field_order='". ($_POST["current_order"]+1) ."'")->data($arr_data)->save();
		$arr_data = array( "field_order" => ($_POST["current_order"]+1) );
		$res = $custom_fields->where("id='". $_POST["id"] ."'")->data($arr_data)->save();
		echo json_encode(array('success'=>true));
		//$this->fieldCache();
	}
	*/


	function upOrder(){
		$custom_fields = new Model("custom_fields");

		$table_name = $_REQUEST["table_name"];
		$form_id = $_REQUEST["form_id"];

		$where = "1 ";
		$where .= empty($table_name) ? "" : " AND table_name = '$table_name'";
		$where .= empty($form_id) ? " AND table_name not like 'work_form_%'" : " AND table_name = 'work_form_$form_id'";

		$cmData = $custom_fields->order("field_enabled desc,field_order asc")->where($where)->select();   //排序要跟列表的排序一样
		foreach($cmData as $val){
			$fieldId[] = $val["id"];
		}
		$orderId = array_flip($fieldId);
		if($_POST["id"] == $fieldId[0]){
			echo json_encode(array("msg"=>"此行已经在最上面!"));
			exit;
		}

		$thisId = $_POST["id"];
		$thisField = $custom_fields->where("id = '$thisId'")->find();
		$thisData = array("field_order"=>$thisField["field_order"]);

		$upId = $fieldId[$orderId[$thisId]-1];
		$upField = $custom_fields->where("id = '$upId'")->find();
		if($thisField["field_order"] == $upField["field_order"]){
			$upData = array("field_order"=>$upField["field_order"]-1);
		}else{
			$upData = array("field_order"=>$upField["field_order"]);
		}



		$thisRes = $custom_fields->data($upData)->where("id = '$thisId'")->save();
		$upRes = $custom_fields->data($thisData)->where("id = '$upId'")->save();

		echo json_encode(array('success'=>true));
		//$this->fieldCache();
	}

	function downOrder(){
		$custom_fields = new Model("custom_fields");

		$table_name = $_REQUEST["table_name"];
		$form_id = $_REQUEST["form_id"];

		$where = "1 ";
		$where .= empty($table_name) ? "" : " AND table_name = '$table_name'";
		$where .= empty($form_id) ? " AND table_name not like 'work_form_%'" : " AND table_name = 'work_form_$form_id'";

		$cmData = $custom_fields->order("field_enabled desc,field_order asc")->where($where)->select();   //排序要跟列表的排序一样
		foreach($cmData as $val){
			$fieldId[] = $val["id"];
		}
		/*
		foreach($cmData as $val){
			if($val["field_enabled"] == "Y"){  //是否启用
				$fieldId[] = $val["id"];
			}
		}
		if($_POST["id"] == $fieldId[count($fieldId)-1]){
			echo json_encode(array("msg"=>"在启用字段中，此行已经在最下面!"));
			exit;
		}
		*/
		$orderId = array_flip($fieldId);
		$thisId = $_POST["id"];
		$thisField = $custom_fields->where("id = '$thisId'")->find();
		$thisData = array("field_order"=>$thisField["field_order"]);

		$downId = $fieldId[$orderId[$thisId]+1];
		$downField = $custom_fields->where("id = '$downId'")->find();
		//$downData = array("field_order"=>$downField["field_order"]);
		if($thisField["field_order"] == $downField["field_order"]){
			$downData = array("field_order"=>$downField["field_order"]+1);
		}else{
			$downData = array("field_order"=>$downField["field_order"]);
		}

		$thisRes = $custom_fields->data($downData)->where("id = '$thisId'")->save();
		$downRes = $custom_fields->data($thisData)->where("id = '$downId'")->save();

		echo json_encode(array('success'=>true));
		//$this->fieldCache();
	}


	function addFields(){
		checkLogin();
		$form_id = $_REQUEST["form_id"];
		$this->assign("form_id",$form_id);
		if($form_id){
			$this->assign("form_have","Y");
		}else{
			$this->assign("form_have","N");
		}


		$custom_fields = new Model("custom_fields");
		$maxID = $custom_fields->max("id");
		$num = $maxID+1;
		$en_name = "name_".$num;
		$this->assign("en_name",$en_name);


		$max_orderid = $custom_fields->max("field_order");
		$field_order = $max_orderid+1;
		$this->assign("field_order",$field_order);

		$this->display();
	}

	function insertFields(){
		//dump($_REQUEST);die;
		$custom_fields = new Model("custom_fields");
		$table_name = $_REQUEST["table_name"];
		$count = $custom_fields->where("en_name='". $_POST["en_name"] ."' AND table_name = '$table_name'")->count();
		if( $count>0 ){
				echo json_encode(array('msg'=>"该字段已存在！"));
				exit;
		}
		$field_values = json_encode($_REQUEST["field_value"]);
		$arrData = array(
			"table_name" => $_REQUEST["table_name"],
			"cn_name" => $_REQUEST["cn_name"],
			"en_name" => $_REQUEST["en_name"],
			"field_attr" => $_REQUEST["field_attr"],
			"field_order" => $_REQUEST["field_order"],
			"text_type" => $_REQUEST["text_type"],
			"field_width" => $_REQUEST["field_width"],
			"field_enabled" => $_REQUEST["field_enabled"],
			"list_enabled" => $_REQUEST["list_enabled"],
			"required_enabled" => $_REQUEST["required_enabled"],
			"tiptools_enabled" => $_REQUEST["tiptools_enabled"],
			"user_enabled" => $_REQUEST["user_enabled"],
			"field_values" => $field_values,
		);
		$result = $custom_fields->data($arrData)->add();

		if($_REQUEST["field_attr"] == "datetime" || $_REQUEST["field_attr"] == "text" || $_REQUEST["field_attr"] == "date" || $_REQUEST["field_attr"] == "double"){
			$sqlF = "ALTER TABLE `$table_name`  ADD `".$_REQUEST["en_name"]."` ".$_REQUEST["field_attr"]."  COMMENT '".$_REQUEST["cn_name"]."' ";
		}else{
			$sqlF = "ALTER TABLE `$table_name`  ADD `".$_REQUEST["en_name"]."` ".$_REQUEST["field_attr"]."(".$_REQUEST["field_width"].")"."  COMMENT '".$_REQUEST["cn_name"]."' ";
		}
		//$en_name,$cn_name,$field_attr,$field_width
		$addF = $custom_fields->execute($sqlF);
		//dump($sqlF);die;

		if($_REQUEST["text_type"] == '2'){
			if($result){
				$custom_select = new Model("custom_select");
				$sql = "insert into custom_select(select_value, select_name, select_enabled,field_id ,select_order) values ";
				$value = "";
				$fieldValue = $_REQUEST["field_value"];
				$i = 0;
				foreach( $fieldValue AS $row ){
					$str = "(";
					$str .= "'" .$row[0]. "',";  	 //选择框value值
					$str .= "'" .$row[1]. "',";		//选择框的值
					$str .= "'" .$row[2]. "',";		//是否显示
					$str .= "'" .$result. "',";		//字段id
					//$str .= "'" .$_REQUEST["en_name"]. "',";		//字段id
					$str .= "'" .$i. "'";

					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $custom_select->execute($sql);
				}
				if($res){
					//$this->fieldCache();
					//$this->selectCache();
					echo json_encode(array('success'=>true,'msg'=>'字段添加成功！'));
				} else {
					echo json_encode(array('msg'=>'字段添加失败！'));
				}
			}
		}else{
			if($result){
				//$this->fieldCache();
				echo json_encode(array('success'=>true,'msg'=>'字段添加成功！'));
			} else {
				echo json_encode(array('msg'=>'字段添加失败！'));
			}
		}
	}

	function editFields(){
		header("Content-Type:text/html; charset=utf-8");
		checkLogin();
		$id = $_REQUEST["id"];
		$custom_fields = new Model("custom_fields");
		$fieldData = $custom_fields->where("id = $id")->find();
		$this->assign("id",$id);
		$this->assign("fieldData",$fieldData);

		if($fieldData['text_type'] == '2'){
			$custom_select = new Model("custom_select");
			$selectData = $custom_select->order("select_order asc")->where("field_id = '$id'")->select();
			$max = $custom_select->where("field_id = '$id'")->max("select_order");
			$this->assign("max",$max);
			$i = 1;
			foreach($selectData as &$val){
				$val["select_order"] = $i;
				$i++;
			}
			$this->assign("selectData",$selectData);
		}
		//dump($max);
		//dump($selectData);die;
		//$this->fieldCache();


		$form_id = $_REQUEST["form_id"];
		$this->assign("form_id",$form_id);
		if($form_id){
			$this->assign("form_have","Y");
		}else{
			$this->assign("form_have","N");
		}

		$this->display();
	}

	function updateFields(){
		$id = $_REQUEST["id"];
		$field_values = json_encode($_REQUEST["field_value"]);
		//dump($field_values);die;
		//dump($_REQUEST["field_value"]);die;
		if($_REQUEST['en_name'] == 'hide_fields'){
			$field_enabled = "Y";
			$list_enabled = "N";
			$required_enabled = "N";
		}else{
			$field_enabled = $_REQUEST["field_enabled"];
			$list_enabled = $_REQUEST["list_enabled"];
			$required_enabled = $_REQUEST["required_enabled"];
		}

		$custom_fields = new Model("custom_fields");
		$arrData = array(
			"cn_name" => $_REQUEST["cn_name"],
			"field_order" => $_REQUEST["field_order"],
			"field_width" => $_REQUEST["field_width"],
			"tiptools_enabled" => $_REQUEST["tiptools_enabled"],
			"user_enabled" => $_REQUEST["user_enabled"],
			"field_enabled" => $field_enabled,
			"list_enabled" => $list_enabled,
			"required_enabled" => $required_enabled,
			"field_values" => $field_values,
		);

		$fieldD = $custom_fields->where("id = '$id'")->find();
		$count = $custom_fields->where("field_width='". $_POST["field_width"] ."'")->count();
		$count2 = $custom_fields->where("cn_name='". $_POST["cn_name"] ."'")->count();
		$result = $custom_fields->where("id = '$id'")->data($arrData)->save();
		//echo $custom_fields->getLastSql();die;
		//dump($count);
		if( $count == 0 || $count2 == 0 ){
			if( $fieldD["field_attr"] == 'datetime' || $fieldD["field_attr"] == 'text'  || $fieldD["field_attr"] == 'date' ){
				$sqlF = "ALTER TABLE $table_name  MODIFY `".$fieldD['en_name']."` ".$fieldD['field_attr']."  COMMENT '".$_REQUEST["cn_name"]."' ";
			}else{
				$sqlF = "ALTER TABLE $table_name  MODIFY `".$fieldD['en_name']."` ".$fieldD['field_attr']."(".$_REQUEST["field_width"].")  COMMENT '".$_REQUEST["cn_name"]."' ";
			}
			$addF = $custom_fields->execute($sqlF);
		}
		if($fieldD["text_type"] == "2"){
			$custom_select = new Model("custom_select");
			$resDel = $custom_select->where("field_id = '$id'")->delete();
			$sql = "insert into custom_select(select_value, select_name, select_enabled,field_id ,select_order) values ";
			$value = "";
			$fieldValue = $_REQUEST["field_value"];
			//dump($fieldValue);die;
			$i = 0;
			foreach( $fieldValue AS $row ){
				$str = "(";
				$str .= "'" .$row[0]. "',";  	 //选择框value值
				$str .= "'" .$row[1]. "',";		//选择框的值
				$str .= "'" .$row[2]. "',";		//是否显示
				$str .= "'" .$id. "',";		//字段id
				//$str .= "'" .$fieldD['en_name']. "',";		//字段id
				//$str .= "'" .$i. "'";
				//$str .= "'" .$row[3]. "'";
				$str .= "'" .$i. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
				$i++;
			}
			unset($i);
			if( $value ){
				$sql .= $value;
				$res = $custom_select->execute($sql);
			}
			if($res){
				//$this->fieldCache();
				//$this->selectCache();
				echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}else{
			if($result !== false){
				//$this->fieldCache();
				//$this->selectCache();
				echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}
	}

	function groupBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row;
		}
		return $ret;
	}

	function fieldCache(){
		//header("Content-Type:text/html; charset=utf-8");
		$custom_fields = new Model("custom_fields");
		$fieldData = $custom_fields->order("field_order asc")->select();
		$arrData = $this->groupBy($fieldData,"table_name");
		//dump($arrData);die;

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("order_fieldCache_$db_name",$arrData,"BGCC/Conf/crm/");
		F("order_fieldCache",$arrData,"BGCC/Conf/crm/$db_name/");
	}
	function selectCache(){
		header("Content-Type:text/html; charset=utf-8");
		$custom_fields = new Model("custom_fields");
		$fieldData = $custom_fields->order("field_order asc")->where("text_type = '2'")->select();

		foreach($fieldData as $val){
			$val["field_values"] = json_decode($val["field_values"] ,true);
			$selectTpl[] = $val;
		}
		foreach($selectTpl as $v){
			foreach($v['field_values'] as $vm){
				$tmp[$v["table_name"]][$v['en_name']][] = array($vm[0]=>$vm[1]);
			}
		}
		foreach($tmp As $key=>&$val){
			$arr = array();
			foreach($tmp[$key] AS $kv=>$vm){
				foreach($vm as $k=>$v){
					foreach($v as $km=>$vs){
						$arr[$kv][$km]=$vs;
					}
				}
			}
			//$val = $arr;
			$tmp[$key] = $arr;
			unset($arr);
		}
		//dump($tt);
		//dump($tmp);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("order_selectCache_$db_name",$tmp,"BGCC/Conf/crm/");
		F("order_selectCache",$tmp,"BGCC/Conf/crm/$db_name/");
	}

	function selectCache2(){
		$custom_select = new Model("custom_select");
		$selectData = $custom_select->order("field_id asc")->select();
		foreach($selectData as $key => $val){
			$tmp[$val["name"]][]	= array($val["select_value"]=>$val["select_name"]);
		}
		foreach($tmp As $key=>&$val){
			$arr = array();
			foreach($val AS $value){
				foreach($value as $k=>$v){
					$arr[$k] = $v;
				}
			}
			$val = $arr;
		}
		//dump($tmp);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("order_selectCache_$db_name",$tmp,"BGCC/Conf/crm/");
		F("order_selectCache",$tmp,"BGCC/Conf/crm/$db_name/");
	}

	function taskCombobox(){
		$sales_task = new Model("sales_task");
		$taskList = $sales_task->field("id,name as text")->order("createtime")->where("enable='Y'")->select();
		echo json_encode($taskList);
	}

}

?>
