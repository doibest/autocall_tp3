<?php
class QualityReportingAction extends Action{
	function qualityReportList(){
		checkLogin();
		$sales_task = new Model("sales_task");
		$taskList = $sales_task->field("id as task_id,name")->order("createtime")->where("enable='Y'")->select();
		$this->assign("taskList",$taskList);

		$qualityscore = new Model("qualityscore");
		$qlist = $qualityscore->select();
		foreach($qlist as $vm){
			$users[] = $vm["username"];
			$score[] = $vm["score"];
		}
		$uname = "'".implode("','",$users)."'";
		$score_tpl = implode(",",$score);
		$this->assign("uname",$uname);
		$this->assign("score_tpl",$score_tpl);
		$norm = new Model("normtype");
		$normList = $norm->select();
		$this->assign("normList",$normList);

		$i = 0;
		foreach($normList as $vm){
			$arrField[0][$i]['field'] = $vm['en_normname'];
			$arrField[0][$i]['title'] = $vm['cn_normname'];
			//$arrField[0][$i]['width'] = '60';
			$arrField[0][$i]['fitColumns'] = true;
			$i++;
		}
		unset($i);
		$arrF = array("field"=>"username","title"=>"工号","fitColumns"=>true);
		$arrF4 = array("field"=>"cn_name","title"=>"姓名","fitColumns"=>true);
		$arrF2 = array("field"=>"score","title"=>"总分","fitColumns"=>true);
		$arrF3 = array("field"=>"num","title"=>"质检次数","fitColumns"=>true);
		array_unshift($arrField[0],$arrF2);
		array_unshift($arrField[0],$arrF4);
		array_unshift($arrField[0],$arrF);
		array_push($arrField[0],$arrF3);
		//dump($arrField);

		$qualityscore = new Model("qualityscore");
		$qualityData = $qualityscore->limit($page->firstRow.','.$page->listRows)->select();
		$rowsList = count($qualityData) ? $qualityData : false;
		$arrquality["total"] = $count;
		$arrquality["rows"] = $rowsList;
		$arrquality["columns"] = json_encode($arrField);
		$this->assign("field_tpl",$arrquality["columns"]);
		$currentDate = Date('Y-m-d H:i:s');
		$this->assign('currentDate',$currentDate);


		$task_id = empty($_REQUEST["task_id"]) ? "Y" : $_REQUEST["task_id"];
		$this->assign("task_id",$task_id);

		$this->display();
	}
	function qualityOutReportList(){
		checkLogin();
		$sales_task = new Model("sales_task");
		$taskList = $sales_task->field("id as task_id,name")->order("createtime")->where("enable='Y'")->select();
		$this->assign("taskList",$taskList);

		$qualityscore = new Model("qualityscore");
		$qlist = $qualityscore->select();
		foreach($qlist as $vm){
			$users[] = $vm["username"];
			$score[] = $vm["score"];
		}
		$uname = "'".implode("','",$users)."'";
		$score_tpl = implode(",",$score);
		$this->assign("uname",$uname);
		$this->assign("score_tpl",$score_tpl);
		$norm = new Model("normtype");
		$normList = $norm->select();
		$this->assign("normList",$normList);

		$i = 0;
		foreach($normList as $vm){
			$arrField[0][$i]['field'] = $vm['en_normname'];
			$arrField[0][$i]['title'] = $vm['cn_normname'];
			//$arrField[0][$i]['width'] = '60';
			$arrField[0][$i]['fitColumns'] = true;
			$i++;
		}
		unset($i);
		$arrF = array("field"=>"username","title"=>"工号","fitColumns"=>true);
		$arrF4 = array("field"=>"cn_name","title"=>"姓名","fitColumns"=>true);
		$arrF2 = array("field"=>"score","title"=>"总分","fitColumns"=>true);
		$arrF3 = array("field"=>"num","title"=>"质检次数","fitColumns"=>true);
		array_unshift($arrField[0],$arrF2);
		array_unshift($arrField[0],$arrF4);
		array_unshift($arrField[0],$arrF);
		array_push($arrField[0],$arrF3);
		//dump($arrField);

		$qualityscore = new Model("qualityscore");
		$qualityData = $qualityscore->limit($page->firstRow.','.$page->listRows)->select();
		$rowsList = count($qualityData) ? $qualityData : false;
		$arrquality["total"] = $count;
		$arrquality["rows"] = $rowsList;
		$arrquality["columns"] = json_encode($arrField);
		$this->assign("field_tpl",$arrquality["columns"]);
		$currentDate = Date('Y-m-d H:i:s');
		$this->assign('currentDate',$currentDate);


		$task_id = empty($_REQUEST["task_id"]) ? "Y" : $_REQUEST["task_id"];
		$this->assign("task_id",$task_id);

		$this->display();
	}

	function qualityReportData(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_end = Date('Y-m-d',$_GET['ts_end']) ." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){//上个月的起始时间
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1)) ." 00:00:00";
			}else{
				$date_start = Date('Y-m-d',$_GET['ts_start']) ." 00:00:00";
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}

		$task_id = $_GET['task_id'];
		$score = $_GET['score'];  //总分大于
		$score_end = $_GET['score_end'];   //总分小于
		$score_detail = $_GET['score_detail'];
		$score_num = $_GET['score_num'];  //详情分数小于
		$score_num_end = $_GET['score_num_end'];  //详情分数大于
		//dump($task_id);


		$having .= " 1";
		$having .= empty($score)?"":" AND SUM(score) > '$score'";
		$having .= empty($score_end)?"":" AND SUM(score) < '$score_end'";
		if($score_detail){
			$having .= empty($score_num)?"":" AND SUM($score_detail) > '$score_num'";
			$having .= empty($score_num_end)?"":" AND SUM($score_detail) < '$score_num_end'";
		}

		$where = "1";
		$where .= " AND quality_type = 'outbound'";
		$where .= empty($task_id)?"":" AND task_id = $task_id";
		$where .= empty($date_start)?"":" AND createtime > '$date_start'";
		$where .= empty($date_end)?"":" AND createtime < '$date_end'";


		$norm = new Model("normtype");
		$normList = $norm->select();
		foreach($normList as $vm){
			$field[] = "SUM(".$vm['en_normname'].") AS ".$vm['en_normname'];
		}
		$cols = implode(",",$field);

		$qualityscore = new Model("qualityscore");
		import('ORG.Util.Page');
		//$count = $qualityscore->group("username")->where($where)->count();

		$sql = "SELECT username,COUNT(*) as num, $cols, SUM(score) AS score FROM qualityscore where $where GROUP BY username HAVING $having";
		$arrData = $qualityscore->query($sql);

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$i = 0;
		foreach($arrData as $val){
			$arrData[$i]['cn_name'] = $cnName[$val["username"]];
			$i++;
		}

		//echo $qualityscore->getLastSql();die;

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$qualityData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrData AS &$v){
			if($i >= $start && $j < $length){
				$qualityData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		//$qualityData = $qualityscore->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$rowsList = count($qualityData) ? $qualityData : false;
		$arrquality["total"] = $count;
		$arrquality["rows"] = $rowsList;
		if($date_start){
			$arrquality["date_start"] = $date_start;
		}
		if($date_end){
			$arrquality["date_end"] = $date_end;
		}

		echo json_encode($arrquality);
	}









	function test(){
		$norm = new Model("normtype");
		$normList = $norm->select();
		$i = 0;
		foreach($normList as $vm){
			$arrField[0][$i]['field'] = $vm['en_normname'];
			$arrField[0][$i]['title'] = $vm['cn_normname'];
			$arrField[0][$i]['fitColumns'] = true;
			$i++;
		}
		unset($i);
		$arrF = array("field"=>"username","title"=>"工号","fitColumns"=>true);
		$arrF2 = array("field"=>"score","title"=>"总分","fitColumns"=>true);
		//$arrF3 = array("field"=>"id","title"=>"ID");
		//$arrField[] = array("field"=>"id","title"=>"ID");
		array_unshift($arrField[0],$arrF2);
		array_unshift($arrField[0],$arrF);
		//array_push($arrField[0],$arrF3);
		//dump($arrField);die;
		$qualityscore = new Model("qualityscore");

		$qualityData = $qualityscore->limit($page->firstRow.','.$page->listRows)->select();
		//echo $qualityscore->getLastSql();die;
		$rowsList = count($qualityData) ? $qualityData : false;
		$arrquality["total"] = $count;
		$arrquality["rows"] = $rowsList;
		$arrquality["columns"] = json_encode($arrField);
		$this->assign("field_tpl",$arrquality["columns"]);

		$this->display();
	}

	function testData(){

		$qualityscore = new Model("qualityscore");
		import('ORG.Util.Page');
		$count = $qualityscore->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$qualityData = $qualityscore->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $qualityscore->getLastSql();die;
		$rowsList = count($qualityData) ? $qualityData : false;
		$arrquality["total"] = $count;
		$arrquality["rows"] = $rowsList;

		echo json_encode($arrquality);
	}


	function qualityVisitReportList(){
		checkLogin();

		$qualityscore = new Model("qualityscore");
		$qlist = $qualityscore->select();
		foreach($qlist as $vm){
			$users[] = $vm["username"];
			$score[] = $vm["score"];
		}
		$uname = "'".implode("','",$users)."'";
		$score_tpl = implode(",",$score);
		$this->assign("uname",$uname);
		$this->assign("score_tpl",$score_tpl);
		$norm = new Model("normtype");
		$normList = $norm->select();
		$this->assign("normList",$normList);

		$i = 0;
		foreach($normList as $vm){
			$arrField[0][$i]['field'] = $vm['en_normname'];
			$arrField[0][$i]['title'] = $vm['cn_normname'];
			//$arrField[0][$i]['width'] = '60';
			$arrField[0][$i]['fitColumns'] = true;
			$i++;
		}
		unset($i);
		$arrF = array("field"=>"username","title"=>"工号","fitColumns"=>true);
		$arrF4 = array("field"=>"cn_name","title"=>"姓名","fitColumns"=>true);
		$arrF2 = array("field"=>"score","title"=>"总分","fitColumns"=>true);
		$arrF3 = array("field"=>"num","title"=>"质检次数","fitColumns"=>true);
		array_unshift($arrField[0],$arrF3);
		array_unshift($arrField[0],$arrF2);
		array_unshift($arrField[0],$arrF4);
		array_unshift($arrField[0],$arrF);
		//array_push($arrField[0],$arrF3);
		//dump($arrField);

		$qualityscore = new Model("qualityscore");
		$qualityData = $qualityscore->limit($page->firstRow.','.$page->listRows)->select();
		$rowsList = count($qualityData) ? $qualityData : false;
		$arrquality["total"] = $count;
		$arrquality["rows"] = $rowsList;
		$arrquality["columns"] = json_encode($arrField);
		$this->assign("field_tpl",$arrquality["columns"]);
		$currentDate = Date('Y-m-d H:i:s');
		$this->assign('currentDate',$currentDate);


		//分配增删改的权限
		$menuname = "Visit Quality Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}



	function qualityVisitReportData(){
		$search_type = $_REQUEST["search_type"];
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_end = Date('Y-m-d',$_GET['ts_end']) ." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){//上个月的起始时间
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1)) ." 00:00:00";
			}else{
				$date_start = Date('Y-m-d',$_GET['ts_start']) ." 00:00:00";
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}

		$score = $_GET['score'];  //总分大于
		$score_end = $_GET['score_end'];   //总分小于
		$score_detail = $_GET['score_detail'];
		$score_num = $_GET['score_num'];  //详情分数小于
		$score_num_end = $_GET['score_num_end'];  //详情分数大于
		//$date_starts = $_GET['date_start'];
		//$date_ends = $_GET['date_end'];
		//dump($task_id);

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";


		$having .= " 1";
		$having .= empty($score)?"":" AND SUM(score) > '$score'";
		$having .= empty($score_end)?"":" AND SUM(score) < '$score_end'";
		if($score_detail){
			$having .= empty($score_num)?"":" AND SUM($score_detail) > '$score_num'";
			$having .= empty($score_num_end)?"":" AND SUM($score_detail) < '$score_num_end'";
		}

		$where = "1 ";
		$where .= " AND quality_type = 'visit'";
		$where .= empty($date_start)?"":" AND createtime > '$date_start'";
		$where .= empty($date_end)?"":" AND createtime < '$date_end'";
		if($username != "admin"){
			$where .= " AND username in ($deptUser)";
		}



		$norm = new Model("normtype");
		$normList = $norm->select();
		$arrField = array("username","cn_name","score","num");
		$arrTitle = array("工号","姓名","总分","质检次数");
		foreach($normList as $vm){
			$field[] = "SUM(".$vm['en_normname'].") AS ".$vm['en_normname'];

			$arrField[] = $vm["en_normname"];
			$arrTitle[] = $vm["cn_normname"];
		}
		$cols = implode(",",$field);

		$qualityscore = new Model("qualityscore");
		import('ORG.Util.Page');
		//$count = $qualityscore->group("username")->where($where)->count();

		$sql = "SELECT username,COUNT(*) as num, $cols, SUM(score) AS score FROM qualityscore where $where GROUP BY username HAVING $having";
		$arrData = $qualityscore->query($sql);

		//echo $qualityscore->getLastSql();die;


		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];
		$i = 0;
		foreach($arrData as $val){
			$arrData[$i]['cn_name'] = $cnName[$val["username"]];
			$arrData[$i]['dept_id'] = $deptId_user[$val["username"]];
			$arrData[$i]['dept_name'] = $deptName_user[$val["username"]];
			$i++;
		}

		$username = $_SESSION["user_info"]['username'];
		$d_id = $_SESSION["user_info"]['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$dept_arr = explode(",",$deptSet);

		if($username != "admin"){
			foreach($arrData as $val){
				if( in_array($val["dept_id"],$dept_arr) ){
					$arrData_dept[] = $val;
				}
			}
			$arrData = $arrData_dept;
		}
		//dump($arrData);die;



		if($search_type == "xls"){
			$xls_count = count($arrField);
			$excelTiele = "回访质检报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}




		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$qualityData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrData AS &$v){
			if($i >= $start && $j < $length){
				$qualityData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		//$qualityData = $qualityscore->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$rowsList = count($qualityData) ? $qualityData : false;
		$arrquality["total"] = $count;
		$arrquality["rows"] = $rowsList;
		if($date_start){
			$arrquality["date_start"] = $date_start;
		}
		if($date_end){
			$arrquality["date_end"] = $date_end;
		}

		echo json_encode($arrquality);
	}





	//今日统计
	function statisticalReportsList(){
		checkLogin();
		$this->display();
	}

	function statisticalReportsData(){
		$userArr = readU();
		$agent_type = $_SESSION["user_info"]['agent_type'];
		if($agent_type == 'sell'){
			$selluser = $userArr["sell_user"];
			$workno = "'".implode("','",$selluser)."'";
		}
		if($agent_type == 'service'){
			$serviceuser = $userArr["service_user"];
			$workno = "'".implode("','",$serviceuser)."'";
		}

		//用户总数
		$users = new Model("users");
		$usersData = $users->field("username as createuser,cn_name,d_id")->select();
		foreach($usersData as $val){
			$userDataTpl[$val['createuser']] = $val;
		}

		$customer = new Model("customer");
		$date = date("Y-m-d");
		//新增客户
		$cmData = $customer->field("count(*) as add_customer_num,createuser,Date(createtime) as createtime")->where("Date(createtime) = '$date'")->group("createuser")->select();
		foreach($cmData as $key=>$val){
			$cmDataTpl[$val['createuser']] = $val;
		}


		//服务总人数
		$serviceNumData = $customer->field("count(*) as customer_count_num,createuser,createtime")->group("createuser")->select();
		foreach($serviceNumData as $key=>$val){
			$serviceNumDataTpl[$val['createuser']] = $val;
		}
		//echo $customer->getLastSql();
		//dump($serviceNumData);
		//dump($userDataTpl);die;
		/*
		//服务到期客户
		$serviceData = $customer->field("count(*) as service_daoqi_num,createuser,Date(service_expire_time) as service_expire_time")->where("Date(service_expire_time) = '$date'")->group("createuser")->select();
		foreach($serviceData as $key=>$val){
			$serviceDataTpl[$val['createuser']] = $val;
		}


		//当日退款客户
		$tkData = $customer->field("count(*) as tk_num,createuser,Date(modifytime) as modifytime")->where("Date(modifytime) = '$date'")->group("createuser")->select();
		foreach($tkData as $key=>$val){
			$tkDataTpl[$val['createuser']] = $val;
		}


		//服务总人数
		$serviceNumData = $customer->field("count(*) as customer_count_num,createuser,Date(service_expire_time) as createtime")->group("createuser")->select();
		foreach($serviceNumData as $key=>$val){
			$serviceNumDataTpl[$val['createuser']] = $val;
		}


		//服务总人数[已到期]
		$daoQiNumData = $customer->field("count(*) as customer_count_daoqi_num,createuser,Date(service_expire_time) as service_expire_time")->where("Date(service_expire_time) <= '$date'")->group("createuser")->select();
		foreach($daoQiNumData as $key=>$val){
			$daoQiDataTpl[$val['createuser']] = $val;
		}


		//服务总人数[未到期]
		$weiDaoQiNumData = $customer->field("count(*) as customer_count_weidaoqi_num,createuser,Date(service_expire_time) as service_expire_time")->where("Date(service_expire_time) > '$date'")->group("createuser")->select();
		foreach($weiDaoQiNumData as $key=>$val){
			$weiDaoQiDataTpl[$val['createuser']] = $val;
		}
		*/

		//回访工单次数
		$visit_record = new Model("visit_record");
		$where = "1 ";
		if($workno){
			$where .= "AND createname in ($workno)";
		}
		$vrData = $visit_record->field("count(*) as visit_num,Date(createtime) as createtime,createname as createuser")->where("$where AND Date(createtime) = '$date'")->group("createname")->select();
		foreach($vrData as $key=>$val){
			$vrDataTpl[$val['createuser']] = $val;
		}


		//服务工单
		$servicerecords = new Model("servicerecords");
		$srData = $servicerecords->field("count(*) as service_num,Date(createtime) as createtime,seat as createuser")->where("Date(createtime) = '$date'")->group("seat")->select();
		foreach($srData as $key=>$val){
			$srDataTpl[$val['createuser']] = $val;
		}


		//通话次数
		$cdr = new Model("asteriskcdrdb.cdr");
		$where_cdr = "1 ";
		if($workno){
			$where_cdr .= "AND workno in ($workno)";
		}
		$cdrData = $cdr->field("count(*) as cdr_num,Date(calldate) as createtime,workno as createuser")->where("$where_cdr AND Date(calldate) = '$date'")->group("workno")->select();
		foreach($cdrData as $key=>$val){
			$cdrDataTpl[$val['createuser']] = $val;
		}


		//通话总时长
		$cdrtimeData = $cdr->field("sum(billsec) as cdr_billsec,Date(calldate) as createtime,workno as createuser")->where("$where_cdr AND Date(calldate) = '$date'")->group("workno")->select();
		foreach($cdrtimeData as $key=>$val){
			$cdrtimeDataTpl[$val['createuser']] = $val;
		}



		//dump($cmDataTpl);die;
		foreach($userDataTpl as $key=>$val){   //坐席总数
			foreach($val as $k=>$v) {
			  $serviceNumDataTpl[$key][$k] = $v;   //服务总人数
			}
		}

		foreach($serviceNumDataTpl as $key=>$val){  //服务总人数
			foreach($val as $k=>$v) {
			  $cmDataTpl[$key][$k] = $v;   //新增客户
			}
		}
		/*
		foreach($cmDataTpl as $key=>$val){
			foreach($val as $k=>$v) {
			  $serviceDataTpl[$key][$k] = $v;  //服务到期客户
			}
		}

		foreach($serviceDataTpl as $key=>$val){
			foreach($val as $k=>$v) {
			  $tkDataTpl[$key][$k] = $v;  //当日退款客户
			}
		}

		foreach($tkDataTpl as $key=>$val){
			foreach($val as $k=>$v) {
			  $daoQiDataTpl[$key][$k] = $v;  //服务总人数[已到期]
			}
		}

		foreach($daoQiDataTpl as $key=>$val){
			foreach($val as $k=>$v) {
			  $weiDaoQiDataTpl[$key][$k] = $v;  //服务总人数[未到期]
			}
		}
		*/

		foreach($cmDataTpl as $key=>$val){
			foreach($val as $k=>$v) {
			  $vrDataTpl[$key][$k] = $v;  //回访工单次数
			}
		}

		//dump($vrDataTpl);die;
		foreach($vrDataTpl as $key=>$val){
			foreach($val as $k=>$v) {
			  $srDataTpl[$key][$k] = $v;  //服务工单
			}
		}

		foreach($srDataTpl as $key=>$val){
			foreach($val as $k=>$v) {
			  $cdrDataTpl[$key][$k] = $v;  //通话次数
			}
		}

		foreach($cdrDataTpl as $key=>$val){
			foreach($val as $k=>$v) {
			  $cdrtimeDataTpl[$key][$k] = $v;  //通话总时长
			}
		}

		foreach($cdrtimeDataTpl as $val){

			$statisticalArr[] = $val;
		}

		$i = 0;
		foreach($statisticalArr as $val){
			if (!array_key_exists('add_customer_num', $statisticalArr[$i])) {  //新增客户
				$statisticalArr[$i]['add_customer_num'] = '0';
			}
			/*
			if (!array_key_exists('service_daoqi_num', $statisticalArr[$i])) {  //服务到期客户
				$statisticalArr[$i]['service_daoqi_num'] = '0';
			}

			if (!array_key_exists('tk_num', $statisticalArr[$i])) {  //当日退款客户
				$statisticalArr[$i]['tk_num'] = '0';
			}
			*/
			if (!array_key_exists('customer_count_num', $statisticalArr[$i])) {  //服务总人数
				$statisticalArr[$i]['customer_count_num'] = '0';
			}
			/*
			if (!array_key_exists('customer_count_daoqi_num', $statisticalArr[$i])) {  //服务总人数[已到期]
				$statisticalArr[$i]['customer_count_daoqi_num'] = '0';
			}

			if (!array_key_exists('customer_count_weidaoqi_num', $statisticalArr[$i])) {  //服务总人数[未到期]
				$statisticalArr[$i]['customer_count_weidaoqi_num'] = '0';
			}
			*/
			if (!array_key_exists('visit_num', $statisticalArr[$i])) {  //回访工单次数
				$statisticalArr[$i]['visit_num'] = '0';
			}

			if (!array_key_exists('service_num', $statisticalArr[$i])) {  //服务工单
				$statisticalArr[$i]['service_num'] = '0';
			}

			if (!array_key_exists('cdr_num', $statisticalArr[$i])) {  //通话次数
				$statisticalArr[$i]['cdr_num'] = '0';
			}

			if (!array_key_exists('cdr_billsec', $statisticalArr[$i])) {  //通话总时长
				$statisticalArr[$i]['cdr_billsec'] = '00:00:00';
			}

			$i++;
		}
		unset($i);




		//过滤---只显示客服部的工号记录----开始
		/*
		if($agent_type == 'sell'){
			$user_name = $userArr["sell_user"];
		}
		if($agent_type == 'service'){
			$user_name = $userArr["service_user"];
		}
		if($agent_type == 'admin'){
			$user_name = $userArr["service_user"];
		}
		*/
		$user_name = $userArr["service_user"];

		$cnName = $userArr["cn_user"];
		$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];
		$i = 0;
		foreach($statisticalArr as $val){
			$statisticalArr[$i]['cn_name'] = $cnName[$val["createuser"]];
			$statisticalArr[$i]['dept_id'] = $deptId_user[$val["createuser"]];
			$statisticalArr[$i]['dept_name'] = $deptName_user[$val["createuser"]];
			$i++;
		}

		if($user_name){
			//$j = 0;
			foreach($statisticalArr as $val){
				if( in_array($val["createuser"],$user_name) ){
					$statisticalArr2[] = $val;
				}
			}
		}else{
			$statisticalArr2 = $statisticalArr;
		}
		//dump($statisticalArr2);
		//dump($statisticalArr);die;
		//过滤---只显示客服部的工号记录----结束



		$d_id = $_SESSION["user_info"]['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$dept_arr = explode(",",$deptSet);

		$username = $_SESSION["user_info"]["username"];

		//载入页面时只显示自己部门及下级部门的数据------开始
		if($username != "admin"){
			foreach($statisticalArr2 as $val){
				if( in_array($val["dept_id"],$dept_arr) ){
					$statisticalArr_dept[] = $val;
				}
			}
			$statisticalArr2 = $statisticalArr_dept;
		}
		//dump($statisticalArr2);die;
		//载入页面时只显示自己部门及下级部门的数据------结束


		//搜索部门时 显示该部门及下级部门的数据------开始
		if($username != "admin"){
			$deptId = $_REQUEST["deptId"];
			if($deptId){
				$deptst2 = $this->getMeAndSubDeptName($arrDep,$deptId);
				$deptSet2 = rtrim($deptst2,",");
				$dept_arr2 = explode(",",$deptSet2);

				foreach($statisticalArr2 as $val){
					if( in_array($val["dept_id"],$dept_arr2) ){
						$statisticalArr_dept_search[] = $val;
					}
				}
				$statisticalArr2 = $statisticalArr_dept_search;
			}
		}
		//dump($dept_arr2);
		//dump($statisticalArr2);die;
		//搜索部门时 显示该部门及下级部门的数据------结束


		//搜索工号---开始
		$create_username = $_REQUEST["createuser"];
		if($create_username){
			foreach($statisticalArr2 as $val){
				if($val["createuser"] == $create_username){
					$statisticalArr3[] = $val;
				}
			}
			$statisticalArr2 = $statisticalArr3;
		}
		//搜索工号---结束


		//dump($statisticalArr3);die;
		foreach($statisticalArr2 as &$val){
			$add_customer_num_arr[] = $val['add_customer_num'];
			$service_daoqi_num_arr[] = $val['service_daoqi_num'];
			$tk_num_arr[] = $val['tk_num'];
			$customer_count_num_arr[] = $val['customer_count_num'];
			$customer_count_daoqi_num_arr[] = $val['customer_count_daoqi_num'];
			$customer_count_weidaoqi_num_arr[] = $val['customer_count_weidaoqi_num'];
			$visit_num_arr[] = $val['visit_num'];
			$service_num_arr[] = $val['service_num'];
			$cdr_num_arr[] = $val['cdr_num'];
			$cdr_billsec_arr[] = $val['cdr_billsec'];

			$val["cdr_billsec"] = sprintf("%02d",intval($val["cdr_billsec"]/3600)).":".sprintf("%02d",intval(($val["cdr_billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[cdr_billsec]%3600)%60)));
		}
		//dump($cdr_billsec_arr);die;
		$add_customer_num = array_sum($add_customer_num_arr);
		//$service_daoqi_num = array_sum($service_daoqi_num_arr);
		//$tk_num = array_sum($tk_num_arr);
		$customer_count_num = array_sum($customer_count_num_arr);
		//$customer_count_daoqi_num = array_sum($customer_count_daoqi_num_arr);
		//$customer_count_weidaoqi_num = array_sum($customer_count_weidaoqi_num_arr);
		$visit_num = array_sum($visit_num_arr);
		$service_num = array_sum($service_num_arr);
		$cdr_num = array_sum($cdr_num_arr);
		$cdr_billsec = array_sum($cdr_billsec_arr);
		$cdr_billsec_tpl = sprintf("%02d",intval($cdr_billsec/3600)).":".sprintf("%02d",intval(($cdr_billsec%3600)/60)).":".sprintf("%02d",intval((($cdr_billsec%3600)%60)));
		$tmp_mon = array(array(
						"createuser"=>"总计",
						"add_customer_num"=>$add_customer_num,
						//"service_daoqi_num"=>$service_daoqi_num,
						//"tk_num"=>$tk_num,
						"customer_count_num"=>$customer_count_num,
						//"customer_count_daoqi_num"=>$customer_count_daoqi_num,
						//"customer_count_weidaoqi_num"=>$customer_count_weidaoqi_num,
						"visit_num"=>$visit_num,
						"service_num"=>$service_num,
						"cdr_num"=>$cdr_num,
						"cdr_billsec"=>$cdr_billsec_tpl,
					));
		//dump($statisticalArr);die;


		$statisticalArr_sort = $this->array_sort($statisticalArr2,'createuser','asc','no');
		$statisticalArr2 = $statisticalArr_sort;

		//分页
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($statisticalArr2);
		$page = BG_Page($count,$page_rows);
		//$page = BG_Page($count,300);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$statisticalDataArr = Array(); //转换成显示的
		$i = $j = 0;
		foreach($statisticalArr2 AS &$v){
			if($i >= $start && $j < $length){
				$statisticalDataArr[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}
		$rowsList = count($statisticalDataArr) ? $statisticalDataArr : false;

		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["footer"] = $tmp_mon;
		echo json_encode($ary);

		/*
		//部分页----不分页如果搜出的数据为空，则表格中的数据不变
		$ary["total"] = count($statisticalArr2);
		$ary["rows"] = $statisticalArr2;
		$ary["footer"] = $tmp_mon;
		echo json_encode($ary);*/
	}



	/*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		//$str = "'" . $arrDep[$dept_id]['id'] . "',";
		$str =  $arrDep[$dept_id]['id'] . ",";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}


	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

}

?>
