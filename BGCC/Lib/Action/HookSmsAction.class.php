<?php
class HookSmsAction extends Action{
	function hookSmsList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "hangup SMS Setting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function hookSmsData(){
		$hangupsms = new Model("hangupsms");
		$count = $hangupsms->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$hookData = $hangupsms->limit($page->firstRow.','.$page->listRows)->select();
		$status_row = array('ON'=>'开','OFF'=>'关');
		foreach($hookData as &$val){
			$status = $status_row[$val['status']];
			$val['status'] = $status;
		}

		$rowsList = count($hookData) ? $hookData : false;
		$arrHook["total"] = $count;
		$arrHook["rows"] = $rowsList;

		echo json_encode($arrHook);
	}
	function addHookSms(){
		checkLogin();
		$this->display();
	}

	function insertHookSms(){
		$status = isset($_REQUEST['in_enabled']) ? "ON" : "OFF";
		$hangupsms = new Model("hangupsms");
		$count = $hangupsms->where(" exten = ".$_REQUEST['exten'])->count();
		if($count){
			echo json_encode(array("msg"=>"该用户已经设置了挂机短信！"));
			exit;
		}
		$arrData = array(
			"exten"=>$_REQUEST['exten'],
			"status"=>$status,
			"msg"=>$_REQUEST['content'],
		);
		$result = $hangupsms->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function editHookSms(){
		checkLogin();
		$id = $_REQUEST["id"];
		$hangupsms = new Model("hangupsms");
		$hkData = $hangupsms->where("id = '$id'")->find();
		$this->assign("id",$id);
		$this->assign("hkData",$hkData);
		$this->display();
	}

	function updateHookSms(){
		$id = $_REQUEST["id"];
		$status = isset($_REQUEST['in_enabled']) ? "ON" : "OFF";
		$hangupsms = new Model("hangupsms");
		$arrData = array(
			"exten"=>$_REQUEST['exten'],
			"status"=>$status,
			"msg"=>$_REQUEST['content'],
		);
		$result = $hangupsms->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteHookSms(){
		$id = $_REQUEST["id"];
		$hangupsms = new Model("hangupsms");
		$result = $hangupsms->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function allSetSmsStatus(){
		$status = isset($_REQUEST['in_enabled']) ? "ON" : "OFF";
		$hangupsms = new Model("hangupsms");
		$arrData = array(
			"status"=>$status,
			"msg"=>$_REQUEST['content'],
		);
		if($_REQUEST['content']){
			$sql = "update hangupsms set status = '$status',msg = '".$_REQUEST['content']."'";
		}else{
			$sql = "update hangupsms set status = '$status'";
		}
		$result = $hangupsms->query($sql);
		//echo $hangupsms->getLastSql();die;
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function checkSetSmsStatus(){
		$id = $_REQUEST["id"];
		$arrId = explode(",",$id);
		$count = count($arrId);
		$status = isset($_REQUEST['in_enabled']) ? "ON" : "OFF";
		$hangupsms = new Model("hangupsms");
		$arrData = array(
			"status"=>$status,
			"msg"=>$_REQUEST['content'],
		);

		if(!$_REQUEST['content']){
			array_pop($arrData);
		}
		for($i=0;$i<$count;$i++){
			$result[$i] = $hangupsms->data($arrData)->where("id = $arrId[$i]")->save();
		}
		if ($result[0] !== false){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

}
?>
