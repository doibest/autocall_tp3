<?php
class RecordingsAction extends Action{
	function recordings(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Recordings";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		//找出系统的所有路由
		$or = M('asterisk.outbound_routes');
		mysql_query("set names latin1");
		$routes = $or->field("route_id,name")->order("route_id ASC")->select();
		$this->assign('routes',$routes);
		$this->display();
	}

	function recordingsData(){
		$d_id = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$where = "1 ";
		if($username != "admin"){
			$where .= " AND dept_id IN ($deptSet)";
		}

		$recordings = new Model("asterisk.recordings");
		$count = $recordings->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$recordingsData = $recordings->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//$recordingsData = $recordings->table("asterisk.recordings r")->field("r.id,r.displayname,r.description,r.dept_id,d.d_name")->join("bgcrm.department d on r.dept_id = d.d_id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$users = readU();
		$deptId_name = $users["deptId_name"];
		$deptId_name["-1"] = "所有部门";
		foreach($recordingsData as &$val){
			$val["dept_name"] = $deptId_name[$val["dept_id"]];
			$path = "/var/lib/asterisk/sounds/".$val["filename"].".wav";
			if(!$val["file_path"]){
				$val["file_path"] = $path;
			}
			$val["operations"] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["file_path"])."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadSystem&file_path=" .trim($val["file_path"])."'> 下载 </a>" ;
		}
		//dump($recordingsData);die;

		$rowsList = count($recordingsData) ? $recordingsData : false;
		$arrrecordings["total"] = $count;
		$arrrecordings["rows"] = $rowsList;

		echo json_encode($arrrecordings);
	}

	function insertRecordings(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize ='1000000';
		$filePath = "/var/lib/asterisk/sounds/custom/";
		$upload->savePath= $filePath;
		//$upload->saveRule = array_shift(explode(".",$_FILES["recordings_name"]["name"]));
		//$upload->saveRule=uniqid;
		if($_REQUEST["displayname"]){
			$displayname = $_REQUEST["displayname"];
		}else{
			$displayname = array_shift(explode(".",$_FILES["recordings_name"]["name"]));
		}

		$recordings = new Model("asterisk.recordings");
		$count = $recordings->where("displayname = '$displayname'")->count();
		if($count>0){
			echo json_encode(array('msg'=>"此录音文件名已存在！"));
			die;
		}
		$upload->saveRule = $displayname;
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('mp3','wav');
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			echo json_encode(array('msg'=>$mess));
		}else{
			$info=$upload->getUploadFileInfo();
			//dump($info);die;

			$suffix = array_pop(explode(".",$_FILES["recordings_name"]["name"]));
			if($suffix == "mp3"){
				$wav_name = array_shift(explode(".",$info[0]["savename"])).".wav";
			}else{
				$wav_name = $info[0]["savename"];
			}

			$arrData = Array(
			  'displayname' => $displayname,
			  'filename' => "custom/".$displayname,
			  'file_path' => $info[0]["savepath"].$wav_name,
			  'description' => $_REQUEST["description"],
			  'dept_id' => $_REQUEST["dept_id"],
			);
			mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
			$result = $recordings->data($arrData)->add();
			if ($result){

				$filename = $filePath.$displayname;
				$filename_source = $filePath.$displayname."_source";
				if($suffix == "mp3"){
					$cmd = "lame --decode $filename.mp3 $filename_source.wav && sox -G -v 1.0 $filename_source.wav -r 8000 -b 16 -c 1 $filename.wav && chmod 777 $filename.wav && rm -rf $filename.mp3 $filename_source.wav";
					exec($cmd,$retstr,$retno);
				}elseif($suffix == "wav"){
					$cmd = "file $filename";
					$arr = exec($cmd,$retstr,$retno);
					$str = strpos($arr,"WAVE audio, Microsoft PCM, 16 bit, mono 8000 Hz");
					if(!$str){
						//$cmd = "sox $filename.wav -r 8000 -b 16 -c 1 $filename.wav && chmod 777 $filename.wav";
						$cmd = "sox $filename.wav -t raw - | sox -t raw -e signed -b 16 -c 1 -r 8000 - $filename_source.wav && rm -rf $filename.wav && mv  $filename_source.wav $filename.wav &&  chmod 777 $filename.wav";
						exec($cmd,$retstr,$retno);
					}

				}

				echo json_encode(array('success'=>true,'msg'=>'录音文件上传成功!'));
			} else {
				$mess = $upload->getErrorMsg();
				echo json_encode(array('msg'=>$mess));
			}
		}
	}

	function updateRecordings(){
		$id = $_REQUEST["id"];
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize ='1000000';
		$filePath = "/var/lib/asterisk/sounds/custom/";
		$upload->savePath= $filePath;
		if($_REQUEST["displayname"]){
			$displayname = $_REQUEST["displayname"];
		}else{
			$displayname = array_shift(explode(".",$_FILES["recordings_name"]["name"]));
		}
		$upload->saveRule = $displayname;
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('mp3','wav');
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$recordings = new Model("asterisk.recordings");
				$arrD = $recordings->where("id = '$id'")->find();
				$filename = $arrD["displayname"].".wav";
				$name = $displayname.".wav";
				$arrData = Array(
						'displayname' => $displayname,
						'filename' => "custom/".$displayname,
						'description' => $_REQUEST["description"],
						'dept_id' => $_REQUEST["dept_id"],
				);
				mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
				$result = $recordings->data($arrData)->where("id = '$id'")->save();
				if ($result !== false){
					if($arrD["displayname"] != $_REQUEST["displayname"]){
						$cmd = "cd /var/lib/asterisk/sounds/custom/;mv $filename $name";
						//exec("cd /var/lib/asterisk/sounds/custom/;mv $filename $name");
						exec($cmd,$retstr,$retno);
					}
					echo json_encode(array('success'=>true,'msg'=>'更新成功！!'));
				} else {
					$mess = $upload->getErrorMsg();
					echo json_encode(array('msg'=>"更新失败！".$mess));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			//dump($info);
			$suffix = array_pop(explode(".",$_FILES["recordings_name"]["name"]));
			if($suffix == "mp3"){
				$wav_name = array_shift(explode(".",$info[0]["savename"])).".wav";
			}else{
				$wav_name = $info[0]["savename"];
			}

			$recordings = new Model("asterisk.recordings");
			$arrD = $recordings->where("id = '$id'")->find();
			$file_name = $arrD["displayname"].".wav";
			$arrData = Array(
			  'displayname' => $displayname,
			  'filename' => "custom/".$displayname,
			  'file_path' => $info[0]["savepath"].$wav_name,
			  'description' => $_REQUEST["description"],
			  'dept_id' => $_REQUEST["dept_id"],
			);
			mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
			$result = $recordings->data($arrData)->where("id = '$id'")->save();
			if ($result !== false){
				//unlink("var/lib/asterisk/sounds/custom/$filename");
				$name = $arrD["displayname"].".wav";
				if($name != $wav_name){
					unlink($arrD["file_path"]);
				}
				$filename = $filePath.$displayname;
				$filename_source = $filePath.$displayname."_source";
				if($suffix == "mp3"){
					$cmd = "lame --decode $filename.mp3 $filename_source.wav && sox -G -v 1.0 $filename_source.wav -r 8000 -b 16 -c 1 $filename.wav && chmod 777 $filename.wav && rm -rf $filename.mp3 $filename_source.wav";
					exec($cmd,$retstr,$retno);
				}elseif($suffix == "wav"){
					$cmd = "file $filename";
					$arr = exec($cmd,$retstr,$retno);
					$str = strpos($arr,"WAVE audio, Microsoft PCM, 16 bit, mono 8000 Hz");
					if(!$str){
						//$cmd = "sox $filename.wav -r 8000 -b 16 -c 1 $filename.wav && chmod 777 $filename.wav";
						$cmd = "sox $filename.wav -t raw - | sox -t raw -e signed -b 16 -c 1 -r 8000 - $filename_source.wav && rm -rf $filename.wav && mv  $filename_source.wav $filename.wav &&  chmod 777 $filename.wav";
						exec($cmd,$retstr,$retno);
					}

				}

				echo json_encode(array('success'=>true,'msg'=>'录音文件上传成功!'));
			} else {
				$mess = $upload->getErrorMsg();
				echo json_encode(array('msg'=>$mess));
			}
		}
	}

	function deleteRecordings(){
		$id = $_REQUEST["id"];
		$recordings = new Model("asterisk.recordings");
		$arrD = $recordings->where("id = '$id'")->find();
		$filename = $arrD["displayname"].".wav";
		$result = $recordings->where("id in ($id)")->delete();
		if ($result){
			exec("cd /var/lib/asterisk/sounds/custom/;rm -rf $filename");
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }

}
?>
