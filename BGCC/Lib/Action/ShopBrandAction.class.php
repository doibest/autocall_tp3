<?php
//商品品牌
class ShopBrandAction extends Action{
	function brandList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Brand";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$this->display();
	}

	function brandData(){
		$brand = new Model("shop_brand");
		import('ORG.Util.Page');
		$count = $brand->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		//$brandData = $brand->order("brand_order asc")->field("id,brand_name,brand_url,brand_logo,brand_description,brand_order,brand_enabled")->limit($page->firstRow.','.$page->listRows)->select();

		$brandData = $brand->order("brand_order asc")->limit($page->firstRow.','.$page->listRows)->select();

		$row = array('Y'=>'是','N'=>'否');
		foreach($brandData as &$val){
			$type = $row[$val['brand_enabled']];
			$val['brand_enabled'] = $type;
		}

		$rowsList = count($brandData) ? $brandData : false;
		$arrBrand["total"] = $count;
		$arrBrand["rows"] = $rowsList;
		//dump($brandData);die;
		echo json_encode($arrBrand);
	}

	function addBrand(){
		checkLogin();
		$this->display();
	}

	function insertBrand(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize ='1000000';
		$path = "include/images/shop/brand/";
		$this->mkdirs($path);
		$upload->savePath= $path;  //上传路径

		$upload->saveRule=uniqid;    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('jpg','jpeg','png','gif');    //准许上传的文件后缀
		$upload->allowTypes=array('image/png','image/jpg','image/pjpeg','image/gif','image/jpeg');  //检测mime类型

		$brand = new Model("shop_brand");
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = Array(
				  'brand_name' => $_REQUEST['brand_name'],
				  'brand_url' => $_REQUEST['brand_url'],
				  'brand_order' => $_REQUEST['brand_order'],
				  'brand_enabled' => $_REQUEST['brand_enabled'],
				  'brand_description' => $_REQUEST['brand_description'],
				);
				$result = $brand->data($arrData)->add();
				if ($result){
					echo json_encode(array('success'=>true,'msg'=>'品牌添加成功！'));
				} else {
					echo json_encode(array('msg'=>'品牌添加失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess."<br>  只支持jpg、png、gif格式的图片!"));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData = Array(
			  'logo_path' => $info[0]["savepath"],
			  'brand_logo' => $info[0]["savename"],
			  'brand_name' => $_REQUEST['brand_name'],
			  'brand_url' => $_REQUEST['brand_url'],
			  'brand_order' => $_REQUEST['brand_order'],
			  'brand_enabled' => $_REQUEST['brand_enabled'],
			  'brand_description' => $_REQUEST['brand_description'],
			);
			$result = $brand->data($arrData)->add();
			if ($result){
				echo json_encode(array('success'=>true,'msg'=>'品牌添加成功！'));
			} else {
				echo json_encode(array('msg'=>'品牌添加失败！'));
			}
		}
	}

	function editBrand(){
		checkLogin();
		$id = $_REQUEST['id'];
		$brand = new Model("shop_brand");
		$bData = $brand->where("id = $id")->find();
		$this->assign("id",$id);
		$this->assign("bData",$bData);
		$this->display();
	}

	function updateBrand(){
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize ='1000000';
		$path = "include/images/shop/brand/";
		$this->mkdirs($path);
		$upload->savePath= $path;  //上传路径

		$upload->saveRule=uniqid;    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('jpg','jpeg','png','gif');    //准许上传的文件后缀
		$upload->allowTypes=array('image/png','image/jpg','image/pjpeg','image/gif','image/jpeg');  //检测mime类型

		$id = $_REQUEST["id"];
		$brand = new Model("shop_brand");
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = Array(
				  'brand_name' => $_REQUEST['brand_name'],
				  'brand_url' => $_REQUEST['brand_url'],
				  'brand_order' => $_REQUEST['brand_order'],
				  'brand_enabled' => $_REQUEST['brand_enabled'],
				  'brand_description' => $_REQUEST['brand_description'],
				);
				$result = $brand->data($arrData)->where("id = $id")->save();
				if ($result !== false){
					echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
				} else {
					echo json_encode(array('msg'=>'更新失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess."<br>  只支持jpg、png、gif格式的图片!"));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData = Array(
			  'logo_path' => $info[0]["savepath"],
			  'brand_logo' => $info[0]["savename"],
			  'brand_name' => $_REQUEST['brand_name'],
			  'brand_url' => $_REQUEST['brand_url'],
			  'brand_order' => $_REQUEST['brand_order'],
			  'brand_enabled' => $_REQUEST['brand_enabled'],
			  'brand_description' => $_REQUEST['brand_description'],
			);
			$res = $brand->where("id = '$id'")->find();
			$name = $res['logo_path'].$res['brand_logo'];
			if($name){
				unlink($name);
			}
			$result = $brand->data($arrData)->where("id = $id")->save();
			if ($result !== false){
				echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}
	}

	function deleteBrand(){
		$id = $_REQUEST["id"];
		$brand = new Model("shop_brand");
		$res = $brand->where("id = '$id'")->find();
		$name = $res['logo_path'].$res['brand_logo'];
		if($name){
			unlink($name);
		}
		$result = $brand->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}
	}


	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}
}

?>
