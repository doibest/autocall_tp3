<?php
class TemplateSettingAction extends Action{
	//邮件模版设置
	function listTemplateSetting(){
		checkLogin();
		$faxMail = new Model("fax_mail");
		$listMail = $faxMail->where("id=1")->select();
		$this->assign("listMail",$listMail);

		//分配增删改的权限
		$menuname = "Template Setting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function editTemplate(){
		checkLogin();
		$id = $_GET['id'];
		if($id){
			$this->assign("id",$id);
			$faxMail = new Model("fax_mail");
			$editTemp = $faxMail->where("id=$id")->find();
			$this->assign('udata',$editTemp);
		}
		$this->display();
	}

	function updateTemplate(){
		$id = $_POST['id'];
		$faxMail = new Model("fax_mail");
		if($_POST['remite']){
			$arrData = Array(
				'remite'=>$_POST['remite'],
				'content'=>$_POST['content'],
				'remitente'=>$_POST['remitente'],
				'subject'=>$_POST['subject'],
			);
			$result = $faxMail->data($arrData)->where("id=$id")->save();
			if ($result !== false){
				echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}

	}
	function newBackup(){
		checkLogin();
		//echo C("DB_NAME");die;
		$this->display();
	}
}
?>

