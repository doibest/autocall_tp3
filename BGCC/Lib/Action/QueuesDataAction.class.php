<?php
class QueuesDataAction extends Action{
	function index(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Queues";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function queuesDataList(){
		$ques = new Model("asterisk.queues_config");
		$count = $ques->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$queuesData = $ques->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($queuesData) ? $queuesData : false;
		$arrQueues["total"] = $count;
		$arrQueues["rows"] = $rowsList;

		echo json_encode($arrQueues);
	}

	function addQueues(){
		checkLogin();
		include_once ("/var/www/html/admin.php");
		$content = getContent($this ,"exteinsion",false);
		//echo drawselects($goto,0);
		//exit;
		//dump($content["headerNuevo"]);die;
		$this->assign("header",$content);
		$this->display();
	}

	function editQueues(){
		checkLogin();
		include_once ("/var/www/html/admin.php");
		$content = getContent($this ,"exteinsion",false);
		$this->assign("header",$content);

		$extdisplay = $_GET['extdisplay'];
		GLOBAL $db;

		$sql = "SELECT * FROM asterisk.queues_config WHERE extension = '$extdisplay' ";
		//$quesData = $db->getAll($sql,DB_FETCHMODE_ASSOC);
		//$quesData = $db->getAssoc($sql,DB_FETCHMODE_ASSOC);
		$quesData = $db->getRow($sql,DB_FETCHMODE_ASSOC);

		$sql2 = "SELECT * FROM asterisk.queues_details WHERE id = '$extdisplay' ";
		$qData = $db->getAll($sql2,DB_FETCHMODE_ASSOC);

		$sql3 = "SELECT * FROM asterisk.queues_config WHERE extension = '$extdisplay' ";
		$sqData = $db->getRow($sql3,DB_FETCHMODE_ASSOC);
		$guide = $sqData['dest'];
		$this->assign("guide",$guide);
		//dump($guide);
		foreach($qData as $key=>$val){
			$qdetails[$val["keyword"]]	= $val["data"];
		}

		//静态坐席
		$member = array();
		$thisQ = queues_get($extdisplay);
		extract($thisQ);
		$mem_array = array();
		foreach ($member as $mem) {
			if (preg_match("/^(Local|Agent|SIP|DAHDI|ZAP|IAX2)\/([\d]+).*,([\d]+)$/",$mem,$matches)) {
				switch ($matches[1]) {
					case 'Agent':
					  $exten_prefix = 'A';
					  break;
					case 'SIP':
					  $exten_prefix = 'S';
					  break;
					case 'IAX2':
					  $exten_prefix = 'X';
					  break;
					case 'ZAP':
					  $exten_prefix = 'Z';
					  break;
					case 'DAHDI':
					  $exten_prefix = 'D';
					  break;
					case 'Local':
					  $exten_prefix = '';
					  break;
				}
				$mem_array[] = $exten_prefix.$matches[2].','.$matches[3];
			}
		}
		$tpl_mem = implode("\n",$mem_array);
		//dump($qdetails);die;
		$this->assign("quesData",$quesData);
		$this->assign("qdetails",$qdetails);
		$this->assign("extdisplay",$extdisplay);
		$this->assign("tpl_mem",$tpl_mem);

		$this->display();
	}
}

?>
