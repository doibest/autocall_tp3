<?php
class DailyManagementAction extends Action{
	//后台日报列表
	function dailyList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Daily List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$result = $this->dailyDataList();
		$this->assign('result',$result);

		$this->display();
	}

	//后台日报收藏列表
	function collectList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Collect Daily";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$result = $this->dailyDataList("Y");
		$this->assign('result',$result);

		$this->display("dailyList");
	}

	function dailyDataList($collect){
		$daily_info = M("daily_info");
		if($collect){
			$where = "collect = 'Y'";
		}else{
			$where = " 1";
		}

		import('ORG.Util.Page');

		$count      = $daily_info->where($where)->count();
		$Page       = new Page($count,10);
		$show       = $Page->show();
		$arrData = $daily_info->where($where)->order("createtime desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		$arr_collect = array("Y"=>"fa-star","N"=>"fa-star-o");
		foreach($arrData as &$val){
			$val["daily_content"] = mb_substr($val["daily_content"],0,10,'utf-8');
			$val["collect"] = $arr_collect[$val["collect"]];
		}

		$result = array("arrData"=>$arrData,"show"=>$show,"collect"=>$collect);
		return	$result;
	}

	//删除日报
	function deleteDaily(){
		$daily_info = M("daily_info");
		$result = $daily_info->where("id in(".$_REQUEST["idstr"].")")->delete();
		if($result){
			//$this->dailyList();
			echo json_encode(array('success'=>true,'msg'=>'删除成功！'));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}
	}

	//收藏日报
	function collectDaily(){
		$daily_info = M("daily_info");
		$collect = $daily_info->field("collect")->where("id =".$_REQUEST["id"])->find();
		if($collect["collect"]=="Y"){
			$data["collect"] = "N";
		}else{
			$data["collect"] = "Y";
		}
		$result = $daily_info->where("id =".$_REQUEST["id"])->save($data);
		if($result){
			echo json_encode(array('success'=>true,'msg'=>'收藏成功！'));
		} else {
			echo json_encode(array('msg'=>'收藏失败！'));
		}
	}

	//查看日报
	function readDaily(){
		$id = $_GET["id"];
		$collect = $_GET["collect"];
		$daily_info = M("daily_info");
		$result = $daily_info->where("id=$id")->find();
		$result["attachment"] = explode("|",$result["attachment"]);


		$this->assign('result',$result);
		$this->assign('id',$id);
		$this->assign('collect',$collect);
		$this->display();
	}

	//打印日报
	function dailyPrint(){
		$id = $_GET["id"];
		$daily_info = M("daily_info");
		$result = $daily_info->where("id=$id")->find();

		$this->assign('result',$result);
		$this->display();
	}



	//坐席界面日报动态
	function dailyTrends(){
		checkLogin();
		$username = $_SESSION["user_info"]["username"];
		$arrData = $this->dailyTrendsData();
		//print_r($arrData);die;
		$users = M("users");
		$arrUsers = $users->field("username,cn_name")->select();
		//$address = $this->getCity('223.68.186.228');

		$user_info = $users->table("users u")->join("department d on d.d_id = u.d_id")->join("role r on r.r_id = u.r_id")->field("u.*,d.d_name,r.r_name")->where("username = '$username'")->find();

		$this->assign("user_info",$user_info);

		if($user_info["user_img"]){
			$this->assign('user_img',"include/data/headImg/".$user_info["user_img"]);
		}else{
			$this->assign('user_img',"Agent/Tpl/public/html5/img/no-user.jpg");
		}

		$daily_info = M("daily_info");
		$count      = $daily_info->where("createname = '$username'")->count();
		//$timeLine = $this->timeLine();

		$this->assign('count',$count);
		$this->assign('arrUsers',$arrUsers);
		$this->assign('list',$arrData["result"]);
		$this->assign('page',$arrData["show"]);
		$this->assign('username',$username);
		$this->display('dailyTrends');
	}

	function getCity($ip)
	{
	   $url="http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
	   print_r(file_get_contents($url));
	   $ipinfo=json_decode(file_get_contents($url));
	   if($ipinfo->code=='1'){
		   return false;
	   }
	   $city = $ipinfo->data->region.$ipinfo->data->city;
	   return $city;
	}

	function getCategoryTree($daily_id, $pid = 0, $level = 0){
		$daily_comment = M("daily_comment");
		$data = $daily_comment->where("daily_id = $daily_id AND pid=$pid")->order("createtime asc")->select();

		//目录树
		$tree = array();
		//层级
		$level++;
		if(!empty($data)){
			foreach ($data as &$v){
				$child = $this->getCategoryTree($daily_id,$v['id'], $level);
				$tree[] = array('parent' => $v, 'child' => $child, 'level' => $level);
			}
		}
		return $tree;
	}

	//日报动态数据处理
	function dailyTrendsData(){
		$username = $_SESSION["user_info"]["username"];
		$daily_info = M("daily_info d");
		if($username == "admin"){
			$where = " 1";
		}else{
			$where = "find_in_set('$username', d.view_user) or d.recipients='$username' or d.createname='$username'";
			//$where = "view_type=1 OR (view_type=2 and find_in_set($username, view_user)) OR (view_type=3 and !find_in_set($username, view_user))";
		}
		import('ORG.Util.Page');

		$count      = $daily_info->join("users u on u.username = d.createname")->where($where)->count();
		$Page       = new Page($count,10);
		$show       = $Page->show();

		$arrData = $daily_info->join("users u on u.username = d.createname")->field("d.*,u.user_img")->where($where)->order("d.createtime desc")->limit($Page->firstRow.','.$Page->listRows)->select();
		//echo $daily_info->getlastsql();die;
		$status_arr =array("1"=>"<font color='green'>审核通过</font>","2"=>"<font color='orange'>待审核</font>","3"=>"<font color='red'>未通过</font>");
		foreach($arrData as &$val){
			$val["comment"] = $this->getCategoryTree($val["id"]);
			$val["audit_status"] = $status_arr[$val["audit_status"]];
			if($val["user_img"]){
				$val["user_img"] = "include/data/headImg/".$val["user_img"];
			}else{
				$val["user_img"] = "Agent/Tpl/public/html5/img/no-user.jpg";
			}
			$val["attachment"] = explode("|",$val["attachment"]);

		}
		$result = array("result"=>$arrData,"show"=>$show);
		// print_r($result);die;
		return	$result;
	}

	//时间轴
	function timeLine(){
		$timeLine = $this->timeLineData();

		$this->assign('timeLine',$timeLine);
		$this->display();
	}

	function timeLineData(){
		$username = $_SESSION["user_info"]["username"];
		$daily_info = M("daily_info");
		import('ORG.Util.Page');

		$count      = $daily_info->where("createname = '$username'")->count();
		$Page       = new Page($count,10);
		$show       = $Page->show();
		$arrData = $daily_info->where("createname = '$username'")->order("createtime desc")->limit($Page->firstRow.','.$Page->listRows)->select();

		$result = array("result"=>$arrData,"show"=>$show);
		return	$result;
	}

	//发表日报
	function insertDaily(){
		//print_r($_FILES["attachment"]["name"]);die;
		$username = $_SESSION["user_info"]["username"];
		$data["daily_content"] = $_REQUEST["daily_content"];
		$data["daily_title"] = $_REQUEST["daily_title"];
		$data["view_user"] = $_REQUEST["view_user"];
		$data["recipients"] = $_REQUEST["daily_cc"];
		$data["createname"] = $_SESSION["user_info"]["username"];
		$data["createtime"] = date("Y-m-d H:i:s");
		if(!empty($_FILES["attachment"]["name"][0])){
			$res = $this->upload('daily');
			for($i=0;$i<count($res);$i++){
                $files[] = $res[$i]['savename'];
            }
			$files_str = implode("|",$files);
			$data["attachment"] = $files_str;
		}
		$daily_info = M("daily_info");
		$result = $daily_info->add($data);
		if($result){
			echo json_encode(array('success'=>true,'msg'=>'发表成功！'));
		} else {
			echo json_encode(array('msg'=>'发表失败！'));
		}
	}


	//发表评论
	function insertComment(){
		//print_r($_REQUEST);die;
		$data["comment"] = $_REQUEST["comment"];
		$data["daily_id"] = $_REQUEST["daily_id"];
		if(!empty($_REQUEST["comment_id"])){
			$data["pid"] = $_REQUEST["comment_id"];
		}
		$data["receiveman"] = $_REQUEST["uname"];
		$data["createname"] = $_SESSION["user_info"]["username"];
		$data["createtime"] = date("Y-m-d H:i:s");
		$daily_comment = M("daily_comment");
		$result = $daily_comment->add($data);
		//echo $daily_comment->getlastsql();die;
		if($result){
			echo json_encode(array('success'=>true,'msg'=>'发表成功！'));
		} else {
			echo json_encode(array('msg'=>'发表失败！'));
		}
	}

	//点赞
	function addLikes(){
		$username = $_SESSION["user_info"]["username"];
		$daily_info = M("daily_info");
		$likes = $daily_info->where("find_in_set('$username',likes) and id =".$_REQUEST["id"])->find();
		if($likes){
			echo json_encode(array('msg'=>'您已点过赞！'));
		}else{
			$likes = $daily_info->field("likes")->where("id =".$_REQUEST["id"])->find();

			$data["likes"] = $likes["likes"].$username.",";
			$result = $daily_info->where("id =".$_REQUEST["id"])->save($data);
			if($result){
				echo json_encode(array('success'=>true,'msg'=>'点赞成功！'));
			} else {
				echo json_encode(array('msg'=>'点赞失败！'));
			}
		}
	}

	function editPerson(){
		$users = M("users");
		$username = $_SESSION["user_info"]["username"];

		$data['label'] = $_REQUEST["label"];
		$data["motto"] = $_REQUEST["motto"];
		$result = $users->where("username = '$username'")->save($data);

		if($result){
			echo json_encode(array('success'=>true,'msg'=>'编辑成功！'));
		} else {
			echo json_encode(array('msg'=>'编辑失败！'));
		}


	}

	//上传头像
	function uploadImg(){
		$users = M("users");
		$username = $_SESSION["user_info"]["username"];
		$old_user_img = $users->field("user_img")->where("username = '$username'")->find();
		if($old_user_img){
			$oldSrc = 'include/data/headImg/'.$old_user_img['user_img'];
			unlink($oldSrc);
		}
		$res = $this->upload('headImg');
		$data['user_img'] = $res['0']['savename'];
		$result = $users->where("username = '$username'")->save($data);

		if($result){
			$this->dailyTrends();
		} else {
			$this->dailyTrends();
		}

	}

	/**
	* 文件上传
	*/
	function upload($src){

        import('ORG.Net.UploadFile');
        $upload = new UploadFile();// 实例化上传类
        $upload->maxSize = 2048000; // 设置附件上传大小
        //$upload->allowExts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->savePath = 'include/data/'.$src.'/'; // 设置附件上传根目录
        $upload->saveRule = 'getrand';
        $upload->autoSub = true;
        $upload->subType = 'date';
        // 上传文件

		if(!$upload->upload()) {// 上传错误提示错误信息
            $this->error($upload->getErrorMsg());
            echo json_encode(array('msg'=>'添加失败！'));
        }else{// 上传成功 获取上传文件信息
            $info =  $upload->getUploadFileInfo();
			return $info;
        }
    }


	//日报报表
	function dailyReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Daily Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);
		$arrData = $this->dailyReportData();
		$this->assign("result",$arrData);

		$this->display();
	}

	function dailyReportData (){
		$date = date("Y-m");
		$daily_info = M("daily_info");
		$fields = "createname,
		(case when createtime LIKE '$date-01 %' then '√' else '×' end) as day_01,
		(case when createtime LIKE '$date-02 %' then '√' else '×' end) as day_02,
		(case when createtime LIKE '$date-03 %' then '√' else '×' end) as day_03,
		(case when createtime LIKE '$date-04 %' then '√' else '×' end) as day_04,
		(case when createtime LIKE '$date-05 %' then '√' else '×' end) as day_05,
		(case when createtime LIKE '$date-06 %' then '√' else '×' end) as day_06,
		(case when createtime LIKE '$date-07 %' then '√' else '×' end) as day_07,
		(case when createtime LIKE '$date-08 %' then '√' else '×' end) as day_08,
		(case when createtime LIKE '$date-09 %' then '√' else '×' end) as day_09,
		(case when createtime LIKE '$date-10 %' then '√' else '×' end) as day_10,
		(case when createtime LIKE '$date-11 %' then '√' else '×' end) as day_11,
		(case when createtime LIKE '$date-12 %' then '√' else '×' end) as day_12,
		(case when createtime LIKE '$date-13 %' then '√' else '×' end) as day_13,
		(case when createtime LIKE '$date-14 %' then '√' else '×' end) as day_14,
		(case when createtime LIKE '$date-15 %' then '√' else '×' end) as day_15,
		(case when createtime LIKE '$date-16 %' then '√' else '×' end) as day_16,
		(case when createtime LIKE '$date-17 %' then '√' else '×' end) as day_17,
		(case when createtime LIKE '$date-18 %' then '√' else '×' end) as day_18,
		(case when createtime LIKE '$date-19 %' then '√' else '×' end) as day_19,
		(case when createtime LIKE '$date-20 %' then '√' else '×' end) as day_20,
		(case when createtime LIKE '$date-21 %' then '√' else '×' end) as day_21,
		(case when createtime LIKE '$date-21 %' then '√' else '×' end) as day_22,
		(case when createtime LIKE '$date-23 %' then '√' else '×' end) as day_23,
		(case when createtime LIKE '$date-24 %' then '√' else '×' end) as day_24,
		(case when createtime LIKE '$date-25 %' then '√' else '×' end) as day_25,
		(case when createtime LIKE '$date-26 %' then '√' else '×' end) as day_26,
		(case when createtime LIKE '$date-27 %' then '√' else '×' end) as day_27,
		(case when createtime LIKE '$date-28 %' then '√' else '×' end) as day_28,
		(case when createtime LIKE '$date-29 %' then '√' else '×' end) as day_29,
		(case when createtime LIKE '$date-30 %' then '√' else '×' end) as day_30,
		(case when createtime LIKE '$date-31 %' then '√' else '×' end) as day_31
		";

		import('ORG.Util.Page');

		$countarr      = $daily_info->where("createtime >= '$date-01 00:00:00' AND createtime <= '$date-31 23:59:59'")->group("createname")->select();
		$count = count($countarr);

		$Page       = new Page($count,20);
		$show       = $Page->show();
		//print_r($fields);die;
		$arrData = $daily_info->field($fields)->where("createtime >= '$date-01 00:00:00' AND createtime <= '$date-31 23:59:59'")->limit($Page->firstRow.','.$Page->listRows)->group("createname")->select();

		$result = array("arrData"=>$arrData,"show"=>$show);
		return	$result;

	}



}
?>
