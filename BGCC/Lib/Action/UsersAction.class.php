<?php
class UsersAction extends Action{
    //显示用户管理的主界面
    function listUsersParam(){
		checkLogin();
        header("Content-Type:text/html; charset=utf-8");
        $users=new Model("Users");

        $count=$users->table("users u")->field("u.username,u.r_id,r.interface,r.r_name,u.extension")->join("role r on u.r_id=r.r_id")->count();
		$checkRole = getSysinfo();
		if( $count >= $checkRole[5] ){
			$ms = "ms";
			$this->assign("usermsg",$ms);
		}

        $role=new Model('Role');
        $list=$role->select();
        $this->assign('rlist',$list);

        $department=new Model("Department");
        $list=$department->select();
        $this->assign('dlist',$list);

		//分配增删改的权限
		$menuname = "Users";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		//找出系统的所有路由
		$or = M('asterisk.outbound_routes');
		mysql_query("set names latin1");
		$routes = $or->field("route_id,name")->order("route_id ASC")->select();
		$this->assign('routes',$routes);


        $this->display("","utf-8");
    }

	function usersData(){
		$user = $_REQUEST['user'];
		$cn_name = $_REQUEST['cn_name'];
		$r_name = $_REQUEST['r_name'];
		$dept_id = $_REQUEST['dept_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$workno = $_SESSION['user_info']['username'];
		$did = $_SESSION['user_info']['d_id'];
		$deptst2 = $this->getMeAndSubDeptName($arrDep,$did);  //取上级部门
		$deptSet2 = rtrim($deptst2,",");

		if(!$dept_id){
			$d_id = $deptSet2;
		}else{
			$d_id = $deptSet;
		}
		$r_id = array_shift(explode("_",$r_name));
		//dump($r_id);die;

		$where = "1 ";
		$where .= empty($user)?"":" AND u.username like '%$user%'";
		$where .= empty($cn_name)?"":" AND u.cn_name like '%$cn_name%'";
		if($workno == "admin"){
			$where .= empty($dept_id)?"":" AND u.d_id in ($d_id)";
		}else{
			$where .= " AND u.d_id in ($d_id)";
		}
		$where .= empty($r_name)?"":" AND r.r_id = $r_id";

		$where1 = "1 ";
		$where1 .= empty($user)?"":" AND username like '%$user%'";
		$where1 .= empty($cn_name)?"":" AND cn_name like '%$cn_name%'";
		if($workno == "admin"){
			$where1 .= empty($dept_id)?"":" AND d_id in ($d_id)";
		}else{
			$where1 .= " AND d_id in ($d_id)";
		}
		$where1 .= empty($r_name)?"":" AND r_id = $r_id";


		$users=new Model("Users");
        $count=$users->where($where1)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		$sort = $_REQUEST["sort"];
		$order = $_REQUEST["order"];
		if($sort){
			$usort = "u.".$sort." ".$order;
		}else{
			$usort = "u.username asc";
		}


		//$usersList = $users->table('users u')->order($usort)->field('u.username,u.routeid,u.cn_name,u.r_id,r.interface,u.d_id,u.password,u.out_pin,r.r_name,d.d_name,u.extension,u.fax,u.email, u.phone,u.en_name,u.extension_type,e.number,e.trunkid')->join('department d on u.d_id=d.d_id ')->join('role r on u.r_id=r.r_id')->join("asterisk.exten_trunk_cid e on u.extension = e.exten")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$usersList = $users->table('users u')->order($usort)->field('u.username,u.routeid,u.cn_name,u.r_id,r.interface,u.d_id,u.out_pin,r.r_name,d.d_name,u.extension,u.fax,u.email, u.phone,u.en_name,u.extension_type,e.number,e.trunkid')->join('department d on u.d_id=d.d_id ')->join('role r on u.r_id=r.r_id')->join("asterisk.exten_trunk_cid e on u.extension = e.exten")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $users->getLastSql();die;

		$ip = $_SERVER['SERVER_ADDR'];
		$i = 0;
		foreach($usersList as &$val){
			//$usersList[$i]['confirm_password'] = $val["password"];

			$exten = $val['extension'];
			$username = $val['username'];
			if("sip" == $val['extension_type']){
				$sip = M("asterisk.sip");
				$arr = $sip->where("id='$exten' AND keyword='secret'")->find();
				//$usersList[$i]['extenPass'] = $arr['data'];
				//dump($sip->getLastSql());die;

			}elseif("iax2" == $val['extension_type']){
				$iax = M("asterisk.iax");
				$arr = $iax->where("id='$username' AND keyword='secret'")->find();
				//$usersList[$i]['extenPass'] = $arr['data'];
			}else{

			}
			//$data = base64_encode($ip."-".$user."-".$pwd."-".$exten."-".$extenPass);
			/*
			$val["qrcodes"] = $ip."-".$val["username"]."-".$val["password"]."-".$val["extension"]."-".$val["extenPass"];
			$val["qrcodes"] = base64_encode($val["qrcodes"]);
			*/
			$i++;
		}
		unset($i);

		//dump($usersList);die;
		$rowsList = count($usersList) ? $usersList : false;
		$arrUser["total"] = $count;
		$arrUser["rows"] = $rowsList;

		echo json_encode($arrUser);
	}

    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}

    //无限分类
    function getDepartmentTree( $l )
	{
		import("ORG.Util.DepartmentTree");
        $department=new Model('Department');
		$Tree = new Tree();

		$sql ="SELECT * FROM department ORDER BY d_orderid,d_id ASC";
		$rows = $department->query($sql);
		foreach($rows as $val){
			$Tree->setNode($val["d_id"],$val["d_pid"] , $val["d_name"]);
			$pid[$val["d_id"]] = $val["d_pid"];
		}
		$category = $Tree->getChilds();

		foreach ($category as $key=>$id)
		{
			$list[$id]["id"] = $id;
			$list[$id]["layer"] = $Tree->getLayer($id, $l);
			$list[$id]["d_pid"] = $pid[$id];
			$list[$id]["v"] = $Tree->getValue($id);
		}
		return $list;
	}

	//删除选中的用户
    function deleteUsers(){
		import('ORG.Pbx.extensionsBatch');
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');


		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAMP  = $pConfig->leer_configuracion(false);
		$arrAST  = $pConfig->leer_configuracion(false);

		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";

		$pDB = new paloDB($dsnAsterisk);
		$pLoadExtension = new paloSantoLoadExtension($pDB);

        $users=new UsersModel();
        $username = $_REQUEST['username'];
        $where = "username in ('" . str_replace(",","','",$username) ."')";
        //if(strpos($where,'admin')){
        if(strpos($where,"'admin'")){
            $del_mess = L("admin can not be deleted")."!";
			echo json_encode(array('msg'=>$del_mess));
            exit;
        }

		$ext_arr = $users->field("extension")->where($where)->select();

		foreach($ext_arr as $val){
			$Ext_arr[] = $val["extension"];
		}

        $num=$users->where($where)->delete();
        if($num){
			$this->usersCache();
			generateArrConf();//生成缓存信息
			$pLoadExtension->deleteAryExtension($Ext_arr);
			$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());
			$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $Ext_arr);
			if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
				$Messages .= $pLoadExtension->errMsg;


			$trkid = $_REQUEST['trkid'];
			foreach($trkid as $val){
				$arrTrunkID[] = explode("-",$val);
			}
			$exten = $_REQUEST['exten'];
			$trunks = new Model("asterisk.exten_trunk_cid");
			$trunkCount = $trunks->where("exten ='$exten'")->count();
			$result = $trunks->where("exten in ($exten)")->delete();
			import('ORG.Pbx.bmi');
			$bmi = new bmi();
			$bmi->loadAGI();
			$action = "DBDel";
			foreach($arrTrunkID as $val){
				if($val[1] !=  "null"){
					$parameters = Array("Family"=>'AMPUSER',"Key"=>$val[0].'/'.$val[1]);
					$bmi->asm->send_request($action,$parameters);
				}
			}

            //echo ('ok');
			echo json_encode(array('success'=>true));
        }else{
            //echo ($users->getError());
			echo json_encode(array('msg'=>'删除失败'));
        }
    }

	 function importPinExcel(){
		$m = M();
		$sql = "SELECT  u.username AS username,extension,cn_name,email,level,pin1,pin2,pin3,pin4 FROM users u LEFT JOIN pin p ON u.`username`= p.username order by username";
		$arr_rows = $m->query($sql);
		//dump($arr_rows);die;
		$csv = "WorkNo,Extension,name,email,level,DongGuan_PIN,GuangDong_PIN,China_PIN,Foreign_PIN\r\n";
		foreach($arr_rows AS $v){
			$csv .= $v['username'].",";
			$csv .= $v['extension'].",";
			$csv .= $v['cn_name'].",";
			$csv .= $v['email'].",";
			$csv .= $v['level'].",";
			$csv .= $v['pin1'].",";
			$csv .= $v['pin2'].",";
			$csv .= $v['pin3'].",";
			$csv .= $v['pin4']."\r\n";
		}
		//dump($csv);die;
		header('HTTP/1.1 200 OK');
		//header('Accept-Ranges: bytes');
		header('Date: ' . date("D M j G:i:s T Y"));
		header('Last-Modified: ' . date("D M j G:i:s T Y"));
		header("Content-Type: application/force-download");
		header("Content-Length: " . strlen($csv));
		header("Content-Transfer-Encoding: Binary");
		header("Content-Disposition: attachment;filename=Extension-PIN.csv");
		echo $csv;
	 }

	function batchUsersDownload(){
	    header("Content-Type:text/html; charset=utf-8");
		$m = M();
		//setlocale(LC_ALL, 'zh_CN.UTF-8');
		$sql = "SELECT * FROM `bgcrm`.users";
		$arr_rows = $m->query($sql);

		$arr_department = getDepartmentsByid();
		$arr_role = getRoleByid();

		//dump($arr_rows);die;
		$csv = "username,password,extension,department,role,Chinese name,English name,Email,Fax,extension type,Extension mac,Extension model\r\n";
		foreach($arr_rows AS $v){
			$csv .= $v['username'].",";
			$csv .= $v['password'].",";
			$csv .= $v['extension'].",";
			$csv .= utf2gb($arr_department[$v['d_id']]["d_name"]).",";
			$csv .= utf2gb($arr_role[$v['r_id']]["r_name"]).",";
			$csv .= utf2gb($v['cn_name']).",";
			$csv .= $v['en_name'].",";
			$csv .= $v['email'].",";
			$csv .= $v['fax'].",";
			$csv .= $v['extension_type'].",";
			$csv .= $v['extension_mac'].",";
			$csv .= $v['extension_model']."\r\n";
		}
		//dump($csv);die;
		header('HTTP/1.1 200 OK');
		//header('Accept-Ranges: bytes');
		header('Date: ' . date("D M j G:i:s T Y"));
		header('Last-Modified: ' . date("D M j G:i:s T Y"));
		header("Content-Type: application/force-download");
		header("Content-Length: " . strlen($csv));
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=IPPBX".".csv");
		//header("Content-disposition: attachment; filename=IPPBX-".date("YmdHis",time()).".csv");
		echo $csv;

	 }

	 function sendPinEmail(){
		set_time_limit(0);
		Vendor('Phpmailer.PHPM');
		//echo $_GET['usernames'];die;
		$usernames = $_GET['usernames'];
		$m = M('users');
		if( $usernames == "all" ){
			$sql = "SELECT u.username AS username,extension,email,pin1 FROM users u LEFT JOIN pin p ON u.username=p.username";
		}else{
			$tmpArr = explode(',',$usernames);
			//dump($tmpArr);die;
			array_shift($tmpArr);
			//dump($tmpArr);die;
			$str = "";
			foreach( $tmpArr As $v ){
				$str .= empty($str)?"'$v'":",'$v'";
			}
			$sql = "SELECT u.username AS username,extension,email,pin1 FROM users u LEFT JOIN pin p ON u.username=p.username where u.username in ($str)";
		}
		//dump($sql);die;
		$result = $m->query($sql);

		$smtp_host = "smtp.celestica.com";
		$smtp_account = "IPPBX@celestica.com";
		$smtp_password = "";
		//$receive_email = "dennisdu@celestica.com";

		$mail = new PHPMailer(); //建立邮件发送类

		$mail->IsSMTP(); // 使用SMTP方式发送
		$mail->CharSet='UTF-8';// 设置邮件的字符编码
		$mail->Host = $smtp_host;
		$mail->SMTPAuth = false; // 启用SMTP验证功能----关闭
		$mail->Username = $smtp_account;
		$mail->Password = $smtp_password;
		$mail->From = "CSSPhone@celestica.com"; //邮件发送者email地址
		$mail->FromName = "CSSPhone@celestica.com";
		$mail->Subject = "Phone authorization code changed"; //邮件标题

		foreach( $result AS $row ){
			$workNO = $row['username'];
			$exten = $row['extension'];
			$email = $row['email'];
			$pin = $row['pin1'];
			//$mailmsg = "Hello,workNO:$workNO!\nYour phone authorization code is changed to $pin now\n";
			$mailmsg ='account | password


'. $pin .'


Please keep it secure and don’t share your code with others.
The way of dialing outside:
dial 9 + dial outside number + *password* + #
In order to save cost, please dial to below sites & cities thru lease line instead of directly.
https://sites.google.com/a/celestica.com/celestica-intranet/sites/song-shan-lake-china/information-technology/it-services/telephony-service
Just a reminder
"All calling being under monitored & recorded by IT and non-business call will be reported to your departments managers/GM."';
			$mail->AddAddress("$email", "");//收件人地址，可以替换成任何想要接收邮件的email信箱,格式是AddAddress("收件人email","收件人姓名")
			$mail->Body = $mailmsg; //邮件内容
			if(!$mail->Send()){
				echo "邮件发送失败. <p>";
				echo "错误原因: " . $mail->ErrorInfo;
			}
		}
		echo "email is sending...";
	 }

    function addUsers(){

		checkLogin();
        header("Content-Type:text/html; charset=utf-8");
        $users=new Model("Users");

		//找出系统的所有路由
		$or = M('asterisk.outbound_routes');
		//mysql_query("set names latin1");
		$routes = $or->field("route_id,name")->order("route_id ASC")->select();
		$this->assign('routes',$routes);

        //对部门进行无限级分类
        import("ORG.Util.DepartmentTree");
        $department=new Model("Department");
        $dlist = $this->getDepartmentTree("　");
        $this->assign('dlist',$dlist);
        $role=new Model("Role");
        $list=$role->select();
        $this->assign('rlist',$list);

        $count=$users->table("users u")->field("u.username,u.r_id,r.interface,r.r_name,u.extension")->join("role r on u.r_id=r.r_id")->count();
		$checkRole = getSysinfo();
		if( $count >= $checkRole[5] ){
			$ms = "ms";
			$this->assign("usermsg",$ms);
		}

		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$enterprise_number = $_SESSION['user_info']['enterprise_number'];
			$arrTIF = getTenantsInfo();
			$arrTI = $arrTIF[$enterprise_number];
			$working_period = $arrTI["working_period"];
			$extension_period = $arrTI["extension_period"];
			$this->assign("working_period",$working_period);
			$this->assign("extension_period",$extension_period);
		}else{
			$this->assign("working_period","");
			$this->assign("extension_period","");
		}
		//dump($working_period);die;
		//dump($arrAL);die;
		if( in_array("pb",$arrAL) ){
			$this->assign("pb_display","block");
		}else{
			$this->assign("pb_display","none");
		}

		$this->display("","utf-8");
    }



	//批量添加用户
	function userBatchSave(){
		//dump($_POST);die;
		$enterprise_number = $_SESSION['user_info']['enterprise_number'];
		$username_start = $_POST['username_start'];
		$username_end = $_POST['username_end'];
		$extension_start = $_POST['extension_start'];
		$extension_end = $_POST['extension_end'];
		$r_id = $_POST['r_id'];
		$d_id = $_POST['d_id'];
		$extension_type = $_POST['extension_type'];
		$start_working_hours = $_POST['start_working_hours'];
		$end_working_hours = $_POST['end_working_hours'];

		//分解工号为"字母"+"数字"
		preg_match('/^([a-zA-Z]*)([0-9]+)$/', $username_start, $matches1);
		preg_match('/^([a-zA-Z]*)([0-9]+)$/', $username_end, $matches2);
		//dump($extension_start);die;
		if( !is_numeric($extension_start) or !is_numeric($extension_end)){
			echo "分机必须为数字";die;
		}
		if($username_start >= $username_end ){
			echo "非法分机范围";die;
		}
		if($matches1[1] != $matches2[1] or $matches1[2] >= $matches2[2]){
			echo "系统只支持格式为'工号+数字'的工号的批量添加";die;
		}

		$letters = $matches1[1]; //工号前缀
		$start = $matches1[2];// 工号起始数字
		$end = $matches2[2];//工号结束数字
		$range = $extension_end - $extension_start;
		//dump($range);die;
		//echo $letters."==",$start."==",$end."==",$range."==";die;
		if( $range != ($end - $start) ){
			echo "分机范围和工号范围不匹配!";die;
		}

		$users = M('users');

		$user_count = $users->table("users u")->field("u.username,u.r_id,r.interface,r.r_name,u.extension")->join("role r on u.r_id=r.r_id")->where("r.interface != 'pbx'")->count();
		$count = $user_count+$range;

		$checkRole = getSysinfo();
		$roleface = $_REQUEST["roleface"];
		if($roleface != "pbx"){
			if( $count >= $checkRole[5]){
				goback("您已超过了最大用户数，若您想要扩大用户数，请您联系管理员！", "index.php?m=Users&a=listUsersParam","pre");
			}
		}

		$suffix = $_REQUEST["suffix"];
		$successNum = 0;
		for($i=0;$i<=$range;$i++){
			//添加用户表
			$exten = (string)($extension_start+$i);
			$extenPwd = $exten.$suffix;
			$arrU = Array(
				'enterprise_number'=>$enterprise_number,
				'username'=>$letters .(string)($start+$i),
				'password'	=>'123456', //用户默认密码是123456
				'extension'=>$exten,
				'extenPass'=>$extenPwd,
				'd_id'	=> (int)($d_id),
				'r_id'	=> (int)($r_id),
				'extension_type'=>$extension_type,
				'start_working_hours'=>$start_working_hours,
				'end_working_hours'=>$end_working_hours,
			);
			//dump($arrU);
			$res = $users->add($arrU);
			//dump($users->getLastSql());die;
			//dump($res);
			if( $res == 1 ){
				$successNum ++;
			}
			//添加分机:默认分机名=分机号=分机密码
			$this->createExtension($exten,$exten,$extenPwd,$extension_type);
		}
		generateArrConf();//生成缓存信息
		$this->usersCache();
		//goback("成功导入{$successNum}个用户!","index.php?m=Users&a=listUsersParam","pre");
		echo json_encode(array('success'=>true,'msg'=>"成功添加{$successNum}个用户!"));
	}

	/*
	//批量添加用户 导入excel
    function userBatchSave(){
        header("Content-Type:text/html; charset=utf-8");
		import('ORG.Pbx.Fax');
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');

		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAMP  = $pConfig->leer_configuracion(false);
		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";
		$oFax = new Fax($pDB);

		$pDB = new paloDB($dsnAsterisk);

		$arr_department = getDepartmentsByname();
		$arr_role = getRoleByname();
		//setlocale(LC_ALL, 'zh_CN');
		$file = fopen($_FILES['userfile']['tmp_name'],"r");
		//$file = fopen("/tmp/csv.txt",'r');
		if($file){
			$user = M("users");
			$devices = M("asterisk.devices");
			//$rows = fgetcsv($file);
			$line = gb2utf(fgets($file));
			//$rows = mb_convert_encoding(fgetcsv($file), 'GB2312', 'UTF-8');
			//$rows = iconv("GB2312","UTF-8//IGNORE",fgetcsv($file));
			//dump($rows);die;
			while(! feof($file)){
				//$rows = fgetcsv($file);
				$line = gb2utf(fgets($file));
				$rows = explode(",",trim($line));
				//dump($rows);
				$d_id = $arr_department[$rows["3"]]["d_id"];
				$r_id = $arr_role[$rows["4"]]["r_id"];
				//dump($rows["8"]);die;
				$arr = $user->where("username='". $rows["0"] ."'")->count();
				if($arr){
					//更新用户

					$sql = "UPDATE users SET password='". $rows["1"] ."',
											 extension='". $rows["2"] ."',
											 d_id='". $d_id ."',
											 r_id='". $r_id ."',
											 en_name='". $rows["6"] ."',
											 cn_name='". $rows["5"] ."',
											 email='". $rows["7"] ."',
											 fax='". $rows["8"] ."',
											 extension_type='". $rows["9"] ."',
											 extension_mac='". $rows["10"] ."',
											 extension_model='". $rows["11"] ."'
							WHERE username='". $rows["0"] ."'";
					$user->query($sql);
					$u++;
				}else{
					//添加用户
					$count = $devices->where("id='". $rows["2"] ."'")->count();
					//添加分机
					if(strlen($rows["2"])>3 && strlen($rows["2"])<20 && $count<1 ){
						$result = $this->createExtension($rows["6"],$rows["2"],$rows["1"],$rows["9"]);
						//$result = $this->createExtension($_POST["en_name"],$_POST["extension"],$_POST["password"],$_POST["extension_type"]);
						if($result == ""){
							$e++;
						}
					}else{
						$rows["8"] = "";
						$rows["10"] = "";
						$rows["11"] = "";
					}
					//添加传真
					$count = $devices->where("id='". $rows["8"] ."'")->count();
					//dump($count);die;
					if(strlen($rows["8"])>3 && strlen($rows["8"])<20 && $count < 1 ){
						$result = $this->createExtension($rows["6"],$rows["8"],$rows["1"],"iax2");
						if($result == ""){

							$oFax->createFaxExtension($rows["6"], $rows["8"], $rows["1"], $rows["7"],
													  $rows["6"], $rows["8"],"86","769");
							$f++;
						}else{
							$rows["9"] = "";
						}
					}else{
						$rows["8"] = ""; //没有预先建立IAX分机
					}

					$sql = "INSERT  users SET password='". $rows["1"] ."',
											 extension='". $rows["2"] ."',
											 username='". $rows["0"] ."',
											 d_id='". $d_id ."',
											 r_id='". $r_id ."',
											 en_name='". $rows["6"] ."',
											 email='". $rows["7"] ."',
											 cn_name='". $rows["5"] ."',
											 fax='". $rows["8"] ."',
											 extension_type='". $rows["9"] ."',
											 extension_mac='". $rows["10"] ."',
											 extension_model='". $rows["11"] ."'";
					$user->query($sql);

					$u++;
					//echo $user->getLastSql();
				}
			}
			$oFax->restartFax();
			$fa = ($f.$Think['lang']['Fax']) ? $f.$Think['lang']['Fax'] : 0 ;
			//echo $u.$Think["lang"]["User"]." add\r\n".$e.$Think["lang"]["Extension"]." add\r\n".$f.$Think["lang"]["Fax"]." add";
			$mess = "批量添加成功,添加的用户：".$u.$Think['lang']['User']."个，添加的分机：".$e.$Think['lang']['Extension']."个，添加的传真：".$fa."个";
			goback($mess,"index.php?m=Users&a=listUsersParam","pre");
		}else{
			goBack($Think["lang"]["CSV file error"]."!","");
		}
    }
	*/

	function batchUsers(){
		checkLogin();
		$users=new Model("Users");
        $list=$users->order('username')->select();
        $this->assign('ulist',$list);

		$count = $users->count();
		$checkRole = getSysinfo();
		if( $count >= $checkRole[5] ){
			$ms = "ms";
			$this->assign("usermsg",$ms);
		}

        //对部门进行无限级分类
        import("ORG.Util.DepartmentTree");
        $department=new Model("Department");
        $dlist = $this->getDepartmentTree("　");
        $this->assign('dlist',$dlist);
        $role=new Model("Role");
        $list=$role->select();
        $this->assign('rlist',$list);

		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$enterprise_number = $_SESSION['user_info']['enterprise_number'];
			$arrTIF = getTenantsInfo();
			$arrTI = $arrTIF[$enterprise_number];
			$working_period = $arrTI["working_period"];
			$extension_period = $arrTI["extension_period"];
			$this->assign("working_period",$working_period);
			$this->assign("extension_period",$extension_period);
		}else{
			$this->assign("working_period","");
			$this->assign("extension_period","");
		}
		if( in_array("pb",$arrAL) ){
			$this->assign("pb_display","");
		}else{
			$this->assign("pb_display","none");
		}

        $this->display("","utf-8");
    }

	//创建外呼任务表
	function createTaskTabel($username,$d_id){
		$sales_task = M('sales_task');
		$arrTask = array(
			'username_enabled'=> 'Y',  //是否为坐席自己的任务
			//'name'=> $username."专属任务",
			'name'=> "我的任务",
			'createuser'=> $username,
			'calltype'=> "autocall",
			'createtime'=> date('Y-m-d H:i:s'),
			'describle'=> "",
			'dept_id'=> $d_id,  //队列
			'timeout' => "30",  //呼叫超时
			'concurrence'=> "1",  //最大并发数
			'limitperselect'=> (int)$_POST['limitperselect'],  //号码缓冲池
			'answer_target'=> "",
			'trunk'=>"SIP/ims",
			'ivr_id'=> "",
			'enable'=> "Y",
			'qualitypercent'=> "",
			'needrecord'=> "",
			'queue_members'=> $username,
		);
		$res = $sales_task->add($arrTask);
		$last_id = $sales_task->getLastInsID();

		//创建号码表
		$ss = M('sales_source');
		//创建号码资源包的表名
		$ss->execute("create table sales_source_$last_id like sales_source");
		$ss->execute("create table sales_cdr_$last_id like sales_cdr");
		$ss->execute("create table sales_contact_history_$last_id like sales_contact_history");
		$ss->execute("create table task_result_status_$last_id like task_result_status");
		$ss->execute("create table sales_proposal_record_$last_id like sales_proposal_record");

		$arr = $sales_task->where("username_enabled = 'Y'")->select();

		foreach($arr as $key => $val){
			$tmp[$val["createuser"]] = $val["id"];
		}
		F('task_user',$tmp,"BGCC/Conf/");
	}

	//外呼/销售工号、部门id，客服工号、部门id，工号对应的中文名
	function usersCache(){
		$users = new Model("users");
		$sellData = $users->order("username asc")->table("users u")->field("u.username,u.cn_name,u.r_id,u.d_id,r.agent_type")->join("role r on u.r_id = r.r_id")->where("r.agent_type = 'sell'")->select();
		foreach($sellData as $ksy=>$val){
			$sellUser[] = $val['username'];
			$sellDept[] = $val['d_id'];
			//$sell_cnUser[$val['username']] = $val['cn_name'];
		}

		$serviceData = $users->order("username asc")->table("users u")->field("u.username,u.r_id,u.d_id,r.agent_type")->join("role r on u.r_id = r.r_id")->where("r.agent_type = 'service'")->select();
		foreach($serviceData as $ksy=>$val){
			$serviceUser[] = $val['username'];
			$serviceDept[] = $val['d_id'];
			//$service_cnUser[$val['username']] = $val['cn_name'];
		}

		$uData = $users->order("username asc")->table("users u")->field("u.username,u.cn_name,u.extension,u.phone,u.d_id,d.d_name")->join("department d on u.d_id = d.d_id")->select();
		foreach($uData as $key=>$val){
			$exten_user[$val['username']] = $val["extension"];
			$cn_user[$val['username']] = $val["cn_name"];
			$deptId_user[$val['username']] = $val["d_id"];
			$deptName_user[$val['username']] = $val["d_name"];
			if($val["extension"]){
				$exten_cnName[$val["extension"]] = $val["cn_name"];
			}
			if($val["phone"]){
				$phone_cnName[$val["phone"]] = $val["username"];
			}
		}

		$department = new Model("department");
		$deptData = $department->select();
		foreach($deptData as $val){
			$deptId[$val["d_id"]] = $val["d_name"];
		}

		$arrDu = $users->field("username,d_id")->select();
		$arrDeptIdUser = $this->groupBy($arrDu,"d_id","username");

		$userArr = array(
						"sell_user"=>$sellUser,
						"service_user"=>$serviceUser,
						"sell_dept"=>array_unique($sellDept),
						"service_dept"=>array_unique($serviceDept),
						"cn_user"=>$cn_user,
						"deptId_user"=>$deptId_user,
						"deptName_user"=>$deptName_user,
						"deptId_name"=>$deptId,
						"exten_cnName"=>$exten_cnName,
						"phone_cnName"=>$phone_cnName,
						"exten_user"=>$exten_user,
						"deptIdUser"=>$arrDeptIdUser,
						//"cn_user"=>array_merge($sell_cnUser,$service_cnUser),
					);
		//dump($userArr);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F('users',$userArr,"BGCC/Conf/crm/$db_name/");
		//F('users',$userArr,"BGCC/Conf/");
		generateArrConf();
		//dump($userArr);die;
	}


	function groupBy($arr, $key_field,$value_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row[$value_field];
		}
		return $ret;
	}

    //添加用户管理的数据
    function insertUsers(){
		$enterprise_number = $_SESSION['user_info']['enterprise_number'];
		$users=new Model("Users");
        $count=$users->table("users u")->field("u.username,u.r_id,r.interface,r.r_name,u.extension")->join("role r on u.r_id=r.r_id")->where("r.interface != 'pbx'")->count();
		//echo $users->getLastSql();die;
		$checkRole = getSysinfo();
		$roleface = $_REQUEST["roleface"];
		//dump($_REQUEST['roleface']);die;
		//dump($_REQUEST);die;
		if($roleface != "pbx"){
			if( $count >= $checkRole[5]){
				$user_mess = L("You have exceeded the maximum number of users, if you want to expand the number of users, please contact your administrator");
				echo json_encode(array('msg'=>$user_mess));
				exit;
			}
		}

		if(empty($_POST["username"]) ){
			echo L("Username can not be empty");
			exit;
		}
		if(empty($_POST["password"]) ){
			echo L("Password can not be empty");
			exit;
		}
		$user_count = $users->where("username='". $_POST["username"] ."'")->count();
		if( $user_count>0 ){
				$user_mess = L("User already exists");
				echo json_encode(array('msg'=>$user_mess));
				exit;
		}
		$extension_count = $users->where("extension='". $_POST["extension"] ."' AND extension!=''")->count();
		if( $extension_count>0 ){
				echo json_encode(array('msg'=>"此分机已存在！"));
				exit;
		}

		$devices = M("asterisk.devices");
		if( $_POST["extension"] ){
			if(strlen($_POST["extension"])<3 || strlen($_POST["extension"])>20 ){
				$ext_mess = L("Extension number can not be less than three or more than 20");;
				echo json_encode(array('msg'=>$ext_mess));
				//echo L("Extension number can not be less than three or more than 20");
				exit;
			}
			$count = $devices->where("id='". $_POST["extension"] ."'")->count();
			if($count>0){
				$extension_mess = L("Extension already exists");
				echo json_encode(array('msg'=>$extension_mess));
				exit;
			}

		}

		if( ! empty($_POST["fax"]) ){
			if(strlen($_POST["fax"])<3 || strlen($_POST["fax"])>20 ){
				$fax_mess =  L("Fax number can not be less than three or more than 20");
				echo json_encode(array('msg'=>$fax_mess));
				exit;
			}
			$count = $devices->where("id='". $_POST["fax"] ."'")->count();
			if($count>0){
				$fax_message = L("Fax already exists");
				echo json_encode(array('msg'=>$fax_message));
				exit;
			}
			if($_POST["email"] == ""){
				$email_mess = L("Email can not be empty");
				echo json_encode(array('msg'=>$email_mess));
				exit;
			}
		}

        if($_REQUEST['cn_name']){
			$fixed_closed = implode(",",$_REQUEST["fixed_closed"]);
			$routeid = implode("/",$_REQUEST['priv_routes']);
			$arr = array (
				'enterprise_number' => $enterprise_number,
				'username' => $_REQUEST["username"],
				'cn_name' => $_REQUEST["cn_name"],
				'en_name' => $_REQUEST["en_name"],
				'password' => $_REQUEST["password"],
				'r_id' => $_REQUEST["r_id"],
				'd_id' => $_REQUEST["d_id"],
				'extension' => $_REQUEST["extension"],
				'extenPass' => $_REQUEST["extenPass"],
				'fax' => $_REQUEST["fax"],
				'email' => $_REQUEST["email"],
				'phone' => $_REQUEST["phone"],
				'extension_type' => $_REQUEST["extension_type"],
				'out_pin' => $_REQUEST["out_pin"],
				'routeid' => $routeid,
				'start_working_hours' => $_REQUEST["start_working_hours"],
				'end_working_hours' => $_REQUEST["end_working_hours"],
				'fixed_closed' => $fixed_closed,
				'company_name' => $_REQUEST['company_name'],
				'management_leader' => $_REQUEST['management_leader'],
				'management_leader_phone' => $_REQUEST['management_leader_phone'],
				'management_leader_email' => $_REQUEST['management_leader_email'],
				'management_leader_qq' => $_REQUEST['management_leader_qq'],
				'company_contact' => $_REQUEST['company_contact'],
				'contact_phone' => $_REQUEST['contact_phone'],
				'contact_email' => $_REQUEST['contact_email'],
				'contact_qq' => $_REQUEST['contact_qq'],
				'start_lease_time' => $_REQUEST['start_lease_time'],
				'end_lease_time' => $_REQUEST['end_lease_time'],
				'servicepause' => $_REQUEST['servicepause'],
				'max_concurrent' => $_REQUEST['max_concurrent'],
				'description' => $_REQUEST['description'],
				'head_image' => $_REQUEST['imgFile'],
			);
			
			$result = $users->add($arr);

			if($result){
				$this->usersCache();
				//=======================分机呼出callerid number列表=======================
				$trunks = new Model("asterisk.exten_trunk_cid");
				$arrData = array(
					"exten"=>$_REQUEST['extension'],
					"trunkid"=>$_REQUEST['trunkid'],
					"number"=>$_REQUEST['number'],
				);
				//dump($arrData);die;
				if($_REQUEST['extension']){
					$result = $trunks->data($arrData)->add();
				}
				//echo $trunks->getLastSql();die;

				import('ORG.Pbx.bmi');
				$bmi = new bmi();
				$bmi->loadAGI();
				$action = "DBPut";

				$parameters = Array("Family"=>'AMPUSER',"Key"=>$_REQUEST['extension'].'/'.$_REQUEST['trunkid'],"Val"=>$_REQUEST['number']);
				$bmi->asm->send_request($action,$parameters);
				//=======================分机呼出callerid number列表=======================

				/*
				if($_REQUEST['agent_type'] == 'sell'){
					$this->createTaskTabel($_POST["username"],$_POST["d_id"]);
				}*/
			}

			//添加分机
			if( $_POST["extension"] ){
				$this->createExtension($_POST["en_name"],$_POST["extension"],$_POST["extenPass"],$_POST["extension_type"]);
			}
			//添加传真_传真密码和分机密码相同
			if( $_POST["fax"] ){
				import('ORG.Pbx.extensionsBatch');
				import('ORG.Pbx.Fax');
				import('ORG.Pbx.pbxConfig');
				import('ORG.Db.bgDB');
				$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
				$arrAMP  = $pConfig->leer_configuracion(false);
				$arrAST  = $pConfig->leer_configuracion(false);
				$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
							   $arrAMP['AMPDBUSER']['valor']. ":".
							   $arrAMP['AMPDBPASS']['valor']. "@".
							   $arrAMP['AMPDBHOST']['valor']. "/asterisk";
				$pDB = new paloDB($dsnAsterisk);
				$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());
				$result = $this->createExtension($_POST["en_name"],$_POST["fax"],$_POST["extenPass"],"iax2");
				$pLoadExtension = new paloSantoLoadExtension($pDB);
				$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP);//重装是iax配置生效
				if($result == ""){
					$oFax = new Fax($pDB);
					//$oFax->createFaxExtension($_POST['en_name'], $_POST['fax'], $_POST['password'], $_POST['email'],$_POST['en_name'], $_POST['fax'],"86","755");
					$oFax->createFaxExtension($_POST['en_name'], $_POST['fax'], $_POST['extenPass'], $_POST['email'],$_POST['en_name'], $_POST['fax'],"86","755");
					$oFax->restartFax();

				}
			}
			generateArrConf();//生成缓存信息
			//if ($result){
				echo json_encode(array('success'=>true,'msg'=>'用户添加成功！'));
			//} else {
				//echo json_encode(array('msg'=>'用户添加失败！'));
			//}
        }
    }

	//测试---只添加单个传真
    function addOneFax(){
			if( $_GET["fax"] ){
				import('ORG.Pbx.extensionsBatch');
				import('ORG.Pbx.Fax');
				import('ORG.Pbx.pbxConfig');
				import('ORG.Db.bgDB');
				$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
				$arrAMP  = $pConfig->leer_configuracion(false);
				$arrAST  = $pConfig->leer_configuracion(false);
				$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
							   $arrAMP['AMPDBUSER']['valor']. ":".
							   $arrAMP['AMPDBPASS']['valor']. "@".
							   $arrAMP['AMPDBHOST']['valor']. "/asterisk";
				$pDB = new paloDB($dsnAsterisk);
				$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());
				$result = $this->createExtension('cn_name',$_GET["fax"],'111111',"iax2");
				$pLoadExtension = new paloSantoLoadExtension($pDB);
				$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP);//重装是iax配置生效

				if($result == ""){
					$oFax = new Fax($pDB);
					$oFax->createFaxExtension('xxxx', $_GET['fax'], '111111', 'abc@def.com','xxx', $_GET['fax'],"86","755");
					$oFax->restartFax();
				}
			}
    }


	//添加单个分机
	function createExtension($Name,$Ext,$Secret,$Tech){

		import('ORG.Pbx.extensionsBatch');
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');

		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAMP  = $pConfig->leer_configuracion(false);
		$arrAST  = $pConfig->leer_configuracion(false);

		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";

		$pDB = new paloDB($dsnAsterisk);
		$pLoadExtension = new paloSantoLoadExtension($pDB);

		$Context        = "from-internal";

//////////////////////////////////////////////////////////////////////////////////
		// validando para que coja las comillas
		$Outbound_CID = ereg_replace('“', "\"", $Outbound_CID);
		$Outbound_CID = ereg_replace('”', "\"", $Outbound_CID);
//////////////////////////////////////////////////////////////////////////////////
		//Paso 1: creando en la tabla sip

		if(!$pLoadExtension->createTechDevices($Ext,$Secret,$VoiceMail,$Context,$Tech))
		{
			$Messages .= $pLoadExtension->errMsg."<br />";
		}else{

			//Paso 2: creando en la tabla users
			if(!$pLoadExtension->createUsers($Ext,$Name,$VoiceMail,$Direct_DID,$Outbound_CID))
				$Messages .= $pLoadExtension->errMsg."<br />";

			//Paso 3: creando en la tabla devices
			if(!$pLoadExtension->createDevices($Ext,$Tech,$Name))
				$Messages .= $pLoadExtension->errMsg."<br />";

			//Paso 4: creando en el archivo /etc/asterisk/voicemail.conf los voicemails
			if(!$pLoadExtension->writeFileVoiceMail(
				$Ext,$Name,$VoiceMail,$VoiceMail_PW,$VM_Email_Address,
				$VM_Pager_Email_Addr,$VM_Options,$VM_EmailAttachment,$VM_Play_CID,
				$VM_Play_Envelope, $VM_Delete_Vmail)
			  )
				$Messages .= L("Failed to update the voicemail");

			//Paso 5: Configurando el call waiting
			if(!$pLoadExtension->processCallWaiting($Call_Waiting,$Ext))
				$Messages .= L("Failed to update the callwaiting");

			$outboundcid = ereg_replace("\"", "'", $Outbound_CID);
			$outboundcid = ereg_replace("\"", "'", $outboundcid);
			$outboundcid = ereg_replace(" ", "", $outboundcid);
			$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());
			if(!$pLoadExtension->putDataBaseFamily($data_connection, $Ext, $Tech, $Name, $VoiceMail, $outboundcid)){
				$Messages .= $Messages .= L("Failed to update the database of PBX");
			}
			$cont++;
		}
		////////////////////////////////////////////////////////////////////////
		//Paso 7: Escribiendo en tabla incoming
		/*
		if($Direct_DID !== ""){
			if(!$pLoadExtension->createDirect_DID($Ext,$Direct_DID))
			$Messages .= "分机: $Ext - 更新did失败<br />";
		}
				/////////////////////////////////////////////////////////////////////////
		/*
		if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
			$Messages .= $pLoadExtension->errMsg;
			*/
		$result = reloadAdd();
		if($result == false)
			$Messages .= $result;
		/*
		if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
			$Messages .= $pLoadExtension->errMsg;*/

		//$Messages = $Messages? $Messages : "分机添加成功!";
		generateArrConf();//生成缓存信息
		return $Messages;
	}




    //获取需要修改的用户的工号
    function editUsers(){
		checkLogin();
        $username=$_GET['username'];
        if($username){
            $this->assign('username',$username);
            $users = new Model("Users");
            //$arrUser = $users->where("username='$username'")->find();
            $arrUser = $users->table('users u')->join('role r on u.r_id=r.r_id')->where("username='$username'")->find();
			$arrUser[rid] = $arrUser["r_id"]."_".$arrUser["interface"];

			$ip = $_SERVER['SERVER_ADDR'];
			$qrcodes_tmp = $ip."-".$arrUser["username"]."-".$arrUser["password"]."-".$arrUser["extension"]."-".$arrUser["extenPass"];
			$qrcodes = base64_encode($qrcodes_tmp);
			//dump($arrUser);die;
            $d_id = $arrUser["d_id"];
            $this->assign("d_id",$d_id);
			$this->assign('udate',$arrUser);
			 
            $this->assign('qrcodes',$qrcodes);

			$exten = $arrUser['extension'];
			if("sip" == $arrUser['extension_type']){
				$sip = M("asterisk.sip");
				$arr = $sip->where("id='$exten' AND keyword='secret'")->find();
				//dump($sip->getLastSql());die;
				$this->assign("extenPass",$arr['data']);

			}elseif("iax2" == $arrUser['extension_type']){
				$iax = M("asterisk.iax");
				$arr = $iax->where("id='$username' AND keyword='secret'")->find();
				$this->assign("extenPass",$arr['data']);
			}else{

			}

            $role=new Model("Role");
            $list=$role->select();
			generateArrConf();//生成缓存信息
            $this->assign('rlist',$list);


			//找出系统的所有路由
			$or = M('asterisk.outbound_routes');
			//mysql_query("set names latin1");
			$routes = $or->field("route_id,name")->order("route_id ASC")->select();
			$this->assign('routes',$routes);

			$trunks = new Model("asterisk.exten_trunk_cid");
			$trunkData = $trunks->where("exten ='$exten'")->find();
			$this->assign('trunkid',$trunkData["trunkid"]);
        }


		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$enterprise_number = $_SESSION['user_info']['enterprise_number'];
			$arrTIF = getTenantsInfo();
			$arrTI = $arrTIF[$enterprise_number];
			$working_period = $arrTI["working_period"];
			$extension_period = $arrTI["extension_period"];
			$this->assign("working_period",$working_period);
			$this->assign("extension_period",$extension_period);
		}else{
			$this->assign("working_period","");
			$this->assign("extension_period","");
		}
		if( in_array("pb",$arrAL) ){
			$this->assign("pb_display","block");
		}else{
			$this->assign("pb_display","none");
		}

        $this->display();
    }

	/*
    function editUsers(){
		checkLogin();
        $username=$_GET['username'];
        if($username){
            $this->assign('username',$username);
            $users = new Model("Users");
            $arrUser = $users->where("username='$username'")->find();
            $d_id = $arrUser["d_id"];
            $this->assign("d_id",$d_id);
            $this->assign('udate',$arrUser);

			$exten = $arrUser['extension'];
			if("sip" == $arrUser['extension_type']){
				$sip = M("asterisk.sip");
				$arr = $sip->where("id='$exten' AND keyword='secret'")->find();
				//dump($sip->getLastSql());die;
				$this->assign("extenPass",$arr['data']);

			}elseif("iax2" == $arrUser['extension_type']){
				$iax = M("asterisk.iax");
				$arr = $iax->where("id='$username' AND keyword='secret'")->find();
				$this->assign("extenPass",$arr['data']);
			}else{

			}

            //对部门进行无限级分类
            import("ORG.Util.DepartmentTree");
            $department=new Model("Department");
            $dlist = $this->getDepartmentTree("　");
            $this->assign('dlist',$dlist);

            $role=new Model("Role");
            $list=$role->select();
			generateArrConf();//生成缓存信息
            $this->assign('rlist',$list);
        }
        $this->display();
    }
	*/


    //执行修改操作
    function updateUsers(){
		$users=new Model("Users");
		$count=$users->table("users u")->field("u.username,u.r_id,r.interface,r.r_name,u.extension")->join("role r on u.r_id=r.r_id")->count();
		$username = $_REQUEST['username'];
		$checkRole = getSysinfo();
		if( $count > $checkRole[5]){
			$user_mess = L("You have exceeded the maximum number of users, if you want to expand the number of users, please contact your administrator");
			echo json_encode(array('msg'=>$user_mess));
			exit;
		}

		import('ORG.Pbx.extensionsBatch');
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');
		import('ORG.Pbx.Fax');

        //$username = $_REQUEST['username'];

		$fixed_closed = implode(",",$_REQUEST["fixed_closed"]);
		$routeid = implode("/",$_POST['priv_routes']);
		$data = Array();
        $data['cn_name'] = $_REQUEST['cn_name'];
		$data['en_name'] = $_REQUEST['en_name'];
		$data['password'] = $_REQUEST['password'];
        $data['r_id'] = $_REQUEST['r_id'];
        $data['d_id'] = $_REQUEST['d_id'];
		$data['extension_type'] = $_REQUEST['extension_type'];
        $data['extension'] = $_REQUEST['extension'];
		$data['extenPass'] = $_REQUEST['extenPass'];
		$data['out_pin'] = $_REQUEST['out_pin'];
		$data['fax'] = $_REQUEST['fax'];
		$data['email'] = $_REQUEST['email'];
		$data['phone'] = $_REQUEST['phone'];
		$data['routeid'] = $routeid;
		$data['start_working_hours'] = $_REQUEST['start_working_hours'];
		$data['end_working_hours'] = $_REQUEST['end_working_hours'];
		$data['fixed_closed'] = $fixed_closed;
		$data['company_name'] = $_REQUEST['company_name'];
		$data['management_leader'] = $_REQUEST['management_leader'];
		$data['management_leader_phone'] = $_REQUEST['management_leader_phone'];
		$data['management_leader_email'] = $_REQUEST['management_leader_email'];
		$data['management_leader_qq'] = $_REQUEST['management_leader_qq'];
		$data['company_contact'] = $_REQUEST['company_contact'];
		$data['contact_phone'] = $_REQUEST['contact_phone'];
		$data['contact_email'] = $_REQUEST['contact_email'];
		$data['contact_qq'] = $_REQUEST['contact_qq'];

		$data['start_lease_time'] = $_REQUEST['start_lease_time'];
		$data['end_lease_time'] = $_REQUEST['end_lease_time'];
		$data['servicepause'] = $_REQUEST['servicepause'];
		$data['max_concurrent'] = $_REQUEST['max_concurrent'];
		$data['description'] = $_REQUEST['description'];
		$data['head_image'] = $_REQUEST['imgFile'];

		if(empty($_POST["password"]) ){
			$pwd_mess = L("Password can not be empty");
			echo json_encode(array('msg'=>$pwd_mess));
			exit;
		}
		if($_POST["extension"]){
		if(empty($_POST["extenPass"]) ){
			$expwd_mess = L("extension Password can not be empty");
			echo json_encode(array('msg'=>$expwd_mess));
			exit;
		}
		}
		//$devices = M("asterisk.devices");
		if( $_POST["fax"] ){	//有传真就必须有email
			if(strlen($_POST["fax"])<3 || strlen($_POST["fax"])>20 ){
				$faxed_mess = L("Fax number can not be less than three or more than 20");
				echo json_encode(array('msg'=>$faxed_mess));
				exit;
			}
			if($_POST["email"] == ""){
				$mailed_mess = L("Email can not be empty");
				echo json_encode(array('msg'=>$mailed_mess));
				exit;
			}
		}

		$users = new Model("Users");
		$arrUser = $users->where("username='$username'")->find();


		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAMP  = $pConfig->leer_configuracion(false);
		$arrAST  = $pConfig->leer_configuracion(false);
		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";

		$pDB = new paloDB($dsnAsterisk);
		$pLoadExtension = new paloSantoLoadExtension($pDB);
		$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());

		//fax改变
		if($arrUser["fax"]){
			//删除原来的传真
			$oFax = new Fax($pDB);
			$oFax->deleteFaxExtensionById($arrUser['fax']);
			$oFax->restartFax();
			//删除传真分机
			$arrExtension = array($arrUser["fax"]);
			$pLoadExtension->deleteAryExtension($arrExtension);
			$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
			if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
				$Messages .= $pLoadExtension->errMsg;
		}
		if($_POST["fax"]){//有传真就新增一个传真
			$pDB = new paloDB($dsnAsterisk);
			$result = $this->createExtension($_POST["en_name"],$_POST["fax"],$_POST["extenPass"],"iax2");
			$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP);
			if($result == ""){
				$oFax = new Fax($pDB);
				$oFax->createFaxExtension($_POST['en_name'], $_POST['fax'], $_POST['extenPass'], $_POST['email'],$_POST['en_name'], $_POST['fax'],"86","755");
				$oFax->restartFax();
			}
		}


		//分机改变
		if($arrUser["extension"] != $_POST["extension"]){ //先删除分机
			$arrExtension = array($arrUser["extension"]);
			$pLoadExtension->deleteAryExtension($arrExtension);
			$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
			if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP)){
				$Messages .= $pLoadExtension->errMsg;
				//echo "----33";die;
			}

			if($_POST["extension"]){ //如果分机非空，就增加分机。

				$result = $this->createExtension($_POST["en_name"],$_POST["extension"],$_POST["extenPass"],$_POST["extension_type"]);
				//echo "-";die;
			}

		}
		//fax改变
		if( $arrUser["fax"] != $_POST["fax"] ){
			if($_POST["fax"] == ""){
				//删除原来的传真
				$oFax = new Fax($pDB);
				$oFax->deleteFaxExtensionById($_POST['fax']);
				$oFax->restartFax();

				//删除传真分机
				$arrExtension = array($arrUser["fax"]);
				$pLoadExtension->deleteAryExtension($arrExtension);
				$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
				if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
					$Messages .= $pLoadExtension->errMsg;
				//删除分机

			}else{

				$faxs = M("asterisk.fax");
				$arrFax = $faxs->where("extension='$extension'")->find();

				$oFax = new Fax($pDB);
				if( $arrUser["email"] != $_POST["email"]  )	{
					$oFax->editFaxExtension($arrFax["id"],$_POST['username'], $_POST['fax'],
                              $_POST['password'], $_POST['email'],$_POST['username'],
                              $_POST['fax'],$arrFax['dev_id'],
                              $arrFax['port'],$arrFax['country_code'],$arrFax['area_code']);
				}else{
					$arrExtension = array($arrUser["fax"]);
					$pLoadExtension->deleteAryExtension($arrExtension);
					$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
					$oFax->deleteFaxExtensionById($arrUser['fax']);
					$result = $this->createExtension($_POST["en_name"],$_POST["fax"],$_POST["extenPass"],"iax2");
					$oFax->createFaxExtension($_POST['en_name'], $_POST['fax'], $_POST['extenPass'], $_POST['email'],
										  $_POST['en_name'], $_POST['fax'],"86","796");
					$oFax->restartFax();
					if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
					$Messages .= $pLoadExtension->errMsg;
				}

			}
		}

		if( $arrUser["extension"] != $_POST["extension"]  || $arrUser["extension_type"] != $_POST["extension_type"] ){
			if($_POST["extension"] == "" ){
				$arrExtension = array($arrUser["extension"]);
				$pLoadExtension->deleteAryExtension($arrExtension);
				$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
				if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
					$Messages .= $pLoadExtension->errMsg;
			} elseif($_POST["extension"] != $arrUser["extension"]  || $_POST["password"] != $arrUser["password"] || $arrUser["extension_type"] != $_POST["extension_type"]  ){  //
				$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());

				$arrExtension = array($arrUser["extension"]);
				$pLoadExtension->deleteAryExtension($arrExtension);
				$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
				if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
					$Messages .= $pLoadExtension->errMsg;
				$result = $this->createExtension($_POST["en_name"],$_POST["extension"],$_POST["extenPass"],$_POST["extension_type"]);
			}
		}

		//dump($data);die;
        $result = $users->where("username='$username'")->save($data);
		generateArrConf();//生成缓存信息
		if ($result !== false){
			$this->usersCache();
			if($arrUser["d_id"] != $_REQUEST["d_id"]){
				$customer = M("customer");
				$customer->where("createuser = '$username'")->save(array("dept_id"=>$_REQUEST["d_id"]));
			}
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
    }

    //执行修改操作
    function updateUsersBak20160224(){
		$users=new Model("Users");
        /*
		//用r_id来判断
		$count = $users->where("r_id!=4")->count();
		//必须去检查用户之前的角色id
		$username = $_REQUEST['username'];
		$arrTmp = $users->field("r_id")->where("username='$username'")->find();
		$last_r_id = $arrTmp['r_id'];
		if( (int)($last_r_id) != 4){//是普通用户的保存,否则就是pbx转普通用户
			$count -= 1;
		}
		$checkRole = getSysinfo();
		if( $count >= $checkRole[5] && (int)($_POST['r_id'])!=4){
			$user_mess = L("You have exceeded the maximum number of users, if you want to expand the number of users, please contact your administrator");
			echo json_encode(array('msg'=>$user_mess));
			exit;
		}
		*/
		$count=$users->table("users u")->field("u.username,u.r_id,r.interface,r.r_name,u.extension")->join("role r on u.r_id=r.r_id")->count();
		$username = $_REQUEST['username'];
		$checkRole = getSysinfo();
		if( $count >= $checkRole[5]){
			$user_mess = L("You have exceeded the maximum number of users, if you want to expand the number of users, please contact your administrator");
			echo json_encode(array('msg'=>$user_mess));
			exit;
		}

		import('ORG.Pbx.extensionsBatch');
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');
		import('ORG.Pbx.Fax');

        //$username = $_REQUEST['username'];

		$fixed_closed = implode(",",$_REQUEST["fixed_closed"]);
		$routeid = implode("/",$_POST['priv_routes']);
		$data = Array();
        $data[cn_name] = $_REQUEST['cn_name'];
		$data[en_name] = $_REQUEST['en_name'];
		$data[password] = $_REQUEST['password'];
        $data[r_id] = $_REQUEST['r_id'];
        $data[d_id] = $_REQUEST['d_id'];
		$data[extension_type] = $_REQUEST['extension_type'];
        $data[extension] = $_REQUEST['extension'];
		$data[extenPass] = $_REQUEST['extenPass'];
		$data['out_pin'] = $_REQUEST['out_pin'];
		$data[fax] = $_REQUEST['fax'];
		$data[email] = $_REQUEST['email'];
		$data[phone] = $_REQUEST['phone'];
		$data[routeid] = $routeid;
		$data['start_working_hours'] = $_REQUEST['start_working_hours'];
		$data['end_working_hours'] = $_REQUEST['end_working_hours'];
		$data['fixed_closed'] = $fixed_closed;

		if(empty($_POST["password"]) ){
			$pwd_mess = L("Password can not be empty");
			echo json_encode(array('msg'=>$pwd_mess));
			exit;
		}
		if($_POST["extension"]){
		if(empty($_POST["extenPass"]) ){
			$expwd_mess = L("extension Password can not be empty");
			echo json_encode(array('msg'=>$expwd_mess));
			exit;
		}
		}
		//$devices = M("asterisk.devices");
		if( $_POST["fax"] ){	//有传真就必须有email
			if(strlen($_POST["fax"])<3 || strlen($_POST["fax"])>20 ){
				$faxed_mess = L("Fax number can not be less than three or more than 20");
				echo json_encode(array('msg'=>$faxed_mess));
				exit;
			}
			if($_POST["email"] == ""){
				$mailed_mess = L("Email can not be empty");
				echo json_encode(array('msg'=>$mailed_mess));
				exit;
			}
		}

		$users = new Model("Users");
		$arrUser = $users->where("username='$username'")->find();


		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAMP  = $pConfig->leer_configuracion(false);
		$arrAST  = $pConfig->leer_configuracion(false);
		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";

		$pDB = new paloDB($dsnAsterisk);
		$pLoadExtension = new paloSantoLoadExtension($pDB);
		$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());

		//fax改变
		if($arrUser["fax"]){
			//删除原来的传真
			$oFax = new Fax($pDB);
			$oFax->deleteFaxExtensionById($arrUser['fax']);
			$oFax->restartFax();
			//删除传真分机
			$arrExtension = array($arrUser["fax"]);
			$pLoadExtension->deleteAryExtension($arrExtension);
			$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
			if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
				$Messages .= $pLoadExtension->errMsg;
		}
		if($_POST["fax"]){//有传真就新增一个传真
			$pDB = new paloDB($dsnAsterisk);
			$result = $this->createExtension($_POST["en_name"],$_POST["fax"],$_POST["extenPass"],"iax2");
			$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP);
			if($result == ""){
				$oFax = new Fax($pDB);
				$oFax->createFaxExtension($_POST['en_name'], $_POST['fax'], $_POST['extenPass'], $_POST['email'],$_POST['en_name'], $_POST['fax'],"86","755");
				$oFax->restartFax();
			}
		}


		//分机改变
		if($arrUser["extension"]){ //先删除分机
			$arrExtension = array($arrUser["extension"]);
			$pLoadExtension->deleteAryExtension($arrExtension);
			$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
			if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
				$Messages .= $pLoadExtension->errMsg;
		}
		//echo "----33";die;
		if($_POST["extension"]){ //如果分机非空，就增加分机。

			$result = $this->createExtension($_POST["en_name"],$_POST["extension"],$_POST["extenPass"],$_POST["extension_type"]);
			//echo "-";die;
		}

		//fax改变
		if( $arrUser["fax"] != $_POST["fax"] ){
			if($_POST["fax"] == ""){
				//删除原来的传真
				$oFax = new Fax($pDB);
				$oFax->deleteFaxExtensionById($_POST['fax']);
				$oFax->restartFax();

				//删除传真分机
				$arrExtension = array($arrUser["fax"]);
				$pLoadExtension->deleteAryExtension($arrExtension);
				$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
				if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
					$Messages .= $pLoadExtension->errMsg;
				//删除分机

			}else{

				$faxs = M("asterisk.fax");
				$arrFax = $faxs->where("extension='$extension'")->find();

				$oFax = new Fax($pDB);
				if( $arrUser["email"] != $_POST["email"]  )	{
					$oFax->editFaxExtension($arrFax["id"],$_POST['username'], $_POST['fax'],
                              $_POST['password'], $_POST['email'],$_POST['username'],
                              $_POST['fax'],$arrFax['dev_id'],
                              $arrFax['port'],$arrFax['country_code'],$arrFax['area_code']);
				}else{
					$arrExtension = array($arrUser["fax"]);
					$pLoadExtension->deleteAryExtension($arrExtension);
					$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
					$oFax->deleteFaxExtensionById($arrUser['fax']);
					$result = $this->createExtension($_POST["en_name"],$_POST["fax"],$_POST["extenPass"],"iax2");
					$oFax->createFaxExtension($_POST['en_name'], $_POST['fax'], $_POST['extenPass'], $_POST['email'],
										  $_POST['en_name'], $_POST['fax'],"86","796");
					$oFax->restartFax();
					if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
					$Messages .= $pLoadExtension->errMsg;
				}

			}
		}

		if( $arrUser["extension"] != $_POST["extension"]  || $arrUser["extension_type"] != $_POST["extension_type"] ){
			if($_POST["extension"] == "" ){
				$arrExtension = array($arrUser["extension"]);
				$pLoadExtension->deleteAryExtension($arrExtension);
				$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
				if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
					$Messages .= $pLoadExtension->errMsg;
			} elseif($_POST["extension"] != $arrUser["extension"]  || $_POST["password"] != $arrUser["password"] || $arrUser["extension_type"] != $_POST["extension_type"]  ){  //
				$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());

				$arrExtension = array($arrUser["extension"]);
				$pLoadExtension->deleteAryExtension($arrExtension);
				$pLoadExtension->deleteTree($data_connection, $arrAST, $arrAMP, $arrExtension);
				if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
					$Messages .= $pLoadExtension->errMsg;
				$result = $this->createExtension($_POST["en_name"],$_POST["extension"],$_POST["extenPass"],$_POST["extension_type"]);
			}
		}

		//dump($data);die;
        $result = $users->where("username='$username'")->save($data);
		generateArrConf();//生成缓存信息
		if ($result !== false){
			$this->usersCache();
		/*
			//=======================分机呼出callerid number列表=======================
			$trunkid = $_POST['trunkid'];
			$exten = $_REQUEST['exten'];
			$trunks = new Model("asterisk.exten_trunk_cid");
			mysql_query("set names latin1");
			$arrData = array(
				"exten"=>$_POST['extension'],
				"trunkid"=>$_POST['trunkid'],
				"number"=>$_POST['number'],
			);
			//dump($arrData);
			$trunkCount = $trunks->where("exten ='$exten'")->count();
			if($trunkCount>0){
				$trunks->data($arrData)->where("exten ='$exten'")->save();
			}else{
				$trunks->data($arrData)->add();
			}
			//echo $trunks->getLastSql();die;
			import('ORG.Pbx.bmi');
			$bmi = new bmi();
			$bmi->loadAGI();
			$action = "DBPut";

			$parameters = Array("Family"=>'AMPUSER',"Key"=>$_REQUEST['extension'].'/'.$_REQUEST['trunkid'],"Val"=>$_REQUEST['number']);
			$bmi->asm->send_request($action,$parameters);
			//=======================分机呼出callerid number列表=======================
		*/
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
    }


	//生成二维码
	function phpQRCodes(){
		$ip = $_SERVER['SERVER_ADDR'];
		$exten = $_REQUEST["extension"];
		$extenPass = $_REQUEST["extenPass"];
		$user = $_REQUEST["user"];
		$pwd = $_REQUEST["pwd"];
		vendor("phpqrcode.phpqrcode");
		$qrcode = new QRtools();
		ob_clean();   //清除输出 清除空格
		$data = base64_encode($ip."-".$user."-".$pwd."-".$exten."-".$extenPass);

		//dump($data);die;
		// 生成的文件名
		//$filename = $errorCorrectionLevel.'|'.$matrixPointSize.'.png';
		// 纠错级别：L、M、Q、H
		$errorCorrectionLevel = 'L';
		// 点的大小：1到10
		$matrixPointSize = 9;
		QRcode::png($data, false, $errorCorrectionLevel, $matrixPointSize, 2);
	}


	//生成二维码
	function phpQRCodes2(){
		vendor("phpqrcode.phpqrcode");
		$qrcode = new QRtools();
		ob_clean();   //清除输出 清除空格
		$qrcodes = $_REQUEST["qrcodes"];
		$content = base64_decode($qrcodes);

		//dump($data);die;
		// 生成的文件名
		//$filename = $errorCorrectionLevel.'|'.$matrixPointSize.'.png';
		// 纠错级别：L、M、Q、H
		$errorCorrectionLevel = 'L';
		// 点的大小：1到10
		$matrixPointSize = 9;
		QRcode::png($content, false, $errorCorrectionLevel, $matrixPointSize, 2);
	}

	//批量编辑外呼权限
	function updateBatchPurview(){
		$username = $_REQUEST['username'];
		$users = new Model("users");
		$routeid = implode("/",$_REQUEST['priv_routes2']);
		$arrData = array(
			"out_pin" => $_REQUEST['out_pin2'],
			"routeid" =>$routeid ,
		);
		$result = $users->data($arrData)->where("username in ($username)")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	//交换位置
	function exchangePosition(){
		$arr = $_REQUEST["field_value"];
		foreach($arr as $val){
			$arrU[] = $val[0];
			$arrE[] = $val[1];
			$arrUE[$val[0]] = $val[1];
		}
		$str_users = "'".implode("','",$arrU)."'";
		$str_exten = "'".implode("','",$arrE)."'";
		$users = M("users");
		$arrOldUser = $users->field("username,extension")->where("username in ($str_users) OR extension in ($str_exten)")->select();
		foreach($arrOldUser as $val){
			if($val["extension"]){
				$arrOldExten[] = $val["extension"];
				$arrOldEU[$val["extension"]] = $val["username"];
				$arrOldUE[$val["username"]] = $val["extension"];
			}
		}
		//var_export($arrOldUser);
		//echo "xxxxxxxx";
		//var_export($arrUE);
		//echo "xxxxxxxx";

		foreach($arr as &$val){
			//工号绑定分机
			$result[] = $users->where("username='".$val[0]."'")->save(array("extension"=>$val[1]));
			/*
			//下面这种有问题
			//如果新的分机号对应的工号与现在绑定的工号不相同
			if($arrOldUE[$val[1]] && $arrOldEU[$val[1]] != $val[0]){
				$arrF[$arrOldEU[$val[1]]] = $arrOldEU[$val[0]];
				$users->where("username='".$arrOldEU[$val[1]]."'")->save(array("extension"=>$arrOldEU[$val[0]]));  //将被绑定的分机对应的工号的分机清空
			}
			*/
		}

		$arrNewUser = $users->field("username,extension")->where("username in ($str_users) OR extension in ($str_exten)")->select();
		foreach($arrNewUser as $val){
			$arr1[] = $val["extension"];
		}
		$arr2 = array_unique($arr1);
		//获取重复的分机
		$arrData = $this->getRepeatArray($arrNewUser,"extension");
		if($arrData){
			foreach($arrData as $val){
				$arrExtens[] = $val["extension"];
			}
			$strExten = "'".implode("','",$arrExtens)."'";

			//找到没用使用的分机【从长传的这些分机中】
			$arrDiff = array_diff($arrOldExten,$arr2);
			sort($arrDiff);
			$arrT = $users->field("username,extension")->where("extension in ($strExten)")->select();
			$i = 0;
			foreach($arrT as $val){
				//不是填写的工号绑定分机的工号，修改分机【重复的】
				if( $arrUE[$val["username"]] != $val["extension"] ){
					$arrN[] = $val;
					$users->where("username='".$val["username"]."'")->save(array("extension"=>$arrDiff[$i]));  //将被绑定的分机对应的工号的分机清空
					$i++;
				}
			}
		}
		//var_export($arrNewUser);
		//echo "xxxxxxxx";
		//var_export($arrData);
		//dump($arrF);die;
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'位置交换成功！'));
		} else {
			echo json_encode(array('msg'=>'位置交换失败！'));
		}
	}

	function testtttt(){
		$users = M("users");

		$arrNewUser = $users->field("username,extension")->select();
		//获取重复的分机
		$arrData = $this->getRepeatArray($arrNewUser,"extension");
		dump($arrData);die;

		$arrOldUser = array ( 0 => array ( 'username' => '801', 'extension' => '801', ), 1 => array ( 'username' => '802', 'extension' => '802', ), 2 => array ( 'username' => '804', 'extension' => '804', ), 3 => array ( 'username' => '805', 'extension' => '805', ), 4 => array ( 'username' => '503', 'extension' => '503', ), 5 => array ( 'username' => '203', 'extension' => '203', ), 6 => array ( 'username' => '201', 'extension' => '201', ), 7 => array ( 'username' => '105', 'extension' => '105', ), 8 => array ( 'username' => '104', 'extension' => '104', ), 9 => array ( 'username' => '101', 'extension' => '101', ), );
		foreach($arrOldUser as $val){
			$arrOldExten[] = $val["extension"];
		}
		$arrNewUser = array ( 0 => array ( 'username' => '801', 'extension' => '805', ), 1 => array ( 'username' => '802', 'extension' => '203', ), 2 => array ( 'username' => '804', 'extension' => '201', ), 3 => array ( 'username' => '805', 'extension' => '805', ), 4 => array ( 'username' => '503', 'extension' => '801', ), 5 => array ( 'username' => '203', 'extension' => '203', ), 6 => array ( 'username' => '201', 'extension' => '201', ), 7 => array ( 'username' => '105', 'extension' => '105', ), 8 => array ( 'username' => '104', 'extension' => '804', ), 9 => array ( 'username' => '101', 'extension' => '105', ), );

		$arrData =  array ( 0 => array ( 'username' => '101', 'extension' => '105', ), 1 => array ( 'username' => '201', 'extension' => '201', ), 2 => array ( 'username' => '203', 'extension' => '203', ), 3 => array ( 'username' => '805', 'extension' => '805', ), );

		foreach($arrNewUser as $val){
			$arr1[] = $val["extension"];
		}
		$arr2 = array_unique($arr1);
		foreach($arrData as $val){
			$arrExtens[] = $val["extension"];
		}
		$strExten = "'".implode("','",$arrExtens)."'";

		$arrDiff = array_diff($arrOldExten,$arr2);
		sort($arrDiff);

		$arrT = $users->field("username,extension")->where("extension in ($strExten)")->select();
		$arrUE = array ( 801 => '805', 802 => '203', 503 => '801', 104 => '804', 101 => '105', 804 => '201', );

		$i = 0;
		foreach($arrT as $val){
			if( $arrUE[$val["username"]] != $val["extension"] ){
				$arrN[] = $val;
				$users->where("username='".$val["username"]."'")->save(array("extension"=>$arrDiff[$i]));  //将被绑定的分机对应的工号的分机清空
				$arrNT[$val["username"]] = array("extension_$i"=>$arrDiff[$i]);

				$i++;
			}else{
				$arrN2[] = $val;
			}
		}


		dump($arrDiff);
		//dump($arrData);
		dump($arrN);
		dump($arrNT);
		die;
	}

	function getRepeatArray($arr, $key){
		$tmp_arr = array();
		//$tmp = array();
		foreach($arr as $k => $v){
			if(in_array($v[$key], $tmp_arr)){
				$tmp[] = $v;
			}else{
				$tmp_arr[] = $v[$key];
			}
		}
		sort($tmp);
		return $tmp;
	}

	//获取融云Token
	function getRyToken(){
		require 'BGCC/Conf/RongYun.class.php';
		$para_sys = readS();
		$username = $_REQUEST['username'];
		$users = M("users");
		$userArr = $users->where("username in ($username)")->select();
		foreach($userArr as $val){
			$data = array();
			$userId = mt_rand(100000000,999999999).time();
			$portraitUri = "http://".$para_sys["IpAddress"]."/include/data/headImg/".$val["user_img"];
			$ryToken = $rongyun->getToken($userId,$val["cn_name"],$portraitUri,$para_sys["appSecret"],$para_sys["appKey"]);
			//print_r($ryToken);die;
			$tokenArr = json_decode($ryToken,true);
			$data["ryId"] = $tokenArr["userId"];
			$data["ryToken"] = $tokenArr["token"];
			$result[] = $users->where("username = '".$val["username"]."'")->data($data)->save();
		}

		if($result!==false){
			echo json_encode(array('success'=>true,'msg'=>'获取成功！'));
		}else{
			echo json_encode(array('msg'=>'获取失败！'));
		}



	}

}
?>

