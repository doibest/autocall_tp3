<?php
class TelemarketingAction extends Action{

	function newOutBoundList(){
		checkLogin();
		$task_id = $_REQUEST["task_id"];
		$this->assign("task_id",$task_id);
		$name = $_REQUEST["name"];
		$this->assign("name",$name);
		$deptId = $_REQUEST["deptId"];
		$this->assign("deptId",$deptId);
		$calltype = $_REQUEST["calltype"];
		$this->assign("calltype",$calltype);

		$menuname = "Task";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}
		$this->assign("priv",$priv);


		$sales_task = new Model("sales_task");
		$arrF = $sales_task->where("id = '$task_id'")->find();
		$this->assign("taskData",$arrF);

		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("wj",$arrAL) ){
			$this->assign("Questionnaire","Y");
		}else{
			$this->assign("Questionnaire","N");
		}
		$geteTitle = (!empty($_GET['title']))?$_GET['title']:'';
		$this->assign('geteTitle',$geteTitle);

		$this->display();
	}

	function getWiatQueueData(){
		$task_id = $_REQUEST["task_id"];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrData = $bmi->waitingQueueInfo($task_id);

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = count($arrData);
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function getCallData(){
		$task_id = $_REQUEST["task_id"];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$group = (string)$task_id;
		$category = "AUTOCALL";
		$arrChannel = $bmi->getGroupChannelsInfo($group,$category);

		$arrData = $bmi->getAllChannelsInfo('Channel');


		foreach($arrData as &$val){
			$val["exten"] = array_pop(explode("/",array_shift(explode("@",$val["BridgedTo"]))));
			$val["Duration"] = sprintf("%02d",intval($val["Duration"]/3600)).":".sprintf("%02d",intval(($val["Duration"]%3600)/60)).":".sprintf("%02d",intval((($val[Duration]%3600)%60)));
			if(in_array($val["Channel"],$arrChannel) && $val["State"] == "Up"  && $val["BridgedTo"] != "(None)" ){
				$arrF[] = $val;
			}
		}

		$rowsList = count($arrF) ? $arrF : false;
		$arrT["total"] = count($arrF);
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);

	}

	function saveConditions(){
		$id = $_REQUEST["id"];
		$calledflag = $_REQUEST["calledflag"];
		$billsec = $_REQUEST["billsec"];
		$key_value = $_REQUEST["key_value"];
		$call_status = $_REQUEST["call_status"];

		$where = "1 ";
		$where .= empty($calledflag) ? "" : " AND calledflag = '$calledflag'";
		$where .= empty($billsec) ? "" : " AND billsec >= '$billsec'";
		$where .= empty($key_value) ? "" : " AND key_value = '$key_value'";
		$where .= empty($call_status) ? "" : " AND call_status = '$call_status'";

		$value = "calledflag:".$calledflag.", billsec:".$billsec.", key_value:".$key_value.", call_status:".$call_status;

		$arrF = array(
			"where" => $where,
			"value" => $value,
		);

		$sales_task = new Model("sales_task");
		$arrData = array(
			"get_conditions" => json_encode($arrF),
		);
		$result = $sales_task->data($arrData)->where("id = '$id'")->save();
		//echo $sales_task->getLastSql();die;
		if ($result !== false){
			$this->taskConditionCache();
			echo json_encode(array('success'=>true,'msg'=>"保存成功！"));
		} else {
			echo json_encode(array('msg'=>'保存失败！'));
		}
	}

	function taskConditionCache(){
		$sales_task = new Model("sales_task");
		$arrData = $sales_task->field("id,get_conditions")->select();
		foreach($arrData as &$val){
			$val["get_conditions"] = json_decode($val["get_conditions"],true);
			if($val["get_conditions"]["where"]){
				$arrF[$val["id"]] = $val["get_conditions"]["where"];
			}
		}
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F("condition",$arrF,"BGCC/Conf/crm/$db_name/");
		//F("condition",$arrF,"BGCC/Conf/");
	}

	/*语音信息*/
	function listTask(){
		checkLogin();

		$menuname = "Task";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}
		$AiGrade = M('customer_fields')->where(array('id'=>32))->getField('field_values');
		$this->assign('aigradeInfo',$this->unicode_decode($AiGrade));
		$this->assign("priv",$priv);

		$this->display();
	}

	function taskDataList(){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$username = $_SESSION["user_info"]["username"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptId_Set = rtrim($deptst,",");
		$where = "1 ";
		if($username != "admin"){
			$where .= " AND dept_id in ($deptId_Set)";
		}

		$sales_task = M('sales_task');
		$count = $sales_task->table("sales_task st")->join("left join department d on st.dept_id=d.d_id")->where("$where AND enable='Y'")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = BG_Page($count,$page_rows);//这个地方要修改

		$tpl_ST = $sales_task->table("sales_task st")->join("left join department d on st.dept_id=d.d_id")->where("$where AND enable='Y'")->order("createtime desc")->limit($page->limit)->select();

		//echo $sales_task->getLastSql();die;

		$preview = L('Preview outbound calls');
		$autocall = L('Automatic outbound calls');
		$status_row = array('preview'=>$preview,'autocall'=>$autocall);

		//$quality = $this->qualityRatio();

		$menuname = "Task";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$username = $_SESSION['user_info']['username'];

		//统计任务人数
		$u = M("users");
		$sql = "SELECT d_id,COUNT(*) AS agentCount FROM users GROUP BY d_id";
		$arr = $u->query($sql);
		$arrDeptCount = Array();
		foreach($arr AS $v){
			if($v['d_id']){
				$arrDeptCount[$v['d_id']] = $v['agentCount'];
			}
		}

		//统计所有队列中的成员
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrA = $bmi->getAllQueueMember();

		//下面代码没加权限的时候是我写的，加了权限后...，然后我又要增加新功能了，没时间在上面修改了。
		$i = 0;
		foreach($tpl_ST as &$val){
			$id = $val["id"];
			if( array_key_exists("AutoCall_$id",$arrA) ){
				//$tpl_ST[$i]['effectiveAgent'] = $arrA["AutoCall_$id"]['agentcount'];  //有效坐席数量
				$tpl_ST[$i]['effectiveAgent'] = count($arrA["AutoCall_$id"]['membername']);  //有效坐席数量
			}else{
				$tpl_ST[$i]['effectiveAgent'] = "0";
			}
			//统计每个任务的进度
			$ss = "sales_source_".$val['id'];
			//通话时长统计

			//通话统计结束
			$m = M($ss);
			$arr = array();
			//需要增加按键值和有效数量、号码总数
			$AiGrade = $this->unicode_decode(M('customer_fields')->where(array('id'=>32))->getField('field_values'));
			foreach($AiGrade AS $v){
				$AiGradeSql .= 'SUM(aigrade="'.$v['0'].'") as aiGrade'.$v['0'].',';
			}
			// SUM(aigrade=1)as aClassCount,SUM(hangup_disposition =1) as customerHangUpRate ,SUM(hangup_disposition =2) as customerAleadyHangUp ,SUM(aigrade=2) as bClassCount,SUM(aigrade=3) as cClassCount,SUM(aigrade=4) as dClassCount,SUM(aigrade=5) as eClassCount,SUM(aigrade=6) as fClassCount,
			$arr = M('sales_source_'.$val['id'])->field($AiGradeSql." COUNT(*) AS totalnum,COUNT(transfer_status=2 or transfer_status=3) AS turnToArtificialNumber,SUM(transfer_status=3) AS manualConnectionHasBeenMade,SUM(transfer_status=2) AS turnManualNotConnected,SUM(calledflag>1) AS calledCount,SUM(calledflag=3) AS answeredCount,SUM(dealuser is not null) as getPhoneNum,SUM(case when key_value is not null  AND key_value!='' then 1 else 0 end) as keyValue,SUM(case when calledflag !=1 then 1 else 0 end) as callNum,aigrade,SUM(aigrade!='') as totalClassCount,SUM(calledflag=3) AS numberOfNumbersConnected,SUM(is_click=1) AS keyStrokes,SUM(quantity_obtained=1) AS acquiredQuantity,SUM(CASE WHEN dealresult_id=1 THEN 1 ELSE 0 END) AS visit_source,SUM(dealresult_id=2) as actualFailureList,SUM(CASE WHEN dealresult_id=3 THEN 1 ELSE 0 END) AS success_source ,SUM(CASE WHEN dealresult_id=2 THEN 1 ELSE 0 END) AS failure_source")->select();
			// $sql = "SELECT COUNT(*) AS totalnum,COUNT(transfer_status=2 and transfer_status=3) AS turnToArtificialNumber,SUM(transfer_status=3) AS manualConnectionHasBeenMade,SUM(transfer_status=2) AS turnManualNotConnected,SUM(calledflag>1) AS calledCount,SUM(calledflag=3) AS answeredCount,SUM(dealuser is not null) as getPhoneNum,SUM(case when key_value is not null  AND key_value!='' then 1 else 0 end) as keyValue,SUM(case when calledflag !=1 then 1 else 0 end) as callNum,aigrade,SUM(aigrade=1)as aClassCount,SUM(hangup_disposition =1) as customerHangUpRate ,SUM(hangup_disposition =2) as customerAleadyHangUp ,SUM(aigrade=2) as bClassCount,SUM(aigrade=3) as cClassCount,SUM(aigrade=4) as dClassCount,SUM(aigrade!='') as totalClassCount,SUM(calledflag=3) AS numberOfNumbersConnected,SUM(is_click=1) AS keyStrokes,SUM(quantity_obtained=1) AS acquiredQuantity,SUM(CASE WHEN dealresult_id=1 THEN 1 ELSE 0 END) AS visit_source,SUM(dealresult_id=2) as actualFailureList,SUM(CASE WHEN dealresult_id=3 THEN 1 ELSE 0 END) AS success_source ,SUM(CASE WHEN dealresult_id=2 THEN 1 ELSE 0 END) AS failure_source, FROM $ss";
			// $arr = $m->query($sql);

			$tpl_ST[$i]['turnToArtificialNumber'] = ($arr[0]['turnToArtificialNumber']==null)?'0 / 0':$arr[0]['turnToArtificialNumber'].'/'.$arr[0]['turnToArtificialNumber']/$arr[0]['totalnum']*100;
			$tpl_ST[$i]['manualConnectionHasBeenMade'] = ($arr[0]['manualConnectionHasBeenMade']==null)?'0 / 0':$arr[0]['manualConnectionHasBeenMade'].'/'.$arr[0]['manualConnectionHasBeenMade']/$arr[0]['totalnum']*100;
			$tpl_ST[$i]['turnManualNotConnected'] = ($arr[0]['turnManualNotConnected']==null)?'0 / 0':$arr[0]['turnManualNotConnected'].'/'.$arr[0]['turnManualNotConnected']/$arr[0]['totalnum']*100;
			$tpl_ST[$i]['customerHangUpRate'] = ($arr[0]['customerHangUpRate']==null)?'0 / 0':$arr[0]['customerHangUpRate']/$arr[0]['customerAleadyHangUp'];
			$tpl_ST[$i]['numberOfNumbersConnected'] = ($arr[0]['numberOfNumbersConnected']==null)?'0':$arr[0]['numberOfNumbersConnected'];
			##通话时长统计开始
			#SUM(CASE WHEN dealresult=1 THEN 1 ELSE 0 END) AS visit,
			$arrHis = M('sales_contact_history_'.$val['id'])->field("SUM(CASE WHEN dealresult=0 THEN 1 ELSE 0 END) AS untreated,SUM(CASE WHEN dealresult=1 THEN 1 ELSE 0 END) AS visit,SUM(CASE WHEN dealresult=2 THEN 1 ELSE 0 END) AS failure,SUM(CASE WHEN dealresult=3 THEN 1 ELSE 0 END) AS success")->select();
			##历史失败单
			$tpl_ST[$i]['historyFailureList'] = ($arrHis[0]['failure']==null)?'0':$arrHis[0]['failure'];
			##历史成功单
			$tpl_ST[$i]['historyOfSuccess'] = ($arrHis[0]['success']==null)?'0':$arrHis[0]['success'];
			##历史回访单,
			$tpl_ST[$i]['historicalReturnList'] = ($arrHis[0]['visit']==null)?'0':$arrHis[0]['visit'];
			## 实际失败单
			$tpl_ST[$i]['actualFailureList'] = ($arr[0]['failure_source']==null)?'0':$arr[0]['failure_source'];

			## 实际成功单
			$tpl_ST[$i]['actualSuccessSheet'] = ($arr[0]['success_source']==null)?'0':$arr[0]['success_source'];

			## 实际回访单
			$tpl_ST[$i]['actualReturnVisitForm'] = ($arr[0]['visit_source']==null)?'0':$arr[0]['visit_source'];
			$dataInfo = array();
			$dataInfo = M('sales_cdr_'.$val['id'])->query('select COUNT(*) AS totalnum,SUM(duration>0 ANDduration<5) as 5Duration,SUM(duration>5 AND duration<10) as 510Duration,SUM(disposition=ANSWERED) as answeredCount,SUM(duration>0) AS AllDuration,SUM(duration) as totalDuration,SUM(duration>10 AND duration<20) as 1020Duration,SUM(duration>20 AND duration<30) as 2030Duration,SUM(duration>30 AND duration<60) as 3060Duration,SUM(duration>60) as 60Duration from sales_cdr_'.$val['id']);
			$dataInfo[0]['totalDuration'] = ($dataInfo[0]['totalDuration']=='' || $dataInfo[0]['totalDuration']==null)?0:$dataInfo[0]['totalDuration'];
			##五秒内
			$tpl_ST[$i]['5Duration'] = ($dataInfo[0]['5Duration']==null)?'0 / 0':$dataInfo[0]['5Duration'] .'/'.$dataInfo[0]['5Duration']/$dataInfo[0]['totalDuration'];
			##5-10秒内
			$tpl_ST[$i]['510Duration'] = ($dataInfo[0]['510Duration']==null)?'0 / 0':$dataInfo[0]['510Duration'] .'/'.$dataInfo[0]['510Duration']/$dataInfo[0]['totalDuration'];
			##10-20秒内
			$tpl_ST[$i]['1020Duration'] = ($dataInfo[0]['1020Duration']==null)?'0 / 0':$dataInfo[0]['1020Duration'] .'/'.$dataInfo[0]['1020Duration']/$dataInfo[0]['totalDuration'];
			##20-30秒内
			$tpl_ST[$i]['2030Duration'] = ($dataInfo[0]['2030Duration']==null)?'0 / 0':$dataInfo[0]['2030Duration'] .'/'.$dataInfo[0]['2030Duration']/$dataInfo[0]['totalDuration'];
			##30-60秒内
			$tpl_ST[$i]['3060Duration'] = ($dataInfo[0]['3060Duration']==null)?'0 / 0':$dataInfo[0]['3060Duration'] .'/'.$dataInfo[0]['3060Duration']/$dataInfo[0]['totalDuration'];
			##大于60秒
			$tpl_ST[$i]['60Duration'] = ($dataInfo[0]['60Duration']==null)?'0 / 0':$dataInfo[0]['60Duration'] .'/'.$dataInfo[0]['60Duration']/$dataInfo[0]['totalDuration'];
			##有效通话次数
			$tpl_ST[$i]['numberOfActiveCalls'] = ($dataInfo[0]['answeredCount']==null)?'0':$dataInfo[0]['answeredCount'];
			##所有通话时长
			$tpl_ST[$i]['totalDuration'] = self::extendtion($dataInfo[0]['totalDuration']);
			##已平均接听时长
			$tpl_ST[$i]['averageAnsweringTime'] = self::extendtion($dataInfo[0]['totalDuration']/$dataInfo[0]['AllDuration']);
			##通话时长统计结束

			##已获取数量
			$tpl_ST[$i]['acquiredQuantity'] = ($arr[0]['acquiredQuantity']==null)?'0':$arr[0]['acquiredQuantity'];
			##接通率
			$tpl_ST[$i]['callCompletingRate'] = ($arr[0]['callCompletingRate']==null)?'0':round($arr[0]['callCompletingRate']/$arr[0]['totalnum'],2)*100;
			##按键次数
			$tpl_ST[$i]['keyStrokes'] = ($arr[0]['keyStrokes']==null)?'0':$arr[0]['keyStrokes'];
			##按键率
			$tpl_ST[$i]['theKeyRate'] = ($arr[0]['keyStrokes']==null)?'0':round($arr[0]['keyStrokes']/$arr[0]['theKeyRate'],2)*100;

			if( $arr[0]['aigrade'] !='' || $arr[0]['aigrade']!=null){
				foreach($AiGrade AS $v){
					$tpl_ST[$i]['aiGrade'.$v['0']] = $arr[0]['aiGrade'.$v['0']].' /  '.round($arr[0]['aiGrade'.$v['0']]/$arr[0]['totalClassCount'], 2)*100;
				}
				// $tpl_ST[$i]['aClass'] = $arr[0]['aClassCount'].' /  '.round($arr[0]['aClassCount']/$arr[0]['totalClassCount'], 2)*100;
				// $tpl_ST[$i]['bClass'] = $arr[0]['bClassCount'].' /  '.round($arr[0]['bClassCount']/$arr[0]['totalClassCount'], 2)*100;
				// $tpl_ST[$i]['cClass'] = $arr[0]['cClassCount'].' /  '.round($arr[0]['cClassCount']/$arr[0]['totalClassCount'], 2)*100;
				// $tpl_ST[$i]['dClass'] = $arr[0]['dClassCount'].' /  '.round($arr[0]['dClassCount']/$arr[0]['totalClassCount'], 2)*100;
				// $tpl_ST[$i]['eClass'] = $arr[0]['eClassCount'].' /  '.round($arr[0]['eClassCount']/$arr[0]['totalClassCount'], 2)*100;
				// $tpl_ST[$i]['fClass'] = $arr[0]['fClassCount'].' /  '.round($arr[0]['fClassCount']/$arr[0]['totalClassCount'], 2)*100;
			}else{
				foreach($AiGrade AS $v){
					$tpl_ST[$i]['aiGrade'.$v['0']] = 0;
				}
				// $tpl_ST[$i]['aClass'] = '0';
				// $tpl_ST[$i]['bClass'] = '0';
				// $tpl_ST[$i]['cClass'] = '0';
				// $tpl_ST[$i]['dClass'] = '0';
				// $tpl_ST[$i]['eClass'] = '0';
				// $tpl_ST[$i]['fClass'] = '0';
			} 
			$totalnum = $arr[0]['totalnum'];
			$arr[0]['calledCount'] = $arr[0]['calledCount']?$arr[0]['calledCount']:0; //已呼叫号码数量
			$arr[0]['answeredCount'] = $arr[0]['answeredCount']?$arr[0]['answeredCount']:0;  //接通的号码数量


			$tpl_ST[$i]["keyValue"] = $arr[0]["keyValue"];
			if($arr[0]["keyValue"]){
				$tpl_ST[$i]["keyRate"] = floor(($arr[0]["keyValue"]/$arr[0]["callNum"])*100);  //按键率
			}else{
				$tpl_ST[$i]["keyValue"] = 0;
				$tpl_ST[$i]["keyRate"] = "0";
			}

			$tpl_ST[$i]['totalnum'] = $totalnum ; //号码总量
			$tpl_ST[$i]['getPhoneNum'] = $arr[0]['getPhoneNum']; //坐席已获取数量
			$tpl_ST[$i]['calledCount'] = $arr[0]['calledCount']; //呼叫量
			$tpl_ST[$i]['answeredCount'] = $arr[0]['answeredCount']; //接通量
			$tpl_ST[$i]['leftCount'] = $arr[0]['totalnum'] - $arr[0]['calledCount']; //剩余量（还没呼叫的号码数量）
			if($arr[0]['calledCount']){
				$tpl_ST[$i]['answeredRate'] = floor(100*$arr[0]['answeredCount']/$arr[0]['calledCount']);  //接通率
			}else{
				$tpl_ST[$i]['answeredRate'] = "0";
			}
			##操作title
			$tpl_ST[$i][operating] = "操作";
			#实时通话按钮
			// if($val['taskSwitch'] =='Y'){
			// 	$tpl_ST[$i][_operatingState] = "<p id='status".$val['id']."'>运行中</p>";
			// }else{
			// 	$tpl_ST[$i][_operatingState] = "<p id='status".$val['id']."'>已停止</p>";

			// }
			// #状态
			// if($val['autocall_status'] =='Y'){
			// 	$tpl_ST[$i][_operating] = '<img style="width: 50px;
			// 	height: 25px;" onClick="turnSwitch(this,'.$val['id'].')" src="/images/switchOff.png">';
			// }else{
			// 	$tpl_ST[$i][_operating] = '<img style="width: 50px;
			// 	height: 25px;" onClick="turnSwitch(this,'.$val['id'].')" src="/images/switchOn.png">';

			// }
			// $tpl_ST[$i][MissionControl] = "<a href='/index.php?m=Telemarketing&a=newOutBoundList&task_id=".$val['id']."&calltype=autocall&title=外呼统计'>详情</a>";
			$tpl_ST[$i][MissionControl] = "<a href='/index.php?m=Telemarketing&a=newOutBoundList&task_id=".$val['id']."&name=".$val['name']."&deptId=1&calltype=autocall'>详情</a>";
			$tpl_ST[$i][RealTime] = "详情";
			if($arr[0]['calledCount']){
				$calledCount = $arr[0]['calledCount'];
				//$tpl_ST[$i]['percent'] = floor(100*$calledCount/$totalnum); //任务进度
				$tpl_ST[$i]['percent'] = sprintf("%01.2f",100*$calledCount/$totalnum);   //任务进度
			}else{
				$tpl_ST[$i]['percent'] = "0";
			}




			$tpl_ST[$i]['calltypes'] = $val["calltype"];
			$type = $val["calltype"];
			$status = $status_row[$val["calltype"]];
			$val["calltype"] = $status;
			$file_name = $db_name.":".$val['id'];

			$val["agentCount"] = $val['dept_id']?$arrDeptCount[$val['dept_id']]:0;
			if( file_exists("/var/tmp/autocall/$file_name") ){
				$val["taskStatus"] = "运行";
				$tpl_ST[$i][_operatingState] = "<p id='status".$val['id']."'>运行中</p>";
				$tpl_ST[$i][_operating] = '<img style="width: 50px;
				height: 25px;" onClick="turnSwitch(this,'.$val['id'].')" src="/images/switchOff.png">';
			}else{
				$val["taskStatus"] = "停止";
				$tpl_ST[$i][_operatingState] = "<p id='status".$val['id']."'>已停止</p>";
				$tpl_ST[$i][_operating] = '<img style="width: 50px;
				height: 25px;" onClick="turnSwitch(this,'.$val['id'].')" src="/images/switchOn.png">';
			}


			//$tpl_ST[$i][quality] = $quality[$val['id']];
			//$tpl_ST[$i][qualitybi] =  floor(($quality[$val['id']]/$val['qualitypercent'])*100);





			$i++;
		}
		// dump($dataInfo);die();

		unset($i);
		//dump($tpl_ST);die;
		$rowsList = count($tpl_ST) ? $tpl_ST : false;
		$arrtask["total"] = $count;
		$arrtask["rows"] = $rowsList;

		echo json_encode($arrtask);
	}


	/*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	//取下级部门
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		//$str = "'" . $arrDep[$dept_id]['id'] . "',";
		$str =  $arrDep[$dept_id]['id'] . ",";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}

	//显示增加任务
	function addTask(){
		checkLogin();

		$checkRole = getSysinfo();
		$this->assign("msg_concurrence",$checkRole[4]);
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("wj",$arrAL) ){
			$wj_display = "Y";
		}else{
			$wj_display = "N";
		}
		$this->assign('wj_display',$wj_display);

		//dump($_SESSION);die;
		$username = $_SESSION['user_info']['username'];
		$this->assign('username',$username);

		//取出部门
		$d_id = $_SESSION["user_info"]["d_id"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptId_Set = rtrim($deptst,",");
		$where = "1 ";
		if($username != "admin"){
			$where .= " AND d_id in ($deptId_Set)";
		}
		$department = M('department');
		$tpl_dept = $department->field("d_id,d_name")->where($where)->select();
		$this->assign("department",$tpl_dept);

		//中继
		$trunks = M('asterisk.trunks');
		$arrT = $trunks->select();
		$tpl_trunks = Array();
		foreach( $arrT AS $v ){
			if($v['tech']=='zap' or $v['tech']=='dahdi'){
				$tpl_trunks[] = Array("name"=>"DAHDI/".$v['channelid']);
			}elseif($v['tech']=='sip'){
				$tpl_trunks[] = Array("name"=>"SIP/".$v['channelid']);
			}elseif($v['tech']=='iax2'){
				$tpl_trunks[] = Array("name"=>"IAX2/".$v['channelid']);
			}else{
				;
			}
		}
		$this->assign('trunk',$tpl_trunks);

		//IVR
		$ivr = M('asterisk.recordings');
		$tpl_ivr = $ivr->field('id,displayname')->select();
		$this->assign('ivr_id',$tpl_ivr);

		$this->display();
	}


	//提交任务的表单
	function doAddTask(){
		$d_id = (int)$_POST['dept_id_queue'];
		$arrTask = array(
			'name'=> $_POST['name'],
			'runtime'=> $_REQUEST['time_group'],
			'hangup_by_idle'=> $_REQUEST['hangup_by_idle'],
			'createuser'=> $_POST['createuser'],
			'calltype'=> $_POST['calltype'],
			'createtime'=> Date('Y-m-d H:i:s'),
			'describle'=> $_POST['describle'],
			'questionnaire_id'=>$_POST['questionnaire_id'],
			'dept_id'=> $_POST['dept_id_queue'], //队列
			'toqueue_timeout'=> (int)$_POST['toqueue_timeout'],
			'maxrecall_times'=> (int)$_POST['maxrecall_times'],
			'concurrence'=> (int)$_POST['concurrence'],  //最大并发数
			'limitperselect'=> (int)$_POST['limitperselect'],
			'answer_target'=> $_POST['answer_target'],
			'join_queue_method'=> $_POST['join_queue_method'],
			'trunk'=> $_REQUEST['trunk_name'],
			'area_code'=> $_REQUEST['area_code'],
			'callout_pre'=> $_POST['callout_pre'],
			'callerid'=> $_POST['callerid'],
			'idle_ratio'=> $_POST['idle_ratio'],
			'ivr_id'=> $_POST['ivr_id'],
			'save_not_dnd'=> $_POST['save_not_dnd'],
			'autocall_status'=> $_POST['autocall_status'],
			'use_meeting'=> empty($_POST['use_meeting']) ? "N" : $_POST['use_meeting'] ,
			'enable'=> $_POST['enable'],
			'qualitypercent'=> $_POST['qualitypercent'],
			'needrecord'=> $_POST['needrecord'],
			//'displayfield'=>'{"name":["\u59d3\u540d","view","name"],"sex":{"0":"\u6027\u522b","2":"sex"},"age":{"0":"\u5e74\u9f84","2":"age"},"birthday":{"0":"\u751f\u65e5","2":"birthday"},"phone1":["\u7535\u8bdd\u53f7\u78011","view","phone1"],"phone2":{"0":"\u7535\u8bdd\u53f7\u78012","2":"phone2"},"fax":{"0":"\u4f20\u771f","2":"fax"},"email":{"0":"\u90ae\u4ef6","2":"email"},"company":{"0":"\u516c\u53f8\u540d\u79f0","2":"company"},"address":{"0":"\u5ba2\u6237\u5730\u5740","2":"address"},"country":{"0":"\u56fd\u5bb6  ","2":"country"},"province":{"0":"\u7701\u4efd","2":"province"},"city":{"0":"\u57ce\u5e02","2":"city"},"description":{"0":"\u5907\u6ce8","2":"description"},"createuser":{"0":"\u521b\u5efa\u4eba","2":"createuser"},"createtime":{"0":"\u521b\u5efa\u65f6\u95f4","2":"createtime"},"modifytime":{"0":"\u4fee\u6539\u65f6\u95f4","2":"modifytime"},"salary":{"0":"\u5de5\u8d44","2":"salary"},"brand":{"0":"\u54c1\u724c","2":"brand"},"callbacktime":[" \u56de\u8bbf\u65f6\u95f4","view","callbacktime"]}',

		);

		$sales_task = M('sales_task');
		$res = $sales_task->add($arrTask);
		$last_id = $sales_task->getLastInsID();
		//dump( $sales_task->getLastSql() );die;

		//创建号码表
		$ss = M('sales_source');
		//创建号码资源包的表名
		$ss->execute("create table sales_source_$last_id like sales_source");
		$ss->execute("create table sales_cdr_$last_id like sales_cdr");
		$ss->execute("create table sales_contact_history_$last_id like sales_contact_history");
		$ss->execute("create table task_result_status_$last_id like task_result_status");
		$ss->execute("create table sales_proposal_record_$last_id like sales_proposal_record");


		//问卷
		$ss->execute("create table wj_questionnaire_answers_$last_id like wj_questionnaire_answers");
		$ss->execute("create table wj_questionnaire_answers_detail_$last_id like wj_questionnaire_answers_detail");


		//添加队列设置-----此段代码等有时间后调整
		if( $_POST['calltype'] == "autocall" ){
			$arrQueue = BG_read_from_ini_format("/etc/asterisk/queues_custom.conf");
			$queueName = "AutoCall_".$last_id;
			if( empty($arrQueue[$queueName]) ){
				/*
				strategy的值可以有以下几种：
				ringall 全部响铃
				leastrecent 最久未呼叫的坐席
				fewestcalls 最少呼叫的坐席
				rrmemory 记忆性搜寻
				random 随机
				linear linear
				wrandom wrandom
				*/
				$arrQueue[$queueName]= Array(
					"joinempty"	=> "yes",
					"autofill"	=> "yes",
					"leavewhenempty"	=>	"no",
					"maxlen"	=>	"0",
					"maxlen"	=>	"0",
					"strategy"	=>	"rrmemory",
					"timeout"	=>	"15",
					"ringinuse"	=>	"no",
				);
				BG_write_to_ini_format("/etc/asterisk/queues_custom.conf",$arrQueue);
				//重载AMI
				import('ORG.Pbx.bmi');
				$bmi = new bmi();
				$bmi->loadAGI();
				$bmi->asm->Command("queue reload all");

				//管理员添加队列成员
				if( $_POST['join_queue_method'] == 'by-admin' ){
					$users = M('users');
					$arrExten = $users->field('extension')->where("extension IS NOT NULL AND extension_type='sip' AND d_id=$d_id")->select();
					foreach( $arrExten AS $row ){
						$ext = $row['extension'];
						$action = "QueueAdd";
						$parameters = Array(
							"Queue"	=>	"AutoCall_${last_id}",
							"Interface"	=>	"Local/$ext@BG-QueueAgent/n",
							"Penalty"	=>	"0",
							"Paused"	=>	"no",
							"MemberName"	=>	"$ext",
						);
						//dump($parameters);die;
						$bmi->asm->send_request($action,$parameters);
					}
				}elseif( $_POST['join_queue_method'] == 'by-select' ){
					$arrExten = $_REQUEST["join_queue_method2"];
					foreach( $arrExten AS $row ){
						$ext = $row['extension'];
						$action = "QueueAdd";
						$parameters = Array(
							"Queue"	=>	"AutoCall_${last_id}",
							"Interface"	=>	"Local/$ext@BG-QueueAgent/n",
							"Penalty"	=>	"0",
							"Paused"	=>	"no",
							"MemberName"	=>	"$ext",
						);
						//dump($parameters);die;
						$bmi->asm->send_request($action,$parameters);
					}
				}
			}
		}
		if ($res){

			$ss->execute("ALTER TABLE sales_source_$last_id  MODIFY `task_id`  int(11)  DEFAULT '$last_id' COMMENT '任务id'  AFTER id");
			$ss->execute("ALTER TABLE sales_cdr_$last_id  MODIFY `task_id`  int(11)  DEFAULT '$last_id' COMMENT '任务id'  AFTER id");
			$arrData = $sales_task->field("id,name")->where("enable = 'Y'")->select();
			foreach($arrData as $val){
				$arrFS[] = "sales_source_".$val["id"];
				$arrFC[] = "sales_cdr_".$val["id"];
			}
			$source_table = implode(",",$arrFS);
			$cdr_table = implode(",",$arrFC);
			//"ALTER TABLE cdr_all_tenant ENGINE=MRG_MYISAM UNION=($union_sql) INSERT_METHOD=LAST";
			$ss->execute("DROP TABLE sales_source_all");
			$ss->execute("CREATE TABLE sales_source_all LIKE sales_source");
			$ss->execute("ALTER TABLE sales_source_all ENGINE =MERGE");
			$ss->execute("ALTER TABLE sales_source_all ENGINE=MRG_MYISAM UNION=($source_table) INSERT_METHOD=LAST");

			$ss->execute("DROP TABLE sales_cdr_all");
			$ss->execute("CREATE TABLE sales_cdr_all LIKE sales_cdr");
			$ss->execute("ALTER TABLE sales_cdr_all ENGINE =MERGE");
			$ss->execute("ALTER TABLE sales_cdr_all ENGINE=MRG_MYISAM UNION=($cdr_table) INSERT_METHOD=LAST");

			echo json_encode(array('success'=>true,'msg'=>'添加成功'));
		} else {
			echo json_encode(array('msg'=>'添加失败'));
		}
	}


	//编辑任务
	function editTask(){
		checkLogin();
		$checkRole = getSysinfo();
		$this->assign("msg_concurrence",$checkRole[4]);
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("wj",$arrAL) ){
			$wj_display = "Y";
		}else{
			$wj_display = "N";
		}
		$this->assign('wj_display',$wj_display);

		$id = $_GET['id'];
		$this->assign('id',$id);
		$sales_task = M('sales_task');
		$arrTask = $sales_task->where("id=$id")->find();
		$arrTask['runtime'] = json_decode($arrTask['runtime'],true);
		$i = 0;
		foreach($arrTask['runtime'] as $val){
			$arrTask['runtime'][$i]['order'] = $i;
			$i++;
		}
		unset($i);
		$count = count($arrTask['runtime']);
		$this->assign("count",$count);

		//dump($arrTask);die;

		//呼叫类型select标签的显示
		$arrTask['select_calltype'] = array();
		$arrTask['select_calltype'][0] = array(
			'value' => 'preview',
			'select' => ("preview" == $arrTask['calltype'])?'selected':"",
			'display' => '预览式外呼',
		);
		$arrTask['select_calltype'][1] = array(
			'value' => 'autocall',
			'select' => ("autocall" == $arrTask['calltype'])?'selected':"",
			'display' => '自动外呼',
		);

		//自动外呼应答目标
		$arrTask['radio_queue'] = $arrTask['radio_ivr'] = "";
		if( $arrTask['answer_target'] == "queue" ){
			$arrTask['radio_queue'] = 'checked';
		}elseif($arrTask['answer_target'] == "ivr_hungup"){
			$arrTask['radio_hungup'] = 'checked';
		}else{
			$arrTask['radio_ivr'] = 'checked';
		}
		//坐席加入队列的方式
		$arrTask['radio_by-self'] = $arrTask['radio_by-admin'] = $arrTask['radio_by-DND'] = "";
		if( $arrTask['join_queue_method'] == "by-self" ){
			$arrTask['radio_by-self'] = 'checked';
		}elseif( $arrTask['join_queue_method'] == "by-admin" ){
			$arrTask['radio_by-admin'] = 'checked';
		}elseif( $arrTask['join_queue_method'] == "by-select" ){
			$arrTask['radio_by-select'] = 'checked';
		}else{
			$arrTask['radio_by-DND'] = 'checked';
		}

		//部门下拉列表的显示
		if( $arrTask['calltype']== "preview" ){ //是否禁用
			$arrTask['disabled_dept_id'] = 'disabled="true"';
			$arrTask['display_none'] = 'style="display:none;"';
		}
		//取出部门
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION["user_info"]["d_id"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptId_Set = rtrim($deptst,",");
		$where = "1 ";
		if($username != "admin"){
			$where .= " AND d_id in ($deptId_Set)";
		}
		$arrTask['select_dept_id'] = Array();
		$department = M('department');
		$tpl_dept = $department->field("d_id,d_name")->where($where)->select();
		foreach( $tpl_dept AS $row ){
			$tmp = ($row['d_id'] == $arrTask['dept_id'])?'selected':"";
			$arrTask['select_dept_id'][]=Array(
				'value' => $row['d_id'],
				'select' => $tmp,
				'display' => $row['d_name'],
			);
		}
		//最大并发
		if( $arrTask['calltype']== "preview" ){ //是否禁用
			$arrTask['disabled_concurrence'] = 'disabled="true"';
		}
		//中继
		if( $arrTask['calltype']== "preview" ){ //是否禁用
			$arrTask['disabled_trunk'] = 'disabled="true"';
		}
		$trunks = M('asterisk.trunks');
		$arrT = $trunks->select();
		$tpl_trunks = Array();
		foreach( $arrT AS $v ){
			if( $v['tech']=='zap' or $v['tech']=='dahdi' ){
				$tpl_trunks[] = Array("name"=>"DAHDI/".$v['channelid']);
			}elseif($v['tech']=='sip'){
				$tpl_trunks[] = Array("name"=>"SIP/".$v['channelid']);
			}elseif($v['tech']=='iax2'){
				$tpl_trunks[] = Array("name"=>"IAX2/".$v['channelid']);
			}else{
				;
			}
		}
		$arrTask['select_trunk'] = Array();
		foreach( $tpl_trunks AS $row ){
			$tmp = ($row['name'] == $arrTask['trunk'])?'selected':"";
			$arrTask['select_trunk'][]=Array(
				'value' => $row['name'],
				'select' => $tmp,
				'display' => $row['name'],
			);
		}
		//IVR
		if( $arrTask['calltype']== "preview" ){ //是否禁用
			$arrTask['disabled_ivr_id'] = 'disabled="true"';
		}

		//Jack修改
		$ivr = M('asterisk.recordings');
		$tpl_ivr = $ivr->field("id,displayname")->select();
		$arrTask['select_ivr_id'] = Array();
		foreach( $tpl_ivr AS $row ){
			$tmp = ($row['displayname'] == $arrTask['ivr_id'])?'selected':"";
			$arrTask['select_ivr_id'][]=Array(
				'value' => $row['id'],
				'select' => $tmp,
				'display' => $row['displayname'],
			);
		}


		$arrTask['dndtime'] = json_decode($arrTask['dndtime'],true);
		$i = 0;
		foreach($arrTask['dndtime'] as $val){
			$arrTask['dndtime'][$i]['order'] = $i;
			$i++;
		}
		unset($i);
		$dndtime_count = count($arrTask['dndtime']);
		$this->assign("dndtime_count",$dndtime_count);


		$this->assign('T',$arrTask);
		$this->assign('department',$tpl_dept);

		//前台获取条件信息
		$get_conditions = json_decode($arrTask["get_conditions"],true);
		$arrF = explode(",",$get_conditions["value"]);
		foreach($arrF as $val){
			$arrD[] = explode(":",$val);
		}
		foreach($arrD as $key=>$val){
			$arrCondittions[trim($val[0])] = $val[1];
		}
		$this->assign('arrCondittions',$arrCondittions);
		//dump($arrCondittions);die;
		//dump($arrTask);die;

		$this->display();
	}

		//保存编辑
	function doEditTask(){
		if(! $_POST['id'])die;
		$id = $_POST['id'];
		$d_id = (int)$_POST['dept_id_queue'];
		/*
		$runtime = $_REQUEST['runtime'];
		foreach($runtime as $val){
			$tt[] = array(
						"from"=>$val[0],
						"to"=>$val[1],
					);
		}
		$jsonRuntime = json_encode($tt);
		*/
		//dump($d_id);die;
		//生成免打扰时间json
		$times = $_REQUEST['time'];
		$tt = Array();
		foreach($times as $val){
			$tt[] = array(
						"from"=>$val[0],
						"to"=>$val[1],
					);
		}
		$dndtime = json_encode($tt);


		$arrTask = array(
			'name'=> $_POST['name'],
			'runtime'=> $_REQUEST['time_group'],
			'hangup_by_idle'=> $_REQUEST['hangup_by_idle'],
			'createuser'=> $_POST['createuser'],
			'calltype'=> $_POST['calltype'],
			//'createtime'=> Date('Y-m-d H:i:s'),
			//'createtime'=> $_POST['calltype'],
			'describle'=> $_POST['describle'],
			'questionnaire_id'=>$_POST['questionnaire_id'],
			'dept_id'=> $d_id,
			'toqueue_timeout'=> (int)$_POST['toqueue_timeout'],
			'maxrecall_times'=> (int)$_POST['maxrecall_times'],
			'concurrence'=> (int)$_POST['concurrence'],
			'limitperselect'=> (int)$_POST['limitperselect'],
			'answer_target'=> $_POST['answer_target'],
			'join_queue_method'=> $_POST['join_queue_method'],
			'callout_pre'=> $_POST['callout_pre'],
			'trunk'=> $_REQUEST['trunk_name'],
			'area_code'=> $_REQUEST['area_code'],
			'callerid'=> $_POST['callerid'],
			'idle_ratio'=> $_POST['idle_ratio'],
			'ivr_id'=> $_POST['ivr_id'],
			'save_not_dnd'=> $_POST['save_not_dnd'],
			'autocall_status'=> $_POST['autocall_status'],
			'use_meeting'=> empty($_POST['use_meeting']) ? "N" : $_POST['use_meeting'] ,
			'enable'=> $_POST['enable'],
			'qualitypercent'=> $_POST['qualitypercent'],
			'needrecord'=> $_POST['needrecord'],
			'starttime'	=>$_REQUEST['starttime'],
			'endtime'	=>$_REQUEST['endtime'],
			'dndtime'	=>$dndtime,
		);
		//dump($arrTask);die;
		$sales_task = M('sales_task');
		$res = $sales_task->where("id=$id")->save($arrTask);

		//dump($sales_task->getLastSql());die;
		if( "autocall" == $_POST['calltype'] ){
			$arrTmp = $sales_task->where("calltype='autocall' AND enable='Y'")->select();
			$arr_Db_Queue = Array();
			foreach($arrTmp AS $v){
				$arr_Db_Queue['AutoCall_'.$v['id']] = true;
			}
			//dump($arr_Db_Queue);die;
			//添加队列成员 /etc/asterisk/queues_custom.conf
			$arrQueue = BG_read_from_ini_format("/etc/asterisk/queues_custom.conf");
			//dump($arrQueue);die;
			$myQueueName = "AutoCall_".$id;
			if(! isset($arrQueue[$myQueueName]) ){
				$arrQueue[$myQueueName] = Array(
					"joinempty"	=> "yes",
					"autofill"	=> "yes",
					"leavewhenempty"	=>	"no",
					"maxlen"	=>	"0",
					"maxlen"	=>	"0",
					"strategy"	=>	"rrmemory",
					"timeout"	=>	"15",
					"ringinuse"	=>	"no",

				);
			}
			//dump($arrQueue);die;
			//数据库中没有，配置文件中有的，自然要删掉
			foreach($arrQueue AS $key=>$val){
				if( !isset($arr_Db_Queue[$key]) ){
					unset($arrQueue[$key]);
				}
			}
			//dump($arrQueue);die;
			BG_write_to_ini_format("/etc/asterisk/queues_custom.conf",$arrQueue);
			import('ORG.Pbx.bmi');
			$bmi = new bmi();
			$bmi->loadAGI();
			$bmi->asm->command("queue reload all");
			if( $_POST['join_queue_method'] == 'by-admin' ){
				//重载AMI
				//添加之前先清空原来的队列成员
				$memberInfo = $bmi->getDynamicQueueMember("AutoCall_$id");
				if( $memberInfo ){
					foreach( $memberInfo AS $v ){
						$cliCmd = "queue remove member ".$v['interface']." from AutoCall_$id";
						$bmi->asm->command($cliCmd);
					}
				}
				//die;
				$users = M('users');
				$arrExten = $users->field('extension')->where("extension IS NOT NULL AND extension_type='sip' AND d_id=$d_id")->select();
				//dump($arrExten);die;
				foreach( $arrExten AS $row ){
					$ext = $row['extension'];
					$action = "QueueAdd";
					$parameters = Array(
						"Queue"	=>	"AutoCall_${id}",
						"Interface"	=>	"Local/$ext@BG-QueueAgent/n",
						"Penalty"	=>	"0",
						"Paused"	=>	"no",
						"MemberName"	=>	"$ext",
					);
					//dump($parameters);
					$bmi->asm->send_request($action,$parameters);
				}
			}elseif( $_POST['join_queue_method'] == 'by-select' ){
				//重载AMI
				//添加之前先清空原来的队列成员
				$memberInfo = $bmi->getDynamicQueueMember("AutoCall_$id");
				if( $memberInfo ){
					foreach( $memberInfo AS $v ){
						$cliCmd = "queue remove member ".$v['interface']." from AutoCall_$id";
						$bmi->asm->command($cliCmd);
					}
				}
				$arrExten = $_REQUEST["join_queue_method2"];
				foreach( $arrExten AS $row ){
					$ext = $row['extension'];
					$action = "QueueAdd";
					$parameters = Array(
						"Queue"	=>	"AutoCall_$id",
						"Interface"	=>	"Local/$ext@BG-QueueAgent/n",
						"Penalty"	=>	"0",
						"Paused"	=>	"no",
						"MemberName"	=>	"$ext",
					);
					//dump($parameters);die;
					$bmi->asm->send_request($action,$parameters);
				}
			}
		}else{
			//删除队列配置文件中的记录 /etc/asterisk/queues_custom.conf
			$arrQueue = BG_read_from_ini_format("/etc/asterisk/queues_custom.conf");
			$myQueueName = "AutoCall_".$id;
			if( isset($arrQueue[$myQueueName]) ){unset($arrQueue[$myQueueName]);}
			BG_write_to_ini_format("/etc/asterisk/queues_custom.conf",$arrQueue);
		}
		if ($res !== false){

			$arrData = $sales_task->field("id,name")->where("enable = 'Y'")->select();
			foreach($arrData as $val){
				$arrFS[] = "sales_source_".$val["id"];
				$arrFC[] = "sales_cdr_".$val["id"];
			}
			$source_table = implode(",",$arrFS);
			$cdr_table = implode(",",$arrFC);
			//"ALTER TABLE cdr_all_tenant ENGINE=MRG_MYISAM UNION=($union_sql) INSERT_METHOD=LAST";
			$sales_task->execute("DROP TABLE sales_source_all");
			$sales_task->execute("CREATE TABLE sales_source_all LIKE sales_source");
			$sales_task->execute("ALTER TABLE sales_source_all ENGINE =MERGE");
			$sales_task->execute("ALTER TABLE sales_source_all ENGINE=MRG_MYISAM UNION=($source_table) INSERT_METHOD=LAST");

			$sales_task->execute("DROP TABLE sales_cdr_all");
			$sales_task->execute("CREATE TABLE sales_cdr_all LIKE sales_cdr");
			$sales_task->execute("ALTER TABLE sales_cdr_all ENGINE =MERGE");
			$sales_task->execute("ALTER TABLE sales_cdr_all ENGINE=MRG_MYISAM UNION=($cdr_table) INSERT_METHOD=LAST");


			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}


	//前台字段显示
	function fieldDisplay(){
		checkLogin();
		$id = $_GET['id'];
		$this->assign('id',$id);
		$sales_task = new Model("sales_task");
		//$arrTask = $sales_task->field('displayfield')->where("id=$id")->find();
		$arrTask = $sales_task->where("id=$id")->find();
		//dump($arrTask);die;
		$arrTmp = json_decode($arrTask['displayfield'],true);
		$tpl_arrChecked = Array();
		foreach( $arrTmp AS $key=>$val){
			$tpl_arrChecked[$key] = Array();
			if( in_array("view",$val) ){
				$tpl_arrChecked[$key]['view'] = "checked";
			}
			if( in_array("edit",$val) ){
				$tpl_arrChecked[$key]['edit'] = "checked";
			}
			$tpl_arrChecked[$key]["name"] = $val[0];
			if($val[0] == ""){
				$this->assign($key,1);
			}else{
				$this->assign($key,0);
			}
		}
		if(empty($tpl_arrChecked)){
			$this->assign("deft",1);
		}else{
			$this->assign("deft",0);
		}
		//dump($tpl_arrChecked);die;
		$this->assign("checked",$tpl_arrChecked);
		$this->assign("taskname",$arrTask["name"]);
		$this->display();
	}

	function saveFieldDisplay(){
		//dump($_POST);
		$id = $_POST['id'];
		$sales_task = new Model("sales_task");
		$arrData = $_POST;
		array_pop($arrData);
		array_shift($arrData);
		//dump($arrData);die;
		$strPriv = json_encode($arrData);
		$result = $sales_task->where("id = $id")->save( array("displayfield"=>$strPriv) );
		//echo $sales_task->getLastSql();die;
		if($result !== false){
			goback("设置成功","index.php?m=Telemarketing&a=fieldDisplay&id=$id");
		}
	}


	//设置队列参数
	function editQueueParam(){




	}



	//移除队列成员
	function removeQueueAgent(){
		$queuename = "AutoCall_". $_REQUEST['id'];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		//添加之前先清空原来的队列成员
		$memberInfo = $bmi->getDynamicQueueMember($queuename);
		//dump($memberInfo);die;
		if( $memberInfo ){
			foreach( $memberInfo AS $v ){
				$cliCmd = "queue remove member ".$v['interface']." from $queuename";
				$bmi->asm->command($cliCmd);
			}
		}
		$msg = "已移除队列${queuename}全部成员!";
		echo json_encode(array('success'=>true,'msg'=>$msg));
		//goBack("已移除队列${queuename}全部成员!","index.php?m=Telemarketing&a=listAutocall");
	}

	//移到回收站
	function removeToRecycleBin(){
		$id = $_REQUEST['id'];
		$sales_task = M('sales_task');
		$result = $sales_task->where("id=$id")->save(array("enable"=>'N'));//修改一下状态就行
		//echo $sales_task->getLastSql();die;
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//显示回收站的项目
	function listRecycleBin(){
		checkLogin();
		$this->display();
	}

	function recycleBinData(){
		$sales_task = M('sales_task');
		$count = $sales_task->table("sales_task st")->join("left join department d on st.dept_id=d.d_id")->where("enable='N'")->count();

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = BG_Page($count,$page_rows);//这个地方要修改

		$tpl_ST = $sales_task->table("sales_task st")->join("left join department d on st.dept_id=d.d_id")->where("enable='N'")->order("createtime desc")->limit($page->limit)->select();

		$preview = L('Preview outbound calls');
		$autocall = L('Automatic outbound calls');
		$status_row = array('preview'=>$preview,'autocall'=>$autocall);
		$i = 0;
		foreach($tpl_ST as &$val){
			$status = $status_row[$val["calltype"]];
			$val["calltype"] = $status;
			//$tpl_ST[$i]['operation'] = "<a target='_self' href='index.php?m=Telemarketing&a=recoverFromRecycleBin&id=" .$val["id"] ."'> 还原 </a>  "."<a target='_self' href='index.php?m=Telemarketing&a=deleteFromRecycleBin&id=" .$val["id"] ."'> 清除 </a>" ;
			$tpl_ST[$i]['operation'] = "<a href='javascript:void(0);' onclick=\"resetTask("."'".$val["id"]."'".")\" >". 还原 ."</a>";
			$tpl_ST[$i]['operation'] .= "<a href='javascript:void(0);' onclick=\"removeTask("."'".$val["id"]."'".")\" >&nbsp;&nbsp;清除 </a>";
			$i++;
		}
		//dump($tpl_ST);die;
		$rowsList = count($tpl_ST) ? $tpl_ST : false;
		$arrtask["total"] = $count;
		$arrtask["rows"] = $rowsList;

		echo json_encode($arrtask);
	}


	//删除任务---->从回收站彻底清除
	function deleteFromRecycleBin(){
		$id = $_REQUEST['id'];
		$sales_task = M('sales_task');
		$res = $sales_task->where("id=$id")->delete();
		//删除号码资源表
		$ss = M('sales_source');
		$ss->execute("drop table sales_source_$id");
		$ss->execute("drop table sales_cdr_$id");
		$ss->execute("drop table sales_contact_history_$id");
		$ss->execute("drop table task_result_status_$id");
		$ss->execute("drop table sales_proposal_record_$id");

		//问卷
		$ss->execute("drop table wj_questionnaire_answers_$id");
		$ss->execute("drop table wj_questionnaire_answers_detail_$id");

		//删除队列配置文件中的记录 /etc/asterisk/queues_custom.conf
		$arrQueue = BG_read_from_ini_format("/etc/asterisk/queues_custom.conf");
		$myQueueName = "AutoCall_".$id;
		unset($arrQueue[$myQueueName]);
		BG_write_to_ini_format("/etc/asterisk/queues_custom.conf",$arrQueue);

		if( $res > 0 ){
			//goBack("清除成功!","index.php?m=Telemarketing&a=listRecycleBin");
			$arrData = $sales_task->field("id,name")->where("enable = 'Y'")->select();
			foreach($arrData as $val){
				$arrFS[] = "sales_source_".$val["id"];
				$arrFC[] = "sales_cdr_".$val["id"];
			}
			$source_table = implode(",",$arrFS);
			$cdr_table = implode(",",$arrFC);
			//"ALTER TABLE cdr_all_tenant ENGINE=MRG_MYISAM UNION=($union_sql) INSERT_METHOD=LAST";
			$ss->execute("ALTER TABLE sales_source_all ENGINE=MRG_MYISAM UNION=($source_table) INSERT_METHOD=LAST");
			$ss->execute("ALTER TABLE sales_cdr_all ENGINE=MRG_MYISAM UNION=($cdr_table) INSERT_METHOD=LAST");

			echo json_encode(array('success'=>true));
		}else{
			//goBack("清除失败!","index.php?m=Telemarketing&a=listRecycleBin");
			echo json_encode(array('msg'=>'清除失败'));
		}
	}


	//从回收站还原
	function recoverFromRecycleBin(){
		$id = $_REQUEST['id'];
		$sales_task = M('sales_task');
		$res = $sales_task->where("id=$id")->save(array("enable"=>'Y'));//修改一下状态就行
		if( $res > 0 ){
			//goBack("任务已经还原!","index.php?m=Telemarketing&a=listRecycleBin");
			echo json_encode(array('success'=>true));
		}else{
			//goBack("任务还原失败!","index.php?m=Telemarketing&a=listRecycleBin");
			echo json_encode(array('msg'=>'任务还原失败'));
		}

	}


	//清空回收站
	function clearRecycleBin(){



	}




	//==========================================号码包资源管理
	//号码包资源
	function NumberSource(){
		checkLogin();
		$menuname = "Number Resource";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function NumberSourceData(){
		$sales_task = M('sales_task');
		$count = $sales_task->table("sales_task st")->join("left join department d on st.dept_id=d.d_id")->where("enable='Y'")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = BG_Page($count,$page_rows);//这个地方要修改

		$tpl_ST = $sales_task->table("sales_task st")->join("left join department d on st.dept_id=d.d_id")->where("enable='Y'")->order("createtime desc")->limit($page->limit)->select();

		$preview = L('Preview outbound calls');
		$autocall = L('Automatic outbound calls');
		$status_row = array('preview'=>$preview,'autocall'=>$autocall);
		$i = 0;
		foreach($tpl_ST as &$val){
			$status = $status_row[$val["calltype"]];
			$val["calltype"] = $status;

			$tpl_ST[$i]['operation'] = "<a target='_self' href='index.php?m=Telemarketing&a=importNumber&id=" .$val["id"] ."'>".L('Import numbers')."</a>";
			$i++;
		}

		$rowsList = count($tpl_ST) ? $tpl_ST : false;
		$arrtask["total"] = $count;
		$arrtask["rows"] = $rowsList;

		echo json_encode($arrtask);
	}


	//导入号码库资源
	function importNumber(){
		checkLogin();
	
		$id = $_GET['id'];
		$this->assign('id',$id);
		$sales_task = M('sales_task');
		$arrTask = $sales_task->where("id=$id")->find();
 

		//呼叫类型select标签的显示
		if("preview" == $arrTask['calltype']){
			$arrTask['select_calltype'] = "预览式外呼";
		}else{
			$arrTask['select_calltype'] = "自动外呼";
		}

		//部门
		$d_id = $arrTask['dept_id'];
		$department = M('department');
		$tpl_dept = $department->field("d_id,d_name")->where("d_id=$d_id")->find();
		$arrTask['select_dept_id'] = $tpl_dept['d_name'];
		$this->assign('T',$arrTask);

		$table = "sales_source_".$id;
		$ss = M($table);
		//$count = $ss->where("locked='N'")->count();
		$count = $ss->count();
		$para_sys = readS();
		$size = $para_sys["page_rows"];
		$page = BG_Page($count,$size);//这个地方要修改
		$show = $page->show();
		$this->assign("page",$show); 
		$this->assign('CallResult',M('noanswer')->select());
		//$tpl_SS = $ss->where("locked='N'")->limit($page->limit)->select();
		$tpl_SS = $ss->limit($page->limit)->select();
		$i = 0;
		foreach( $tpl_SS AS $val ){
			if( $val['calledflag'] == 1 ){
				$tpl_SS[$i]['callresult'] = "未呼叫";
			}else if($val['calledflag'] == 2){
				$tpl_SS[$i]['callresult'] = "未接听";
			}else if($val['calledflag'] == 3){
				$tpl_SS[$i]['callresult'] = "已接听";
			}
			$i++;
		}
		unset($i);
		//dump($tpl_SS);die;
		$this->assign('SS',$tpl_SS);

		$menuname = "Task";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}
		$this->assign('aiGrade',$this->unicode_decode(M('customer_fields')->where(array('id'=>32))->getField('field_values')));
		$this->assign('personGrade',$this->unicode_decode(M('customer_fields')->where(array('id'=>9))->getField('field_values')));
		$this->assign('FollowUp',$this->unicode_decode(M('customer_fields')->where(array('id'=>34))->getField('field_values')));
		$this->assign("priv",$priv);
		 
		$this->display();

	}

	function numberData(){
		$id = $_GET['id'];
		$callstatus = $_GET['callstatus'];
		$table = "sales_source_".$id;
		$source = new Model($table);
		if($callstatus){
			$count = $source->where("calledflag>1")->count();
		}else{
			$count = $source->count();
		}
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($callstatus){
			$phoneData = $source->where("calledflag>1")->limit($page->firstRow.','.$page->listRows)->order("id desc")->select();
		}else{
			$phoneData = $source->limit($page->firstRow.','.$page->listRows)->order("id desc")->select();
		}

		$status_row = array('1'=>'未呼叫','2'=>'未接听','3'=>'已接听');
		$sex_row = array('man'=>'男','woman'=>'女');
		foreach($phoneData as &$val){
			$calledflag = $status_row[$val['calledflag']];
			$val['calledflag'] = $calledflag;
			$sex = $sex_row[$val['sex']];
			$val['sex'] = $sex;
		}


		//dump($phoneData);die;
		$rowsList = count($phoneData) ? $phoneData : false;
		$arrPhone["total"] = $count;
		$arrPhone["rows"] = $rowsList;

		echo json_encode($arrPhone);
	}

	function test(){
		$tt = "13417529056";
		for($i=0;$i<=10000;$i++){
			$rr[] = $tt.$i;
		}
		$yy = implode("\r\n",$rr);
		file_put_contents("BGCC/Conf/ttt4.txt",$yy);
		dump($yy);
	}

	function doImportTxtNumber(){
		set_time_limit(0);
		ini_set('memory_limit','512M');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);

		$id = $_GET['id'];
		$this->assign('id',$id);
		if( empty($_FILES["client_phones_txt"]["tmp_name"]) ){
			goBack("请选择文件!","");
		}

		$time = time();
		$tmp_f = $_FILES["client_phones_txt"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["client_phones_txt"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		//黑名单数据检测
		$DNC = M('sales_dnc');
		$arrDNC = Array();
		$arrTmp = $DNC->select();
		foreach( $arrTmp AS $row){
			$arrDNC[] = $row['phonenumber'];
		}
		//原来就存在表中的电话号码，导入的时候不能和你面重复
		$table = "sales_source_" .$id;
		$SS = M($table);
		//去重
		$arrTmp = $SS->field('phone1')->select();
		$arrExist = Array(); //把所有号码放入关联数组，hash
		foreach( $arrTmp AS $row){
			$arrExist[$row['phone1']] = 'Y';
		}
		$sql = "insert into $table(phone1,createtime) values ";
		$value = "";

		$total = $black = $repeat = $valid = $invalid = 0;
		$dst_f = TMP_UPLOAD ."client_phones_".$time.".csv";
		move_uploaded_file($tmp_f,$dst_f);

		$strphones = trim( file_get_contents($dst_f) );
		$arrPhones2 = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
		$arrPhones = array_unique($arrPhones2);


		$total = count($arrPhones);
		//dump($arrPhones);die;
			foreach( $arrPhones AS $key=>$row ){
				$row = trim($row);
				if( in_array($row,$arrDNC) ){
					$black++;
					continue;
				}elseif( $arrExist[$row] == 'Y'){
					$repeat++;
					continue;
				}else{
					if( is_numeric($row) ){
						$valid++;
						$ph = str_replace("-","",$row);
						$ph = str_replace(" ","",$row);
						//$str = "('".$ph."','".date('Y-m-d H:i:s')."')";
						$str = "('" .$ph ."',NOW())";
						$value .= empty($value)?$str:",$str";
						$arrExist[$row] = 'Y';   //成功的号码需要加入到去重数组中
					}else{
						$invalid++;
					}
				}
			}

		//echo "$value";die;
		if( $value ){
			$sql .= $value;
			$result = $SS->execute($sql);
		}
		//判断导入结果
		if( $result ){
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${total}个号码,${black}个在黑名单中,${repeat}个重复号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}
	}

	function doImportTxtNumber4444444444(){
		set_time_limit(0);
		ini_set('memory_limit','400M');

		$id = $_GET['id'];
		$this->assign('id',$id);
		if( empty($_FILES["client_phones_txt"]["tmp_name"]) ){
			goBack("请选择文件!","");
		}

		$time = time();
		$tmp_f = $_FILES["client_phones_txt"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["client_phones_txt"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		//黑名单数据检测
		$DNC = M('sales_dnc');
		$arrDNC = Array();
		$arrTmp = $DNC->select();
		foreach( $arrTmp AS $row){
			$arrDNC[] = $row['phonenumber'];
		}
		//原来就存在表中的电话号码，导入的时候不能和你面重复
		$table = "sales_source_" .$id;
		$SS = M($table);
		/*
		$arrPhone1 = Array();
		$arrTmp = $SS->field('phone1')->select();
		foreach( $arrTmp AS $row){
			$arrPhone1[] = $row['phone1'];
		}
		*/
		//构造待插入的SQL语句
		$sql = "insert into $table(phone1,createtime) values ";
		$value = "";
		$dst_f = TMP_UPLOAD ."client_phones_".$time.".csv";
		move_uploaded_file($tmp_f,$dst_f);

		$strphones = trim( file_get_contents($dst_f) );
		$arrPhones2 = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
		$arrPhones = array_unique($arrPhones2);
		//dump($arrPhones);die;
		$avg = ceil(count($arrPhones)/8);
		/*
		//方法二start
		$arrData1 = array_slice($arrPhones,0,$avg);
		$arrData2 = array_slice($arrPhones,$avg,$avg);
		$arrData3 = array_slice($arrPhones,$avg*2,$avg);
		$arrData4 = array_slice($arrPhones,$avg*3,$avg);
		$arrData5 = array_slice($arrPhones,$avg*4,$avg);
		$arrData6 = array_slice($arrPhones,$avg*5,$avg);
		$arrData7 = array_slice($arrPhones,$avg*6,$avg);
		$arrData8 = array_slice($arrPhones,$avg*7);

		foreach($arrData1 as $key=>$val){
			$str = "('".$val."','".date('Y-m-d H:i:s')."')";
			$value .= empty($value)?$str:",$str";
		}
		foreach($arrData2 as $key=>$val){
			$str2 = "('".$val."','".date('Y-m-d H:i:s')."')";
			$value2 .= empty($value)?$str2:",$str2";
		}
		foreach($arrData3 as $key=>$val){
			$str3 = "('".$val."','".date('Y-m-d H:i:s')."')";
			$value3 .= empty($value)?$str3:",$str3";
		}
		foreach($arrData4 as $key=>$val){
			$str4 = "('".$val."','".date('Y-m-d H:i:s')."')";
			$value4 .= empty($value)?$str4:",$str4";
		}
		foreach($arrData5 as $key=>$val){
			$str5 = "('".$val."','".date('Y-m-d H:i:s')."')";
			$value5 .= empty($value)?$str5:",$str5";
		}
		foreach($arrData6 as $key=>$val){
			$str6 = "('".$val."','".date('Y-m-d H:i:s')."')";
			$value6 .= empty($value)?$str6:",$str6";
		}
		foreach($arrData7 as $key=>$val){
			$str7 = "('".$val."','".date('Y-m-d H:i:s')."')";
			$value7 .= empty($value)?$str7:",$str7";
		}
		foreach($arrData8 as $key=>$val){
			$str8 = "('".$val."','".date('Y-m-d H:i:s')."')";
			$value8 .= empty($value)?$str8:",$str8";
		}
		//方法二end
		*/


		//后者用上一种方法也行
		foreach($arrPhones as $key=>$val){
			if( in_array($val,$arrDNC) ){  // || in_array($val,$arrPhone1)
				continue;
			}else{
				$ph = str_replace("-","",$val);
				$ph = str_replace(" ","",$val);
				$str = "('".$ph."','".date('Y-m-d H:i:s')."')";
			}
			if($avg){
				if($key>$avg && $key<=$avg*2){
					$value2 .= empty($value2)?$str:",$str";
				}elseif($key>$avg*2 && $key<=$avg*3){
					$value3 .= empty($value3)?$str:",$str";
				}elseif($key>$avg*3 && $key<=$avg*4){
					$value4 .= empty($value4)?$str:",$str";
				}elseif($key>$avg*4 && $key<=$avg*5){
					$value5 .= empty($value5)?$str:",$str";
				}elseif($key>$avg*5 && $key<=$avg*6){
					$value6.= empty($value6)?$str:",$str";
				}elseif($key>$avg*6 && $key<=$avg*7){
					$value7 .= empty($value7)?$str:",$str";
				}elseif($key>$avg*7 && $key<=$avg*8){
					$value8 .= empty($value8)?$str:",$str";
				}elseif($key>$avg*8 && $key<=$avg*9){
					$value9 .= empty($value9)?$str:",$str";
				}elseif($key<=$avg){
					$value .= empty($value)?$str:",$str";
				}
			}else{
				$value .= empty($value)?$str:",$str";
			}

		}
		//导入动作，执行SQL语句
		//$result = false;
		if( $value ){
			$sql .= $value;
			//echo $sql;die;
			$result = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(phone1,createtime) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			//echo $sql;die;
			$result2 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(phone1,createtime) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			//echo $sql;die;
			$result3 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(phone1,createtime) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			//echo $sql;die;
			$result4 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(phone1,createtime) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			//echo $sql;die;
			$result5 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(phone1,createtime) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			//echo $sql;die;
			$result6 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(phone1,createtime) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			//echo $sql;die;
			$result7 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(phone1,createtime) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			//echo $sql;die;
			$result8 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(phone1,createtime) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			//echo $sql;die;
			$result9 = $SS->execute($sql);
		}

		//判断导入结果
		if( $result ){
			$total = count($arrPhones2);
			$paraSys = readS();
			if($paraSys["autocall_remove_duplicate"] == "yes"){
				$cfnum = $this->removePhone($id);
				//$cfnum = $this->removePhone($id,$total);
			}else{
				$cfnum = "0";
			}
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9-$cfnum;
			$fail = $total - $num;
			$msg = "计划导入${total}个号码，成功导入${num}个号码，${fail}个号码在黑名单中或者已经存在，或者数据重复!";
			echo json_encode(array('success'=>true,'msg'=>$msg));
			//goBack("计划导入${total}个号码，成功导入${result}个号码，${fail}个号码在黑名单中或者已经存在!","");
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
			//goBack("您导入了重复的号码或者导入号码出现未知错误!","");
		}

	}

	//处理导入的号码：xls和csv格式
	function doImportNumber444444444444(){
		set_time_limit(0);
		ini_set('memory_limit','512M');
		$id = $_GET['id'];
		$this->assign('id',$id);
		if( empty($_FILES["client_phones"]["tmp_name"]) ){
			goBack("请选择文件!","");
		}
		$time = time();
		$tmp_f = $_FILES["client_phones"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["client_phones"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		//黑名单数据检测
		$DNC = M('sales_dnc');
		$arrDNC = Array();
		$arrTmp = $DNC->select();
		foreach( $arrTmp AS $row){
			$arrDNC[] = $row['phonenumber'];
		}

		$table = "sales_source_" .$id;
		$SS = M($table);

		//去重
		$arrTmp = $SS->field('phone1')->select();
		$arrExist = Array(); //把所有号码放入关联数组，hash
		foreach( $arrTmp AS $row){
			$arrExist[$row['phone1']] = 'Y';
		}

		//构造待插入的SQL语句
		$sql = "insert into $table(phone1,createtime) values ";
		$value = "";
		$total = $black = $repeat = $valid = $invalid = 0;
		if( "csv" == $suffix ){//CSV格式导入
			$dst_f = TMP_UPLOAD ."client_phones_".$time.".csv";
			move_uploaded_file($tmp_f,$dst_f);
			$strphones = trim( file_get_contents($dst_f) );
			$arrPhones = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			array_shift($arrPhones); //第一行要去掉
			$total = count($arrPhones);
			foreach( $arrPhones AS $v ){
				$arrRow = explode(",",trim($v));//分割成字段
				$arrRow[4] = trim($arrRow[4]);
				if( in_array($arrRow[4],$arrDNC)){
					$black++;
					continue;
				}elseif( $arrExist[$arrRow[4]] == 'Y'){
					$repeat++;
					continue;
				}else{
					if( is_numeric($arrRow[4]) ){
						$valid++;
						$str = "('" .$arrRow[4] ."',NOW())";
						$value .= empty($value)?$str:",$str";
						$arrExist[$arrRow[4]] = 'Y'; //成功的号码需要加入到去重数组中
					}else{
						$invalid++;
					}
				}
			}
		}elseif( "xls" == $suffix ){//Excel格式导入
			$dst_f = TMP_UPLOAD ."client_phones_".$time.".xls";
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrPhones = $data->sheets[0];
			//var_dump($arrPhones);die;
			array_shift($arrPhones["cells"]); //第一行要去掉
			$arrPhones = $arrPhones["cells"];
			//var_dump($arrPhones);die;
			$total = count($arrPhones);
			foreach( $arrPhones AS $key=>$row ){
				$row[5] = trim($row[5]);
				if( in_array($row[5],$arrDNC) ){
					$black++;
					continue;
				}elseif( $arrExist[$row[5]] == 'Y'){
					$repeat++;
					continue;
				}else{
					if( is_numeric($row[5]) ){
						$valid++;
						$str = "('" .$row[5] ."',NOW())";
						$value .= empty($value)?$str:",$str";
						$arrExist[$row[5]] = 'Y';   //成功的号码需要加入到去重数组中
					}else{
						$invalid++;
					}
				}
			}

		}
		//echo "$value";die;
		if( $value ){
			$sql .= $value;
			$result = $SS->execute($sql);
		}
		//判断导入结果
		if( $result ){
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${total}个号码,${black}个在黑名单中,${repeat}个重复号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}
	}


	//处理导入的号码：xls和csv格式
	function doImportNumber(){
		set_time_limit(0);
		ini_set('memory_limit','400M');
		$id = $_GET['id'];
		$this->assign('id',$id);
		if( empty($_FILES["client_phones"]["tmp_name"]) ){
			goBack("请选择文件!","");
		}

		$time = time();
		$tmp_f = $_FILES["client_phones"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["client_phones"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		//黑名单数据检测
		$DNC = M('sales_dnc');
		$arrDNC = Array();
		$arrTmp = $DNC->select();
		foreach( $arrTmp AS $row){
			$arrDNC[] = $row['phonenumber'];
		}
		//原来就存在表中的电话号码，导入的时候不能和你面重复
		$table = "sales_source_" .$id;
		$SS = M($table);

		//去重
		$arrTmp = $SS->field('phone1')->select();
		$arrExist = Array(); //把所有号码放入关联数组，hash
		foreach( $arrTmp AS $row){
			$arrExist[$row['phone1']] = 'Y';
		}

		//构造待插入的SQL语句
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		$value = "";

		$arrSex = array("男"=>"man","女"=>"woman");
		$total = $black = $repeat = $valid = $invalid = 0;
		if( "csv" == $suffix ){//CSV格式导入
			$dst_f = TMP_UPLOAD ."client_phones_".$time.".csv";
			move_uploaded_file($tmp_f,$dst_f);

			$strphones = trim( file_get_contents($dst_f) );
			$arrPhones = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			//$arrPhones = $this->assoc_unique($arrPhones2,"5");
			//dump($arrPhones);die;
			foreach( $arrPhones AS $row ){
				$row[5] = trim($row[5]);
				if( in_array($row[5],$arrDNC) ){
					$black++;
					continue;
				}elseif( $arrExist[$row[5]] == 'Y'){
					$repeat++;
					continue;
				}else{
					if( is_numeric($row[5]) ){
						$valid++;
						$str = "(";
						$str .= "'" .str_replace("'","",trim($row[1])). "',";//姓名
						$str .= "'" .$arrSex[$row[2]]. "',";//性别
						if($row[3]){//年龄-------------------整数
							$str .= $row[3] . ",";
						}else{
							$str .= "NULL,";
						}
						$str .= "'" .$row[4]. "',";//生日
						$str .= "'" .str_replace(" ","",str_replace("-","",$row[5])). "',";   //$row[5]手机号
						$str .= "'" .$row[6]. "',";
						$str .= "'" .$row[7]. "',";//传真
						$str .= "'" .$row[8]. "',";
						$str .= "'" .$row[9]. "',";//公司
						$str .= "'" .$row[10]. "',";
						$str .= "'" .$row[11]. "',";
						$str .= "'" .$row[12]. "',";//省份
						$str .= "'" .$row[13]. "',";//城市-----------------------【Excel只需要填写到这里】
						$str .= "'" .$_SESSION['user_info']['username']. "',";//创建人
						$str .= "'" .date("Y-m-d H:i:s"). "'";//创建时间//----------------最后一个不需要","

						$str .= ")";
					//}

					if(count($arrPhones) >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}

						$arrExist[$row[5]] = 'Y';   //成功的号码需要加入到去重数组中
					}else{
						$invalid++;
					}
				}

			}
		}elseif( "xls" == $suffix ){//Excel格式导入
			$dst_f = TMP_UPLOAD ."client_phones_".$time.".xls";
			//echo $dst_f;die;
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrPhones = $data->sheets[0];
			array_shift($arrPhones["cells"]);
			$arrPhones = $arrPhones["cells"];
			//$arrPhones = $this->assoc_unique($arrPhones2,"5");
			//dump($arrPhones);die;
			$avg = ceil(count($arrPhones)/8);

			//dump($arrPhones);die;
			foreach( $arrPhones AS $key=>$row ){
				$row[5] = trim($row[5]);
				if( in_array($row[5],$arrDNC) ){
					$black++;
					continue;
				}elseif( $arrExist[$row[5]] == 'Y'){
					$repeat++;
					continue;
				}else{
					if( is_numeric($row[5]) ){
						$valid++;
						$str = "(";
						$str .= "'" .str_replace("'","",trim($row[1])). "',";//姓名
						$str .= "'" .$arrSex[$row[2]]. "',";//性别
						if($row[3]){//年龄-------------------整数
							$str .= $row[3] . ",";
						}else{
							$str .= "NULL,";
						}
						$str .= "'" .$row[4]. "',";//生日
						$str .= "'" .str_replace(" ","",str_replace("-","",$row[5])). "',";   //$row[5]手机号
						$str .= "'" .$row[6]. "',";
						$str .= "'" .$row[7]. "',";//传真
						$str .= "'" .$row[8]. "',";
						$str .= "'" .$row[9]. "',";//公司
						$str .= "'" .$row[10]. "',";
						$str .= "'" .$row[11]. "',";
						$str .= "'" .$row[12]. "',";//省份
						$str .= "'" .$row[13]. "',";//城市-----------------------【Excel只需要填写到这里】
						$str .= "'" .$_SESSION['user_info']['username']. "',";//创建人
						$str .= "'" .date("Y-m-d H:i:s"). "'";//创建时间//----------------最后一个不需要","

						$str .= ")";
					//}

					if(count($arrPhones) >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}

						$arrExist[$row[5]] = 'Y';   //成功的号码需要加入到去重数组中
					}else{
						$invalid++;
					}
				}

			}
		}else{
			goBack("文件类型错误!系统只支持处理.xls和.csv格式的Microsoft Excel文件!","");
		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			//echo $sql;die;
			$result2 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			//echo $sql;die;
			$result3 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			//echo $sql;die;
			$result4 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			//echo $sql;die;
			$result5 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			//echo $sql;die;
			$result6 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			//echo $sql;die;
			$result7 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			//echo $sql;die;
			$result8 = $SS->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			//echo $sql;die;
			$result9 = $SS->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			$total = count($arrPhones);
			/*
			$paraSys = readS();
			if($paraSys["autocall_remove_duplicate"] == "yes"){
				$cfnum = $this->removePhone($id);
				//$cfnum = $this->removePhone($id,$total);
			}else{
				$cfnum = "0";
			}
			*/
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $total - $num;
			//$msg = "计划导入${total}个号码，成功导入${num}个号码，${fail}个号码在黑名单中或者已经存在，或者数据重复!";
			//echo json_encode(array('success'=>true,'msg'=>$msg));
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${total}个号码,${black}个在黑名单中,${repeat}个重复号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
			//goBack("计划导入${total}个号码，成功导入${result}个号码，${fail}个号码在黑名单中或者已经存在!","");
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
			//goBack("您导入了重复的号码或者导入号码出现未知错误!","");
		}

	}



	//处理导入的号码：xls和csv格式--原始
	function doImportNumber444555(){
		set_time_limit(0);
		ini_set('memory_limit','400M');
		$id = $_GET['id'];
		$this->assign('id',$id);
		if( empty($_FILES["client_phones"]["tmp_name"]) ){
			goBack("请选择文件!","");
		}

		$time = time();
		$tmp_f = $_FILES["client_phones"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["client_phones"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		//黑名单数据检测
		$DNC = M('sales_dnc');
		$arrDNC = Array();
		$arrTmp = $DNC->select();
		foreach( $arrTmp AS $row){
			$arrDNC[] = $row['phonenumber'];
		}
		//原来就存在表中的电话号码，导入的时候不能和你面重复
		$table = "sales_source_" .$id;
		$SS = M($table);
		/*
		$arrPhone1 = Array();
		$arrTmp = $SS->field('phone1')->select();
		foreach( $arrTmp AS $row){
			$arrPhone1[] = $row['phone1'];
		}
		*/
		//构造待插入的SQL语句
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		$value = "";

		if( "csv" == $suffix ){//CSV格式导入
			$dst_f = TMP_UPLOAD ."client_phones_".$time.".csv";
			move_uploaded_file($tmp_f,$dst_f);

			$strphones = trim( file_get_contents($dst_f) );
			$arrPhones = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			//$arrPhones = $this->assoc_unique($arrPhones2,"5");
			//dump($arrPhones);die;
			foreach( $arrPhones AS $v ){
				$arrRow = explode(",",trim($v));//分割成字段
				if( in_array($arrRow[4],$arrDNC) ){ // || in_array($arrRow[4],$arrPhone1)
					continue;
				}else{
					if( is_numeric($arrRow[4]) ){
						$str = "(";
						$str .= "'" .$arrRow[0]. "',";//姓名
						$str .= "'" .$arrRow[1]. "',";//性别
						if($arrRow[2]){//年龄-------------------整数
							$str .= $arrRow[2] . ",";
						}else{
							$str .= "NULL,";
						}
						$str .= "'" .$arrRow[3]. "',";//生日
						$str .= "'" .$arrRow[4]. "',";
						$str .= "'" .$arrRow[5]. "',";
						$str .= "'" .$arrRow[6]. "',";//传真
						$str .= "'" .$arrRow[7]. "',";
						$str .= "'" .$arrRow[8]. "',";//公司
						$str .= "'" .$arrRow[9]. "',";//地址
						$str .= "'" .$arrRow[10]. "',";
						$str .= "'" .$arrRow[11]. "',";//省份
						$str .= "'" .$arrRow[12]. "',";//城市-----------------------【Excel只需要填写到这里】
						$str .= "'" .$_SESSION['user_info']['username']. "',";//创建人
						$str .= "'" .date("Y-m-d H:i:s"). "'";//创建时间//----------------最后一个不需要","

						$str .= ")";
					}
					$value .= empty($value)?$str:",$str";
				}
			}
		}elseif( "xls" == $suffix ){//Excel格式导入
			$dst_f = TMP_UPLOAD ."client_phones_".$time.".xls";
			//echo $dst_f;die;
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrPhones = $data->sheets[0];
			array_shift($arrPhones["cells"]);
			$arrPhones = $arrPhones["cells"];
			//$arrPhones = $this->assoc_unique($arrPhones2,"5");
			//dump($arrPhones);die;
			$avg = ceil(count($arrPhones)/8);

			//dump($arrPhones);die;
			foreach( $arrPhones AS $key=>$row ){
				if( in_array($row[5],$arrDNC) ){ //  || in_array($row[5],$arrPhone1)
					continue;
				}else{
					//if( is_numeric($row[5]) ){
						$str = "(";
						$str .= "'" .$row[1]. "',";//姓名
						$str .= "'" .$row[2]. "',";//性别
						if($row[3]){//年龄-------------------整数
							$str .= $row[3] . ",";
						}else{
							$str .= "NULL,";
						}
						$str .= "'" .$row[4]. "',";//生日
						$str .= "'" .str_replace(" ","",str_replace("-","",$row[5])). "',";   //$row[5]手机号
						$str .= "'" .$row[6]. "',";
						$str .= "'" .$row[7]. "',";//传真
						$str .= "'" .$row[8]. "',";
						$str .= "'" .$row[9]. "',";//公司
						$str .= "'" .$row[10]. "',";
						$str .= "'" .$row[11]. "',";
						$str .= "'" .$row[12]. "',";//省份
						$str .= "'" .$row[13]. "',";//城市-----------------------【Excel只需要填写到这里】
						$str .= "'" .$_SESSION['user_info']['username']. "',";//创建人
						$str .= "'" .date("Y-m-d H:i:s"). "'";//创建时间//----------------最后一个不需要","

						$str .= ")";
					//}

					if(count($arrPhones) >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}
					//$value .= empty($value)?"$str":",$str";
				}
			}
		}else{
			goBack("文件类型错误!系统只支持处理.xls和.csv格式的Microsoft Excel文件!","");
		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			//echo $sql;die;
			$result2 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			//echo $sql;die;
			$result3 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			//echo $sql;die;
			$result4 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			//echo $sql;die;
			$result5 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			//echo $sql;die;
			$result6 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			//echo $sql;die;
			$result7 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			//echo $sql;die;
			$result8 = $SS->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser, createtime) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			//echo $sql;die;
			$result9 = $SS->execute($sql);
		}

		//判断导入结果
		if( $result ){
			$total = count($arrPhones);
			$paraSys = readS();
			if($paraSys["autocall_remove_duplicate"] == "yes"){
				$cfnum = $this->removePhone($id);
				//$cfnum = $this->removePhone($id,$total);
			}else{
				$cfnum = "0";
			}
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9-$cfnum;
			$fail = $total - $num;
			$msg = "计划导入${total}个号码，成功导入${num}个号码，${fail}个号码在黑名单中或者已经存在，或者数据重复!";
			echo json_encode(array('success'=>true,'msg'=>$msg));
			//goBack("计划导入${total}个号码，成功导入${result}个号码，${fail}个号码在黑名单中或者已经存在!","");
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
			//goBack("您导入了重复的号码或者导入号码出现未知错误!","");
		}

	}



	function addOnePhone(){
		checkLogin();
		$task_id = $_GET["task_id"];
		$this->assign("task_id",$task_id);
		//dump($task_id);
		$this->display();
	}
	function addPhones(){
		checkLogin();
		$task_id = $_GET["task_id"];
		$this->assign("task_id",$task_id);
		//dump($task_id);
		$this->display();
	}

	//增加一个号码
	//URL: http://192.168.1.69/index.php?m=Telemarketing&a=addOnePhone&task_id=29&name=wjj&phone=15813705853
	function doAddOnePhone(){
		$task_id = $_POST['task_id'];
		$name = $_POST['name'];
		$phone = $_POST['phone'];
		$arr = Array(
			'name'	=> $name,
			'phone1'	=>	trim($phone),
			'createtime'	=>	date("Y-m-d H:i:s"),
		);
		$ss = M("sales_source_${task_id}");
		$count = $ss->where("phone1='$phone'")->count();
		if($count>0){
			goBack("该号码已经存在!","");
		}
		$result = $ss->add($arr);
		if( $result ){
			echo "ok";
		}else{
			echo "error";
		}

	}

	//批量增加号码
	//URL: http://192.168.1.69/index.php?m=Telemarketing&a=addOnePhone&task_id=29&name=wjj&phone=15813705853
	function doAddPhones(){
		$task_id = $_REQUEST['task_id'];
		$phone_start = $_POST['phone_start'];  //开始号码
		$preNum = substr($phone_start,0,1);
		$phones = $_POST['phones'];  //增加数量
		if(empty($_POST['phones'])){
			$msg["error"] = "开始号码不能为空";
			echo json_encode($msg);
			exit;
		}

		if($_POST['phones']<1){
			$msg["error"] = "数量少于1";
			echo json_encode($msg);
			exit;
		}
		//dump($preNum);die;
		$ss = M("sales_source_${task_id}");

		//去重
		$arrTmp = $ss->field('phone1')->select();
		$arrExist = Array(); //把所有号码放入关联数组，hash
		foreach( $arrTmp AS $row){
			$arrExist[$row['phone1']] = 'Y';
		}
		$repeat = 0;
		for($i=0;$i<$phones;$i++){
			if($preNum == "0"){
				$num = $phone_start+$i;
				$phone = "0".$num;
			}else{
				$phone = $phone_start+$i;
			}

			if($arrExist[$phone] == 'Y'){
				$repeat++;
				continue;
			}else{
				$arr = Array(
					'phone1'	=>	trim($phone),
					'createtime'	=>	date("Y-m-d H:i:s"),
				);
				$result = $ss->add($arr);
			}
		}

		if( $result ){
			$msg = "添加成功！";
			if($repeat>0){
				$total = $phones-$repeat;
				$msg .= "总共处理${phones}个号码，成功添加${total}条，重复号码有${repeat}条！";
			}
			echo json_encode(array('success'=>true,'msg'=>$msg));
		}else{
			echo json_encode(array('msg'=>'添加失败！'));
		}

	}


	//清空所有号码
	//URL: http://192.168.1.69/index.php?m=Telemarketing&a=deleteAllPhone&task_id=29
	function deleteAllPhone(){
		$task_id = $_POST['task_id'];
		if( $task_id ){
			$ss = M('sales_source_'.$task_id);
			$ss->execute("truncate table sales_source_${task_id}");   //清空资源表
			$ss->execute("truncate table sales_contact_history_${task_id}"); //清空历史联络记录表
			$ss->execute("truncate table sales_cdr_${task_id}");   //清空通话记录表

			$username = $_SESSION['user_info']['username'];
			$enterprise_number = $_SESSION['user_info']['enterprise_number'];
			$mod = M("task_operation");
			$arrData = array(
				"create_time"=>date("Y-m-d H:i:s"),
				"create_user"=>$username,
				"enterprise_number"=>$enterprise_number,
				"task_id"=>$task_id,
				"operation_type"=>"清空号码",
			);
			$result = $mod->data($arrData)->add();

			echo "ok";
		}else{
			echo "error";
		}
	}

	//复位任务
	//URL: http://192.168.1.69/index.php?m=Telemarketing&a=taskReset&task_id=29
	function taskReset(){
		$task_id = $_POST['task_id'];
		if( $task_id ){
			$table = 'sales_source_'.$task_id;
			$ss = M($table);
			$result = $ss->execute("update {$table} SET calledflag=1,locked='N',callresult_id=1,dealresult_id=0");  // where calledflag = 2
			//$result = $ss->execute("update {$table} SET calledflag=1,locked='N',callresult_id=1,dealresult_id=0 where dealresult_id!='3' AND (recordtag IS NULL OR recordtag='')  AND (key_value IS NULL OR key_value='') ");
			//CDR记录也要复位
			//$cdrtable = 'sales_cdr_'.$task_id;
			//$ss->execute("TRUNCATE TABLE {$cdrtable}");

			$username = $_SESSION['user_info']['username'];
			$enterprise_number = $_SESSION['user_info']['enterprise_number'];
			$mod = M("task_operation");
			$arrData = array(
				"create_time"=>date("Y-m-d H:i:s"),
				"create_user"=>$username,
				"enterprise_number"=>$enterprise_number,
				"task_id"=>$task_id,
				"operation_type"=>"复位任务",
			);
			$result = $mod->data($arrData)->add();

			echo "ok";
		}else{
			echo "error";
		}
	}

	//下载导入号码资源的excel和csv的模板文件
	function DownloadTemplate(){
		$file = $_GET['file'];
		$path = "/var/www/html/BGCC/Tpl/Public/download/";
		$realfile = $path.$file;
		if(!file_exists($realfile)){
			$this->error("File $realfile is not exist!");
		}
        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}



	//==========================================拨号器管理
	//显示外呼状态
	function listAutocall(){
		checkLogin();
		$menuname = "AutoCall List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();

	}

	function autocallData(){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$sales_task = M('sales_task');
		$count = $sales_task->table("sales_task st")->join("left join department d on st.dept_id=d.d_id")->where("enable='Y' and calltype='autocall'")->count();

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = BG_Page($count,$page_rows);//这个地方要修改

		$tpl_ST = $sales_task->table("sales_task st")->join("left join department d on st.dept_id=d.d_id")->where("enable='Y' and calltype='autocall'")->order("createtime desc")->limit($page->limit)->select();

		//统计所有队列中的成员
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrA = $bmi->getAllQueueMember();
		//dump($arr);die;

		//判断任务状态
		$i = 0;
		foreach($tpl_ST AS $v){
			$id = $v['id'];

			$tpl_ST[$i]['queue'] = "AutoCall_$id";
			if( array_key_exists("AutoCall_$id",$arrA) ){
				$tpl_ST[$i]['agentcount'] = $arrA["AutoCall_$id"]['agentcount'];
			}

			$task_file = "/var/tmp/autocall/$db_name:" . $id;
			if( file_exists($task_file) ){
				$tpl_ST[$i]['status'] = "启动";
			}else{
				$tpl_ST[$i]['status'] = "停止";
			}

			//统计每个任务的进度
			$ss = "sales_source_".$id;
			$m = M($ss);
			$sql = "SELECT COUNT(*) AS totalnum,SUM(calledflag>1) AS calledCount,SUM(calledflag=3) AS answeredCount FROM $ss";
			$arr = $m->query($sql);
			$totalnum = $arr[0]['totalnum'];
			$arr[0]['calledCount'] = $arr[0]['calledCount']?$arr[0]['calledCount']:0;
			$arr[0]['answeredCount'] = $arr[0]['answeredCount']?$arr[0]['answeredCount']:0;

			$tpl_ST[$i]['totalnum'] = $totalnum; //号码总量
			$tpl_ST[$i]['calledCount'] = $arr[0]['calledCount']; //呼叫量
			$tpl_ST[$i]['answeredCount'] = $arr[0]['answeredCount']; //接通量
			$tpl_ST[$i]['leftCount'] = $arr[0]['totalnum'] - $arr[0]['calledCount']; //剩余量
			if($arr[0]['calledCount']){
				$tpl_ST[$i]['answeredRate'] = sprintf("%01.2f",100*$arr[0]['answeredCount']/$arr[0]['calledCount']).'%';
			}else{
				$tpl_ST[$i]['answeredRate'] = "0.0%";
			}
			if($arr[0]['calledCount']){
				$calledCount = $arr[0]['calledCount'];
				$percent = sprintf("%01.2f",100*$calledCount/$totalnum); //任务进度
				$tpl_ST[$i]['percent'] = "<div style='width:100%;border:1px solid #ccc'><div style='width:$percent%;background:#F8B600;color:#000000'>$percent%</div></div>";
			}else{
				$tpl_ST[$i]['percent'] = "0.0%";
			}

			$tpl_ST[$i]['agentcount'] = $tpl_ST[$i]['agentcount'] ."  "."<a class='opt' target='_self' href='index.php?m=Telemarketing&a=removeQueueAgent&id=" .$v["id"] ."'>".L('Empty the queue seats')."</a>";
			$tpl_ST[$i]['agentinformation'] = '"'.$v['callername'].'" <'.$v['callerid'].">";

			$tpl_ST[$i]['operation'] = "<a target='_self' href='index.php?m=Telemarketing&a=dialManager&id=" .$v["id"] ."'>".L('Mission Control')."</a>";
			//$tpl_ST[$i]['operation'] = "<div class='make-switch switch-mini'><input type='checkbox' value='${id}' checked></div>";

			$i++;
		}
		//dump($tpl_ST);die;
		$rowsList = count($tpl_ST) ? $tpl_ST : false;
		$arrtask["total"] = $count;
		$arrtask["rows"] = $rowsList;

		echo json_encode($arrtask);
	}

	//拨号器显示
	function dialManager(){
		checkLogin();
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$checkRole = getSysinfo();
		$this->assign("msg_concurrence",$checkRole[4]);

		$id = $_GET['id'];
		$this->assign('id',$id);
		$tpl_ST = Array();
		//号码总量
		$ss = "sales_source_".$id;
		$m = M($ss);
		$total = $m->count();
		$tpl_ST['total'] = $total;

		//已经呼叫数量,体现在数据库中
		$calledCount = $m->where("calledflag >1")->count();
		$tpl_ST['db_calledCount'] = $calledCount;

		//呼叫进度
		$progress = "";
		if($calledCount){
				//$calledCount = $arr[0]['calledCount'];
				$percent = sprintf("%01.2f",100*$calledCount/$total); //任务进度
				$progress = $percent;
		}else{
				$progress = "0";
		}
		$this->assign('progress',$progress);

		//先获取本项目的组通道:
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();


		$group = (string)$id;
		$category = "AUTOCALL";
		$arrGroupChannel = $bmi->getGroupChannelsInfo($group,$category);
		
		$arrData = $bmi->getAllChannelsInfo('Channel');

		foreach($arrData as $val){
			if(in_array($val["Channel"],$arrGroupChannel) && $val["State"] == "Up"  && $val["BridgedTo"] != "(None)" ){
				$arrCall[] = $val;   //通话中的
			}
			if(in_array($val["Channel"],$arrGroupChannel) && $val["State"] == "Ringing"  && $val["BridgedTo"] != "(None)" ){
				$arrRing[] = $val;   //振铃中的
			}
		}
		$arrWaitQueue = $bmi->waitingQueueInfo($id);  //等待队列中的

		//$tpl_ST['callingCount'] = count($arrGroupChannel);  //当前通过中继呼叫数量
		$tpl_ST['ringingCount'] = count($arrRing); 	//正在振铃通道数量
		$tpl_ST['connectingCount'] = count($arrCall);   //正在通话通话的通道数量
		$tpl_ST['waitQueue'] = count($arrWaitQueue);   //在队列中等待的数量


		/*
		$a1 = $bmi->asm->GetVar("","GROUP_COUNT($id@AUTOCALL)");
		$a2 = $bmi->asm->GetVar("","GROUP_COUNT($id@client_answered)");
		$a3 = $bmi->asm->GetVar("","GROUP_COUNT($id@queue_enter)");
		$a4 = $bmi->asm->GetVar("","GROUP_COUNT($id@queue_answered)");
		//var_dump($a1);var_dump($a2);die;

		$tpl_ST['callingCount'] = $a1['Value'];  //当前呼出数量
		$tpl_ST['ringingCount'] = $a2['Value']; 	//当前外线接听数量
		$tpl_ST['ringCount'] = $a3['Value']; 	//进入队列的数量
		$tpl_ST['connectingCount'] = $a4['Value'];   //队列坐席接听数量
		$tpl_ST['waitQueue'] = $tpl_ST['ringCount'] - $tpl_ST['connectingCount'];   //在队列中等待的数量
		*/

		$a1 = $bmi->asm->GetVar("","GROUP_COUNT($id@AUTOCALL)");
		$tpl_ST['callingCount'] = $a1['Value'];  //当前呼出数量
		//项目并发和状态
		$sales_task = M('sales_task');
		$arrTmp = $sales_task->where("ENABLE='y' AND id=$id")->find();

		// $getStatus = M('sales_task')->where(array('id'=>$id))->find();
		// if($getStatus['autocall_status'] =='N'){
		// 	$tpl_ST['status'] = "off";
		// 	$tpl_ST['concurrence'] = $arrTmp['concurrence'];
		// }else{
		// 	$tpl_ST['status'] = "on";
		// 	$tpl_ST['concurrence'] = file_get_contents("/var/tmp/autocall/$db_name:$id");
		// }
		if( file_exists("/var/tmp/autocall/$db_name:$id") ){
			$tpl_ST['status'] = "on";
			$tpl_ST['concurrence'] = file_get_contents("/var/tmp/autocall/$db_name:$id");
		}else{
			$tpl_ST['status'] = "off";
			$tpl_ST['concurrence'] = $arrTmp['concurrence'];
		} 
		##已连接
		$Connected = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>1))->group('customer_id')->order('calldate desc')->count();
		$this->assign('Connected',$Connected);
		#用户拒接
		$UserReject = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>2))->group('customer_id')->order('calldate desc')->count();
		$this->assign('UserReject',$UserReject);
		#用户正忙
		$TheUserIsBusy = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>3))->group('customer_id')->order('calldate desc')->count();
		$this->assign('TheUserIsBusy',$TheUserIsBusy);
		#正在通话中
		$beOnThePhone = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>4))->group('customer_id')->order('calldate desc')->count();
		$this->assign('beOnThePhone',$beOnThePhone);
		#无法接通
		$unavailable = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>5))->group('customer_id')->order('calldate desc')->count();
		$this->assign('unavailable',$unavailable);
		#关机
		$shutdown = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>6))->group('customer_id')->order('calldate desc')->count();
		$this->assign('shutdown',$shutdown);
		#网络忙
		$TheNetworkIsBusy = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>7))->group('customer_id')->order('calldate desc')->count();
		$this->assign('TheNetworkIsBusy',$TheNetworkIsBusy);
		#无人接听
		$ThereIsNoAnswer = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>8))->group('customer_id')->order('calldate desc')->count();
		$this->assign('ThereIsNoAnswer',$ThereIsNoAnswer);
		#呼叫转移失败
		$CallTransferFailed = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>9))->group('customer_id')->order('calldate desc')->count();
		$this->assign('CallTransferFailed',$CallTransferFailed);
		#各类秘书服务
		$SecretarialServices = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>10))->group('customer_id')->order('calldate desc')->count();
		$this->assign('SecretarialServices',$SecretarialServices);
		#呼入限制
		$IncomingLimit = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>11))->group('customer_id')->order('calldate desc')->count();
		$this->assign('IncomingLimit',$IncomingLimit);
		#拨号方式不正确
		$dialingModeIsIncorrect = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>12))->group('customer_id')->order('calldate desc')->count();
		$this->assign('dialingModeIsIncorrect',$dialingModeIsIncorrect);
		#欠费
		$Arrearage = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>13))->group('customer_id')->order('calldate desc')->count();
		$this->assign('Arrearage',$Arrearage);
		#无法接听
		$UnableAnswer = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>14))->group('customer_id')->order('calldate desc')->count();
		$this->assign('UnableAnswer',$UnableAnswer);
		#各种稍后再拨提示
		$dialingLate = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>15))->group('customer_id')->order('calldate desc')->count();
		$this->assign('dialingLate',$dialingLate);
		#改号
		$changingNumber = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>16))->group('customer_id')->order('calldate desc')->count();
		$this->assign('changingNumber',$changingNumber);
		#线路不能呼出
		$notExhaled = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>17))->group('customer_id')->order('calldate desc')->count();
		$this->assign('notExhaled',$notExhaled);
		#暂停服务
		$outOfService = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>18))->group('customer_id')->order('calldate desc')->count();
		$this->assign('outOfService',$outOfService);
		#停机
		$stopCode = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>19))->group('customer_id')->order('calldate desc')->count();
		$this->assign('stopCode',$stopCode);
		#空号
		$vacantNumber = M('bgcrm.sales_cdr_'.$_GET['id'])->where(array('callresult'=>20))->group('customer_id')->order('calldate desc')->count();
		$this->assign('vacantNumber',$vacantNumber);
		// dump($arrTmp);die;
		$this->assign("ST",$tpl_ST);
		$trunkStatus = explode('/',strtolower(M('sales_task')->where(array('id'=>$_GET['id']))->getField('trunk')));
		$numberStatus = M('asterisk.trunks')->where(array('name'=>$trunkStatus[1],'tech'=>$trunkStatus[0]))->getField('numberStatus');
		$this->assign('numberStatus',$numberStatus);

		if($numberStatus==1){ 
			$this->assign('styleCss','display:block');
		}else{
			$this->assign('styleCss','display:none');
		}
		$this->display();
	}

	//拨号器显示
	function dialManagerBak(){
		checkLogin();
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$checkRole = getSysinfo();
		$this->assign("msg_concurrence",$checkRole[4]);

		$id = $_GET['id'];
		$this->assign('id',$id);

		$tpl_ST = Array();

		//号码总量
		$ss = "sales_source_".$id;
		$m = M($ss);
		$total = $m->count();
		$tpl_ST['total'] = $total;

		//已经呼叫数量,体现在数据库中
		$calledCount = $m->where("calledflag >1")->count();
		$tpl_ST['db_calledCount'] = $calledCount;

		//呼叫进度
		$progress = "";
		if($calledCount){
				//$calledCount = $arr[0]['calledCount'];
				$percent = sprintf("%01.2f",100*$calledCount/$total); //任务进度
				$progress = $percent;
		}else{
				$progress = "0";
		}

		$this->assign('progress',$progress);
		/*当前正在呼叫数量

		$varName = "GROUP_COUNT($id@AUTOCALL)";//变量名
		$res = $bmi->asm->send_request('Getvar',Array('Variable'=>$varName));//获取变量
		$tpl_ST['callingCount'] = 0;
		if($res['Response']=='Success'){
			$tpl_ST['callingCount'] = $res['Value'];
		}
		//var_dump($tpl_ST['callingCount'] );die;
		*/

		//先获取本项目的组通道:
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$group = (string)$id;
		$category = "AUTOCALL";
		$arrGroupChannel = $bmi->getGroupChannelsInfo($group,$category);
		//dump($arrGroupChannel);die;

		//再获取所有通道信息
		$arrChannel = $bmi->getAllChannelsInfo('Channel');
		//if($_GET['d'])dump($arrChannel);


		//当前通过中继呼叫数量
		$tpl_ST['callingCount'] = count($arrGroupChannel);

		//正在振铃通道数量
		$tpl_ST['ringingCount'] = 0;

		//正在通话通话的通道数量
		$tpl_ST['connectingCount'] = 0;

		foreach($arrGroupChannel AS $channel){
			if($arrChannel[$channel]['State']=="Ringing" && $arrChannel[$channel]['BridgedTo']=="(None)"){
				$tpl_ST['ringingCount'] ++;
			}
			//if($arrChannel[$channel]['State']=="Up" && $arrChannel[$channel]['BridgedTo']!="(None)"){
			if($arrChannel[$channel]['State']=="Up" && $arrChannel[$channel]['BridgedTo']=="(None)"){
				$tpl_ST['connectingCount'] ++;
			}
		}
		//dump($tpl_ST);die;

		//项目并发和状态
		$sales_task = M('sales_task');
		$arrTmp = $sales_task->where("ENABLE='y' AND id=$id")->find();
		if( file_exists("/var/tmp/autocall/$db_name:$id") ){
			$tpl_ST['status'] = "on";
			$tpl_ST['concurrence'] = file_get_contents("/var/tmp/autocall/$db_name:$id");
		}else{
			$tpl_ST['status'] = "off";
			$tpl_ST['concurrence'] = $arrTmp['concurrence'];
		}
		$this->assign("ST",$tpl_ST);

		//当前呼叫通道列表
		//if($_GET['d'])dump($arrChannel);

		$calllist = Array();
		foreach($arrGroupChannel AS $Channel){
			//$calllist[$Channel]['Number'] = $arrChannel[$Channel]['CallerID'];//外呼号码，相当于CID
			$calllist[$Channel]['Channel'] = $arrChannel[$Channel]['Channel'];//外呼通道
			if(FALSE !== strpos($arrChannel[$Channel]['State'],"Ringing") && $arrChannel[$Channel]['BridgedTo']=="(None)"){
				$calllist[$Channel]['State'] = "正在外呼";//外呼状态
			}
			//if($arrChannel[$Channel]['State']=="Up" && $arrChannel[$Channel]['BridgedTo']!="(None)"){
			if($arrChannel[$Channel]['State']=="Up" && $arrChannel[$Channel]['BridgedTo']=="(None)"){
				$calllist[$Channel]['State'] = "通话中";//外呼状态
			}
			if($arrChannel[$Channel]['State']=="Down" && $arrChannel[$Channel]['BridgedTo']=="(None)"){
				$calllist[$i]['State'] = "挂断中";//外呼状态
			}
			$dstChannel = $arrChannel[$Channel]['BridgedTo'];
			#$calllist[$Channel]['AnswerAgent'] = "801";//坐席分机号
			if( preg_match('/Local\/(.*)@/',$dstChannel,$arr1)){
				$calllist[$Channel]['AnswerAgent'] = $arr1[1];
			}elseif( preg_match('/\/([^-]+)-/',$dstChannel,$arr2)){
				$calllist[$Channel]['AnswerAgent'] = $arr2[1];
			}else{

			}


			$calllist[$Channel]['BridgedTo'] = $arrChannel[$Channel]['BridgedTo'];//桥接通道
			$calllist[$Channel]['Duration'] = $arrChannel[$Channel]['Duration'];//呼叫时长
			//$calllist[$Channel]['Billsec'] = $arrChannel[$Channel]['Billsec'];//通话时长
			//下面的效率可能比较慢，但是没有更好的办法
			$cmd = "/usr/sbin/asterisk -rx'core show channel $Channel' |egrep 'clientphone|billsec'|cut -d= -f2";
			//dump($cmd);
			$ret = shell_exec($cmd);
			//dump($ret);
			$tmpArr = explode("\n",$ret);
			//dump($tmpArr);
			$calllist[$Channel]['clientphone'] = $tmpArr[0];
			$calllist[$Channel]['Billsec'] = $tmpArr[1];

		}
		if($_GET['d'])dump($calllist);

		$this->assign("CL",$calllist);

		$this->display();
	}

	function dialManagerData(){
		$id = $_GET['id'];

		//先获取本项目的组通道:
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$group = (string)$id;
		$category = "AUTOCALL";
		$arrGroupChannel = $bmi->getGroupChannelsInfo($group,$category);
		//dump($arrGroupChannel);

		//再获取所有通道信息
		$arrChannel = $bmi->getAllChannelsInfo('Channel');

		$calllist = Array();
		$i = 0;
		foreach($arrGroupChannel AS $Channel){
			//$calllist[$i]['Number'] = $arrChannel[$Channel]['CallerID'];//外呼号码，相当于CID
			$calllist[$i]['Channel'] = $arrChannel[$Channel]['Channel'];//外呼通道
			if(FALSE !== strpos($arrChannel[$Channel]['State'],"Ringing") && $arrChannel[$Channel]['BridgedTo']=="(None)"){
				$calllist[$i]['State'] = "正在外呼";//外呼状态
			}
			//if($arrChannel[$Channel]['State']=="Up" && $arrChannel[$Channel]['BridgedTo']!="(None)"){
			if($arrChannel[$Channel]['State']=="Up" && $arrChannel[$Channel]['BridgedTo']=="(None)"){
				$calllist[$i]['State'] = "通话中";//外呼状态
			}
			if($arrChannel[$Channel]['State']=="Down" && $arrChannel[$Channel]['BridgedTo']=="(None)"){
				$calllist[$i]['State'] = "挂断中";//外呼状态
			}
			$dstChannel = $arrChannel[$Channel]['BridgedTo'];
			#$calllist[$i]['AnswerAgent'] = "801";//坐席分机号
			if( preg_match('/Local\/(.*)@/',$dstChannel,$arr1)){
				$calllist[$i]['AnswerAgent'] = $arr1[1];
			}elseif( preg_match('/\/([^-]+)-/',$dstChannel,$arr2)){
				$calllist[$i]['AnswerAgent'] = $arr2[1];
			}else{

			}


			$calllist[$i]['BridgedTo'] = $arrChannel[$Channel]['BridgedTo'];//桥接通道
			$calllist[$i]['Duration'] = $arrChannel[$Channel]['Duration'];//呼叫时长
			//$calllist[$i]['Billsec'] = $arrChannel[$Channel]['Billsec'];//通话时长
			//下面的效率可能比较慢，但是没有更好的办法
			$cmd = "/usr/sbin/asterisk -rx'core show channel $Channel' |egrep 'clientphone|billsec'|cut -d= -f2";
			//dump($cmd);
			$ret = shell_exec($cmd);
			//dump($ret);
			$tmpArr = explode("\n",$ret);
			//dump($tmpArr);
			$calllist[$i]['clientphone'] = $tmpArr[0];
			$calllist[$i]['Billsec'] = $tmpArr[1];
			$i++;
		}
		//dump($arrChannel);

		$rowsList = count($calllist) ? $calllist : false;
		$arrManager["total"] = count($calllist);
		$arrManager["rows"] = $rowsList;
		//dump($rowsList);die;
		echo json_encode($arrManager);

	}

	function recordConcurrent($id,$concurrence){
		$arrData = array(array("task_id"=>$id, "concurrent"=>$concurrence));
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = array();
		if( file_exists("BGCC/Conf/crm/$db_name/concurrentTask.php") ){
			$arrF = require "BGCC/Conf/crm/$db_name/concurrentTask.php";
		}

		if($arrF){
			foreach($arrF as &$val){
				if($val["task_id"] == $id){
					$val["concurrent"] = $concurrence;
				}
				$arrTid[] = $val["task_id"];
			}
			if( !in_array($id,$arrTid) ){
				$arr = array("task_id"=>$id, "concurrent"=>$concurrence);
				array_push($arrF,$arr);
			}
			$arrT = $arrF;
		}else{
			$arrT = $arrData;
		}

		$content = "<?php\nreturn " .var_export($arrT,true) .";\n ?>";
		file_put_contents("BGCC/Conf/crm/$db_name/concurrentTask.php",$content);
	}

	function deleteConcurrent($id,$concurrence){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrData = array(array("task_id"=>$id, "concurrent"=>$concurrence));
		$arrF = array();
		if( file_exists("BGCC/Conf/crm/$db_name/concurrentTask.php") ){
			$arrF = require "BGCC/Conf/crm/$db_name/concurrentTask.php";
		}

		if($arrF){
			foreach($arrF as &$val){
				if($val["task_id"] == $id){
					$val["concurrent"] = $concurrence;
				}else{
					$tmp_arr[] = $val;
				}
			}
			$arrT = $tmp_arr;
		}else{
			$arrT = $arrData;
		}
		//dump($arrT);die;
		$content = "<?php\nreturn " .var_export($arrT,true) .";\n ?>";
		file_put_contents("BGCC/Conf/crm/$db_name/concurrentTask.php",$content);
	}

	//刚载入任务控制页面时，如果状态关闭执行这段代码
	function clearConcurrent(){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$sales_task = new Model('sales_task');
		$arrData = $sales_task->field("id,name")->select();
		foreach($arrData as $val){
			$arrID[] = $val["id"];
			if( file_exists("/var/tmp/autocall/$db_name:" .$val['id'] ) ){
				$arrF[] = $val["id"];
			}else{
				$arr[] = $val["id"];
				$this->deleteConcurrent($val["id"],"0");
			}
		}
		//任务不存在的删掉
		$arrF2 = getConcurrentTask();
		foreach($arrF2 as $val){
			if( !in_array($val["task_id"],$arrID) ){
				$this->deleteConcurrent($val["task_id"],"0");
			}
		}
	}


	function deleteConcurrent2(){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$id = $_REQUEST["id"];
		$concurrence = $_REQUEST["concurrence"];
		$arrData = array(array("task_id"=>$id, "concurrent"=>$concurrence));
		$arrF = array();
		if( file_exists("BGCC/Conf/crm/$db_name/concurrentTask.php") ){
			$arrF = require "BGCC/Conf/crm/$db_name/concurrentTask.php";
		}

		if($arrF){
			foreach($arrF as &$val){
				if($val["task_id"] == $id){
					$val["concurrent"] = $concurrence;
				}else{
					$tmp_arr[] = $val;
				}
			}
			$arrT = $tmp_arr;
		}else{
			$arrT = $arrData;
		}
		$content = "<?php\nreturn " .var_export($arrT,true) .";\n ?>";
		file_put_contents("BGCC/Conf/crm/$db_name/concurrentTask.php",$content);
	}

	//拨号参数设置
	//URL：index.php?m=Telemarketing&a=changeDialManager
	function changeDialManager(){
		checkLogin();
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		if(!$db_name){
			echo "<script>top.location.href='index.php?m=Index&a=Login';</script>";
			die;
		}
		$id = $_POST['id'];
		$status = $_POST['status'];
		$concurrence = $_POST['concurrence'];
		//$file_name = $id;
		$file_name = $db_name.":".$id;
		//dump($file_name);die;

		$task_file = "/var/tmp/autocall/$file_name";
		//dump($task_file);die;
		if('on'== $status){
			//号码总量
			$ss = "sales_source_".$id;
			$m = M($ss);
			$noCalledCount = $m->where("calledflag=1")->count();
			if(0 == $noCalledCount){
				echo "没有导入号码或者所有号码都已经被呼叫过了！请导入号码或者复位任务后再开启任务。";
				@unlink($task_file);
				die;
			}

			$arrF = getConcurrentTask();
			if($arrF){
				foreach($arrF as $val){
					if($val["task_id"] != $id){
						$arrC[] = $val["concurrent"];
					}
				}
				$count = array_sum($arrC)+$concurrence;
				$checkRole = getSysinfo();
				$arrAL = explode(",",$checkRole[2]);
				if( in_array("zh",$arrAL) ){
					$enterprise_number = $_SESSION['user_info']['enterprise_number'];
					$arrTIF = getTenantsInfo();
					$arrTI = $arrTIF[$enterprise_number];
					if($db_name == "bgcrm"){
						$max_concurrent = $checkRole[4];
					}else{
						$max_concurrent = $arrTI["max_concurrent"];
					}
				}else{
					$max_concurrent = $checkRole[4];
				}
				if($count > $max_concurrent){
					echo "您超过了最大并发数,其它任务开启的总并发数为：".array_sum($arrC)." 您授权的最大并发数为：".$max_concurrent;
					die;
				}
			}

			//中继状态检测
			$st = M('sales_task');
			$arr = $st->where("id=$id")->find();
			$trunk = $arr['trunk'];
			require_once("/var/lib/asterisk/agi-bin/phpagi-asmanager.php");
			$ami = new AGI_AsteriskManager();
			$host = C('PBX_HOST');
			$username = C('PBX_USER');
			$secret = C('PBX_PWD');
			$conn = $ami->connect($host,$username,$secret);
			$this->recordConcurrent($id,$concurrence);
			/*
			//加了这段代码不能外呼--lch
			//wjj--add
			if( (int)$arr['runtime'] >0 ){
				$task_file = "/var/tmp/autocall/time_$id";
			}
			//wjj--end
			*/

			if( $conn == false ){
				$this->deleteConcurrent($id,$concurrence);
				echo "任务开启失败！asterisk管理接口连接错误!";
				die;
			}/*
			else{
				//看是否可以拨打10086这个号码，如果可以打通，说明中继是好的。否则就是坏的。
				$arr = $ami->Originate("${trunk}/10086",'test','test','1');
				if($arr["Response"] == 'Success'){
					;
				}else{
					echo "中继线路(${trunk})故障！请检测PBX配置！";
					die;
				}
				//检测队列中是否有坐席存在
				$qname = "AutoCall_" .$id;
				$arrTemp = $ami->Command("queue show AutoCall_" .$id);
				$arr = explode("\n",$arrTemp['data']);
				$num = 0;
				foreach($arr AS $line){
					if(preg_match('/^\s{6}/',$line) )$num++; //队列中的成员是以6个空格开头的
				}
				if(0 == $num){
					echo "接听该任务的坐席尚未迁入，请让坐席登陆CRM并迁入相关外呼任务！";
					die;
				}
			}*/

		}else{
			$this->deleteConcurrent($id,$concurrence);
		}

		//先判断项目状态
		if( file_exists($task_file) ){//项目文件存在
			if($status == 'on'){
				if( 0 < file_put_contents($task_file,$concurrence) ){echo "ok";}else{echo "error";};
			}else{
				if( unlink($task_file) ){echo "ok";}else{echo "error";};
			}
		}else{//项目文件不存在
			if($status == 'on'){
				if( $concurrence > 0 ){
					if( 0 < file_put_contents($task_file,$concurrence) ){echo "ok";}else{echo "error";};
				}else{
					echo "操作失败!并发数必须大于0!";
				}
			}else{
				echo "ok";
			}
		}

		//goBack("设置成功!","index.php?m=Telemarketing&a=dialManager&id=$id");
	}


	//拨号并发数改变
	//URL：index.php?m=Telemarketing&a=concurrenceChange
	function concurrenceChange(){
		checkLogin();
		$id = $_POST['id'];
		$concurrence = $_POST['concurrence'];
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		if(!$db_name){
			echo "<script>top.location.href='index.php?m=Index&a=Login';</script>";
			die;
		}
		$file_name = $db_name.":".$id;

		$arrF = getConcurrentTask();
		if($arrF){
			foreach($arrF as $val){
				if($val["task_id"] != $id){
					$arrC[] = $val["concurrent"];
				}
			}
			$count = array_sum($arrC)+$concurrence;
			$checkRole = getSysinfo();
			if($count > $checkRole[4]){
				echo "您超过了最大并发数,其它任务开启的总并发数为：".array_sum($arrC)." 您授权的最大并发数为：".$checkRole[4];
				die;
			}else{
				$this->recordConcurrent($id,$concurrence);
			}
		}

		$task_file = "/var/tmp/autocall/$file_name";
		//先判断项目状态
		if( file_exists($task_file) ){//项目文件存在
			if( 0 < file_put_contents($task_file,$concurrence) ){echo "ok";}else{echo "error";};
		}else{//项目文件不存在
			echo "操作失败!呼叫已完成或者项目文件不存在!";
		}
	}

	function qualityRatio(){
		$sales_task = M('sales_task');

		$taskData = $sales_task->select();
		$mod = M();
		foreach($taskData as $vm){
			$sql[$vm['id']] = "select count(*) as count from sales_cdr_".$vm['id']." where disposition='ANSWERED'";
			$res[$vm['id']] = $mod->query($sql[$vm['id']]);
			$sql2[$vm['id']] = "select count(*) as count from sales_cdr_".$vm['id']." where disposition='ANSWERED'  AND qualitype = 'Y'";
			$res2[$vm['id']] = $mod->query($sql2[$vm['id']]);
		}
		foreach($res as $k=>$v){
			$cdr1[$k] = $v[0]['count'];
		}
		foreach($res2 as $k2=>$v2){
			$cdr2[$k2] = $v2[0]['count'];
		}
		$cdrs = $this->array_Abteilung($cdr2,$cdr1);
		//dump($cdrs);die;
		return $cdrs;
	}

	function array_Abteilung($a,$b){
		//根据键名获取两个数组的交集
		$arr=array_intersect_key($a, $b);
		//遍历第二个数组，如果键名不存在与第一个数组，将数组元素增加到第一个数组
		foreach($b as $key=>$value){
			if(!array_key_exists($key, $a)){
				$a[$key]=$value;
			}
		}
		//计算键名相同的数组元素的和，并且替换原数组中相同键名所对应的元素值
		foreach($arr as $key=>$value){
			$a[$key]=substr(($a[$key]/$b[$key])*100,0,4);
			//$a[$key]=$a[$key]/$b[$key];
			//$a[$key]=ceil($a[$key]/$b[$key]);
		}
		//返回相加后的数组
		krsort($a);
		return $a;
	}

	//外呼通话记录
	function outcallRecord(){
		checkLogin();
		$trunkStatus = explode('/',strtolower(M('sales_task')->where(array('id'=>$_GET['id']))->getField('trunk')));
		$numberStatus = M('asterisk.trunks')->where(array('name'=>$trunkStatus[1],'tech'=>$trunkStatus[0]))->getField('numberStatus');
		$this->assign('numberStatus',$numberStatus);
		if($numberStatus==1){
			$CallResult = M('sales_callresult')->select();
			$this->assign('CallResult',$CallResult);
		} 
		
 

		$menuname = "Task";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$id = $_GET['id'];
		$this->assign('id',$id);
		/*
		$sales_cdr = new Model("sales_cdr_".$id);
		$res = $sales_cdr->where("(dst is null OR dst = '') AND disposition = 'ANSWERED'")->save(array("disposition"=>"NO ANSWER"));
		*/
		$AM = M('autocall_mark');
		$arrTag = $AM->order("key_value ASC")->select();
		//echo $AM->getLastSql();die;
		$this->assign('arrTag',$arrTag);

		$this->display();
	}

	//呼叫结果
	function outcallResult(){
		checkLogin();
		$id = $_GET['id'];
		$this->assign('id',$id);
		$sales_task = M('sales_task');
		$arrTask = $sales_task->where("id=$id")->find();
		//dump($arrTask);die;

		//呼叫类型select标签的显示
		if("preview" == $arrTask['calltype']){
			$arrTask['select_calltype'] = "预览式外呼";
		}else{
			$arrTask['select_calltype'] = "自动外呼";
		}

		//部门
		$d_id = $arrTask['dept_id'];
		$department = M('department');
		$tpl_dept = $department->field("d_id,d_name")->where("d_id=$d_id")->find();
		$arrTask['select_dept_id'] = $tpl_dept['d_name'];
		$this->assign('T',$arrTask);

		//号码总数
		$table = "sales_source_".$id;
		$ss = M($table);
		$total = $ss->count();
		$this->assign('total',$total);
		//呼叫总数
		$calledTotal = $ss->where("calledflag>1")->count();
		$this->assign('calledTotal',$calledTotal);
		//剩余未呼叫总数
		$leftTotal = $total - $calledTotal;
		$this->assign('leftTotal',$leftTotal);

		//已接听数
		$answeredCount = $ss->where("calledflag=3")->count();
		$this->assign('answeredCount',$answeredCount);
		//未接听数
		$noansweredCount = $ss->where("calledflag=2")->count();
		$this->assign('noansweredCount',$noansweredCount);

		//任务完成进度
		if($total == 0){
			$finishPercent = "0.0%";
		}else{
			$finishPercent = number_format(100*$calledTotal/$total,1) .'%';
		}
		$this->assign('finishPercent',$finishPercent);
		//号码有效率
		if($calledTotal == 0){
			$qualityPercent = "0.0%";
		}else{
			$qualityPercent = number_format(100*$answeredCount/$calledTotal,1) .'%';
		}
		$this->assign('qualityPercent',$qualityPercent);

		//分页列出号码
		$count = $ss->count();
		$para_sys = readS();
		$size = $para_sys["page_rows"];
		$page = BG_Page($count,$size);//这个地方要修改
		$show = $page->show();
		$this->assign("page",$show);
		$tpl_SS = $ss->limit($page->limit)->select();
		$i = 0;
		foreach( $tpl_SS AS $val ){
			if( $val['calledflag'] == 1 ){
				$tpl_SS[$i]['calledResult'] = "未呼叫";
			}else if( $val['calledflag'] == 2 ){
				$tpl_SS[$i]['calledResult'] = "呼叫未接听";
			}else{
				$tpl_SS[$i]['calledResult'] = "已接听";
			}
			$i++;
		}

		$this->assign('SS',$tpl_SS);
		$this->display();
	}

	//删除呼叫失败的号码
	function delCallFailedNum(){
		$task_id = $_POST['task_id'];
		if( $task_id ){
			$table = 'sales_source_'.$task_id;
			$ss = M($table);
			$result = $ss->execute("DELETE FROM {$table} WHERE calledflag=2");

			$username = $_SESSION['user_info']['username'];
			$enterprise_number = $_SESSION['user_info']['enterprise_number'];
			$mod = M("task_operation");
			$arrData = array(
				"create_time"=>date("Y-m-d H:i:s"),
				"create_user"=>$username,
				"enterprise_number"=>$enterprise_number,
				"task_id"=>$task_id,
				"operation_type"=>"删除未接号码",
			);
			$result = $mod->data($arrData)->add();

			echo "ok";
		}else{
			echo "error";
		}
	}

	//复位呼叫失败的号码
	function resetCallFailedNum(){
		$task_id = $_POST['task_id'];
		if( $task_id ){
			$table = 'sales_source_'.$task_id;
			$ss = M($table);

			$cdrTable = "sales_cdr_".$task_id;
			//$sql = "UPDATE $table s INNER JOIN $cdrTable c ON s.id=c.customer_id SET s.calledflag='3' WHERE c.disposition IN ('ANSWERED','NO ANSWER')";
			$sql = "UPDATE $table s INNER JOIN $cdrTable c ON s.phone1=c.src SET s.calledflag='3' WHERE c.disposition  IN ('ANSWERED','NO ANSWER')";
			$res = $ss->execute($sql);
			$result = $ss->execute("UPDATE {$table} SET calledflag=1,locked='N' WHERE calledflag=2");   //,callresult_id=1,dealresult_id=0

			$username = $_SESSION['user_info']['username'];
			$enterprise_number = $_SESSION['user_info']['enterprise_number'];
			$mod = M("task_operation");
			$arrData = array(
				"create_time"=>date("Y-m-d H:i:s"),
				"create_user"=>$username,
				"enterprise_number"=>$enterprise_number,
				"task_id"=>$task_id,
				"operation_type"=>"重置未接号码",
			);
			$result = $mod->data($arrData)->add();

			echo "ok";
		}else{
			echo "error";
		}
	}


	//处理导入的号码：xls和csv格式  坐席导入
	function doAgentImportNumber(){
		set_time_limit(0);
		ini_set('memory_limit','400M');
		$id = $_GET['id'];
		$this->assign('id',$id);
		if( empty($_FILES["client_phones"]["tmp_name"]) ){
			goBack("请选择文件!","");
		}

		$time = time();
		$tmp_f = $_FILES["client_phones"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["client_phones"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		//黑名单数据检测
		$DNC = M('sales_dnc');
		$arrDNC = Array();
		$arrTmp = $DNC->select();
		foreach( $arrTmp AS $row){
			$arrDNC[] = $row['phonenumber'];
		}
		//原来就存在表中的电话号码，导入的时候不能和你面重复
		$table = "sales_source_" .$id;
		$SS = M($table);

		//去重
		$arrTmp = $SS->field('phone1')->select();
		$arrExist = Array(); //把所有号码放入关联数组，hash
		foreach( $arrTmp AS $row){
			$arrExist[$row['phone1']] = 'Y';
		}

		//构造待插入的SQL语句
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser,dealuser,locked,dealresult_id,createtime) values ";
		$value = "";

		$arrSex = array("男"=>"man","女"=>"woman");
		$total = $black = $repeat = $valid = $invalid = 0;
		if( "csv" == $suffix ){//CSV格式导入
			$dst_f = TMP_UPLOAD ."client_phones_".$time.".csv";
			move_uploaded_file($tmp_f,$dst_f);

			$strphones = trim( file_get_contents($dst_f) );
			$arrPhones = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			//$arrPhones = $this->assoc_unique($arrPhones2,"5");
			//dump($arrPhones);die;
			foreach( $arrPhones AS $row ){
				$row[5] = trim($row[5]);
				if( in_array($row[5],$arrDNC) ){
					$black++;
					continue;
				}elseif( $arrExist[$row[5]] == 'Y'){
					$repeat++;
					continue;
				}else{
					if( is_numeric($row[5]) ){
						$valid++;
						$str = "(";
						$str .= "'" .$row[1]. "',";//姓名
						$str .= "'" .$arrSex[$row[2]]. "',";//性别
						if($row[3]){//年龄-------------------整数
							$str .= $row[3] . ",";
						}else{
							$str .= "NULL,";
						}
						$str .= "'" .$row[4]. "',";//生日
						$str .= "'" .str_replace(" ","",str_replace("-","",$row[5])). "',";   //$row[5]手机号
						$str .= "'" .$row[6]. "',";
						$str .= "'" .$row[7]. "',";//传真
						$str .= "'" .$row[8]. "',";
						$str .= "'" .$row[9]. "',";//公司
						$str .= "'" .$row[10]. "',";
						$str .= "'" .$row[11]. "',";
						$str .= "'" .$row[12]. "',";
						$str .= "'" .$row[13]. "',";
						$str .= "'" .$_SESSION['user_info']['username']. "',";//创建人
						$str .= "'" .$_SESSION['user_info']['username']. "',";//所有者-----
						$str .= "'Y',";  //------
						$str .= "'0',";  //------
						$str .= "'" .date("Y-m-d H:i:s"). "'";

						$str .= ")";
					//}

					if(count($arrPhones) >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}

						$arrExist[$row[5]] = 'Y';   //成功的号码需要加入到去重数组中
					}else{
						$invalid++;
					}
				}

			}
		}elseif( "xls" == $suffix ){//Excel格式导入
			$dst_f = TMP_UPLOAD ."client_phones_".$time.".xls";
			//echo $dst_f;die;
			move_uploaded_file($tmp_f,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrPhones = $data->sheets[0];
			array_shift($arrPhones["cells"]);
			$arrPhones = $arrPhones["cells"];
			//$arrPhones = $this->assoc_unique($arrPhones2,"5");
			//dump($arrPhones);die;
			$avg = ceil(count($arrPhones)/8);

			//dump($arrPhones);die;
			foreach( $arrPhones AS $key=>$row ){
				$row[5] = trim($row[5]);
				if( in_array($row[5],$arrDNC) ){
					$black++;
					continue;
				}elseif( $arrExist[$row[5]] == 'Y'){
					$repeat++;
					continue;
				}else{
					if( is_numeric($row[5]) ){
						$valid++;
						$str = "(";
						$str .= "'" .$row[1]. "',";//姓名
						$str .= "'" .$arrSex[$row[2]]. "',";//性别
						if($row[3]){//年龄-------------------整数
							$str .= $row[3] . ",";
						}else{
							$str .= "NULL,";
						}
						$str .= "'" .$row[4]. "',";//生日
						$str .= "'" .str_replace(" ","",str_replace("-","",$row[5])). "',";   //$row[5]手机号
						$str .= "'" .$row[6]. "',";
						$str .= "'" .$row[7]. "',";//传真
						$str .= "'" .$row[8]. "',";
						$str .= "'" .$row[9]. "',";//公司
						$str .= "'" .$row[10]. "',";
						$str .= "'" .$row[11]. "',";
						$str .= "'" .$row[12]. "',";//省份
						$str .= "'" .$row[13]. "',";//城市-----------------------【Excel只需要填写到这里】
						$str .= "'" .$_SESSION['user_info']['username']. "',";//创建人
						$str .= "'" .$_SESSION['user_info']['username']. "',";//所有者-----
						$str .= "'Y',";  //------
						$str .= "'0',";  //------
						$str .= "'" .date("Y-m-d H:i:s"). "'";//创建时间//----------------最后一个不需要","

						$str .= ")";
					//}

					if(count($arrPhones) >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}

						$arrExist[$row[5]] = 'Y';   //成功的号码需要加入到去重数组中
					}else{
						$invalid++;
					}
				}

			}
		}else{
			goBack("文件类型错误!系统只支持处理.xls和.csv格式的Microsoft Excel文件!","");
		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser,dealuser,locked,dealresult_id,createtime) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			//echo $sql;die;
			$result2 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser,dealuser,locked,dealresult_id,createtime) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			//echo $sql;die;
			$result3 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser,dealuser,locked,dealresult_id,createtime) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			//echo $sql;die;
			$result4 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser,dealuser,locked,dealresult_id,createtime) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			//echo $sql;die;
			$result5 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser,dealuser,locked,dealresult_id,createtime) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			//echo $sql;die;
			$result6 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser,dealuser,locked,dealresult_id,createtime) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			//echo $sql;die;
			$result7 = $SS->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser,dealuser,locked,dealresult_id,createtime) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			//echo $sql;die;
			$result8 = $SS->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table(name, sex, age, birthday, phone1, phone2, fax, email, company, address, country, province, city, createuser,dealuser,locked,dealresult_id,createtime) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			//echo $sql;die;
			$result9 = $SS->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $total - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${total}个号码,${black}个在黑名单中,${repeat}个重复号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}

	}



	//任务报表
	function taskReport(){
		checkLogin();
		$sales_task = new Model("sales_task");
		$d_id = $_SESSION["user_info"]["d_id"];

		$username = $_SESSION["user_info"]["username"];
		//$arrDep = $this->getDepTreeArray();
		//$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$arrDep = R('TaskPhoneRecords/getDepTreeArray',array());
		$deptst = R('TaskPhoneRecords/getMeAndSubDeptName',array($arrDep,$d_id));

		$deptSet = rtrim($deptst,",");
		$where = "enable='Y'";
		$where .= " AND dept_id IN ($deptSet)";

		if($username == "admin"){
			$taskList = $sales_task->field("id as task_id,name")->order("createtime desc")->where("enable='Y'")->select();
		}else{
			$taskList = $sales_task->field("id as task_id,name")->order("createtime desc")->where($where)->select();
		}

		$this->assign("taskList",$taskList);


		//分配增删改的权限
        $menuname = "CDR List";
        $p_menuname = $_SESSION['menu'][$menuname]; //父菜单
        $priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

        $this->assign("excel",$priv['excel']);
        $this->assign("Play",$priv['Play']);

		//dump( $_SESSION["user_info"]);die;
		$this->display();
	}

	function taskReportData(){
		$arrReport = Array();
		$id = $_REQUEST['task_id'];
		//echo $id;die;
		$table = "sales_source_".$id;
		$ss = M($table);
		//dump($ss);die;
		$sql = "SELECT COUNT(*) AS totalnum,SUM(calledflag>1) AS calledTotal,SUM(calledflag=3) AS answeredCount FROM $table";
		$arr = $ss->query($sql);
		//echo $ss->getLastSql();die;
		//号码总数
		$totalnum = $arr[0]['totalnum']?$arr[0]['totalnum']:0;
		$arrReport[0]['totalnum'] = $totalnum;
		//呼叫总数
		$calledTotal = $arr[0]['calledTotal']?$arr[0]['calledTotal']:0;
		$arrReport[0]['calledTotal'] = $calledTotal;
		//任务完成进度
		if($totalnum == 0){
			$finishPercent = "0.0%";
		}else{
			$finishPercent = number_format(100*$calledTotal/$totalnum,1) .'%';
		}
		//$arrReport[0]['finishPercent'] = $finishPercent;
		$arrReport[0]['finishPercent'] = "<div style='width:100%;border:1px solid #ccc'><div style='width:$finishPercent;background:#F8B600;color:#000000'>$finishPercent</div></div>";
		//呼通量
		$table = "sales_cdr_".$id;
		$cdr = M($table);
		$sql = "SELECT COUNT(*) AS answeredCount ,SUM(disposition='ANSWERED') AS dealedCount,SUM(duration) AS totalDuring FROM $table";
		$arr = $cdr->query($sql);
		//echo $cdr->getLastSql();die;
		$answeredCount = $arr[0]['answeredCount']?$arr[0]['answeredCount']:0;
		$arrReport[0]['answeredCount'] = $answeredCount;

		//号码有效率
		if($answeredCount == 0){
			$okNumPercent = "0.0%";
		}else{
			$okNumPercent = number_format(100*$answeredCount/$calledTotal,1) .'%';
		}
		$arrReport[0]['okNumPercent'] = $okNumPercent;

		//呼通处理量
		$dealedCount = $arr[0]['dealedCount']?$arr[0]['dealedCount']:0;
		$arrReport[0]['dealedCount'] = $dealedCount;
		//呼通未处理量
		$notdealedCount = $answeredCount - $dealedCount;
		$arrReport[0]['notdealedCount'] = $notdealedCount;
		//呼损率
		if($notdealedCount == 0){
			$notAnsweredPercent = "0.0%";
		}else{
			$notAnsweredPercent = number_format(100*$notdealedCount/$answeredCount,1) .'%';
		}
		$arrReport[0]['notAnsweredPercent'] = $notAnsweredPercent;
		//通话总时长
		$totalDuring = $arr[0]['totalDuring']?$arr[0]['totalDuring']:0;

		$arrReport[0]['totalDuring'] = ceil($totalDuring/60);

		$rowsList = count($arrReport) ? $arrReport : false;
		$arrtask["total"] = count($arrReport);
		$arrtask["rows"] = $rowsList;

		echo json_encode($arrtask);
	}


	function outBoundList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Task";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['list_enabled'] == "Y" ){
				if($val['en_name'] != "createuser" ){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					$arrField[0][$i]['width'] = "100";
				}
				if( $val['tiptools_enabled'] == 'Y' ||  $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}

		$arrFd1 = array("field"=>"createtime","title"=>"创建时间","width"=>"130","sortable"=>"true");
		$arrFd = array("field"=>"modifytime","title"=>"最后呼叫时间","width"=>"130","sortable"=>"true");
		$arrFd2 = array("field"=>"ck","checkbox"=>true);
		$arrFd3 = array("field"=>"dealuser","title"=>"创建人工号","width"=>"100");
		$arrFd4 = array("field"=>"cn_name","title"=>"创建人姓名","width"=>"100");
		$arrFd5 = array("field"=>"dealresult_id","title"=>"客户状态","width"=>"100");
		$arrFd6 = array("field"=>"calledflag","title"=>"呼叫状态","width"=>"100");
		$arrFd8 = array("field"=>"key_value","title"=>"按键值","width"=>"50");
		$arrFd9 = array("field"=>"recordtag","title"=>"坐席标记","width"=>"80");
		//$arrFd7 = array("field"=>"brand","title"=>"是否已经转交到呼入平台","width"=>"150");
		//array_push($arrField[0],$arrFd7);
		array_push($arrField[0],$arrFd8);
		array_push($arrField[0],$arrFd9);
		array_unshift($arrField[0],$arrFd6);
		array_unshift($arrField[0],$arrFd5);
		array_unshift($arrField[0],$arrFd4);
		array_unshift($arrField[0],$arrFd3);
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd1);
		array_unshift($arrField[0],$arrFd2);
		//dump($fielddTpl);die;
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("fielddTpl",$fielddTpl);

		$areaTpl = implode(",",$areaTpl2);
		$this->assign("areaTpl",$areaTpl);

		$para_sys = readS();
		$export_rule = $para_sys["export_rule"];
		$this->assign("export_rule",$export_rule);

		$task_id = $_GET["task_id"];
		$sales_task = new Model("sales_task");
		$arrTF = $sales_task->where("id = '$task_id'")->find();
		$this->assign("task_id",$task_id);
		$this->assign("deptId",$arrTF["dept_id"]);

		$this->display();
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	function assoc_unique($arr, $key){
	   $tmp_arr = array();
	   foreach($arr as $k => $v){
		 if(in_array($v[$key], $tmp_arr)){
		   unset($arr[$k]);
		}else{
		  $tmp_arr[] = $v[$key];
		}
	  }
	  sort($arr);
	  return $arr;
	}


	function FetchRepeatMemberInArray($array) {
		$unique_arr = array_unique ( $array );
		$repeat = array_diff_assoc ( $array, $unique_arr );

		$repeat_arr = array_unique($repeat);
		return $repeat_arr;
	}


	function removeDuplicateData($id){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$source = new Model("sales_source_".$id);
		$cmData = $source->field("id,phone1")->select();
		foreach($cmData as $val){
			$tt[] = $val["phone1"];
		}
		$yy = array_unique($tt);
		$kk = $this->FetchRepeatMemberInArray($tt);
		foreach($kk as $key=>$val){
			if($val){
				$phone[] = $val;
			}
		}
		//dump($phone);die;
		$tt = array_slice($phone,0,5000);
		$ss = "'".implode("','",$tt)."'";
		//dump(explode(",",$ss));die;
		$usersList = $source->field("id,phone1")->where("phone1 in ($ss)")->select();
		$arr = $this->assoc_unique($usersList,"phone1");
		//dump($usersList);die;
		foreach($arr as $val){
			$ids[] = $val["id"];
		}
		$id = "'".implode("','",$ids)."'";
		$res = $source->where("id in ($id)")->delete();
		//echo "1111111<br>";
		sleep(1);
		return $res;
	}
	/*
	function removePhone($id){
		//set_time_limit(0);
		//ini_set('memory_limit','-1');
		$source = new Model("sales_source_".$id);
		$total = $source->count();
		$avg = ceil($total/5000);
		$sql = "SELECT COUNT(phone1) FROM sales_source_".$id." GROUP BY phone1 HAVING COUNT(phone1) > 1";
		$re = $source->query($sql);
		if(!$re){
			return false;
		}
		//$del_sql = "DELETE FROM sales_source_".$id." WHERE id IN(SELECT id FROM (SELECT MAX(id) AS id, COUNT(phone1) AS COUNT  FROM sales_source_".$id." GROUP BY phone1 HAVING COUNT > 1 ORDER BY COUNT DESC) AS tab)";
		//$num = $source->execute($del_sql);

		//php删除重复数据的方法
		if($avg>1){
			for($i=1;$i<=$avg;$i++){
				$arrNum[$i] = $this->removeDuplicateData($id);
			}
			$num = array_sum($arrNum);
		}else{
			$num = $this->removeDuplicateData($id);
		}
		return $num;
		//dump($arrNum);die;
	}
	*/

	function removePhone($id){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$source = new Model("sales_source_".$id);
		$arrData = $source->order("id asc")->field("id,phone1")->select();
		$arrTmp = $this->getRepeatArray($arrData,"phone1");
		foreach($arrTmp as $val){
			$arrId[] = $val["id"];
		}
		//dump($arrTmp);die;
		$strId = "'".implode("','",$arrId)."'";
		$res = $source->where("id in ($strId)")->delete();

		/*
		$del_sql = "DELETE FROM sales_source_".$id." WHERE id IN(SELECT id FROM (SELECT MAX(id) AS id, COUNT(phone1) AS COUNT  FROM sales_source_".$id." GROUP BY phone1 HAVING COUNT > 1 ORDER BY COUNT DESC) AS tab)";
		$res = $source->execute($del_sql);
		*/
		return $res;
	}

	function getRepeatArray($arr, $key){
		$tmp_arr = array();
		//$tmp = array();
		foreach($arr as $k => $v){
			if(in_array($v[$key], $tmp_arr)){
				$tmp[] = $v;
			}else{
				$tmp_arr[] = $v[$key];
			}
		}
		sort($tmp);
		return $tmp;
	}

	function ttts(){
		/*$aa = $this->removePhone("27","24017");
		dump($aa);die;
		*/
		$source = new Model("sales_source_30");
		$arrData = $source->order("id asc")->field("id,phone1")->select();
		$arrTmp = $this->getRepeatArray($arrData,"phone1");
		foreach($arrTmp as $val){
			$arrId[] = $val["id"];
		}
		//dump($arrTmp);die;
		$strId = "'".implode("','",$arrId)."'";
		$res = $source->where("id in ($strId)")->delete();
		//dump($arrData);die;
	}


	function deptUser(){
		$id_dept = $_REQUEST['id_dept'];
		$web_type = $_REQUEST['web_type'];
		$dept_user = M('users');
		$where = 'd_id='.$id_dept;
		$arrData = $dept_user->field('username,extension')->where($where)->select();

		$arrF = array();
		if($web_type == "edit"){
			$task_id = $_REQUEST['task_id'];
			import('ORG.Pbx.bmi');
			$bmi = new bmi();
			$bmi->loadAGI();
			$arrA = $bmi->getAllQueueMember();
			$arrF = $arrA["AutoCall_$task_id"]['membername'];
		}
		$str="";
		if($arrData){
			foreach($arrData as $k=>$v){
				if(in_array($v['extension'],$arrF)){
					$str.="<label style='width:120px;'><input type='checkbox' name='join_queue_method2[][extension]' checked value='".$v['extension']."' /><span style='padding:1px 10px 0px 1px;line-height:17px;'>".$v['username']."/".$v['extension']."</span></label>";
				}else{
					$str.="<label style='width:120px;'><input type='checkbox' name='join_queue_method2[][extension]' value='".$v['extension']."' /><span style='padding:1px 10px 0px 1px;line-height:17px;'>".$v['username']."/".$v['extension']."</span></label>";
				}
			}
		}


		echo json_encode(array('success'=>true,'msg'=>$str));
	}
	public static function extendtion($time=0){

		$seconds = $time %60 ;
		if($seconds==0){
			$seconds='00';
		}
		$minute = round($time/60) ;
		$minte = (!empty($minute))?$minte.':':'00:';
		$hour = round($time/60/60) ;
		$hour = (!empty($hour))?$hour.':':'00:';
		return $hour.$minte.$seconds;
	}
	public function changeStatus()
	{
		$type = $_POST['changeType'];
		$id = $_POST['task_id'];
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$file_name = $db_name.':'.$id;
 
		if($type ==1){
			
			$status = $_POST['status'];

			$data = array(
				'autocall_status'=>$status,
				'taskSwitch'=>$status,
			);
			// fopen("/var/tmp/autocall/$file_name", "w");
			
			// dump(fwrite("/var/tmp/autocall/$file_name",'1'));die();
			M('sales_task')->where(array('id'=>$id))->save($data);
			$res = file_put_contents("/var/tmp/autocall/$file_name",'1'); 
		}else if($type = 2){
			 M('sales_task')->where(array('id'=>array('gt',0)))->save(array('autocall_status'=>'N',
				'taskSwitch'=>'N')); 
			$res = @unlink("/var/tmp/autocall/$file_name");
 
		}else{
			echo json_encode(array('status'=>0));exit;
		}
		if($res){
			echo json_encode(array('status'=>1));exit;
		}else{
			echo json_encode(array('status'=>0));exit;

		}
	}
	public function resetPhone()
	{

		if(IS_POST){
			$task_id = $_GET['task_id'];
			if(empty($_POST['RefusedType'])){
				echo json_encode(array('status'=>2,'msg'=>'请选择重置范围'));exit;
			}
			$RefusedType = $_POST['RefusedType'];

			switch ($RefusedType) {
				case '1':

					$where .= 'name_36 !=1';
					break;
				case '2':
					$name36Data = implode(',',$_POST['name36']);
					$where .= 'name_37 in ('.$nameData.')';
					if(!empty($_POST['aigrade']))$where .= ' and aigrade="'.$POST['aigrade'].'"';
					if(!empty($_POST['grade']))$where .= ' and grade ="'.$_POST['grade'].'"';
					if(!empty($_POST['name34']))$where .= ' and name_34="'.$_POST['name34'].'"';
					if(!empty($_POST['gtBillsec']) && !empty($_POST['ltBillsec']))$where .= ' and billsec between "'.$_POST['gtBillsec'].'" and "'.$_POST['ltBillsec'].'"';
					if(empty($_POST['gtBillsec']) || empty($_POST['ltBillsec'])){
						echo json_encode(array('status'=>2,'msg'=>'请选择通话时长区间'));exit;
					}
					break;

				default:
					$where .= 'name_36 !=1';
					break;
			}

			$table = 'sales_source_'.$task_id;
			$cdrTable = "sales_cdr_".$task_id;
			//$sql = "UPDATE $table s INNER JOIN $cdrTable c ON s.id=c.customer_id SET s.calledflag='3' WHERE c.disposition IN ('ANSWERED','NO ANSWER')";
			$sql = "UPDATE $table SET calledflag='3' WHERE ".$where;
			$res = M('sales_source_'.$task_id)->execute($sql);
			// dump($sql);die();
			$results = M('sales_source_'.$task_id)->execute("UPDATE {$table} SET calledflag=1,locked='N' WHERE ".$where);   //,callresult_id=1,dealresult_id=0

			$username = $_SESSION['user_info']['username'];
			$enterprise_number = $_SESSION['user_info']['enterprise_number'];
			$mod = M("task_operation");
			$arrData = array(
				"create_time"=>date("Y-m-d H:i:s"),
				"create_user"=>$username,
				"enterprise_number"=>$enterprise_number,
				"task_id"=>$task_id,
				"operation_type"=>"重置未接号码",
			);
			$result = $mod->data($arrData)->add();
			if($results ==true || $results ==1 ){
				echo json_encode(array('status'=>1,'msg'=>'重置成功'));exit;
			}else{
				echo json_encode(array('status'=>2,'msg'=>'重置失败'));exit;

			}
		}
		$this->display();
	}
	function unicode_decode($name){
 
		$json = $name;
		$arr = json_decode($json,true);
		if(empty($arr)) return '';
		return $arr;
	  
	}
	public function uploadFile()
	{  
 
		
		if($_FILES["avatar"]["error"]){
			
			echo json_encode(array('status'=>0,'errorMsg'=>$_FILES["avatar"]["error"]));exit; 
   
		}else{

			if(($_FILES["avatar"]["type"]=="image/png"||$_FILES["avatar"]["type"]=="image/jpeg")){
				//防止文件名重复
				$filename ="images/headImage/".time().rand(99999).pathinfo($_FILES["avatar"]["name"])['extension'];
				//转码，把utf-8转成gb2312,返回转换后的字符串， 或者在失败时返回 FALSE。
				$filename =iconv("UTF-8","gb2312",$filename);
				//检查文件或目录是否存在
				if(file_exists($filename)){
					echo json_encode(array('status'=>0,'errorMsg'=>'该文件已存在'));exit; 
				}else{  
					//保存文件,   move_uploaded_file 将上传的文件移动到新位置  
					move_uploaded_file($_FILES["avatar"]["tmp_name"],$filename);//将临时地址移动到指定地址 
					echo json_encode(array('status'=>1,'imgName'=>$filename));exit;
				}        
			}else{
				echo json_encode(array('status'=>0,'errorMsg'=>'文件类型不对'));exit;
				 
			}
		}
	}


}
?>
