<?php
class QualityInspectionAction extends Action{
	function qualityList(){
		checkLogin();
		$sales_task = new Model("sales_task");
		$d_id = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = rtrim($deptst,",");
		$where = "enable='Y'";
		$where .= " AND dept_id IN ($deptSet)";
		if($username == "admin"){
			$taskList = $sales_task->field("id as task_id,name")->order("createtime")->where("enable='Y'")->select();
		}else{
			$taskList = $sales_task->field("id as task_id,name")->order("createtime")->where($where)->select();
		}
		$this->assign("taskList",$taskList);
		//dump( $_SESSION["user_info"]);die;
		//dump($sales_task->getLastSql());

		//分配增删改的权限
		$menuname = "Autocall Quality monitoring";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }


	function taskRecordsList(){
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$task_id = $_REQUEST['name'];
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $username = $_REQUEST['username'];

		$where = "1 ";
		$where .= " AND disposition='ANSWERED' ";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";
		$where .= empty($src)?"":" AND src ='$src'";
		$where .= empty($dst)?"":" AND dst ='$dst'";
		$where .= empty($username)?"":" AND workno ='$username'";

		$workno = $_SESSION["user_info"]["username"];
		/*
		if($workno != "admin"){
			$where .= " AND workno = '$workno'";
		}
		*/
		import('ORG.Util.Page');
		$sales_task = new Model("sales_task");
		$task_pre = $sales_task->where("id = $task_id")->find();
		$qualitypercent = $task_pre["qualitypercent"];    //预定的质检量
		//dump($qualitypercent);

		$this->assign("taskId",$name);

		$sales_cdr = new Model("sales_cdr_".$task_id);
		$count = $sales_cdr->where($where)->count();
		$qualitype_Y = $sales_cdr->where("$where AND qualitype = 'Y'")->count();
		$percentage = floor(($qualitype_Y/$count)*100);    //完成的质检量

		if( ($percentage >= $qualitypercent) && $count > 0){
			$complete_msg = "<span style='color:red;'>"."预定的质检量:  "."<span style='color:#0066CC;'>".$qualitypercent."%</span>"."  该任务已经完成！</span>";
		}elseif( $count > 0 ){
			$undone_msg = "预定的质检量："."<span style='color:#0066CC;'>".$qualitypercent."%</span> "."   已完成的质检量：".$percentage."%";
		}else{
			$noData = "该任务没有数据！";
		}

		//dump($percentage);
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$sales_cdrList = $sales_cdr->order("calldate desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $sales_cdr->getLastSql();die;

		$i = 0;
		foreach($sales_cdrList as $vm){
			$sales_cdrList[$i]["billsec"] = sprintf("%02d",intval($vm["billsec"]/3600)).":".sprintf("%02d",intval(($vm["billsec"]%3600)/60)).":".sprintf("%02d",intval((($vm[billsec]%3600)%60)));

			$arrTmp = explode('.',$vm["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$vm["uniqueid"].".WAV";

			if(file_exists($WAVfile)){
				$sales_cdrList[$i]['operating'] = "<a href='javascript:void(0);' onclick=\"openQuality($task_id,"."'".trim($vm["uniqueid"])."'".","."'".$vm["src"]."'".","."'".$vm["workno"]."'".","."'".$vm["id"]."'".",'"."autocall_".$vm["src"]."_".$vm["uniqueid"].".mp3"."'".")\" >质检</a>";
			}
			$i++;
		}
		unset($i);
		//echo $sales_cdr->getLastSql();
		//dump($sales_cdrList);die;

		$cdr_status_row = array('Y'=>'<span style="color:red;">已质检</span>','N'=>'未质检');
		foreach($sales_cdrList as &$val){
			$status = $cdr_status_row[$val["qualitype"]];
			$val["qualitype"] = $status;
		}

		$rowsList = count($sales_cdrList) ? $sales_cdrList : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		if($complete_msg){
			$ary["complete_msg"] = $complete_msg;
		}
		if($undone_msg){
			$ary["undone_msg"] = $undone_msg;
		}
		if($noData){
			$ary["noData"] = $noData;
		}

		echo json_encode($ary);
	}
	/*
	function playTaskRecord(){
		$clientphone = $_GET['phone'];
		$uniqueid = $_GET['uniqueid'];
		$task_id = $_GET['task_id'];
		$workno = $_GET['workno'];
		$cdr_id = $_GET['cdr_id'];
		$userfield = $_GET['userfield'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		//http://192.168.1.69/agent.php?m=TasksCallRecords&a=playCDR&uniqueid=1363344674.456
		$AudioURL = "agent.php?m=TasksCallRecords&a=downloadCDR&clientphone=${clientphone}&uniqueid=${uniqueid}". "&userfield=".$userfield;;
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->assign("clientphone",$clientphone);
		$this->assign("task_id",$task_id);
		$this->assign("workno",$workno);
		$this->assign("cdr_id",$cdr_id);

		$norm = new Model("normtype");
		$normList = $norm->select();
		$this->assign("normList",$normList);

		$sales_cdr = new Model("sales_cdr_".$task_id);
		$vData = $sales_cdr->where("id = $cdr_id")->find();
		$this->assign("vData",$vData);

		$this->display();
	}
	*/
	function playTaskRecord(){
		checkLogin();
		$clientphone = $_GET['phone'];
		$task_id = $_GET['task_id'];
		$workno = $_GET['workno'];
		$cdr_id = $_GET['cdr_id'];
		$userfield = $_GET['userfield'];

		$uniqueid = $_GET['uniqueid'];
		header('Content-Type: text/html; charset=utf-8');
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$AudioURL = "index.php?m=CDR&a=downloadCDR&uniqueid=${uniqueid}";
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);

		$this->assign("clientphone",$clientphone);
		$this->assign("task_id",$task_id);
		$this->assign("workno",$workno);
		$this->assign("cdr_id",$cdr_id);

		$norm = new Model("normtype");
		$normList = $norm->select();
		$this->assign("normList",$normList);

		$sales_cdr = new Model("sales_cdr_".$task_id);
		$vData = $sales_cdr->where("id = $cdr_id")->find();
		$this->assign("vData",$vData);

		$this->display();
	}

	function insertQuality(){
		//dump($_POST);die;
		$username = $_SESSION['user_info']['username'];
		$norm = new Model("normtype");
		$normList = $norm->select();
		foreach($normList as $vm){
			$score["score_".$vm[id]] = $_POST[$vm[id]];
		}
		$score["score"] = array_sum($score);
		$score["createtime"] = date("Y-m-d H:i:s");
		$score["task_id"] = $_POST['task_id'];
		$score["username"] = $_POST['workno'];
		$score["quality_username"] = $username;
		//dump($score);die;
		$qualityscore = new Model("qualityscore");
		$result = $qualityscore->data($score)->add();


		if ($result){
			$task_id = $_POST['task_id'];
			$sales_cdr = new Model("sales_cdr_".$task_id);
			$cdrId = $_POST['cdr_id'];
			$arrData = array(
							"quality_level" => $_POST['quality_level'],
							"quality_content" => $_POST['quality_content'],
							"qualitype"=>"Y",
							"quality_workno"=>$username,
						);
			//$taskRes = $sales_cdr->where("id = $cdrId")->save( array("qualitype"=>"Y") );
			$taskRes = $sales_cdr->data($arrData)->where("id = $cdrId")->save();
			echo "ok";
		} else {
			echo "Error";
		}
	}


	//回访质检
	function visitRecordList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Visit Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		$this->display();
	}

	function visitRecordData(){
		$username = $_SESSION['user_info']['username'];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$visit_record = new Model("visit_record");

		$createname = $_REQUEST['createname'];
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$content = $_REQUEST['content'];
		$visit_name = $_REQUEST['visit_name'];
		$quality_level = $_REQUEST['quality_level'];
		$quality_enabled = $_REQUEST['quality_enabled'];

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");


		$where = "1 ";
		$where .= empty($startime)?"":" AND v.createtime >'$startime'";
        $where .= empty($endtime)?"":" AND v.createtime <'$endtime'";
        $where .= empty($content)?"":" AND v.visit_content like '%$content%'";
        $where .= empty($createname)?"":" AND v.createname like '%$createname%'";
		$where .= empty($visit_name)?"":" AND c.name like '%$visit_name%'";
		$where .= empty($quality_level)?"":" AND v.quality_level = '$quality_level'";
		$where .= empty($quality_enabled)?"":" AND v.quality_enabled = '$quality_enabled' AND v.visit_type='phone' AND v.accountcode IS NOT NULL";

		if($username == 'admin'){
			//$count2 = $visit_record->table("visit_record v")->field("v.id,v.createtime,v.quality_content,v.quality_level,v.customer_id,v.quality_enabled,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->group("v.id")->where($where)->select();
			$count = $visit_record->table("visit_record v")->join("customer c on v.customer_id = c.id")->where($where)->count();
		}else{
			//$count2 = $visit_record->table("visit_record v")->field("v.id,v.createtime,v.quality_content,v.quality_level,v.quality_enabled,v.customer_id,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->group("v.id")->where("$where  AND v.dept_id in ($deptSet)")->select();
			$count = $visit_record->table("visit_record v")->join("customer c on v.customer_id = c.id")->where("$where  AND v.dept_id in ($deptSet)")->count();
		}
		//$count = count($count2);

		//echo $visit_record->getLastSql();die;
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == 'admin'){
			$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.id,v.createtime,v.quality_content,v.quality_level,v.customer_id,v.quality_enabled,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->group("v.id")->where($where)->select();
		}else{
			$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.id,v.createtime,v.quality_content,v.quality_level,v.quality_enabled,v.customer_id,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->group("v.id")->where("$where  AND v.dept_id in ($deptSet)")->select();
		}


		//echo $visit_record->getLastSql();die;
		$i = 0;
		$row = array("phone"=>"电话回访","qq"=>"QQ回访");
		$row2 = array("Y"=>"<span style='color:red'>已质检</sapn>","N"=>"未质检");
		foreach($visitData as &$v){
			$v["accountcode"] = trim($v["accountcode"]);

			$arrTmp = explode('.',$v["accountcode"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$v["accountcode"].".WAV";

			if( file_exists($WAVfile) && $v['visit_type'] == 'phone' ){
				$visitData[$i]['operations'] .= "<a href='javascript:void(0);' onclick=\"openQuality("."'".$v["accountcode"]."','".$v["visit_phone"]."','".$v["createname"]."','".$v["id"]."','".$v["customer_id"]."'".")\" > 质检 </a>";
			}else{
				$visitData[$i]['operations'] = "";
			}

			$visit_type = $row[$v['visit_type']];
			$v['visit_type'] = $visit_type;

			$quality_enabled = $row2[$v['quality_enabled']];
			$v['quality_enabled'] = $quality_enabled;

			if($username != "admin"){
				if($para_sys["hide_phone"] =="yes"){
					$v["visit_phone"] = substr($v["visit_phone"],0,3)."***".substr($v["phone2"],-4);
					if($v["phone2"]){
						$v["phone2"] = substr($v["phone2"],0,3)."***".substr($v["phone2"],-4);
					}
				}
			}

			$i++;
		}
		unset($i);


		$userArr = readU();
		$user_name = $userArr["service_user"];
		$cnName = $userArr["cn_user"];
		$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];
		$i = 0;
		foreach($visitData as $val){
			$visitData[$i]['cn_name'] = $cnName[$val["createname"]];
			$visitData[$i]['dept_id'] = $deptId_user[$val["createname"]];
			$visitData[$i]['dept_name'] = $deptName_user[$val["createname"]];
			$i++;
		}

		//dump($visitData);die;

		$rowsList = count($visitData) ? $visitData : false;
		$arrV["total"] = $count;
		$arrV["rows"] = $rowsList;


		echo json_encode($arrV);
	}


	function exportVisitRecord(){
		$username = $_SESSION['user_info']['username'];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$visit_record = new Model("visit_record");

		$createname = $_REQUEST['createname'];
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$content = $_REQUEST['content'];
		$visit_name = $_REQUEST['visit_name'];
		$quality_level = $_REQUEST['quality_level'];
		$quality_enabled = $_REQUEST['quality_enabled'];

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");


		$where = "1 ";
		$where .= empty($startime)?"":" AND v.createtime >'$startime'";
        $where .= empty($endtime)?"":" AND v.createtime <'$endtime'";
        $where .= empty($content)?"":" AND v.visit_content like '%$content%'";
        $where .= empty($createname)?"":" AND v.createname like '%$createname%'";
		$where .= empty($visit_name)?"":" AND c.name like '%$visit_name%'";
		$where .= empty($quality_level)?"":" AND v.quality_level = '$quality_level'";
		$where .= empty($quality_enabled)?"":" AND v.quality_enabled = '$quality_enabled' AND v.visit_type='phone' AND v.accountcode IS NOT NULL";

		if($username == 'admin'){
			$count = $visit_record->table("visit_record v")->join("customer c on v.customer_id = c.id")->where($where)->count();
		}else{
			$count = $visit_record->table("visit_record v")->join("customer c on v.customer_id = c.id")->where("$where  AND v.dept_id in ($deptSet)")->count();
		}
		//echo $visit_record->getLastSql();die;
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == 'admin'){
			$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.id,v.createtime,v.quality_content,v.quality_level,v.customer_id,v.quality_enabled,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->where($where)->select();
		}else{
			$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.id,v.createtime,v.quality_content,v.quality_level,v.quality_enabled,v.customer_id,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->where("$where  AND v.dept_id in ($deptSet)")->select();
		}


		//echo $visit_record->getLastSql();die;
		$userArr = readU();
		$user_name = $userArr["service_user"];
		$cnName = $userArr["cn_user"];
		$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = array("phone"=>"电话回访","qq"=>"QQ回访");
		$content = "回访时间,工号,姓名,回访客户,回访内容,回访类型,回访号码,电话,邮箱地址,QQ号,质检等级,评分内容\r\n";
		foreach($visitData as &$val){
			$visit_type = $row[$val['visit_type']];
			$val['visit_type'] = $visit_type;

			$val['cn_name'] = $cnName[$val["createname"]];
			$val['visit_content'] = str_replace("\r\n"," ",$val['visit_content']);

			$rows = $val['createtime'].",".
					$val['createname'].",".
					$val['cn_name'].",".
					$val['name'].",".
					$val['visit_content'].",".
					//trim($val['visit_content']).",".
					$val['visit_type'].",".
					$val['phone1'].",".
					$val['phone2'].",".
					$val['email'].",".
					$val['qq_number'].",".
					$val['quality_level'].",".
					$val['quality_content']."\r\n";
			$content .= $rows;
		}

		$content = iconv("utf-8","gb2312",$content);
		//dump($content);die;
		$name = "回访记录".date("Ymd");
		$filename = iconv("utf-8","gb2312",$name);
		$this->export_csv($filename,$content);

	}

	function exportVisitRecordToExecl(){
		$username = $_SESSION['user_info']['username'];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$visit_record = new Model("visit_record");

		$createname = $_REQUEST['createname'];
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$content = $_REQUEST['content'];
		$visit_name = $_REQUEST['visit_name'];
		$quality_level = $_REQUEST['quality_level'];
		$quality_enabled = $_REQUEST['quality_enabled'];

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");


		$where = "1 ";
		$where .= empty($startime)?"":" AND v.createtime >'$startime'";
        $where .= empty($endtime)?"":" AND v.createtime <'$endtime'";
        $where .= empty($content)?"":" AND v.visit_content like '%$content%'";
        $where .= empty($createname)?"":" AND v.createname like '%$createname%'";
		$where .= empty($visit_name)?"":" AND c.name like '%$visit_name%'";
		$where .= empty($quality_level)?"":" AND v.quality_level = '$quality_level'";
		$where .= empty($quality_enabled)?"":" AND v.quality_enabled = '$quality_enabled' AND v.visit_type='phone' AND v.accountcode IS NOT NULL";

		if($username == 'admin'){
			$count = $visit_record->table("visit_record v")->join("customer c on v.customer_id = c.id")->where($where)->count();
		}else{
			$count = $visit_record->table("visit_record v")->join("customer c on v.customer_id = c.id")->where("$where  AND v.dept_id in ($deptSet)")->count();
		}
		//echo $visit_record->getLastSql();die;
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == 'admin'){
			$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.id,v.createtime,v.quality_content,v.quality_level,v.customer_id,v.quality_enabled,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->where($where)->select();
		}else{
			$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.id,v.createtime,v.quality_content,v.quality_level,v.quality_enabled,v.customer_id,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->where("$where  AND v.dept_id in ($deptSet)")->select();
		}


		//echo $visit_record->getLastSql();die;
		$userArr = readU();
		$user_name = $userArr["service_user"];
		$cnName = $userArr["cn_user"];
		$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = array("phone"=>"电话回访","qq"=>"QQ回访");
		foreach($visitData as &$val){
			$visit_type = $row[$val['visit_type']];
			$val['visit_type'] = $visit_type;

			$val['cn_name'] = $cnName[$val["createname"]];
			$val['visit_content'] = str_replace("\r\n"," ",$val['visit_content']);

		}


		vendor("PHPExcel176.PHPExcel");
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置行高度
		//$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(22);
		//$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);

		//设置字体大小加粗
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		//设置水平居中
		for($j='A';$j<='L';$j++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($j)->setWidth(20);  //设置单元格（列）的宽度
			$objPHPExcel->getActiveSheet()->getStyle($j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//合并单元格
		//$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');

		//设置表 标题内容
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue('A1', '回访时间')
			->setCellValue('B1', '工号')
			->setCellValue('C1', '姓名')
			->setCellValue('D1', '回访客户')
			->setCellValue('E1', '回访内容')
			->setCellValue('F1', '回访类型')
			->setCellValue('G1', '回访号码')
			->setCellValue('H1', '电话')
			->setCellValue('I1', '邮箱地址')
			->setCellValue('J1', 'QQ号')
			->setCellValue('K1', '质检等级')
			->setCellValue('L1', '评分内容');
		$i = 2;
		foreach($visitData as $val){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $val['createtime']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $val['createname']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $val['cn_name']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val['name']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $val['visit_content']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $val['visit_type']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $val['phone1']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $val['phone2']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $val['email']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $val['qq_number']);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $val['quality_level']);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $val['quality_content']);
			$i++;
		}
		// Rename sheet
		//$objPHPExcel->getActiveSheet()->setTitle('通话记录');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$name = "回访记录";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');


	}



	function deleteUniqueid(){
		$userfield = $_GET["userfield"];
		$WAVfile = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp) .'/'. $userfield."WAV";
		unlink($WAVfile);
	}

	function downloadCDR(){
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		$WAVfile = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp) .'/'. $userfield;
		if(file_exists($WAVfile .".WAV")){
			$WAVfile = $WAVfile .".WAV";
		}elseif(file_exists($WAVfile .".wav")){
			$WAVfile = $WAVfile .".wav";
		}
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=" .$userfield .".mp3");
        system("/usr/local/bin/sox $WAVfile -t mp3 -");

	}
	////192.168.1.210/agent.php?m=CustomerData&a=playQualityRecord&phone=13828863511&uniqueid=1381667397.2680&workno=test8201&id=1&userfield=203001_8201_1381667397.2680
	function playQualityRecord(){
		checkLogin();
		$clientphone = $_GET['phone'];
		//$userfield = $_GET['userfield'];
		$workno = $_GET['workno'];
		$id = $_GET['id'];
		$customer_id = $_GET['customer_id'];

		$uniqueid = $_GET['uniqueid'];
		header('Content-Type: text/html; charset=utf-8');
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$AudioURL = "index.php?m=CDR&a=downloadCDR&uniqueid=${uniqueid}";
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);

		$this->assign("clientphone",$clientphone);
		$this->assign("workno",$workno);
		$this->assign("id",$id);

		$norm = new Model("normtype");
		$normList = $norm->select();
		$this->assign("normList",$normList);



		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				if($val['text_type'] == '1'){
					$textTpl[] = $val;
				}
				if($val['text_type'] == '2'){
					$val["field_values"] = json_decode($val["field_values"] ,true);
					$selectTpl[] = $val;
				}
				if($val['text_type'] == '3'){
					$areaTpl[] = $val;
				}
				if($val['text_type'] == '4'){
					$timeTpl[] = $val;
				}
				$arrF[] = $val['en_name'];
			}
		}
		$fields = "`".implode("`,`",$arrF)."`".",`country`,`province`,`city`,`district`";

		$this->assign("textTpl",$textTpl);
		$this->assign("selectTpl",$selectTpl);
		$this->assign("areaTpl",$areaTpl);
		$this->assign("timeTpl",$timeTpl);

		$this->assign("userfield",$userfield);

		$customer = new Model("customer");
		$cData = $customer->field($fields)->where("id = '$customer_id'")->find();
		$row = getSelectCache();
		//dump($row);die;
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				$cData[$val['en_name']] = $row[$val['en_name']][$cData[$val['en_name']]];
			}
		}
		$row2 = array("Y"=>"是","N"=>"否");
		if($cData["sms_cust"]){
			$cData["sms_cust"] = $row2[$cData["sms_cust"]];
		}
		$this->assign("custList",$cData);
		//echo $customer->getLastSql();die;
		//dump($cData);die;
		$visit_record = new Model("visit_record");
		$vData = $visit_record->where("id = '$id'")->find();
		$this->assign("vData",$vData);

		$this->display();
	}


	function deleteVisitRecord(){
		$id = $_REQUEST["id"];
		$visit_record = new Model("visit_record");
		$result = $visit_record->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function insertVisitQuality(){
		$username = $_SESSION['user_info']['username'];
		$norm = new Model("normtype");
		$normList = $norm->select();
		foreach($normList as $vm){
			$score["score_".$vm[id]] = $_POST[$vm[id]];
		}
		$score["score"] = array_sum($score);
		$score["createtime"] = date("Y-m-d H:i:s");
		$score["username"] = $_POST['workno'];
		$score["quality_type"] = "visit";
		$score["quality_username"] = $username;
		//dump($score);die;
		$qualityscore = new Model("qualityscore");
		$result = $qualityscore->data($score)->add();


		if ($result){
			$visit_record = new Model("visit_record");
			$id = $_POST['id'];
			$arrData = array(
							"quality_level" => $_POST['quality_level'],
							"quality_content" => $_POST['quality_content'],
							"quality_enabled"=>"Y",
							"quality_workno"=>$username,
						);
			//$taskRes = $visit_record->where("id = $id")->save( array("quality_enabled"=>"Y") );
			$taskRes = $visit_record->data($arrData)->where("id = $id")->save();
			echo "ok";
		} else {
			echo "Error";
		}
	}


	function qualityAuditList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Quality Audit";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function qualityAuditData(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$endtime = Date('Y-m-d',$_GET['ts_end']) ." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){//上个月的起始时间
				$day = Date('d',$_GET['ts_end']);
				$startime = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1)) ." 00:00:00";
			}else{
				$startime = Date('Y-m-d',$_GET['ts_start']) ." 00:00:00";
			}
		}else{
			//$date_start = $_GET['date_start'];
			//$date_end = $_GET['date_end'];
			$startime = $_REQUEST["startime"];
			$endtime = $_REQUEST["endtime"];
		}

		$task_id = $_REQUEST["task_id"];

		$history = new Model("sales_contact_history_".$task_id);
		$createname = $_REQUEST["createname"];
		$auditworkno = $_REQUEST["auditworkno"];
		$phone = $_REQUEST["phone"];
		$billsec = $_REQUEST["billsec"];
		$dealresult = $_REQUEST["dealresult"];
		$quality_enabled = $_REQUEST["quality_enabled"];
		$quality_level = $_REQUEST["quality_level"];
		//dump($_REQUEST);
		$where = "1 ";
		$where .= " AND uniqueid is not null AND uniqueid != '' AND callstatus='ANSWERED'";
		$where .= empty($startime) ? "" : " AND dealtime >= '$startime'";
		$where .= empty($endtime) ? "" : " AND dealtime <= '$endtime'";
		$where .= empty($createname) ? "" : " AND dealuser = '$createname'";
		$where .= empty($auditworkno) ? "" : " AND audit_workno = '$auditworkno'";
		$where .= empty($phone) ? "" : " AND phone1 like '%$phone%'";
		//$where .= empty($dealresult) ? "" : " AND dealresult = '$dealresult'";
		if($dealresult == "0"){
			$where .= " AND dealresult = '0'";
		}else{
			$where .= empty($dealresult)?"":" AND dealresult = '$dealresult'";
		}
		$where .= empty($quality_enabled) ? "" : " AND quality_enabled = '$quality_enabled'";
		$where .= empty($quality_level) ? "" : " AND quality_level = '$quality_level'";
		$where .= empty($billsec) ? "" : " AND billsec >= '$billsec'";

		$where1 = " uniqueid is not null AND uniqueid != ''";
		$count = $history->where($where)->count();
		$count2 = $history->where($where1)->count();
		$qualitype_Y = $history->where("$where1 AND quality_enabled = 'Y'")->count();
		$percentage = floor(($qualitype_Y/$count2)*100);    //完成的质检量
		//dump($count2);die;
		$sales_task = new Model("sales_task");
		$task_pre = $sales_task->where("id = $task_id")->find();
		$qualitypercent = $task_pre["qualitypercent"];    //预定的质检量

		if( ($percentage >= $qualitypercent) && $count2 > 0){
			$complete_msg = "<span style='color:red;'>"."预定的质检量:  "."<span style='color:#0066CC;'>".$qualitypercent."%</span>"."  该任务已经完成！</span>";
		}elseif( $count2 > 0 ){
			$undone_msg = "预定的质检量："."<span style='color:#0066CC;'>".$qualitypercent."%</span> "."   已完成的质检量：".$percentage."%";
		}else{
			$noData = "该任务没有数据！";
		}

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $history->order("dealtime desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$sqlSearch = $history->getLastSql();
		//echo $history->getLastSql();die;

		$row = array('Y'=>"<span style='color:red'>已质检</sapn>",'N'=>'未质检');
		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$arrCallType = array("autoid"=>"自动外呼","preview"=>"预览式外呼");
		$level = array("pass"=>"通过","notpass"=>"不通过");
		$i = 0;
		foreach($arrData as &$val){
			$quality_enabled = $row[$val['quality_enabled']];
			$val['quality_enabled'] = $quality_enabled;

			$dealresult = $statusArr[$val['dealresult']];
			$val['dealresult'] = $dealresult;

			$calltype = $arrCallType[$val['calltype']];
			$val['calltype'] = $calltype;

			if($val["quality_level"]){
				$quality_level = $level[$val['quality_level']];
				$val['quality_level'] = $quality_level;
			}
			$val["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));

			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";

			if( !empty($val["uniqueid"]) ){
				//$val["uniqueid"] = trim($val["uniqueid"]);
				if(file_exists($WAVfile)){
					$arrData[$i]['operations'] .= "<a href='javascript:void(0);' onclick=\"openQuality("."'".trim($val["uniqueid"])."','".$val["id"]."','".$val["source_id"]."','".$val["dealuser"]."'".")\" > 质检 </a>";
				}
			}
			$i++;
		}
		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["sears"] = base64_encode($sqlSearch);
		if($startime){
			$arrT["startime"] = $startime;
		}
		if($endtime){
			$arrT["endtime"] = $endtime;
		}
		if($complete_msg){
			$arrT["complete_msg"] = $complete_msg;
		}
		if($undone_msg){
			$arrT["undone_msg"] = $undone_msg;
		}
		if($noData){
			$arrT["noData"] = $noData;
		}

		echo json_encode($arrT);
	}
	function exportHistoryTOCsv(){
		set_time_limit(0);
		ini_set('memory_limit','-1');

		$startime = $_REQUEST["startime"];
		$endtime = $_REQUEST["endtime"];

		$task_id = $_REQUEST["task_id"];

		$history = new Model("sales_contact_history_".$task_id);
		$createname = $_REQUEST["createname"];
		$auditworkno = $_REQUEST["auditworkno"];
		$phone = $_REQUEST["phone"];
		$billsec = $_REQUEST["billsec"];
		$dealresult = $_REQUEST["dealresult"];
		$quality_enabled = $_REQUEST["quality_enabled"];
		$quality_level = $_REQUEST["quality_level"];
		//dump($_REQUEST);
		$where = "1 ";
		$where .= " AND uniqueid is not null AND uniqueid != '' AND callstatus='ANSWERED'";
		$where .= empty($startime) ? "" : " AND dealtime >= '$startime'";
		$where .= empty($endtime) ? "" : " AND dealtime <= '$endtime'";
		$where .= empty($createname) ? "" : " AND dealuser = '$createname'";
		$where .= empty($auditworkno) ? "" : " AND audit_workno = '$auditworkno'";
		$where .= empty($phone) ? "" : " AND phone1 like '%$phone%'";
		//$where .= empty($dealresult) ? "" : " AND dealresult = '$dealresult'";
		if($dealresult == "0"){
			$where .= " AND dealresult = '0'";
		}else{
			$where .= empty($dealresult)?"":" AND dealresult = '$dealresult'";
		}
		$where .= empty($quality_enabled) ? "" : " AND quality_enabled = '$quality_enabled'";
		$where .= empty($quality_level) ? "" : " AND quality_level = '$quality_level'";
		$where .= empty($billsec) ? "" : " AND billsec >= '$billsec'";

		$where1 = " uniqueid is not null AND uniqueid != ''";
		$count = $history->where($where)->count();
		$count2 = $history->where($where1)->count();
		$qualitype_Y = $history->where("$where1 AND quality_enabled = 'Y'")->count();
		$percentage = floor(($qualitype_Y/$count2)*100);    //完成的质检量
		//dump($count2);die;
		$sales_task = new Model("sales_task");
		$task_pre = $sales_task->where("id = $task_id")->find();
		$qualitypercent = $task_pre["qualitypercent"];    //预定的质检量

		if( ($percentage >= $qualitypercent) && $count2 > 0){
			$complete_msg = "<span style='color:red;'>"."预定的质检量:  "."<span style='color:#0066CC;'>".$qualitypercent."%</span>"."  该任务已经完成！</span>";
		}elseif( $count2 > 0 ){
			$undone_msg = "预定的质检量："."<span style='color:#0066CC;'>".$qualitypercent."%</span> "."   已完成的质检量：".$percentage."%";
		}else{
			$noData = "该任务没有数据！";
		}

		$arrData = $history->order("dealtime desc")->where($where)->select();
		$sqlSearch = $history->getLastSql();
		//echo $history->getLastSql();die;

		$row = array('Y'=>"已质检",'N'=>'未质检');
		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$arrCallType = array("autoid"=>"自动外呼","preview"=>"预览式外呼");
		$level = array("pass"=>"通过","notpass"=>"不通过");
		$i = 0;
		foreach($arrData as &$val){
			$quality_enabled = $row[$val['quality_enabled']];
			$val['quality_enabled'] = $quality_enabled;

			$dealresult = $statusArr[$val['dealresult']];
			$val['dealresult'] = $dealresult;

			$calltype = $arrCallType[$val['calltype']];
			$val['calltype'] = $calltype;

			if($val["quality_level"]){
				$quality_level = $level[$val['quality_level']];
				$val['quality_level'] = $quality_level;
			}
			$val["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));

			if( !empty($val["uniqueid"]) ){
				//$val["uniqueid"] = trim($val["uniqueid"]);
				$arrData[$i]['operations'] .= "<a href='javascript:void(0);' onclick=\"openQuality("."'".trim($val["uniqueid"])."','".$val["id"]."','".$val["source_id"]."','".$val["dealuser"]."'".")\" > 质检 </a>";
			}
			$i++;
		}
		$content = "创建时间,坐席工号,呼叫类型,质检号码,客户状态,通话时长,备注,是否质检,质检等级,评分内容,质检人工号\r\n";
		foreach($arrData as $val){
			$rows = $val['dealtime'].",".
					$val['dealuser'].",".
					$val['calltype'].",".
					$val['phone1'].",".
					$val['dealresult'].",".
					$val['billsec'].",".
					$val['description'].",".
					$val['quality_enabled'].",".
					$val['quality_level'].",".
					$val['quality_content'].",".
					$val['audit_workno']."\r\n";
			$content .= $rows;
		}
		$content = iconv("utf-8","gb2312",$content);
		//dump($content);die;
		$name = "通话记录".date("Ymd");
		$filename = iconv("utf-8","gb2312",$name);
		$this->export_csv($filename,$content);
	}


	function export_csv($filename,$data){
		$content = iconv("utf-8","gb2312",$data);
		$d = date("D M j G:i:s T Y");
        header('HTTP/1.1 200 OK');
        header('Date: ' . $d);
        header('Last-Modified: ' . $d);
        header("Content-Type: application/force-download");
        header("Content-Length: " . strlen($data));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".$filename.".csv");
        echo $data;
	}

	/*
	function qualityAuditData(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$endtime = Date('Y-m-d',$_GET['ts_end']) ." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){//上个月的起始时间
				$day = Date('d',$_GET['ts_end']);
				$startime = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1)) ." 00:00:00";
			}else{
				$startime = Date('Y-m-d',$_GET['ts_start']) ." 00:00:00";
			}
		}else{
			//$date_start = $_GET['date_start'];
			//$date_end = $_GET['date_end'];
			$startime = $_REQUEST["startime"];
			$endtime = $_REQUEST["endtime"];
		}

		$task_id = $_REQUEST["task_id"];

		$table = "sales_contact_history_".$task_id;
		$table2 = "sales_cdr_".$task_id;
		$history = new Model("sales_contact_history_".$task_id);
		$createname = $_REQUEST["createname"];
		$auditworkno = $_REQUEST["auditworkno"];
		$phone = $_REQUEST["phone"];
		$billsec = $_REQUEST["billsec"];
		$dealresult = $_REQUEST["dealresult"];
		$quality_enabled = $_REQUEST["quality_enabled"];
		//dump($_REQUEST);
		$where = "1 ";
		$where .= " AND h.uniqueid is not null AND h.uniqueid != '' and c.disposition='ANSWERED'";
		$where .= empty($startime) ? "" : " AND h.dealtime >= '$startime'";
		$where .= empty($endtime) ? "" : " AND h.dealtime <= '$endtime'";
		$where .= empty($createname) ? "" : " AND h.dealuser = '$createname'";
		$where .= empty($auditworkno) ? "" : " AND h.audit_workno = '$auditworkno'";
		$where .= empty($phone) ? "" : " AND h.phone1 like '%$phone%'";
		//$where .= empty($dealresult) ? "" : " AND h.dealresult = '$dealresult'";
		if($dealresult == "0"){
			$where .= " AND h.dealresult = '0'";
		}else{
			$where .= empty($dealresult)?"":" AND h.dealresult = '$dealresult'";
		}
		$where .= empty($quality_enabled) ? "" : " AND h.quality_enabled = '$quality_enabled'";
		$where .= empty($billsec) ? "" : " AND c.billsec >= '$billsec'";

		$where1 = " h.uniqueid is not null AND h.uniqueid != '' and c.disposition='ANSWERED'";
		$count = $history->table("$table h")->field("h.dealtime,h.dealuser,h.calltype,h.phone1,h.dealresult,h.description,h.quality_enabled,h.quality_content,h.audit_workno,h.source_id,h.id,h.uniqueid,c.billsec,c.disposition")->join("$table2 c on (h.uniqueid = c.uniqueid) ")->where($where)->count();
		$count2 = $history->table("$table h")->field("h.dealtime,h.dealuser,h.calltype,h.phone1,h.dealresult,h.description,h.quality_enabled,h.quality_content,h.audit_workno,h.source_id,h.id,h.uniqueid,c.billsec,c.disposition")->join("$table2 c on (h.uniqueid = c.uniqueid) ")->where($where1)->count();
		$qualitype_Y = $history->table("$table h")->field("h.dealtime,h.dealuser,h.calltype,h.phone1,h.dealresult,h.description,h.quality_enabled,h.quality_content,h.audit_workno,h.source_id,h.id,h.uniqueid,c.billsec,c.disposition")->join("$table2 c on (h.uniqueid = c.uniqueid) ")->where("$where1 AND h.quality_enabled = 'Y'")->count();
		$percentage = floor(($qualitype_Y/$count2)*100);    //完成的质检量
		//dump($count2);die;
		$sales_task = new Model("sales_task");
		$task_pre = $sales_task->where("id = $task_id")->find();
		$qualitypercent = $task_pre["qualitypercent"];    //预定的质检量

		if( ($percentage >= $qualitypercent) && $count2 > 0){
			$complete_msg = "<span style='color:red;'>"."预定的质检量:  "."<span style='color:#0066CC;'>".$qualitypercent."%</span>"."  该任务已经完成！</span>";
		}elseif( $count2 > 0 ){
			$undone_msg = "预定的质检量："."<span style='color:#0066CC;'>".$qualitypercent."%</span> "."   已完成的质检量：".$percentage."%";
		}else{
			$noData = "该任务没有数据！";
		}

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $history->order("h.dealtime desc")->table("$table h")->field("h.dealtime,h.dealuser,h.calltype,h.phone1,h.dealresult,h.description,h.quality_enabled,h.quality_content,h.audit_workno,h.source_id,h.id,h.uniqueid,c.billsec,c.disposition")->join("$table2 c on (h.uniqueid = c.uniqueid) ")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$sqlSearch = $history->getLastSql();
		//echo $history->getLastSql();die;

		$row = array('Y'=>"<span style='color:red'>已质检</sapn>",'N'=>'未质检');
		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$arrCallType = array("autoid"=>"自动外呼","preview"=>"预览式外呼");
		$i = 0;
		foreach($arrData as &$val){
			$quality_enabled = $row[$val['quality_enabled']];
			$val['quality_enabled'] = $quality_enabled;

			$dealresult = $statusArr[$val['dealresult']];
			$val['dealresult'] = $dealresult;

			$calltype = $arrCallType[$val['calltype']];
			$val['calltype'] = $calltype;

			$val["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));

			if( !empty($val["uniqueid"]) ){
				//$val["uniqueid"] = trim($val["uniqueid"]);
				$arrData[$i]['operations'] .= "<a href='javascript:void(0);' onclick=\"openQuality("."'".trim($val["uniqueid"])."','".$val["id"]."','".$val["source_id"]."','".$val["dealuser"]."'".")\" > 质检 </a>";
			}
			$i++;
		}
		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["sears"] = base64_encode($sqlSearch);
		if($startime){
			$arrT["startime"] = $startime;
		}
		if($endtime){
			$arrT["endtime"] = $endtime;
		}
		if($complete_msg){
			$arrT["complete_msg"] = $complete_msg;
		}
		if($undone_msg){
			$arrT["undone_msg"] = $undone_msg;
		}
		if($noData){
			$arrT["noData"] = $noData;
		}

		echo json_encode($arrT);
	}
	*/

	function playQualityAudit(){
		checkLogin();
		$uniqueid = $_GET['uniqueid'];
		header('Content-Type: text/html; charset=utf-8');
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$AudioURL = "index.php?m=CDR&a=downloadCDR&uniqueid=${uniqueid}";
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);


		$sears = base64_decode($_GET["sears"]);
		$allSql = array_shift(explode("LIMIT",$sears));
		$searsql = base64_encode($allSql);
		$this->assign("sears",$searsql);

		$extension = $_SESSION["user_info"]["extension"];
		$task_id = $_REQUEST['task_id'];
		$id = $_REQUEST['id'];   //历史记录的id
		$source_id = $_REQUEST['source_id'];   //客户资料的id
		$dealuser = $_REQUEST['dealuser'];

		$source = new Model("sales_source_".$task_id);

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');
		$para_sys = readS();

		foreach($fielddTpl2 as $val){
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields" && $val["en_name"] != "sms_cust"){
					$fielddTpl[] = $val;
				}
			}else{
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
					$fielddTpl[] = $val;
				}
			}
		}
		$fields = "`".implode("`,`",$arrF)."`,`dealresult_id`";
		$this->assign("fielddTpl",$fielddTpl);

		$sourceData =  $source->field($fields)->where("id = '$source_id'")->find();
		//echo $source->getLastSql();die;
		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				$sourceData[$val['en_name']] = $row[$val['en_name']][$sourceData[$val['en_name']]];
			}
		}
		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$sourceData["dealresult_id"] = $statusArr[$sourceData["dealresult_id"]];
		/*
		$row2 = array("Y"=>"是","N"=>"否");
		if($sourceData["sms_cust"]){
			$sourceData["sms_cust"] = $row2[$sourceData["sms_cust"]];
		}
		*/
		//dump($sourceData);die;
		$this->assign("task_id",$task_id);
		$this->assign("exten",$extension);
		$this->assign("id",$id);
		$this->assign("source_id",$source_id);
		$this->assign("dealuser",$dealuser);
		$this->assign("uniqueid",$uniqueid);
		$this->assign("custList",$sourceData);

		$this->display();
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	function insertQualityAudit(){
		$username = $_SESSION["user_info"]["username"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$task_id = $_REQUEST['task_id'];
		$id = $_REQUEST['id'];   //历史记录的id
		$source_id = $_REQUEST['source_id'];   //客户资料的id
		$time = date('Y-m-d H:i:s',strtotime("30 minutes"));
		//dump($time);die;
		$history = new Model("sales_contact_history_".$task_id);
		$arrData = array(
			"audit_workno" => $username,
			"audit_time" => date('Y-m-d H:i:s'),
			"quality_enabled" => "Y",
			"quality_content" => $_REQUEST['quality_content'],
			"quality_level" => $_REQUEST['quality_level'],
		);
		//dump($_REQUEST);die;
		$result = $history->data($arrData)->where("id = '$id'")->save();

		if ($result !== false){
			$source = new Model("sales_source_".$task_id);
			$arrTmp = $source->field("id,phone1,callresult_id,dealresult_id")->where("id = '$source_id'")->find();
			$res = $source->where("id = '$source_id'")->save( array("dealresult_id" => $_REQUEST['dealresult']) );

			$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
			if($arrTmp['dealresult_id'] != $_REQUEST['dealresult'] && $arrTmp['dealresult_id'] != "0"){
				$re = new Model("task_result_status_".$task_id);
				$arrTs = array(
					"createtime"=>date("Y-m-d H:i:s"),
					"username"=>$username,
					"customer_id"=>$source_id,
					"phone"=>$arrTmp["phone1"],
					"old_status"=>$statusArr[$arrTmp['dealresult_id']],
					"new_status"=>$statusArr[$_REQUEST['dealresult']],
				);
				$rst = $re->data($arrTs)->add();
			}

			$sales_task = new Model("sales_task");
			$arrData = $sales_task->field("id,name")->select();
			foreach($arrData as $val){
				$tmp[$val["id"]] = $val["name"];
			}

			$url = "agent.php?m=Telemarketing&a=editWCustomer&calltype=preview&visitid=Y&task_id=".$task_id."&id=".$source_id;
			$para_sys = readS();
			if($para_sys["windowOpen_hide"] == "Y"){
				$arrTmp['phone_hide'] = substr($arrTmp["phone1"],0,3)."***".substr($arrTmp["phone1"],-4);
			}else{
				$arrTmp['phone_hide'] = $arrTmp["phone1"];
			}

			if($arrTmp['dealresult_id'] != $_REQUEST['dealresult']){
				$waitcontent = $username."/".$cn_name." 将任务名称：".$tmp[$task_id]." 号码为：".$arrTmp["phone_hide"]." 的客户状态由 ".$statusArr[$arrTmp['dealresult_id']]." 转成 ".$statusArr[$_REQUEST['dealresult']]."。 转变原因：".$_REQUEST["quality_content"]."<a href='".$url."' target='_blank' >  链接</a>";

				$title = "质检审核通知， 任务名称： ".$tmp[$task_id]." 号码：".$arrTmp["phone_hide"]."被审核了";
				$waitmatter = new Model("waitmatter");
				$arrWaitData = array(
					"createtime"=>date("Y-m-d H:i:s"),
					"title"=>$title,
					//"remindertime"=>$_REQUEST["reminder_time"],
					"remindertime"=>$time,
					"content"=>$waitcontent,
					"status"=>"N",
					"matterstype"=>"3",
					"username"=>$_REQUEST["dealuser"],
					"url"=>$url,
				);
				$waitResult = $waitmatter->data($arrWaitData)->add();
			}
			if($_REQUEST['dealresult'] == '1'){
				$visit_customer = new Model("visit_customer");
				$arrV = array(
					//"visit_time"=>$_REQUEST['visittime'],
					"visit_time"=>$time,
					"visit_status"=>"N",
					"visit_type"=>"out",
					"visit_name"=>$_REQUEST["dealuser"],
					"task_id"=>$task_id,
					"customer_id"=>$source_id,
					"phone"=>$arrTmp["phone1"],
				);
				$resV = $visit_customer->data($arrV)->add();
			}

			echo json_encode(array('success'=>true,'msg'=>"保存成功！"));
		} else {
			echo json_encode(array('msg'=>'保存失败！'));
		}
	}


	function insertQualityAudit2(){
		$username = $_SESSION["user_info"]["username"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$task_id = $_REQUEST['task_id'];
		$id = $_REQUEST['id'];   //历史记录的id
		$source_id = $_REQUEST['source_id'];   //客户资料的id

		$history = new Model("sales_contact_history_".$task_id);
		$arrData = array(
			"audit_workno" => $username,
			"audit_time" => date('Y-m-d H:i:s'),
			"quality_enabled" => "Y",
			"quality_content" => $_REQUEST['quality_content'],
			"quality_level" => "pass",
		);
		//dump($arrData);die;
		$result = $history->data($arrData)->where("id = '$id'")->save();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'保存成功！'));
		} else {
			echo json_encode(array('msg'=>'保存失败！'));
		}
	}

	function nextHistoryRecord(){
		$task_id = $_REQUEST["task_id"];
		$history = new Model("sales_contact_history_".$task_id);
		$sears = base64_decode($_GET["sears"]);
		//dump($sears);die;
		$id = $_GET["id"];
		$cmData = $history->query($sears);

		foreach($cmData as $val){
			$cmId[] = $val["id"];
			$arrUid[$val["id"]] = array("id"=>$val["id"],"uniqueid"=>$val["uniqueid"],"dealuser"=>$val["dealuser"],"source_id"=>$val["source_id"]);
		}

		$srId = array_flip($cmId);
		//dump($arrUid);die;
		$afterId = $cmId[$srId[$id]+1];
		$lastUid = $arrUid[$afterId]["uniqueid"];
		echo json_encode(array('id'=>$afterId,'lastUid'=>$lastUid,'dealuser'=>$arrUid[$afterId]["dealuser"],'source_id'=>$arrUid[$afterId]["source_id"]));
	}

	function lastHistoryRecord(){
		$task_id = $_REQUEST["task_id"];
		$history = new Model("sales_contact_history_".$task_id);
		$sears = base64_decode($_REQUEST["sears"]);
		$id = $_GET["id"];
		$cmData = $history->query($sears);
		foreach($cmData as $val){
			$cmId[] = $val["id"];
			$arrUid[$val["id"]] = array("id"=>$val["id"],"uniqueid"=>$val["uniqueid"],"dealuser"=>$val["dealuser"],"source_id"=>$val["source_id"]);
		}

		$srId = array_flip($cmId);
		$afterId = $cmId[$srId[$id]-1];
		$lastUid = $arrUid[$afterId]["uniqueid"];

		echo json_encode(array('id'=>$afterId,'lastUid'=>$lastUid,'dealuser'=>$arrUid[$afterId]["dealuser"],'source_id'=>$arrUid[$afterId]["source_id"]));
	}



	function tyr(){
		$sales_task = M('sales_task');
		$arrTaskId = $sales_task->select();
		$mod = M();
		foreach($arrTaskId as $vm){
			$sql = "ALTER TABLE `sales_cdr_".$vm['id']."` ADD INDEX newindex (src,dst,workno,disposition,uniqueid,username)";
			$addC2 = $mod->execute($sql);

			$sql2 = "ALTER TABLE `sales_contact_history_".$vm['id']."` ADD INDEX newindex (dealuser,source_id,uniqueid,callresult,quality_level,callstatus)";
			$addC3 = $mod->execute($sql2);

			$sql4 = "ALTER TABLE `sales_source_".$vm['id']."` ADD INDEX newindex (dealuser,calledflag,callresult_id,dealresult_id,locked)";
			$addC4 = $mod->execute($sql4);
		}
	}
}

?>
