<?php
class RateAction extends Action{
    function listRateParam(){
		checkLogin();
		$trunks = new Model('asterisk.Trunks');
        $list = $trunks->field('trunkid,name')->select();
        $this->assign('trlist',$list);

		//分配增删改的权限
		$menuname = "Rate";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		//dump($_SESSION["user_priv"]);die;

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

        $this->display();
    }
	function rateData(){
        $rate=new RateModel();
        $count = $rate->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
        $resultrate = $rate->order('create_time desc')->select();

		$row = array("pbx"=>"pbx通话","outbound"=>"外呼通话");
		foreach($resultrate as &$val){
			$val["call_type2"] = $val["call_type"];
			$call_type = $row[$val["call_type2"]];
			$val["call_type2"] = $call_type;
		}

		$rowsList = count($resultrate) ? $resultrate : false;
		$arrRate["total"] = $count;
		$arrRate["rows"] = $rowsList;

		echo json_encode($arrRate);
	}

    function insertRate(){
		$username = $_SESSION['user_info']['username'];
        $rate=new RateModel();
		$call_type = empty($_POST['call_type']) ? "pbx" : $_POST['call_type'];
		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'rate_name'=>$_POST['rate_name'],
			'call_type'=>$call_type,
			'rate_prefix'=>$_POST['rate_prefix'],
			'rate_len'=>$_POST['rate_len'],
			'rate_trunk'=>$_POST['rate_trunk'],
			'rate_cycle'=>$_POST['rate_cycle'],
			'rate_money'=>$_POST['rate_money'],
			'create_user'=>$username,
			'rate_type'=>"1",   //费率类型--1：运营商，2：代理商，3：用户
		);
		$result = $rate->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'费率添加成功！'));
		} else {
			echo json_encode(array('msg'=>'费率添加失败！'));
		}
    }

    function updateRate(){
        $id = $_REQUEST['id'];
		$rate=new RateModel();
		$arrData = Array(
			'rate_name'=>$_POST['rate_name'],
			'rate_prefix'=>$_POST['rate_prefix'],
			'rate_len'=>$_POST['rate_len'],
			'rate_trunk'=>$_POST['rate_trunk'],
			'rate_cycle'=>$_POST['rate_cycle'],
			'rate_money'=>$_POST['rate_money'],
			'call_type'=>$_POST['call_type'],
		);
		$result = $rate->data($arrData)->where("rate_id = $id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

    }

    function deleteRate(){
		$id = $_REQUEST["id"];
		$rate = new RateModel();
		$result = $rate->where("rate_id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
    }







}

