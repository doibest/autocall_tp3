<?php
class ShopTypeAction extends Action{
	function shopTypeList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Shop Type";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}


	function shopTypeData(){
		$shop_type = new Model("shop_type");
		import('ORG.Util.Page');
		$count = $shop_type->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$goodTypeData = $shop_type->order("cat_order asc")->limit($page->firstRow.','.$page->listRows)->select();

		$i = 0;
		foreach($goodTypeData as $val){
			//$goodTypeData[$i]['operating'] = "<a href='javascript:void(0);' onclick=\"openAttribute("."'".$val["id"]."'".")\">属性列表</a>";
			$goodTypeData[$i]['operating'] = "<a  target='_self' href='index.php?m=ShopAttribute&a=attributeList&id=".$val["id"]."'>属性列表</a>";
			$i++;
		}
		unset($i);
		//dump($goodTypeData);die;
		$rowsList = count($goodTypeData) ? $goodTypeData : false;
		$arrType["total"] = $count;
		$arrType["rows"] = $rowsList;

		echo json_encode($arrType);
	}

	function insertShopType(){
		$shop_type = new Model("shop_type");
		$arrData = array(
			"caty_name"=>$_REQUEST["caty_name"],
			"cat_order"=>$_REQUEST["cat_order"],
			"caty_enabled"=>$_REQUEST["caty_enabled"],
		);
		$result = $shop_type->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'商品类型添加成功！'));
		} else {
			echo json_encode(array('msg'=>'商品类型添加失败！'));
		}
	}

	function updateShopType(){
		$id = $_REQUEST["id"];
		$shop_type = new Model("shop_type");
		$arrData = array(
			"caty_name"=>$_REQUEST["caty_name"],
			"cat_order"=>$_REQUEST["cat_order"],
			"caty_enabled"=>$_REQUEST["caty_enabled"],
		);
		$result = $shop_type->data($arrData)->where("id = $id")->save();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteShopType(){
		$id = $_REQUEST["id"];
		$shop_type = new Model("shop_type");
		$result = $shop_type->where("id = $id")->delete();

		$shop_attribute = new Model("shop_attribute");
		$res = $shop_attribute->where("type_id = $id")->delete();

		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

}

?>
