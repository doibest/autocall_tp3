<?php
class SMSQueryAction extends Action{
	function smsList(){
		checkLogin();
		$this->display();
	}

	function sendSmsList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "SMS Query";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function sendSmsData(){
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$createname = $_REQUEST['createname'];
		$phone = $_REQUEST['phone'];
		$sendcontent = $_REQUEST['sendcontent'];

		$where = "1 ";
		$where .= " AND send_timing = 'N' OR (send_results = 'success' AND  send_timing = 'Y')";
		$where .= empty($startime)?"":" AND createtime > '$startime'";
		$where .= empty($endtime)?"":" AND createtime < '$endtime'";
		$where .= empty($createname)?"":" AND createname like '%$createname%'";
		$where .= empty($phone)?"":" AND phone like '%$phone%'";
		$where .= empty($sendcontent)?"":" AND sendcontent like '%$sendcontent%'";

		$username = $_SESSION['user_info']['username'];
		$sendsms = new Model("sendsms");
		if($username == "admin"){
			$count = $sendsms->where($where)->count();
		}else{
			$count = $sendsms->where("createname = '$username' AND $where")->count();
		}

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		if($username == "admin"){
			$smsData = $sendsms->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}else{
			$smsData = $sendsms->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where("createname = '$username' AND $where")->select();
		}
		//echo $sendsms->getLastSql();die;

		$rowsList = count($smsData) ? $smsData : false;
		$arrSms["total"] = $count;
		$arrSms["rows"] = $rowsList;

		echo json_encode($arrSms);
	}

	function unSendSmsList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "SMS Query";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function unSendSmsData(){
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$createname = $_REQUEST['createname'];
		$phone = $_REQUEST['phone'];
		$sendcontent = $_REQUEST['sendcontent'];

		$where = "1 ";
		$where .= " AND send_timing = 'Y' AND send_results = 'timing' ";
		$where .= empty($startime)?"":" AND sendtime > '$startime'";
		$where .= empty($endtime)?"":" AND sendtime < '$endtime'";
		$where .= empty($createname)?"":" AND createname like '%$createname%'";
		$where .= empty($phone)?"":" AND phone like '%$phone%'";
		$where .= empty($sendcontent)?"":" AND sendcontent like '%$sendcontent%'";

		$username = $_SESSION['user_info']['username'];
		$sendsms = new Model("sendsms");
		if($username == "admin"){
			$count = $sendsms->where($where)->count();
		}else{
			$count = $sendsms->where("$where AND createname = '$username'")->count();
		}

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		if($username == "admin"){
			$smsData = $sendsms->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}else{
			$smsData = $sendsms->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where("$where AND createname = '$username'")->select();
		}
		$rowsList = count($smsData) ? $smsData : false;
		$arrSms["total"] = $count;
		$arrSms["rows"] = $rowsList;

		echo json_encode($arrSms);
	}

	function deleteSms(){
		$id = $_REQUEST["id"];
		$sendsms = new Model("sendsms");
		$result = $sendsms->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function searchCostList(){
		checkLogin();
		$this->display();
	}
}
?>
