<?php
class FileTypeSettingAction extends Action{
	function fileList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "File Type Setting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		//dump($priv);
		$this->display();
	}

	function fileDataList(){
		$file = new Model("filetype");
		import('ORG.Util.Page');
		$count = $file->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$fileData = $file->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($fileData) ? $fileData : false;
		$arrfile["total"] = $count;
		$arrfile["rows"] = $rowsList;

		echo json_encode($arrfile);
	}

	function insertfile(){
		$file = new Model("filetype");
		$arrData = array(
			"filename"=>$_REQUEST["filename"],
		);
		$result = $file->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function updatefile(){
		$id = $_REQUEST["id"];
		$file = new Model("filetype");
		$arrData = array(
			"filename"=>$_REQUEST["filename"],
		);
		$result = $file->data($arrData)->where("id = $id")->save();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function deletefile(){
		$id = $_REQUEST["id"];
		$file = new Model("filetype");
		$result = $file->where("id = $id")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}
}

?>
