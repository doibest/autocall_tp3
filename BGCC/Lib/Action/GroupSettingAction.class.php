<?php
class GroupSettingAction extends Action{
	function groupList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Group Type Setting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function groupDataList(){
		$group = new Model("grouptype");
		import('ORG.Util.Page');
		$count = $group->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$groupData = $group->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($groupData) ? $groupData : false;
		$arrgroup["total"] = $count;
		$arrgroup["rows"] = $rowsList;

		echo json_encode($arrgroup);
	}

	function insertgroup(){
		$group = new Model("grouptype");
		$arrData = array(
			"groupname"=>$_REQUEST["groupname"],
		);
		$result = $group->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function updategroup(){
		$id = $_REQUEST["id"];
		$group = new Model("grouptype");
		$arrData = array(
			"groupname"=>$_REQUEST["groupname"],
		);
		$result = $group->data($arrData)->where("id = $id")->save();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function deletegroup(){
		$id = $_REQUEST["id"];
		$group = new Model("grouptype");
		$result = $group->where("id = $id")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}
}

?>
