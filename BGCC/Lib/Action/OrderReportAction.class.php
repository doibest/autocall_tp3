<?php
class OrderReportAction extends Action{
	function orderReportList(){
		checkLogin();

		//分配增删改的权限
		$menuname = "Order Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	//订单报表数据
	function orderReportData(){
		$search_type = $_REQUEST["search_type"];
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_end = Date('Y-m-d',$_GET['ts_end']) ." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){//上个月的起始时间
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1)) ." 00:00:00";
			}else{
				$date_start = Date('Y-m-d',$_GET['ts_start']) ." 00:00:00";
			}
		}else{
			$date_start = $_GET['date_start']." 00:00:00";
			$date_end = $_GET['date_end']." 23:59:59";
		}
		if(! $date_end){$date_end = Date('Y-m-d H:i:s');}

		$where = "1 ";
        $where .= empty($date_start)?"":" AND createtime >'$date_start'";
        $where .= empty($date_end)?"":" AND createtime <'$date_end'";

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		//$deptUser = "'".implode("','",$userArr["deptIdUser"][$d_id])."'";
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";
		//echo $deptUser;die;

		if($username != "admin"){
			$where .= " AND createname in ($deptUser)";
		}else{
			$where .= " ";
		}


		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$order_info = new Model("order_info");
		$count = $order_info->query("SELECT COUNT(DISTINCT createname) as count FROM order_info where $where");
		//echo $order_info->getLastSql();die;
		if($search_type == "xls"){
			$orData = $order_info->field("createname,COUNT(*) AS num,sum(cope_money) as cope_money,sum(goods_total) as goods_total,createtime")->group("createname")->where($where)->select();
		}else{
			$orData = $order_info->field("createname,COUNT(*) AS num,sum(cope_money) as cope_money,sum(goods_total) as goods_total,createtime")->group("createname")->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		}
		//echo $order_info->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$i = 0;
		foreach($orData as $val){
			$orData[$i]['cn_name'] = $cnName[$val["createname"]];
			$i++;
		}

		if($search_type == "xls"){
			$arrField = array('createname','cn_name','num','goods_total','cope_money');
			$arrTitle = array('工号','姓名','订单数','商品数','总销售额');
			$xls_count = count($arrField);
			$excelTiele = "订单报表".date("Y-m-d");
			exportDataFunction($xls_count,$arrField,$arrTitle,$orData,$excelTiele);
			die;
		}
		//dump($orData);die;

		$rowsList = count($orData) ? $orData : false;
		$arrOrder["total"] = $count[0]['count'];
		$arrOrder["rows"] = $rowsList;
		if($date_start){
			$arrOrder["date_start"] = $date_start;
		}
		if($date_end){
			$arrOrder["date_end"] = $date_end;
		}
		echo json_encode($arrOrder);

	}


	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }

	function test111(){
		$order_goods = new Model("order_goods");
		$time = date("Y-m-d H:i:s");
		$arrData = $order_goods->field("createname,createtime,sum(goods_num) AS num,sum(goods_money) as cope_money")->where("createtime >='2014-12-02 00:00:00' and createtime <='$time'")->group("createname")->select();
		echo $order_goods->getLastSql();
		dump($arrData);die;

	}
}

?>
