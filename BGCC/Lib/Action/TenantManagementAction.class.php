<?php
/*
租户管理
*/
class TenantManagementAction extends Action{
	function tenantList(){
		//header("Content-Type:text/html; charset=utf-8");
		checkLogin();
		//分配增删改的权限
		$menuname = "Tenant List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$this->display();
	}

	function tenantData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$mod = M("tenants");
		$count = $mod->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->limit($page->firstRow.','.$page->listRows)->select();

		//租户状态: 租用中、未开始、过期
		$nowdate = date('Y-m-d');
		foreach($arrData as &$val){
			if($val['start_lease_time'] > $nowdate){
				$val['rent_state'] = '未开始';
			}elseif($val['start_lease_time'] <= $nowdate && $val['end_lease_time'] >= $nowdate){
				$val['rent_state'] = '使用中';
			}elseif($val['end_lease_time'] < $nowdate){
				$val['rent_state'] = '已到期';
			}

		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addTenants(){
		checkLogin();
		$this->display();
	}

	function insertTenants(){
		$username = $_SESSION['user_info']['username'];
		$tenants = M("tenants");
		$count = $tenants->where("enterprise_number='". $_REQUEST["enterprise_number"] ."'")->count();
		if( $count>0 ){
			echo json_encode(array('msg'=>"该租户管理员帐号已存在，请换个租户管理员帐号！"));
			exit;
		}

		$arrData = array(
			'create_user'=>$username,
			'create_time'=>date("Y-m-d H:i:s"),
			'enterprise_number'=>$_REQUEST['enterprise_number'],
			'password'=>$_REQUEST['password'],
			'company_name'=>$_REQUEST['company_name'],
			'management_leader'=>$_REQUEST['management_leader'],
			'management_leader_phone'=>$_REQUEST['management_leader_phone'],
			'management_leader_email'=>$_REQUEST['management_leader_email'],
			'management_leader_qq'=>$_REQUEST['management_leader_qq'],
			'company_contact'=>$_REQUEST['company_contact'],
			'contact_phone'=>$_REQUEST['contact_phone'],
			'contact_email'=>$_REQUEST['contact_email'],
			'contact_qq'=>$_REQUEST['contact_qq'],
			'start_lease_time'=>$_REQUEST['start_lease_time'],
			'end_lease_time'=>$_REQUEST['end_lease_time'],
			'start_work_number'=>$_REQUEST['start_work_number'],
			'end_work_number'=>$_REQUEST['end_work_number'],
			'start_extension'=>$_REQUEST['start_extension'],
			'end_extension'=>$_REQUEST['end_extension'],
			'role_id'=>$_REQUEST['role_id'],
			'max_concurrent'=>$_REQUEST['max_concurrent'],
			'description'=>$_REQUEST['description'],
		);
		$result = $tenants->data($arrData)->add();
		if ($result){
			$baseName = "bgcrm".$result;
			$cmd = "sudo /bin/cp -rp bgcrm_backup/ $baseName";
			chdir("/var/lib/mysql/");
			exec($cmd,$retstr1,$retno1);
			chdir("/var/www/html/");
			if( $retno1 == 0 ){
				$mod = M();
				$password = $_REQUEST["password"];
				$enterprise_number = $_REQUEST["enterprise_number"];
				$role_id = $_REQUEST["role_id"];

				$sql_up2 = "update $baseName.users set password='$password',enterprise_number='$enterprise_number',username='$enterprise_number' where username='admin'";
				$mod->execute($sql_up2);

				//把角色表的管理员权限也给它更新一次
				$tenants_role = M("tenants_role");
				$arrRole = $tenants_role->where("r_id = '$role_id'")->find();
				$action_list = $arrRole["action_list"];
				$sql_up_role = "update $baseName.role set action_list='$action_list' where r_id=1";
				$mod->execute($sql_up_role);

				$cmd = "cp -rp /var/www/html/BGCC/Conf/crm/bgcrm_backup  /var/www/html/BGCC/Conf/crm/$baseName;   chmod 777 /var/www/html/BGCC/Conf/crm/$baseName ";
				exec($cmd,$retstr,$retno);

				$this->tenantOperation($_REQUEST['start_work_number'],$_REQUEST['start_extension'],$_REQUEST['end_extension'],$enterprise_number,$baseName);
			}
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function tenantOperation($start_work_number,$start_extension,$end_extension,$enterprise_number,$baseName){
		$users = M("$baseName.users");
		$range = $end_extension - $start_extension;
		$suffix = $this->random(6,1);
		$extension_type = "sip";
		$successNum = 0;
		for($i=0;$i<=$range;$i++){
			//添加用户表
			$exten = (string)($start_extension+$i);
			$work_num = (string)($start_work_number+$i);
			$extenPwd = $exten.$suffix;
			$arrU = Array(
				'enterprise_number'=>$enterprise_number,
				'username'=>$work_num,
				'password'	=>$exten.'147',
				'extension'=>$exten,
				'extenPass'=>$extenPwd,
				'd_id'	=> "1",
				'r_id'	=> "3",
				'extension_type'=>$extension_type,
			);
			//dump($arrU);
			$res = $users->add($arrU);
			//添加分机:默认分机名=分机号=分机密码
			$this->createExtension($exten,$exten,$extenPwd,$extension_type);
		}
	}

	function random($length = 6 , $numeric = 0) {
		PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
		if($numeric) {
			$hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
		} else {
			$hash = '';
			$chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
			$max = strlen($chars) - 1;
			for($i = 0; $i < $length; $i++) {
				$hash .= $chars[mt_rand(0, $max)];
			}
		}
		return $hash;
	}

	//添加单个分机
	function createExtension($Name,$Ext,$Secret,$Tech){

		import('ORG.Pbx.extensionsBatch');
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');

		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAMP  = $pConfig->leer_configuracion(false);
		$arrAST  = $pConfig->leer_configuracion(false);

		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";

		$pDB = new paloDB($dsnAsterisk);
		$pLoadExtension = new paloSantoLoadExtension($pDB);

		$Context        = "from-internal";

//////////////////////////////////////////////////////////////////////////////////
		// validando para que coja las comillas
		$Outbound_CID = ereg_replace('“', "\"", $Outbound_CID);
		$Outbound_CID = ereg_replace('”', "\"", $Outbound_CID);
//////////////////////////////////////////////////////////////////////////////////
		//Paso 1: creando en la tabla sip

		if(!$pLoadExtension->createTechDevices($Ext,$Secret,$VoiceMail,$Context,$Tech))
		{
			$Messages .= $pLoadExtension->errMsg."<br />";
		}else{

			//Paso 2: creando en la tabla users
			if(!$pLoadExtension->createUsers($Ext,$Name,$VoiceMail,$Direct_DID,$Outbound_CID))
				$Messages .= $pLoadExtension->errMsg."<br />";

			//Paso 3: creando en la tabla devices
			if(!$pLoadExtension->createDevices($Ext,$Tech,$Name))
				$Messages .= $pLoadExtension->errMsg."<br />";

			//Paso 4: creando en el archivo /etc/asterisk/voicemail.conf los voicemails
			if(!$pLoadExtension->writeFileVoiceMail(
				$Ext,$Name,$VoiceMail,$VoiceMail_PW,$VM_Email_Address,
				$VM_Pager_Email_Addr,$VM_Options,$VM_EmailAttachment,$VM_Play_CID,
				$VM_Play_Envelope, $VM_Delete_Vmail)
			  )
				$Messages .= L("Failed to update the voicemail");

			//Paso 5: Configurando el call waiting
			if(!$pLoadExtension->processCallWaiting($Call_Waiting,$Ext))
				$Messages .= L("Failed to update the callwaiting");

			$outboundcid = ereg_replace("\"", "'", $Outbound_CID);
			$outboundcid = ereg_replace("\"", "'", $outboundcid);
			$outboundcid = ereg_replace(" ", "", $outboundcid);
			$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());
			if(!$pLoadExtension->putDataBaseFamily($data_connection, $Ext, $Tech, $Name, $VoiceMail, $outboundcid)){
				$Messages .= $Messages .= L("Failed to update the database of PBX");
			}
			$cont++;
		}
		////////////////////////////////////////////////////////////////////////
		//Paso 7: Escribiendo en tabla incoming
		/*
		if($Direct_DID !== ""){
			if(!$pLoadExtension->createDirect_DID($Ext,$Direct_DID))
			$Messages .= "分机: $Ext - 更新did失败<br />";
		}
				/////////////////////////////////////////////////////////////////////////
		/*
		if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
			$Messages .= $pLoadExtension->errMsg;
			*/
		$result = reloadAdd();
		if($result == false)
			$Messages .= $result;
		/*
		if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
			$Messages .= $pLoadExtension->errMsg;*/

		//$Messages = $Messages? $Messages : "分机添加成功!";
		generateArrConf();//生成缓存信息
		return $Messages;
	}


	function editTenants(){
		$id = $_REQUEST['id'];
		$mod = M("tenants");
		$arrData = $mod->where("id = '$id'")->find();
		$this->assign("arrData",$arrData);
		$this->display();
	}

	function updateTenants(){
		$id = $_REQUEST['id'];
		$mod = M("tenants");
		$arrData = array(
			'enterprise_number'=>$_REQUEST['enterprise_number'],
			'password'=>$_REQUEST['password'],
			'company_name'=>$_REQUEST['company_name'],
			'management_leader'=>$_REQUEST['management_leader'],
			'management_leader_phone'=>$_REQUEST['management_leader_phone'],
			'management_leader_email'=>$_REQUEST['management_leader_email'],
			'management_leader_qq'=>$_REQUEST['management_leader_qq'],
			'company_contact'=>$_REQUEST['company_contact'],
			'contact_phone'=>$_REQUEST['contact_phone'],
			'contact_email'=>$_REQUEST['contact_email'],
			'contact_qq'=>$_REQUEST['contact_qq'],
			'start_lease_time'=>$_REQUEST['start_lease_time'],
			'end_lease_time'=>$_REQUEST['end_lease_time'],
			'start_work_number'=>$_REQUEST['start_work_number'],
			'end_work_number'=>$_REQUEST['end_work_number'],
			'start_extension'=>$_REQUEST['start_extension'],
			'end_extension'=>$_REQUEST['end_extension'],
			'role_id'=>$_REQUEST['role_id'],
			'max_concurrent'=>$_REQUEST['max_concurrent'],
			'description'=>$_REQUEST['description'],
		);
		$result = $mod->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){

			//把角色表的管理员权限也给它更新一次
			$role_id = $_REQUEST["role_id"];
			$baseName = "bgcrm".$id;
			$tenants_role = M("tenants_role");
			$arrRole = $tenants_role->where("r_id = '$role_id'")->find();
			$action_list = $arrRole["action_list"];
			$sql_up_role = "update $baseName.role set action_list='$action_list' where r_id=1";
			$mod->execute($sql_up_role);

			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteTenants(){
		$id = $_REQUEST["id"];
		$mod = M("tenants");
		$result = $mod->where("id in ($id)")->delete();
		if ($result){
			//删除数据库
			$del_sql = "DROP DATABASE IF EXISTS bgcrm$id";
			$mod->query($del_sql);

			//清除缓存
			$cmd2 = "cd /var/www/html/BGCC/Conf/crm;  rm -rf bgcrm$id";
			exec($cmd2,$retstr2,$retno2);

			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//租户权限
	function permissionsList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Tenant Permissions";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("username",$_SESSION['user_info']['username']);
		$this->assign("priv",$priv);

		$this->display();
	}

	function permissionsData(){
		$arrAdmin = getAdministratorNum();

		$menuname = "Tenant Permissions";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$mod = M("tenants_role");
		$count = $mod->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->limit($page->firstRow.','.$page->listRows)->select();

		foreach($arrData as &$val){
			if(in_array($username,$arrAdmin)  || $priv["purview"] == "Y"){
				$val['operating'] = "<a href='javascript:void(0);'  onclick=\"setRoles("."'".$val["r_id"]."'".",'".$val["r_name"]."'".");\">设置权限</a>";
			}else{
				$val['operating'] = "";
			}

		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	function insertPermissions(){
		$username = $_SESSION['user_info']['username'];
		$mod = M("tenants_role");
		$arrData = array(
			'r_name'=>$_REQUEST['r_name'],
			'r_description'=>$_REQUEST['r_description'],
		);
		$result = $mod->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updatePermissions(){
		$id = $_REQUEST['id'];
		$mod = M("tenants_role");
		$arrData = array(
			'r_name'=>$_REQUEST['r_name'],
			'r_description'=>$_REQUEST['r_description'],
		);
		$result = $mod->data($arrData)->where("r_id = '$id'")->save();
		//echo $mod->getLastSql();die;
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deletePermissions(){
		$id = $_REQUEST["id"];
		$mod = M("tenants_role");
		$result = $mod->where("r_id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//设置权限
    function showRoleSet(){
		checkLogin();
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$sys_type = $checkRole[2];
		//dump($checkRole);die;
		$menu_id = $this->getModuleType($sys_type);
		$mpid = $menu_id['menu_pid'];
		$sub_id = $menu_id['sub_id'];

		//角色的相关权限
        $r_id=$_GET['r_id'];
        if($r_id){
			$mod = M("tenants_role");
            $arrRole = $mod->where("r_id=$r_id")->find();
            if($arrRole){
				$arrPriv = json_decode( $arrRole['action_list'],true );//权限转换为数组
            }else{
                $this->error('要分配的权限的角色不存在！');
            }
        }else{
            $this->error('权限项不存在！');
        }
		$roleName = $arrRole['r_name'];
		$this->assign("roleName",$roleName);
		//dump($arrPriv);die;
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model("$menu_table");
		$where = "me.pid=0 AND enabled='Y' AND me.id in($mpid)";
		$mainMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where($where)->order("`order` ASC")->select();
		//echo $menu->getLastSql();
		//dump($mainMenu);die;
		$i = 0;
		foreach( $mainMenu AS $row ){
			$tpl_menu[$i] = Array();
			//直接在php代码中处理一切模版显示的内容，这样比在模版中灵活多了
			$mainId = $row['id'];//一级菜单id
			$mainText = $row['name'];//一级菜单标题

			$tpl_menu[$i]['id'] = $mainId;
			$tpl_menu[$i]['state'] = 'closed';     //打开页面时 一级标题全部折叠
			$tpl_menu[$i]['text'] = L($mainText);
			$tpl_menu[$i]['children'] = Array();

			$subMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y' AND me.id in($sub_id)")->order("`order` ASC")->select();
			//dump($subMenu);die;
			$j = 0;
			foreach($subMenu AS $arrRow){
				$arrTmp2 = array();//---------------------------------------临时数组
				$subId = $arrRow['id'];//二级菜单id
				$subText = $arrRow['name'];//二级菜单标题

				$arrTmp2['id'] = $subId;
				$arrTmp2['text'] = L($subText);
				$arrTmp2['state'] = 'closed';   //打开页面时 点击一级标题后 二级标题全部折叠
				$arrTmp2['children'] = Array();

				//最小 子权限 列表框的显示
				$k = 0;
				$arrP = explode(',',$arrRow['privilege']);//---------------------------------------临时数组
				foreach($arrP AS $v){
					//echo $arrPriv[$i]['children'][$j]['children'][$k]["checked"];
					if( $arrPriv[$mainText][$subText][$v] == 'Y' ){
						$checked = true;//从【权限数组】中判断
					}else{
						$checked = false;
					}
					$arrTmp3 = array(//---------------------------------------临时数组
						//"id" => $subId ."_". $v,
						"id" => $v,
						"text" => L($v),//翻译
						"iconCls"=>$v,
						"checked" => $checked,
					);
					$arrTmp2['children'][$k] = $arrTmp3;
					$k++;
				}
				$tpl_menu[$i]['children'][$j] = $arrTmp2;//把子菜单赋值给父菜单
				$j++;
			}
			$i++;
		}
		//dump($tpl_menu);die;
		//echo "<pre>";
		//var_export($tpl_menu);die;
		$strJSON = json_encode( $tpl_menu,true );
		//dump($strJSON);die;
		$this->assign('id',$r_id);
		$this->assign("strJSON",$strJSON);
        $this->display();
    }



	function getModuleType($sys_type){
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		//$sys_type = "outbound,inbound,wechat,pb,ks,wj,wf";
		$arrAL = explode(",",$sys_type);
		$where = "";
		foreach($arrAL as $val){
			$str = "find_in_set('".$val."',sys_info) ";
			$where .= empty($where) ? "$str" : " OR $str";
		}
		//dump($where);die;
		$menu = new Model("$menu_table");
		$arrData = $menu->field("id,name,url,sys_type,pid")->where($where)->select();
		foreach($arrData as $val){
			if($val["pid"] == "0"){
				$arrPid[] = $val["id"];
			}else{
				$arrId[] = $val["id"];
			}
		}
		//dump($arrData);die;
		$str_id = implode(",",$arrId);
		$str_pid = implode(",",$arrPid);
		$arr_menuid = array(
			"sub_id" => $str_id,
			"menu_pid" => $str_pid,
		);
		return $arr_menuid;
	}

    //修改权限
    function savePriv(){
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		//dump($_POST);die;
		$r_id = $_POST['r_id'];
		$json = $_POST['json'];
		$arrJSON = json_decode($json,true);
		//构造一张一维数组$menuList: "id" =>"name" 的键值对
		$menuList = Array();
		$menu = M("$menu_table");
		$arrMenu = $menu->table("$menu_table me")->field("me.id AS id,name")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where("enabled='Y'")->order("`order` ASC")->select();
		foreach( $arrMenu As $row ){
			$menuList[$row['id']] = $row['name'];
		}
		$arrPriv = Array();
		$allNoChecked1 = true;//初始化【父菜单】都没选择
		foreach($arrJSON AS $val1){
			$mainId = $val1['id'];//父菜单ID
			$mainName = $menuList[$mainId];
			//$arrPriv[$mainName] = Array();//存储子菜单==============================
			$arrSub = Array();
			$allNoChecked2 = true;//初始化【子菜单】都没选择
			foreach( $val1["children"] AS $val2 ){
				$subId = $val2['id'];//子菜单ID
				$subName = $menuList[$subId];
						//$arrPriv[$mainName] = Array();//========================
						$arrP = Array();
						$allNoChecked3 = true;//初始化【权限】都没有
						foreach( $val2["children"] AS $val3 ){
							if( $val3["checked"] == true ){
								$arrP[$val3['id']] = 'Y';
								$allNoChecked3 = false;
							}
						}
				if( $allNoChecked3 == false && $arrP ){//如果有子菜单选中，都没选中就不要管它
					$arrSub[$subName] = $arrP;
					$allNoChecked2 = false;
				}
			}
			if( $allNoChecked2 == false && $arrSub){//如果二级菜单有选中的，但不一定是全选
				$arrPriv[$mainName] = $arrSub;
				$allNoChecked1 = false;
			}
		}
		//dump($arrPriv);die;
		$strPriv = json_encode($arrPriv);
		$mod = M("tenants_role");
		$res = $mod->where("r_id = '$r_id'")->save( array("action_list"=>$strPriv) );
        if(false !== $res){

			//把角色表的管理员权限也给它更新一次
			$tenants = M("bgcrm.tenants");
			$arrD = $tenants->field("id,role_id")->where("role_id = '$r_id'")->select();
			foreach($arrD as $val){
				$baseName = "bgcrm".$val["id"];
				$sql_up_role = "update $baseName.role set action_list='$strPriv' where r_id=1";
				$mod2 = M("$baseName.role");
				$mod2->execute($sql_up_role);
			}
			//dump($baseName);die;

           echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
        }else{
            echo json_encode(array('msg'=>'更新失败！'));
        }
    }



}
?>
