<?php
class SystemAdminAction extends Action{

    //系统参数设置的主界面
	function listSystemSetParam(){
		checkLogin();
        header("Content-Type:text/html; charset=utf-8");


		//分配增删改的权限
		$menuname = "System parameter";
		$p_menuname = $_SESSION['menu'][$menuname];//父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display("","utf-8");
    }

    //系统参数设置的主界面
	function systemSetList(){
		checkLogin();
        header("Content-Type:text/html; charset=utf-8");


		//分配增删改的权限
		$menuname = "System parameter";
		$p_menuname = $_SESSION['menu'][$menuname];//父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$parameter_type = $_REQUEST["parameter_type"];
		$this->assign("parameter_type",$parameter_type);

		$this->display("","utf-8");
    }

	function test(){
		$sys = new SystemSetModel();
		$arrData = $sys->select();
		$arrTmp = $this->getRepeatArray($arrData,"name");
		foreach($arrTmp as $val){
			$arrId[] = $val["id"];
		}
		dump($arrTmp);die;
	}

	function getRepeatArray($arr, $key){
		$tmp_arr = array();
		//$tmp = array();
		foreach($arr as $k => $v){
			if(in_array($v[$key], $tmp_arr)){
				$tmp[] = $v;
			}else{
				$tmp_arr[] = $v[$key];
			}
		}
		sort($tmp);
		return $tmp;
	}

	function systemParamData(){
        $sys = new SystemSetModel();
		$name = $_REQUEST["name"];
		$description = $_REQUEST["description"];
		$parameter_type = $_REQUEST["parameter_type"];
		$where = "1 ";
		$where .= empty($name) ? "" : " AND `name` like '%$name%'";
		$where .= empty($description) ? "" : " AND `description` like '%$description%'";
		$where .= empty($parameter_type) ? "" : " AND find_in_set('$parameter_type',parameter_type)";

		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( !in_array("wf",$arrAL) ){
			$where .= " AND `name` not in ('collection_types','leave_process_id','leave_name')";
		}
		if( !in_array("pb",$arrAL) ){
			$where .= " AND `name` not in ('go_to_work_time','go_off_work_time')";
		}

		import('ORG.Util.Page');
        $count=$sys->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
        $systemData = $sys->order('id')->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		foreach($systemData as &$val){
			if($val["radio_able"] == "Y"){
				$val["radioText"] = explode("|",$val["radio_value"]);
				foreach($val["radioText"] as &$vm){
					$vm = explode(",",$vm);
					if($vm[0] == $val["svalue"]){
						$vm["checked"] = "checked";
						$vm["radio"] = "<input type='radio' name='".$val["name"]."' value='".$vm[0]."' checked onclick=\"updateSys("."'".$val["id"]."','".$vm[0]."'".");\" >".$vm[1]."&nbsp;&nbsp;";
					}else{
						$vm["radio"] = "<input type='radio' name='".$val["name"]."' value='".$vm[0]."' onclick=\"updateSys("."'".$val["id"]."','".$vm[0]."'".");\" >".$vm[1]."&nbsp;&nbsp;";
					}
					$val["radioValue"][] = $vm["radio"];
				}
				$val["radioMsg"] = implode("&nbsp;",$val["radioValue"]);
			}

			if($val["radio_able"] == "S"){
				$val["radioMsg"] = '<input class="easyui-combobox" name="'.$val["name"].'" id="id_'.$val["name"].'"
					data-options="
					checkbox:true,
					valueField:\'id\',
					textField:\'name\',
					panelHeight:\'auto\'
				">';
			}

		}
		//dump($systemData);die;

        $rowsList = count($systemData) ? $systemData : false;
		$arrSystem["total"] = $count;
		$arrSystem["rows"] = $rowsList;

		echo json_encode($arrSystem);
	}

	function updateSys(){
		$id = $_REQUEST["id"];
		$value = $_REQUEST["value"];
		$system_set = new Model("system_set");

		$result = $system_set->where("id='$id'")->save(array("svalue"=>$value));

		if ($result !== false){
			$this->updateSystemparameter();
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

    //显示添加系统参数的页面
    function addSystemSet(){
		checkLogin();
        header("Content-Type:text/html; charset=utf-8");
    	$sys=new SystemSetModel();
		$list=$sys->select();
		$this->assign('slist',$list);
		$this->display("","utf-8");
    }

 /*   //添加系统参数
    function insertSystemSet(){
        $sys = new SystemSetModel();
        if($_POST['description']){
            $data=$sys->create();
            if( $data ){
                if(false!==$sys->add()){
                    echo "ok";
					$this->updateSystemparameter();
                }else{
                    echo ($sys->getDbError());
                }
            }else{
                echo ($sys->getError());
            }
        }else{
            echo L("The description can not be empty").'！';
        }

    }
*/
    //添加系统参数
    function insertSystemSet(){
        $sys = new SystemSetModel();
		$count = $sys->where("name ='".$_REQUEST["name"]."'")->count();
		if( $count>0 ){
			echo json_encode(array('msg'=>"此系统参数已存在！"));
			exit;
		}
		$parameter_type = implode(",",$_REQUEST["parameter_type"]);
		$arrData = array(
			"parameter_type" => $parameter_type,
			"description" => $_REQUEST["description"],
			"name" => $_REQUEST["name"],
			"svalue" => $_REQUEST["svalue"],
		);
		$result = $sys->data($arrData)->add();
		if ($result){
			$this->updateSystemparameter();
			echo json_encode(array('success'=>true,'msg'=>'系统参数添加成功！'));
		} else {
			echo json_encode(array('msg'=>'系统参数添加失败！'));
		}
    }

    //获取要编辑的系统参数编号
    function editSystemSet(){
		checkLogin();
        //print_r($_GET);
        $tpl_id=$_GET['id'];
        if(!empty($tpl_id)){
            $this->assign("id",$tpl_id);
            $sys=new SystemSetModel();
            $date=$sys->getById($tpl_id);
            if($date){
                $this->assign('udate',$date);
                $this->display();
            }else{
                echo L('Record does not exist');
            }
        }else{
            echo L('Record does not exist');
        }
    }

	function updateSystemSet(){
		$name = $_REQUEST["name"];
		$id = $_REQUEST["id"];
		$sys=new SystemSetModel();
		$parameter_type = implode(",",$_REQUEST["parameter_type"]);
		$arrData = array(
			"parameter_type" => $parameter_type,
			"description" => $_REQUEST["description"],
			//"name" => $_REQUEST["name"],
			"svalue" => $_REQUEST["svalue"],
		);
		$result = $sys->data($arrData)->where("id = $id")->save();
		if ($result !== false){
			$this->updateSystemparameter();
			if($name == "bindport"){
				$content = "bindport=".$_REQUEST["svalue"];
				file_put_contents("/etc/asterisk/sip_bindport.conf",$content);
				$ip = $_SERVER['SERVER_ADDR'];
				getHttpPost("http://$ip/config.php?handler=reload","");
			}
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function updateSelectSystemSet(){
		$name = $_REQUEST["name"];
		$sys=new SystemSetModel();
		$arrData = array(
			"svalue" => $_REQUEST["svalue"],
		);
		$result = $sys->data($arrData)->where("`name` = '$name'")->save();
		if ($result !== false){
			$this->updateSystemparameter();
			if($name == "record_storage_time"){
				$day_num = $_REQUEST["svalue"]*30;
				file_put_contents('/var/www/html/data/system/recordNum',$day_num);
			}
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}


	//设置缓存
	function updateSystemparameter(){

		$sys=new SystemSetModel();
		$arr = $sys->select();

		foreach($arr as $key => $val){
			$tmp[$val["name"]]	= $val["svalue"];
		}

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F('system',$tmp,"BGCC/Conf/crm/$db_name/");
		//F('system',$tmp,"BGCC/Conf/");
	}

    //删除系统参数
    function deleteSystemSet(){
		$id = $_REQUEST["id"];
		//dump($id);die;
		$sys = new SystemSetModel();
		$result = $sys->where("id in ($id)")->delete();
		if ($result){
			$this->updateSystemparameter();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
    }



	 function sccpReload()
	 {
		sccpdeviceReload();
	 }

	 //自定义功能
	 function pin(){
		checkLogin();
		$m = M();
		if( $count = $_POST['count'] ){//表示是表单提交操作
			//dump($count);die;

			//改数据库
			$sql_insert = "INSERT INTO pin VALUES";
			$insertContent = "";
			$PIN1="";
			$PIN2="";
			$PIN3="";
			$PIN4="";
			//dump($_POST["id_3"]);die;
			for($i=0;$i<$count;$i++){

				$v = "('".$_POST["username_$i"]."'"  .
					 ",'".$_POST["level_$i"]."'"  .
					 ",'".$_POST["pin1_$i"]."'"  .
					 ",'".$_POST["pin2_$i"]."'"  .
					 ",'".$_POST["pin3_$i"]."'"  .
					 ",'".$_POST["pin4_$i"]."'"  .  ")";

				$insertContent .= empty($insertContent)?$v:",".$v;
				if( $_POST["pin1_$i"] )$PIN1 .= $_POST["pin1_$i"] . "\n";
				if( $_POST["pin2_$i"] )$PIN2 .= $_POST["pin2_$i"] . "\n";
				if( $_POST["pin3_$i"] )$PIN3 .= $_POST["pin3_$i"] . "\n";
				if( $_POST["pin4_$i"] )$PIN4 .= $_POST["pin4_$i"] . "\n";
			}
			if($fh1 = fopen("/etc/asterisk/pinset_1",'w')){
				fwrite($fh1,$PIN1);
				$sqlpin1 = "UPDATE asterisk.pinsets SET passwords ='$PIN1' where pinsets_id=1";
				$m->query($sqlpin1);
			}
			if($fh2 = fopen("/etc/asterisk/pinset_2",'w')){
				fwrite($fh2,$PIN2);
				$sqlpin2 = "UPDATE asterisk.pinsets SET passwords ='$PIN2' where pinsets_id=2";
				$m->query($sqlpin2);
			}
			if($fh3 = fopen("/etc/asterisk/pinset_3",'w')){
				fwrite($fh3,$PIN3);
				$sqlpin3 = "UPDATE asterisk.pinsets SET passwords ='$PIN3' where pinsets_id=3";
				$m->query($sqlpin3);
			}
			if($fh4 = fopen("/etc/asterisk/pinset_4",'w')){
				fwrite($fh4,$PIN4);
				$sqlpin4 = "UPDATE asterisk.pinsets SET passwords ='$PIN4' where pinsets_id=4";
				$m->query($sqlpin4);
			}
			//echo "pinset_11--pinset_22--pinset_33--pinset_44";die;



			//生成/etc/asterisk下面的pin文件
			$sql_insert .= $insertContent;
			//dump($sql_insert);die;
			$m->query('TRUNCATE TABLE pin');
			$m->query($sql_insert);
		}

		$sql_count = "SELECT  count(*) AS count FROM users u LEFT JOIN pin p ON u.`username`= p.username";
		$sql = "SELECT  u.username AS username,extension,cn_name,email,level,pin1,pin2,pin3,pin4 FROM users u LEFT JOIN pin p ON u.`username`= p.username order by username";
		$arr_count = $m->query($sql_count);
		$count = $arr_count[0]['count'];

		$arr_rows = $m->query($sql);
		//dump($arr_row);die;
		for($i=0;$i<$count;$i++){
			$arr_rows[$i]["username_name"] = "username_$i";
			$arr_rows[$i]["level_name"] = "level_$i";
			$arr_rows[$i]["pin1_name"] = "pin1_$i";
			$arr_rows[$i]["pin2_name"] = "pin2_$i";
			$arr_rows[$i]["pin3_name"] = "pin3_$i";
			$arr_rows[$i]["pin4_name"] = "pin4_$i";
			$arr_rows[$i]["id"] = "$i";
			if( isset($_POST["id_$i"]) ){
				$arr_rows[$i]['checkbox'] = "checked";//重要
			}else{
				$arr_rows[$i]['checkbox'] = "";//重要
			}
		}
		//dump($arr_rows);die;
		$this->assign('count',$count);
		$this->assign('rows',$arr_rows);
		$this->display();

	 }

	 function importPinExcel(){
		$m = M();
		$sql = "SELECT  u.username AS username,extension,cn_name,email,level,pin1,pin2,pin3,pin4 FROM users u LEFT JOIN pin p ON u.`username`= p.username order by username";
		$arr_rows = $m->query($sql);
		//dump($arr_rows);die;
		$csv = "WorkNo,Extension,name,email,level,DongGuan_PIN,GuangDong_PIN,China_PIN,Foreign_PIN\r\n";
		foreach($arr_rows AS $v){
			$csv .= $v['username'].",";
			$csv .= $v['extension'].",";
			$csv .= $v['cn_name'].",";
			$csv .= $v['email'].",";
			$csv .= $v['level'].",";
			$csv .= $v['pin1'].",";
			$csv .= $v['pin2'].",";
			$csv .= $v['pin3'].",";
			$csv .= $v['pin4']."\r\n";
		}
		//dump($csv);die;
		header('HTTP/1.1 200 OK');
		//header('Accept-Ranges: bytes');
		header('Date: ' . date("D M j G:i:s T Y"));
		header('Last-Modified: ' . date("D M j G:i:s T Y"));
		header("Content-Type: application/force-download");
		header("Content-Length: " . strlen($csv));
		header("Content-Transfer-Encoding: Binary");
		header("Content-Disposition: attachment;filename=Extension-PIN.csv");
		echo $csv;
	 }

	 function batchUsersDownload(){
		$m = M();
		$sql = "SELECT * FROM `bgcrm`.users";
		$arr_rows = $m->query($sql);

		$arr_department = getDepartmentsByid();
		$arr_role = getRoleByid();

		//dump($arr_rows);die;
		$csv = "username,password,extension,department,role,Chinese name,English name,Email,Fax,extension type,Extension mac,Extension model\r\n";
		foreach($arr_rows AS $v){
			$csv .= $v['username'].",";
			$csv .= $v['password'].",";
			$csv .= $v['extension'].",";
			$csv .= utf2gb($arr_department[$v['d_id']]["d_name"]).",";
			$csv .= utf2gb($arr_role[$v['r_id']]["r_name"]).",";
			$csv .= utf2gb($v['cn_name']).",";
			$csv .= $v['en_name'].",";
			$csv .= $v['email'].",";
			$csv .= $v['fax'].",";
			$csv .= $v['extension_type'].",";
			$csv .= $v['extension_mac'].",";
			$csv .= $v['extension_model']."\r\n";
		}
		//dump($csv);die;
		header('HTTP/1.1 200 OK');
		//header('Accept-Ranges: bytes');
		header('Date: ' . date("D M j G:i:s T Y"));
		header('Last-Modified: ' . date("D M j G:i:s T Y"));
		header("Content-Type: application/force-download");
		header("Content-Length: " . strlen($csv));
		header("Content-Transfer-Encoding: Binary");
		header("Content-Disposition: attachment;filename=IPPBX Users.csv");
		echo $csv;

	 }

	 function sendPinEmail(){
		set_time_limit(0);
		Vendor('Phpmailer.PHPM');
		//echo $_GET['usernames'];die;
		$usernames = $_GET['usernames'];
		$m = M('users');
		if( $usernames == "all" ){
			$sql = "SELECT u.username AS username,extension,email,pin1 FROM users u LEFT JOIN pin p ON u.username=p.username";
		}else{
			$tmpArr = explode(',',$usernames);
			//dump($tmpArr);die;
			array_shift($tmpArr);
			//dump($tmpArr);die;
			$str = "";
			foreach( $tmpArr As $v ){
				$str .= empty($str)?"'$v'":",'$v'";
			}
			$sql = "SELECT u.username AS username,extension,email,pin1 FROM users u LEFT JOIN pin p ON u.username=p.username where u.username in ($str)";
		}
		//dump($sql);die;
		$result = $m->query($sql);

		$smtp_host = "smtp.celestica.com";
		$smtp_account = "IPPBX@celestica.com";
		$smtp_password = "";
		//$receive_email = "dennisdu@celestica.com";

		$mail = new PHPMailer(); //建立邮件发送类

		$mail->IsSMTP(); // 使用SMTP方式发送
		$mail->CharSet='UTF-8';// 设置邮件的字符编码
		$mail->Host = $smtp_host;
		$mail->SMTPAuth = false; // 启用SMTP验证功能----关闭
		$mail->Username = $smtp_account;
		$mail->Password = $smtp_password;
		$mail->From = "CSSPhone@celestica.com"; //邮件发送者email地址
		$mail->FromName = "CSSPhone@celestica.com";
		$mail->Subject = "Phone authorization code changed"; //邮件标题

		foreach( $result AS $row ){
			$workNO = $row['username'];
			$exten = $row['extension'];
			$email = $row['email'];
			$pin = $row['pin1'];
			//$mailmsg = "Hello,workNO:$workNO!\nYour phone authorization code is changed to $pin now\n";
			$mailmsg ='account | password


'. $pin .'


Please keep it secure and don’t share your code with others.
The way of dialing outside:
dial 9 + dial outside number + *password* + #
In order to save cost, please dial to below sites & cities thru lease line instead of directly.
https://sites.google.com/a/celestica.com/celestica-intranet/sites/song-shan-lake-china/information-technology/it-services/telephony-service
Just a reminder
"All calling being under monitored & recorded by IT and non-business call will be reported to your departments managers/GM."';
			$mail->AddAddress("$email", "");//收件人地址，可以替换成任何想要接收邮件的email信箱,格式是AddAddress("收件人email","收件人姓名")
			$mail->Body = $mailmsg; //邮件内容
			if(!$mail->Send()){
				echo "邮件发送失败. <p>";
				echo "错误原因: " . $mail->ErrorInfo;
			}
		}
		echo "email is sending...";
	 }



 }


?>

