<?php
class LanguageAction extends Action{
	//语言
	function Language(){
		checkLogin();
		$language = $_POST['language'];
		$system_set = M('system_set');
		if( $language ){
			$system_set->where("name='language'")->save( array('svalue'=>$language) );
			$this->updateSystemparameter();
			setcookie("lang",$language);
			setcookie("think_language",$language);
			goBack("","index.php","top");
		}else{
			//$arrTmp = $system_set->where("name='language'")->find();
			$para_sys = readS();
			$language = $para_sys["language"];
		}
		$this->assign("language",$language);

		//分配增删改的权限
		$menuname = "Language";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv['edit']);

		$this->display();
	}

	function updateSystemparameter(){

		$sys=new Model("System_set");
		$arr = $sys->select();

		foreach($arr as $key => $val){
			$tmp[$val["name"]]	= $val["svalue"];
		}
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F('system',$tmp,"BGCC/Conf/crm/$db_name/");
		//F('system',$tmp,"BGCC/Conf/");
	}


	function importLanguage(){
		header("Content-Type:text/html; charset=utf-8");
		$arrBGCCLageEN = require "BGCC/Lang/en_es/common.php";
		$arrBGCCLageCN = require "BGCC/Lang/zh_cn/common.php";

		foreach($arrBGCCLageCN as $key=>$val){
			$arrData[] = array(
				"php_mod"=>str_replace("'","!^^^!",$key),
				"en_name"=>str_replace("'","!^^^!",$arrBGCCLageEN[$key]),
				"cn_name"=>str_replace("'","!^^^!",$val),
			);
		}

		//dump($arrData);die;
		$sql = "INSERT INTO sys_language(php_mod,en_name,cn_name) VALUES ";
		$value = "";
		foreach($arrData as $key=>$val){
			if(!$val["php_mod"]){
				continue;
			}else{
				$str = "(";
				$str .= "'" .$val["php_mod"]. "',";
				$str .= "'" .$val["en_name"]. "',";
				$str .= "'" .$val["cn_name"]. "'";
				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
		}
		$mod = M();
		if( $value ){
			$sql .= $value;
			echo $sql;
			$result = $mod->execute($sql);
		}
		//dump($result);
		//dump($arrData);die;
	}

	function importLanguage2(){
		header("Content-Type:text/html; charset=utf-8");
		$arrAgentLageEN = require "Agent/Lang/en_es/common.php";
		$arrAgentLageCN = require "Agent/Lang/zh_cn/common.php";

		foreach($arrAgentLageCN as $key=>$val){
			$arrData[] = array(
				"php_mod"=>str_replace("'","!^^^!",$key),
				"en_name"=>str_replace("'","!^^^!",$arrAgentLageEN[$key]),
				"cn_name"=>str_replace("'","!^^^!",$val),
			);
		}

		$sql = "INSERT INTO sys_language2(php_mod,en_name,cn_name) VALUES ";
		$value = "";
		foreach($arrData as $key=>$val){
			if(!$val["php_mod"]){
				continue;
			}else{
				$str = "(";
				$str .= "'" .$val["php_mod"]. "',";
				$str .= "'" .$val["en_name"]. "',";
				$str .= "'" .$val["cn_name"]. "'";
				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
		}
		$mod = M();
		if( $value ){
			$sql .= $value;
			//echo $sql;
			$result = $mod->execute($sql);
		}

		$sql2 = "INSERT INTO sys_language(php_mod,en_name,cn_name) SELECT php_mod,en_name,cn_name FROM sys_language2  WHERE NOT EXISTS(SELECT * FROM sys_language WHERE sys_language.php_mod=sys_language2.php_mod)";
		$result2 = $mod->execute($sql2);
		dump($result);
		dump($arrData);die;
	}

	function sysLanguageList(){
		checkLogin();
		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);
		$this->display();
	}

	function sysLanguageData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$key = $_REQUEST["key"];
		$where = "1 ";
		$where .= empty($key) ? "" : " AND (en_name like '%$key%' OR cn_name like '%$key%')";

		$mod = M("sys_language");
		$count = $mod->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $mod->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $mod->getLastSql();die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function saveLanguage(){
		$arrInsertData = json_decode($_REQUEST["inserted"],true);
		$arrUpdatedData = json_decode($_REQUEST["updated"],true);
		$arrDeletedData = json_decode($_REQUEST["deleted"],true);
		//dump($arrInsertData);
		//dump($arrUpdatedData);
		//dump($arrDeletedData);die;

		$mod = M("sys_language");
		if($arrInsertData){
			$insert_sql = "INSERT INTO sys_language(php_mod,en_name,cn_name) VALUES ";
			$value = "";
			foreach($arrInsertData as $key=>$val){
				if(!$val["php_mod"]){
					continue;
				}else{
					$str = "(";
					$str .= "'" .$val["php_mod"]. "',";
					$str .= "'" .$val["en_name"]. "',";
					$str .= "'" .$val["cn_name"]. "'";
					$str .= ")";
					$value .= empty($value)?"$str":",$str";
				}
			}
			if( $value ){
				$insert_sql .= $value;
				$result = $mod->execute($insert_sql);
			}
		}

		if($arrUpdatedData){
			foreach($arrUpdatedData as $val){
				$update_sql = "update sys_language set en_name='".$val["en_name"]."',cn_name='".$val["cn_name"]."' where id='".$val["id"]."'";
				$result = $mod->execute($update_sql);
			}
		}

		if($arrDeletedData){
			foreach($arrDeletedData as $val){
				$delete_sql = "delete from sys_language where id='".$val["id"]."'";
				$result = $mod->execute($delete_sql);
			}
		}

		if($result){
			$this->writeLanguage();
			echo json_encode(array('success'=>true,'msg'=>'修改成功！'));
		} else {
			echo json_encode(array('msg'=>$mod->getLastSql()));
		}
	}

	function writeLanguage(){
		header("Content-Type:text/html; charset=utf-8");
		$mod = M("sys_language");
		$arrData = $mod->select();
		$arrENLanguage = array();
		$arrCNLanguage = array();
		foreach($arrData as $key=>&$val){
			$val["php_mod"] = str_replace("!^^^!","'",$val["php_mod"]);
			$val["en_name"] = str_replace("!^^^!","'",$val["en_name"]);
			$val["cn_name"] = str_replace("!^^^!","'",$val["cn_name"]);

			$arrENLanguage[$val["php_mod"]] = $val["en_name"];
			$arrCNLanguage[$val["php_mod"]] = $val["cn_name"];
		}
		//dump($arrData);die;
		$cmd = "chmod 777 /var/www/html/BGCC/Lang/zh_cn/common.php && chmod 777 /var/www/html/BGCC/Lang/en_es/common.php &&  chmod 777 /var/www/html/Agent/Lang/zh_cn/common.php && chmod 777 /var/www/html/Agent/Lang/en_es/common.php";
		$res = exec($cmd,$retstr1,$retno1);

		$en_content = "<?php\nreturn " .var_export($arrENLanguage,true) .";\n ?>";
		$cn_content = "<?php\nreturn " .var_export($arrCNLanguage,true) .";\n ?>";
		$result = file_put_contents("BGCC/Lang/zh_cn/common.php",$cn_content);
		$result1 = file_put_contents("BGCC/Lang/en_es/common.php",$en_content);
		$result3 = file_put_contents("Agent/Lang/zh_cn/common.php",$cn_content);
		$result4 = file_put_contents("Agent/Lang/en_es/common.php",$en_content);
		/*
		dump($result);
		dump($result1);
		dump($result3);
		dump($result4);die;
		*/
		$type = $_REQUEST["type"];
		if($type){
			if($result4){
				echo json_encode(array('success'=>true,'msg'=>'应用成功！'));
			}else{
				echo json_encode(array('msg'=>'应用失败！'));
			}
		}else{
			return true;
		}
	}

}
?>
