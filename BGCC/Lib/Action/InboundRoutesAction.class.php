<?php
class InboundRoutesAction extends Action{
	function routeList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Inbound Routes";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function routeDataList(){
		$incoming = new Model("asterisk.incoming");
		$count = $incoming->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		mysql_query("set names latin1"); //设置字符集，要跟数据库中的一样
		$incomingData = $incoming->limit($page->firstRow.",".$page->listRows)->select();

		$i = 0;
		foreach($incomingData as $v){
			$incomingData[$i]['extension2'] = urlencode($v['extension']);
			$incomingData[$i]['cidnum2'] = urlencode($v['cidnum']);
			$i++;
		}
		unset($i);
		//dump($incomingData);die;
		$listRows = count($incomingData) ? $incomingData : false;
		$arrIncoming["total"] = $count;
		$arrIncoming["rows"] = $listRows;

		echo json_encode($arrIncoming);
	}

	function addRoutein(){
		checkLogin();
		include_once ("/var/www/html/admin.php");
		$content = getContent($this ,"exteinsion",false);
		GLOBAL $db;


		$sql = "select `cidlookup_id`,`description` from asterisk.cidlookup";
		$cidlookup = $db->getAll($sql,DB_FETCHMODE_ASSOC);
		$this->assign("cidlookup",$cidlookup);
		//dump($cidlookup);die;
		$this->display();
	}

	function editRoutein(){
		checkLogin();
		include_once ("/var/www/html/admin.php");
		$content = getContent($this ,"exteinsion",false);
		GLOBAL $db;

		$extension = $_GET["extension"];
		$cidnum = $_GET["cidnum"];
		$sql = "select * from asterisk.incoming where extension = '$extension' AND cidnum= '$cidnum'";
		$incoming = $db->getRow($sql,DB_FETCHMODE_ASSOC);
		$guide = $incoming['destination'];
		$this->assign("incoming",$incoming);
		$this->assign("guide",$guide);

		$sql2 = "select `cidlookup_id`,`description` from asterisk.cidlookup";
		$cidlookup = $db->getAll($sql2,DB_FETCHMODE_ASSOC);
		$this->assign("cidlookup",$cidlookup);

		$sql3 = "select * from asterisk.cidlookup_incoming where extension = '$extension' AND cidnum= '$cidnum'";
		$source = $db->getRow($sql3,DB_FETCHMODE_ASSOC);
		$this->assign("source_edit",$source);

		//dump($cidlookup_incoming);die;
		$this->display();
	}


	function importRoutein(){
		$phonenum_exten = M("phonenum_exten");
		$arrData = $phonenum_exten->order("phonenum asc")->field("phonenum,exten")->where("phonenum not in ('56170701','56170702')")->select();
		$i = "3";
		$value = "";
		foreach($arrData as &$val){
			$val["destination"] = "from-did-direct,".$val["exten"].",1";
			$val["description"] = "coming".$i;

			$str = "(";
			$str .= "'" .$val["phonenum"]. "',";
			$str .= "'" .$val["destination"]. "',";
			$str .= "'" .$val["description"]. "',";
			$str .= "'0',";
			$str .= "'0',";
			$str .= "'3',";
			$str .= "'10',";
			$str .= "'',";
			$str .= "''";

			$str .= ")";
			$value .= empty($value)?"$str":",$str";

			$i++;
		}

		$sql = "insert into asterisk.incoming(extension,destination,description,privacyman,delay_answer,pmmaxretries,pmminlength,cidnum,alertinfo) values ";
		if( $value ){
			$sql .= $value;
			//echo $sql;die;
			$res = $phonenum_exten->execute($sql);
		}
		dump($res);die;
		//导入后在ssh中执行下面这条命令 var/lib/asterisk/bin/retrieve_conf
	}

}

?>
