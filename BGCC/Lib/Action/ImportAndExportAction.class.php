<?php
class ImportAndExportAction extends Action{
	//下载导入资料的xls模板
	function downloadXlsTemplates(){
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser' && $val['en_name'] != 'import_mark'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
		}
		$count = count($field);
		$fields = "`".implode("`,`",$field)."`";
		$arrData = array();
		$excelTiele = "导入资料模板";
		exportDataFunction($count,$field,$title,$arrData,$excelTiele);
	}


	function downloadXlsTemplatesToPHPExcel(){
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
		}
		$count = count($field);
		$fields = "`".implode("`,`",$field)."`";
		/*
		$customer = new Model("customer");
		$cmlist = $customer->order("createtime desc")->field($fields)->select();

		$row = getSelectCache();
		//dump($row);
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}
		*/
		vendor("PHPExcel176.PHPExcel");
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}
		/*
		$start_row = 2;
		foreach($cmlist as $val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				if($start_row<5){
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
				}
			}
			$start_row++;
		}
		*/
		$objPHPExcel->setActiveSheetIndex(0);

		$name = "导入资料模板";
		$filename = iconv("utf-8","gb2312",$name);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}

	//下载导入资料的csv模板
	function downloadCSVTemplates(){
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = iconv("utf-8","gb2312",$val['cn_name']);
				}
			}
		}
		$count = count($field);
		$fields = "`".implode("`,`",$field)."`";
		$csvContent = implode(",",$title)."\r\n";
		/*
		$customer = new Model("customer");
		$cmlist = $customer->order("createtime desc")->field($fields)->select();
		$row = getSelectCache();
		//dump($row);
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}
		$i = 0;
		foreach($cmlist as $val){
			if($i<3){
				$vv = implode(',',str_replace(",","",$val));
				$vv = str_replace("\r","",$vv);
				$vv = str_replace("\n","",$vv);
				$csvContent .= iconv("utf-8","gb2312",$vv) ."\r\n";
			}
			$i++;
		}
		*/

		$name = "导入资料模板".date("Y-m-d");
		$filename = iconv("utf-8","gb2312",$name);
		$this->export_csv($filename,$csvContent);
	}


	function export_csv($filename,$data){
		$content = iconv("utf-8","gb2312",$data);
		$d = date("D M j G:i:s T Y");
        header('HTTP/1.1 200 OK');
        header('Date: ' . $d);
        header('Last-Modified: ' . $d);
        header("Content-Type: application/force-download");
        header("Content-Length: " . strlen($data));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".$filename.".csv");
        echo $data;
	}

	function importTaskData(){
		set_time_limit(0);
		@ini_set('memory_limit','512M');


		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);

		$id = $_GET['id'];
		$import_mark = $_GET['import_mark'];
		//黑名单数据检测
		$DNC = new Model('sales_dnc');
		$arrDNC = Array();
		$arrTmp = $DNC->select();
		foreach( $arrTmp AS $row){
			$arrDNC[] = $row['phonenumber'];
		}

		$table = "sales_source_" .$id;
		$source = new Model("sales_source_" .$id);
		//去重
		$systemParam = readS();
		$arrExist = Array(); //把所有号码放入关联数组，hash
		if($systemParam["repeatImportCustomer"] == "phone" || $systemParam["repeatImportCustomer"] == "all"){
			$arrTmp2 = $source->field('phone1')->select();
			foreach( $arrTmp2 AS $row){
				$arrExist[$row['phone1']] = 'Y';
			}
		}

		//导入字段
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser' && $val['en_name'] != 'import_mark'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
			if($val["text_type"] == "2"){
				$selectField[] = $val["en_name"];
			}
			//==========================选择框开始===============================
			if($val["text_type"] == "2" && $val['list_enabled'] == 'Y'){
				$ttField[] = $val["en_name"];
			}
			//==========================选择框结束===============================
		}
		$count = count($field)+1;
		$fields = "`".implode("`,`",$field)."`".",import_mark,`createtime`";
		$field_key = array_flip($field);

		//判断文件类型
		$tmp_file = $_FILES["client_phones"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["client_phones"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));


		if($suffix == "csv"){
			$dst_file = TMP_UPLOAD ."client_phones_".$time.".csv";
			move_uploaded_file($tmp_file,$dst_file);

			$strphones = trim( file_get_contents($dst_file) );
			$arrF = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			array_shift($arrF);
			foreach($arrF as $val){
				$arrData[] = explode(",",rtrim($val,","));
			}
		}else if($suffix == "xlsx"){
				vendor("phpoffice.PHPExcel");

                $objPHPExcel = \PHPExcel_IOFactory::load($_FILES["client_phones"]["tmp_name"]);//读取上传的文件
				$arrData = $objPHPExcel->getSheet(0)->toArray();//获取其中的数据
		}else{
			vendor("PHPExcel176.PHPExcel");
			//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
			$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
			$cacheSettings = array();
			PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);

			$objPHPExcel = new PHPExcel();
			$objPHPExcel = PHPExcel_IOFactory::load($_FILES["client_phones"]["tmp_name"]);

			$arrData = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组
			/*
			//$indata2 = $objPHPExcel->getSheet(1)->toArray();   //多个sheet
			$sheet = $objPHPExcel->getSheetCount();  //excel  sheet个数
			$sheetInfo = $objPHPExcel->getSheet(0);
			$highestRow = $sheetInfo->getHighestRow(); // 取得总行数   包括第一行标题
			$highestColumn = $sheetInfo->getHighestColumn(); // 取得总列数
			*/
			array_shift($arrData);   //去掉第
		}
		//$avg = ceil(count($arrData)/8);
		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		//==========================选择框开始===============================
		$selectValue = getSelectCache();
		foreach($selectValue as $key=>$val){
			$selectKey[$key] = array_flip($val);
		}

		foreach($selectKey as $key=>$val){
			if(in_array($key,$ttField)){
				$arrK[$key] = $val;
			}
		}
		$selectKey = $arrK;
		//dump($selectKey);die;
		$selectF = array_flip($selectField);
		$i = 0;
		foreach($selectKey as $key=>$val){
			$selectKey[$key]['index'] = $key;
			$i++;
		}
		foreach($selectKey as &$val){
			$val['index'] = $field_key[$val['index']];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $val['index'] = $field_key[$val['index']]+1;
			$selectKey2[$val['index']] = $val;
		}
		foreach($selectKey2 as $val){
			$index[] = $val['index'];
		}


		foreach($arrK as $k=>$vm){
			$cm_select[] = $field_key[$k];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $cm_select[] = $field_key[$k]+1;
		}


		foreach($cm_select as $vm){
			foreach($arrData as &$val){
				$tt = $selectKey2[$vm][$val[$vm]];
				$val[$vm] = $tt;
			}
		}
		//==========================选择框结束==============================



		$sql = "insert into $table($fields) values ";
		$value = "";
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>&$val ){
			//$val[$field_key["phone1"]] = trim($val[$field_key["phone1"]]);
			$val[$field_key["phone1"]] = str_replace("-","",trim($val[$field_key["phone1"]]));
			$val[$field_key["phone2"]] = str_replace("-","",trim($val[$field_key["phone2"]]));
			if( in_array($val[$field_key["phone1"]],$arrDNC) ){
				$black++;
				continue;
			}elseif( $arrExist[$val[$field_key["phone1"]]] == 'Y'){
				$repeat++;
				continue;
			}else{
				if( is_numeric($val[$field_key["phone1"]]) ){
					$valid++;
					$str = "(";
					for($i=0;$i<=($count-2);$i++){
						$str .= "'" .str_replace("'","",$val[$i]). "',";
					}
					$str .= "'" .$import_mark. "',";
					$str .= "'" .date("Y-m-d H:i:s"). "'";
					$str .= ")";

					if($count_rows >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}

					if($systemParam["repeatImportCustomer"] == "phone" || $systemParam["repeatImportCustomer"] == "all"){
						$arrExist[$val[$field_key["phone1"]]] = 'Y';   //成功的号码需要加入到去重数组中
					}
				}else{
					$invalid++;
				}
			}

		}

		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${count_rows}个号码,${black}个在黑名单中,${repeat}个重复号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}


	}


	function importTaskData2(){
		set_time_limit(0);
		@ini_set('memory_limit','512M');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);

		$id = $_GET['id'];
		//黑名单数据检测
		$DNC = new Model('sales_dnc');
		$arrDNC = Array();
		$arrTmp = $DNC->select();
		foreach( $arrTmp AS $row){
			$arrDNC[] = $row['phonenumber'];
		}

		$table = "sales_source_" .$id;
		$source = new Model("sales_source_" .$id);
		//去重
		$arrTmp = $source->field('phone1')->select();
		$arrExist = Array(); //把所有号码放入关联数组，hash
		foreach( $arrTmp AS $row){
			$arrExist[$row['phone1']] = 'Y';
		}

		//导入字段
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
			if($val["text_type"] == "2"){
				$selectField[] = $val["en_name"];
			}
			//==========================选择框开始===============================
			if($val["text_type"] == "2" && $val['list_enabled'] == 'Y'){
				$ttField[] = $val["en_name"];
			}
			//==========================选择框结束===============================
		}
		$count = count($field)+1;
		$fields = "`".implode("`,`",$field)."`".",`createtime`";
		$field_key = array_flip($field);

		//判断文件类型
		$tmp_file = $_FILES["client_phones"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["client_phones"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		if($suffix == "csv"){
			$dst_file = TMP_UPLOAD ."client_phones_".$time.".csv";
			move_uploaded_file($tmp_file,$dst_file);

			$strphones = trim( file_get_contents($dst_file) );
			$arrF = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			array_shift($arrF);
			foreach($arrF as $val){
				$arrData[] = explode(",",rtrim($val,","));
			}
		}else{
			if($suffix == "xls"){
				$dst_f = TMP_UPLOAD ."client_phones_".$time.".xls";
			}
			if($suffix == "xlsx"){
				$dst_f = TMP_UPLOAD ."client_phones_".$time.".xlsx";
			}
			//echo $dst_f;die;  // /tmp/IPPBX_Tmp_Upload/customer_name_1364281330.xls
			move_uploaded_file($tmp_file,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrCustomer = $data->sheets[0];
			array_shift($arrCustomer["cells"]);
			$arrData = $arrCustomer["cells"];
		}
		//$avg = ceil(count($arrData)/8);
		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		//==========================选择框开始===============================
		$selectValue = getSelectCache();
		foreach($selectValue as $key=>$val){
			$selectKey[$key] = array_flip($val);
		}

		foreach($selectKey as $key=>$val){
			if(in_array($key,$ttField)){
				$arrK[$key] = $val;
			}
		}
		$selectKey = $arrK;
		//dump($selectKey);die;
		$selectF = array_flip($selectField);
		$i = 0;
		foreach($selectKey as $key=>$val){
			$selectKey[$key]['index'] = $key;
			$i++;
		}
		foreach($selectKey as &$val){
			$val['index'] = $field_key[$val['index']];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $val['index'] = $field_key[$val['index']]+1;
			$selectKey2[$val['index']] = $val;
		}
		foreach($selectKey2 as $val){
			$index[] = $val['index'];
		}


		foreach($arrK as $k=>$vm){
			$cm_select[] = $field_key[$k];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $cm_select[] = $field_key[$k]+1;
		}


		foreach($cm_select as $vm){
			foreach($arrData as &$val){
				$tt = $selectKey2[$vm][$val[$vm]];
				$val[$vm] = $tt;
			}
		}
		//==========================选择框结束==============================

		//dump($arrData);die;

		$sql = "insert into $table($fields) values ";
		$value = "";
		$total = $black = $repeat = $valid = $invalid = 0;
		if($suffix == "csv"){
			foreach( $arrData AS $key=>&$val ){
				//$val[$field_key["phone1"]] = trim($val[$field_key["phone1"]]);
				$val[$field_key["phone1"]] = str_replace("-","",trim($val[$field_key["phone1"]]));
				$val[$field_key["phone2"]] = str_replace("-","",trim($val[$field_key["phone2"]]));
				if( in_array($val[$field_key["phone1"]],$arrDNC) ){
					$black++;
					continue;
				}elseif( $arrExist[$val[$field_key["phone1"]]] == 'Y'){
					$repeat++;
					continue;
				}else{
					if( is_numeric($val[$field_key["phone1"]]) ){
						$valid++;
						$str = "(";
						for($i=0;$i<=($count-2);$i++){
							$str .= "'" .str_replace("'","",$val[$i]). "',";
						}
						$str .= "'" .date("Y-m-d H:i:s"). "'";
						$str .= ")";

						if($count_rows >=8){
							if($key>$avg && $key<=$avg*2){
								$value2 .= empty($value2)?$str:",$str";
							}elseif($key>$avg*2 && $key<=$avg*3){
								$value3 .= empty($value3)?$str:",$str";
							}elseif($key>$avg*3 && $key<=$avg*4){
								$value4 .= empty($value4)?$str:",$str";
							}elseif($key>$avg*4 && $key<=$avg*5){
								$value5 .= empty($value5)?$str:",$str";
							}elseif($key>$avg*5 && $key<=$avg*6){
								$value6.= empty($value6)?$str:",$str";
							}elseif($key>$avg*6 && $key<=$avg*7){
								$value7 .= empty($value7)?$str:",$str";
							}elseif($key>$avg*7 && $key<=$avg*8){
								$value8 .= empty($value8)?$str:",$str";
							}elseif($key>$avg*8 && $key<=$avg*9){
								$value9 .= empty($value9)?$str:",$str";
							}elseif($key<=$avg){
								$value .= empty($value)?$str:",$str";
							}
						}else{
							$value .= empty($value)?$str:",$str";
						}

						$arrExist[$val[$field_key["phone1"]]] = 'Y';   //成功的号码需要加入到去重数组中
					}else{
						$invalid++;
					}
				}

			}
		}else{
			foreach( $arrData AS $key=>&$val ){
				//$val[$field_key["phone1"]] = trim($val[$field_key["phone1"]]);
				$val[$field_key["phone1"]+1] = str_replace("-","",trim($val[$field_key["phone1"]+1]));
				$val[$field_key["phone2"]+1] = str_replace("-","",trim($val[$field_key["phone2"]+1]));
				if( in_array($val[$field_key["phone1"]+1],$arrDNC) ){
					$black++;
					continue;
				}elseif( $arrExist[$val[$field_key["phone1"]+1]] == 'Y'){
					$repeat++;
					continue;
				}else{
					if( is_numeric($val[$field_key["phone1"]+1]) ){
						$valid++;
						$str = "(";
						for($i=0;$i<=($count-2);$i++){
							$str .= "'" .str_replace("'","",$val[$i]). "',";
						}
						$str .= "'" .date("Y-m-d H:i:s"). "'";
						$str .= ")";

						if($count_rows >=8){
							if($key>$avg && $key<=$avg*2){
								$value2 .= empty($value2)?$str:",$str";
							}elseif($key>$avg*2 && $key<=$avg*3){
								$value3 .= empty($value3)?$str:",$str";
							}elseif($key>$avg*3 && $key<=$avg*4){
								$value4 .= empty($value4)?$str:",$str";
							}elseif($key>$avg*4 && $key<=$avg*5){
								$value5 .= empty($value5)?$str:",$str";
							}elseif($key>$avg*5 && $key<=$avg*6){
								$value6.= empty($value6)?$str:",$str";
							}elseif($key>$avg*6 && $key<=$avg*7){
								$value7 .= empty($value7)?$str:",$str";
							}elseif($key>$avg*7 && $key<=$avg*8){
								$value8 .= empty($value8)?$str:",$str";
							}elseif($key>$avg*8 && $key<=$avg*9){
								$value9 .= empty($value9)?$str:",$str";
							}elseif($key<=$avg){
								$value .= empty($value)?$str:",$str";
							}
						}else{
							$value .= empty($value)?$str:",$str";
						}

						$arrExist[$val[$field_key["phone1"]+1]] = 'Y';   //成功的号码需要加入到去重数组中
					}else{
						$invalid++;
					}
				}

			}
		}
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${count_rows}个号码,${black}个在黑名单中,${repeat}个重复号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}


	}











	//导入呼入平台客户资料
	function importCustomerData(){
		set_time_limit(0);
		@ini_set('memory_limit','-1');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);

		$workname = $_REQUEST['workname'];
		$import_mark = $_REQUEST['import_mark'];
		$table = "customer";
		$source = new Model("customer");

		//去重
		$systemParam = readS();
		$arrExist = Array(); //把所有电话号码放入关联数组，hash
		$arrQQExist = Array(); //把所有QQ号码放入关联数组，hash
		if($systemParam["repeatImportCustomer"] == "phone" || $systemParam["repeatImportCustomer"] == "all"){
			$arrTmp = $source->field('phone1,qq_number')->select();
			foreach( $arrTmp AS $row){
				$arrExist[$row['phone1']] = 'Y';
				if($systemParam["repeatImportCustomer"] == "qq" || $systemParam["repeatImportCustomer"] == "all"){
					$arrQQExist[$row['qq_number']] = 'Y';
				}
			}
		}

		//导入字段
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser' && $val['en_name'] != 'visit_type' && $val['en_name'] != 'fenpei_status' && $val['en_name'] != 'import_mark'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
			if($val["text_type"] == "2"){
				$selectField[] = $val["en_name"];
			}
			//==========================选择框开始===============================
			if($val["text_type"] == "2" && $val['list_enabled'] == 'Y'){
				$ttField[] = $val["en_name"];
			}
			//==========================选择框结束===============================
		}
		$count = count($field)+1;    //看$arrData下标是从0还是1开始的 从0开始的的 用 $count相加的个数比$fields后添加的字段个数少1个;
		$fields = "`".implode("`,`",$field)."`".",`createtime`,`createuser`,import_mark,`dept_id`";

		$field_key = array_flip($field);

		//判断文件类型
		$tmp_file = $_FILES["customer_name"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		if($suffix == "csv"){
			$dst_file = TMP_UPLOAD ."customer_name_".$time.".csv";
			move_uploaded_file($tmp_file,$dst_file);

			$strphones = trim( file_get_contents($dst_file) );
			$arrF = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			array_shift($arrF);
			foreach($arrF as $val){
				$arrData[] = explode(",",rtrim($val,","));
			}
		}else{
			vendor("PHPExcel176.PHPExcel");
			//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
			$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
			$cacheSettings = array();
			PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);

			$objPHPExcel = new PHPExcel();
			$objPHPExcel = PHPExcel_IOFactory::load($_FILES["customer_name"]["tmp_name"]);

			$arrData = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组
			array_shift($arrData);   //去掉第
		}
		//$avg = ceil(count($arrData)/8);
		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		//==========================选择框开始===============================
		$selectValue = getSelectCache();
		foreach($selectValue as $key=>$val){
			$selectKey[$key] = array_flip($val);
		}

		foreach($selectKey as $key=>$val){
			if(in_array($key,$ttField)){
				$arrK[$key] = $val;
			}
		}
		$selectKey = $arrK;
		//dump($selectKey);die;
		$selectF = array_flip($selectField);
		$i = 0;
		foreach($selectKey as $key=>$val){
			$selectKey[$key]['index'] = $key;
			$i++;
		}
		foreach($selectKey as &$val){
			$val['index'] = $field_key[$val['index']];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $val['index'] = $field_key[$val['index']]+1;
			$selectKey2[$val['index']] = $val;
		}
		foreach($selectKey2 as $val){
			$index[] = $val['index'];
		}


		foreach($arrK as $k=>$vm){
			$cm_select[] = $field_key[$k];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $cm_select[] = $field_key[$k]+1;
		}


		foreach($cm_select as $vm){
			foreach($arrData as &$val){
				$tt = $selectKey2[$vm][$val[$vm]];
				$val[$vm] = $tt;
			}
		}
		//==========================选择框结束==============================


		//=========================平均分配开始==============================
		if($workname){
			$arrWorkN = explode(",",$workname);
			foreach($arrWorkN as &$val){
				if($val){
					$workname_arr[] = $val;
				}
			}
			$count_user = count($workname_arr);
			$user_avg = floor($count_rows/$count_user);
		}
		//=========================平均分配结束===============================


		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];
		//$d_id = $_SESSION['user_info']['d_id'];

		$sql = "insert into $table($fields) values ";
		$value = "";
		$username = $_SESSION['user_info']['username'];
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>&$val ){
			//$val[$field_key["phone1"]] = trim($val[$field_key["phone1"]]);
			$val[$field_key["phone1"]] = str_replace("-","",trim($val[$field_key["phone1"]]));
			$val[$field_key["phone2"]] = str_replace("-","",trim($val[$field_key["phone2"]]));

			if($workname){
				//$val["workname"] = $workname;

				//=========================平均分配开始==============================
				//$yushu[] = ($key%$user_avg)%$count_user;
				$yushu = (floor($key/$user_avg))%$count_user;
				$val["workname"] = $workname_arr[$yushu];
				//=========================平均分配结束===============================
			}else{
				$val["workname"] = $username;
			}
			$val["dept_id"] = $deptId_user[$val["workname"]];


			if( $arrQQExist[$val[$field_key["qq_number"]]] == 'Y' && $val[$field_key["qq_number"]]){
				$black++;   //QQ号码重复的
				continue;
			}elseif( $arrExist[$val[$field_key["phone1"]]] == 'Y' ){
				$repeat++;	 //电话号码重复的
				continue;
			}else{
				if( is_numeric($val[$field_key["phone1"]]) ){
					$valid++;
					$str = "(";
					for($i=0;$i<=($count-2);$i++){     //看$arrData下标是从0还是1开始的 从1开始的的 用 $i=1;
						$str .= "'" .str_replace("'","",$val[$i]). "',";
					}
					$str .= "'" .date("Y-m-d H:i:s"). "',";
					$str .= "'" .$val["workname"]. "',";
					$str .= "'" .$import_mark. "',";
					$str .= "'" .$val["dept_id"]. "'";
					$str .= ")";

					if($count_rows >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}
					if($systemParam["repeatImportCustomer"] == "phone" || $systemParam["repeatImportCustomer"] == "all"){
						$arrExist[$val[$field_key["phone1"]]] = 'Y';   //成功的号码需要加入到去重数组中
					}
					if($systemParam["repeatImportCustomer"] == "qq" || $systemParam["repeatImportCustomer"] == "all"){
						$arrQQExist[$val[$field_key["qq_number"]]] = 'Y';   //成功的号码需要加入到去重数组中
					}
				}else{
					$invalid++;
				}
			}

		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);
		dump($value9);
		dump($arrData);
		die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${count_rows}个号码,${black}个QQ号码重复的,${repeat}个重复电话号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}


	}



	//导入呼入平台客户资料
	function importCustomerData2(){
		set_time_limit(0);
		@ini_set('memory_limit','-1');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);

		$workname = $_REQUEST['workname'];
		$table = "customer";
		$source = new Model("customer");

		//去重
		$systemParam = readS();
		$arrExist = Array(); //把所有电话号码放入关联数组，hash
		$arrQQExist = Array(); //把所有QQ号码放入关联数组，hash

		if($systemParam["repeatImportCustomer"] == "phone" || $systemParam["repeatImportCustomer"] == "all"){
			$arrTmp = $source->field('phone1,qq_number')->select();
			foreach( $arrTmp AS $row){
				$arrExist[$row['phone1']] = 'Y';
				if($systemParam["repeatImportCustomer"] == "qq" || $systemParam["repeatImportCustomer"] == "all"){
					$arrQQExist[$row['qq_number']] = 'Y';
				}
			}
		}

		//导入字段
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
			if($val["text_type"] == "2"){
				$selectField[] = $val["en_name"];
			}
			//==========================选择框开始===============================
			if($val["text_type"] == "2" && $val['list_enabled'] == 'Y'){
				$ttField[] = $val["en_name"];
			}
			//==========================选择框结束===============================
		}
		$count = count($field)+2;    //看$arrData下标是从0还是1开始的 从1开始的的 用 $count相加的个数比$fields后添加的字段个数一样多;
		$fields = "`".implode("`,`",$field)."`".",`createtime`,`createuser`,`dept_id`";

		$field_key = array_flip($field);

		//判断文件类型
		$tmp_file = $_FILES["customer_name"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		$time = time();

		if($suffix == "csv"){
			$dst_file = TMP_UPLOAD ."customer_name_".$time.".csv";
			move_uploaded_file($tmp_file,$dst_file);

			$strphones = trim( file_get_contents($dst_file) );
			$arrF = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			array_shift($arrF);
			foreach($arrF as $val){
				$arrData[] = explode(",",rtrim($val,","));
			}
		}else{
			if($suffix == "xls"){
				$dst_f = TMP_UPLOAD ."customer_name_".$time.".xls";
			}
			if($suffix == "xlsx"){
				$dst_f = TMP_UPLOAD ."customer_name_".$time.".xlsx";
			}
			//echo $dst_f;die;  // /tmp/IPPBX_Tmp_Upload/customer_name_1364281330.xls
			move_uploaded_file($tmp_file,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrCustomer = $data->sheets[0];
			array_shift($arrCustomer["cells"]);
			$arrData = $arrCustomer["cells"];

		}
		//$avg = ceil(count($arrData)/8);
		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		//==========================选择框开始===============================
		$selectValue = getSelectCache();
		foreach($selectValue as $key=>$val){
			$selectKey[$key] = array_flip($val);
		}

		foreach($selectKey as $key=>$val){
			if(in_array($key,$ttField)){
				$arrK[$key] = $val;
			}
		}
		$selectKey = $arrK;
		//dump($selectKey);die;
		$selectF = array_flip($selectField);
		$i = 0;
		foreach($selectKey as $key=>$val){
			$selectKey[$key]['index'] = $key;
			$i++;
		}
		foreach($selectKey as &$val){
			$val['index'] = $field_key[$val['index']]+1;  //看$arrData下标是从0还是1开始的 从0开始的的 用 $val['index'] = $field_key[$val['index']];
			$selectKey2[$val['index']] = $val;
		}
		foreach($selectKey2 as $val){
			$index[] = $val['index'];
		}


		foreach($arrK as $k=>$vm){
			$cm_select[] = $field_key[$k]+1;  //看$arrData下标是从0还是1开始的 从0开始的的 用 $cm_select[] = $field_key[$k];
		}


		foreach($cm_select as $vm){
			foreach($arrData as &$val){
				$tt = $selectKey2[$vm][$val[$vm]];
				$val[$vm] = $tt;
			}
		}
		//==========================选择框结束==============================

		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];
		//$d_id = $_SESSION['user_info']['d_id'];

		$sql = "insert into $table($fields) values ";
		$value = "";
		$username = $_SESSION['user_info']['username'];
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>&$val ){
			//$val[$field_key["phone1"]] = trim($val[$field_key["phone1"]]);
			$val[$field_key["phone1"]+1] = str_replace("-","",trim($val[$field_key["phone1"]+1]));
			$val[$field_key["phone2"]+1] = str_replace("-","",trim($val[$field_key["phone2"]+1]));

			if($workname){
				$val["workname"] = $workname;
			}else{
				$val["workname"] = $username;
			}
			$val["dept_id"] = $deptId_user[$val["workname"]];


			if( $arrQQExist[$val[$field_key["qq_number"]+1]] == 'Y' && $val[$field_key["qq_number"]+1]){
				$black++;   //QQ号码重复的
				continue;
			}elseif( $arrExist[$val[$field_key["phone1"]+1]] == 'Y' ){
				$repeat++;	 //电话号码重复的
				continue;
			}else{
				if( is_numeric($val[$field_key["phone1"]+1]) ){
					$valid++;
					$str = "(";
					for($i=1;$i<=($count-2);$i++){       //看$arrData下标是从0还是1开始的 从0开始的的 用 $i=0;
						$str .= "'" .str_replace("'","",$val[$i]). "',";
					}
					$str .= "'" .date("Y-m-d H:i:s"). "',";
					$str .= "'" .$val["workname"]. "',";
					$str .= "'" .$val["dept_id"]. "'";
					$str .= ")";

					if($count_rows >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}
					if($systemParam["repeatImportCustomer"] == "phone" || $systemParam["repeatImportCustomer"] == "all"){
						$arrExist[$val[$field_key["phone1"]+1]] = 'Y';   //成功的号码需要加入到去重数组中
					}
					if($systemParam["repeatImportCustomer"] == "qq" || $systemParam["repeatImportCustomer"] == "all"){
						$arrQQExist[$val[$field_key["qq_number"]+1]] = 'Y';   //成功的号码需要加入到去重数组中
					}
				}else{
					$invalid++;
				}
			}

		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);
		dump($value9);
		dump($arrData);
		die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${count_rows}个号码,${black}个QQ号码重复的,${repeat}个重复电话号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}


	}


	//导入呼入平台客户资料--多选-平均分配
	function importCustomerDataMultiple(){
		set_time_limit(0);
		@ini_set('memory_limit','-1');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);

		$workname = $_REQUEST['workname'];
		$table = "customer";
		$source = new Model("customer");

		//去重
		$systemParam = readS();
		$arrExist = Array(); //把所有电话号码放入关联数组，hash
		$arrQQExist = Array(); //把所有QQ号码放入关联数组，hash

		if($systemParam["repeatImportCustomer"] == "phone" || $systemParam["repeatImportCustomer"] == "all"){
			$arrTmp = $source->field('phone1,qq_number')->select();
			foreach( $arrTmp AS $row){
				$arrExist[$row['phone1']] = 'Y';
				if($systemParam["repeatImportCustomer"] == "qq" || $systemParam["repeatImportCustomer"] == "all"){
					$arrQQExist[$row['qq_number']] = 'Y';
				}
			}
		}

		//导入字段
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				if($val['en_name'] != 'createuser'){
					$field[] = $val['en_name'];
					$title[] = $val['cn_name'];
				}
			}
			if($val["text_type"] == "2"){
				$selectField[] = $val["en_name"];
			}
			//==========================选择框开始===============================
			if($val["text_type"] == "2" && $val['list_enabled'] == 'Y'){
				$ttField[] = $val["en_name"];
			}
			//==========================选择框结束===============================
		}
		$count = count($field)+2;    //看$arrData下标是从0还是1开始的 从1开始的的 用 $count相加的个数比$fields后添加的字段个数一样多;
		$fields = "`".implode("`,`",$field)."`".",`createtime`,`createuser`,`dept_id`";

		$field_key = array_flip($field);

		//判断文件类型
		$tmp_file = $_FILES["customer_name"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		$time = time();

		if($suffix == "csv"){
			$dst_file = TMP_UPLOAD ."customer_name_".$time.".csv";
			move_uploaded_file($tmp_file,$dst_file);

			$strphones = trim( file_get_contents($dst_file) );
			$arrF = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			array_shift($arrF);
			foreach($arrF as $val){
				$arrData[] = explode(",",rtrim($val,","));
			}
		}else{
			if($suffix == "xls"){
				$dst_f = TMP_UPLOAD ."customer_name_".$time.".xls";
			}
			if($suffix == "xlsx"){
				$dst_f = TMP_UPLOAD ."customer_name_".$time.".xlsx";
			}
			//echo $dst_f;die;  // /tmp/IPPBX_Tmp_Upload/customer_name_1364281330.xls
			move_uploaded_file($tmp_file,$dst_f);
			Vendor('phpExcelReader.phpExcelReader');
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('utf8');
			$data->read($dst_f);
			//dump($data->sheets[0]);die;
			$arrCustomer = $data->sheets[0];
			array_shift($arrCustomer["cells"]);
			$arrData = $arrCustomer["cells"];

		}
		//$avg = ceil(count($arrData)/8);
		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		//==========================选择框开始===============================
		$selectValue = getSelectCache();
		foreach($selectValue as $key=>$val){
			$selectKey[$key] = array_flip($val);
		}

		foreach($selectKey as $key=>$val){
			if(in_array($key,$ttField)){
				$arrK[$key] = $val;
			}
		}
		$selectKey = $arrK;
		//dump($selectKey);die;
		$selectF = array_flip($selectField);
		$i = 0;
		foreach($selectKey as $key=>$val){
			$selectKey[$key]['index'] = $key;
			$i++;
		}
		foreach($selectKey as &$val){
			$val['index'] = $field_key[$val['index']]+1;  //看$arrData下标是从0还是1开始的 从0开始的的 用 $val['index'] = $field_key[$val['index']];
			$selectKey2[$val['index']] = $val;
		}
		foreach($selectKey2 as $val){
			$index[] = $val['index'];
		}


		foreach($arrK as $k=>$vm){
			$cm_select[] = $field_key[$k]+1;  //看$arrData下标是从0还是1开始的 从0开始的的 用 $cm_select[] = $field_key[$k];
		}


		foreach($cm_select as $vm){
			foreach($arrData as &$val){
				$tt = $selectKey2[$vm][$val[$vm]];
				$val[$vm] = $tt;
			}
		}
		//==========================选择框结束==============================

		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];
		//$d_id = $_SESSION['user_info']['d_id'];


		//=========================平均分配开始==============================
		if($workname){
			$arrWorkN = explode(",",$workname);
			foreach($arrWorkN as &$val){
				if($val){
					$workname_arr[] = $val;
				}
			}
			$count_user = count($workname_arr);
			$user_avg = floor($count_rows/$count_user);
		}
		//=========================平均分配结束===============================

		$sql = "insert into $table($fields) values ";
		$value = "";
		$username = $_SESSION['user_info']['username'];
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>&$val ){
			//$val[$field_key["phone1"]] = trim($val[$field_key["phone1"]]);
			$val[$field_key["phone1"]+1] = str_replace("-","",trim($val[$field_key["phone1"]+1]));
			$val[$field_key["phone2"]+1] = str_replace("-","",trim($val[$field_key["phone2"]+1]));

			if($workname){
				//$val["workname"] = $workname;

				//=========================平均分配开始==============================
				//$yushu[] = ($key%$user_avg)%$count_user;
				$yushu = (floor($key/$user_avg))%$count_user;
				$val["workname"] = $workname_arr[$yushu];
				//=========================平均分配结束===============================

			}else{
				$val["workname"] = $username;
			}
			$val["dept_id"] = $deptId_user[$val["workname"]];


			if( $arrQQExist[$val[$field_key["qq_number"]+1]] == 'Y' && $val[$field_key["qq_number"]+1]){
				$black++;   //QQ号码重复的
				continue;
			}elseif( $arrExist[$val[$field_key["phone1"]+1]] == 'Y' ){
				$repeat++;	 //电话号码重复的
				continue;
			}else{
				if( is_numeric($val[$field_key["phone1"]+1]) ){
					$valid++;
					$str = "(";
					for($i=1;$i<=($count-2);$i++){       //看$arrData下标是从0还是1开始的 从0开始的的 用 $i=0;
						$str .= "'" .str_replace("'","",$val[$i]). "',";
					}
					$str .= "'" .date("Y-m-d H:i:s"). "',";
					$str .= "'" .$val["workname"]. "',";
					$str .= "'" .$val["dept_id"]. "'";
					$str .= ")";

					if($count_rows >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}
					if($systemParam["repeatImportCustomer"] == "phone" || $systemParam["repeatImportCustomer"] == "all"){
						$arrExist[$val[$field_key["phone1"]+1]] = 'Y';   //成功的号码需要加入到去重数组中
					}
					if($systemParam["repeatImportCustomer"] == "qq" || $systemParam["repeatImportCustomer"] == "all"){
						$arrQQExist[$val[$field_key["qq_number"]+1]] = 'Y';   //成功的号码需要加入到去重数组中
					}
				}else{
					$invalid++;
				}
			}

		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);
		dump($value9);
		dump($arrData);
		die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${count_rows}个号码,${black}个QQ号码重复的,${repeat}个重复电话号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}


	}

	//导出服务记录
	function exportSerciveData(){
		$username = $_SESSION["user_info"]["username"];

		$createtime_start = $_REQUEST["createtime_start"];
		$createtime_end = $_REQUEST["createtime_end"];
		$seat = $_REQUEST["seat"];
		$name = $_REQUEST["name"];
		$phone = $_REQUEST["phone"];
		$servicename = $_REQUEST["servicename"];
		$status = $_REQUEST["status"];

		$where = "1 ";
		$where .= empty($createtime_start) ? "" : " AND sv.createtime >= '$createtime_start'";
		$where .= empty($createtime_end) ? "" : " AND sv.createtime <= '$createtime_end'";
		$where .= empty($seat) ? "" : " AND sv.seat = '$seat'";
		$where .= empty($name) ? "" : " AND c.name like '%$name%'";
		$where .= empty($phone) ? "" : " AND sv.phone like '%$phone%'";
		$where .= empty($servicename) ? "" : " AND sv.servicetype_id = '$servicename'";
		$where .= empty($status) ? "" : " AND sv.status = '$status'";
		if($username != "admin"){
			$where .= " AND sv.seat = '$username'";
		}

		$servicerecords = new Model("servicerecords");
		$arrData = $servicerecords->order("sv.createtime desc")->table("servicerecords sv")->field("c.name,sv.phone,sv.id,sv.seat,sv.createtime,sv.customer_id,sv.recording,sv.status,st.servicename")->join("customer c  on sv.customer_id = c.id")->join("servicetype st on sv.servicetype_id = st.id")->where($where)->select();
		//dump($arrData);die;

		$field = array("createtime","id","name","phone","seat","servicename","status");
		$title = array("记录时间","工单号","客户名称","客户电话","受理坐席","服务类型","处理状态");
		$count = count($field);
		$excelTiele = "服务记录".date("Y-m-d");

		$this->exportDataFunction($count,$field,$title,$arrData,$excelTiele);
	}


	function exportDataFunction($count,$field,$title,$arrData,$excelTiele){
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type:text/html;charset=UTF-8");
		$filename = iconv("utf-8","gb2312",$excelTiele);
        header("Content-Disposition: attachment;filename=$excelTiele.xls ");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();
        $start_row	=	0;

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
			xlsWriteLabel($start_row,$i,utf2gb($title[$i]));
		}

        $start_row++;
		foreach($arrData as &$val){
			for($j=0;$j<$count;$j++){
				 xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
			}
			$start_row++;
		}
        xlsEOF();
	}


	function exportDataToPHPExcel($count,$field,$title,$arrData,$excelTiele){
		vendor("PHPExcel176.PHPExcel");
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			//$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setWrapText(true);    //自动换行
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  //水平居中
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($arrData as &$val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
			}
			$start_row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$filename = iconv("utf-8","gb2312",$excelTiele);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}



	//导入用户
	function importUsers(){
		set_time_limit(0);
		@ini_set('memory_limit','-1');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);

		vendor("PHPExcel176.PHPExcel");
		//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array();
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($_FILES["user_name"]["tmp_name"]);

		$arrData = $objPHPExcel->getSheet(0)->toArray();
		array_shift($arrData);   //去掉第

		$sql = "insert into users(username,password,extension,extenPass,cn_name,d_id,r_id,extension_type) values ";
		$value = "";

		$deptN = $this->getDeptName();
		$roleN = $this->getRoleName();
		foreach($arrData as &$val){
			$val["dept_name"] = array_pop(explode("|",$val[4]));
			$val["extension"] = substr($val[0],2);

			$val["d_id"] = $deptN[$val["dept_name"]];
			$val["r_id"] = $roleN[$val[3]];
			$arrDept[] = $val["extension"];

			$str = "(";
			$str .= "'" .$val[0]. "',";
			$str .= "'123456',";
			$str .= "'" .$val["extension"]. "',";
			$str .= "'" .$val["extension"]. "',";
			$str .= "'" .$val[1]. "',";
			$str .= "'" .$val["d_id"]. "',";
			$str .= "'" .$val["r_id"]. "',";
			$str .= "'sip'";

			$str .= ")";
			$value .= empty($value)?"$str":",$str";
			//$value2[] = empty($value)?"$str":",$str";

			//注册名 分机号 分机密码 类型
			$this->createExtension($val["extension"],$val["extension"],$val["extension"],"sip");
		}

		$users = M("users");
		if( $value ){
			$sql .= $value;
			$res = $users->execute($sql);
		}

		if($res){
			echo json_encode(array('success'=>true,'msg'=>'导入成功！'));
		} else {
			echo json_encode(array('msg'=>'导入失败！'));
		}

	}

	function getDeptName(){
		$department = M("department");
		$arrD = $department->select();
		foreach($arrD as $val){
			$arrF[$val["d_name"]] = $val["d_id"];
		}
		return $arrF;
	}

	function getRoleName(){
		$role = M("role");
		$arrD = $role->select();
		foreach($arrD as $val){
			$arrF[$val["r_name"]] = $val["r_id"];
		}
		return $arrF;
	}


	//添加单个分机
	function createExtension($Name,$Ext,$Secret,$Tech){

		import('ORG.Pbx.extensionsBatch');
		import('ORG.Pbx.pbxConfig');
		import('ORG.Db.bgDB');

		$pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
		$arrAMP  = $pConfig->leer_configuracion(false);
		$arrAST  = $pConfig->leer_configuracion(false);

		$dsnAsterisk = $arrAMP['AMPDBENGINE']['valor']."://".
					   $arrAMP['AMPDBUSER']['valor']. ":".
					   $arrAMP['AMPDBPASS']['valor']. "@".
					   $arrAMP['AMPDBHOST']['valor']. "/asterisk";

		$pDB = new paloDB($dsnAsterisk);
		$pLoadExtension = new paloSantoLoadExtension($pDB);

		$Context        = "from-internal";

//////////////////////////////////////////////////////////////////////////////////
		// validando para que coja las comillas
		$Outbound_CID = ereg_replace('“', "\"", $Outbound_CID);
		$Outbound_CID = ereg_replace('”', "\"", $Outbound_CID);
//////////////////////////////////////////////////////////////////////////////////
		//Paso 1: creando en la tabla sip

		if(!$pLoadExtension->createTechDevices($Ext,$Secret,$VoiceMail,$Context,$Tech))
		{
			$Messages .= $pLoadExtension->errMsg."<br />";
		}else{

			//Paso 2: creando en la tabla users
			if(!$pLoadExtension->createUsers($Ext,$Name,$VoiceMail,$Direct_DID,$Outbound_CID))
				$Messages .= $pLoadExtension->errMsg."<br />";

			//Paso 3: creando en la tabla devices
			if(!$pLoadExtension->createDevices($Ext,$Tech,$Name))
				$Messages .= $pLoadExtension->errMsg."<br />";

			//Paso 4: creando en el archivo /etc/asterisk/voicemail.conf los voicemails
			if(!$pLoadExtension->writeFileVoiceMail(
				$Ext,$Name,$VoiceMail,$VoiceMail_PW,$VM_Email_Address,
				$VM_Pager_Email_Addr,$VM_Options,$VM_EmailAttachment,$VM_Play_CID,
				$VM_Play_Envelope, $VM_Delete_Vmail)
			  )
				$Messages .= L("Failed to update the voicemail");

			//Paso 5: Configurando el call waiting
			if(!$pLoadExtension->processCallWaiting($Call_Waiting,$Ext))
				$Messages .= L("Failed to update the callwaiting");

			$outboundcid = ereg_replace("\"", "'", $Outbound_CID);
			$outboundcid = ereg_replace("\"", "'", $outboundcid);
			$outboundcid = ereg_replace(" ", "", $outboundcid);
			$data_connection = array('host' => "127.0.0.1", 'user' => "admin", 'password' => AMIAdmin());
			if(!$pLoadExtension->putDataBaseFamily($data_connection, $Ext, $Tech, $Name, $VoiceMail, $outboundcid)){
				$Messages .= $Messages .= L("Failed to update the database of PBX");
			}
			$cont++;
		}
		////////////////////////////////////////////////////////////////////////
		//Paso 7: Escribiendo en tabla incoming
		/*
		if($Direct_DID !== ""){
			if(!$pLoadExtension->createDirect_DID($Ext,$Direct_DID))
			$Messages .= "分机: $Ext - 更新did失败<br />";
		}
				/////////////////////////////////////////////////////////////////////////
		/*
		if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
			$Messages .= $pLoadExtension->errMsg;
			*/
		$result = reloadAdd();
		if($result == false)
			$Messages .= $result;
		/*
		if(!$pLoadExtension->do_reloadAll($data_connection, $arrAST, $arrAMP))
			$Messages .= $pLoadExtension->errMsg;*/

		//$Messages = $Messages? $Messages : "分机添加成功!";
		generateArrConf();//生成缓存信息
		return $Messages;
	}

	//导入资料模板--不带选择框的
	function importUsersTrack(){
		set_time_limit(0);
		@ini_set('memory_limit','-1');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);


		//判断文件类型
		$tmp_file = $_FILES["customer_name"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		$username = $_SESSION['user_info']['username'];
		$table = "users_track_record";
		$field = array('company','pay_amount','pay_date');
		$title = array('公司名称','交费金额','交费时间');
		$fields = "`".implode("`,`",$field)."`".",`create_time`,`create_user`";
		$count = count($field)+1;    //看$arrData下标是从0还是1开始的 从0开始的的 用
		$source = M($table);

		$arrTmp = $source->select();
		$arrPayTime = array();
		foreach($arrTmp as $val){
			$arrPayTime[$val['pay_date']] = 'Y';
		}

		vendor("PHPExcel176.PHPExcel");
		//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array();
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($_FILES["customer_name"]["tmp_name"]);
		$arrData = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组
		array_shift($arrData);   //去掉第

		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		$sql = "insert into $table($fields) values ";
		$value = "";
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>$val ){
			if( $arrPayTime[$val[2]] == 'Y' ){
				$repeat++;	 //电话号码重复的
				continue;
			}else{
				$valid++;
				$str = "(";
				for($i=0;$i<=($count-2);$i++){     //看$arrData下标是从0还是1开始的 从1开始的的 用 $i=1;
					$str .= "'" .str_replace("'","",$val[$i]). "',";
				}
				$str .= "'" .date("Y-m-d H:i:s"). "',";
				$str .= "'" .$username. "'";
				$str .= ")";
				//echo $str;die;
				if($count_rows >=8){
					if($key>$avg && $key<=$avg*2){
						$value2 .= empty($value2)?$str:",$str";
					}elseif($key>$avg*2 && $key<=$avg*3){
						$value3 .= empty($value3)?$str:",$str";
					}elseif($key>$avg*3 && $key<=$avg*4){
						$value4 .= empty($value4)?$str:",$str";
					}elseif($key>$avg*4 && $key<=$avg*5){
						$value5 .= empty($value5)?$str:",$str";
					}elseif($key>$avg*5 && $key<=$avg*6){
						$value6.= empty($value6)?$str:",$str";
					}elseif($key>$avg*6 && $key<=$avg*7){
						$value7 .= empty($value7)?$str:",$str";
					}elseif($key>$avg*7 && $key<=$avg*8){
						$value8 .= empty($value8)?$str:",$str";
					}elseif($key>$avg*8 && $key<=$avg*9){
						$value9 .= empty($value9)?$str:",$str";
					}elseif($key<=$avg){
						$value .= empty($value)?$str:",$str";
					}
				}else{
					$value .= empty($value)?$str:",$str";
				}
				$arrPayTime[$val[2]] == 'Y';
			}
		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);
		dump($value9);
		dump($arrData);
		die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$up_sql = "UPDATE users_track_record u INNER JOIN customer c ON u.company=c.company SET u.customer_id=c.id,u.user_name=c.createuser WHERE u.user_name IS NULL OR u.user_name = ''";
			$up_sql = "UPDATE users_track_record u INNER JOIN customer c ON u.company=c.company SET u.customer_id=c.id,u.user_name=c.createuser ";
			$source->execute($up_sql);
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${count_rows}条数据,最后成功导入${num}条数据！,有${fail}没有导入!"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}
	}

	//导入线路
	function importLineData(){
		set_time_limit(0);
		@ini_set('memory_limit','-1');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);


		//判断文件类型
		$tmp_file = $_FILES["customer_name"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		$username = $_SESSION['user_info']['username'];
		$table = "users_line_record";
		$field = array('line_phone','line_name','company','line_num');
		$title = array('电话号码','落地网关群组','账户名称','线路数量');
		$fields = "`".implode("`,`",$field)."`".",`create_time`,`create_user`";
		$count = count($field)+1;    //看$arrData下标是从0还是1开始的 从0开始的的 用
		$source = M($table);
		$source->execute("truncate table $table");   //清空资源表

		vendor("PHPExcel176.PHPExcel");
		//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array();
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($_FILES["customer_name"]["tmp_name"]);
		$arrData = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组
		array_shift($arrData);   //去掉第

		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		$sql = "insert into $table($fields) values ";
		$value = "";
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>$val ){
			$valid++;
			$str = "(";
			for($i=0;$i<=($count-2);$i++){     //看$arrData下标是从0还是1开始的 从1开始的的 用 $i=1;
				$str .= "'" .str_replace("'","",$val[$i]). "',";
			}
			$str .= "'" .date("Y-m-d H:i:s"). "',";
			$str .= "'" .$username. "'";
			$str .= ")";
			//echo $str;die;
			if($count_rows >=8){
				if($key>$avg && $key<=$avg*2){
					$value2 .= empty($value2)?$str:",$str";
				}elseif($key>$avg*2 && $key<=$avg*3){
					$value3 .= empty($value3)?$str:",$str";
				}elseif($key>$avg*3 && $key<=$avg*4){
					$value4 .= empty($value4)?$str:",$str";
				}elseif($key>$avg*4 && $key<=$avg*5){
					$value5 .= empty($value5)?$str:",$str";
				}elseif($key>$avg*5 && $key<=$avg*6){
					$value6.= empty($value6)?$str:",$str";
				}elseif($key>$avg*6 && $key<=$avg*7){
					$value7 .= empty($value7)?$str:",$str";
				}elseif($key>$avg*7 && $key<=$avg*8){
					$value8 .= empty($value8)?$str:",$str";
				}elseif($key>$avg*8 && $key<=$avg*9){
					$value9 .= empty($value9)?$str:",$str";
				}elseif($key<=$avg){
					$value .= empty($value)?$str:",$str";
				}
			}else{
				$value .= empty($value)?$str:",$str";
			}
		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);
		dump($value9);
		dump($arrData);
		die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$up_sql = "UPDATE users_line_record u INNER JOIN customer c ON u.company=c.company SET u.customer_id=c.id,u.user_name=c.createuser WHERE u.user_name IS NULL OR u.user_name = ''";
			$up_sql = "UPDATE users_line_record u INNER JOIN customer c ON u.company=c.company SET u.customer_id=c.id,u.user_name=c.createuser ";
			$source->execute($up_sql);
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${count_rows}条数据,最后成功导入${num}条数据！,有${fail}没有导入!"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}
	}





	//导入资料模板--不带选择框的
	function importCustomerProfit(){
		set_time_limit(0);
		@ini_set('memory_limit','-1');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);


		//判断文件类型
		$tmp_file = $_FILES["customer_name"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		$username = $_SESSION['user_info']['username'];
		$table = "users_customer_profit";
		$field = array('settlement_date','company','call_num','total_fee','call_time','cost_money','cost_call_time','settlement_account');
		$title = array('结算日期','公司名称','话单数量','费用总计','通话时长','结算成本','结算时长','结算账户');
		$fields = "`".implode("`,`",$field)."`".",`create_time`,`create_user`";
		$count = count($field)+1;    //看$arrData下标是从0还是1开始的 从0开始的的 用
		$source = M($table);

		vendor("PHPExcel176.PHPExcel");
		//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array();
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($_FILES["customer_name"]["tmp_name"]);
		$arrData = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组
		array_shift($arrData);   //去掉第

		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		$sql = "insert into $table($fields) values ";
		$value = "";
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>$val ){
			$valid++;
			$str = "(";
			for($i=0;$i<=($count-2);$i++){     //看$arrData下标是从0还是1开始的 从1开始的的 用 $i=1;
				$str .= "'" .str_replace("'","",$val[$i]). "',";
			}
			$str .= "'" .date("Y-m-d H:i:s"). "',";
			$str .= "'" .$username. "'";
			$str .= ")";
			//echo $str;die;
			if($count_rows >=8){
				if($key>$avg && $key<=$avg*2){
					$value2 .= empty($value2)?$str:",$str";
				}elseif($key>$avg*2 && $key<=$avg*3){
					$value3 .= empty($value3)?$str:",$str";
				}elseif($key>$avg*3 && $key<=$avg*4){
					$value4 .= empty($value4)?$str:",$str";
				}elseif($key>$avg*4 && $key<=$avg*5){
					$value5 .= empty($value5)?$str:",$str";
				}elseif($key>$avg*5 && $key<=$avg*6){
					$value6.= empty($value6)?$str:",$str";
				}elseif($key>$avg*6 && $key<=$avg*7){
					$value7 .= empty($value7)?$str:",$str";
				}elseif($key>$avg*7 && $key<=$avg*8){
					$value8 .= empty($value8)?$str:",$str";
				}elseif($key>$avg*8 && $key<=$avg*9){
					$value9 .= empty($value9)?$str:",$str";
				}elseif($key<=$avg){
					$value .= empty($value)?$str:",$str";
				}
			}else{
				$value .= empty($value)?$str:",$str";
			}
		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);
		dump($value9);
		dump($arrData);
		die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$up_sql = "UPDATE users_track_record u INNER JOIN customer c ON u.company=c.company SET u.customer_id=c.id,u.user_name=c.createuser WHERE u.user_name IS NULL OR u.user_name = ''";
			$up_sql = "UPDATE users_customer_profit u INNER JOIN customer c ON u.company=c.company SET u.customer_id=c.id,u.user_name=c.createuser ";
			$source->execute($up_sql);
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${count_rows}条数据,最后成功导入${num}条数据！,有${fail}没有导入!"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}
	}





	function importDataTemplate($table,$field,$title,$tmp_file,$tmpArr){
		set_time_limit(0);
		@ini_set('memory_limit','-1');

		$tmp_file_path = "/tmp/IPPBX_Tmp_Upload/";
		mkdirs($tmp_file_path);

		$source = new Model("customer");


		$count = count($field)+1;    //看$arrData下标是从0还是1开始的 从0开始的的 用 $count相加的个数比$fields后添加的字段个数少1个;
		$fields = "`".implode("`,`",$field);

		$field_key = array_flip($field);

		//判断文件类型
		//$tmp_file = $_FILES["customer_name"]["tmp_name"];
		//$tmpArr = explode(".",$_FILES["customer_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		if($suffix == "csv"){
			$dst_file = TMP_UPLOAD ."customer_name_".$time.".csv";
			move_uploaded_file($tmp_file,$dst_file);

			$strphones = trim( file_get_contents($dst_file) );
			$arrF = explode("\r\n",iconv('GB2312','UTF-8',$strphones));
			array_shift($arrF);
			foreach($arrF as $val){
				$arrData[] = explode(",",rtrim($val,","));
			}
		}else{
			vendor("PHPExcel176.PHPExcel");
			//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
			$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
			$cacheSettings = array();
			PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);

			$objPHPExcel = new PHPExcel();
			$objPHPExcel = PHPExcel_IOFactory::load($_FILES["customer_name"]["tmp_name"]);

			$arrData = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组
			array_shift($arrData);   //去掉第
		}
		//$avg = ceil(count($arrData)/8);
		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		//==========================选择框开始===============================
		$selectValue = getSelectCache();
		foreach($selectValue as $key=>$val){
			$selectKey[$key] = array_flip($val);
		}

		foreach($selectKey as $key=>$val){
			if(in_array($key,$ttField)){
				$arrK[$key] = $val;
			}
		}
		$selectKey = $arrK;
		//dump($selectKey);die;
		$selectF = array_flip($selectField);
		$i = 0;
		foreach($selectKey as $key=>$val){
			$selectKey[$key]['index'] = $key;
			$i++;
		}
		foreach($selectKey as &$val){
			$val['index'] = $field_key[$val['index']];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $val['index'] = $field_key[$val['index']]+1;
			$selectKey2[$val['index']] = $val;
		}
		foreach($selectKey2 as $val){
			$index[] = $val['index'];
		}


		foreach($arrK as $k=>$vm){
			$cm_select[] = $field_key[$k];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $cm_select[] = $field_key[$k]+1;
		}


		foreach($cm_select as $vm){
			foreach($arrData as &$val){
				$tt = $selectKey2[$vm][$val[$vm]];
				$val[$vm] = $tt;
			}
		}
		//==========================选择框结束==============================


		//=========================平均分配开始==============================
		if($workname){
			$arrWorkN = explode(",",$workname);
			foreach($arrWorkN as &$val){
				if($val){
					$workname_arr[] = $val;
				}
			}
			$count_user = count($workname_arr);
			$user_avg = floor($count_rows/$count_user);
		}
		//=========================平均分配结束===============================


		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];
		//$d_id = $_SESSION['user_info']['d_id'];

		$sql = "insert into $table($fields) values ";
		$value = "";
		$username = $_SESSION['user_info']['username'];
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>&$val ){
			//$val[$field_key["phone1"]] = trim($val[$field_key["phone1"]]);
			$val[$field_key["phone1"]] = str_replace("-","",trim($val[$field_key["phone1"]]));
			$val[$field_key["phone2"]] = str_replace("-","",trim($val[$field_key["phone2"]]));

			if($workname){
				//$val["workname"] = $workname;

				//=========================平均分配开始==============================
				//$yushu[] = ($key%$user_avg)%$count_user;
				$yushu = (floor($key/$user_avg))%$count_user;
				$val["workname"] = $workname_arr[$yushu];
				//=========================平均分配结束===============================
			}else{
				$val["workname"] = $username;
			}
			$val["dept_id"] = $deptId_user[$val["workname"]];


			if( $arrQQExist[$val[$field_key["qq_number"]]] == 'Y' && $val[$field_key["qq_number"]]){
				$black++;   //QQ号码重复的
				continue;
			}elseif( $arrExist[$val[$field_key["phone1"]]] == 'Y' ){
				$repeat++;	 //电话号码重复的
				continue;
			}else{
				if( is_numeric($val[$field_key["phone1"]]) ){
					$valid++;
					$str = "(";
					for($i=0;$i<=($count-2);$i++){     //看$arrData下标是从0还是1开始的 从1开始的的 用 $i=1;
						$str .= "'" .str_replace("'","",$val[$i]). "',";
					}
					$str .= "'" .date("Y-m-d H:i:s"). "',";
					$str .= "'" .$val["workname"]. "',";
					$str .= "'" .$val["dept_id"]. "'";
					$str .= ")";

					if($count_rows >=8){
						if($key>$avg && $key<=$avg*2){
							$value2 .= empty($value2)?$str:",$str";
						}elseif($key>$avg*2 && $key<=$avg*3){
							$value3 .= empty($value3)?$str:",$str";
						}elseif($key>$avg*3 && $key<=$avg*4){
							$value4 .= empty($value4)?$str:",$str";
						}elseif($key>$avg*4 && $key<=$avg*5){
							$value5 .= empty($value5)?$str:",$str";
						}elseif($key>$avg*5 && $key<=$avg*6){
							$value6.= empty($value6)?$str:",$str";
						}elseif($key>$avg*6 && $key<=$avg*7){
							$value7 .= empty($value7)?$str:",$str";
						}elseif($key>$avg*7 && $key<=$avg*8){
							$value8 .= empty($value8)?$str:",$str";
						}elseif($key>$avg*8 && $key<=$avg*9){
							$value9 .= empty($value9)?$str:",$str";
						}elseif($key<=$avg){
							$value .= empty($value)?$str:",$str";
						}
					}else{
						$value .= empty($value)?$str:",$str";
					}
					if($systemParam["repeatImportCustomer"] == "phone" || $systemParam["repeatImportCustomer"] == "all"){
						$arrExist[$val[$field_key["phone1"]]] = 'Y';   //成功的号码需要加入到去重数组中
					}
					if($systemParam["repeatImportCustomer"] == "qq" || $systemParam["repeatImportCustomer"] == "all"){
						$arrQQExist[$val[$field_key["qq_number"]]] = 'Y';   //成功的号码需要加入到去重数组中
					}
				}else{
					$invalid++;
				}
			}

		}
		/*
		dump($value);
		dump($value2);
		dump($value3);
		dump($value4);
		dump($value5);
		dump($value6);
		dump($value7);
		dump($value8);
		dump($value9);
		dump($arrData);
		die;
		*/
		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${count_rows}个号码,${black}个QQ号码重复的,${repeat}个重复电话号码,${invalid}个非法号码，最后成功导入${valid}个号码！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}


	}

	function importOrderLogistics(){
		vendor("PHPExcel176.PHPExcel");
		//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array();
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($_FILES["file_name"]["tmp_name"]);

		$arrData = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组
		array_shift($arrData);   //去掉第

		$mod = M("order_info");
		foreach($arrData as $val){
			$result = $mod->where("phone = '".$val["1"]."' AND (logistics_account is null OR logistics_account = '') ")->save(array("logistics_account"=>$val[2],"shipping_status"=>"1"));
		}
		//dump($arrData);die;
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}


}
?>