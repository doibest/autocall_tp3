<?php
class ModulesAction extends Action{
	function listModules(){
		checkLogin();
		$modules=new Model('module');
		$tpl_modules = Array();
		$arrpid = $modules->where("pid = 0 and modulename <> ''")->select();
		//dump($arrpid);die;
		foreach($arrpid as $rows){
			$moduleid = $rows['id'];
			$submodule = $modules->where("pid=$moduleid and modulename <> ''")->select();
			//dump($submodule);die;
			$tpl_modules[$rows['modulename']]['thisid'] = $rows['id'];
			$tpl_modules[$rows['modulename']]['thispid'] = $rows['pid'];
			$tpl_modules[$rows['modulename']]['thismodule'] = $rows['modulename'];
			$tpl_modules[$rows['modulename']]['thisversion'] = $rows['version'];
			$tpl_modules[$rows['modulename']]['thisenabled'] = $rows['enabled'];
			$tpl_modules[$rows['modulename']]['thisdescription'] = $rows['description'];
			$tpl_modules[$rows['modulename']]['submodule'] = $submodule;
		}
		$this->assign('modules',$tpl_modules);
		//dump($tpl_modules);die;

		//分配增删改的权限
		$menuname = "Modules";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		//dump($priv);die;
		$this->assign("add",$priv['add']);
		$this->assign("Uninstall",$priv['Uninstall']);
		$this->assign("edit",$priv['edit']);

		$this->display();
	}


	function editModules(){
		checkLogin();
		$id = $_GET['id'];
		//dump($id);die;
		if(!empty($id)){
			$this->assign("id",$id);
			$modules = new Model("module");
			$data = $modules->where("id = $id")->find();
			//dump($data);die;
			if($data){
				$this->assign("udata",$data);
				$this->display();
			}else{
				echo L('Record does not exist');
			}
		}else{
			echo L('Record does not exist');
		}
	}

	function deleteModules(){
		//$id = $_POST['id'];
		$id = $_GET['id'];
		$module = M('module');
		$arrTemp = $module->where("id=$id")->find();
		$modulename = $arrTemp['modulename'];
		$pid = $arrTemp['pid'];
		//echo $modulename,"</br>",$pid,"</br>";die;
		/*父模块的移除也要找到子模块的菜单等，因为父模块的移除也要移除子模块的控制器和模板文件*/
		/*子模块的移除也要找到父模块的模块名，因为子模块的私有文件是放在 Private/父模块名/子模块名 文件夹中的*/
		if($pid){//子模块移除
			$this->deleteSubModules($modulename,$id,$pid);
		}else{//父模块的移除
			//echo "execute---here--";die;
			$this->deleteParentModules($modulename,$id);
		}
	}


	function deleteSubModules($modulename,$id,$pid){
		$module = M('module');
		$menu = M('menu');
		//取出该子模块的父模块名字，确定私有文件存放路径
		$arrTemp = $module->where("id=$pid")->find();
		$parentmodulename = $arrTemp['modulename'];
		$path = "/var/www/html/BGCC/Tpl/Private/".$parentmodulename."/".$modulename;
		//dump($parentmodulename);dump($path);die;

		$optStatus = true;
		//找到子模块对应的菜单名，然后根据菜单删除控制器和模版文件
		$arrMenu = $menu->where("moduleclass='$modulename'")->select();
		//dump($arrMenu);die;
		foreach($arrMenu AS $row){
			if($row['url']){
				if( preg_match('/\?m=(.*)&a=(.*)&?/',$row['url'],$matches) ){
					$m = $matches[1];
					$a = $matches[2];
					if($m && $a){
						$controler_file = "/var/www/html/BGCC/Lib/Action/{$m}Action.class.php";
						$template_dir = "/var/www/html/BGCC/Tpl/{$m}";
						$cmd1 = "rm -rf $controler_file $template_dir";//删掉控制器和模版文件
						//dump($cmd1);die;
						if( exec($cmd1) ){
							$optStatus = false;
						}
					}
				}else{
					//说明不是Thinkphp模块化的菜单文件

				}
			}
		}
		$cmd2 = "rm -rf $path";//删掉本模块对应的图片，css，js等文件
		if( exec($cmd2) ){
			$optStatus = false;
		}

		//删除数据库中的相关模块和菜单
		$ret1 = $module->where("id=$id")->delete();
		$ret2 = $menu->where("moduleclass='$modulename'")->delete();
		if( $optStatus === true && $ret1 !== false && $ret2 !== false ){
			echo "ok";
		}else{
			echo "Error";
		}

	}

	function deleteParentModules($modulename,$id){
		$module = M('Module');
		$menu = M('menu');
		//先找到父模块的子模块名和id
		$arrModule = $module->where("pid=$id")->select();
		//dump($arrModule);die;
		foreach( $arrModule AS $row ){
			$this->deleteSubModules($row['modulename'],$row['id'],$id);
		}
		$path = "/var/www/html/BGCC/Tpl/Private/".$modulename;
		//dump($path);die;

		$optStatus = true;
		//找到本模块下的直接子菜单，然后根据菜单删除控制器和模版文件
		$arrMenu = $menu->where("moduleclass='$modulename'")->select();
		//dump($arrMenu);die;
		foreach($arrMenu AS $row){
			if($row['url']){
				if( preg_match('/\?m=(.*)&a=(.*)&?/',$row['url'],$matches) ){
					$m = $matches[1];
					$a = $matches[2];
					//dump($m);dump($a);die;
					if($m && $a){
						$controler_file = "/var/www/html/BGCC/Lib/Action/{$m}Action.class.php";
						$template_dir = "/var/www/html/BGCC/Tpl/{$m}";
						$cmd1 = "rm -rf $controler_file $template_dir";//删掉控制器和模版文件
						if( exec($cmd1) ){
							$optStatus = false;
						}
					}
				}else{
					//说明不是Thinkphp模块化的菜单文件

				}
			}
		}
		$cmd2 = "rm -rf $path";//删掉本模块对应的图片，css，js等文件
		if( exec($cmd2) ){
			$optStatus = false;
		}

		//删掉本模块，并且删掉本模块相关菜单。
		$ret1 = $module->where("id=$id")->delete();
		$ret2 = $menu->where("moduleclass='$modulename'")->delete();
		if( $optStatus === true && $ret1 !== false && $ret2 !== false ){
			echo "ok";
		}else{
			echo "Error";
		}

	}


	function updateModules(){
		$id = $_POST['id'];
		//dump($id);die;
		$modules = new Model("module");
		if($_POST['enabled']){
			//$data['enabled'] == $_POST['enabled'];
			$arrData = Array(
				"enabled"=>$_POST['enabled'],
			);
			$upData = $modules->data($arrData)->where("id=$id")->save();
			//echo $modules->getLastSql();die;
			if(false === $upData){
				echo L('Error');
			}else{
				echo "ok";
			}
		}
	}


	//安装上载的模块
	function installModules(){

		if( !empty($_FILES) ){
			$file = $_FILES['newmodule']['name'];
			if( $file ){
				if (!eregi('.zip', $_FILES['newmodule']['name']) ) {
					goback("只能上传zip文件","index.php?m=Modules&a=addModules","");
				}
				move_uploaded_file($_FILES['newmodule']['tmp_name'],"/var/www/html/BGCC/Runtime/Temp/".$file);
			}else{
				$this->error("File upload failed!");
			}
		}
		/*
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize ='1000000';
		$aa = $upload->savePath='/var/www/html/BGCC/Runtime/Temp/';  //上传路径

		//$upload->saveRule=uniqid;    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('zip','rar');     //准许上传的文件后缀
		//$upload->allowTypes=array('application/zip','application/rar');  //检测mime类型

		if(!$upload->upload()){ // 上传错误提示错误信息
				$message = $upload->getErrorMsg();
				goback($message,"index.php?m=Modules&a=addModules","");
			}else{
				$info=$upload->getUploadFileInfo();
				//dump($info);
		}
		$file = $info[0]["savename"];

		*/
		//文件解压后所在的目录
		$extractToDir = "/tmp/IPPBX_modules/". time();
		if(! is_dir($extractToDir)){
			system("mkdir -p $extractToDir");
		}

		$cmd = "/bin/tar zxf /var/www/html/BGCC/Runtime/Temp/$file -C $extractToDir";
		$ret = exec($cmd);
		if( 0 == $ret){
			$dh = opendir( $extractToDir );
			while(false !== ($dir = readdir($dh))){
				if($dir !== "." || $dir !== ".."){
					break;
				}
			}
			$path = $extractToDir."/".$dir."/";//配置文件目录
			$cfgfile = $path."config.php";
			$arrCfg = require($cfgfile);
			//dump($arrCfg);die;
			//插入数据库
			$module = M("module");
			$menu = M("menu");

			//找到模块父id,构造插入模块的数据
			$parentmodule = $arrCfg['general']['parentmodule'];
			if( $parentmodule ){//二级模块
				$arrTemp = $module->where("modulename='$parentmodule'")->find();
				$module_pid = $arrTemp['id'];
			}else{//顶级模块
				$module_pid = 0;
			}
			$arrAddModule = Array(
				'modulename' => $arrCfg['general']["modulename"],
				'version' => $arrCfg['general']["version"],
				'pid' => $module_pid ,
				'enabled' => $arrCfg['general']["enabled"]=='yes'?'Y':'N',
				'description' => $arrCfg['general']["description"],
			);
			$module->add($arrAddModule);
			if( $module_pid == 0){ //如果是父菜单，判断是否要形成新的一级菜单，以及这个一级菜单的菜单名
				$addMainMenu = $arrCfg['general']["mainmenuname"];
				$arrTmp = $menu->query("SELECT MAX(`order`) AS maxorder FROM menu where pid=0");
				$maxOrder = $arrTmp[0]['maxorder'];
				$arrAddMainMenu = Array(
					'name' => $addMainMenu,
					'moduleclass' => $$arrCfg['general']["modulename"], //这个地方有严重bug，一个模块可能有很多子模块
					'order' => $maxOrder + 2,
					'pid' => $menu_pid ,
				);
				$menu->add($arrAddMenu);
			}

			//构造插入菜单的数据
			$arrTmp = $menu->query("SELECT MAX(`order`) AS maxorder FROM menu");
			$maxOrder = $arrTmp[0]['maxorder'];
			$moduleclass = $arrCfg['general']["modulename"];
			$i = 1;
			foreach( $arrCfg['menu'] AS $val ){
				$parentmenu = $val['parentmenu'];//取得父菜单的名字
				$arrTemp = $menu->where("name='$parentmenu'")->find();
				$menu_pid = $arrTemp['id'];//取得父菜单id，下面插入的时候要用到
				$arrAddMenu = Array(
					'name' => $val["menuname"],
					'moduleclass' => $moduleclass,
					'order' => $maxOrder + 10*$i,
					'url' => $val["url"],
					'pid' => $menu_pid ,
					'icon' => $val["icon"],
					'privilege' => $val["privilege"],
				);
				$menu->add($arrAddMenu);
				$i++;
			}

			//移动文件
			//首先移动控制器
			$cmd1 = "mv $path*.class.php /var/www/html/BGCC/Lib/Action";
			exec($cmd1);

			//其次移动css/js/image文件
			if( $module_pid == 0){ //如果是父菜单
				$cmd2 = "mv $path$moduleclass /var/www/html/BGCC/Tpl/Private";
				exec($cmd2);
			}else{//如果是子菜单
				if( !is_dir("/var/www/html/BGCC/Tpl/Private/$parentmodule")){
					exec("mkdir -p /var/www/html/BGCC/Tpl/Private/$parentmodule");
				}
				$cmd2 = "mv $path$moduleclass /var/www/html/BGCC/Tpl/Private/$parentmodule";
				exec($cmd2);
			}

			//删掉配置文件
			unlink($cfgfile);
			//移动剩下的模版文件
			$cmd3 = "mv $path* /var/www/html/BGCC/Tpl";
			exec($cmd3);

		}else{
			$this->error("Unpack $file failed!");
		}

	}



}
?>
