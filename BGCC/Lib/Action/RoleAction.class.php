<?php
class RoleAction extends Action{
	//显示角色列表的主页面
    function listRoleParam(){
		checkLogin();
        header("Content-Type:text/html; charset=utf-8");
        $strR_id=$_GET['r_id'];
        $this->assign('id',$strR_id);

		$menuname = "Roles";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}
		//dump($priv);die;
		$this->assign("priv",$priv);

		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("IPPBX",$arrAL) ){
			$wj_IPPBX = "Y";
		}else{
			$wj_IPPBX = "N";
		}
		if( in_array("interface",$arrAL) ){
			$wj_interface = "Y";
		}else{
			$wj_interface = "N";
		}

		if($wj_IPPBX == "Y" || $wj_interface == "Y"){
			$this->assign("sys_type","IPPBX");
		}else{
			$this->assign("sys_type","all");
		}

        $this->display("","utf-8");
    }

	function roleData(){
		$role=new Model('role');
        $count=$role->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

        $roleList=$role->order('r_id')->limit($page->firstRow.','.$page->listRows)->select();
		$i = 0;

		$menuname = "Roles";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		//dump($priv["purview"]);die;
		foreach($roleList as $val){
			if($_SESSION['user_info']['username'] == 'admin' || $priv["purview"] == "Y"){
				$roleList[$i]['operating'] = "<a href='javascript:void(0);'  onclick=\"setRoles("."'".$val["r_id"]."'".",'".$val["r_name"]."'".");\">". 设置权限 ."</a>";
			}
			$i++;
		}
		unset($i);
		//dump($roleList);die;
		$rowsList = count($roleList) ? $roleList : false;
		$arrRole["total"] = $count;
		$arrRole["rows"] = $rowsList;

		echo json_encode($arrRole);
	}

    //实现添加角色的功能
    function insertRole(){
        $role=new Model('role');
		$role_count = $role->where("r_name='". $_POST["r_name"] ."'")->count();
		if( $role_count>0 ){
				echo json_encode(array('msg'=>"此角色名已存在！不能重复添加."));
				exit;
		}
		$arrData = Array(
			'r_name'=>$_POST['r_name'],
			'interface'=>$_POST['interface'],
		);
		$result = $role->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'角色添加成功！'));
		} else {
			echo json_encode(array('msg'=>'角色添加失败！'));
		}
    }

    //实现角色修改功能
    function updateRole(){
        $id = $_REQUEST['id'];
		$role=new Model('role');
		$roleF = $role->where("r_id='$id'")->find();
		if($roleF["r_name"] != $_POST["r_name"] ){
			$role_count = $role->where("r_name='". $_POST["r_name"] ."'")->count();
			if( $role_count>0 ){
					echo json_encode(array('msg'=>"此角色名已存在！不能重复添加."));
					exit;
			}
		}
		$arrData = Array(
			'r_name'=>$_POST['r_name'],
			'interface'=>$_POST['interface'],
		);
		$pbx = $role->where("r_id=$id")->find();
		if($pbx['interface'] != "pbx"){
			$result = $role->data($arrData)->where("r_id=$id")->save();
		}else{
			echo json_encode(array('msg'=>'此项不能编辑！'));die;
		}
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
    }

    //删除所选的角色
    function deleteRole(){
		$id = $_REQUEST["id"];

		$users = new Model("users");
		$count = $users->where("r_id in ($id)")->count();
		if($count){
			echo json_encode(array("msg"=>"该角色已有绑定的分机，不能删除！"));
			exit;
		}

		$role=new Model('role');

		$pbx = $role->where("r_id=$id")->find();
		if($pbx['interface'] != "pbx"){
			$result = $role->where("r_id in ($id) and r_name not in ('管理员','admin')")->delete();
		}else{
			echo json_encode(array('msg'=>'此项不能删除！'));die;
		}

		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
    }
	//设置权限
    function showRoleSet(){
		checkLogin();
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$sys_type = $checkRole[2];
		//dump($checkRole);die;
		$menu_id = $this->getModuleType($sys_type);
		$mpid = $menu_id['menu_pid'];
		$sub_id = $menu_id['sub_id'];

		//角色的相关权限
        $r_id=$_GET['r_id'];
        if($r_id){
            $role=new Model('Role');
            $arrRole=$role->where("r_id=$r_id")->find();
            if($arrRole){
				$arrPriv = json_decode( $arrRole['action_list'],true );//权限转换为数组
            }else{
                $this->error('要分配的权限的角色不存在！');
            }
        }else{
            $this->error('权限项不存在！');
        }
		$roleName = $arrRole['r_name'];
		$this->assign("roleName",$roleName);


		$enterprise_number = $_SESSION['user_info']['enterprise_number']; // 租户管理员 或者 admin
		if($enterprise_number !='admin' ){
			$tenants_role = M("bgcrm.tenants_role");
			$arrTR = $tenants_role->table("bgcrm.tenants_role tr")->field("tr.*,t.enterprise_number,t.role_id")->join("bgcrm.tenants t on (tr.r_id = t.role_id)")->where("t.enterprise_number = '$enterprise_number'")->find();
			$arrRole_TENANTS = json_decode($arrTR["action_list"],true);

			//从数据库中读取菜单
			$tpl_menu = Array();
			$menu = new Model("$menu_table");
			//$where = "me.pid=0 AND enabled='Y' AND me.id in($mpid)";
			$where = "me.pid=0 AND enabled='Y' AND me.id in($mpid)";

			$mainMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where($where)->order("`order` ASC")->select();
			//echo $menu->getLastSql();
			//dump($mainMenu);die;
			$i = 0;
			foreach( $mainMenu AS $row ){
			 //如果一级菜单在租户权限中不存在，那么就跳过！【过滤】
			 $hasSubMenu = false;
			 foreach($arrRole_TENANTS[$row['name']] AS $k=>$r){
			   //echo $k,'-->',count($r),"\n";
			   if($r){
				 $hasSubMenu = true;
				 //syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------1111---- '); //调试
				 break;
			   }
			 }
			 if( !$hasSubMenu ){
				//syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------22---- '); //调试
				//syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------22---- ' .$arrRole_TENANTS[$row['name']]); //调试
			   continue;
			 }
			 //如果一级菜单在租户权限中不存在，那么就跳过！-----end

				$tpl_menu[$i] = Array();
				//直接在php代码中处理一切模版显示的内容，这样比在模版中灵活多了
				$mainId = $row['id'];//一级菜单id
				$mainText = $row['name'];//一级菜单标题

				$tpl_menu[$i]['id'] = $mainId;
				$tpl_menu[$i]['state'] = 'closed';     //打开页面时 一级标题全部折叠
				$tpl_menu[$i]['text'] = L($mainText);//翻译
				$tpl_menu[$i]['children'] = Array();

				//$subMenu = $menu->table("crm_public.menu me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join crm_public.module mo on me.moduleclass=mo.modulename")->where("$where_sub AND me.pid=$mainId AND enabled='Y' AND me.id in($sub_id)")->order("`order` ASC")->select();
				$subMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
				//dump($subMenu);die;
				$tpl_menu[$i]['children'] = Array();
				$j = 0;
				foreach($subMenu AS $arrRow){
			   if( empty($arrRole_TENANTS[$row['name']][$arrRow['name']]) ){
				  //syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------4444---- ' .$row['name'] .'-----' .$arrRow['name']); //调试
				  //【过滤】如果2级菜单在租户权限中不存在，那么就跳过！
				 continue;
			   }
					$arrTmp2 = array();//---------------------------------------临时数组
					$subId = $arrRow['id'];//二级菜单id
					$subText = $arrRow['name'];//二级菜单标题

					$arrTmp2['id'] = $subId;
					$arrTmp2['text'] = L($subText);//翻译
					$arrTmp2['state'] = 'closed';   //打开页面时 点击一级标题后 二级标题全部折叠
					$arrTmp2['children'] = Array();

					//菜单的 最小 子权限 列表框的显示
					$k = 0;
					$arrP = explode(',',$arrRow['privilege']);//---------------------------------------临时数组
					foreach($arrP AS $v){
					if( $arrRole_TENANTS[$row['name']][$arrRow['name']][$v]!=='Y' ){  //【过滤】如果3级叶子菜单没给权限，那么就跳过！
					  //syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------5555---- ' .$row['name'] .'-----' .$arrRow['name'] .'-----' .$v); //调试
					  continue;
					}
						if( $arrPriv[$mainText][$subText][$v] == 'Y' ){
							$checked = true;//从【权限数组】中判断
						}else{
							$checked = false;
						}
						$arrTmp3 = array(//---------------------------------------临时数组
							"id" => $v,
							"text" => L($v),//翻译
							"iconCls"=>$v,
							"checked" => $checked,
						);
						$arrTmp2['children'][$k] = $arrTmp3;
						$k++;
					}
					$tpl_menu[$i]['children'][$j] = $arrTmp2;//把子菜单赋值给父菜单
					$j++;
				}
				$i++;
			}

		}else{
			//dump($arrPriv);die;
			//从数据库中读取菜单
			$tpl_menu = Array();
			$menu = new Model("$menu_table");
			$where = "me.pid=0 AND enabled='Y' AND me.id in($mpid)";
			$mainMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where($where)->order("`order` ASC")->select();
			//dump($mainMenu);die;
			$i = 0;
			foreach( $mainMenu AS $row ){
				$tpl_menu[$i] = Array();
				//直接在php代码中处理一切模版显示的内容，这样比在模版中灵活多了
				$mainId = $row['id'];//一级菜单id
				$mainText = $row['name'];//一级菜单标题

				$tpl_menu[$i]['id'] = $mainId;
				$tpl_menu[$i]['state'] = 'closed';     //打开页面时 一级标题全部折叠
				$tpl_menu[$i]['text'] = L($mainText);
				$tpl_menu[$i]['children'] = Array();

				$subMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y' AND me.id in($sub_id)")->order("`order` ASC")->select();
				//dump($subMenu);die;
				$j = 0;
				foreach($subMenu AS $arrRow){
					$arrTmp2 = array();//---------------------------------------临时数组
					$subId = $arrRow['id'];//二级菜单id
					$subText = $arrRow['name'];//二级菜单标题

					$arrTmp2['id'] = $subId;
					$arrTmp2['text'] = L($subText);
					$arrTmp2['state'] = 'closed';   //打开页面时 点击一级标题后 二级标题全部折叠
					$arrTmp2['children'] = Array();

					//最小 子权限 列表框的显示
					$k = 0;
					$arrP = explode(',',$arrRow['privilege']);//---------------------------------------临时数组
					foreach($arrP AS $v){
						//echo $arrPriv[$i]['children'][$j]['children'][$k]["checked"];
						if( $arrPriv[$mainText][$subText][$v] == 'Y' ){
							$checked = true;//从【权限数组】中判断
						}else{
							$checked = false;
						}
						$arrTmp3 = array(//---------------------------------------临时数组
							//"id" => $subId ."_". $v,
							"id" => $v,
							"text" => L($v),//翻译
							"iconCls"=>$v,
							"checked" => $checked,
						);
						$arrTmp2['children'][$k] = $arrTmp3;
						$k++;
					}
					$tpl_menu[$i]['children'][$j] = $arrTmp2;//把子菜单赋值给父菜单
					$j++;
				}
				$i++;
			}
		}
		//dump($tpl_menu);die;
		//echo "<pre>";
		//var_export($tpl_menu);die;
		$strJSON = json_encode( $tpl_menu,true );
		//dump($strJSON);die;
		$this->assign('id',$r_id);
		$this->assign("strJSON",$strJSON);
        $this->display();
    }

	//设置权限
    function showRoleSetBAK20160421(){
		checkLogin();
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$sys_type = $checkRole[2];
		//dump($checkRole);die;
		$menu_id = $this->getModuleType($sys_type);
		$mpid = $menu_id['menu_pid'];
		$sub_id = $menu_id['sub_id'];

		//角色的相关权限
        $r_id=$_GET['r_id'];
        if($r_id){
            $role=new Model('Role');
            $arrRole=$role->where("r_id=$r_id")->find();
            if($arrRole){
				$arrPriv = json_decode( $arrRole['action_list'],true );//权限转换为数组
            }else{
                $this->error('要分配的权限的角色不存在！');
            }
        }else{
            $this->error('权限项不存在！');
        }
		$roleName = $arrRole['r_name'];
		$this->assign("roleName",$roleName);


		$enterprise_number = $_SESSION['user_info']['enterprise_number']; // 租户管理员 或者 admin
		if($enterprise_number !='admin' ){
			$tenants_role = M("bgcrm.tenants_role");
			$arrTR = $tenants_role->table("bgcrm.tenants_role tr")->field("tr.*,t.enterprise_number,t.role_id")->join("bgcrm.tenants t on (tr.r_id = t.role_id)")->where("t.enterprise_number = '$enterprise_number'")->find();
			$arrRole_TENANTS = json_decode($arrTR["action_list"],true);

			//从数据库中读取菜单
			$tpl_menu = Array();
			$menu = new Model("$menu_table");
			//$where = "me.pid=0 AND enabled='Y' AND me.id in($mpid)";
			$where = "me.pid=0 AND enabled='Y' AND me.id in($mpid)";

			$mainMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join crm_public.module mo on me.moduleclass=mo.modulename")->where($where)->order("`order` ASC")->select();
			//echo $menu->getLastSql();
			//dump($mainMenu);die;
			$i = 0;
			foreach( $mainMenu AS $row ){
			 //如果一级菜单在租户权限中不存在，那么就跳过！【过滤】
			 $hasSubMenu = false;
			 foreach($arrRole_TENANTS[$row['name']] AS $k=>$r){
			   //echo $k,'-->',count($r),"\n";
			   if($r){
				 $hasSubMenu = true;
				 //syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------1111---- '); //调试
				 break;
			   }
			 }
			 if( !$hasSubMenu ){
				//syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------22---- '); //调试
				//syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------22---- ' .$arrRole_TENANTS[$row['name']]); //调试
			   continue;
			 }
			 //如果一级菜单在租户权限中不存在，那么就跳过！-----end

				$tpl_menu[$i] = Array();
				//直接在php代码中处理一切模版显示的内容，这样比在模版中灵活多了
				$mainId = $row['id'];//一级菜单id
				$mainText = $row['name'];//一级菜单标题

				$tpl_menu[$i]['id'] = $mainId;
				$tpl_menu[$i]['state'] = 'closed';     //打开页面时 一级标题全部折叠
				$tpl_menu[$i]['text'] = L($mainText);//翻译
				$tpl_menu[$i]['children'] = Array();

				//$subMenu = $menu->table("crm_public.menu me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join crm_public.module mo on me.moduleclass=mo.modulename")->where("$where_sub AND me.pid=$mainId AND enabled='Y' AND me.id in($sub_id)")->order("`order` ASC")->select();
				$subMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join crm_public.module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
				//dump($subMenu);die;
				$tpl_menu[$i]['children'] = Array();
				$j = 0;
				foreach($subMenu AS $arrRow){
			   if( empty($arrRole_TENANTS[$row['name']][$arrRow['name']]) ){
				  //syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------4444---- ' .$row['name'] .'-----' .$arrRow['name']); //调试
				  //【过滤】如果2级菜单在租户权限中不存在，那么就跳过！
				 continue;
			   }
					$arrTmp2 = array();//---------------------------------------临时数组
					$subId = $arrRow['id'];//二级菜单id
					$subText = $arrRow['name'];//二级菜单标题

					$arrTmp2['id'] = $subId;
					$arrTmp2['text'] = L($subText);//翻译
					$arrTmp2['state'] = 'closed';   //打开页面时 点击一级标题后 二级标题全部折叠
					$arrTmp2['children'] = Array();

					//菜单的 最小 子权限 列表框的显示
					$k = 0;
					$arrP = explode(',',$arrRow['privilege']);//---------------------------------------临时数组
					foreach($arrP AS $v){
					if( $arrRole_TENANTS[$row['name']][$arrRow['name']][$v]!=='Y' ){  //【过滤】如果3级叶子菜单没给权限，那么就跳过！
					  //syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'>> --------5555---- ' .$row['name'] .'-----' .$arrRow['name'] .'-----' .$v); //调试
					  continue;
					}
						if( $arrPriv[$mainText][$subText][$v] == 'Y' ){
							$checked = true;//从【权限数组】中判断
						}else{
							$checked = false;
						}
						$arrTmp3 = array(//---------------------------------------临时数组
							"id" => $v,
							"text" => L($v),//翻译
							"iconCls"=>$v,
							"checked" => $checked,
						);
						$arrTmp2['children'][$k] = $arrTmp3;
						$k++;
					}
					$tpl_menu[$i]['children'][$j] = $arrTmp2;//把子菜单赋值给父菜单
					$j++;
				}
				$i++;
			}

		}else{
			//dump($arrPriv);die;
			//从数据库中读取菜单
			$tpl_menu = Array();
			$menu = new Model("$menu_table");
			$where = "me.pid=0 AND enabled='Y' AND me.id in($mpid)";
			$mainMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join crm_public.module mo on me.moduleclass=mo.modulename")->where($where)->order("`order` ASC")->select();
			//dump($mainMenu);die;
			$i = 0;
			foreach( $mainMenu AS $row ){
				$tpl_menu[$i] = Array();
				//直接在php代码中处理一切模版显示的内容，这样比在模版中灵活多了
				$mainId = $row['id'];//一级菜单id
				$mainText = $row['name'];//一级菜单标题

				$tpl_menu[$i]['id'] = $mainId;
				$tpl_menu[$i]['state'] = 'closed';     //打开页面时 一级标题全部折叠
				$tpl_menu[$i]['text'] = L($mainText);
				$tpl_menu[$i]['children'] = Array();

				$subMenu = $menu->table("$menu_table me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join crm_public.module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y' AND me.id in($sub_id)")->order("`order` ASC")->select();
				//dump($subMenu);die;
				$j = 0;
				foreach($subMenu AS $arrRow){
					$arrTmp2 = array();//---------------------------------------临时数组
					$subId = $arrRow['id'];//二级菜单id
					$subText = $arrRow['name'];//二级菜单标题

					$arrTmp2['id'] = $subId;
					$arrTmp2['text'] = L($subText);
					$arrTmp2['state'] = 'closed';   //打开页面时 点击一级标题后 二级标题全部折叠
					$arrTmp2['children'] = Array();

					//最小 子权限 列表框的显示
					$k = 0;
					$arrP = explode(',',$arrRow['privilege']);//---------------------------------------临时数组
					foreach($arrP AS $v){
						//echo $arrPriv[$i]['children'][$j]['children'][$k]["checked"];
						if( $arrPriv[$mainText][$subText][$v] == 'Y' ){
							$checked = true;//从【权限数组】中判断
						}else{
							$checked = false;
						}
						$arrTmp3 = array(//---------------------------------------临时数组
							//"id" => $subId ."_". $v,
							"id" => $v,
							"text" => L($v),//翻译
							"iconCls"=>$v,
							"checked" => $checked,
						);
						$arrTmp2['children'][$k] = $arrTmp3;
						$k++;
					}
					$tpl_menu[$i]['children'][$j] = $arrTmp2;//把子菜单赋值给父菜单
					$j++;
				}
				$i++;
			}
		}
		//dump($tpl_menu);die;
		//echo "<pre>";
		//var_export($tpl_menu);die;
		$strJSON = json_encode( $tpl_menu,true );
		//dump($strJSON);die;
		$this->assign('id',$r_id);
		$this->assign("strJSON",$strJSON);
        $this->display();
    }


	function getModuleType($sys_type){
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		//$sys_type = "outbound,inbound,wechat,pb,ks,wj,wf";
		$arrAL = explode(",",$sys_type);
		$where = "";
		foreach($arrAL as $val){
			$str = "find_in_set('".$val."',sys_info) ";
			$where .= empty($where) ? "$str" : " OR $str";
		}
		//dump($where);die;
		$menu = new Model("$menu_table");
		$arrData = $menu->field("id,name,url,sys_type,pid")->where($where)->select();
		foreach($arrData as $val){
			if($val["pid"] == "0"){
				$arrPid[] = $val["id"];
			}else{
				$arrId[] = $val["id"];
			}
		}
		//dump($arrData);die;
		$str_id = implode(",",$arrId);
		$str_pid = implode(",",$arrPid);
		$arr_menuid = array(
			"sub_id" => $str_id,
			"menu_pid" => $str_pid,
		);
		return $arr_menuid;
	}


	function getModuleTypeBak($sys_type){
		$menu = new Model("menu");
		$arrData = $menu->field("id,name,url,sys_type,pid")->select();

		foreach($arrData as &$val){
			$val["sys_type"] = json_decode($val["sys_type"],true);
			if($sys_type == "outbound"){
				if($val["sys_type"]["outbound"] == "Y"){
					$arr[] = $val;
				}
			}else if($sys_type == "inbound"){
				if($val["sys_type"]["inbound"] == "Y"){
					$arr[] = $val;
				}
			}else if($sys_type == "outbound,inbound"){
				if($val["sys_type"]["inbound"] == "Y" || $val["sys_type"]["outbound"] == "Y"){
					$arr[] = $val;
				}
			}else if($sys_type == "IPPBX" || $sys_type == "interface"){
				if($val["sys_type"]["IPPBX"] == "Y"){
					$arr[] = $val;
				}
			}
		}
		foreach($arr as $val){
			$arrId[] = $val["id"];
			if($val["pid"] == "0"){
				$arrPid[] = $val["id"];
			}
		}
		//dump($arrPid);die;
		$str_id = implode(",",$arrId);
		$str_pid = implode(",",$arrPid);
		$arr_menuid = array(
			"sub_id" => $str_id,
			"menu_pid" => $str_pid,
		);
		return $arr_menuid;
	}


	//设置权限
    function test(){
		checkLogin();
		header("Content-Type:text/html; charset=utf-8");
		$sys_type = "inbound";
		$menu_id = $this->getModuleType($sys_type);
		$mpid = $menu_id['menu_pid'];
		$sub_id = $menu_id['sub_id'];
		//dump($menu_id);die;
		//角色的相关权限
        $r_id=$_GET['r_id'];
        if($r_id){
            $role=new Model('Role');
            $arrRole=$role->where("r_id=$r_id")->find();
            if($arrRole){
				$arrPriv = json_decode( $arrRole['action_list'],true );//权限转换为数组
            }else{
                $this->error('要分配的权限的角色不存在！');
            }
        }else{
            $this->error('权限项不存在！');
        }
		$roleName = $arrRole['r_name'];
		$this->assign("roleName",$roleName);
		//dump($arrPriv);die;
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model('crm_public.menu');
		$mainMenu = $menu->table("crm_public.menu me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join crm_public.module mo on me.moduleclass=mo.modulename")->where("me.pid=0 AND enabled='Y' AND me.id in($mpid)")->order("`order` ASC")->select();
		//dump($mainMenu);die;
		$i = 0;
		foreach( $mainMenu AS $row ){
			$tpl_menu[$i] = Array();
			//直接在php代码中处理一切模版显示的内容，这样比在模版中灵活多了
			$mainId = $row['id'];//一级菜单id
			$mainText = $row['name'];//一级菜单标题

			$tpl_menu[$i]['id'] = $mainId;
			$tpl_menu[$i]['state'] = 'closed';     //打开页面时 一级标题全部折叠
			$tpl_menu[$i]['text'] = L($mainText);
			$tpl_menu[$i]['children'] = Array();

			$subMenu = $menu->table("crm_public.menu me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join crm_public.module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'  AND me.id in($sub_id)")->order("`order` ASC")->select();
			//dump($subMenu);die;
			$j = 0;
			foreach($subMenu AS $arrRow){
				$arrTmp2 = array();//---------------------------------------临时数组
				$subId = $arrRow['id'];//二级菜单id
				$subText = $arrRow['name'];//二级菜单标题

				$arrTmp2['id'] = $subId;
				$arrTmp2['text'] = L($subText);
				$arrTmp2['state'] = 'closed';   //打开页面时 点击一级标题后 二级标题全部折叠
				$arrTmp2['children'] = Array();

				//最小 子权限 列表框的显示
				$k = 0;
				$arrP = explode(',',$arrRow['privilege']);//---------------------------------------临时数组
				foreach($arrP AS $v){
					//echo $arrPriv[$i]['children'][$j]['children'][$k]["checked"];
					if( $arrPriv[$mainText][$subText][$v] == 'Y' ){
						$checked = true;//从【权限数组】中判断
					}else{
						$checked = false;
					}
					$arrTmp3 = array(//---------------------------------------临时数组
						//"id" => $subId ."_". $v,
						"id" => $v,
						"text" => L($v),//翻译
						"iconCls"=>$v,
						"checked" => $checked,
					);
					$arrTmp2['children'][$k] = $arrTmp3;
					$k++;
				}
				$tpl_menu[$i]['children'][$j] = $arrTmp2;//把子菜单赋值给父菜单
				$j++;
			}
			$i++;
		}
		//dump($tpl_menu);die;
		//echo "<pre>";
		//var_export($tpl_menu);die;
		$strJSON = json_encode( $tpl_menu,true );
		//dump($strJSON);die;
		$this->assign('id',$r_id);
		$this->assign("strJSON",$strJSON);
        $this->display();
    }


	/*
	//设置权限
    function showRoleSet(){
		//角色的相关权限
        $r_id=$_GET['r_id'];
        if($r_id){
            $role=new Model('Role');
            $arrRole=$role->where("r_id=$r_id")->find();
            if($arrRole){
				$arrPriv = json_decode( $arrRole['action_list'],true );//权限转换为数组
            }else{
                $this->error('要分配的权限的角色不存在！');
            }
        }else{
            $this->error('权限项不存在！');
        }
		$roleName = $arrRole['r_name'];
		$this->assign("roleName",$roleName);
		//dump($arrPriv);die;
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model('menu');
		$mainMenu = $menu->table("menu me")->field("me.id AS id,name,moduleclass,order,url,me.pid AS pid,privilege")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=0 AND enabled='Y'")->order("`order` ASC")->select();
		//dump($mainMenu);die;
		foreach( $mainMenu AS $row ){
			//直接在php代码中处理一切模版显示的内容，这样比在模版中灵活多了
			$mainId = $row['id'];
			$firstMenu = $row['name'];//一级菜单标题
			$subMenu = $menu->table("menu me")->field("me.id AS id,name,moduleclass,order,url,me.pid AS pid,privilege")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			//dump($subMenu);die;
			$tpl_menu[$firstMenu]['thisMenu'] = $row['name'];//菜单显示名
			$tpl_menu[$firstMenu]['chk_name'] = "mainname_all[]";//一级菜单检查框名字为:"mainname_all"
			$tpl_menu[$firstMenu]['chk_id'] = $row['id'];//检查框id为:"id_菜单id"
			$tpl_menu[$firstMenu]['chk_value'] = $row['name'];//菜单标题
			$tpl_menu[$firstMenu]['chk_checked'] = $arrPriv[$firstMenu]?"checked":"";//检查框状态,如果存在，那么就选中
			//$tpl_menu[$firstMenu]['subMenu'] = $subMenu;
			foreach($subMenu AS $arrRow){
				$secondMenu = $arrRow['name'];//二级菜单标题
				$arrTmp = array();
				$arrTmp['name'] = $arrRow['name'];
				$arrTmp['chk_name'] = "subname_".$row['id'].'[]';//检查框名字为:"name_菜单id",是一租检查框
				$arrTmp['chk_id'] = $arrRow['id'];//检查框id为:"菜单id"
				$arrTmp['chk_value'] = $arrRow['name'];
				$arrTmp['chk_checked'] = $arrPriv[$firstMenu][$secondMenu]?"checked":"";
				//最小 子权限 列表框的显示
				$arrTmp['privilege'] = Array();
				$arrP = explode(',',$arrRow['privilege']);
				//dump($arrP);die;
				foreach($arrP AS $v){
					$arrTmp['privilege']['value'][$v] = $arrPriv[$firstMenu][$secondMenu][$v];
				}
				$arrTmp['privilege']['chk_name'] = "subname_".$arrRow['id'].'[]';

				$arrTmp['hidden_name'] = $arrRow['name'];
				$arrTmp['hidden_value'] = $arrRow['id'];
				$tpl_menu[$firstMenu]['subMenu'][] = $arrTmp;//把子菜单赋值给父菜单
			}

		}
		//dump($tpl_menu);die;
		//$arrjosn = json_encode ( $tpl_menu,true );
		//dump($arrjosn);die;
		$this->assign('id',$r_id);
		$this->assign("menu",$tpl_menu);
        $this->display();
    }
	*/


    //修改权限
    function savePriv(){
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		//dump($_POST);die;
		$r_id = $_POST['r_id'];
		$json = $_POST['json'];
		$arrJSON = json_decode($json,true);
		//构造一张一维数组$menuList: "id" =>"name" 的键值对
		$menuList = Array();
		$menu = M("$menu_table");
		$arrMenu = $menu->table("$menu_table me")->field("me.id AS id,name")->join("left join $module_table mo on me.moduleclass=mo.modulename")->where("enabled='Y'")->order("`order` ASC")->select();
		foreach( $arrMenu As $row ){
			$menuList[$row['id']] = $row['name'];
		}
		$arrPriv = Array();
		$allNoChecked1 = true;//初始化【父菜单】都没选择
		foreach($arrJSON AS $val1){
			$mainId = $val1['id'];//父菜单ID
			$mainName = $menuList[$mainId];
			//$arrPriv[$mainName] = Array();//存储子菜单==============================
			$arrSub = Array();
			$allNoChecked2 = true;//初始化【子菜单】都没选择
			foreach( $val1["children"] AS $val2 ){
				$subId = $val2['id'];//子菜单ID
				$subName = $menuList[$subId];
						//$arrPriv[$mainName] = Array();//========================
						$arrP = Array();
						$allNoChecked3 = true;//初始化【权限】都没有
						foreach( $val2["children"] AS $val3 ){
							if( $val3["checked"] == true ){
								$arrP[$val3['id']] = 'Y';
								$allNoChecked3 = false;
							}
						}
				if( $allNoChecked3 == false && $arrP ){//如果有子菜单选中，都没选中就不要管它
					$arrSub[$subName] = $arrP;
					$allNoChecked2 = false;
				}
			}
			if( $allNoChecked2 == false && $arrSub){//如果二级菜单有选中的，但不一定是全选
				$arrPriv[$mainName] = $arrSub;
				$allNoChecked1 = false;
			}
		}
		//dump($arrPriv);die;
		$strPriv = json_encode($arrPriv);
        $role = M("role");
		$res = $role->where("r_id = '$r_id'")->save( array("action_list"=>$strPriv) );
        if(false !== $res){
            echo "ok";
			//$msg = L("Set successfully");
			//goBack($msg,"index.php?m=Role&a=listRoleParam","");
        }else{
            echo "Error";
        }
    }



	//设置权限
    function showRoleSet2(){
		header("Content-Type:text/html; charset=utf-8");
		checkLogin();
		//$sys_type = "inbound";
		//$sys_type = "outbound,inbound";
		//$sys_type = "IPPBX";
		//$sys_type = "outbound";
		$checkRole = getSysinfo();
		if( strstr($checkRole[2],"wechat") ){
			$wechat = "Y";
			$checkRole[2] = trim(str_replace(",wechat","",$checkRole[2]));
		}else{
			$wechat = "N";
		}
		$sys_type = $checkRole[2];
		//dump($checkRole);die;
		$menu_id = $this->getModuleType($sys_type);
		$mpid = $menu_id['menu_pid'];
		$sub_id = $menu_id['sub_id'];

		//角色的相关权限
        $r_id=$_GET['r_id'];
        if($r_id){
            $role=new Model('Role');
            $arrRole=$role->where("r_id=$r_id")->find();
            if($arrRole){
				$arrPriv = json_decode( $arrRole['action_list'],true );//权限转换为数组
            }else{
                $this->error('要分配的权限的角色不存在！');
            }
        }else{
            $this->error('权限项不存在！');
        }
		$roleName = $arrRole['r_name'];
		$this->assign("roleName",$roleName);
		//dump($arrPriv);die;
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model('menu');
		$where = "me.pid=0 AND enabled='Y' AND me.id in($mpid)";
		if($wechat == "N"){
			$where .= " AND me.moduleclass != 'WeChat'";
		}
		$mainMenu = $menu->table("menu me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join module mo on me.moduleclass=mo.modulename")->where($where)->order("`order` ASC")->select();
		//dump($mainMenu);die;
		$i = 0;
		foreach( $mainMenu AS $row ){
			$tpl_menu[$i] = Array();
			//直接在php代码中处理一切模版显示的内容，这样比在模版中灵活多了
			$mainId = $row['id'];//一级菜单id
			$mainText = $row['name'];//一级菜单标题

			$tpl_menu[$i]['id'] = $mainId;
			$tpl_menu[$i]['state'] = 'closed';     //打开页面时 一级标题全部折叠
			$tpl_menu[$i]['text'] = L($mainText);
			$tpl_menu[$i]['children'] = Array();

			$subMenu = $menu->table("menu me")->field("me.id AS id,name,moduleclass,order,me.pid AS pid,privilege")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y' AND me.id in($sub_id)")->order("`order` ASC")->select();
			//dump($subMenu);die;
			$j = 0;
			foreach($subMenu AS $arrRow){
				$arrTmp2 = array();//---------------------------------------临时数组
				$subId = $arrRow['id'];//二级菜单id
				$subText = $arrRow['name'];//二级菜单标题

				$arrTmp2['id'] = $subId;
				$arrTmp2['text'] = L($subText);
				$arrTmp2['state'] = 'closed';   //打开页面时 点击一级标题后 二级标题全部折叠
				$arrTmp2['children'] = Array();

				//最小 子权限 列表框的显示
				$k = 0;
				$arrP = explode(',',$arrRow['privilege']);//---------------------------------------临时数组
				foreach($arrP AS $v){
					//echo $arrPriv[$i]['children'][$j]['children'][$k]["checked"];
					if( $arrPriv[$mainText][$subText][$v] == 'Y' ){
						$checked = true;//从【权限数组】中判断
					}else{
						$checked = false;
					}
					$arrTmp3 = array(//---------------------------------------临时数组
						//"id" => $subId ."_". $v,
						"id" => $v,
						"text" => L($v),//翻译
						"iconCls"=>$v,
						"checked" => $checked,
					);

					//这里有改动----只能看到分配的菜单
					if($arrTmp3["checked"] == true){
						$arrTmp2['children'][] = $arrTmp3;
					}
					$k++;
				}
				$tpl_menu[$i]['children'][$j] = $arrTmp2;//把子菜单赋值给父菜单
				$j++;
			}
			$i++;
		}

		//添加下面的东西
		foreach($tpl_menu as $key=>&$val){
			$arrChild = $val["children"];
			foreach($arrChild as $k=>$vm){
				if(count($vm["children"]) == "0"){
					unset($arrChild[$k]);
				}else{
					$arrChild[$k] = $vm;
					$arrChildPriv[] = $vm;
				}
				$val["children"] = $arrChildPriv;
			}
			unset($arrChildPriv);

			if(count($val["children"]) == "0"){
				unset($tpl_menu[$key]);
			}else{
				$arr_menu[] = $val;
			}
		}


		//dump($arrChild);
		//dump($arr_menu);die;
		//echo "<pre>";
		//var_export($arr_menu);die;
		$strJSON = json_encode( $arr_menu,true );
		//dump($strJSON);die;
		$this->assign('id',$r_id);
		$this->assign("strJSON",$strJSON);
        $this->display();
    }



}
?>
