<?php
//物流信息
class ExpressAction extends Action{
	//获取物流信息
	function getLogisticsInfo(){
		$logistics_account = $_REQUEST["logistics_account"];
		$logistics_state = $_REQUEST["logistics_state"];

		if($logistics_state != "3" || $logistics_state != "6"){
			import('ORG.Express.Express');
			$express = new Express();
			$arrData = $express -> getorder($logistics_account);
			$state = $arrData["state"];
			$arrInfo = $arrData["data"];
			foreach($arrInfo as $val){
				//$arrF[] = "时间: ".$val["time"]." &nbsp;&nbsp;&nbsp;&nbsp;地点和跟踪进度:".$val["context"];
				$arrF[] = $val["time"]." &nbsp;&nbsp;|&nbsp;&nbsp;".$val["context"];
			}
			$info = implode("<br>",$arrF);
			//dump($arrData);die;
			if($info){
				$result = $this->updateOrderInfo($logistics_account,$state,$info);
				if ($result !== false){
					echo json_encode(array('success'=>true,'msg'=>"更新成功！",'data'=>$info));
				} else {
					echo json_encode(array('msg'=>'更新失败！'));
				}
			} else {
				echo json_encode(array('msg'=>$arrData["message"]));
			}
		}else{
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		}

	}

	//改变订单信息
	function updateOrderInfo($logistics_account,$state,$info){
		$order_info = M("order_info");
		$arrD = array(
			"logistics_state"=>$state,
			"logistics_Info"=>$info,
		);
		if($state == "3"){
			//收货确认
			$arrD["shipping_status"] = "2";
			$arrD["receipttime"] = date("Y-m-d H:i:s");

			//付款
			$arrD["pay_status"] = "Y";
			$arrD["payment"] = "3";     //付款方式--货到付款
			$arrD["paytime"] = date("Y-m-d H:i:s");
		}

		if($state == "6"){
			$arrD["order_status"] = "4";  //退货
			$arrD["pay_status"] = "N";  //未付款
			$arrD["shipping_status"] = "N";  //未发货
			$arrD["canceltime"] = date("Y-m-d H:i:s"); //退货时间
			$arrD["description4"] = "快递100接口推送改变状态";  //操作备注
			$arrD["refund4"] = "";  //退款方式
			$arrD["explanation4"] = "快递100接口推送改变状态";  //退款说明

		}

		$result = $order_info->data($arrD)->where("logistics_account = '$logistics_account'")->save();

		if ($result !== false){
			return true;
		} else {
			return false;
		}

	}

}
?>
