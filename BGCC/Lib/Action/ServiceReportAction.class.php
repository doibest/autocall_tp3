<?php
//服务报表
class ServiceReportAction extends Action{
	function serviceList(){
		checkLogin();
		$currentDate = date("Y-m-d H:i:s");
		$this->assign("currentDate",$currentDate);

		//分配增删改的权限
		$menuname = "Service Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function serviceData(){
		$search_type = $_REQUEST["search_type"];
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_start = Date('Y-m-d',$_GET['ts_start'])." 00:00:00";
			$date_end = Date('Y-m-d',$_GET['ts_end'])." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1))." 00:00:00";
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";

		if($username != "admin"){
			$where = " AND username in ($deptUser)";
		}else{
			$where = " ";
		}

		$serp = new Model("asteriskcdrdb.report_feedback");
		$sql = "SELECT username,SUM(CASE WHEN score='1' THEN 1 ELSE 0 END) AS very_satisfied,SUM(CASE WHEN score='2' THEN 1 ELSE 0 END) AS satisfied,SUM(CASE WHEN score='3' THEN 1 ELSE 0 END) AS general, SUM(CASE WHEN score='4' THEN 1 ELSE 0 END) AS not_satisfied FROM asteriskcdrdb.report_feedback where calltime >= '$date_start' AND calltime <= '$date_end'  $where GROUP BY username ORDER BY username DESC";

		$serverData = $serp->query($sql);
		//echo $serp->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$i = 0;
		foreach($serverData as $val){
			$serverData[$i]['cn_name'] = $cnName[$val["username"]];
			$i++;
		}
		//dump($serverData);die;


		if($search_type == "xls"){
			$arrField = array('username','cn_name','very_satisfied','satisfied','general','not_satisfied');
			$arrTitle = array('工号','姓名','非常满意','满意','一般','不满意');
			$xls_count = count($arrField);
			$excelTiele = "服务评分报表".date("Y-m-d");
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$serverData,$excelTiele);
			die;
		}


		$count = count($serverData);

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($serverData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$serviceData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($serverData AS &$v){
			if($i >= $start && $j < $length){
				$serviceData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}

		$rowsList = count($serviceData) ? $serviceData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		if($date_start){
			$ary["date_start"] = $date_start;
		}
		if($date_end){
			$ary["date_end"] = $date_end;
		}

		echo json_encode($ary);


	}



    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
}

?>
