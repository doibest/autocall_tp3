<?php
class ServiceSettingAction extends Action{
	function serviceList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Service Settings";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function serviceDataList(){
		$service = new Model("servicetype");
		import('ORG.Util.Page');
		$count = $service->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$serviceData = $service->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($serviceData) ? $serviceData : false;
		$arrservice["total"] = $count;
		$arrservice["rows"] = $rowsList;

		echo json_encode($arrservice);
	}

	function insertservice(){
		$service = new Model("servicetype");
		$arrData = array(
			"servicename"=>$_REQUEST["servicename"],
		);
		$result = $service->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function updateservice(){
		$id = $_REQUEST["id"];
		$service = new Model("servicetype");
		$arrData = array(
			"servicename"=>$_REQUEST["servicename"],
		);
		$result = $service->data($arrData)->where("id = $id")->save();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function deleteservice(){
		$id = $_REQUEST["id"];
		$service = new Model("servicetype");
		$result = $service->where("id = $id")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}
}

?>
