<?php
class MembersAction extends Action{
	//会员列表
	function membersList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Members";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function membersData(){
		$username = $_SESSION['user_info']['username'];
		$customer = new Model("customer");

		$name = $_REQUEST["name"];
		$sex = $_REQUEST["sex"];
		$phone1 = $_REQUEST["phone1"];
		$member_Level = $_REQUEST["member_Level"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		/*
		$where .= " AND (integral is not null OR stored_value is not null) ";
		$where .= " AND (integral != '' OR stored_value  != '') ";
		*/
		//$where .= " AND integral is not null AND integral != ''  ";
		$where .= " AND integral is not null AND member_Level != '0'";
		$where .= empty($name) ? "" : " AND `name` like '%$name%'";
		$where .= empty($sex) ? "" : " AND `sex` = '$sex'";
		$where .= empty($phone1) ? "" : " AND `phone1` like '%$phone1%'";
		$where .= empty($member_Level) ? "" : " AND `member_Level` = '$member_Level'";

		$count = $customer->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		if($search_type == "xls"){
			$arrData = $customer->field("id,name,phone1,sex,integral,stored_value,member_Level")->where($where)->select();  //,stored_frozen
		}else{
			$arrData = $customer->field("id,name,phone1,sex,integral,stored_value,member_Level")->limit($page->firstRow.','.$page->listRows)->where($where)->select();  //,stored_frozen
		}

		$sex_row = array('man'=>'男','woman'=>'女');
		//$stored_frozen_row = array('Y'=>'是','N'=>'否');
		$groups_row = $this->memberLevel();
		foreach($arrData as &$val){
			$sex = $sex_row[$val['sex']];
			$val['sex'] = $sex;

			$val["member_Level"] = $groups_row[$val["member_Level"]];
			//$val["stored_frozen"] = $stored_frozen_row[$val["stored_frozen"]];
		}


		if($search_type == "xls"){
			$arrField = array('name','sex','phone1','integral','member_Level');
			$arrTitle = array('姓名','性别','号码','积分','会员等级');
			$xls_count = count($arrField);
			$excelTiele = "会员".date("Y-m-d");
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//会员等级设置
	function membershipLevelList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Membership Level";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function membershipLevelData(){
		$username = $_SESSION['user_info']['username'];
		$membership_level = new Model("membership_level");
		$count = $membership_level->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $membership_level->limit($page->firstRow.','.$page->listRows)->select();
		$groups_row = $this->memberLevel();
		//dump($groups_row);die;
		foreach($arrData as &$val){
			$val["groups2"] = $groups_row[$val["groups"]];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	function memberLevel(){
		$customer_fields = new Model("customer_fields");
		$fieldData = $customer_fields->order("field_order asc")->where("en_name = 'member_Level'")->find();
		$selectTpl = json_decode($fieldData["field_values"] ,true);

		foreach($selectTpl as $vm){
			$arrF[$vm[0]] = $vm[1];
		}
		$arrF["failure"] = "不成单";
		return $arrF;
	}

	function insertMemberLevel(){
		$username = $_SESSION['user_info']['username'];
		$membership_level = M("membership_level");
		$arrData = array(
			'start_num'=>$_REQUEST['start_num'],
			'end_num'=>$_REQUEST['end_num'],
			'groups'=>$_REQUEST['groups'],
		);
		$result = $membership_level->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateMemberLevel(){
		$id = $_REQUEST['id'];
		$membership_level = M("membership_level");
		$arrData = array(
			'start_num'=>$_REQUEST['start_num'],
			'end_num'=>$_REQUEST['end_num'],
			'groups'=>$_REQUEST['groups'],
		);
		$result = $membership_level->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteMemberLevel(){
		$id = $_REQUEST["id"];
		$membership_level = M("membership_level");
		$result = $membership_level->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function updateVIP(){
		$customer = M("customer");
		$arrData = $customer->field("id,name,phone1,sex,integral,stored_value,member_Level")->where("member_Level<0")->select();

		foreach($arrData as $val){
			$result = $this->saveIntegral($val["id"]);
		}

		if($result !== false){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'失败'));
		}
	}


	//用户积分
	function saveIntegral($customer_id){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$order_info = new Model("order_info");
		$arrF = $order_info->field("sum(goods_amount) as goods_amount,id")->where("customer_id = '$customer_id' AND logistics_state = '3'")->find();
		$integral = $arrF["goods_amount"];
		if(!$integral || $integral == "0.00"){
			$integral = "0";
		}
		//echo $order_info->getLastSql();

		$membership_level = M("membership_level");
		$arrML = $membership_level->where(" start_num <= '$integral' AND end_num >= '$integral'")->find();
		$groups = $arrML["groups"];
		if(!$arrML){
			if(!$integral){
				$groups = "failure";
			}else{
				$arrML2 = $membership_level->where("end_num = '0'")->find();
				$groups = $arrML2["groups"];
			}
		}

		$customer = M("customer");
		$arrData = array(
			"integral"=>$integral,
			"member_Level"=>$groups,
		);
		$res = $customer->data($arrData)->where("id = '$customer_id'")->save();
		return true;
	}

}

?>
