<?php
/*
报表管理
*/
class ReportManagerAction extends Action{
	//业绩 接通率 时长  日报表
	function performanceDayReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Performance Day Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$this->assign("start_time",$start_time);
		$this->assign("end_time",$end_time);

		$this->display();
	}

	function performanceDayData(){

		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$user_name = $_REQUEST["user_name"];
		$dept_id = $_REQUEST['dept_id'];
		$search_type = $_REQUEST["search_type"];

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$arrDep = getDepTreeData();
		$deptst = getMeAndSubDeptID($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$deptst2 = getMeAndSubDeptID($arrDep,$d_id);  //取上级部门
		$deptSet2 = rtrim($deptst2,",");

		if(!$dept_id){
			$deptId = $deptSet2;
		}else{
			$deptId = $deptSet;
		}
		//dump($deptId);die;
		/*
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";
		//echo $deptUser;die;
		*/

		$order_info = M("order_info");
		$fields = "createname,dept_id,count(*) as total_order_num,
		sum(cope_money) as total_cope_money,
		sum(case when order_status = '1' then 1 else 0 end) as actual_order_num,
		sum(if(order_status = '1',`cope_money`,0)) as actual_cope_money,
		sum(case when order_status != '1' then 1 else 0 end) as audit_order_num,
		sum(if(order_status != '1',`cope_money`,0)) as audit_cope_money
		";
		/*
		sum(case when order_status = '3' then 1 else 0 end) as audit_order_num,
		sum(if(order_status = '3',`cope_money`,0)) as audit_cope_money
		*/

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND createtime >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND createtime <= '$end_time'";
		$where .= empty($user_name) ? "" : " AND createname = '$user_name'";

		if($username != "admin" || $dept_id){
			//$where .= " AND createname in ($deptUser)";
			$where .= " AND dept_id in ($deptId)";
		}

		$arrCount = $order_info->Distinct(true)->field("createname")->where($where)->select();
		$count = count($arrCount);
		//echo $order_info->getLastSql();
		//dump($arrCount);
		//$count = $order_info->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($search_type == "xls"){
			$arrData = $order_info->order("createname asc")->field($fields)->group("createname")->where($where)->select();
		}else{
			$arrData = $order_info->order("createname asc")->field($fields)->limit($page->firstRow.','.$page->listRows)->where($where)->group("createname")->select();
		}
		//echo $order_info->getLastSql();
		//dump($arrData);die;

		$cdr = M("asteriskcdrdb.cdr");
		$where1 = "1 ";
		$where1 .= empty($start_time) ? "" : " AND calldate >= '$start_time'";
		$where1 .= empty($end_time) ? "" : " AND calldate <= '$end_time'";
		$where1 .= empty($user_name) ? "" : " AND workno = '$user_name'";
		if($username != "admin" || $dept_id){
			//$where1 .= " AND workno in ($deptUser)";
			$where1 .= " AND dept_id in ($deptId)";
		}
		$fields_cdr = "workno,
			sum(case when calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') then 1 else 0 end) as total_out_call,
			sum(case when calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') AND disposition = 'ANSWERED' then 1 else 0 end) as answered_out_call,
			sum(case when calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') AND disposition != 'ANSWERED' then 1 else 0 end) as noanswer_out_call,
			sum(if(calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT'),`billsec`,0)) as total_billsec
		";
		$arrCD = $cdr->field($fields_cdr)->where($where1)->group("workno")->select();
		//echo $cdr->getLastSql();die;
		foreach($arrCD as $key=>&$val){
			$val["avg_billsec"] = ceil($val["total_billsec"]/$val["total_out_call"]);
			$val["out_connection_rate2"] = ($val["answered_out_call"]/$val["total_out_call"])*100;
			$val["out_connection_rate"] = round($val["out_connection_rate2"],2)."%";
			$val["total_billsec"] = secendHMS($val["total_billsec"]);
			$val["avg_billsec"] = secendHMS($val["avg_billsec"]);
			$arrWk[$val["workno"]] = $val;
		}



		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];
		$deptId_name = $userArr["deptId_name"];
		foreach($arrData as $key=>&$val){
			$val["cn_name"] = $cnName[$val["createname"]];
			$val["dept_name"] = $deptId_name[$val["dept_id"]];

			foreach($arrWk[$val["createname"]] as $k=>$v) {
			  $val[$k] = $v;;
			}

		}
		//dump($arrData);die;


		$arrFT1 = $order_info->order("createname asc")->field($fields)->where($where)->find();
		//echo $order_info->getLastSql();die;
		$arrFT2 = $cdr->field($fields_cdr)->where($where1)->find();
		$arrFooter = array_merge($arrFT1,$arrFT2);
		$arrFooter["avg_billsec"] = ceil($arrFooter["total_billsec"]/$arrFooter["total_out_call"]);
		$arrFooter["total_billsec"] = secendHMS($arrFooter["total_billsec"]);
		$arrFooter["avg_billsec"] = secendHMS($arrFooter["avg_billsec"]);
		$arrFooter["createname"] = "总计";
		$arrFooter2[0] = $arrFooter;
		//dump($arrFT1);
		//dump($arrFooter2);die;

		if($search_type == "xls"){
			$arrField = array('createname','cn_name','total_order_num','total_cope_money','actual_order_num','actual_cope_money','audit_order_num','audit_cope_money','total_out_call','answered_out_call','noanswer_out_call','out_connection_rate','total_billsec','avg_billsec');
			$arrTitle = array('工号','姓名','总订单数','总销售金额','实际销售单数','实际销售金额','其它订单数','其它订单金额','外呼总数','外呼接通数','外呼未接数','外呼接通率','呼出总时长','平均时长');
			$xls_count = count($arrField);
			$excelTiele = "业绩-接通率-时长-日报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}
		//dump($arrData);die;





		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($start_time){
			$arrT["start_time"] = $start_time;
		}
		if($end_time){
			$arrT["end_time"] = $end_time;
		}
		$arrT["footer"] = $arrFooter2;

		echo json_encode($arrT);
	}


	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }

	//业绩详细报表
	function performanceDetailReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Performance Detail Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$this->assign("start_time",$start_time);
		$this->assign("end_time",$end_time);

		$this->display();
	}

	function performanceDetail(){
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$user_name = $_REQUEST["user_name"];
		$dept_id = $_REQUEST['dept_id'];
		$search_type = $_REQUEST["search_type"];

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$arrDep = getDepTreeData();
		$deptst = getMeAndSubDeptID($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$deptst2 = getMeAndSubDeptID($arrDep,$d_id);  //取上级部门
		$deptSet2 = rtrim($deptst2,",");

		if(!$dept_id){
			$deptId = $deptSet2;
		}else{
			$deptId = $deptSet;
		}

		$order_info = M("order_info");

		$fields = "createname,dept_id,count(*) as total_order_num,
			sum(cope_money) as total_cope_money,
			sum(case when order_status = '1' then 1 else 0 end) as order_num_1,
			sum(if(order_status = '1',`cope_money`,0)) as cope_money_1,
			sum(case when order_status = '3' then 1 else 0 end) as order_num_3,
			sum(if(order_status = '3',`cope_money`,0)) as cope_money_3,
			sum(case when order_status = '8' then 1 else 0 end) as order_num_8,
			sum(if(order_status = '8',`cope_money`,0)) as cope_money_8,
			sum(case when order_status = '7' then 1 else 0 end) as order_num_7,
			sum(if(order_status = '7',`cope_money`,0)) as cope_money_7,
			sum(case when order_status = '2' then 1 else 0 end) as order_num_2,
			sum(if(order_status = '2',`cope_money`,0)) as cope_money_2,
			sum(case when logistics_state = '6' then 1 else 0 end) as send_back_num,
			sum(cope_money) as cope_money_total,
			sum(case when shipping_status = '1' then 1 else 0 end) as shipping_status_1,
			sum(if(shipping_status = '1',`cope_money`,0)) as shipping_money_1,
			sum(case when logistics_state = '3' then 1 else 0 end) as order_sign_for_num,
			sum(if(logistics_state = '3',`cope_money`,0)) as sign_for_money
		";

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND createtime >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND createtime <= '$end_time'";
		$where .= empty($user_name) ? "" : " AND createname = '$user_name'";
		if($username != "admin" || $dept_id){
			$where .= " AND dept_id in ($deptId)";
		}
		$arrCount = $order_info->Distinct(true)->field("createname")->where($where)->select();
		$count = count($arrCount);
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($search_type == "xls"){
			$arrData = $order_info->order("createname asc")->field($fields)->group("createname")->where($where)->select();
		}else{
			$arrData = $order_info->order("createname asc")->field($fields)->limit($page->firstRow.','.$page->listRows)->where($where)->group("createname")->select();
		}
		//echo $order_info->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		//$deptName_user = $userArr["deptName_user"];
		$deptId_name = $userArr["deptId_name"];
		foreach($arrData as $key=>&$val){
			$val["cn_name"] = $cnName[$val["createname"]];
			//$val["dept_name"] = $deptName_user[$val["createname"]];
			$val["dept_name"] = $deptId_name[$val["dept_id"]];

			//实际销售单数/总订单数
			$val["audit_rate2"] = ($val["order_num_1"]/$val["total_order_num"])*100;
			$val["audit_rate"] = round($val["audit_rate2"],2)."%";;  //审核率

			//发货单数/实际销售单数
			$val["delivery_rate2"] = ($val["shipping_status_1"]/$val["order_num_1"])*100;
			$val["delivery_rate"] = round($val["delivery_rate2"],2)."%";;  //发货率

			//签收的订单数/发货单数
			//$val["sign_for_rate2"] = ($val["order_sign_for_num"]/$val["shipping_status_1"])*100;
			//签收的订单数/实际销售单数
			$val["sign_for_rate2"] = ($val["order_sign_for_num"]/$val["order_num_1"])*100;
			$val["sign_for_rate"] = round($val["sign_for_rate2"],2)."%";;  //签收率
		}

		$arrFooter = $order_info->order("createname asc")->field($fields)->where($where)->find();
		$arrFooter["createname"] = "总计";
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array ('createname','cn_name','dept_name','total_order_num','total_cope_money','order_num_1','cope_money_1','order_num_3','cope_money_3','order_num_8','cope_money_8','order_num_7','cope_money_7','order_num_2','cope_money_2','send_back_num','audit_rate','cope_money_total','shipping_status_1','shipping_money_1','delivery_rate','sign_for_rate','order_sign_for_num','sign_for_money');
			$arrTitle = array ('工号','姓名','部门','总订单数','总销售金额','实际销售单数','实际销售金额','待审核订单数','待审核金额','问题单数','问题单金额','未通过单数','未通过金额','取消单数','取消单数金额','物流退回单数','审核率','订单总金额','发货单数','发货金额','发货率','签收率','签收的订单数','签收订单数金额');
			$xls_count = count($arrField);
			$excelTiele = "坐席业绩详细报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($start_time){
			$arrT["start_time"] = $start_time;
		}
		if($end_time){
			$arrT["end_time"] = $end_time;
		}
		$arrT["footer"] = $arrFooter2;
		echo json_encode($arrT);
	}



	//产品销售报表
	function productSalesReports(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Product Sales Reports";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$this->assign("start_time",$start_time);
		$this->assign("end_time",$end_time);

		$this->display();
	}

	function productSalesData(){
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$search_type = $_REQUEST["search_type"];

		$username = $_SESSION['user_info']['username'];
		$order_goods = M("order_goods");

		$fields = "og.goods_id,og.order_id,og.goods_num,g.good_name,g.good_num,
			sum(og.goods_num) as total_good_sales_num,
			sum(og.goods_money) as total_good_sales_money,
			sum(if(o.order_status = '1',og.goods_num,0)) as actual_good_sales_num,
			sum(if(o.order_status = '1',og.goods_money,0)) as actual_good_sales_money,
			sum(if(o.order_status != '1',og.goods_num,0)) as other_good_sales_num,
			sum(if(o.order_status != '1',og.goods_money,0)) as other_good_sales_money

		";

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND o.createtime >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND o.createtime <= '$end_time'";

		//$count = $order_goods->table("order_goods og")->field($fields)->join("order_info o on (og.order_id = o.id)")->join("shop_goods g on (og.goods_id = g.id) ")->where($where)->count();

		$arrCount = $order_goods->Distinct(true)->table("order_goods og")->field("og.goods_id")->join("order_info o on (og.order_id = o.id)")->join("shop_goods g on (og.goods_id = g.id) ")->where($where)->select();
		$count = count($arrCount);

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($search_type == "xls"){
			$arrData = $order_goods->table("order_goods og")->field($fields)->join("order_info o on (og.order_id = o.id)")->join("shop_goods g on (og.goods_id = g.id) ")->where($where)->group("goods_id")->select();
		}else{
			$arrData = $order_goods->table("order_goods og")->field($fields)->join("order_info o on (og.order_id = o.id)")->join("shop_goods g on (og.goods_id = g.id) ")->limit($page->firstRow.','.$page->listRows)->where($where)->group("goods_id")->select();
		}
		//echo $order_goods->getLastSql();die;
		/*
		$fields2 = "g.good_num,
			sum(og.goods_num) as actual_good_sales_num,sum(og.goods_money) as actual_good_sales_money
		";
		*/
		$fields2 = "g.good_num,
			sum(og.goods_num) as total_good_sales_num,
			sum(og.goods_money) as total_good_sales_money,
			sum(if(o.order_status = '1',og.goods_num,0)) as actual_good_sales_num,
			sum(if(o.order_status = '1',og.goods_money,0)) as actual_good_sales_money,
			sum(if(o.order_status != '1',og.goods_num,0)) as other_good_sales_num,
			sum(if(o.order_status != '1',og.goods_money,0)) as other_good_sales_money
		";
		$arrFooter = $order_goods->table("order_goods og")->field($fields2)->join("order_info o on (og.order_id = o.id)")->join("shop_goods g on (og.goods_id = g.id) ")->where($where)->select();
		$arrFooter[0]["good_num"] = "总计";

		if($search_type == "xls"){
			$arrField = array ("good_num","good_name","total_good_sales_num","total_good_sales_money","actual_good_sales_num","actual_good_sales_money","other_good_sales_num","other_good_sales_money");
			$arrTitle = array ("产品编码","产品名称","总销售金额","实际销售个数","实际销售个数","实际销售金额","其它销售个数","其它销售金额");
			$xls_count = count($arrField);
			$excelTiele = "产品销售报表".date("Y-m-d");
			array_push($arrData,$arrFooter[0]);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($start_time){
			$arrT["start_time"] = $start_time;
		}
		if($end_time){
			$arrT["end_time"] = $end_time;
		}
		$arrT["footer"] = $arrFooter;

		echo json_encode($arrT);
	}


	//通话统计报表
	function callStatisticsReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Call Statistics";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$this->assign("start_time",$start_time);
		$this->assign("end_time",$end_time);

		$para_sys = readS();
		$out_call_noanswer = $para_sys["out_call_noanswer"];
		$out_call_answered = $para_sys["out_call_answered"];
		$this->assign("out_call_noanswer",$out_call_noanswer);
		$this->assign("out_call_answered",$out_call_answered);

		$this->display();
	}

	function callStatisticsData(){
		$para_sys = readS();
		$out_call_noanswer = $para_sys["out_call_noanswer"];
		$out_call_answered = $para_sys["out_call_answered"];

		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$user_name = $_REQUEST["user_name"];
		$dept_id = $_REQUEST['dept_id'];
		$search_type = $_REQUEST["search_type"];

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$arrDep = getDepTreeData();
		$deptst = getMeAndSubDeptID($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$deptst2 = getMeAndSubDeptID($arrDep,$d_id);  //取上级部门
		$deptSet2 = rtrim($deptst2,",");

		if(!$dept_id){
			$deptId = $deptSet2;
		}else{
			$deptId = $deptSet;
		}

		$cdr = M("asteriskcdrdb.cdr");

		$fields = "workno,dept_id,count(*) as call_total,
			sum(case when disposition = 'ANSWERED' then 1 else 0 end) as total_answered_num,
			sum(case when disposition != 'ANSWERED' then 1 else 0 end) as total_noanswer_num,
			sum(case when calltype = 'IN' then 1 else 0 end) as in_call_total,
			sum(case when calltype = 'IN' AND disposition = 'ANSWERED' then 1 else 0 end) as in_answered_num,
			sum(case when calltype = 'IN' AND disposition != 'ANSWERED' then 1 else 0 end) as in_noanswer_num,
			sum(case when calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') then 1 else 0 end) as out_call_total,
			sum(case when calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') AND disposition = 'ANSWERED' then 1 else 0 end) as out_answered_num,
			sum(case when calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') AND disposition != 'ANSWERED' then 1 else 0 end) as out_noanswer_num,
			sum(billsec) as call_billsec_total,
			sum(if(calltype = 'IN',`billsec`,0)) as in_billsec_total,
			sum(if(calltype = 'IN' AND disposition = 'ANSWERED',`billsec`,0)) as in_answered_billsec_total,
			sum(if(calltype = 'IN' AND disposition != 'ANSWERED',`billsec`,0)) as in_noanswer_billsec_total,
			sum(if(calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT'),`billsec`,0)) as out_billsec_total,
			sum(if(calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') AND disposition = 'ANSWERED',`billsec`,0)) as out_answered_billsec_total,
			sum(if(calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') AND disposition != 'ANSWERED',`billsec`,0)) as out_noanswer_billsec_total,
			sum(case when calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') AND disposition != 'ANSWERED' AND billsec <= '$out_call_noanswer' then 1 else 0 end) as out_x_noanswer_num,
			sum(case when calltype IN ('AUTOCALL','PREVIEW','PREDICTION','OUT') AND disposition = 'ANSWERED' AND billsec >= '$out_call_answered' then 1 else 0 end) as out_x_answered_num
		";
		/*

			sum(if(calltype = 'OUT' AND disposition != 'ANSWERED' AND billsec <= '$out_call_noanswer',`billsec`,0)) as out_x_noanswer_num,
			sum(if(calltype = 'OUT' AND disposition = 'ANSWERED' AND billsec >= '$out_call_answered',`billsec`,0)) as out_x_answered_num
		*/
		$where = "workno is not null AND workno != '' ";
		//$where = "  AND lastapp = 'Dial' ";
		if($username != "admin" || $dept_id){
			$where .= " AND dept_id in ($deptId)";
		}
		$where .= empty($start_time) ? "" : " AND calldate >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND calldate <= '$end_time'";
		$where .= empty($user_name) ? "" : " AND workno = '$user_name'";

		$arrCount = $cdr->Distinct(true)->field("workno")->where($where)->select();
		$count = count($arrCount);

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($search_type == "xls"){
			$arrData = $cdr->field($fields)->where($where)->group("workno")->select();
		}else{
			$arrData = $cdr->field($fields)->limit($page->firstRow.','.$page->listRows)->where($where)->group("workno")->select();
		}
		//echo $cdr->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];

		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["workno"]];
			$val["dept_name"] = $deptName_user[$val["workno"]];

			$val["total_call_rate2"] = ($val["total_answered_num"]/$val["call_total"])*100;
			$val["total_call_rate"] = round($val["total_call_rate2"],2)."%";;  //总接通率

			$val["in_call_rate2"] = ($val["in_answered_num"]/$val["in_call_total"])*100;
			$val["in_call_rate"] = round($val["in_call_rate2"],2)."%";;  //呼入接通率

			$val["out_call_rate2"] = ($val["out_answered_num"]/$val["out_call_total"])*100;
			$val["out_call_rate"] = round($val["out_call_rate2"],2)."%";;  //外呼接通率

			//外呼有效数（X秒以上）/外呼总数
			$val["out_x_answered_rate2"] = ($val["out_x_answered_num"]/$val["out_call_total"])*100;
			$val["out_x_answered_rate"] = round($val["out_x_answered_rate2"],2)."%";;  //外呼有效率（X秒以上）

			$val["in_billsec_avg"] = ceil($val["in_billsec_total"]/$val["in_call_total"]);     //呼入通话平均时长
			$val["in_billsec_avg"] = secendHMS($val["in_billsec_avg"]);

			$val["out_billsec_avg"] = ceil($val["out_billsec_total"]/$val["out_call_total"]);     //外呼通话平均时长
			$val["out_billsec_avg"] = secendHMS($val["out_billsec_avg"]);

			$val["in_answered_billsec_avg"] = ceil($val["in_answered_billsec_total"]/$val["in_answered_num"]);     //呼入接通平均时长
			$val["in_answered_billsec_avg"] = secendHMS($val["in_answered_billsec_avg"]);

			$val["in_noanswer_billsec_avg"] = ceil($val["in_noanswer_billsec_total"]/$val["in_noanswer_num"]);     //呼入未接平均时长
			$val["in_noanswer_billsec_avg"] = secendHMS($val["in_noanswer_billsec_avg"]);

			$val["out_answered_billsec_avg"] = ceil($val["out_answered_billsec_total"]/$val["out_answered_num"]);     //外呼接通平均时长
			$val["out_answered_billsec_avg"] = secendHMS($val["out_answered_billsec_avg"]);

			$val["out_noanswer_billsec_avg"] = ceil($val["out_noanswer_billsec_total"]/$val["out_noanswer_num"]);     //外呼未接平均时长
			$val["out_noanswer_billsec_avg"] = secendHMS($val["out_noanswer_billsec_avg"]);


			$val["call_billsec_total"] = secendHMS($val["call_billsec_total"]);  //通话总时长
			$val["in_billsec_total"] = secendHMS($val["in_billsec_total"]);  //呼入通话总时长
			$val["out_billsec_total"] = secendHMS($val["out_billsec_total"]);  //外呼通话总时长
			$val["in_answered_billsec_total"] = secendHMS($val["in_answered_billsec_total"]);  //呼入接通总时长
			$val["in_noanswer_billsec_total"] = secendHMS($val["in_noanswer_billsec_total"]);  //呼入未接总时长
			$val["out_answered_billsec_total"] = secendHMS($val["out_answered_billsec_total"]);  //外呼接通总时长
			$val["out_noanswer_billsec_total"] = secendHMS($val["out_noanswer_billsec_total"]);  //外呼未接总时长


		}

		$arrFooter = $cdr->field($fields)->where($where)->select();
		foreach($arrFooter as &$val){
			$val["workno"] = "总计";

			$val["total_call_rate2"] = ($val["total_answered_num"]/$val["call_total"])*100;
			$val["total_call_rate"] = round($val["total_call_rate2"],2)."%";;  //总接通率

			$val["in_call_rate2"] = ($val["in_answered_num"]/$val["in_call_total"])*100;
			$val["in_call_rate"] = round($val["in_call_rate2"],2)."%";;  //呼入接通率

			$val["out_call_rate2"] = ($val["out_answered_num"]/$val["out_call_total"])*100;
			$val["out_call_rate"] = round($val["out_call_rate2"],2)."%";;  //外呼接通率

			//外呼有效数（X秒以上）/外呼总数
			$val["out_x_answered_rate2"] = ($val["out_x_answered_num"]/$val["out_call_total"])*100;
			$val["out_x_answered_rate"] = round($val["out_x_answered_rate2"],2)."%";;  //外呼有效率（X秒以上）

			$val["in_billsec_avg"] = ceil($val["in_billsec_total"]/$val["in_call_total"]);     //呼入通话平均时长
			$val["in_billsec_avg"] = secendHMS($val["in_billsec_avg"]);

			$val["out_billsec_avg"] = ceil($val["out_billsec_total"]/$val["out_call_total"]);     //外呼通话平均时长
			$val["out_billsec_avg"] = secendHMS($val["out_billsec_avg"]);

			$val["in_answered_billsec_avg"] = ceil($val["in_answered_billsec_total"]/$val["in_answered_num"]);     //呼入接通平均时长
			$val["in_answered_billsec_avg"] = secendHMS($val["in_answered_billsec_avg"]);

			$val["in_noanswer_billsec_avg"] = ceil($val["in_noanswer_billsec_total"]/$val["in_noanswer_num"]);     //呼入未接平均时长
			$val["in_noanswer_billsec_avg"] = secendHMS($val["in_noanswer_billsec_avg"]);

			$val["out_answered_billsec_avg"] = ceil($val["out_answered_billsec_total"]/$val["out_answered_num"]);     //外呼接通平均时长
			$val["out_answered_billsec_avg"] = secendHMS($val["out_answered_billsec_avg"]);

			$val["out_noanswer_billsec_avg"] = ceil($val["out_noanswer_billsec_total"]/$val["out_noanswer_num"]);     //外呼未接平均时长
			$val["out_noanswer_billsec_avg"] = secendHMS($val["out_noanswer_billsec_avg"]);


			$val["call_billsec_total"] = secendHMS($val["call_billsec_total"]);  //通话总时长
			$val["in_billsec_total"] = secendHMS($val["in_billsec_total"]);  //呼入通话总时长
			$val["out_billsec_total"] = secendHMS($val["out_billsec_total"]);  //外呼通话总时长
			$val["in_answered_billsec_total"] = secendHMS($val["in_answered_billsec_total"]);  //呼入接通总时长
			$val["in_noanswer_billsec_total"] = secendHMS($val["in_noanswer_billsec_total"]);  //呼入未接总时长
			$val["out_answered_billsec_total"] = secendHMS($val["out_answered_billsec_total"]);  //外呼接通总时长
			$val["out_noanswer_billsec_total"] = secendHMS($val["out_noanswer_billsec_total"]);  //外呼未接总时长


		}

		if($search_type == "xls"){
			$arrField = array ('workno','cn_name','dept_name','call_total','total_answered_num','total_noanswer_num','total_call_rate','in_call_total','in_answered_num','in_noanswer_num','in_call_rate','out_call_total','out_answered_num','out_noanswer_num','out_call_rate','call_billsec_total','in_billsec_total','in_billsec_avg','out_billsec_total','out_billsec_avg','out_x_noanswer_num','out_x_answered_num','out_x_answered_rate','in_answered_billsec_total','in_answered_billsec_avg','in_noanswer_billsec_total','in_noanswer_billsec_avg','out_answered_billsec_total','out_answered_billsec_avg','out_noanswer_billsec_total','out_noanswer_billsec_avg');
			$arrTitle = array ('工号','姓名','部门','通话总数','接通总数','未接总数','总接通率','呼入总数','呼入接通数','呼入未接数','呼入接通率','外呼总数','外呼接通数','外呼未接数','外呼接通率','通话总时长','呼入通话总时长','呼入通话平均时长','外呼通话总时长','外呼通话平均时长','外呼无效数(30秒以内）','外呼有效数（20秒以上）','外呼有效率（20秒以上）','呼入接通总时长','呼入接通平均时长','呼入未接总时长','呼入未接平均时长','外呼接通总时长','外呼接通平均时长','外呼未接总时长','外呼未接平均时长');

			$xls_count = count($arrField);
			$excelTiele = "通话统计报表".date("Y-m-d");
			array_push($arrData,$arrFooter[0]);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($start_time){
			$arrT["start_time"] = $start_time;
		}
		if($end_time){
			$arrT["end_time"] = $end_time;
		}
		$arrT["footer"] = $arrFooter;

		echo json_encode($arrT);
	}


	function test(){
		header("Content-Type:text/html; charset=utf-8");
		$table_test = M("table_test");
		$arrData = $table_test->where("tab_name = '签收报表'")->select();
		foreach($arrData as $val){
			$arrF[] = "<th data-options=\"field:'".$val["en_name"]."',width:".$val["tab_width"]."\">".$val["cn_name"]."</th>";
			$arrField[] = $val["en_name"];
			$arrTitle[] = $val["cn_name"];
		}
		$str = implode("<br>",$arrF);
		var_export($arrField);
		var_export($arrTitle);
		dump($str);die;
	}

	//签收报表
	function signForReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Sign For Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$this->assign("start_time",$start_time);
		$this->assign("end_time",$end_time);

		$arrDF = $this->tableFields();
		$arrF1 = $arrDF["send"];
		$arrF2 = $arrDF["sign_for"];
		$arrF3 = $arrDF["sign_for_rate"];
		$arrF4 = $arrDF["sign_for_money"];
		$arrF5 = $arrDF["sign_send_money"];
		$arrFT = array_merge($arrF1,$arrF2,$arrF3,$arrF4,$arrF5);
		$arrF = $this->array_sort($arrFT,"fields","asc","no");
		$i = 0;
		$fields = "";
		foreach($arrF as $key=>$val){
			$arrField[0][$i]['field'] = $val['fields'];
			$arrField[0][$i]['title'] = $val['display'];
			$arrField[0][$i]['width'] = "115";
			$i++;
		}
		$arrFd = array("field"=>"createname","title"=>"工号","width"=>"80");
		$arrFd1 = array("field"=>"cn_name","title"=>"姓名","width"=>"80");
		$arrFd2 = array("field"=>"dept_name","title"=>"部门","width"=>"80");
		$arrFd3 = array("field"=>"total_send_num","title"=>"总发货数","width"=>"90");
		$arrFd4 = array("field"=>"total_sign_for_num","title"=>"总签收数","width"=>"90");
		$arrFd4 = array("field"=>"total_sign_for_money","title"=>"总签收数金额","width"=>"90");
		$arrFd5 = array("field"=>"total_sign_for_rate","title"=>"总签收率","width"=>"90");
		$arrFd6 = array("field"=>"total_send_money","title"=>"总发货金额","width"=>"90");
		array_unshift($arrField[0],$arrFd5);
		array_unshift($arrField[0],$arrFd4);
		array_unshift($arrField[0],$arrFd6);
		array_unshift($arrField[0],$arrFd3);
		array_unshift($arrField[0],$arrFd2);
		array_unshift($arrField[0],$arrFd1);
		array_unshift($arrField[0],$arrFd);

		//dump($arrField);die;
		$arrFs = json_encode($arrField);
		$this->assign("fieldList",$arrFs);
		//dump($arrF);die;

		$this->display();
	}


	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}
	function tableFields(){
		header("Content-Type:text/html; charset=utf-8");
		$logistics_mode = new Model("logistics_mode");
		$arrData2 = $logistics_mode->field("id as value,logistics_name,com_code")->select();

		foreach($arrData2 as &$val){
			$val["display"] = $val["logistics_name"]."发货数";
			//$val["fields"] = "send_num_".$val["value"];
			$val["fields"] = $val["com_code"]."_send_num";
			$val["name"] = "shopping_name";
			$arrF1[] = $val;
		}
		foreach($arrData2 as &$val){
			$val["display"] = $val["logistics_name"]."签收数";
			//$val["fields"] = "sign_for_num_".$val["value"];
			$val["fields"] = $val["com_code"]."_sign_for_num";
			$val["name"] = "shopping_name";
			$arrF2[] = $val;
		}
		foreach($arrData2 as &$val){
			$val["display"] = $val["logistics_name"]."签收率";
			//$val["fields"] = "sign_for_num_".$val["value"];
			$val["fields"] = $val["com_code"]."_sign_for_rate";
			$val["name"] = "shopping_name";
			$arrF3[] = $val;
		}
		foreach($arrData2 as &$val){
			$val["display"] = $val["logistics_name"]."签收金额";
			//$val["fields"] = "sign_for_num_".$val["value"];
			$val["fields"] = $val["com_code"]."_sign_for_qmoney";
			$val["name"] = "shopping_name";
			$arrF4[] = $val;
		}
		foreach($arrData2 as &$val){
			$val["display"] = $val["logistics_name"]."发货金额";
			//$val["fields"] = "send_num_".$val["value"];
			$val["fields"] = $val["com_code"]."_send_omoney";
			$val["name"] = "shopping_name";
			$arrF5[] = $val;
		}
		$arrD = array(
			"send"=>$arrF1,
			"sign_for"=>$arrF2,
			"sign_for_rate"=>$arrF3,
			"sign_for_money"=>$arrF4,
			"sign_send_money"=>$arrF5,
		);
		//dump($arrD);die;
		return $arrD;
	}

	function signForData(){
		header("Content-Type:text/html; charset=utf-8");
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$user_name = $_REQUEST["user_name"];
		$dept_id = $_REQUEST['dept_id'];
		$search_type = $_REQUEST["search_type"];

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];


		$arrDep = getDepTreeData();
		$deptst = getMeAndSubDeptID($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$deptst2 = getMeAndSubDeptID($arrDep,$d_id);  //取上级部门
		$deptSet2 = rtrim($deptst2,",");

		if(!$dept_id){
			$deptId = $deptSet2;
		}else{
			$deptId = $deptSet;
		}

		$order_info = M("order_info");
		/*
		$fields = "createname,
			sum(case when shopping_name = '1' AND (shipping_status = '1' OR shipping_status = '2') then 1 else 0 end) as sf_send_num,
			sum(case when shopping_name = '2' AND (shipping_status = '1' OR shipping_status = '2') then 1 else 0 end) as ems_send_num,
			sum(case when shopping_name = '1' AND shipping_status = '2' AND logistics_state = '3' then 1 else 0 end) as sf_sign_for_num,
			sum(case when shopping_name = '2' AND shipping_status = '2'  AND logistics_state = '3' then 1 else 0 end) as ems_sign_for_num,
			sum(case when shipping_status = '1' OR shipping_status = '2' then 1 else 0 end) as total_send_num,
			sum(case when logistics_state = '3' then 1 else 0 end) as total_sign_for_num
		";
		*/
		$fields = "";
		$arrF = $this->tableFields();
		foreach($arrF["send"] as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' AND (shipping_status = '1' OR shipping_status = '2') then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
		}

		foreach($arrF["sign_for"] as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' AND shipping_status = '2' AND logistics_state = '3' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
		}
		foreach($arrF["sign_for_money"] as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' AND shipping_status = '2' AND logistics_state = '3' then cope_money else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
		}

		foreach($arrF["sign_send_money"] as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' AND (shipping_status = '1' OR shipping_status = '2') then cope_money else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
		}

		$fields .= " ,createname,dept_id,
			sum(case when shipping_status = '1' OR shipping_status = '2' then 1 else 0 end) as total_send_num,
			sum(case when shipping_status = '1' OR shipping_status = '2' then cope_money else 0 end) as total_send_money,
			sum(case when logistics_state = '3' then cope_money else 0 end) as total_sign_for_money,
			sum(case when logistics_state = '3' then 1 else 0 end) as total_sign_for_num";

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND logistics_time >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND logistics_time <= '$end_time'";
		$where .= empty($user_name) ? "" : " AND createname = '$user_name'";
		if($username != "admin" || $dept_id){
			$where .= " AND dept_id in ($deptId)";
		}
		$arrCount = $order_info->Distinct(true)->field("createname")->where($where)->select();
		$count = count($arrCount);

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];
		$deptId_name = $userArr["deptId_name"];

		if($search_type == "xls"){
			$arrData = $order_info->field($fields)->where($where)->group("createname")->select();
		}else{
			$arrData = $order_info->field($fields)->limit($page->firstRow.','.$page->listRows)->where($where)->group("createname")->select();
		}
		//echo $order_info->getLastSql();die;

		$arrFR = $arrF["sign_for_rate"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["createname"]];
			//$val["dept_name"] = $deptName_user[$val["createname"]];
			$val["dept_name"] = $deptId_name[$val["dept_id"]];

			/*
			$val["sf_send_rate2"] = ($val["sf_sign_for_num"]/$val["sf_send_num"])*100;
			$val["sf_send_rate"] = round($val["sf_send_rate2"],2)."%";;  //SF签收率

			$val["ems_send_rate2"] = ($val["ems_sign_for_num"]/$val["ems_send_num"])*100;
			$val["ems_send_rate"] = round($val["ems_send_rate2"],2)."%";;  //SF签收率
			*/
			$val["total_sign_for_rate2"] = ($val["total_sign_for_num"]/$val["total_send_num"])*100;
			$val["total_sign_for_rate"] = round($val["total_sign_for_rate2"],2)."%";;  //总签收率

			foreach($arrFR as $vm){
				$val[$vm["fields"]] = round(($val["".$vm["com_code"]."_sign_for_num"]/$val["".$vm["com_code"]."_send_num"])*100,2)."%";
			}


		}
		//dump($arrData);die;

		$arrFooter = $order_info->field($fields)->where($where)->find();
		$arrFooter["createname"] = "总计";
		$arrFooter["total_sign_for_rate"] = round(($arrFooter["total_sign_for_num"]/$arrFooter["total_send_num"])*100,2)."%";  //总签收率
		foreach($arrFR as $vm){
			$arrFooter[$vm["fields"]] = round(($arrFooter["".$vm["com_code"]."_sign_for_num"]/$arrFooter["".$vm["com_code"]."_send_num"])*100,2)."%";
		}

		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrFT = array_merge($arrF["send"],$arrF["sign_for"],$arrF["sign_for_rate"],$arrF["sign_for_money"],$arrF["cope_money"]);
			$arrFM = $this->array_sort($arrFT,"fields","asc","no");

			$arrFd = array("fields"=>"createname","display"=>"工号");
			$arrFd1 = array("fields"=>"cn_name","display"=>"姓名");
			$arrFd2 = array("fields"=>"dept_name","display"=>"部门");
			$arrFd3 = array("fields"=>"total_send_num","display"=>"总发货数");
			$arrFd4 = array("fields"=>"total_sign_for_num","display"=>"总签收数");
			$arrFd5 = array("fields"=>"total_sign_for_rate","display"=>"总签收率");
			$arrFd6 = array("field"=>"total_send_money","title"=>"总发货金额","width"=>"90");
			array_unshift($arrFM,$arrFd5);
			array_unshift($arrFM,$arrFd4);
			array_unshift($arrFM,$arrFd6);
			array_unshift($arrFM,$arrFd3);
			array_unshift($arrFM,$arrFd2);
			array_unshift($arrFM,$arrFd1);
			array_unshift($arrFM,$arrFd);

			foreach($arrFM as &$val){
				$arrField[] = $val["fields"];
				$arrTitle[] = $val["display"];
			}

			$xls_count = count($arrField);
			$excelTiele = "签收报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($start_time){
			$arrT["start_time"] = $start_time;
		}
		if($end_time){
			$arrT["end_time"] = $end_time;
		}
		$arrT["footer"] = $arrFooter2;

		echo json_encode($arrT);
	}

	//物流状态报表
	function logisticsStateReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "GroupSetting";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$this->assign("start_time",$start_time);
		$this->assign("end_time",$end_time);

		$this->display();
	}

	function logisticsStateReportData(){
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$user_name = $_REQUEST["user_name"];
		$dept_id = $_REQUEST['dept_id'];
		$search_type = $_REQUEST["search_type"];

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];


		$arrDep = getDepTreeData();
		$deptst = getMeAndSubDeptID($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$deptst2 = getMeAndSubDeptID($arrDep,$d_id);  //取上级部门
		$deptSet2 = rtrim($deptst2,",");

		if(!$dept_id){
			$deptId = $deptSet2;
		}else{
			$deptId = $deptSet;
		}

		$order_info = M("order_info");

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND logistics_time >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND logistics_time <= '$end_time'";
		$where .= empty($user_name) ? "" : " AND createname = '$user_name'";
		if($username != "admin" || $dept_id){
			$where .= " AND createname in ($deptUser)";
		}
		$arrCount = $order_info->Distinct(true)->field("createname")->where($where)->select();
		$count = count($arrCount);

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$fields = "createname,dept_id,sum(case when logistics_state!='' AND logistics_state is not null then 1 else 0 end) as total_num,sum(case when logistics_state!='' AND logistics_state is not null then cope_money else 0 end) as total_money,sum(case when logistics_state='0' then 1 else 0 end) as state0_num,sum(case when logistics_state='1' then 1 else 0 end) as state1_num,sum(case when logistics_state='2' then 1 else 0 end) as state2_num,sum(case when logistics_state='3' then 1 else 0 end) as state3_num,sum(case when logistics_state='4' then 1 else 0 end) as state4_num,sum(case when logistics_state='5' then 1 else 0 end) as state5_num,sum(case when logistics_state='6' then 1 else 0 end) as state6_num,sum(case when logistics_state='0' then cope_money else 0 end) as state0_money,sum(case when logistics_state='1' then cope_money else 0 end) as state1_money,sum(case when logistics_state='2' then cope_money else 0 end) as state2_money,sum(case when logistics_state='3' then cope_money else 0 end) as state3_money,sum(case when logistics_state='4' then cope_money else 0 end) as state4_money,sum(case when logistics_state='5' then cope_money else 0 end) as state5_money,sum(case when logistics_state='6' then cope_money else 0 end) as state6_money";

		if($search_type == "xls"){
			$arrData = $order_info->field($fields)->where($where)->group("createname")->select();
		}else{
			$arrData = $order_info->field($fields)->limit($page->firstRow.','.$page->listRows)->where($where)->group("createname")->select();
		}

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptName_user = $userArr["deptName_user"];
		$deptId_name = $userArr["deptId_name"];
		foreach($arrData as &$val){
			$val["cn_name"] = $cnName[$val["createname"]];
			//$val["dept_name"] = $deptName_user[$val["createname"]];
			$val["dept_name"] = $deptId_name[$val["dept_id"]];

			//退回率
			$val["state6_num_rate"] = round(($val["state6_num"]/$val["total_num"])*100,2)."%";

			//派件率
			$val["state5_num_rate"] = round(($val["state5_num"]/$val["total_num"])*100,2)."%";

			//退签率
			$val["state4_num_rate"] = round(($val["state4_num"]/$val["total_num"])*100,2)."%";

			//签收率
			$val["state3_num_rate"] = round(($val["state3_num"]/$val["total_num"])*100,2)."%";

			//疑难率
			$val["state2_num_rate"] = round(($val["state2_num"]/$val["total_num"])*100,2)."%";

			//揽件率
			$val["state1_num_rate"] = round(($val["state1_num"]/$val["total_num"])*100,2)."%";

			//在途率
			$val["state0_num_rate"] = round(($val["state0_num"]/$val["total_num"])*100,2)."%";

		}

		$arrFooter = $order_info->field($fields)->where($where)->find();
		//退回率
		$arrFooter["state6_num_rate"] = round(($arrFooter["state6_num"]/$arrFooter["total_num"])*100,2)."%";
		//派件率
		$arrFooter["state5_num_rate"] = round(($arrFooter["state5_num"]/$arrFooter["total_num"])*100,2)."%";
		//退签率
		$arrFooter["state4_num_rate"] = round(($arrFooter["state4_num"]/$arrFooter["total_num"])*100,2)."%";
		//签收率
		$arrFooter["state3_num_rate"] = round(($arrFooter["state3_num"]/$arrFooter["total_num"])*100,2)."%";
		//疑难率
		$arrFooter["state2_num_rate"] = round(($arrFooter["state2_num"]/$arrFooter["total_num"])*100,2)."%";
		//揽件率
		$arrFooter["state1_num_rate"] = round(($arrFooter["state1_num"]/$arrFooter["total_num"])*100,2)."%";
		//在途率
		$arrFooter["state0_num_rate"] = round(($arrFooter["state0_num"]/$arrFooter["total_num"])*100,2)."%";
		$arrFooter["createname"] = "总计";
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array('createname','cn_name','total_num','total_money','state6_num','state6_num_rate','state6_money','state5_num','state5_num_rate','state5_money','state4_num','state4_num_rate','state4_money','state3_num','state3_num_rate','state3_money','state2_num','state2_num_rate','state2_money','state1_num','state1_num_rate','state1_money','state0_num','state0_num_rate','state0_money');
			$arrTitle = array('工号','姓名','总数量','总订单金额','退回数量','退回率','退回订单金额','派件数量','派件率','派件订单金额','退签数量','退签率','退签订单金额','签收数量','签收率','签收订单金额','疑难数量','疑难率','疑难订单金额','揽件数量','揽件率','揽件订单金额','在途数量','在途率','在途订单金额');

			$xls_count = count($arrField);
			$excelTiele = "物流状态报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($start_time){
			$arrT["start_time"] = $start_time;
		}
		if($end_time){
			$arrT["end_time"] = $end_time;
		}
		$arrT["footer"] = $arrFooter2;

		echo json_encode($arrT);
	}

	//部门业绩详细报表
	function deptOrderDetailReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Department Order Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$this->assign("start_time",$start_time);
		$this->assign("end_time",$end_time);

		$this->display();
	}

	function deptPerformanceDetail(){
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$user_name = $_REQUEST["user_name"];
		$dept_id = $_REQUEST['dept_id'];
		$search_type = $_REQUEST["search_type"];

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$arrDep = getDepTreeData();
		$deptst = getMeAndSubDeptID($arrDep,$dept_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$deptst2 = getMeAndSubDeptID($arrDep,$d_id);  //取上级部门
		$deptSet2 = rtrim($deptst2,",");

		if(!$dept_id){
			$deptId = $deptSet2;
		}else{
			$deptId = $deptSet;
		}

		$order_info = M("order_info");

		$fields = "dept_id,count(*) as total_order_num,
			sum(cope_money) as total_cope_money,
			sum(case when order_status = '1' then 1 else 0 end) as order_num_1,
			sum(if(order_status = '1',`cope_money`,0)) as cope_money_1,
			sum(case when order_status = '3' then 1 else 0 end) as order_num_3,
			sum(if(order_status = '3',`cope_money`,0)) as cope_money_3,
			sum(case when order_status = '8' then 1 else 0 end) as order_num_8,
			sum(if(order_status = '8',`cope_money`,0)) as cope_money_8,
			sum(case when order_status = '7' then 1 else 0 end) as order_num_7,
			sum(if(order_status = '7',`cope_money`,0)) as cope_money_7,
			sum(case when order_status = '2' then 1 else 0 end) as order_num_2,
			sum(if(order_status = '2',`cope_money`,0)) as cope_money_2,
			sum(case when logistics_state = '6' then 1 else 0 end) as send_back_num,
			sum(cope_money) as cope_money_total,
			sum(case when shipping_status = '1' then 1 else 0 end) as shipping_status_1,
			sum(if(shipping_status = '1',`cope_money`,0)) as shipping_money_1,
			sum(case when logistics_state = '3' then 1 else 0 end) as order_sign_for_num,
			sum(if(logistics_state = '3',`cope_money`,0)) as sign_for_money
		";

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND createtime >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND createtime <= '$end_time'";
		$where .= empty($user_name) ? "" : " AND createname = '$user_name'";
		if($username != "admin" || $dept_id){
			$where .= " AND dept_id in ($deptId)";
		}
		$arrCount = $order_info->Distinct(true)->field("dept_id")->where($where)->select();
		$count = count($arrCount);
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($search_type == "xls"){
			$arrData = $order_info->order("dept_id asc")->field($fields)->group("dept_id")->where($where)->select();
		}else{
			$arrData = $order_info->order("dept_id asc")->field($fields)->limit($page->firstRow.','.$page->listRows)->where($where)->group("dept_id")->select();
		}
		//echo $order_info->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$deptId_name = $userArr["deptId_name"];
		foreach($arrData as $key=>&$val){
			$val["dept_name"] = $deptId_name[$val["dept_id"]];

			//实际销售单数/总订单数
			$val["audit_rate2"] = ($val["order_num_1"]/$val["total_order_num"])*100;
			$val["audit_rate"] = round($val["audit_rate2"],2)."%";;  //审核率

			//发货单数/实际销售单数
			$val["delivery_rate2"] = ($val["shipping_status_1"]/$val["order_num_1"])*100;
			$val["delivery_rate"] = round($val["delivery_rate2"],2)."%";;  //发货率

			//签收的订单数/发货单数
			//$val["sign_for_rate2"] = ($val["order_sign_for_num"]/$val["shipping_status_1"])*100;
			//签收的订单数/实际销售单数
			$val["sign_for_rate2"] = ($val["order_sign_for_num"]/$val["order_num_1"])*100;
			$val["sign_for_rate"] = round($val["sign_for_rate2"],2)."%";;  //签收率
		}

		$arrFooter = $order_info->order("createname asc")->field($fields)->where($where)->find();
		$arrFooter["createname"] = "总计";
		$arrFooter2[0] = $arrFooter;

		if($search_type == "xls"){
			$arrField = array ('dept_name','total_order_num','total_cope_money','order_num_1','cope_money_1','order_num_3','cope_money_3','order_num_8','cope_money_8','order_num_7','cope_money_7','order_num_2','cope_money_2','send_back_num','audit_rate','cope_money_total','shipping_status_1','shipping_money_1','delivery_rate','sign_for_rate','order_sign_for_num','sign_for_money');
			$arrTitle = array ('部门','总订单数','总销售金额','实际销售单数','实际销售金额','待审核订单数','待审核金额','问题单数','问题单金额','未通过单数','未通过金额','取消单数','取消单数金额','物流退回单数','审核率','订单总金额','发货单数','发货金额','发货率','签收率','签收的订单数','签收订单数金额');
			$xls_count = count($arrField);
			$excelTiele = "部门业绩详细报表".date("Y-m-d");
			array_push($arrData,$arrFooter);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($start_time){
			$arrT["start_time"] = $start_time;
		}
		if($end_time){
			$arrT["end_time"] = $end_time;
		}
		$arrT["footer"] = $arrFooter2;
		echo json_encode($arrT);
	}

	//工单坐席报表
	function workOrderReport(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order Agent Reports";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		$year_month = date("Y-m");


		$types = $_REQUEST["types"];
		if($types){
			$this->assign("types","Y");
		}else{
			$this->assign("types","N");
		}

		$month = date("Y-m");
		$this->assign("month",$month);

		$arrF = $this->workOrderTableFields();
		$i = 0;
		$fields = "";
		foreach($arrF as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
			$arrTF[] = $val["fields"];
			$arr_fields[] = array(
				"fields" => $val["fields"],
				"display" => $val["display"],
				"color" => $val["color"],
			);
			if($val["color"]){
				$arrColor[] = $val["color"];
			}

			$arrField[0][$i]['field'] = $val['fields'];
			$arrField[0][$i]['title'] = $val['display'];
			$arrField[0][$i]['width'] = "100";
			$i++;
		}
		//dump($arrF);
		$arrFd = array("field"=>"create_user","title"=>"工号","width"=>"80");
		$arrFd2 = array("field"=>"cn_name","title"=>"姓名","width"=>"80");
		array_unshift($arrField[0],$arrFd2);
		array_unshift($arrField[0],$arrFd);

		$arrFs = json_encode($arrField);
		$this->assign("fieldList",$arrFs);
		$this->assign("arr_fields",$arr_fields);

		$str_color = "'".implode("','",$arrColor)."'";
		$this->assign("str_color",$str_color);
		//dump($str_color);die;



		$this->display();
	}

	function workOrderTableFields(){
		$work_order_type = M("work_order_type");
		$arrData = $work_order_type->field("id as value,type_name as display,color")->select();

		$i = 1;
		foreach($arrData as &$val){
			$val["fields"] = "servicetype_".$i;
			$val["name"] = "work_order_type";
			$i++;
		}
		unset($i);
		//dump($arrData);die;
		return $arrData;
	}

	function workOrderReportData(){
		header("Content-Type:text/html; charset=utf-8");
		$types = $_REQUEST["types"];
		$search_type = $_REQUEST["search_type"];
		$work_order_list = new Model("work_order_list");

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";
		//echo $deptUser;die;

		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_end = Date('Y-m-d',$_GET['ts_end']);
			if("lastmonth_start" == $_GET['ts_start']){//上个月的起始时间
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1));
			}else{
				$date_start = Date('Y-m-d',$_GET['ts_start']);
			}
		}else{
			$date_start = $_REQUEST["start_time"];
			$date_end = $_REQUEST["end_time"];
		}
		if(! $date_end){$date_end = Date('Y-m-d H:i:s');}

		$where = "1 ";
		if($username != "admin"){
			if($types == "Y"){
				$where .= " AND create_user in ($deptUser)";
			}else{
				$where .=  "AND create_user='$username' ";
			}
		}
		$nowdate = date("Y-m-d");
		$where .= empty($date_start) ? " AND DATE(create_time) >= '$nowdate'" : " AND DATE(create_time) >= '$date_start'";
		$where .= empty($date_end) ? "" : " AND DATE(create_time) <= '$date_end'";

		$fields = "";
		$field_arr = "";
		$arrField = array("create_user","cn_name");
		$arrTitle = array("工号","姓名");
		$arrF = $this->workOrderTableFields();
		foreach($arrF as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
			$str2 = "'".$val["fields"]."' => '0'";
			$field_arr .= empty($field_arr)?"$str2":",$str2";
			$arrFT[] = $val["fields"];

			$arrTF[] = $val["fields"];
			$arrTF2[] = $val["fields"]."[]";
			$arr_fields[] = array(
				"fields" => $val["fields"],
				"display" => $val["display"],
				"color" => $val["color"],
			);

			$arrField[] = $val["fields"];
			$arrTitle[] = $val["display"];

		}

		//dump($field_arr);die;

		$arrCount = $work_order_list->Distinct(true)->field('create_user')->select();
		$count = count($arrCount);
		if($search_type == "xls"){
			$arrData = $work_order_list->field("$fields,create_user")->where($where)->group("create_user")->select();
		}else{
			$arrData = $work_order_list->field("$fields,create_user")->limit($page->firstRow.','.$page->listRows)->where($where)->group("create_user")->select();
		}
		//echo $work_order_list->getLastSql();
		//dump($arrData);die;

		//=========页脚
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arr_fields as &$val){
			foreach($arrData as &$vm){
				$arrFoot[$val["fields"]][] = $vm[$val["fields"]];
				$tt[] = $vm[$val["fields"]];

				$vm["cn_name"] = $cnName[$vm["create_user"]];
			}
		}

		foreach($arr_fields as &$val){
			$arrSum[$val["fields"]] = array_sum($arrFoot[$val["fields"]]);
		}
		$arrSum["create_user"] = "本页总计";
		$footer = array($arrSum);


		if($search_type == "xls"){
			$xls_count = count($arrField);
			$excelTiele = "工单坐席报表".date("Y-m-d");
			array_push($arrData,$arrSum);
			//dump($arrField);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["arr_fields"] = $arr_fields;
		$ary["arrField"] = $arrTF;
		$ary["footer"] = $footer;
		if($date_start){
			$ary["date_start"] = $date_start;
		}else{
			$ary["date_start"] = $nowdate;
		}
		if($date_end){
			$ary["date_end"] = $date_end;
		}

		echo json_encode($ary);

	}

	//工单月报表
	function workOrderMonthlyReport(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order reports";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		$year_month = date("Y-m");


		$types = $_REQUEST["types"];
		if($types){
			$this->assign("types","Y");
		}else{
			$this->assign("types","N");
		}

		$month = date("Y-m");
		$this->assign("month",$month);

		$arrF = $this->workOrderTableFields();
		$i = 0;
		$fields = "";
		foreach($arrF as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
			$arrTF[] = $val["fields"];
			$arr_fields[] = array(
				"fields" => $val["fields"],
				"display" => $val["display"],
				"color" => $val["color"],
			);
			if($val["color"]){
				$arrColor[] = $val["color"];
			}

			$arrField[0][$i]['field'] = $val['fields'];
			$arrField[0][$i]['title'] = $val['display'];
			$arrField[0][$i]['width'] = "100";
			$i++;
		}
		//dump($arrF);
		$arrFd = array("field"=>"date","title"=>"日期","width"=>"130");
		array_unshift($arrField[0],$arrFd);

		$arrFs = json_encode($arrField);
		$this->assign("fieldList",$arrFs);
		$this->assign("arr_fields",$arr_fields);

		$str_color = "'".implode("','",$arrColor)."'";
		$this->assign("str_color",$str_color);
		//dump($str_color);die;



		$this->display();
	}

	function workOrderMonthlyReportData(){
		header("Content-Type:text/html; charset=utf-8");
		$types = $_REQUEST["types"];
		$search_type = $_REQUEST["search_type"];
		$work_order_list = new Model("work_order_list");

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$month = $_REQUEST["month"];
		if($month){
			$year_month = $_REQUEST["month"];
		}else{
			$year_month = date("Y-m");
		}

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";
		//echo $deptUser;die;

		if($username != "admin"){
			if($types == "Y"){
				$where = " AND create_user in ($deptUser)";
			}else{
				$where =  "AND create_user='$username' ";
			}
		}else{
			$where = " ";
		}

		$fields = "";
		$field_arr = "";
		$arrField = array("date");
		$arrTitle = array("日期");
		$arrF = $this->workOrderTableFields();
		foreach($arrF as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
			$str2 = "'".$val["fields"]."' => '0'";
			$field_arr .= empty($field_arr)?"$str2":",$str2";
			$arrFT[] = $val["fields"];

			$arrTF[] = $val["fields"];
			$arrTF2[] = $val["fields"]."[]";
			$arr_fields[] = array(
				"fields" => $val["fields"],
				"display" => $val["display"],
				"color" => $val["color"],
			);

			$arrField[] = $val["fields"];
			$arrTitle[] = $val["display"];
		}

		//dump($field_arr);die;

		if($username == "admin"){
			$sql = "SELECT $fields,DATE(create_time) as date FROM work_order_list WHERE create_time LIKE '%$year_month%' GROUP BY DATE(create_time)";
		}else{
			$sql = "SELECT $fields,DATE(create_time) as date FROM work_order_list WHERE create_time LIKE '%$year_month%' $where GROUP BY DATE(create_time)";
		}

		$serviceData = $work_order_list->query($sql);
		//echo $work_order_list->getLastSql();die;


		//dump($year_month);die;
		$timestamp = strtotime($year_month . "-10 00:00:00");
		$days = date('t',$timestamp);
		$arrLAST = Array();
		for($i=0;$i<$days;$i++){
			if( $i< 9){
				$day = "0" . ($i+1);
			}else{
				$day = $i+1;
			}
			$current_day = $year_month .'-' .$day;
			$arrLAST[$i] = Array(
				'date' => $current_day,
				"resciveday" => $i+1,
			);
		}
		//添加servicetype_*字段
		foreach($arrLAST as &$val){
			foreach($arrTF as $vm){
				$val[$vm] = "0";
			}
		}

		//dump($arrLAST);die;
		foreach($serviceData AS $v){
			$arrTemp = explode('-',$v['date']);
			$index = (int)($arrTemp[2]);
			//echo "---------$index---</br>";
			$arrLAST[$index-1] = $v;
		}


		//=========页脚
		foreach($arr_fields as &$val){
			foreach($arrLAST as &$vm){
				$arrFoot[$val["fields"]][] = $vm[$val["fields"]];
				$tt[] = $vm[$val["fields"]];
			}
		}

		foreach($arr_fields as &$val){
			$arrSum[$val["fields"]] = array_sum($arrFoot[$val["fields"]]);
		}
		$arrSum["date"] = "总计";
		$footer = array($arrSum);

		if($search_type == "xls"){
			$xls_count = count($arrField);
			$excelTiele = "工单月报表".date("Y-m-d");
			array_push($arrLAST,$arrSum);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrLAST,$excelTiele);
			die;
		}

		$count = count($arrLAST);

		$rowsList = count($arrLAST) ? $arrLAST : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["arr_fields"] = $arr_fields;
		$ary["arrField"] = $arrTF;
		$ary["footer"] = $footer;

		echo json_encode($ary);

	}

}
?>
