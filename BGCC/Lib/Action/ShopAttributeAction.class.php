<?php
class ShopAttributeAction extends Action{
	function attributeList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Commercial Property";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$id = $_REQUEST["id"];
		if($id){
			$this->assign("id",$id);
		}
		$this->display();
	}


	function ShopAttributeData(){
		$id = $_REQUEST["id"];
		$tid = $_REQUEST["tid"];

		$where = "1 ";
		$where .= empty($id)?"":" AND a.type_id = '$id'";
		$where .= empty($tid)?"":" AND a.type_id = '$tid'";


		$where2 = "1 ";
		$where2 .= empty($id)?"":" AND type_id = '$id'";
		$where2 .= empty($tid)?"":" AND type_id = '$tid'";


		$shop_attribute = new Model("shop_attribute");
		import('ORG.Util.Page');
		$count = $shop_attribute->where($where2)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$goodTypeData = $shop_attribute->table("shop_attribute a")->field("a.id as id,a.type_id,a.attr_name,a.attr_en_name,a.text_type,a.text_type as text_type2,a.attr_values,t.caty_name,t.id as tid")->join("shop_type t on a.type_id=t.id ")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$rows = array("1"=>"手工录入","2"=>"从列表中选择","3"=>"多行文本框");
		foreach($goodTypeData as &$val){
			$text_type = $rows[$val['text_type']];
			$val['text_type'] = $text_type;
		}

		//dump($goodTypeData);die;
		$rowsList = count($goodTypeData) ? $goodTypeData : false;
		$arrType["total"] = $count;
		$arrType["rows"] = $rowsList;

		echo json_encode($arrType);
	}

	function insertShopAttribute(){
		$shop_attribute = new Model("shop_attribute");
		$count = $shop_attribute->where("attr_en_name='". $_POST["attr_en_name"] ."'")->count();
		if( $count>0 ){
				echo json_encode(array('msg'=>'改英文名称已存在！'));
				exit;
		}
		$arrData = array(
			"attr_name"=>$_REQUEST["attr_name"],
			"attr_en_name"=>$_REQUEST["attr_en_name"],
			"type_id"=>$_REQUEST["type_id"],
			"text_type"=>$_REQUEST["text_type"],
			"attr_values"=>$_REQUEST["attr_values"],
		);
		$result = $shop_attribute->data($arrData)->add();
		if ($result){
			$this->attrCache();
			echo json_encode(array('success'=>true,'msg'=>'商品类型添加成功！'));
		} else {
			echo json_encode(array('msg'=>'商品类型添加失败！'));
		}
	}

	function attrCache(){
		$shop_attribute = new Model("shop_attribute");
		$arrData = $shop_attribute->order("type_id asc")->select();
		F('shopAttr',$arrData,"BGCC/Conf/");
	}

	function updateShopAttribute(){
		$id = $_REQUEST["id"];
		$shop_attribute = new Model("shop_attribute");
		$count = $shop_attribute->where("id != $id AND attr_en_name='". $_POST["attr_en_name"] ."'")->count();
		if( $count>0 ){
			echo json_encode(array('msg'=>'改英文名称已存在！'));
			exit;
		}
		$arrData = array(
			"attr_name"=>$_REQUEST["attr_name"],
			"attr_en_name"=>$_REQUEST["attr_en_name"],
			"type_id"=>$_REQUEST["type_id"],
			"text_type"=>$_REQUEST["text_type"],
			"attr_values"=>$_REQUEST["attr_values"],
		);
		$result = $shop_attribute->data($arrData)->where("id = $id")->save();
		if ($result !== false){
			$this->attrCache();
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteShopAttribute(){
		$id = $_REQUEST["id"];
		$shop_attribute = new Model("shop_attribute");
		$result = $shop_attribute->where("id = $id")->delete();
		if ($result){
			$this->attrCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function shopTypeCombox(){
		$shop_type = new Model("shop_type");
		$goodTypeData = $shop_type->order("cat_order asc")->field("id,caty_name as text")->where("caty_enabled = 'Y'")->select();
		if($goodTypeData){
			$arr = array("id"=>"","text"=>"请选择...");
			array_unshift($goodTypeData,$arr);
		}else{
			$goodTypeData = array(array("id"=>"","text"=>"请选择..."));
		}
		echo json_encode($goodTypeData);
	}
}

?>
