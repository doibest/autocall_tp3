<?php
class FaqAction extends Action{
	function faqList(){
		checkLogin();

		//分配增删改的权限
		$menuname = "Faq Type";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function interfacetest(){
		$name = $_POST["user"];
		$customer = new Model("customer");

		$arr = $customer->where("`name` like '$name'")->find();
		 echo json_encode($arr);
		 exit;

	}


	function menu(){
		checkLogin();

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->display();
	}

	function menuList(){
		checkLogin();

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->display();
	}

	function menuData(){
		$username = $_SESSION['user_info']['username'];
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$menu = M("$menu_table");
		$count = $menu->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrMenu = $menu->order("m.order asc")->table("$menu_table m")->field("m.id,m.name,m.moduleclass,m.order,m.url,m.pid,m.icon,m.sys_type,m.privilege")->join("$module_table md on (m.moduleclass = md.modulename)")->where("md.enabled = 'Y'")->select();
		foreach($arrMenu as &$val){
			$val["name"] = L($val["name"]);
			$val['state'] = "closed";
		}


		$arrData = $this->getMenuTree($arrMenu,'0','pid','id');

		$j = 0;
		foreach($arrData as $v){
			if( $arrData[$j]["pid"] == "0"){
				$arrData[$j]['iconCls'] = "shop";  //顶级分类的图标，叶子节点的图标在getTree()方法中设置。
			}
			$j++;
		}
		unset($j);
		//dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	function getMenuTree($data, $pId,$field_pid,$field_id) {
		$tree = '';
		foreach($data as $k =>$v) {
			if($v[$field_pid] == $pId)    {
				$v['children'] = $this->getMenuTree($data, $v[$field_id],$field_pid,$field_id);

				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				/*
				if ( empty($v["children"]) && $v['state'] =='closed'){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}
				if ( $v["children"]  && $v['state'] =='closed'){
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				*/
				if ( empty($v["children"])){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}else{
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				$tree[] = $v;     //unset($data[$k]);
			}
		}
		return $tree;
	}

	function insertMenu(){
		$username = $_SESSION['user_info']['username'];
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$menu = new Model("$menu_table");
		$arrData = array(
			'name'=>$_REQUEST['name'],
			'pid'=>$_REQUEST['pid'],
			'url'=>$_REQUEST['url'],
			'order'=>$_REQUEST['order'],
			'privilege'=>$_REQUEST['privilege'],
			'moduleclass'=>$_REQUEST['moduleclass'],
		);
		//dump($arrData);die;
		$result = $menu->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function insertMenu2(){
		$username = $_SESSION['user_info']['username'];
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$menu = new Model("$menu_table");
		$arrData = array(
			'name'=>$_REQUEST['name2'],
			'pid'=>"0",
			'url'=>"",
			'order'=>$_REQUEST['order2'],
			'privilege'=>"view",
			'moduleclass'=>$_REQUEST['modulename'],
		);
		//dump($arrData);die;
		$result = $menu->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateMenu(){
		$id = $_REQUEST['id'];
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$menu = new Model("$menu_table");
		$arrData = array(
			//'name'=>$_REQUEST['name'],
			'pid'=>$_REQUEST['pid'],
			'url'=>$_REQUEST['url'],
			'order'=>$_REQUEST['order'],
			'privilege'=>$_REQUEST['privilege'],
			'moduleclass'=>$_REQUEST['moduleclass'],
		);
		//dump($arrData);die;
		$result = $menu->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteMenu(){
		$id = $_REQUEST["id"];
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$menu = new Model("$menu_table");
		$result = $menu->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function moduleList(){
		checkLogin();

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->display();
	}

	function moduleData(){
		$username = $_SESSION['user_info']['username'];
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$module = new Model("$module_table");
		$count = $module->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $module->limit($page->firstRow.','.$page->listRows)->select();
		$enabled_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$enabled = $enabled_row[$val['enabled']];
			$val['enabled2'] = $enabled;

			//$val["modulename"] = L($val["modulename"]);

		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertModule(){
		$username = $_SESSION['user_info']['username'];
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$module = new Model("$module_table");
		$arrData = array(
			'modulename'=>$_REQUEST['modulename'],
			'enabled'=>$_REQUEST['enabled'],
			'description'=>$_REQUEST['description']
		);
		$result = $module->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateModule(){
		$id = $_REQUEST['id'];
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$module = new Model("$module_table");
		$arrData = array(
			'enabled'=>$_REQUEST['enabled'],
			'description'=>$_REQUEST['description']
		);
		$result = $module->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteModule(){
		$id = $_REQUEST["id"];
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$menu_table = "crm_public.menu";
			$module_table = "crm_public.module";
		}else{
			$menu_table = "menu";
			$module_table = "module";
		}
		$module = new Model("$module_table");
		$result = $module->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function test(){
		$arrData = require "/var/www/html/BGCC/Conf/ttttt.php";
		$arrE = require "/var/www/html/BGCC/Conf/hf.php";

		$sql_insert = "INSERT INTO bgcrm.agent_report(calldate,workno,extension,call_method,calltype,callin_times,callin_billsec_total,callout_times,callout_billsec_total)";
		$value = "";

		foreach($arrData as $val){
			/*
			if( in_array($val["src"],$arrE) && in_array($val["dst"],$arrE) ){
				$arrT[] = $val;
			}
			*/
			$str = "(";

			if($val["callin_times"] > 0 && $val["callout_times"] > 0){
				if( in_array($val["src"],$arrE) ){
					$val["extension"] = $val["src"];
					$val["call_method"] = "";

					//$arrT[] = $val;

				}else{
					$val["extension"] = $val["dst"];
					$val["call_method"] = "";

					//$arrO[] = $val;
				}
			}else{

			}

			$str .= ")";
			$value .= empty($value)?"$str":",$str";
		}

		dump($arrT);
		dump($arrO);
		die;

		$username = $_SESSION['user_info']['username'];
		$vis = new Model("visit_content");
		$where = "1";
		if($username != "admin"){
			$where .= " AND createname = '$username'";
		}

		$arrData = $vis->field("title,content,description")->where($where)->select();

		$this->assign("visitData",$arrData);

		$this->display();
	}

	function insertTest(){
		$visit_record = new Model("visit_record");
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION["user_info"]["d_id"];
		$visit_contents = $_REQUEST["visit_contents"];
		$sql = "insert into visit_record_problem(`problem`,`answer`,`description`,`visit_record_id`,`customer_id`,`create_name`,`dept_id`,`createtime`) values";
		$result = "1";
		foreach($visit_contents as $val){
			$str = "(";
			$str .= "'" .$val[0]. "',";
			$str .= "'" .$val[1]. "',";
			$str .= "'" .$val[2]. "',";
			$str .= "'" .$result. "',";
			$str .= "'" .$$_REQUEST['id']. "',";
			$str .= "'" .$username. "',";
			$str .= "'" .$d_id. "',";
			$str .= "'" .date("Y-m-d H:i:s"). "'";

			$str .= ")";
			$value .= empty($value)?"$str":",$str";
		}
		if( $value ){
			$sql .= $value;
			$res = $visit_record->execute($sql);
		}

		dump($res);
		dump($sql);
		dump($visit_contents);
		die;
	}

	function faqData(){
		$faq=new Model("faq_type");
		$count = $faq->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		//$page = new Page($count,$page_rows);
		$page = new Page($count,"500");

		$rows = $faq->field("name,id,pid")->limit($page->firstRow.','.$page->listRows)->select();
		$i = 0;
		foreach($rows as $v){
			$rows[$i]['state'] = "closed";
			//$rows[$i]['iconCls'] = "icon-add";
			$i++;
		}
		unset($i);
        $arrTree = $this->getTree($rows,0);

		$j = 0;
		foreach($arrTree as $v){
			if( $arrTree[$j]["pid"] == "0" && !$arrTree[$j]["children"]){
				$arrTree[$j]['iconCls'] = "icon-files";
			}
			$j++;
		}
		unset($j);

		//dump($arrTree);die;
		$rowsList = count($arrTree) ? $arrTree : false;

		$arrFaq["total"] = $count;
		$arrFaq["rows"] = $rowsList;
		//dump($arrmail);die;
		echo json_encode($arrFaq);
	}

	function insertFaq(){
		$faq=new Model('faq_type');
		$arrData = array(
			"name" =>$_POST["name"],
			"pid" =>$_POST["pid"],
		);
		//dump($arrData);die;
		$result = $faq->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'faq添加成功！'));
		} else {
			echo json_encode(array('msg'=>'faq添加失败！'));
		}
	}

	function updateFaq(){
		$id = $_REQUEST['id'];
		$faq=new Model('faq_type');
		$arrData = array(
			"name" =>$_POST["name"],
			"pid" =>$_POST["pid"],
		);
		$result = $faq->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteFaq(){
		$id = $_REQUEST["id"];
		$faq=new Model('faq_type');
		$result = $faq->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function faqTree(){
		$faq=new Model("faq_type");
		$rows = $faq->field("id,name as text,pid")->select();
		//dump($rows);
		$alldept = array(
			"text"=>"顶级名称",
			"id"=>"0",
			"d_pid"=>"0",
		);
		$i = 0;
		foreach($rows as $v){
			$rows[$i]['state'] = "closed";
			$i++;
		}
		unset($i);
        $arrTree = $this->getTree($rows,0);
		$j = 0;
		foreach($arrTree as $v){
			if( $arrTree[$j]["pid"] == "0" && !$arrTree[$j]["children"]){
				$arrTree[$j]['iconCls'] = "icon-files";
			}
			$j++;
		}
		unset($j);
		array_unshift($arrTree,$alldept);
		echo json_encode($arrTree);
	}

	function faqEditTree(){
		$faq=new Model("faq_type");
		$rows = $faq->field("id,name as text,pid")->select();
		//dump($rows);
		$alldept = array(
			"text"=>"顶级名称",
			"id"=>"0",
			"d_pid"=>"0",
		);
        $arrTree = $this->getTree($rows,0);
		$j = 0;
		foreach($arrTree as $v){
			if( $arrTree[$j]["pid"] == "0" && !$arrTree[$j]["children"]){
				$arrTree[$j]['iconCls'] = "icon-files";
			}
			$j++;
		}
		unset($j);
		array_unshift($arrTree,$alldept);
		echo json_encode($arrTree);
	}

    function getTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
					$v['children'] = $this->getTree($data, $v['id']);

					if ( empty($v["children"])  )  unset($v['children']) ;
					if ( empty($v["children"]) && $v['state'] =='closed'){
					$v['state'] =  'open';
					}
					$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }


	function backupData(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		import('ORG.Backup.DbBak');
		import('ORG.Backup.TableBak');

		$connectid = mysql_connect(C("DB_HOST"),C("DB_USER"),C("DB_PWD"));
		mysql_query("set names utf8"); //声明字符集
		$backupDir = 'include/backup';
		$DbBak = new DbBak($connectid,$backupDir);

		$arrTab = array("ks_courseware","ks_curriculum","ks_exam_scores","ks_exam_scores_detail","ks_examination_program","ks_examination_staff","ks_papers","ks_policy_configuration","ks_policy_scores","ks_question_bank","ks_question_bank_select","ks_train","ks_train_staff","pb_attendance","pb_event","pb_holiday_date","pb_holidays","pb_leave_table","pb_scheduling","pb_shift_definition","pb_shift_set","pb_traffic_forecast","wj_questionnaire_answers","wj_questionnaire_answers_detail","wj_questionnaire_bank","wj_questionnaire_bank_select","wj_questionnaire_generation","wj_questionnaire_topic","wj_questionnaire_type","work_form","work_form_category","work_form_design","work_form_file","work_process_category","work_process_design","work_process_design_node","work_process_design_node_role","work_workflow_ticket","work_workflow_ticket_none");
		//$DbBak->backupDb('bgcrm',$arrTab);

		//$DbBak->backupDb('bgcrm',array('wx_group','wx_menu','wx_message_record','wx_msg_record','wx_news','wx_news_content','wx_quick_reply','wx_set','wx_users'));
		$DbBak->backupDb('bgcrm');
	}

	function restoreData(){
		import('ORG.Backup.DbBak');
		import('ORG.Backup.TableBak');

		$connectid = mysql_connect(C("DB_HOST"),C("DB_USER"),C("DB_PWD"));
		mysql_query("set names utf8"); //声明字符集
		$backupDir = 'include/backup';
		$DbBak = new DbBak($connectid,$backupDir);

		$arrTab = array("ks_courseware","ks_curriculum","ks_exam_scores","ks_exam_scores_detail","ks_examination_program","ks_examination_staff","ks_papers","ks_policy_configuration","ks_policy_scores","ks_question_bank","ks_question_bank_select","ks_train","ks_train_staff","pb_attendance","pb_event","pb_holiday_date","pb_holidays","pb_leave_table","pb_scheduling","pb_shift_definition","pb_shift_set","pb_traffic_forecast","wj_questionnaire_answers","wj_questionnaire_answers_detail","wj_questionnaire_bank","wj_questionnaire_bank_select","wj_questionnaire_generation","wj_questionnaire_topic","wj_questionnaire_type","work_form","work_form_category","work_form_design","work_form_file","work_process_category","work_process_design","work_process_design_node","work_process_design_node_role","work_workflow_ticket","work_workflow_ticket_none");

		//$DbBak->restoreDb('bgcrm',array('wx_message_record','wx_news','wx_news_content','wx_set','wx_menu','sales_cdr_26','sales_contact_history_26','region','sales_proposal_record_26','sales_source_26','task_result_status_26'));
		$DbBak->restoreDb('bgcrm',$arrTab);
		//$DbBak->restoreDb('bgcrm','region');
	}

	function deleteTaskTable(){
		$sales_task = M("sales_task");
		$arrData = $sales_task->field("id,name")->select();

		foreach($arrData as $val){
			$id = $val["id"];
			//删除号码资源表
			$ss = M('sales_source');
			$ss->execute("drop table sales_source_$id");
			$ss->execute("drop table sales_cdr_$id");
			$ss->execute("drop table sales_contact_history_$id");
			$ss->execute("drop table task_result_status_$id");
			$ss->execute("drop table sales_proposal_record_$id");

			//问卷
			$ss->execute("drop table wj_questionnaire_answers_$id");
			$ss->execute("drop table wj_questionnaire_answers_detail_$id");
		}
	}
}

?>

