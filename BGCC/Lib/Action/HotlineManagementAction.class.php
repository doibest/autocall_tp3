<?php
/*
热线管理
*/
class HotlineManagementAction extends Action{
	//媒体来源
	function mediaTypeList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Media Sources";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function mediaTypeData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$hotline_media_sources = M("hotline_media_sources");
		$count = $hotline_media_sources->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $hotline_media_sources->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertMediaType(){
		$username = $_SESSION['user_info']['username'];
		$hotline_media_sources = M("hotline_media_sources");
		$arrData = array(
			'source_name'=>$_REQUEST['source_name'],
		);
		$result = $hotline_media_sources->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateMediaType(){
		$id = $_REQUEST['id'];
		$hotline_media_sources = M("hotline_media_sources");
		$arrData = array(
			'source_name'=>$_REQUEST['source_name'],
		);
		$result = $hotline_media_sources->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteMediaType(){
		$id = $_REQUEST["id"];
		$hotline_media_sources = M("hotline_media_sources");
		$result = $hotline_media_sources->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//媒体热线
	function hotlineMediaList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Hotline Media";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function MediaHotlineData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$hotline_did = M("hotline_did");
		$count = $hotline_did->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $hotline_did->limit($page->firstRow.','.$page->listRows)->select();

		$media_sources_id_row = $this->getMediaType();
		foreach($arrData as &$val){
			$val["source_name"] = $media_sources_id_row[$val["media_sources_id"]];

			if($val["record_name"]){
				$val["file_path"] = "/var/lib/asterisk/sounds/custom/".$val["record_name"].".wav";
				$val["operations"] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["file_path"])."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadSystem&file_path=" .trim($val["file_path"])."'> 下载 </a>" ;
			}else{
				$val["operations"] = "";
			}
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function getMediaType(){
		$hotline_media_sources = M("hotline_media_sources");
		$arrData = $hotline_media_sources->order("id asc")->select();
		$arrF = array();
		foreach($arrData as $key=>$val){
			$arrF[$val["id"]] = $val["source_name"];
		}
		return $arrF;
	}

	function insertMediaHotline(){
		$username = $_SESSION['user_info']['username'];
		$hotline_did = M("hotline_did");
		$arrData = array(
			'did_num'=>$_REQUEST['did_num'],
			'media_sources_id'=>$_REQUEST['media_sources_id'],
			'record_name'=>$_REQUEST['record_name'],
		);
		$result = $hotline_did->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateMediaHotline(){
		$id = $_REQUEST['id'];
		$hotline_did = M("hotline_did");
		$arrData = array(
			'did_num'=>$_REQUEST['did_num'],
			'media_sources_id'=>$_REQUEST['media_sources_id'],
			'record_name'=>$_REQUEST['record_name'],
		);
		$result = $hotline_did->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function deleteMediaHotline(){
		$id = $_REQUEST["id"];
		$hotline_did = M("hotline_did");
		$result = $hotline_did->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//热线资源
	function hotlineResourcesList(){
		checkLogin();
		//分配增删改的权限
		$web_type = empty($_REQUEST["web_type"]) ? "back" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$menuname = "Hotline Resources";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		if($web_type == "agent"){
			$priv["allocation"] = "N";
			$priv["export"] = "N";
		}

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$this->assign("start_time",$start_time);
		$this->assign("end_time",$end_time);




		$this->display();
	}

	function hotlineResourcesData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();

		$web_panel = $_REQUEST["web_panel"];
		$web_type = $_REQUEST["web_type"];
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$did_number = $_REQUEST["did_number"];
		$phone = $_REQUEST["phone"];
		$hangup_type = $_REQUEST["hangup_type"];
		$assign_status = $_REQUEST["assign_status"];
		$visit_status = $_REQUEST["visit_status"];
		$member_Level = $_REQUEST["member_Level"];
		$name = $_REQUEST["name"];
		$create_user = $_REQUEST["create_user"];
		$search_type = $_REQUEST["search_type"];

		$where = "1 ";
		if($web_type == "agent"){  // && $username != "admin"
			$where .= " AND create_user = '$username'";
		}
		if($web_panel == "panel"){
			$where .= " AND (visit_status = 'N' OR visit_status = 'T')";
		}
		$where .= empty($start_time) ? "" : " AND r.create_time >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND r.create_time <= '$end_time'";
		$where .= empty($did_number) ? "" : " AND r.did_number like '%$did_number%'";
		$where .= empty($phone) ? "" : " AND r.phone like '%$phone%'";
		$where .= empty($hangup_type) ? "" : " AND r.hangup_type = '$hangup_type'";
		$where .= empty($assign_status) ? "" : " AND r.assign_status = '$assign_status'";
		$where .= empty($visit_status) ? "" : " AND r.visit_status = '$visit_status'";
		$where .= empty($member_Level) ? "" : " AND c.member_Level = '$member_Level'";
		$where .= empty($name) ? "" : " AND c.name like '%$name%'";
		$where .= empty($create_user) ? "" : " AND r.create_user = '$create_user'";

		$hotline_record = M("hotline_record");
		$fields = "r.*,c.name,c.integral,c.member_Level";
		$count = $hotline_record->table("hotline_record r")->field($fields)->join("customer c on (r.customer_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		$sort = $_REQUEST["sort"];
		$order = $_REQUEST["order"];
		if($sort){
			$arrCField = array("integral","member_Level");
			if( !in_array($sort,$arrCField) ){
				$usort = "r.".$sort." ".$order;
			}else{
				$usort = "c.".$sort." ".$order;
			}
		}else{
			if($web_panel == "panel"){
				$usort = "c.member_Level desc";
			}else{
				$usort = "r.create_time asc";
			}
		}

		if($search_type == "xls"){
			//$arrData = $hotline_record->order("create_time desc")->where($where)->select();
			$arrData = $hotline_record->order($usort)->table("hotline_record r")->field($fields)->join("customer c on (r.customer_id = c.id)")->where($where)->select();
		}else{
			//$arrData = $hotline_record->order("create_time desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
			$arrData = $hotline_record->order($usort)->table("hotline_record r")->field($fields)->join("customer c on (r.customer_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}

		$hangup_type_row = array('1'=>'主叫挂机','2'=>'被叫挂机');
		$visit_status_row = array('Y'=>'已回访','N'=>'未回访');
		$assign_status_row = array('Y'=>'已分配','N'=>'未分配','T'=>'系统自动分配');
		$intention_did_row = $this->getHotlineDid();
		$groups_row = $this->memberLevel();
		//dump($intention_did_row);die;
		foreach($arrData as &$val){
			$val["visit_status2"] = $val["visit_status"];
			$val["src_phone"] = $val["phone"];
			$val['hangup_type'] = $hangup_type_row[$val['hangup_type']];
			$val['visit_status'] = $visit_status_row[$val['visit_status']];
			$val['assign_status'] = $assign_status_row[$val['assign_status']];
			$val['intention_did'] = $intention_did_row[$val['intention_did_id']];
			$val["member_Level"] = $groups_row[$val["member_Level"]];

			$val["operating"] = '<a href="#" title="点击呼叫"  onClick="callHotline('."'".$val["src_phone"]."'".');"><img style="vertical-align:middle;cursor:pointer;padding:0px;padding-right:10px;margin:0px;border:none;" src="Agent/Tpl/public/js/themes/icons/call.png"  ></a>';

			//if($web_panel == "panel"){
				//$val["operating"] .= "<a href='javascript:void(0);' onclick=\"openVisit("."'".$val["id"]."','".$val["visit_status2"]."','".$val["visit_content"]."'".")\" >&nbsp;&nbsp;编辑</a>";
			//}
			if($val["member_Level"]){
				$val["operating"] .= "<span style='color:red;'>紧急</span>";
			}else{
				$val["operating"] .= "<span style='color:#0066cc;'>一般</span>";
			}
			$val["uniqueid"] = trim($val["uniqueid"]);
			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";
			if(file_exists($WAVfile) ){
				$val["operating"] .= "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["uniqueid"])."'".")\" > &nbsp;&nbsp;&nbsp;播放 </a> ";
				$val["WAVfile"] = $WAVfile;
			}else{
				$val["WAVfile"] = "on";
			}

			if($username != "admin"){
				if($para_sys["hide_phone"] == "yes"){
					$val["phone"] = substr($val["phone"],0,3)."***".substr($val["phone"],-4);
				}
			}

		}


		if($search_type == "xls"){
			$arrField = array('create_time','did_number','phone','hangup_type','assign_status','assign_user','assign_time','create_user','visit_status','visit_time','intention_did');
			$arrTitle = array('呼叫时间','呼入did号','主叫号码','挂机方式','分配状态','分配人员','分配时间','资料所属坐席','回访状态','回访时间','媒体意向');
			$xls_count = count($arrField);
			$excelTiele = "热线资源".date("Y-m-d");
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($start_time){
			$arrT["start_time"] = $start_time;
		}
		if($end_time){
			$arrT["end_time"] = $end_time;
		}
		echo json_encode($arrT);
	}

	function memberLevel(){
		$customer_fields = new Model("customer_fields");
		$fieldData = $customer_fields->order("field_order asc")->where("en_name = 'member_Level'")->find();
		$selectTpl = json_decode($fieldData["field_values"] ,true);

		foreach($selectTpl as $vm){
			$arrF[$vm[0]] = $vm[1];
		}
		$arrF["failure"] = "不成单";
		//dump($arrF);die;
		return $arrF;
	}

	function getHotlineDid(){
		$hotline_did = M("hotline_did");
		$fields = "d.id,d.did_num,s.source_name";
		$arrData = $hotline_did->table("hotline_did d")->field($fields)->join("hotline_media_sources s on (d.media_sources_id = s.id)")->select();
		//echo $hotline_did->getLastSql();
		$arrF = array();
		foreach($arrData as $key=>$val){
			$arrF[$val["id"]] = $val["source_name"];
		}
		//dump($arrData);die;
		return $arrF;
	}

	//分配热线
	function assignHotline(){
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST["id"];
		$arrId = explode(",",$id);
		$count_rows = count($arrId);
		foreach($arrId as $key=>&$val){
			$arrF[] = array("id"=>$val);
		}
		$workname = $_REQUEST["deptuser"];
		$arrU = explode(",",$workname);
		foreach($arrU as &$val){
			if($val){
				$workname_arr[] = $val;
			}
		}
		$count_user = count($workname_arr);
		$user_avg = floor($count_rows/$count_user);

		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];

		foreach($arrF as $key=>&$val){
			$yushu = (floor($key/$user_avg))%$count_user;
			$val["workname"] = $workname_arr[$yushu];
		}

		$arrData = $this->groupBy($arrF,"workname");
		$hotline_record = M("hotline_record");
		foreach($arrData as $key=>$val){
			$arrUser[] = $key;
			$arrUserId[$key] = $val;
			$arr = array(
				"assign_user"=>$username,
				"create_user"=>$key,
				"assign_status"=>"Y",
				"assign_time"=>date("Y-m-d H:i:s"),
			);
			$result[] = $hotline_record->data($arr)->where("id in (".$val.")")->save();

		}

		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"热线资源分配成功","user_name"=>$arrUser,"id"=>$arrUserId));
		} else {
			echo json_encode(array('msg'=>'出现未知错误！'));
		}
	}

	function groupBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row["id"];
		}
		foreach($ret as $key=>$val){
			$arrT[$key] = implode(",",$ret[$key]);
		}
		//dump($arrT);die;
		return $arrT;
	}

	function editHotline(){
		checkLogin();
		$hotline_id = $_REQUEST["hotline_id"];
		$hotline_record = M("hotline_record");
		$arrData = $hotline_record->where("id = '$hotline_id'")->find();

		$this->assign("arrData",$arrData);

		$this->display();
	}

	function updateVisit(){
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST['id'];
		$hotline_record = M("hotline_record");
		$arrData = array(
			'visit_status'=>$_REQUEST['visit_status2'],
			'visit_time'=>date("Y-m-d H:i:s"),
			'visit_content'=>$_REQUEST['visit_content'],
			'uniqueid'=>$_SESSION[$username."_hotline"][$id],
		);
		$result = $hotline_record->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteHotlineResources(){
		$id = $_REQUEST["id"];
		$hotline_record = M("hotline_record");
		$result = $hotline_record->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

}
?>
