<?php
//商品分类
class ShopCategoryAction extends Action{
	function categoryList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Categories";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$this->display();
	}

	function categoryData(){
		$category = new Model("shop_category");
		import('ORG.Util.Page');
		$count = $category->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$categoryData = $category->order("sort_orderid desc")->field("id,cat_name as text,cat_pid,sort_orderid,cat_enabled,measure_unit")->limit($page->firstRow.','.$page->listRows)->select();


		$i = 0;
		$row = array('Y'=>'是','N'=>'否');
		foreach($categoryData as &$val){
			$type = $row[$val['cat_enabled']];
			$val['cat_enabled'] = $type;
			$categoryData[$i]['state'] = "closed";
			$i++;
		}
		unset($i);

		$arrTree = $this->getTree($categoryData,0);

		$j = 0;
		foreach($arrTree as $v){
			//if( $arrTree[$j]["cat_pid"] == "0" && !$arrTree[$j]["children"]){
			if( $arrTree[$j]["cat_pid"] == "0"){
				$arrTree[$j]['iconCls'] = "shop";  //顶级分类的图标，叶子节点的图标在getTree()方法中设置。
			}
			$j++;
		}
		unset($j);

		/*
		//打开时只有顶级部门中 有子部门的 是闭合状态，其他的都是是打开状态
		$i = 0;
		foreach($arrTree as $v){
			if( is_array($arrTree[$i]['children'])==true ){
				$arrTree[$i]['state'] = "closed";
			}
			$i++;
		}
		unset($i);
		*/

		//dump($arrTree);die;
		//$strJSON = json_encode($arrTree);
		//echo ($strJSON);

		$rowsList = count($arrTree) ? $arrTree : false;
		$arrCat["total"] = $count;
		$arrCat["rows"] = $rowsList;
		//dump($arrTree);die;
		echo json_encode($arrCat);
	}

	function insertCategory(){
		$category = new Model("shop_category");
		$arrData = array(
			"cat_name" => $_REQUEST["text"],
			"cat_pid" => $_REQUEST["cat_pid"],
			"sort_orderid" => $_REQUEST["sort_orderid"],
			"cat_enabled" => $_REQUEST["cat_enabled"],
			"measure_unit" => $_REQUEST["measure_unit"],
		);
		$result = $category->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'分类添加成功！'));
		} else {
			echo json_encode(array('msg'=>'分类添加失败！'));
		}
	}

	function updateCategory(){
		$id = $_REQUEST['id'];
		$category = new Model("shop_category");
		$arrData = array(
			"cat_name" => $_REQUEST["text"],
			"cat_pid" => $_REQUEST["cat_pid"],
			"sort_orderid" => $_REQUEST["sort_orderid"],
			"cat_enabled" => $_REQUEST["cat_enabled"],
			"measure_unit" => $_REQUEST["measure_unit"],
		);
		$result = $category->data($arrData)->where("id = $id")->save();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteCategory(){
		$id = $_REQUEST["id"];
		$category = new Model("shop_category");
		$result = $category->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function getTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['cat_pid'] == $pId)    {
				$v['children'] = $this->getTree($data, $v['id']);

				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				/*
				if ( empty($v["children"]) && $v['state'] =='closed'){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}
				if ( $v["children"]  && $v['state'] =='closed'){
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				*/
				if ( empty($v["children"])){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}else{
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	function cateTree(){
		$category = new Model("shop_category");

		$categoryData = $category->order("sort_orderid desc")->field("id,cat_name as text,cat_pid,cat_enabled,measure_unit")->select();
		$nodeStatus = $_GET['nodeStatus'];
		if($nodeStatus == 'colse'){
			$i = 0;
			foreach($categoryData as $v){
				$categoryData[$i]['state'] = "closed";
				$i++;
			}
			unset($i);
		}

		$arrTree = $this->getTree($categoryData,0);
		if( $nodeStatus != 'other' && $nodeStatus !== "fitting"){
			$allCate = array(
				"text"=>"顶级分类",
				"id"=>"0",
				"cat_pid"=>"0",
				"iconCls"=>"shop",
			);
			array_unshift($arrTree,$allCate);
		}
		if($nodeStatus == "fitting"){
			$allCate = array(
				"text"=>"所有分类",
				"id"=>"0",
				"cat_pid"=>"0",
				"iconCls"=>"shop",
			);
			array_unshift($arrTree,$allCate);
		}

		if($nodeStatus == "open"){
			$j = 0;
			foreach($arrTree as $v){
				//if( $arrTree[$j]["cat_pid"] == "0" && !$arrTree[$j]["children"]){
				if( $arrTree[$j]["cat_pid"] == "0"){
					$arrTree[$j]['iconCls'] = "shop";
				}
				$j++;
			}
			unset($j);
		}
		//dump($categoryData);
		$arr = array("id"=>"","text"=>"请选择...");
		array_unshift($arrTree,$arr);
		echo  json_encode($arrTree);
	}
}

?>
