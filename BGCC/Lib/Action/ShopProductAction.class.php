<?php
//商品列表
class ShopProductAction extends Action{
	function goodsList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Product List";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();var_dump($user_name2,$arrAdmin);
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function productData(){
		header("Content-Type:text/html; charset=utf-8");
		$good_name = $_REQUEST['good_name'];
		$good_num = $_REQUEST['good_num'];
		$price_da = $_REQUEST['price_da'];
		$price_xiao = $_REQUEST['price_xiao'];
		$cate_id = $_REQUEST['cate_id'];
		$brand_id = $_REQUEST['brand_id'];
		$type_id = $_REQUEST['type_id'];

		$arrCat = $this->getCategoryArray();
		$category = $this->getMeAndSubCategoryID($arrCat,$cate_id);
		$cat_id = rtrim($category,",");

		//dump($_REQUEST);die;

		$where = "1 ";
		$where .= empty($good_name)?"":" AND g.good_name like '%$good_name%'";
		$where .= empty($good_num)?"":" AND g.good_num like '%$good_num%'";
		$where .= empty($price_da)?"":" AND g.price > '$price_da'";
		$where .= empty($price_xiao)?"":" AND g.price < '$price_xiao'";
		//$where .= empty($cate_id)?"":" AND g.cate_id = '$cate_id'";
		$where .= empty($cate_id)?"":" AND g.cate_id in ($cat_id)";
		$where .= empty($brand_id)?"":" AND g.brand_id = '$brand_id'";
		$where .= empty($type_id)?"":" AND g.good_type = '$type_id'";
		/*
		$arrAttr = require "BGCC/Conf/shopAttr.php";
		foreach($arrAttr as $val){
			if($val["type_id"] == $type_id){
				$$val["attr_en_name"] = $_REQUEST[$val["attr_en_name"]];
				$arrF[] = array("name"=>$val["attr_en_name"],"value"=>$_REQUEST[$val["attr_en_name"]]);
				$arrT[] = $val["attr_en_name"].":".$_REQUEST[$val["attr_en_name"]];
			}
		}
		dump($arrT);
		dump($arrF);
		*/
		$goods = new Model("shop_goods");
		$count = $goods->table("shop_goods g")->field("g.id,g.good_name,g.good_num,g.good_Inventory,g.price,g.cate_id,g.brand_id,g.simple_description,g.good_description,g.good_order,g.good_enabled,g.good_type,g.good_attribute,c.cat_name,b.brand_name,t.caty_name")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->join("shop_type t on g.good_type =  t.id")->where($where)->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$goodsData = $goods->table("shop_goods g")->order("g.good_order desc")->field("g.id,g.good_name,g.good_num,g.good_Inventory,g.price,g.cate_id,g.brand_id,g.simple_description,g.good_description,g.good_order,g.good_enabled,g.good_type,g.good_attribute,c.cat_name,b.brand_name,t.caty_name")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->join("shop_type t on g.good_type =  t.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//->order("g.good_order desc,g.createtime desc")
		//echo $goods->getLastSql();die;

		foreach($goodsData as &$val){
			$val["good_attribute"] = json_decode($val["good_attribute"],true);

			foreach($val["good_attribute"] as &$vm){
				$vm["name"] = $vm["attribute_name"].":".$vm["attribute_value"];
				$val["attribute"] .= $vm["attribute_label"].": ".$vm["attribute_value"].",  ";
			}
			$val["attribute"] = rtrim($val["attribute"],",  ");
		}
		//dump($goodsData);die;
		$rowsList = count($goodsData) ? $goodsData : false;
		$arrProduct["total"] = $count;
		$arrProduct["rows"] = $rowsList;
		//dump($arrProduct);die;
		echo json_encode($arrProduct);

	}

	function addProduct(){
		checkLogin();
		$brand = new Model("shop_brand");
		$blist = $brand->where("brand_enabled = 'Y'")->select();
		$this->assign("blist",$blist);
		$this->display();
	}

	function addAttribute($id){
		$shop_attribute = new Model("shop_attribute");
		$arrData = $shop_attribute->where("type_id = '$id'")->select();

		foreach($arrData as $val){
			//$fieldValue[$val[attr_en_name]] = $_POST[$val[attr_en_name]];
			$fieldValue[] = array("attribute_label"=>$val["attr_name"],"attribute_name"=>$val["attr_en_name"],"attribute_value"=>$_POST[$val["attr_en_name"]],"text_type"=>$val["text_type"],"select_value"=>$val["attr_values"]);
		}
		$strAttribute = json_encode($fieldValue);
		//dump($fieldValue);die;
		return $strAttribute;
	}

	//添加订单是商品属性
	function orderAttribute(){
		$id = $_POST["id"];
		//$good_attribute = $this->addAttribute($id);
		$goods = new Model("shop_goods");
		$attrData = $goods->where("id = '$id'")->find();
		echo $attrData['good_attribute'];
	}

	function insertProduct(){
		$type_id = $_REQUEST['type_id'];
		$good_attribute = $this->addAttribute($type_id);
		//dump($good_attribute);die;

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize ='1000000';
		$path = "include/images/shop/product/";
		$this->mkdirs($path);
		$upload->savePath= $path;  //上传路径

		$upload->saveRule=uniqid;    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('jpg','jpeg','png','gif');    //准许上传的文件后缀
		$upload->allowTypes=array('image/png','image/jpg','image/pjpeg','image/gif','image/jpeg');  //检测mime类型

		$upload->thumb=true;    //是否需要对图片文件进行缩略图处理，默认为false
		$thumb_path = 'include/images/shop/product/thumb/';
		$this->mkdirs($thumb_path);
		$upload->thumbPath = $thumb_path;
		/*
		$upload->thumbMaxWidth='300,500';  //以字串格式来传，如果你希望有多个，那就在此处，用,分格，写上多个最大宽
		$upload->thumbMaxHeight='200,400';	//最大高度
		$upload->thumbPrefix='s_,m_';//缩略图文件前缀
		*/
		$upload->thumbMaxWidth='100';
		$upload->thumbMaxHeight='60';
		$upload->thumbPrefix='shop_';


		$goods = new Model("shop_goods");
		$good_num = $_REQUEST['good_num'];
		$num_count = $goods->where("good_num ='$good_num'")->count();
		if($num_count>0){
			$user_mess = "该商品货号已存在！";
			echo json_encode(array('msg'=>$user_mess));
			exit;
		}

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = array(
					"good_type"=>$type_id,
					"good_attribute"=>$good_attribute,
					"good_name"=>$_REQUEST['good_name'],
					"good_num"=>$_REQUEST['good_num'],
					"good_Inventory"=>$_REQUEST['good_Inventory'],
					"cate_id"=>$_REQUEST['cate_id'],
					"brand_id"=>$_REQUEST['brand_id'],
					"price"=>$_REQUEST['price'],
					"good_description"=>$_REQUEST['good_description'],
					"simple_description"=>$_REQUEST['simple_description'],
					"good_order"=>$_REQUEST['good_order'],
					"good_enabled"=>$_REQUEST['good_enabled'],
					"createtime"=>date("Y-m-d H:i:s"),
				);
				$result = $goods->data($arrData)->add();
				if ($result){
					//$this->goodsAttr($result,$type_id);
					echo json_encode(array('success'=>true,'msg'=>'商品添加成功！','lastid'=>$result));
				} else {
					echo json_encode(array('msg'=>'商品添加失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess."<br>  只支持jpg、png、gif格式的图片!"));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData = array(
				"good_type"=>$type_id,
				"good_attribute"=>$good_attribute,
				"good_name"=>$_REQUEST['good_name'],
				"good_num"=>$_REQUEST['good_num'],
				"good_Inventory"=>$_REQUEST['good_Inventory'],
				"cate_id"=>$_REQUEST['cate_id'],
				"brand_id"=>$_REQUEST['brand_id'],
				"price"=>$_REQUEST['price'],
				"good_img"=>$info[0]["savename"],    //原图名称
				"original_img"=>"shop_".$info[0]["savename"],  //缩略图名称
				"img_path"=>$info[0]["savepath"],  //原图路径
				"thumb_path"=>$info[0]["savepath"]."thumb/",  //缩略图路径
				"good_description"=>$_REQUEST['good_description'],
				"simple_description"=>$_REQUEST['simple_description'],
				"good_order"=>$_REQUEST['good_order'],
				"good_enabled"=>$_REQUEST['good_enabled'],
				"createtime"=>date("Y-m-d H:i:s"),
			);
			//dump($info);
			//dump($arrData);die;
			$result = $goods->data($arrData)->add();
			if ($result){
				//$this->goodsAttr($result,$type_id);
				echo json_encode(array('success'=>true,'msg'=>'商品添加成功！'));
			} else {
				echo json_encode(array('msg'=>'商品添加失败！'));
			}
		}

	}


	function editProduct(){
		checkLogin();
		$id = $_REQUEST['id'];
		$brand = new Model("shop_brand");
		$blist = $brand->where("brand_enabled = 'Y'")->select();
		$this->assign("blist",$blist);
		$this->assign("id",$id);

		$goods = new Model("shop_goods");
		$gData = $goods->where("id = '$id'")->find();
		$this->assign("gData",$gData);
		$good_attribute = json_decode($gData['good_attribute'] ,true);

		foreach($good_attribute as $val){
			if($val['text_type'] == "1"){
				$textAttr[] = $val;
			}
			if($val['text_type'] == "2"){
				$val["select_value"] = explode(",",$val["select_value"]);
				$selectAttr[] = $val;

			}
			if($val['text_type'] == "3"){
				$areaAttr[] = $val;
			}
		}
		$this->assign("textAttr",$textAttr);
		$this->assign("selectAttr",$selectAttr);
		$this->assign("areaAttr",$areaAttr);
		//dump($textAttr);
		//dump($selectAttr);
		//dump($selectAttr);die;

		$fit = new Model("shop_group_goods");
		$fdata = $fit->where("parent_id = '$id'")->find();
		$this->assign("fdata",$fdata);
		$this->display();
	}

	function updateProduct(){
		$type_id = $_REQUEST['type_id'];
		$good_attribute = $this->addAttribute($type_id);

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->maxSize ='1000000';
		$path = "include/images/shop/product/";
		$this->mkdirs($path);
		$upload->savePath= $path;  //上传路径

		$upload->saveRule=uniqid;    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		$upload->allowExts=array('jpg','jpeg','png','gif');    //准许上传的文件后缀
		$upload->allowTypes=array('image/png','image/jpg','image/pjpeg','image/gif','image/jpeg');  //检测mime类型

		$upload->thumb=true;    //是否需要对图片文件进行缩略图处理，默认为false
		$thumb_path = 'include/images/shop/product/thumb/';
		$this->mkdirs($thumb_path);
		$upload->thumbPath = $thumb_path;
		/*
		$upload->thumbMaxWidth='300,500';  //以字串格式来传，如果你希望有多个，那就在此处，用,分格，写上多个最大宽
		$upload->thumbMaxHeight='200,400';	//最大高度
		$upload->thumbPrefix='s_,m_';//缩略图文件前缀
		*/
		$upload->thumbMaxWidth='100';
		$upload->thumbMaxHeight='60';
		$upload->thumbPrefix='shop_';

		$id = $_REQUEST["id"];
		$goods = new Model("shop_goods");
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = array(
					"good_type"=>$type_id,
					"good_attribute"=>$good_attribute,
					"good_name"=>$_REQUEST['good_name'],
					//"good_num"=>$_REQUEST['good_num'],
					"good_Inventory"=>$_REQUEST['good_Inventory'],
					"cate_id"=>$_REQUEST['cate_id'],
					"brand_id"=>$_REQUEST['brand_id'],
					"price"=>$_REQUEST['price'],
					"good_description"=>$_REQUEST['good_description'],
					"simple_description"=>$_REQUEST['simple_description'],
					"good_order"=>$_REQUEST['good_order'],
					"good_enabled"=>$_REQUEST['good_enabled'],
					"createtime"=>date("Y-m-d H:i:s"),
				);
				$result = $goods->data($arrData)->where("id = '$id'")->save();
				if ($result !== false){
					$shop_goods_attr = new Model("shop_goods_attr");
					$res = $shop_goods_attr->where("goods_id in ($id)")->delete();
					//$this->goodsAttr($id,$type_id);
					echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
				} else {
					echo json_encode(array('msg'=>'更新失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess."<br>  只支持jpg、png、gif格式的图片!"));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData = array(
				"good_type"=>$type_id,
				"good_attribute"=>$good_attribute,
				"good_name"=>$_REQUEST['good_name'],
				//"good_num"=>$_REQUEST['good_num'],
				"good_Inventory"=>$_REQUEST['good_Inventory'],
				"cate_id"=>$_REQUEST['cate_id'],
				"brand_id"=>$_REQUEST['brand_id'],
				"price"=>$_REQUEST['price'],
				"good_img"=>$info[0]["savename"],    //原图名称
				"original_img"=>"shop_".$info[0]["savename"],  //缩略图名称
				"img_path"=>$info[0]["savepath"],  //原图路径
				"thumb_path"=>$info[0]["savepath"]."thumb/",  //缩略图路径
				"good_description"=>$_REQUEST['good_description'],
				"simple_description"=>$_REQUEST['simple_description'],
				"good_order"=>$_REQUEST['good_order'],
				"good_enabled"=>$_REQUEST['good_enabled'],
				"createtime"=>date("Y-m-d H:i:s"),
			);
			//dump($info);
			//dump($arrData);die;
			$res = $goods->where("id = '$id'")->find();
			$name = $res['img_path'].$res['good_img'];
			$name2 = $res['thumb_path'].$res['original_img'];
			if($name){
				unlink($name);
			}
			if($name2){
				unlink($name2);
			}

			$result = $goods->data($arrData)->where("id = '$id'")->save();
			if ($result !== false ){
				$shop_goods_attr = new Model("shop_goods_attr");
				$res = $shop_goods_attr->where("goods_id in ($id)")->delete();
				//$this->goodsAttr($id,$type_id);
				echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}
	}


	function goodsAttr($id,$type_id){
		$shop_attribute = new Model("shop_attribute");
		$arrData = $shop_attribute->where("type_id = '$type_id'")->select();

		foreach($arrData as $val){
			//$fieldValue[$val[attr_en_name]] = $_POST[$val[attr_en_name]];
			$fieldValue[] = array("attribute_label"=>$val["attr_name"],"attribute_name"=>$val["attr_en_name"],"attribute_value"=>$_POST[$val["attr_en_name"]],"text_type"=>$val["text_type"],"select_value"=>$val["attr_values"]);
		}
		foreach($fieldValue as $val){
			$value .= "('".implode("','",$val)."','$id','$type_id'),";
		}
		$sql = "insert into shop_goods_attr(attribute_label,attribute_name,attribute_value,text_type,select_value,goods_id,goods_type_id) values ".trim($value,",");

		$res = $shop_attribute->execute($sql);
		return true;
	}


	function deleteProduct(){
		$id = $_REQUEST["id"];
		$goods = new Model("shop_goods");
		$res = $goods->where("id = '$id'")->find();
		$name = $res['img_path'].$res['good_img'];
		$name2 = $res['thumb_path'].$res['original_img'];
		if($name){
			unlink($name);
		}
		if($name2){
			unlink($name2);
		}

		$fit = new Model("shop_group_goods");
		$resf = $fit->where("parent_id = '$id'")->count();
		if($resf){
			$resfit = $fit->where("parent_id = '$id'")->delete();
		}
		$result = $goods->where("id in ($id)")->delete();
		if ($result){
			$shop_goods_attr = new Model("shop_goods_attr");
			$res = $shop_goods_attr->where("goods_id in ($id)")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}
	}

	function brandCombox(){
		$brand = new Model("shop_brand");
		$arrBrand = $brand->field("id,brand_name as text")->select();
		/*
		$arrTop = array(
			"id" => "0",
			"text" => "所有品牌",
		);
		array_unshift($arrBrand,$arrTop);
		*/
		if($arrBrand){
			$arr = array("id"=>"","text"=>"请选择...");
			array_unshift($arrBrand,$arr);
		}else{
			$arrBrand = array(array("id"=>"","text"=>"请选择..."));
		}
		echo json_encode($arrBrand);
	}


	//配件
	function fittingSearch(){
		$cate_id = $_REQUEST['cate_id'];
		$brand_id = $_REQUEST['brand_id'];
		$name = $_REQUEST['name'];

		$where = "1 ";
		if($cate_id && $cate_id != 0){
			$where .= " AND g.cate_id ='$cate_id'";
		}
		if($brand_id && $brand_id != 0){
			$where .= " AND g.brand_id ='$cate_id'";
		}
		$where .= empty($name)?"":" AND g.good_name ='$name'";

		$goods = new Model("shop_goods");
		$goodsData = $goods->table("shop_goods g")->order("g.good_order desc")->field("g.id,g.good_name,g.good_num,g.good_Inventory,g.price,g.cate_id,g.brand_id,g.simple_description,g.good_description,g.good_order,g.good_enabled,c.cat_name,b.brand_name")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->where($where)->select();

		$i = 0;
		foreach($goodsData as $val){
			$goodsData[$i]['text'] = $val['good_name']."--".$val['price'];
			$i++;
		}
		unset($i);
		echo json_encode($goodsData);
	}


	function insertFitting(){
		//dump($_REQUEST);die;
		$fit = new Model("shop_group_goods");
		$arrData = array(
			"fit_id"=>$_REQUEST['fit_id'],
			"parent_id"=>$_REQUEST['parent_id'],
		);
		$result = $fit->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'配件添加成功！'));
		} else {
			echo json_encode(array('msg'=>'配件添加失败！'));
		}
	}

	function updateFitting(){
		$parent_id = $_REQUEST['parent_id'];

		$fit = new Model("shop_group_goods");
		$arrData = array(
			"fit_id"=>$_REQUEST['fit_id'],
			"fitting"=>$_REQUEST['fitting'],
		);
		$result = $fit->data($arrData)->where("parent_id = '$parent_id'")->save();
		if ($result !== false ){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}


	function getMeAndSubCategoryID($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubCategoryID($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别分类的id
    */
    function getCategoryArray(){
        $DepTree = array();//一维数组
        $cate = new Model("shop_category");
        $arr = $cate->select();
        foreach($arr AS $v){
            $currentId = $v['id'];
            $arrSonId = $cate->field('id')->where("cat_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['id'],
                "pid" => $v['cat_pid'],
                "name"=> $v['cat_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }

	function attributeValues(){
		$id = $_POST['id'];
		$shop_attribute = new Model("shop_attribute");
		$arrData = $shop_attribute->where("type_id = '$id'")->select();
		echo json_encode($arrData);
	}

}

?>
