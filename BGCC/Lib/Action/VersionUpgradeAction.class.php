<?php
class VersionUpgradeAction extends Action{
	function installVersion(){
		checkLogin();
		$sys = new Model("system_set");
		$ver = $sys->where("name = 'version_num'")->find();
		$this->assign("version",$ver["svalue"]);

		$para_sys = readS();
		if($para_sys["Secondary_development"] == "Y"){
			$this->assign("develop","Y");
		}else{
			$this->assign("develop","N");
		}

		$this->display();
	}

	function DoInstallVersion(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$install = $_REQUEST["install"];
		$vpath_tmp = "/var/www/html/data/attechment/version_tmp/";
		$vpath_install = "data/attechment/install_version/";
		$this->mkdirs($vpath_tmp);
		$this->mkdirs($vpath_install);
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$upload->savePath= $vpath_tmp;  //上传路径

		//$upload->saveRule = uniqid;
		//如果存在同名文件是否进行覆盖
		$upload->uploadReplace=true;
		$upload->allowExts=array('gz'); //准许上传的文件后缀

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			echo json_encode(array('msg'=>$mess."<br>  只允许上传.tar.gz格式的文件!"));
		}else{
			if($install){
				$info=$upload->getUploadFileInfo();
				$file = $info[0]["savename"];
				$version_num = array_pop( explode("-",array_shift( explode(".",$file) )) );
				$time = explode("-",array_shift( explode(".",$file) ));

				$sys = new Model("system_set");
				$ver = $sys->where("name = 'version_num'")->find();
				$ver_num = array_pop(explode("-",$ver["svalue"]));
				if($ver_num != $time[2]){
					echo json_encode(array('msg'=>'您上传的版本错误,只能按版本顺序升级！'));
					die;
				}

				$cmd = "/bin/tar --overwrite -zxf $vpath_tmp$file -C /var/www/html";
				$res = exec($cmd);
				if( $res == 0 || $res == ""){
					$num = $sys->where("name = 'version_num'")->save(array("svalue"=>$time[3]."-".$version_num));
					if(file_exists("/var/www/html/version_msg.php")){
						$mage = require "/var/www/html/version_msg.php";
					}else{
						$mage = "";
					}
					exec("cd $vpath_tmp;rm -rf $file");


					//修改数据库结构=====开始===================
					$filename = "/var/www/html/BGCC/Conf/sqlUpdate.php";
					if(file_exists($filename)){
						$conn = M();
						$config = require '/var/www/html/BGCC/Conf/sqlUpdate.php';

						foreach($config as $key=>$v){
							if($key > $time[2] && $key <= $time[4]){
								foreach(explode(";",$v) as $vm){
									$conn->query($vm);
								}
							}
						}
						unlink('/var/www/html/BGCC/Conf/sqlUpdate.php');
					}
					//修改数据库结构=====开始===================


					//导入第三方类库====开始=================
					if(file_exists("/var/www/html/BGCC/Conf/VersionFunction.class.php")){
						$version = require '/var/www/html/BGCC/Conf/VersionFunction.class.php';
						$arr =  new VersionFunction();
						$aryFunc = get_class_methods($arr);   //返回由类的方法名组成的数组
						$aryAttr = get_class_vars(get_class($arr));  ///返回由类的属性名组成的数组
						/*
						$obj = new VersionFunction();
						$ref = new ReflectionObject($obj);
						$aa = $ref->getProperties();
						$tt = $ref->getMethods();
						*/

						foreach($aryFunc as $val){
							$func[] = explode("_",$val);
						}

						foreach($func as &$val){
							$val['name'] = $val[0]."_".$val[1];
							if($val[1] > $time[2] && $val[1] <= $time[4]){
								$arr->$val['name']();
							}
						}
						unlink('/var/www/html/BGCC/Conf/VersionFunction.class.php');
					}
					//导入第三方类库====结束=================



					//导入sql文件=====开始===================
					$files = scandir("/var/www/html/BGCC/Conf/");
					array_shift($files);
					array_shift($files);
					for($i = 0;$i < count($files);$i++){
						if(preg_match('/(.*)(\.)sql$/i',$files[$i])){
							$filenames[] = $files[$i];
						}
						//echo $files[$i]."<br>";
					}
					if($filenames){
						foreach($filenames as $val){
							$arrFile[] = explode(".",$val);
						}
						if($arrFile){
							import('ORG.Util.DBManager');
							$dbM = new DBManager(C("DB_HOST"),C("DB_USER"),C("DB_PWD"),C("DB_NAME"));
							foreach($arrFile as $val){
								$dbM->createFromFile('/var/www/html/BGCC/Conf/'.$val[0].'.sql',null,'');
								unlink('/var/www/html/BGCC/Conf/'.$val[0].'.sql');
							}
						}
					}

					//dump($arrFile);die;
					/*
					if(file_exists("/var/www/html/BGCC/Conf/city.sql")){
						import('ORG.Util.DBManager');
						$dbM->createFromFile('/var/www/html/BGCC/Conf/city.sql',null,'');
						unlink('/var/www/html/BGCC/Conf/city.sql');
					}
					if(file_exists("/var/www/html/BGCC/Conf/new_table.sql")){
						import('ORG.Util.DBManager');
						$dbM->createFromFile('/var/www/html/BGCC/Conf/new_table.sql',null,'');
						unlink('/var/www/html/BGCC/Conf/new_table.sql');
					}
					if(file_exists("/var/www/html/BGCC/Conf/new_table2.sql")){
						import('ORG.Util.DBManager');
						$dbM->createFromFile('/var/www/html/BGCC/Conf/new_table2.sql',null,'');
						unlink('/var/www/html/BGCC/Conf/new_table2.sql');
					}
					if(file_exists("/var/www/html/BGCC/Conf/new_table3.sql")){
						import('ORG.Util.DBManager');
						$dbM->createFromFile('/var/www/html/BGCC/Conf/new_table3.sql',null,'');
						unlink('/var/www/html/BGCC/Conf/new_table3.sql');
					}
					*/
					//导入sql文件=====结束===================


					if(file_exists("/var/www/html/other.tar.gz")){
						exec("sudo /bin/tar  --overwrite -zxf /var/www/html/other.tar.gz -C /");
						unlink('/var/www/html/other.tar.gz');
					}

					unlink("/var/www/html/version.php");
					unlink("/var/www/html/version_msg.php");
					/*
					$arrConf = require '/var/www/html/BGCC/Conf/system.php';
					$arrConf['version_num'] = $time[3]."-".$time[4];
					$content = "<?php\nreturn " .var_export($arrConf,TRUE) .";\n ?>";
					file_put_contents('/var/www/html/BGCC/Conf/system.php',$content);
					*/
					$this->updateSystemparameter();
					$this->fieldCache($file);

					echo json_encode(array('success'=>true,'msg'=>'升级成功！','massage'=>$mage));
				}
			}else{
				echo json_encode(array('success'=>true,'msg'=>'文件上传成功！'));
			}
		}
	}

	//设置缓存
	function updateSystemparameter(){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];

		$sys=new Model("system_set");
		$arr = $sys->select();

		foreach($arr as $key => $val){
			$tmp[$val["name"]]	= $val["svalue"];
		}
		F('system',$tmp,"BGCC/Conf/crm/$db_name/");
	}

	function fieldCache($file){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = array();
		if( file_exists("BGCC/Conf/versionRecord.php") ){
			$arrF = require "BGCC/Conf/versionRecord.php";
		}
		if($arrF){
			$arr = array("name"=>$file,"upgrade_time"=>date("Y-m-d H:i:s"));
			array_push($arrF,$arr);
		}else{
			$arrF = array(array("name"=>$file,"upgrade_time"=>date("Y-m-d H:i:s")));
		}

		//dump($arrF);die;
		$content = "<?php\nreturn " .var_export($arrF,true) .";\n ?>";
		file_put_contents("BGCC/Conf/versionRecord.php",$content);

		$customer_fields = new Model("customer_fields");
		$fieldData = $customer_fields->order("field_order asc")->select();
		F('fieldCache',$fieldData,"BGCC/Conf/crm/$db_name/");
	}

	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}

	//联网升级---自动升级
	function automaticUpgrades(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$file = $_REQUEST["filename"];
		$version_num = array_pop( explode("-",array_shift( explode(".",$file) )) );
		$time = explode("-",array_shift( explode(".",$file) ));

		$sys = new Model("system_set");
		$ver = $sys->where("name = 'version_num'")->find();
		$ver_num = array_pop(explode("-",$ver["svalue"]));
		if($ver_num != $time[2]){
			echo json_encode(array('msg'=>'您上传的版本错误,只能按版本顺序升级！'));
			die;
		}
		$vpath_tmp = "/var/www/html/data/version/";
		$cmd = "/bin/tar --overwrite -zxf $vpath_tmp$file -C /var/www/html";
		//dump($cmd);die;
		$res = exec($cmd);
		$sys = new Model("system_set");
		$num = $sys->where("name = 'version_num'")->save(array("svalue"=>$time[3]."-".$version_num));
		if( $res == 0 || $res == ""){
			if(file_exists("/var/www/html/version_msg.php")){
				$mage = require "/var/www/html/version_msg.php";
			}else{
				$mage = "";
			}
			exec("cd $vpath_tmp;rm -rf $file");

			$filename = "/var/www/html/BGCC/Conf/sqlUpdate.php";
			if(file_exists($filename)){
				$conn = M();
				$config = require '/var/www/html/BGCC/Conf/sqlUpdate.php';

				foreach($config as $key=>$v){
					if($key > $time[2] && $key <= $time[4]){
						foreach(explode(";",$v) as $vm){
							$conn->query($vm);
						}
					}
				}
				unlink('/var/www/html/BGCC/Conf/sqlUpdate.php');
			}

			//导入sql文件=====开始===================
			$files = scandir("/var/www/html/BGCC/Conf/");
			array_shift($files);
			array_shift($files);
			for($i = 0;$i < count($files);$i++){
				if(preg_match('/(.*)(\.)sql$/i',$files[$i])){
					$filenames[] = $files[$i];
				}
				//echo $files[$i]."<br>";
			}
			if($filenames){
				foreach($filenames as $val){
					$arrFile[] = explode(".",$val);
				}
				if($arrFile){
					import('ORG.Util.DBManager');
					$dbM = new DBManager(C("DB_HOST"),C("DB_USER"),C("DB_PWD"),C("DB_NAME"));
					foreach($arrFile as $val){
						$dbM->createFromFile('/var/www/html/BGCC/Conf/'.$val[0].'.sql',null,'');
						unlink('/var/www/html/BGCC/Conf/'.$val[0].'.sql');
					}
				}
			}

			//导入sql文件=====结束===================

			//导入第三方类库====开始=================
			if(file_exists("/var/www/html/BGCC/Conf/VersionFunction.class.php")){
				$version = require '/var/www/html/BGCC/Conf/VersionFunction.class.php';
				$arr =  new VersionFunction();
				$aryFunc = get_class_methods($arr);   //返回由类的方法名组成的数组
				$aryAttr = get_class_vars(get_class($arr));  ///返回由类的属性名组成的数组
				/*
				$obj = new VersionFunction();
				$ref = new ReflectionObject($obj);
				$aa = $ref->getProperties();
				$tt = $ref->getMethods();
				*/

				foreach($aryFunc as $val){
					$func[] = explode("_",$val);
				}

				foreach($func as &$val){
					$val['name'] = $val[0]."_".$val[1];
					if($val[1] > $time[2] && $val[1] <= $time[4]){
						$arr->$val['name']();
					}
				}
				unlink('/var/www/html/BGCC/Conf/VersionFunction.class.php');
			}
			//导入第三方类库====结束=================

			if(file_exists("/var/www/html/other.tar.gz")){
				exec("sudo /bin/tar  --overwrite -zxf /var/www/html/other.tar.gz -C /");
				unlink('/var/www/html/other.tar.gz');
			}

			unlink("/var/www/html/version.php");
			unlink("/var/www/html/version_msg.php");
			/*
			$arrConf = require '/var/www/html/BGCC/Conf/system.php';
			$arrConf['version_num'] = $time[3]."-".$time[4];
			$content = "<?php\nreturn " .var_export($arrConf,TRUE) .";\n ?>";
			file_put_contents('/var/www/html/BGCC/Conf/system.php',$content);
			*/
			$this->updateSystemparameter();
			$this->fieldCache($file);
			echo json_encode(array('success'=>true,'msg'=>'升级成功！','massage'=>$mage));
		}
	}



	//二次开发升级
	function developmentUpgrades(){
		$fileName = "/tmp/secondaryDevelopment/secondaryDevelopment.tar.gz";
		$cmd = "/bin/tar --overwrite -zxf $fileName -C /var/www/html";
		//dump($cmd);die;
		$res = exec($cmd);
		if( $res == 0 || $res == ""){
			if(file_exists("/var/www/html/version_msg.php")){
				$mage = require "/var/www/html/version_msg.php";
			}else{
				$mage = "";
			}
			if(file_exists("/var/www/html/other.tar.gz")){
				exec("sudo /bin/tar  --overwrite -zxf /var/www/html/other.tar.gz -C /");
				unlink('/var/www/html/other.tar.gz');
			}

			unlink("/var/www/html/version_msg.php");
			$this->updateSystemparameter();
			$this->fieldCache($fileName);
			echo json_encode(array('success'=>true,'msg'=>'升级成功！','massage'=>$mage));
		}
	}

	//强制升级
	function forcedUpgrades(){
		set_time_limit(0);
		ini_set('memory_limit','-1');
		$file = $_REQUEST["name"];
		$uptype = $_REQUEST["uptype"];

		//$file = "robotforced-20140522-635-20140626-725.tar.gz";
		$version_num = array_pop( explode("-",array_shift( explode(".",$file) )) );
		$time = explode("-",array_shift( explode(".",$file) ));
		$newNum = $time[3]."-".$version_num;

		$vpath_tmp = "/var/www/html/data/forcedUpgrade/";
		$cmd = "/bin/tar --overwrite -zxf $vpath_tmp$file -C /var/www/html";
		//dump($uptype);
		//dump($newNum);die;
		$res = exec($cmd);
		if( $res == 0 || $res == ""){
			//exec("cd $vpath_tmp;rm -rf $file");
			if($uptype == "forced"){
				file_put_contents('/var/www/html/BGCC/Conf/forcedUp.txt',$newNum);
			}else{
				$sys = new Model("system_set");
				$num = $sys->where("name = 'version_num'")->save(array("svalue"=>$newNum));
			}

			$filename = "/var/www/html/BGCC/Conf/sqlUpdate.php";
			if(file_exists($filename)){
				$conn = M();
				$config = require '/var/www/html/BGCC/Conf/sqlUpdate.php';

				foreach($config as $key=>$v){
					if($key > $time[2] && $key <= $time[4]){
						foreach(explode(";",$v) as $vm){
							$conn->query($vm);
						}
					}
				}
				unlink('/var/www/html/BGCC/Conf/sqlUpdate.php');
			}

			//导入sql文件=====开始===================
			$files = scandir("/var/www/html/BGCC/Conf/");
			array_shift($files);
			array_shift($files);
			for($i = 0;$i < count($files);$i++){
				if(preg_match('/(.*)(\.)sql$/i',$files[$i])){
					$filenames[] = $files[$i];
				}
				//echo $files[$i]."<br>";
			}
			if($filenames){
				foreach($filenames as $val){
					$arrFile[] = explode(".",$val);
				}
				if($arrFile){
					import('ORG.Util.DBManager');
					$dbM = new DBManager(C("DB_HOST"),C("DB_USER"),C("DB_PWD"),C("DB_NAME"));
					foreach($arrFile as $val){
						$dbM->createFromFile('/var/www/html/BGCC/Conf/'.$val[0].'.sql',null,'');
						unlink('/var/www/html/BGCC/Conf/'.$val[0].'.sql');
					}
				}
			}
			//导入sql文件=====结束===================

			//导入第三方类库====开始=================
			if(file_exists("/var/www/html/BGCC/Conf/VersionFunction.class.php")){
				$version = require '/var/www/html/BGCC/Conf/VersionFunction.class.php';
				$arr =  new VersionFunction();
				$aryFunc = get_class_methods($arr);   //返回由类的方法名组成的数组
				$aryAttr = get_class_vars(get_class($arr));  ///返回由类的属性名组成的数组
				foreach($aryFunc as $val){
					$func[] = explode("_",$val);
				}

				foreach($func as &$val){
					$val['name'] = $val[0]."_".$val[1];
					if($val[1] > $time[2] && $val[1] <= $time[4]){
						$arr->$val['name']();
					}
				}
				unlink('/var/www/html/BGCC/Conf/VersionFunction.class.php');
			}
			//导入第三方类库====结束=================

			if(file_exists("/var/www/html/other.tar.gz")){
				exec("sudo /bin/tar  --overwrite -zxf /var/www/html/other.tar.gz -C /");
				unlink('/var/www/html/other.tar.gz');
			}

			unlink("/var/www/html/version.php");
			unlink("/var/www/html/version_msg.php");
			$this->updateSystemparameter();
			$this->fieldCache($file);
		}
	}

}


?>

