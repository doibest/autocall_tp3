<?php
class BlackListAction extends Action{
	function Blacklist(){
		checkLogin();

		//分配增删改的权限
		$menuname = "Outbound blacklist";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function blackData(){
		$dnc = new Model("sales_dnc");
		$count = $dnc->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$dncList = $dnc->limit($page->firstRow.','.$page->listRows)->select();
		$rowsList = count($dncList) ? $dncList : false;
		$arrDnc["total"] = $count;
		$arrDnc["rows"] = $rowsList;

		echo json_encode($arrDnc);
	}

	function insertBlacklist(){
		$dnc = new Model("sales_dnc");
		$result = $dnc->add( array("phonenumber"=>$_POST['phonenumber']) );
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'黑名单添加成功！'));
		} else {
			echo json_encode(array('msg'=>'黑名单添加失败！'));
		}
	}

	function deleteBlacklist(){
		$id = $_REQUEST["id"];
		$dnc = new Model("sales_dnc");
		$result = $dnc->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function importBlack(){
		set_time_limit(0);
		ini_set('memory_limit','512M');
		if( empty($_FILES["client_phones_txt"]["tmp_name"]) ){
			goBack("请选择文件!","");
		}
		$time = time();
		$tmp_f = $_FILES["client_phones_txt"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["client_phones_txt"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));
		$dst_f = TMP_UPLOAD ."client_phones_".$time.".csv";
		move_uploaded_file($tmp_f,$dst_f);
		$arrData = trim( file_get_contents($dst_f) );

		//$arrData = file_get_contents("BGCC/Conf/black.txt");
		$arrPhones2 = explode("\n",$arrData);
		foreach($arrPhones2 as $val){
			$arrPhones[] = trim($val);
		}
		//dump($arrPhones);die;
		$dnc = new Model("sales_dnc");
		$sql = "insert into sales_dnc(`phonenumber`) values ";

		foreach($arrPhones as $val){
			$str = "('" .$val ."')";
			$value .= empty($value)?$str:",$str";
		}
		$sql .= $value;
		$result = $dnc->execute($sql);//判断导入结果
		if( $result ){
			$total = count($arrPhones);
			echo json_encode(array('success'=>true,'msg'=>"导入成功！总共处理${total}个号码"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}
	}
}

?>
