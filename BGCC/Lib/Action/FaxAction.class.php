<?php
class FaxAction extends Action {
    function listFax(){
		checkLogin();
		$u = $_SESSION["user_info"]["username"];
		$fax_num = $_SESSION["user_info"]["fax"];
		//dump($fax_num);
		import('ORG.Util.Page');
		$fax = new Model("Fax_recvq");
		if($u == "admin"){
			$count = $fax->count();
			$para_sys = readS();
			$page = new Page($count,$para_sys["page_rows"]);
			$page->setConfig("header",L("Records"));
			$page->setConfig("prev",L("Prev"));
			$page->setConfig("next",L("Next"));
			$page->setConfig("first",L("First"));
			$page->setConfig("last",L("Last"));
			$page->setConfig('theme','<span>  %totalRow%%header% &nbsp;  %nowPage%/%totalPage%</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');
			$faxlist = $fax->order("date")->limit($page->firstRow.','.$page->listRows)->select();
		}else{
			$count = $fax->count();
			$para_sys = readS();
			$page = new Page($count,$para_sys["page_rows"]);
			$page->setConfig("header",L("Records"));
			$page->setConfig("prev",L("Prev"));
			$page->setConfig("next",L("Next"));
			$page->setConfig("first",L("First"));
			$page->setConfig("last",L("Last"));
			$page->setConfig('theme','<span>  %totalRow%%header% &nbsp;  %nowPage%/%totalPage%</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');
			$faxlist = $fax->order("date")->limit($page->firstRow.','.$page->listRows)->where("company_fax='$fax_num'")->select();
		}
		$show = $page->show();
		$this->assign("page",$show);
		//dump($faxlist);die;
		$this->assign("faxlist",$faxlist);

		//分配增删改的权限
		$menuname = "Fax";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("Download",$priv['Download']);
		$this->assign("delete",$priv['delete']);

		$this->display();
	}

	function downloadFax(){
        $uniqueid = $_GET['id'];
		$recFilePath = $_GET['path'];

        $timestamp = explode('.',$uniqueid);
        $timestamp = $timestamp[0];
        //$recFilePath = 'c:\\bbb.gif';
       header('HTTP/1.1 200 OK');
        //header('Accept-Ranges: bytes');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download"); // changed to force download
        header("Content-Length: " . (string)(filesize($recFilePath)));
        header("Content-Transfer-Encoding: Binary"); // added
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($recFilePath))."");
        readfile($recFilePath);
    }

	function deleteFax(){
		//$id = $_GET["id"];
		$id = $_POST["id"];
		$fax = new Model("Fax_recvq");
		$delnum = $fax->where("id=$id")->delete();
		//echo $fax->getLastSql();
		if($delnum){
			echo "ok";
		}else{
			$this->error("操作失败！".$fax->getDbError());
		}
	}
}
