<?php
class SystemSetModel extends Model{
    protected $_validate=array(
        array('name','require','数据名称不能为空',1,'regex',3),
        array('description','require','数据描述不能为空',1,'regex',3),
        array('name','','数据名称已存在',1,'unique',3),
        array('allowTmpl',array(0,1),'启用：1 ; 停用：0',0,'in'),
    );
}

?>