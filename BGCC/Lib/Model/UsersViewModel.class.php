<?php
class UsersViewModel extends ViewModel{
    public $viewFields=array(
        'Users'=>array('username','password','extension','d_id'=>'ud_id','r_id'=>'ur_id','cn_name','extension_lock'),
        'Department'=>array('d_id'=>'dd_id','d_name','_on'=>'Department.d_id=Users.d_id'),
        'Role'=>array('r_id'=>'rr_id','r_name','_on'=>'Role.r_id=Users.r_id'),
    );
}