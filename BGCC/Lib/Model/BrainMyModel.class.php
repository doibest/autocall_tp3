<?php

/**
 * Class PbxModel
 * author : robot 8001
 */
class BrainMyModel extends Model {
    protected $_validate=array(
        array('template_name','require','大脑名称不能为空',1,'regex',3),
        array('brain_type_id','require','大脑类型不能为空',1,'regex',3),
        array('industry', 'require', '大脑行业不能为空', 1, 'regex', 3),
        array('dept_id', 'require', '大脑部门不能为空', 1, 'regex', 3)
    );

    public function getAll(){
        return $this->select();
    }

}
