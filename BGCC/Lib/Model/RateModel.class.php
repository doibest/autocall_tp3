<?php
class RateModel extends Model{
    protected $_validate=array(
        array('rate_name','require','费率名称不能为空',1,'regex',3),
        //array('rate_name','','费率名称已存在',1,'unique',3),
        array('rate_type',array(1,2,3),'注意数据，运营商：1 ; 代理商：2；用户：3',0,'in'),
    );
    protected $_auto=array(
        array('create_time','getDate',3,'callback'),
    );
    public function getDate(){
        if(empty($_POST['create_time'])){
            return date("Y-m-d H:i:s");
        }else{
            //需要判断是否是中文
            return $_POST['create_time'];
        }
    }

}
