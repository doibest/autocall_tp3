<?php
class DepartmentModel extends Model{
    protected $_validate=array(
        array('d_name','require','部门名称不能为空',1,'regex',3),
        array('d_name','','部门名称已存在',1,'unique',3),
    );
	protected $_auto=array(
		array('path','getPath',3,'callback'),
	);
    function getPath(){
        $d_pid=$_POST['d_pid'];
        $mi=$this->field('d_id,path')->getById($d_pid);
        $path=$d_pid!=0?$mi['path'].'-'.$mi['d_id']:0;
        return $path;
    }
}

?>