
//正常点击拨号
function pbx_normal_clickcall(myexten,phone){
	var URL = "agent.php?m=Pbx&a=normal_clickcall&exten="+myexten+"&phone="+phone;
	$.ajax({url: URL,
			type: 'GET',
			dataType: 'html',
			error: function(){},
			success: function(result){
				//alert(result);
				if(result == "Blacklist"){
					alert("该号码在黑名单中，无法呼出！");
					return false;
				}else if(result == "Illegal_numbers"){
					alert("非法电话号码或者被叫号码不存在！");
					return false;
				}else if(result == "success"){
					return true;
				}else{
					alert(result);
				}
			}
	});
}
/*
//外呼呼点击拨号
function pbx_preview_clickcall(exten,phone,callerid){
	var URL = "agent.php?m=Pbx&a=preview_clickcall&exten="+exten+"&phone="+phone+"&callerid="+callerid;
	$.ajax({url: URL,
			type: 'GET',
			dataType: 'html',
			error: function(){},
			success: function(result){
				var arr = result.split('!');
				var event = arr[0];
				var exten = arr[1];
				agentStateChange();
			}
	
	});
}
*/
//外呼呼点击拨号
function pbx_preview_clickcall(exten,phone,task_id,trunk,callerid){
	var URL = "agent.php?m=Pbx&a=preview_clickcall&exten="+exten+"&phone="+phone+"&task_id="+task_id+"&trunk="+trunk+"&callerid="+callerid;
	$.ajax({url: URL,
			type: 'GET',
			dataType: 'html',
			error: function(){},
			success: function(result){
				//console.log(result);
				if(result == "Blacklist"){
					alert("该号码在黑名单中，无法呼出！");
					return false;
				}
				if(result == "Illegal_numbers"){
					alert("非法电话号码或者被叫号码不存在！");
					return false;
				}
				var arr = result.split('!');
				var event = arr[0];
				var exten = arr[1];
			}
	
	});
}

//通话暂停
function pbx_callPause(exten){
	var URL = "agent.php?m=Pbx&a=callPause&exten="+exten;
	$.ajax({url: URL,
			type: 'GET',
			dataType: 'html',
			error: function(){},
			success: function(result){
				;
			}
	
	});
}


//通话恢复
function pbx_callRestore(exten){
	var URL = "agent.php?m=Pbx&a=callRestore&exten="+exten;
	$.ajax({url: URL,
			type: 'GET',
			dataType: 'html',
			error: function(){},
			success: function(result){
				;
			}
	
	});
}

//通话转移
function pbx_transfer(exten,phone){
	var URL = "agent.php?m=Pbx&a=transfer&exten=" +exten +"&phone=" +phone;
	$.ajax({url: URL,
			type: 'GET',
			dataType: 'html',
			error: function(){},
			success: function(result){
				;
			}
	
	});
}

//三方通话
function pbx_3waycall(exten,phone){
	var URL = "agent.php?m=Pbx&a=nwaycall&exten=" +exten +"&phone=" +phone;
	$.ajax({url: URL,
			type: 'GET',
			dataType: 'html',
			error: function(){},
			success: function(result){
				;
			}
	
	});
}

function pbx_memberpin(exten){
	var URL = "agent.php?m=Pbx&a=memberpin&exten=" +exten;
	$.ajax({url: URL,
			type: 'GET',
			dataType: 'html',
			error: function(){},
			success: function(result){
				;
			}
	
	});
}

