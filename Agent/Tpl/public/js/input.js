$(function(){
	$('input').focus(function(){
		$('input').removeClass("input_on");
		$(this).addClass("input_on");
		
	});
	$("input").blur(function(){
		$('input').removeClass("input_on");
	});
	$('textarea').focus(function(){
		$('textarea').removeClass("input_on");
		$(this).addClass("textarea_on");
		
	});
	$("textarea").blur(function(){
		$('textarea').removeClass("textarea_on");
	});
	/*
	$('select').focus(function(){
		$('select').removeClass("input_on");
		$(this).addClass("input_on");
		
	});
	$("select").blur(function(){
		$('select').removeClass("input_on");
	});	
	*/
	
	//Input文本框默认说明文字获得焦点后消失效果 .
	/*$('input:text').each(function(){  
		var txt = $(this).val(); 
		$(this).focus(function(){  
			//alert(txt);
			if(txt === $(this).val()) $(this).val("");  
		}).blur(function(){  
			if($(this).val() == "") $(this).val(txt);  
		});  
	})*/  
	//Input文本框 class以input开头的 默认说明文字获得焦点后消失效果 .
	$('input[class^=input]').each(function(){  
		var txt = $(this).val();  
		$(this).focus(function(){  
			if(txt === $(this).val()) $(this).val("");  
		}).blur(function(){  
			if($(this).val() == "") $(this).val(txt);  
		});  
	});
});