/** 
*File Name:validate.js 
*Author:Liao Chun Hai  
*create Version:01.00.000 
*Create Date:2013-07-24 
*modify Version: 
*modify Date: 
**/  
$.extend( $.fn.validatebox.defaults.rules,{ 
	//最小长度
	minLength: {   
		validator: function(value, param){   
			return value.length >= param[0];   
		},   
		message: '至少输入{0}个字符.'  
	},  
	
	//最大长度
	maxLength: {   
		validator: function(value, param){   
			return value.length <= param[0];   
		},   
		message: '最多输入{0}个字符.'  
	},
			
			
			
	//扩展远程调用，为验证过程添加其他参数，默认空值为"-1"  
	//事例：  
	//remoteExtend['taskCfgCtrl.action?cmd=checkNameIsExist','name','taskId']  
	//<input type="hidden" id="taskId" name="taskId"  disabled="disabled" value="-1"></input>，必须设置为disabled，防止提交到后台  
	remoteExtend : {  
		validator : function(value, param) {  
			var data = {};  
			data[param[1]] = value;     //当前value所对应的name  
			  
			var paramName = null;  
			var paramValue = null;  
			for(var i = 2; i < param.length; i ++){  
				paramName = $("#" + param[i]).attr("name");  
				paramValue = $("#" + param[i]).attr("value");  
				if(paramValue == ""){           //默认空值为"-1",防止参数传递转换成Numbe可能出现的异常  
					paramValue = "-1";  
				}  
				  
				data[paramName] = paramValue;  
			}  
			  
			var isValidate = $.ajax({  
						url : param[0], //url  
						dataType : "json",  
						data : data,  
						async : false,  
						cache : false,  
						type : "post"  
					}).responseText;  
			  
			return isValidate == "true";  
		},  
		message : ""  
	},  
	  
	//ip地址验证  
	ipValid : {  
		validator : function(value, param) {  
			var result = false;  
			var dataArray = value.split(".");  
			if(dataArray.length == 4){  
				var num = 0;  
				var index = 0;  
				for(var i = 0; i < 4; i ++){  
					if(dataArray[i] != ""){  
						num = Number(dataArray[i]);  
						if(num <= 255 && num >= 0){  
							index ++;  
						}  
					}  
				}  
				  
				if(index == 4){  
					result = true;  
				}  
			}  
			  
			return result;  
		},  
		message : "格式错误"  
	},  
	  
	//正则表达式验证  
	custRegExp : {  
		validator : function(value, param) {  
			var regExp = new RegExp(eval(param[0]));  
			  
			return regExp.test(value);  
		},  
		message : ""  
	},  
	  
	//空值验证  
	emptyValid:{  
		validator : function(value, param) {  
			return ($.trim(value).length ==0) ? false : true;  
		},  
		message : "不能为空"  
	},  
	  
	//整数判断  
	//事例：  
	//intValid[9],intValid[,9]  表示最小值为9  
	//intValid[0,9] 表示取值范围为0-9  
	//intValid[,9] 表示最大值为9  
	intValid:{  
		validator : function(value, param) {  
			//先验证是否为整数  
			var regExp = new RegExp(/^-?\d+$/);  
			if(!regExp.test(value)){  
				$.fn.validatebox.defaults.rules.intValid.message = "只能输入整数";  
				return false;  
			}  
			  
			var isValueCorrect = true;  //判断指定值是否在某一范围内  
			if(param != null){  
				switch(param.length){  
					case 1:     //intValid[9] 表示最小值为9  
						isValueCorrect = (value >= param[0]);  
						$.fn.validatebox.defaults.rules.intValid.message = "最小值为{0}";  
						break;  
						  
					case 2:       
						if(typeof(param[0]) == "undefined"){    //intValid[,9] 表示最大值为9  
							isValueCorrect = (value <= param[1]);  
							$.fn.validatebox.defaults.rules.intValid.message = "最大值为{1}";  
						}  
						else if(typeof(param[1]) == "undefined"){   //intValid[9,] 表示最小值为9  
							isValueCorrect = (value >= param[0]);  
							$.fn.validatebox.defaults.rules.intValid.message = "最小值为{0}";  
						}  
						else{       //intValid[0,9] 表示取值范围为0-9  
							isValueCorrect =((value >= param[0]) && (value <= param[1]));  
							$.fn.validatebox.defaults.rules.intValid.message = "范围为{0}到{1}";  
						}  
						break;  
						  
					defalut:  
						isValueCorrect = true;  
				}  
			}  
			  
			return isValueCorrect;  
		},  
		message : ""  
	},
	
	
	//整数判断  
	//事例：  
	//intValidExten[9],intValidExten[,9]  表示最小值为9  
	//intValidExten[0,9] 表示取值范围为0-9  
	//intValidExten[,9] 表示最大值为9  
	//intValidExten[,] 表示没有范围
	intValidExten:{  
		validator : function(value, param) {  
			//先验证是否为整数  
			var regExp = new RegExp(/^-?\d+$/);  
			if(!regExp.test(value)){  
				$.fn.validatebox.defaults.rules.intValidExten.message = "只能输入整数";  
				return false;  
			}  
			  
			var isValueCorrect = true;  //判断指定值是否在某一范围内  
			if(param != null){  
				switch(param.length){  
					case 1:     //intValidExten[9] 表示最小值为9  
						if(typeof(param[0]) == "undefined" && typeof(param[1]) == "undefined"){   //intValidExten[9,] 表示最小值为9  
							return true; 
						} else{
							isValueCorrect = (value >= param[0]);  
							$.fn.validatebox.defaults.rules.intValidExten.message = "最小值为{0}";  
						}
						break;  
						  
					case 2:       
						if(typeof(param[0]) == "undefined" && typeof(param[1]) == "undefined"){   //intValidExten[,] 表示没有范围  
							return true; 
						} else if(typeof(param[0]) == "undefined"){    //intValidExten[,9] 表示最大值为9  
							isValueCorrect = (value <= param[1]);  
							$.fn.validatebox.defaults.rules.intValidExten.message = "最大值为{1}";  
						} else if(typeof(param[1]) == "undefined"){   //intValidExten[9,] 表示最小值为9  
							isValueCorrect = (value >= param[0]);  
							$.fn.validatebox.defaults.rules.intValidExten.message = "最小值为{0}";  
						}else{       //intValidExten[0,9] 表示取值范围为0-9  
							isValueCorrect =((value >= param[0]) && (value <= param[1]));  
							$.fn.validatebox.defaults.rules.intValidExten.message = "范围为{0}到{1}";  
						}  
						break;  
						  
					defalut:  
						isValueCorrect = true;  
				}  
			}  
			  
			return isValueCorrect;  
		},  
		message : ""  
	},
	
	
	//密码
    Password: {  
        validator: function (value, param) {  
            if (param != undefined) {  
                if (value.length < param[0] || value.length > param[1]) {  
                    $.fn.validatebox.defaults.rules.Password.message = '长度为{0}到{1}个字符';  
                    return false;  
                }  
            }  
            var reg = /^\w+$/;  
            return reg.exec(value);  
        },  
        message: '包含非法字符(只能输入数字、英文字母、下划线)'  
    },  
    PasswordAgain: {  
		//param传的参数为 password的id值
        validator: function (value, param) {  
            return $.trim(value) == $.trim($("#"+param[0]).val());  
        },  
        message: '两次输入的密码不一致'  
    }, 
	selectCombobx: {  
		//combobox非空验证 param传的参数为 combobox的id值
		validator: function (value, param) {
			//console.log($("#"+param[0]).combobox('getValue'));
			return $("#"+param[0]).combobox('getValue') != "";  
		},  
		message: '请选择下拉框的值！'  
	},	
    faxExten: {  
		//param传的参数为 password的id值
        validator: function (value, param) { 
				var regExp = new RegExp(/^-?\d+$/);  
				if(!regExp.test(value)){
					$.fn.validatebox.defaults.rules.faxExten.message = '只能输入整数';  
					return false;
					
				}  
				if( !($.trim(value) != $.trim($("#"+param[0]).val())) ){
					$.fn.validatebox.defaults.rules.faxExten.message = '分机号和传真号不能一致！';  
					return false;
					
				}
				return $.trim(value) != $.trim($("#"+param[0]).val());  
				
             
        },  
        message: '分机号和传真号不能一致！'  
    },
	
	
	//整数和长度范围
    numLength: {  
        validator: function (value, param) {  
            if (param != undefined) { 
				var regExp = new RegExp(/^-?\d+$/);  
				if(!regExp.test(value)){
					$.fn.validatebox.defaults.rules.numLength.message = '只能输入整数';  
					return false;
					
				}else{
					if (value.length < param[0] || value.length > param[1]) {  
						$.fn.validatebox.defaults.rules.numLength.message = '长度为{0}到{1}个字符';  
						return false;  
					}   
				}				
            }
            var reg = /^\w+$/;  
            return reg.exec(value);  
        },  
        message: ''  
    },
	
	
	//只能输入数字、下划线、-
	telephone: {  
        validator: function (value, param) {  
            if (param != undefined) {
				//var regExp = new RegExp(/^-?\d+$/);  
				var regExp = new RegExp(/^[0-9_\-]+$/);  
				if(!regExp.test(value)){
					$.fn.validatebox.defaults.rules.telephone.message = '包含非法字符(只能输入数字、-)';  
					return false; 
				}else{
					if (value.length < param[0] || value.length > param[1]) {  
						$.fn.validatebox.defaults.rules.telephone.message = '长度为{0}到{1}个字符';  
						return false;  
					}
				}
            }  
            //var reg = /^[0-9\-\u4e00-\u9fa5]+$/;  
            var reg = /^[0-9\-]+$/;  
            return reg.exec(value);  
        },  
        message: '包含非法字符(只能输入数字、-)'  
    },
	
	//包含非法字符(只能输入汉字、数字、英文字母、下划线)
    accountLength: {  
        validator: function (value, param) {  
            if (param != undefined) { 
				var regExp = new RegExp(/^[a-zA-Z0-9_\u4e00-\u9fa5]+$/);  
				//var reg = /^[a-zA-Z0-9_\u4e00-\u9fa5]+$/;  
				//return reg.exec(value); 
				if(!regExp.test(value)){
					$.fn.validatebox.defaults.rules.accountLength.message = '包含非法字符(只能输入汉字、数字、英文字母、下划线)';  
					return false;
					
				}else{
					if (value.length < param[0] || value.length > param[1]) {  
						$.fn.validatebox.defaults.rules.accountLength.message = '长度为{0}到{1}个字符';  
						return false;  
					}   
				}				
            }
			var reg = /^[a-zA-Z0-9_\u4e00-\u9fa5]+$/;  
            return reg.exec(value);  
        },  
        message: '包含非法字符(只能输入汉字、数字、英文字母、下划线)'  
    },
	
	
	//包含非法字符(只能输入汉字、数字、英文字母、下划线)
    accountLengthUser: {  
        validator: function (value, param) {  
            if (param != undefined) { 
				var regExp = new RegExp(/^[a-zA-Z0-9_]+$/);  
				//var reg = /^[a-zA-Z0-9_\u4e00-\u9fa5]+$/;  
				//return reg.exec(value); 
				if(!regExp.test(value)){
					$.fn.validatebox.defaults.rules.accountLengthUser.message = '包含非法字符(只能输入字母、数字、下划线)';  
					return false;
					
				}else{
					if (value.length < param[0] || value.length > param[1]) {  
						$.fn.validatebox.defaults.rules.accountLengthUser.message = '长度为{0}到{1}个字符';  
						return false;  
					}   
				}				
            }
			var reg = /^[a-zA-Z0-9_]+$/;  
            return reg.exec(value);  
        },  
        message: '包含非法字符(只能输入字母、数字、下划线)'  
    },
	
	//组合验证的写法
	combination : {
		validator : function (value, param) {
			var rules = $.fn.validatebox.defaults.rules;
			rules.combination.message = '请输入至少{0}个字符。';
			if(!rules.email.validator(value)){
				rules.combination.message = rules.email.message;
				return false;
			}
			if(!rules.length.validator(value,param)){
				rules.combination.message = rules.length.message;
				return false;
		}
			return value.length >= param[0];
		},
		message : ''
	},
	mobile:{// 验证手机号码
		validator:function(value) {
			return/^(13|15|18|0)\d{9,12}$/i.test(value);
		},
		message:'手机号码格式不正确'
	},
	mobile_phone:{// 验证手机号码
		validator:function(value) {
			return/^(13|15|18|17|14)\d{9}$/i.test(value);
		},
		message:'手机号码格式不正确'
	},
	phone:{// 验证电话号码
		validator:function(value) {
			return/^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value);
		},
		message:'格式不正确,请使用下面格式:020-88888888'
	},
	idcard:{// 验证身份证
		validator:function(value) {
			return/^\d{15}(\d{2}[A-Za-z0-9\*\?])?$/i.test(value);
		},
		message:'身份证号码格式不正确'
	},
	zip:{// 验证邮政编码
		validator:function(value) {
			return/^[1-9]\d{5}$/i.test(value);
		},
		message:'邮政编码格式不正确'
	},
	name:{// 验证姓名，可以是全中文或全英文,不能同时出现中文和英文
		validator:function(value) {
			return/^[\u0391-\uFFE5]+$/i.test(value)|/^\w+[\w\s]+\w+$/i.test(value);
		},
		message:'请输入姓名'
	},
	chinese:{ 
		validator:function(value) {
			//return/^[\u0391-\uFFE5]+$/i.test(value); // 验证中文
			return/^[A-Za-z0-9_\-\u0391-\uFFE5]+$/i.test(value);
		},
		//message:'该项只能输入中文'
		message:'该项只能输入中文、字母、数字'
	},
	english:{
		validator:function(value) {
			//return/^[A-Za-z\-\.]+$/i.test(value);  // 验证英语
			return/^[A-Za-z0-9_\-\.]+$/i.test(value);
		},
		message:'该项只能输入字母、数字、下划线、-！'
	},
	faxno:{// 验证传真
		validator:function(value) {
		// return /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/i.test(value);
			return/^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value);
		},
		message:'传真号码不正确'
	}
	
	
});  