<?php
class AgentChatsAction extends Action{
	function uichatList(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$sinfo = $_SESSION['user_info'];
		$attributes_tpl = $sinfo["cn_name"].",".$sinfo["en_name"].",".$sinfo["extension"].",".$sinfo["phone"].",".$sinfo["username"];


		$users = new Model("users");
		$ulist = $users->field("username")->where("username != '$username'")->select();


		$this->assign("ulist",$ulist);

		$arrAdmin = getAdministratorNum();
		if( in_array($username,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$username);
		}

		$this->assign("cn_name",$cn_name);
		$this->assign("attributes_tpl",$attributes_tpl);


		$this->display();
	}

	//节点展开时的记录
	function chatRecord(){
		$username = $_SESSION['user_info']['username'];
		$othername = $_REQUEST['othername'];

		$my_procedure = $username."->".$othername;
		$other_procedure = $othername."->".$username;
		$filename = "/var/www/html/BGCC/Conf/chat.text";
		if(file_exists($filename)){
			$opfile = fopen($filename,"r");
			$tmp = fread($opfile,filesize($filename));
			$tmp_arr = explode("|",$tmp);

			foreach($tmp_arr as $vm ){
				$arrData[] = explode(",",$vm);
			}

			foreach($arrData as $val){
				if($val[3] == $my_procedure || $val[3] == $other_procedure){
					$tmp_arrData[] = $val;
				}
			}
			$chr = new Model("chat_record");
			$chatContent = $chr->order("createtime asc")->where("procedures = '$my_procedure' OR procedures = '$other_procedure'")->select();

			$i = 0;
			foreach($chatContent as $val){
				$chatContent[$i][0] = $val['createtime'];
				$chatContent[$i][1] = $val['myname'];
				$chatContent[$i][2] = $val['othername'];
				$chatContent[$i][3] = $val['procedures'];
				$chatContent[$i][4] = $val['chat_content'];
				$chatContent[$i][5] = $val['mycn_name'];
				$i++;
			}
			$chat_arrData = count($tmp_arrData) ? $tmp_arrData : $chatContent;
			if($_POST["record_other"] == "my"){
				$record_arrData = array_slice($chat_arrData,-3,3);
				echo json_encode($record_arrData);
			}else{
				$record_arrData = array_slice($chat_arrData,-12,12);
				echo json_encode($record_arrData);
			}
		}else{
			$chr = new Model("chat_record");
			$chatContent = $chr->order("createtime asc")->where("procedures = '$my_procedure' OR procedures = '$other_procedure'")->select();

			$i = 0;
			foreach($chatContent as $val){
				$chatContent[$i][0] = $val['createtime'];
				$chatContent[$i][1] = $val['myname'];
				$chatContent[$i][2] = $val['othername'];
				$chatContent[$i][3] = $val['procedures'];
				$chatContent[$i][4] = $val['chat_content'];
				$chatContent[$i][5] = $val['mycn_name'];
				$i++;
			}
			//dump($chatContent);die;
			if($_POST["record_other"] == "my"){
				$record_arrData = array_slice($chatContent,-3,3);
				echo json_encode($record_arrData);
			}else{
				$record_arrData = array_slice($chatContent,-12,12);
				echo json_encode($record_arrData);
			}
		}

	}

	function chatSearchList(){
		checkLogin();
		//$chr = new Model("chat_record");
		$name = $_REQUEST['name'];
		$myname = $_REQUEST['myname'];
		$this->assign("name",$name);
		$this->assign("myname",$myname);
		$this->display();
	}

	function chatRecordSearch(){
		$chr = new Model("chat_record");
		$name = $_REQUEST["name"];
		$myname = $_REQUEST["myname"];
		$msg = $_REQUEST['msg'];
		$range = $_REQUEST['range'];
		$now = date("Y-m-d H:i:s");
		$date1 = date("Y-m-d");
		if($range){
			if($range == "recent_week"){
				$time = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-7,date("Y")))." 00:00:00";
				$where = " AND createtime > '$time' AND createtime < '$now'";
			}elseif($range == "recent_month"){
				$time = date('Y-m-d',strtotime("$date1 -1 month"))." 00:00:00";
				$where = " AND createtime > '$time' AND createtime < '$now'";
			}elseif($range == "recent_year"){
				$time = date('Y-m-d',strtotime("$date1 -1 year"))." 00:00:00";
				$where = " AND createtime > '$time' AND createtime < '$now'";
			}else{
				$where .= " AND 1 ";
			}
		}else{
			$time = date("Y-m-d",strtotime("-3 day"))." 00:00:00";
			$where = " AND createtime > '$time' AND createtime < '$now'";
		}

		$my_procedure = $name."->".$myname;
		$other_procedure = $myname."->".$name;

		//$where = "1 ";
		//$where = " AND chat_content like '%$msg%'";
		$where .= empty($msg)?"":" AND chat_content like '%$msg%'";

		//dump($name);
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$count = $chr->where("(procedures like '%$my_procedure%' OR procedures like '%$other_procedure%') $where")->count();
		$recordData = $chr->order("createtime asc")->limit($page->firstRow.",".$page->listRows)->where("(procedures like '%$my_procedure%' OR procedures like '%$other_procedure%') $where")->select();
		//echo $chr->getLastSql();die;
		//dump($recordData);die;
		$i = 0;
		foreach($recordData as $val){
			if($val['othername'] == $name){
				$recordData[$i]['name'] = "<span style='color:#66B3FF;'>".$val['myname']."[".$val['mycn_name']."]  ".$val['createtime']."<br><span style='margin-left:20px;'>".$val['chat_content']."</span><br>";
			}
			if($val['myname'] == $name){
				$recordData[$i]['name'] = "<span style='color:red;'>".$val['myname']."[".$val['mycn_name']."]  ".$val['createtime']."<br><span style='margin-left:20px;'>".$val['chat_content']."</span><br>";
			}
			$i++;
		}
		unset($i);

		$filename = "/var/www/html/BGCC/Conf/chat.text";
		if(file_exists($filename)){
			$opfile = fopen($filename,"r");
			$tmp = fread($opfile,filesize($filename));
			$tmp_arr = explode("|",$tmp);

			foreach($tmp_arr as $vm ){
				$arrData[] = explode(",",$vm);
			}

			foreach($arrData as $val){
				//if($val[3] == $my_procedure || $val[3] == $other_procedure){
				if(substr_count($val[3], $my_procedure) || substr_count($val[3],$other_procedure)){
					$tmp_arrData[] = $val;
				}
			}

			$j = 0;
			foreach($tmp_arrData as $val){
				if($val[2] == $name){
					$tmp_arrData[$j]['name'] = "<span style='color:#66B3FF;'>".$val[1]."[".$val[5]."]  ".$val[0]."<br><span style='margin-left:20px;'>".$val[4]."</span><br>";
				}
				if($val[1] == $name){
					$tmp_arrData[$j]['name'] = "<span style='color:red;'>".$val[1]."[".$val[5]."]  ".$val[0]."<br><span style='margin-left:20px;'>".$val[4]."</span><br>";
				}
				$tmp_arrData[$j]['createtime'] = $val[0];
				$j++;
			}
			unset($j);

		}
		$recordData_tmp = count($tmp_arrData) ? array_merge($recordData,$tmp_arrData) : $recordData;
		$arrF = $this->array_sort($recordData_tmp,"createtime");
		//dump($tmp_arrData);die;

		$count_tmp = count($tmp_arrData) ? $count+count($tmp_arrData) :$count;

		$rowsList = count($arrF) ? $arrF : false;
		//$arrChatR["total"] = $count;
		$arrChatR["total"] = $count_tmp;
		$arrChatR["rows"] = $rowsList;
		//dump(count($arrF));die;
		echo json_encode($arrChatR);
	}


	function getTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
					$v['children'] = $this->getTree($data, $v['tid']);
					if ( empty($v["children"])  )  unset($v['children']) ;
					/*
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['children'] =  array(array());
					*/
					if ( empty($v["children"]) && $v['state'] =='closed')  $v['state'] =  'open';
				 $tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	/*
	部门用户树
	要改聊天记录中的 呼叫分机 那个条时 需要改以下内容
	function uichatList(){$attributes_tpl =
	function userContacts()
	$userList = $users->field("username as text,d_id as pid,cn_name,en_name,extension,phone")->where("username != '$username'")->select();
	$userList[$i]['attributes']
	$val['attributes'] = $vb['cn_name'].",".$vb['en_name'].",".$vb["extension"].",".$vb['phone'].",".$id; //$id 永远放在最后一项
	*/
	function userContacts(){
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();

		$username = $_SESSION['user_info']['username'];
		$users = new Model("users");
		//$userList = $users->field("username as text,d_id as pid,cn_name,en_name,extension,phone")->where("username != '$username'")->select();

		$userList = $users->field("username as text,d_id as pid,cn_name,en_name,extension,phone")->select();

		$j = 0;
		foreach($dept as $vm){
			$dept[$j]['iconCls'] = "door";
			$dept[$j]['state'] = "closed";
			$j++;
		}
		unset($j);


		$session_info = $_SESSION['user_info'];
		$i = 0;
		foreach($userList as $val){
			$userList[$i]['id'] = $i+1;
			$userList[$i]['iconCls'] = "icon-user_chat";
			$userList[$i]['attributes'] = $val['cn_name'].",".$val['en_name'].",".$val["extension"].",".$val['phone'].",".$val['text'];   //绑定到节点的自定义属性
			$userList[$i]['my_attributes'] = $session_info['cn_name'].",".$session_info['en_name'].",".$session_info["extension"].",".$session_info['phone'].",".$session_info['username'];   //搜索时绑定到节点的自定义属性
			$i++;
		}
		unset($i);

		$n = 0;
		foreach($userList as $vb){
			if($vb["text"] == $username){
				$id = $vb["id"];
			}
			$n++;
		}
		unset($n);
		foreach($userList as $val){
			if($val["text"] != $username){    //这里可能有问题吧
				$val['attributes'] = $val['cn_name'].",".$val['en_name'].",".$val["extension"].",".$val['phone'].",".$val['text'].",".$id; //$id 永远放在最后一项


				$arr[$val["pid"]][] = $val;
				array_push($dept,$val);
			}
		}
        $arrTree = $this->getTree($dept,0);
		//dump($userList);die;
		//dump($arrTree);die;
		$strJSON = json_encode($arrTree);

		if($_GET['searchid'] == "treeid"){
			$user_name = $_POST["user_name"];
			foreach($userList as $v){
				/*if($v['text'] == $user_name || $v['cn_name'] == $user_name){
					$arr_tmp[] = $v;
				}*/
				if( strpos($v['text'],$user_name)!== false || strpos($v['cn_name'],$user_name)!== false ){
					$arr_tmp[] = $v;
				}
			}
			echo json_encode($arr_tmp);
		}else{
			echo ($strJSON);
		}
	}


	function sessionChat(){
		//dump($_POST);die;
		$procedure = $_POST['procedure'];
		$custom_myname = $_POST['custom_myname'];
		$exten = explode(",",$custom_myname);
		$name = explode("->",$procedure);
		$online_chat = new Model("online_chat");
		$arrData = array(
			"createtime"=> $_POST['time'],
			"myname"=>$name[0],
			"othername"=>$name[1],
			"procedures"=>$procedure,
			"chat_content"=>$_POST["msg"],
			"judge"=>"N",
		);
		//$arrContent = "time=>".$_POST['time'].",username=>".$name[0].",othername=>".$name[1].",procedures=>".$procedure.",chat_content=>".$_POST["msg"]."|";

		$arrContent = $_POST['time'].",".$name[0].",".$name[1].",".$procedure.",".$_POST["msg"].",".$exten[0]."|";
		//dump($arrData);die;
		//$result = $online_chat->data($arrData)->add();
		$_SESSION["chat_info"] = $arrData;
		$file = fopen('/var/www/html/BGCC/Conf/chat.text','a');
		fwrite($file,$arrContent);
		fclose($file);

	}



	function array_sort($arr,$keys,$type='asc'){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			$new_array[] = $arr[$k];
		}
		return $new_array;
	}

	/*
	function chatList(){
		$username = $_SESSION['user_info']['username'];
		$cn_name = $_SESSION['user_info']['cn_name'];

		$arrAdmin = getAdministratorNum();
		if( in_array($username,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$username);
		}

		$this->assign("cn_name",$cn_name);
		$this->display();
	}

	function chartDataList(){
		$username = $_SESSION['user_info']['username'];
		$users = new Model("users");
		$count = $users->table("users u")->field("u.username,u.r_id,u.cn_name,u.en_name,r.interface,r.r_name,u.extension")->join("role r on u.r_id=r.r_id")->where("r.interface ='agent' AND username != '$username'")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$ulist = $users->table("users u")->field("u.username,u.r_id,u.cn_name,u.en_name,r.interface,r.r_name,u.extension")->join("role r on u.r_id=r.r_id")->where("r.interface ='agent' AND username != '$username'")->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($ulist) ? $ulist : false;
		$arrUser["total"] = $count;
		$arrUser["rows"] = $rowsList;

		echo json_encode($arrUser);
	}


	function chatqqList(){
		$username = $_SESSION['user_info']['username'];
		$cn_name = $_SESSION['user_info']['cn_name'];
		$othername = $_REQUEST["username"];
		$my_procedure = $username."->".$othername;
		$other_procedure = $othername."->".$username;

		$online_chat = new Model("online_chat");
		$count = $online_chat->where("myname ='$username' OR othername = '$othername'")->count();
		import('ORG.Util.Page');
		$page=new Page($count,'6');
        $show=$page->show();

		$my_chatData = $online_chat->order("createtime desc")->where("`procedures` = '$my_procedure'")->limit($page->firstRow.','.$page->listRows)->select();

		//echo $online_chat->getLastSql();die;

		$other_chatData = $online_chat->order("createtime desc")->where("`procedures` = '$other_procedure'")->limit($page->firstRow.','.$page->listRows)->select();

		$arrT = array_merge($my_chatData,$other_chatData);
		$arrF = $this->array_sort($arrT,"createtime");
		//dump($arrF);die;




		$arrAdmin = getAdministratorNum();
		if( in_array($username,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$username);
		}

		$this->assign("cn_name",$cn_name);
		$this->assign("othername",$othername);
		$this->assign("arrF",$arrF);
		$this->assign("my_chatData",$my_chatData);
		$this->assign("other_chatData",$other_chatData);

		$this->display();
	}

	function insertChat(){
		$username = $_SESSION['user_info']['username'];
		$othername = $_REQUEST['othername'];
		$procedure = $username."->".$othername;

		$online_chat = new Model("online_chat");
		$arrData = array(
			"createtime"=>date("Y-m-d H:i:s"),
			"myname"=>$username,
			"othername"=>$othername,
			"procedures"=>$procedure,
			"chat_content"=>$_REQUEST['chat_content'],
			"judge"=>"N",
		);
		//dump($arrData);die;
		$result = $online_chat->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>"发送成功！"));
		} else {
			echo json_encode(array('msg'=>'发送失败！'));
		}
	}

	*/

}

?>
