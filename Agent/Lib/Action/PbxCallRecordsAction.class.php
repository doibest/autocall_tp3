<?php
class PbxCallRecordsAction extends Action{
	function pbxRecords(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$cn_name = $_SESSION['user_info']['cn_name'];

		$arrAdmin = getAdministratorNum();
		if( in_array($username,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$username);
		}

		$this->assign("cn_name",$cn_name);
		//dump($_SESSION["user_info"]);
		$this->display();

	}

	function pbxRecordesList(){
		getArrCache();//必须包含缓存文件
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $src = $_REQUEST['src'];
        $dst = $_REQUEST['dst'];
        $dept_id = $_REQUEST['dept_id'];
        $dept = $_REQUEST['dept'];//部门
        $workno = $_REQUEST['workno'];//工号
        $username = $_REQUEST['username'];//姓名
		$calltype = $_REQUEST['calltype'];
        $disposition = $_REQUEST['disposition'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";

        $where = "1 ";
        $where .= empty($calldate_start)?"":" AND calldate >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND calldate <'$calldate_end'";

		$arrDep = $this->getDepTreeArray();
		$deptSet = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$searchDeptId = rtrim($deptSet,",");
		$d_id = $_SESSION["user_info"]["d_id"];
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$dept_name_Set = rtrim($deptst,",");
		if( !$dept_id ){
			$where .= " AND dept_id IN ($dept_name_Set)";
		}
        if( $searchmethod == "equal"){
            $where .= empty($src)?"":" AND src ='$src'";
            $where .= empty($dst)?"":" AND dst ='$dst'";
            $where .= empty($workno)?"":" AND workno ='$workno'";
            $where .= empty($dept_id)?"":" AND dept_id ='$dept_id'";
            if(empty($username)){
                $where .= " ";
            }else{
                //dump($arrCacheWorkNo);dump($username);die;
                $arrWorkNo = Array();
                foreach($arrCacheWorkNo AS $k=>$v){
                    if($v['cn_name'] == $username){
                        $arrWorkNo[] = $k;
                    }
                }
                $where .= empty($arrWorkNo)?" AND 0":" AND workno IN " .gen_SQL_IN($arrWorkNo,"string");//构造查询字符串
            }
        }else{
            $where .= empty($src)?"":" AND src like '%$src%'";
            $where .= empty($dst)?"":" AND dst like '%$dst%'";
            $where .= empty($workno)?"":" AND workno like '%$workno%'";
            $arrWorkNo = Array();
            foreach($arrCacheWorkNo AS $k=>$v){
                if(false !== strpos($v['cn_name'],$username)){
                    $arrWorkNo[] = $k;
                }
            }
            $where .= empty($arrWorkNo)?"":" AND workno IN " .gen_SQL_IN($arrWorkNo,"string");//构造查询字符串
            unset($arrWorkNo);
			$where .=  empty($dept_id) ? "" : " AND dept_id IN ($searchDeptId)";
        }
		if($_SESSION['user_info']['username'] != 'admin'){
			$where .= " AND dept_id IN ($dept_name_Set)";
		}

		//dump($where);die;
        $where .= empty($calltype)?"":" AND calltype ='$calltype'";
		//$disposition除了有ANSWERED和"NO ANSWER"两个值外，原来还有个BUSY等，所以只能用下面的方法了
		if(! empty($disposition) ){
			if($disposition == "ANSWERED"){
				$where .= " AND disposition = 'ANSWERED'";
			}else{
				$where .= " AND disposition != 'ANSWERED'";
			}
		}
		$pbxcdr = new Model('asteriskcdrdb.Cdr');
		import('ORG.Util.Page');
		$worknname = $_SESSION['user_info']['username'];
		$count = $pbxcdr->where("$where AND workno ='$worknname'")->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($_SESSION['user_info']['username'] == 'admin'){
			$pbxCallData = $pbxcdr->field("calldate,src,dst,outnum,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield")->where($where)->order("calldate desc")->limit($page->firstRow.",".$page->listRows)->select();
        }else{
			$pbxCallData = $pbxcdr->field("calldate,src,dst,outnum,billsec,workno,dept_id,calltype, disposition,uniqueid,userfield")->where("$where AND workno ='$worknname'")->order("calldate desc")->limit($page->firstRow.",".$page->listRows)->select();
        }
		//echo $pbxcdr->getLastSql();die;
		$in_answer = "<img src='Agent/Tpl/public/images/calltype/IN_ANSWER.png'>";
		$out_answer = "<img src='Agent/Tpl/public/images/calltype/OUT_ANSWER.png'>";
		$cdr_status_row = array('ANSWERED'=>'已接听','NO ANSWER'=>'未接听');
		$cdr_calltype = array('IN'=>$in_answer,'OUT'=>$out_answer);
		$i = 0;
		$para_sys = readS();
		foreach($pbxCallData as &$val){
			$val["srcP"] = trim($val["src"]);
			$val["dstP"] = trim($val["dst"]);
			if($_SESSION['user_info']['username'] != 'admin'){
				if( $para_sys["callrecords_hide"] == "pbx" || $para_sys["callrecords_hide"] == "all" ){
					if($val["calltype"] == "IN"){
						if(strlen($val["src"])>5){
							$val["src"] = substr($val["src"],0,3)."***".substr($val["src"],-4);
						}
					}else{
						if(strlen($val["dst"])>5){
							$val["dst"] = substr($val["dst"],0,3)."***".substr($val["dst"],-4);
						}
					}
				}
			}
			$status = $cdr_status_row[$val["disposition"]];
			$val["disposition"] = $status;
			$Incalltype = $cdr_calltype[$val["calltype"]];
			$val["calltype"] = $Incalltype;

			$val['username'] = $arrCacheWorkNo[$val['workno']]['cn_name'];
			$val['dept_name'] = $arrCacheDept[$val['dept_id']]['dept_name'];
			$val['dept_pname'] = $arrCacheDept[$val['dept_id']]['dept_pname'];

			$arrTmp = explode('.',$val["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["uniqueid"].".WAV";

			if($val["disposition"] == "已接听"  && !empty($val["uniqueid"]) ){
				if(file_exists($WAVfile)){
					$pbxCallData[$i]['operations'] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["uniqueid"])."'".")\" > 播放 </a> "."<a target='_blank' href='index.php?m=CDR&a=downloadCDR&uniqueid=" .trim($val["uniqueid"]) ."&src=" .trim($val["src"]) ."&dst=" .trim($val["dst"]) ."'> 下载 </a>" ;
				}
			}
			$pbxCallData[$i]["billsec"] = sprintf("%02d",intval($val["billsec"]/3600)).":".sprintf("%02d",intval(($val["billsec"]%3600)/60)).":".sprintf("%02d",intval((($val[billsec]%3600)%60)));

			$i++;
		}
		//dump($pbxCallData);die;
		$rowsList = count($pbxCallData) ? $pbxCallData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		echo json_encode($ary);
	}

	function playCDR(){
		checkLogin();
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = str_replace(".wav",".mp3",$_GET['userfield']);//lame把.wav的文件转化成了.mp3文件
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		//http://192.168.1.69/agent.php?m=TasksCallRecords&a=playCDR&uniqueid=1363344674.456
		$AudioURL = "agent.php?m=PbxCallRecords&a=downloadCDR&clientphone=${clientphone}&uniqueid=${uniqueid}" ."&userfield=".$userfield;
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->assign("clientphone",$clientphone);
		$this->display();
	}

	function downloadCDR(){
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		$WAVfile = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp) .'/'. $userfield;

		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=" .str_replace(".wav", ".mp3", basename($WAVfile))."");
        readfile($WAVfile);

	}

	function getDepartmentArr(){
		$ptlid = $_SESSION["user_info"]["d_id"];
		$username = $_SESSION["user_info"]["username"];

		//dump($ptlid);die;
        import("ORG.Util.DepartmentTree");
        $department=new Model("Department");
		$pname = $department->field("d_name as text,d_id as id,d_pid")->where("d_id = $ptlid")->find();
        $Tree = new Tree();

		$rows = $department->field("d_name as text,d_id as id,d_pid")->order("d_orderid")->select();

		if($username == "admin"){
			$arrTree = $this->getTree($rows,0);
		}else{
			$arrTree = $this->getTree($rows,$ptlid);
		}
		$pname["children"] = $arrTree;
		$arrTreeData[0] =  $pname;
		//dump($arrTree);
		//dump($arrTreeData);
		echo json_encode($arrTreeData);
    }

	function getTree($data, $pId) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v['d_pid'] == $pId)    {
                     $v['children'] = $this->getTree($data, $v['id']);

                      if ( empty($v["children"])  )  unset($v['children']) ;
                      $tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }


	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }
}
?>

