<?php
class AnnouncementAction extends Action{
	function announcementAll(){
		checkLogin();
		$this->display();
	}

	function announcementContent(){
		checkLogin();
		$id = $_GET['id'];
		$gg = new Model("announcement");
		if($id){
			$this->assign("id",$id);
			$ggList = $gg->where("id = $id")->find();
			$this->assign("ggList",$ggList);
		}
		$this->display();
	}

	function announcementAllList(){
		$gg = new Model("announcement");
		import('ORG.Util.Page');


		$username = $_SESSION["user_info"]["username"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$r_id = $_SESSION["user_info"]["r_id"];
		$dtime = date("Y-m-d")." 23:59:59";
		$dtime1 = date("Y-m-d")." 00:00:00";
		$now = date("Y-m-d H:i:s");

		//$arrDep = $this->getDepTreeArray();
		//$deptst = $this->getSuperiorsDeptName($arrDep,$d_id);
		//$deptSet = rtrim($deptst,",");
		//$where = "d.dept_id IN ($deptSet)";
		//$where .= " OR reader = $r_id";
		$where = "find_in_set('$d_id',a.department_id)";

		$count1 = $gg->table("announcement a")->join("announcementtype t on a.noticetype = t.id")->join("announcement_dept d on a.id = d.gg_id")->where("$where AND a.an_enabeld = 'Y' AND a.endtime >='$dtime1'  AND a.starttime < '$now'")->group("a.id")->select();
		$count = count($count1);

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$ggList = $gg->order("createtime desc")->table("announcement a")->field("a.id,a.createtime,a.starttime,a.name,a.noticetype, a.an_enabeld,a.title,a.endtime,t.announcementname")->join("announcementtype t on a.noticetype = t.id")->join("announcement_dept d on a.id = d.gg_id")->limit($page->firstRow.','.$page->listRows)->where("$where AND a.an_enabeld = 'Y' AND a.endtime >='$dtime1'  AND a.starttime < '$now'")->group("a.id")->select();
		/*
		$row = array('0'=>'pbx公告','1'=>'系统公告','2'=>'站内公告');
		foreach($ggList as &$val){
			$type = $row[$val['noticetype']];
			$val['noticetype'] = $type;
			//dump($val['noticetype']);
		}
		*/
		//dump($ggList);die;
		$rowsList = count($ggList) ? $ggList : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
		//echo $aa;

	}

	function getSuperiorsDeptName($arrDep,$dept_id){
		global $dep_str;
		$dep_str .= "'" . $dept_id . "',";
		$pid = $arrDep[$dept_id]['pid'];

		foreach($arrDep as $key=>$val){
			if($key == $pid){
				$this->getSuperiorsDeptName($arrDep,$pid);
			}
		}
		//dump($dep_str);die;
		return $dep_str;

	}
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }


	//所有未接来电
	function missedCalls(){
		checkLogin();
		$this->display();
	}

	function missedCallsData(){
		$username = $_SESSION["user_info"]["username"];
		$extension = $_SESSION["user_info"]["extension"];
		$cdr = new Model('asteriskcdrdb.Cdr');
		import('ORG.Util.Page');

		$where = "disposition !='ANSWERED' ";
		if($username != "admin"){
			$where .= " AND dst='$extension'";
		}
		//$count = $cdr->where("dst='$extension' AND disposition !='ANSWERED'")->count();
		$count = $cdr->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$callData = $cdr->order("calldate desc")->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		foreach($callData as &$val){
			$val["src2"] = $val["src"];
			if($username != "admin"){
				if($para_sys["hide_phone"] =="yes"){
					$val["src3"] = substr($val["src2"],0,3)."***".substr($val["src2"],-4);
				}
			}
			$val["src"] = '<a href="#" title="点击呼叫"  onClick="clickCall1('."'".$val["src2"]."'".');"><img style="vertical-align:middle;cursor:pointer;padding:0px;padding-right:10px;margin:0px;border:none;" src="Agent/Tpl/public/js/themes/icons/call.png"  >'.  $val["src3"].'</a>';
		}

		//dump($callData);die;
		$rowsList = count($callData) ? $callData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
	}


	//回访任务
	function visitAllList(){
		checkLogin();
		$this->display();
	}

	function visitData(){
		$username = $_SESSION["user_info"]["username"];
		$visittime = date("Y-m-d ")."23:59:59";
		$visittime2 = date("Y-m-d ")."00:00:00";  //只显示今天的回访任务
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$visit_status = $_REQUEST['visit_status'];

		/*
		$agent_type = $_SESSION["user_info"]['agent_type'];
		$taskU = require 'BGCC/Conf/task_user.php';
		$task_id = $taskU[$username];
		*/

		$where = "1 ";
		$where .= empty($startime)?"":" AND v.visit_time >'$startime'";
        $where .= empty($endtime)?"":" AND v.visit_time <'$endtime'";
        $where .= empty($visit_status)?"":" AND v.visit_status = '$visit_status'";

		$where1 = "1 ";
		$where1 .= empty($startime)?"":" AND visit_time >'$startime'";
        $where1 .= empty($endtime)?"":" AND visit_time <'$endtime'";
        $where1 .= empty($visit_status)?"":" AND visit_status = '$visit_status'";

		$visit_customer = new Model("visit_customer");
		/*
		if($agent_type == 'service'){
			$count = $visit_customer->where("$where1 AND visit_name = '$username' AND visit_time<'$visittime' AND visit_type='in'")->count();
		}else{
			$count = $visit_customer->where("$where1 AND visit_name = '$username' AND visit_time<'$visittime' AND visit_status = 'N' AND visit_type='out'")->count();
		}
		*/
		$count = $visit_customer->where("$where1 AND visit_name = '$username' AND visit_time<'$visittime' AND visit_time>'$visittime2' AND visit_type='in'")->count();

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		/*
		if($agent_type == 'service'){
			$arrVisitData = $visit_customer->table("visit_customer v")->order("v.visit_time asc")->field("v.id,v.visit_name,v.visit_time,v.customer_id,v.visit_status,c.name,c.phone1,c.phone2,c.email")->join('customer c on v.customer_id = c.id')->limit($page->firstRow.','.$page->listRows)->where("$where AND v.visit_name = '$username' AND v.visit_time<'$visittime'")->select();
		}else{
			$table = "sales_source_".$task_id;
			$arrVisitData = $visit_customer->table("visit_customer v")->order("v.visit_time asc")->field("v.id,v.visit_name,v.visit_time,v.customer_id,v.visit_status,v.visit_type,c.name,c.phone1,c.phone2,c.email")->join("$table c on v.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where("visit_name = '$username' AND visit_time<'$visittime' AND v.visit_status = 'N' AND v.visit_type='out'")->select();
		}
		*/
		$arrVisitData = $visit_customer->table("visit_customer v")->order("v.visit_time asc")->field("v.id,v.visit_name,v.visit_time,v.customer_id,v.visit_status,c.name,c.phone1,c.phone2,c.email")->join('customer c on v.customer_id = c.id')->limit($page->firstRow.','.$page->listRows)->where("$where AND v.visit_name = '$username' AND v.visit_time<'$visittime'  AND v.visit_time>'$visittime2'  AND visit_type='in'")->select();

		$i = 0;
		$status_row = array('Y'=>'已回访','N'=>'未回访');
		foreach($arrVisitData as &$val){
			$status = $status_row[$val['visit_status']];
			$val['visit_status'] = $status;
			/*
			if($agent_type == 'service'){
				$arrVisitData[$i]["name"] = "<a target='_blank' style='color:#0066CC' href='agent.php?m=CustomerData&a=editCustomer&id=".$val['customer_id']."&visit_id=".$val['id']."'>". $val["name"] ."</a>";
			}else{
				$arrVisitData[$i]["name"] = "<a  target='_blank' href='agent.php?m=Telemarketing&a=editWCustomer&task_id=$task_id&phone_num=".$val['phone1']."&id=".$val['customer_id']."&visit_id=".$val['id']."'>".$val['name']."</a>";
			}
			*/
			$arrVisitData[$i]["name"] = "<a target='_blank' style='color:#0066CC' href='agent.php?m=CustomerData&a=editCustomer&id=".$val['customer_id']."&visit_id=".$val['id']."'>". $val["name"] ."</a>";
			$i++;
		}

		//echo $visit_customer->getLastSql();
		//dump($arrVisitData);die;
		$rowsList = count($arrVisitData) ? $arrVisitData : false;
		$arrVisit["total"] = $count;
		$arrVisit["rows"] = $rowsList;

		echo json_encode($arrVisit);
	}

	//外呼回访任务
	function visitOutboundAllList(){
		checkLogin();
		$this->display();
	}

	function visitOutboundData(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION["user_info"]["username"];
		$visittime = date("Y-m-d ")."23:59:59";
		$visittime2 = date("Y-m-d ")."00:00:00";  //只显示今天的回访任务
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$visit_status = $_REQUEST['visit_status'];

		$where = "1 ";
		$where .= empty($startime)?"":" AND  visit_time >'$startime'";
        $where .= empty($endtime)?"":" AND  visit_time <'$endtime'";
        $where .= empty($visit_status)?"":" AND  visit_status = '$visit_status'";

		$where1 = "1 ";
		$where1 .= empty($startime)?"":" AND visit_time >'$startime'";
        $where1 .= empty($endtime)?"":" AND visit_time <'$endtime'";
        $where1 .= empty($visit_status)?"":" AND visit_status = '$visit_status'";

		$visit_customer = new Model("visit_customer");
		$count = $visit_customer->where("$where AND visit_name = '$username' AND visit_time<'$visittime' AND visit_time>'$visittime2' AND visit_type='out'")->count();


		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrVisitOutboundData = $visit_customer->order("visit_time desc")->limit($page->firstRow.','.$page->listRows)->where("$where AND visit_name = '$username' AND visit_time<'$visittime' AND visit_time>'$visittime2' AND visit_type='out'")->select();

		/*
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
		}
		$fields = "`id`,`createtime`,"."`".implode("`,`",$arrF)."`";
		$row = getSelectCache();
		*/

		$sales_task = new Model("sales_task");
		$arrTask = $sales_task->where("enable='Y'")->select();
		foreach($arrTask as $key=>$val){
			$taskName[$val["id"]] = $val["name"];
		}

		$rows = array("Y"=>"已回访","N"=>"未回访");
		$i = 0;
		foreach($arrVisitOutboundData as $val){
			$arrVisitOutboundData[$i]["phone"] = "<a target='_blank' style='color:#0066CC' href='agent.php?m=Telemarketing&a=editWCustomer&calltype=preview&task_id=".$val['task_id']."&phone_num=".$val['phone']."&id=".$val['customer_id']."&visit_id=".$val['id']."'>". $val["phone"] ."</a>";
			$arrVisitOutboundData[$i]["visit_status"] = $rows[$val["visit_status"]];
			$arrVisitOutboundData[$i]["task_name"] = $taskName[$val["task_id"]];
			/*
			$id = $val["task_id"];
			$customer_id = $val["customer_id"];
			$source = new Model("sales_source_".$id);
			$arr = $source->field($fields)->where("id = '$customer_id'")->find();
			$arrVisitOutboundData[$i]["name"] = $arr["name"];
			$arrVisitOutboundData[$i]["grade"] = $row["grade"][$arr["grade"]];
			*/
			$i++;
		}
		unset($i);

		//echo $visit_customer->getLastSql();
		//dump($arrVisitOutboundData);die;
		$rowsList = count($arrVisitOutboundData) ? $arrVisitOutboundData : false;
		$arrVisit["total"] = $count;
		$arrVisit["rows"] = $rowsList;

		echo json_encode($arrVisit);
	}

	function updateVisit(){
		$id = $_REQUEST['id'];
		$visit_customer = new Model("visit_customer");
		$result = $visit_customer->where("id = '$id'")->save(array('visit_status'=>$_REQUEST['visit_status']));
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'修改成功！'));
		} else {
			echo json_encode(array('msg'=>'修改失败！'));
		}
	}


	function updateVisitData(){
		$fields = explode('-',$this->_post("name"));  //_post("name")<a>标签为id值
		$ary = array($fields[0]=>$this->_post("value"));
		$visit_customer = new Model("visit_customer");
		$count = $visit_customer->where("id=".$fields[1])->save($ary);
		//echo $visit_customer->getLastSql();
	}

	//最近未联系客户
	function noVisitCustomer(){
		checkLogin();
		$this->display();
	}

	function noVisitCustomerData(){
		//$visit_record = new Model("visit_record");
		$visit_record = new Model("customer");
		$para_sys = readS();
		$noVist_num = $para_sys['no_visit'];
		$username = $_SESSION["user_info"]["username"];
		$visR = $visit_record->order("recently_visittime desc")->field("id,createuser,name,recently_visittime,createtime")->where("createuser = '$username'")->select();
		$dttime = date("Y-m-d H:i:s");
		//echo $visit_record->getLastSql();
		//dump($visR);die;
		$i = 0;
		foreach($visR as $vm){
			$visR[$i]['interval'] = round( (strtotime($dttime) - strtotime($vm['recently_visittime']) )/3600/24);
			$visR[$i]["name"] = "<a target='_blank' style='color:#0066CC' href='agent.php?m=CustomerData&a=editCustomer&id=".$vm['id']."'>". $vm["name"] ."</a>";
			if(!$vm["recently_visittime"]){
				$visR[$i]["recently_visittime"] = $vm["createtime"];
			}
			$i++;
		}
		unset($i);
		foreach($visR as $val){
			if($val['interval'] >= $noVist_num){
				$noVisitData[] = $val;
			}
		}
		//dump($noVisitData);die;
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($noVisitData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;

		$disNoVisitData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($noVisitData AS &$v){
			if($i >= $start && $j < $length){
				$disNoVisitData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}
		//dump($disNoVisitData);die;
		$rowsList = count($disNoVisitData) ? $disNoVisitData : false;

		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		echo json_encode($ary);

	}
}

?>
