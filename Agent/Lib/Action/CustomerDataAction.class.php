<?php
class CustomerDataAction extends Action{
	function customerManagement(){
		checkLogin();
		$dept_id = $_SESSION["user_info"]["d_id"];

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['list_enabled'] == "Y" ){
				if($val['en_name'] != "createuser" ){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					//$arrField[0][$i]['fitColumns'] = true;
					$arrField[0][$i]['width'] = "100";
				}
				/*
				if( $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
				*/
				if( $val['tiptools_enabled'] == 'Y' ||  $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}

		$arrFd = array("field"=>"createtime","title"=>"创建时间","width"=>"130");
		$arrFd2 = array("field"=>"ck","checkbox"=>true);
		$arrFd3 = array("field"=>"createuser","title"=>"创建人工号","width"=>"100");
		$arrFd7 = array("field"=>"dealuser","title"=>"共享坐席","width"=>"100");
		//$arrFd4 = array("field"=>"cn_name","title"=>"创建人姓名");
		//$arrFd5 = array("field"=>"d_name","title"=>"所属部门");
		//array_unshift($arrField[0],$arrFd5);
		//array_unshift($arrField[0],$arrFd4);
		array_unshift($arrField[0],$arrFd7);
		array_unshift($arrField[0],$arrFd3);
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd2);
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("fielddTpl",$fielddTpl);
		//dump($areaTpl);die;
		$this->assign("dept_id",$dept_id);

		$areaTpl = implode(",",$areaTpl2);
		$this->assign("areaTpl",$areaTpl);

		$para_sys = readS();
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		if($para_sys["agent_customer_menu"] != "Y"){
			$priv["add"] = "Y";
			$priv["edit"] = "Y";
			//$priv["delete"] = "Y";
			$priv["transmission"] = "Y";
		}
		$this->assign("priv",$priv);

		//销售漏斗
		$menuname1 = "Sales Funnel";
		$p_menuname1 = $_SESSION['menu'][$menuname1]; //父菜单
		$priv_Sales = $_SESSION["user_priv"][$p_menuname1][$menuname1];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv_Sales["view"] == "Y"){
				$this->assign("priv_Sales","Y");
			}else{
				$this->assign("priv_Sales","N");
			}
		}else{
			$this->assign("priv_Sales","Y");
		}

		//服务记录
		$menuname2 = "Service records";
		$p_menuname2 = $_SESSION['menu'][$menuname2]; //父菜单
		$priv1_Service = $_SESSION["user_priv"][$p_menuname2][$menuname2];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Service["view"] == "Y"){
				$this->assign("priv1_Service","Y");
			}else{
				$this->assign("priv1_Service","N");
			}
		}else{
			$this->assign("priv1_Service","Y");
		}

		//订单列表
		$menuname3 = "Order Management";
		$p_menuname3 = $_SESSION['menu'][$menuname3]; //父菜单
		$priv1_Order = $_SESSION["user_priv"][$p_menuname3][$menuname3];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Order["view"] == "Y"){
				$this->assign("priv1_Order","Y");
			}else{
				$this->assign("priv1_Order","N");
			}
		}else{
			$this->assign("priv1_Order","Y");
		}

		//回访记录列表
		$menuname4 = "Visit Record";
		$p_menuname4 = $_SESSION['menu'][$menuname4]; //父菜单
		$priv1_Visit_Record = $_SESSION["user_priv"][$p_menuname4][$menuname4];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Visit_Record["view"] == "Y"){
				$this->assign("priv1_Visit_Record","Y");
			}else{
				$this->assign("priv1_Visit_Record","N");
			}
		}else{
			$this->assign("priv1_Visit_Record","Y");
		}

		//回访内容列表
		$menuname5 = "Visit Content";
		$p_menuname5 = $_SESSION['menu'][$menuname5]; //父菜单
		$priv1_Visit_Content = $_SESSION["user_priv"][$p_menuname5][$menuname5];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Visit_Content["view"] == "Y"){
				$this->assign("priv1_Visit_Content","Y");
			}else{
				$this->assign("priv1_Visit_Content","N");
			}
		}else{
			$this->assign("priv1_Visit_Content","Y");
		}

		//转交历史
		$menuname6 = "Forwarded History";
		$p_menuname6 = $_SESSION['menu'][$menuname6]; //父菜单
		$priv1_History = $_SESSION["user_priv"][$p_menuname6][$menuname6];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_History["view"] == "Y"){
				$this->assign("priv1_History","Y");
			}else{
				$this->assign("priv1_History","N");
			}
		}else{
			$this->assign("priv1_History","Y");
		}

		//服务记录
		$menuname7 = "Work Order reports";
		$p_menuname7 = $_SESSION['menu'][$menuname7]; //父菜单
		$priv1_Work = $_SESSION["user_priv"][$p_menuname7][$menuname7];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_Work["view"] == "Y"){
				$this->assign("priv1_Work","Y");
			}else{
				$this->assign("priv1_Work","N");
			}
		}else{
			$this->assign("priv1_Work","Y");
		}

		//客户资料--相关文件
		$menuname8 = "Related Documents";
		$p_menuname8 = $_SESSION['menu'][$menuname8]; //父菜单
		$priv1_File = $_SESSION["user_priv"][$p_menuname8][$menuname8];
		if($para_sys["agent_customer_menu"] == "Y"){
			if($priv1_File["view"] == "Y"){
				$this->assign("priv1_File","Y");
			}else{
				$this->assign("priv1_File","N");
			}
		}else{
			$this->assign("priv1_File","Y");
		}

		/*
		dump($priv_Sales);
		dump($priv1_Service);
		dump($priv1_Order);
		dump($priv1_Visit_Record);
		dump($priv1_Visit_Content);
		dump($priv1_History);die;
		*/

		$this->display();
	}

	function customerList(){
		checkLogin();
		$dept_id = $_SESSION["user_info"]["d_id"];

		$cmFields = getFieldCache();
		$i = 0;
		$arrNH = array("name","sex");
		foreach($cmFields as $val){
			if($val['list_enabled'] == "Y" ){
				if($val['en_name'] != "createuser" ){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					//$arrField[0][$i]['fitColumns'] = true;
					if( in_array($val['en_name'],$arrNH)){
						$arrField[0][$i]['width'] = "80";
					}else{
						$arrField[0][$i]['width'] = "120";
					}
				}
				/*
				if( $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
				*/
				if( $val['tiptools_enabled'] == 'Y' ||  $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}

		$arrFd = array("field"=>"createtime","title"=>"创建时间","width"=>"130");
		$arrFd2 = array("field"=>"ck","checkbox"=>true);
		$arrFd3 = array("field"=>"createuser","title"=>"创建人工号","width"=>"100");
		$arrFd7 = array("field"=>"dealuser","title"=>"共享坐席","width"=>"100");
		//$arrFd4 = array("field"=>"cn_name","title"=>"创建人姓名");
		//$arrFd5 = array("field"=>"d_name","title"=>"所属部门");
		//array_unshift($arrField[0],$arrFd5);
		//array_unshift($arrField[0],$arrFd4);
		array_unshift($arrField[0],$arrFd7);
		array_unshift($arrField[0],$arrFd3);
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd2);
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("fielddTpl",$fielddTpl);
		//dump($areaTpl);die;
		$this->assign("dept_id",$dept_id);

		$areaTpl = implode(",",$areaTpl2);
		$this->assign("areaTpl",$areaTpl);

		$para_sys = readS();
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		if($para_sys["agent_customer_menu"] != "Y"){
			$priv["add"] = "Y";
			$priv["edit"] = "Y";
			//$priv["delete"] = "Y";
			$priv["transmission"] = "Y";
		}
		$this->assign("priv",$priv);


		$this->display();
	}


	function customerDataList(){
		$para_sys = readS();
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$deptId = $_REQUEST['dept_id'];
		$order_integral = $_REQUEST['order_integral'];

		/*
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$deptSet = rtrim($deptst,",");

		$deptst2 = $this->getMeAndSubDeptName($arrDep,$deptId);
		$deptSet2 = rtrim($deptst2,",");
		*/

		$where = "(1 ";
		$where .= " AND recycle = 'N'";
		$where .= empty($startime)?"":" AND createtime >'$startime'";
        $where .= empty($endtime)?"":" AND createtime <'$endtime'";

		if($para_sys["equal_all"] == "agent" || $para_sys["equal_all"] == "all"){
			$arr = $_REQUEST;
			foreach($arr as $k=>$v){
				if($v && $k != "page"  && $k != "rows"  && $k != "searchmethod" ){
					$arrS[$k] = $v;
				}
			}
			//精确查询可以查所有的客户资料
			if($_REQUEST['searchmethod']){
				if($username != "admin"){
					if( $searchmethod == "equal" && $arrS){
						$where .= " AND 1)";
					}else{
						$where .= "  AND (createuser = '$username'  OR share_deptid = '$dept_id' OR dealuser = '$username') ) ";
					}
				}else{
					$where .= " AND 1)";
				}
			}else{
				if($username == "admin"){
					$where .= " AND 1)";
				}else{
					$where .= "  AND (createuser = '$username'  OR share_deptid = '$dept_id' OR dealuser = '$username') )";
				}
			}
		}else{
			if($username == "admin"){
				$where .= " AND 1)";
			}else{
				//$where .= "  AND createuser = '$username' )";
				$where .= "  AND (createuser = '$username'  OR share_deptid = '$dept_id' OR dealuser = '$username') )";  //共享客户资料
			}
		}

		//备选字段
		if($para_sys["hide_field"]=='yes'){
			if(!$_REQUEST['searchmethod']){
				$where .= " AND hide_fields='Y'";
			}else{
				if($searchmethod != "equal"){
					$where .= " AND hide_fields='Y'";
				}
			}
		}

		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){  //  && $val['text_type'] != '3'
				//if($val['en_name'] != "createuser"){
				$arrF[] = $val['en_name'];
				//$$val['en_name'] = $_REQUEST[$val['en_name']];
				if( $val['text_type'] == '3'){
					$areaTpl[$val["en_name"]] = $val["en_name"];
				}
				if($val['text_type'] == '4'){
					$start = $val['en_name']."_start";
					$$start = $_REQUEST[$start];
					$end = $val['en_name']."_end";
					$$end = $_REQUEST[$end];
				}else{
					$$val['en_name'] = $_REQUEST[$val['en_name']];
				}
				//}
			}
		}
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3' ){
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
				}

				if($vm['text_type'] == '4'){
					$start = $vm['en_name']."_start";
					$end = $vm['en_name']."_end";
					 $where .= empty($$start)?"":" AND ".$vm['en_name'] ." >= '".$$start."'";
					 $where .= empty($$end)?"":" AND ".$vm['en_name'] ." <= '".$$end."'";
				}
			}
		}

		if($order_integral == "Y"){
			$where .= " AND integral is not null AND integral != ''";
		}else{
			$where .= empty($order_integral) ? "" : "  AND integral is null";
		}



		//dump($where);die;

		//匹配上次查询条件--start
		$last = "(1  AND recycle = 'N'  AND (createuser = '".$username."'  OR share_deptid = '".$dept_id."' OR dealuser = '".$username."') )";
		$search_last_sql = new Model("search_last_sql");
		if($where != $last){
			$search_res = $search_last_sql->where("table_name = 'customer'")->save( array('last_sql'=>$where) );
		}
		$search_data = $search_last_sql->where("table_name = 'customer'")->find();
		if($_REQUEST['search'] == "lastTime"){
			$where = $search_data['last_sql'];
		}else{
			$where = $where;
		}
		//匹配上次查询条件--end



		$customer = new Model("customer");
		import('ORG.Util.Page');


		if($username == "admin"){
			$count = $customer->where("$where  AND customer_source = 'complete' ")->count();
		}else{
			$count = $customer->where("$where AND customer_source = 'complete'")->count();
		}

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		$fields = "`id`,`createtime`,dealuser,"."`".implode("`,`",$arrF)."`";
		if($username == "admin"){
			$cmlist = $customer->order("createtime desc")->field($fields)->limit($page->firstRow.','.$page->listRows)->where("$where AND customer_source = 'complete'")->select();
		}else{
			$cmlist = $customer->order("createtime desc")->field($fields)->limit($page->firstRow.','.$page->listRows)->where("$where AND customer_source = 'complete'")->select();
		}
		$sqlSearch = $customer->getLastSql();
		//echo $customer->getLastSql();die;
		//dump($fields);die;
		$row = getSelectCache();
		//dump($row);die;
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}

		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		//dump($priv);die;
		$i = 0;
		foreach($cmlist as &$vm){
			$cmlist[$i]["massPhone"] = $vm['phone1'];
			$cmlist[$i]["massEmail"] = $vm['email'];
			$cmlist[$i]["massPhone2"] = $vm['phone2'];
			/*
			foreach($areaTpl as $val){
				$cmlist[$i][$val."s1"] = $vm[$val];
				if(mb_strlen($vm[$val],'utf-8') >15){
					$cmlist[$i][$val] = mb_substr($vm[$val],0,15,'utf-8')."...";
				}else{
					$cmlist[$i][$val] = $vm[$val];
				}
			}
			*/
			if($para_sys['hide_rule'] == "role"){
				if($priv["hide_phone"] =="Y"){
					$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
					if($vm["phone2"]){
						$cmlist[$i]["phone2"] = substr($vm["phone2"],0,3)."***".substr($vm["phone2"],-4);
					}
				}
				if($vm['email']){
					if($priv["hide_email"] =="Y"){
						$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
					}
				}
				if($vm['qq_number']){
					if($priv["hide_qq"] =="Y"){
						$cmlist[$i]["qq_number"] = "***".substr($vm["qq_number"],3);
					}
				}
				if($vm['bank_account']){
					if($priv["hide_bank"] =="Y"){
						$cmlist[$i]["bank_account"] = substr($vm["bank_account"],0,3)."***".substr($vm["bank_account"],-4);
					}
				}
			}else{
				if($para_sys["hide_phone"] =="yes"){
					$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
					if($vm["phone2"]){
						$cmlist[$i]["phone2"] = substr($vm["phone2"],0,3)."***".substr($vm["phone2"],-4);
					}
				}
				if($vm['email']){
					if($para_sys["hide_email"] =="yes"){
						$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
					}
				}
				if($vm['qq_number']){
					if($para_sys["hide_qq"] =="yes"){
						$cmlist[$i]["qq_number"] = "***".substr($vm["qq_number"],3);
					}
				}
				if($vm['bank_account']){
					if($para_sys["hide_bank"] =="yes"){
						$cmlist[$i]["bank_account"] = substr($vm["bank_account"],0,3)."***".substr($vm["bank_account"],-4);
					}
				}
			}

			foreach($cmFields as $val){
				if($val['list_enabled'] == 'Y'){
					if($val["text_type"] == "4"){
						if($vm[$val["en_name"]] == "0000-00-00 00:00:00"){
							$vm[$val["en_name"]] = "";
						}
					}elseif($val["text_type"] == "5"){
						if($vm[$val["en_name"]] == "0000-00-00"){
							$vm[$val["en_name"]] = "";
						}
					}
				}
			}

			$i++;
		}
		unset($i);


		//dump($cmlist);die;
		//dump($customer->getLastSql());die;
		$rowsList = count($cmlist) ? $cmlist : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["sears"] = base64_encode($sqlSearch);

		echo json_encode($ary);
		//$this->assign("cmlist",$cmlist);
	}

	function addCustomer(){
		checkLogin();
		$phone = $_GET["phone"];
		$openid = $_GET["openid"];
		$uniqueid = $_GET["uniqueid"];
		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			/*
			if($val['field_enabled'] == 'Y'){
				if($val['text_type'] == '1'){
					$textTpl[] = $val;
				}
				if($val['text_type'] == '2'){
					$val["field_values"] = json_decode($val["field_values"] ,true);
					$selectTpl[] = $val;
				}
				if($val['text_type'] == '3'){
					$areaTpl[] = $val;
				}
				if($val['text_type'] == '4'){
					$timeTpl[] = $val;
				}
			}
			*/
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');
		$para_sys = readS();

		foreach($fielddTpl2 as $val){
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields"){
					$fielddTpl[] = $val;
				}
			}else{
				if($val['field_enabled'] == 'Y'){
					$fielddTpl[] = $val;
				}
			}
		}
		//dump($fielddTpl);die;
		//$this->assign("textTpl",$textTpl);
		//$this->assign("selectTpl",$selectTpl);
		//$this->assign("areaTpl",$areaTpl);
		///$this->assign("timeTpl",$timeTpl);
		$this->assign("phone_num",$phone);
		$this->assign("openid",$openid);
		$this->assign("fielddTpl",$fielddTpl);
		$this->assign("uniqueid",$uniqueid);

		$service = new Model("servicetype");
		$serviceList = $service->select();
		$this->assign("serviceList",$serviceList);

		$hotline_id = $_REQUEST["hotline_id"];
		$this->assign("hotline_id",$hotline_id);

		$this->display();
	}

	//添加客户资料
	function insertCustomer(){
		$dept_id = $_SESSION["user_info"]["d_id"];
		$customer = new Model("customer");
		//手机去重
		$para_sys = readS();
		if($para_sys["repeat_phone"] == "yes"){
			$ph = $_POST["phone1"];
			$phcount = $customer->where("phone1='$ph' OR phone2 = '$ph'")->count();
			if( $phcount>0 ){
					echo json_encode(array('msg'=>"这个手机号已存在！"));
					exit;
			}
		}
		//QQ去重
		if($para_sys["repeat_qq"] == "yes"){
			if($_POST["qq_number"]){
				$qqcount = $customer->where("qq_number='". $_POST["qq_number"] ."'")->count();
				if( $qqcount>0 ){
						echo json_encode(array('msg'=>"这个QQ号已存在！"));
						exit;
				}
			}
		}

		$username = $_SESSION["user_info"]["username"];
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields" && $val["en_name"] != "sms_cust"){
				$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
			}
		}

		if($_REQUEST['hide_fields']){
			$hide_fields = $_REQUEST['hide_fields'];
		}else{
			$hide_fields = "Y";
		}
		$openid = empty($_REQUEST["nickname"]) ? "" : $_REQUEST["openid"];
		$sms_cust = isset($_REQUEST['sms_cust']) ? "Y" : "N";  //定时发送
		$arrData["createtime"] = date("Y-m-d H:i:s");
		$arrData["recently_visittime"] = date("Y-m-d H:i:s");
		$arrData["createuser"] = $username;
		$arrData["customer_source"] = "complete";
		$arrData["hide_fields"] = $hide_fields;
		$arrData["sms_cust"] = $sms_cust;
		$arrData["openid"] = $openid;
		$arrData["dept_id"] = $dept_id;

		//城市
		$arrData["province"] = $_REQUEST['province'];
		$arrData["city"] = $_REQUEST['city'];
		$arrData["district"] = $_REQUEST['district'];
		$arrData["street"] = $_REQUEST['street'];

		//dump($arrData);die;
		$result = $customer->data($arrData)->add();
		//echo $customer->getLastSql();
		$hotline_id = $_REQUEST["hotline_id"];
		if($result){
			if($hotline_id){
				$hotline_record = M("hotline_record");
				$arrData = array(
					'customer_id'=>$result,
					'visit_status'=>"Y",
					'visit_time'=>date("Y-m-d H:i:s"),
					'visit_content'=>$_REQUEST['description'],
					'uniqueid'=>$_SESSION[$username."_hotline"][$hotline_id],
				);
				$result = $hotline_record->data($arrData)->where("id = '$hotline_id'")->save();
			}
			echo json_encode(array('success'=>true,'msg'=>'客户资料添加成功!','lastid'=>$result));
		}else{
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function editCustomer(){
		checkLogin();
		$id = $_GET["id"];
		$uniqueid = $_GET["uniqueid"];
		$phone_num = $_GET["phone_num"];
		$visit_id = $_GET["visit_id"];
		$hotline_id = $_GET["hotline_id"];

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y' && $val["en_name"] != "createuser"){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');
		$para_sys = readS();

		foreach($fielddTpl2 as $val){
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields"){
					$fielddTpl[] = $val;
				}
			}else{
				if($val['field_enabled'] == 'Y'){
					$fielddTpl[] = $val;
				}
			}
		}
		//$fields = "`".implode("`,`",$arrF)."`".",`country`,`province`,`city`,`district`";
		$fields = "`".implode("`,`",$arrF)."`".",openid,createuser,`province`,`city`,`district`,`street`";
		//dump($fielddTpl);die;
		$this->assign("fielddTpl",$fielddTpl);

		//dump($id);
		if($id){
			$this->assign("id",$id);
			$this->assign("visit_id",$visit_id);
			$this->assign("hotline_id",$hotline_id);
			$this->assign("uniqueid",$uniqueid);
			$this->assign("phone_num",$phone_num);
			$customer = new Model("customer");
			$custList = $customer->field($fields)->where("id = $id")->find();
			$userArr = readU();
			$cnName = $userArr["cn_user"];
			//所属客服
			$extension = $_SESSION['user_info']['extension'];
			$users = new Model("users");
			$user = $custList["createuser"];
			$arrU = $users->where("username = '$user'")->find();
			$custList["createuser"] = $user."/".$cnName[$user]."/".$arrU["extension"];
			//dump($custList);die;

			$para_sys = readS();
			$menuname = "Customer Data";
			$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
			$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
			if($para_sys['hide_rule'] == "role"){
				if($priv["hide_phone"] =="Y"){
					$custList["phone"] = $custList["phone1"];
					$custList["telephone"] = $custList["phone2"];
					if( $para_sys["editCustomer_hide"] == "phone" || $para_sys["editCustomer_hide"] == "all" ){
						$custList["phone1"] = substr($custList["phone1"],0,3)."***".substr($custList["phone1"],-4);
						if($custList["phone2"]){
							$custList["phone2"] = substr($custList["phone2"],0,3)."***".substr($custList["phone2"],-3);
						}
					}
					$this->assign("message_phone","ms");
					$this->assign("message_telephone","ms");
				}else{
					$custList["phone"] = $custList["phone1"];
					$this->assign("message_phone","show_phone");
					$this->assign("message_telephone","show_phone");
				}
				if($custList['email']){
					if($priv["hide_email"] =="Y"){
						$custList["email2"] = $custList["email"];
						if( $para_sys["editCustomer_hide"] == "email" || $para_sys["editCustomer_hide"] == "all" ){
							$custList["email"] = "***".strstr($custList["email"],"@");
						}
						$this->assign("message_email","ms");
					}else{
						$custList["email2"] = $custList["email"];
						$this->assign("message_email","show_email");
					}
				}
				if($custList['qq_number']){
					$custList["qq_number2"] = $custList["qq_number"];
					if($priv["hide_qq"] =="Y"){
						if( $para_sys["editCustomer_hide"] == "qq" || $para_sys["editCustomer_hide"] == "all" ){
							$custList["qq_number"] = "***".substr($custList["qq_number"],3);
						}
						$this->assign("message_qq","ms");
					}else{
						$this->assign("message_qq","show_qq");
					}
				}
			}else{
				if($para_sys["hide_phone"] =="yes"){
					$custList["phone"] = $custList["phone1"];
					$custList["telephone"] = $custList["phone2"];
					if( $para_sys["editCustomer_hide"] == "phone" || $para_sys["editCustomer_hide"] == "all" ){
						$custList["phone1"] = substr($custList["phone1"],0,3)."***".substr($custList["phone1"],-4);
						if($custList["phone2"]){
							$custList["phone2"] = substr($custList["phone2"],0,3)."***".substr($custList["phone2"],-3);
						}
					}
					//$custList["phone1"] = $custList["phone1"];
					$this->assign("message_phone","ms");
					$this->assign("message_telephone","ms");
				}else{
					$custList["phone"] = $custList["phone1"];
					$this->assign("message_phone","show_phone");
					$this->assign("message_telephone","show_phone");
				}
				if($custList['email']){
					if($para_sys["hide_email"] =="yes"){
						$custList["email2"] = $custList["email"];
						if( $para_sys["editCustomer_hide"] == "email"  || $para_sys["editCustomer_hide"] == "all" ){
							$custList["email"] = "***".strstr($custList["email"],"@");
						}
						$this->assign("message_email","ms");
					}else{
						$custList["email2"] = $custList["email"];
						$this->assign("message_email","show_email");
					}
				}
				if($custList['qq_number']){
					$custList["qq_number2"] = $custList["qq_number"];
					if($para_sys["hide_qq"] =="yes"){
						if( $para_sys["editCustomer_hide"] == "qq" || $para_sys["editCustomer_hide"] == "all" ){
							$custList["qq_number"] = "***".substr($custList["qq_number"],3);
						}
						$this->assign("message_qq","ms");
					}else{
						$this->assign("message_qq","show_qq");
					}
				}
			}

			//dump($custList);die;

			//dump($custList);
			$this->assign("custList",$custList);

			$service = new Model("servicetype");
			$serviceList = $service->select();
			$this->assign("serviceList",$serviceList);

			import('ORG.Util.Page');
			$servicerecords = new Model("servicerecords");
			$count = $servicerecords->where("customer_id = $id")->count();
			//echo $count;die;
			$page = new Page($count,5);
			$page->setConfig('theme','<span>共%totalRow%%header% &nbsp;  %nowPage%/%totalPage%页</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');
			//$page=new Page($count,'30');
			$show=$page->show();


			//$srdList = $servicerecords->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where("customer_id = $id")->select();
			//$srdList = $servicerecords->order("createtime desc")->table("bgcrm.servicerecords s")->field("s.id,s.createtime,s.content,s.customer_id,s.servicetype_id,s.phone,s.status,s.seat,s.recording,c.uniqueid,c.userfield")->join("asteriskcdrdb.cdr c on (s.recording = c.uniqueid  OR s.recording = c.accountcode)")->limit($page->firstRow.','.$page->listRows)->where("s.customer_id = $id")->select();
			$srdList = $servicerecords->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where("customer_id = $id")->select();

			foreach($srdList as &$val){
				$arrTmp = explode('.',$val["recording"]);
				$timestamp = $arrTmp[0];
				$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
				$WAVfile = $dirPath ."/".$val["recording"].".WAV";

				if(file_exists($WAVfile)){
					$val["recording"] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["recording"])."'".")\" > 播放 </a> ";
				}else{
					$val["recording"] = "";
				}
			}

			//echo $servicerecords->getLastSql();die;
			$this->assign("srdList",$srdList);
			$this->assign('page',$show);
			//dump($srdList);die;
			$file = new Model("filetype");
			$fileList = $file->select();
			$this->assign("fileList",$fileList);

			$filecontent = new Model("filecontent");
			$filecount = $filecontent->where("customerid = $id")->count();
			$filepage = new Page($filecount,5);
			$filepage->setConfig('theme','<span>共%totalRow%%header% &nbsp;  %nowPage%/%totalPage%页</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');
			//$page=new Page($count,'30');
			$show=$filepage->show();
			$fileData = $filecontent->order("createtime desc")->limit($filepage->firstRow.','.$filepage->listRows)->where("customerid = $id")->select();
			$this->assign("fileData",$fileData);
			$this->assign('filepage',$show);

			$username = $_SESSION['user_info']['username'];
			$visit_record = new Model("visit_record");
			$vcount = $visit_record->where("customer_id = '$id'")->count();
			$visitpage = new Page($vcount,5);
			$visitpage->setConfig('theme','<span>共%totalRow%%header% &nbsp;  %nowPage%/%totalPage%页</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');
			//$page=new Page($count,'30');
			$show=$visitpage->show();

			$vsData = $visit_record->order("createtime desc")->limit($visitpage->firstRow.','.$visitpage->listRows)->group("id")->where("customer_id = '$id'")->select();
			//echo $visit_record->getLastSql();
			$i = 0;
			$row = array("phone"=>"电话回访","qq"=>"QQ回访");
			foreach($vsData as &$val){
				if($val['visit_type'] == 'phone'){
					$vsData[$i]['recording'] = "<a href='agent.php?m=CustomerData&a=visitPlayCDR&uniqueid=".$val["uniqueid"]."&phone=".$val["visit_phone"]."&userfield=".$val["userfield"]."&id=".$val["id"]."' target='_blank'>播放</a>";
				}else{
					$vsData[$i]['recording'] = "";
				}

				$visit_type = $row[$val['visit_type']];
				$val['visit_type'] = $visit_type;
				$i++;
			}
			$this->assign("vsData",$vsData);
			$this->assign("vpage",$show);
			//dump($vsData);die;
		}

		$sears = $_GET["sears"];
		$this->assign("sears",$sears);


		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("wj",$arrAL) ){
			$wj_display = "Y";
		}else{
			$wj_display = "N";
		}
		$this->assign("wj_display",$wj_display);

		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$this->display();
	}

	function editCustmoerServiceData(){
		$servicerecords = new Model("servicerecords");
		/*
		$sql = "SHOW COLUMNS FROM servicerecords";
		$arr = $servicerecords->query($sql);
		foreach($arr as $val){
			$ff[] = "s.".$val["Field"];
		}
		$str = implode(",",$ff);
		dump($str);die;
		*/
		$customer_id = $_REQUEST["customer_id"];
		$where = "s.customer_id = '$customer_id'";
		$count = $servicerecords->table("servicerecords s")->join("servicetype st on s.servicetype_id = st.id")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $servicerecords->order("s.createtime desc")->table("servicerecords s")->field("s.id,s.createtime,s.seat,s.status,s.recipientseat,s.phone,s.content,s.content,s.customer_id,s.servicetype_id,s.recording,s.modifytime,st.servicename")->join("servicetype st on s.servicetype_id = st.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		//echo $servicerecords->getLastSql();die;
		$arr = readU();
		$deptId_name = $arr["deptId_name"];
		//echo $para_sys["hide_phone"];die;
		foreach($arrData as &$val){
			$val["phone1"] = $val["phone"];
			if( $para_sys["hide_phone"] == "yes"){
				$val["phone"] = substr($val["phone1"],0,3)."***".substr($val["phone1"],-4);
			}

			$val["dept_id2"] = $deptId_name[$val["dept_id2"]];
			$arrTmp = explode('.',$val["recording"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["recording"].".WAV";

			if(file_exists($WAVfile) ){
				$val["recording"] = trim($val["recording"]);
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["recording"])."'".")\" >播放</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href='javascript:void(0);' onclick=\"openService("."'".$val["phone"]."','".$val["id"]."','".$val["customer_id"]."','".$val["seat"]."'".")\" >转交</a>";
			}else{
				$val['operating'] = "<a href='javascript:void(0);' onclick=\"openService("."'".$val["phone"]."','".$val["id"]."','".$val["customer_id"]."','".$val["seat"]."'".")\" >转交</a>";
			}
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);

	}

	function editCustmoerVisitData(){
		$id = $_REQUEST["id"];
		$visit_record = new Model("visit_record");
		$count = $visit_record->where("customer_id = '$id'")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$visitData = $visit_record->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->group("id")->where("customer_id = '$id'")->select();

		//echo $visit_record->getLastSql();

		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$para_sys = readS();
		$row = array("phone"=>"电话回访","qq"=>"QQ回访");
		$i = 0;
		foreach($visitData as &$val){
			$val["accountcode"] = trim($val["accountcode"]);
			$arrTmp = explode('.',$val["accountcode"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["accountcode"].".WAV";

			if($val['visit_type'] == 'phone' && file_exists($WAVfile) ){
				$visitData[$i]["operating"] = "<a href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["accountcode"])."'".")\" >播放</a>";
			}else{
				$vsData[$i]['operating'] = "";
			}
			$visit_type = $row[$val['visit_type']];
			$val['visit_type'] = $visit_type;

			if($para_sys['hide_rule'] == "role"){
				if($priv["hide_phone"] =="Y"){
					$visitData[$i]["visit_phone"] = substr($val["visit_phone"],0,3)."***".substr($val["visit_phone"],-4);
				}
			}else{
				if($para_sys["hide_phone"] =="yes"){
					$visitData[$i]["visit_phone"] = substr($val["visit_phone"],0,3)."***".substr($val["visit_phone"],-4);
				}
			}

			$i++;
		}

		//dump($visitData);die;

		$rowsList = count($visitData) ? $visitData : false;
		$arrVisit["total"] = $count;
		$arrVisit["rows"] = $rowsList;

		echo json_encode($arrVisit);
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	//编辑客户资料
	function updateCustomer(){
		$username = $_SESSION["user_info"]["username"];
		$id = $_POST["id"];
		//dump($id);
		$customer = new Model("customer");
		$cmFields = getFieldCache();
		$para_sys = readS();
		foreach($cmFields as $val){
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields" && $val["en_name"] != "sms_cust"){
					if($val['en_name'] == 'phone1'){
						if($_REQUEST['message_phone'] == "ms"){
							$arrData['phone1'] = $_REQUEST['mess_phone'];
						}else{
							$arrData['phone1'] = $_REQUEST['phone1'];
						}
					}elseif($val['en_name'] == 'email'){
						if($_REQUEST['message_email'] == "ms"){
							$arrData['email'] = $_REQUEST['mess_email'];
						}else{
							$arrData['email'] = $_REQUEST['email'];
						}
					}elseif($val['en_name'] == 'phone2'){
						if($_REQUEST['message_telephone'] == "ms"){
							$arrData['phone2'] = $_REQUEST['mess_telephone'];
						}else{
							$arrData['phone2'] = $_REQUEST['phone2'];
						}
					}elseif($val['en_name'] == 'qq_number'){
						if($_REQUEST['message_qq'] == "ms"){
							$arrData['qq_number'] = $_REQUEST['mess_qq'];
						}else{
							$arrData['qq_number'] = $_REQUEST['qq_number'];
						}
					}else{
						$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
					}
					$arrF[] = $val['en_name'];
					$tmp[$val['en_name']] = $val['cn_name'];
				}
			}else{
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
					if($val['en_name'] == 'phone1'){
						if($_REQUEST['message_phone'] == "ms"){
							$arrData['phone1'] = $_REQUEST['mess_phone'];
						}else{
							$arrData['phone1'] = $_REQUEST['phone1'];
						}
					}elseif($val['en_name'] == 'email'){
						if($_REQUEST['message_email'] == "ms"){
							$arrData['email'] = $_REQUEST['mess_email'];
						}else{
							$arrData['email'] = $_REQUEST['email'];
						}
					}elseif($val['en_name'] == 'phone2'){
						if($_REQUEST['message_telephone'] == "ms"){
							$arrData['phone2'] = $_REQUEST['mess_telephone'];
						}else{
							$arrData['phone2'] = $_REQUEST['phone2'];
						}
					}elseif($val['en_name'] == 'qq_number'){
						if($_REQUEST['message_qq'] == "ms"){
							$arrData['qq_number'] = $_REQUEST['mess_qq'];
						}else{
							$arrData['qq_number'] = $_REQUEST['qq_number'];
						}
					}else{
						$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
					}
					$arrF[] = $val['en_name'];
					$tmp[$val['en_name']] = $val['cn_name'];
				}
			}
		}
		//dump($tmp);die;
		$sms_cust = isset($_REQUEST['sms_cust']) ? "Y" : "N";
		$openid = empty($_REQUEST["nickname"]) ? "" : $_REQUEST["openid"];

		$arrData["modifytime"] = date("Y-m-d H:i:s");
		$arrData["customer_source"] = "complete";
		$arrData["sms_cust"] = $sms_cust;
		$arrData["openid"] = $openid;


		//城市
		$arrData["province"] = $_REQUEST['province'];
		$arrData["city"] = $_REQUEST['city'];
		$arrData["district"] = $_REQUEST['district'];
		$arrData["street"] = $_REQUEST['street'];

		$fields = "`".implode("`,`",$arrF)."`,`province`,`city`,`district`,`street`";

		$upBeforeData = $customer->field($fields)->where("id = $id")->find();

		//dump($cmFields);die;
		//dump($arrData);die;
		$result = $customer->data($arrData)->where("id = $id")->save();
		//echo $customer->getLastSql();

		//修改记录开始-------------------------------
		$upAfterData = $customer->field($fields)->where("id = $id")->find();
		$arrModify = Array();
		foreach($upBeforeData AS $k=>$v){
			if( $v !== $upAfterData[$k]){
				$arrModify[$k] = Array(
					'upbefore'=>$v,
					'upafter'=>$upAfterData[$k],
					'name'=>$k,
					'field'=>$k,
				);
			}

		}
		//dump($arrModify);die;
		/*
		var_export($upBeforeData);
		var_export($upAfterData);
		$arrDiff = array_diff($upBeforeData,$upAfterData);    //array_diff这个函数有bug
		$arrDiff2 = array_diff($upAfterData,$upBeforeData);
		dump($arrDiff);
		dump($arrDiff2);
		die;


		foreach($arrDiff as $k=>$v){
			$arrModify[$k] = Array(
				'upbefore'=>$v,
				'upafter'=>$arrDiff2[$k],
				'name'=>$k,
				'field'=>$k,
			);
		}*/

		$row = getSelectCache();
		$i = 0;
		foreach($arrModify as $k=>$vm){
			$arrModify[$k]['name'] = $tmp[$vm['name']];
			//$arrModify[$k]['upbefore'] = $row[$k][$vm['upbefore']];
			//$arrModify[$k]['upafter'] = $row[$k][$vm['upafter']];
			$i++;
		}
		//dump($arrModify);
		//修改记录结束-------------------------------

		//echo $customer->getLastSql();die;
		if ($result !== false){
			$this->insertModifyRecord($id,json_encode($arrModify));
			echo json_encode(array('success'=>true,'msg'=>'客户资料修改成功!','editid'=>$id));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	//添加修改记录
	function insertModifyRecord($id,$arrModify){
		$modify = new Model("customer_modify_record");
		$arrData = array(
			"customer_id"=>$id,
			"modifytime"=>date("Y-m-d H:i:s"),
			"modifiedby"=>$_SESSION["user_info"]["username"],
			"modify_content"=>$arrModify,
		);
		$result = $modify->data($arrData)->add();
	}


	function modifyData(){
		$modify = new Model("customer_modify_record");

		$count = $modify->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$modData = $modify->table("customer_modify_record m")->field("m.id,m.customer_id,m.modifytime,m.modifiedby,m.modify_content,c.name")->join("customer c on (m.customer_id = c.id)")->limit($page->firstRow.','.$page->listRows)->select();

		//echo $modify->getLastSql();
		foreach($modData as &$val){
			$val['modify_content'] = json_decode($val['modify_content'],true);
		}

		foreach($modData as $v){

			foreach($v['modify_content'] as $vm){
				$tt[] = $vm[''];
			}
		}

		//dump($modData);die;
		$rowsList = count($modData) ? $modData : false;
		$arrModify["total"] = $count;
		$arrModify["rows"] = $rowsList;

		echo json_encode($arrModify);
	}


	//将客户资料放到回收站
	function recycleCustomer(){
		$id = $_REQUEST["id"];
		$arrId = explode(",",$id);
		$count = count($arrId);
		//dump($id);die;
		$customer = new Model("customer");


		$arrData = array(
			recycle => 'Y',
		);
		for($i=0;$i<$count;$i++){
			$result[$i] = $customer->data($arrData)->where("id = $arrId[$i]")->save();
		}
		if ($result[0] !== false){
			echo json_encode(array('success'=>true,'msg'=>"删除成功！"));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}

	}

	//还原客户资料
	function reductionCustomer(){
		$id = $_REQUEST["id"];
		$arrId = explode(",",$id);
		$count = count($arrId);
		//dump($id);die;
		$customer = new Model("customer");


		$arrData = array(
			recycle => 'N',
		);
		for($i=0;$i<$count;$i++){
			$result[$i] = $customer->data($arrData)->where("id = $arrId[$i]")->save();
		}
		if ($result[0] !== false){
			echo json_encode(array('success'=>true,'msg'=>"删除成功！"));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}

	}


	function deleteCustomer(){
		$id = $_REQUEST["id"];
		//dump($id);die;
		$customer = new Model("customer");
		$result = $customer->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}



	function addVisit(){
		checkLogin();
		$id = $_REQUEST['id'];
		//dump($id);
		$this->assign("id",$id);
		$this->display();
	}

	function saveVisits(){
		$customer_id = explode(",",$_REQUEST['id']);
		$policy_type = $_REQUEST['policy_type'];
		$time_period = $_REQUEST['time_period'];
		$recipient = $_SESSION["user_info"]["username"];
		$cycle = $_REQUEST['cycle'];
		$infos = explode(",",$_REQUEST['info']);

		foreach($infos as $vm){
			$arrInfo[] = explode("->",$vm);
		}
		$date = date("Y-m-");
		if($policy_type == '2'){  //规则性
			if($time_period == '1'){  //每周
				$n = 0;
				foreach($arrInfo as $val){
					if($arrInfo[$n][1] == '星期一'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Monday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期二'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Tuesday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期三'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Wednesday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期四'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Thursday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期五'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Friday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期六'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Saturday"))." ".$_REQUEST['start_time'].":00";
					}
					if($arrInfo[$n][1] == '星期日'){
						$arrInfo[$n]['time'] = date("Y-m-d",strtotime("Sunday"))." ".$_REQUEST['start_time'].":00";
					}
					$n++;
				}
				foreach($arrInfo as $val){
					$info[] = $val['time'];
				}
			}else{  //每月
				foreach($arrInfo as &$val){
					if($val[1] <= 9){
						$info[] = $date."0".$val[1]." ".$val[0].":00";
					}else{
						$info[] = $date.$val[1]." ".$val[0].":00";
					}
				}
			}
		}else{
			foreach($arrInfo as &$val){
				$info[] = $val[1]." ".$val[0].":00";
			}
		}

		//dump($arrInfo);die;
		if($policy_type == '2'){  //规则性
			if($time_period == '1'){  //每周
				for($i=0;$i<count($info);$i++){
					for($j=0;$j<$cycle;$j++){
						$visit_time[] =  date('Y-m-d H:i:s',strtotime($info[$i]." +".$j." week"));
					}
				}

			}else{  //每月
				for($i=0;$i<count($info);$i++){
					for($j=0;$j<$cycle;$j++){
						$visit_time[] =  date('Y-m-d H:i:s',strtotime($info[$i]." +".$j." month"));
					}
				}
			}
		}else{  //不规则性
			$visit_time = $info;
		}
		foreach($customer_id as $val){
			$visit[$val] = $visit_time;
		}
		//dump($visit);die;
		$sql = "insert into visit_customer(customer_id, visit_time, visit_status,visit_name ) values ";
		$value = "";
		foreach( $visit AS $key=>$row ){
			foreach($row AS $v){
				$s = "($key,'$v','N','$recipient')";
				$value .= empty($value) ? $s : ",".$s;
			}
		}
		$sql .= $value;
		//dump($sql);die;
		$visit_customer = new Model("visit_customer");
		$result = $visit_customer->execute($sql);

		//visit_task_enabled
		$customer = new Model("customer");
		$res = $customer->where("id in (".$_REQUEST['id'].")")->save(array('visit_task_enabled'=>'Y'));

		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	//添加服务记录---添加售后记录
	function insertService(){
		if($_REQUEST['message_phone'] == "ms"){
			$phone = $_REQUEST['mess_phone'];
		}else{
			$phone = $_REQUEST['phone'];
		}
		$time = date("Y-m-d H:i:s");
		$username = $_SESSION["user_info"]["username"];
		$d_id = $_SESSION["user_info"]["d_id"];
		//$extension = $_SESSION["user_info"]["extension"];
		//$seat = $username.'('.$extension.')';
		$service = new Model("servicerecords");
		$arrData = array(
			//"createtime"=>$_REQUEST["createtime"],
			"createtime"=>$time,
			"servicetype_id"=>$_REQUEST["servicetype_id"],
			"status"=>$_REQUEST["status"],
			"phone"=>$phone,
			//"recording"=>$_REQUEST["uniqueid"],
			"recording"=>$_REQUEST["record_uniqueid"],
			"content"=>$_REQUEST["content"],
			"seat"=>$username,
			"dept_id"=>$d_id,
			"customer_id"=>$_REQUEST["id"],
		);
		//dump($_REQUEST);die;
		$result = $service->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'服务记录添加成功'));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	//修改服务记录---售后记录
	function updateService(){
		$id = $_GET['id'];
		$time = date("Y-m-d H:i:s");
		//dump($id);
		$service = new Model("servicerecords");
		$arrData = array(
			//"createtime"=>$_REQUEST["createtime"],
			"modifytime"=>$time,
			"servicetype_id"=>$_REQUEST["servicetype_id_".$id],
			"status"=>$_REQUEST["status_".$id],
			"phone"=>$_REQUEST["phone_".$id],
			"content"=>$_REQUEST["content_".$id],
			//"seat"=>$seat,
			//"customer_id"=>$_POST["id"],
		);
		//dump($arrData);die;
		$result = $service->data($arrData)->where("id = $id")->save();
		//echo $service->getLastSql();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'服务记录修改成功'));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	//添加上传文件---添加相关文件
	function insertFile(){
		//$filepath = "/var/www/html/data/attechment/". date("Y-m-d")."/";
		$filepath = "data/attechment/". date("Y-m-d")."/";
		if(! is_dir($filepath)){
			mkdir("mkdir $filepath");
		}

		$tmp_file = $_FILES["filepath"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["filepath"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));
		if($suffix == "sql" || $suffix == "php"){
			echo json_encode(array('msg'=>"上传的类型不正确！"));
			die;
		}


		$file_name = $_REQUEST["file_name"];
		$pattern = '/[^\x00-\x80]/';
		//判断字符串是否全是中文或含有中文的实现代码
		if(preg_match($pattern,$file_name)){
			$cn_str = "Y";
		}else{
			$cn_str = "N";
		}

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize ='1000000';
		$upload->savePath= $filepath;  //上传路径
		if($cn_str == "Y"){
			$upload->saveRule=uniqid;    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		}
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		//$upload->allowExts=array('jpg','jpeg','png','gif','txt','xls','xlsx','doc','ppt','pdf','zip','RAR','rar','avi','mp3','mp4','rm');     //准许上传的文件后缀
		//$upload->allowTypes=array('image/png','image/jpg','image/pjpeg','image/gif','image/jpeg');  //检测mime类型
		//dump($_REQUEST);die;
		if(!$upload->upload()){ 	// 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			echo json_encode(array('msg'=>$mess));
		}else{
			$info=$upload->getUploadFileInfo();
			//dump($info);
			$username = $_SESSION["user_info"]["username"];
			$filecontent = new Model("filecontent");
			$arrData = Array(
			  'createtime' =>date("Y-m-d H:i:s"),
			  'filetype_id' => $_REQUEST['filetype_id'],
			  'customerid' => $_REQUEST['customer_id'],
			  'filepath' => $info[0]["savepath"],
			  'filecontentname' => $info[0]["savename"],
			  'description' => $_REQUEST['description'],
			  'createuser' => $username,
			  'contract_amount' => $_REQUEST['contract_amount'],
			  'repayment_installments' => $_REQUEST['repayment_installments'],
			  'has_also_amount' => $_REQUEST['has_also_amount'],
			  'contract_number' => $_REQUEST['contract_number'],
			  'repay_plan_date' => $_REQUEST['repay_plan_date'],
			  'repayment_status' => "未付款",
			);
			//dump($arrData);die;
			$result = $filecontent->data($arrData)->add();
			if ($result){
				echo json_encode(array('success'=>true,'msg'=>'文件上传成功'));
			} else {
				echo json_encode(array('msg'=>'Some errors occured.'));
			}
		}

	}

	function updateFile(){
		$id = $_REQUEST['id'];
		$filecontent = new Model("filecontent");
		$arrData = Array(
			  'filetype_id' => $_REQUEST['filetype_id'],
			  'description' => $_REQUEST['description'],
			  'contract_amount' => $_REQUEST['contract_amount'],
			  'repayment_installments' => $_REQUEST['repayment_installments'],
			  'has_also_amount' => $_REQUEST['has_also_amount'],
			  'contract_number' => $_REQUEST['contract_number'],
			  'repay_plan_date' => $_REQUEST['repay_plan_date'],
			);
		$result = $filecontent->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	//下载相关文件
	function downloadFile(){
		header("Content-Type:text/html; charset=utf-8");
		$path = $_GET["path"];
		$filename = urlencode($_GET["filename"]);
		$filePath = $path.$filename;
		//dump($filePath);die;
		if(!file_exists($filePath)){
			//$this->error("File $WAVfile is not exist!");
			goback("File $filename is not exist!","");
			//echo "<script>alert('File $filePath is not exist!');</script>";
		}

        header('HTTP/1.1 200 OK');
        //header('Accept-Ranges: bytes');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($filePath)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($filePath))."");
        readfile($filePath);
	}

	//付款信息
	function payInfoList(){
		$id = $_REQUEST['id'];
		$customer_id = $_REQUEST['customer_id'];
		$this->assign("filecontent_id",$id);
		$this->assign("customer_id",$customer_id);

		$now_date = date("Y-m-d");
		$this->assign("now_date",$now_date);

		$this->display();
	}

	function payInfoData(){
		$username = $_SESSION['user_info']['username'];
		$filecontent_id = $_REQUEST['filecontent_id'];
		$customer_id = $_REQUEST['customer_id'];

		$customer_pay = new Model("customer_pay");
		$fields = "cp.id,cp.create_name,cp.createtime,cp.pay_date,cp.customer_id,cp.filecontent_id,cp.payment_types,cp.payment_process,cp.repayment_times,cp.repayment_amount,cp.remark,c.name,c.phone1,c.dept_id,c.createuser,c.company,f.contract_number,f.contract_amount,f.repayment_installments,f.has_also_amount";

		$where = "1 ";
		$where .= empty($filecontent_id) ? "" : " AND cp.filecontent_id='$filecontent_id'";

		$count = $customer_pay->table("customer_pay cp")->field($fields)->join("customer c on (cp.customer_id = c.id)")->join("filecontent f on (cp.filecontent_id = f.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		$arrData = $customer_pay->order("cp.createtime desc")->table("customer_pay cp")->field($fields)->join("customer c on (cp.customer_id = c.id)")->join("filecontent f on (cp.filecontent_id = f.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$rows = array("first"=>"第一阶段","second"=>"第二阶段","third"=>"第三阶段","fourth"=>"第四阶段","fifth"=>"第五阶段","complete"=>"付款完成");
		foreach($arrData as &$val){
			$payment_process = $rows[$val["payment_process"]];
			$val["payment_process2"] = $payment_process;
			/*
			$createuser = $val["createuser"];
			$name = $val["name"];
			$phone1 = $val["phone1"];
			$company = $val["company"];
			*/
		}

		$filecontent = new Model("filecontent");
		$pay_data = $filecontent->where("id = '$filecontent_id'")->find();
		$customer = new Model("customer");
		$arrF = $customer->field("name,phone1,dept_id,createuser,company")->where("id = '$customer_id'")->find();

		$pay_data["create_user"] = $arrF["createuser"];
		$pay_data["name"] = $arrF["name"];
		$pay_data["phone1"] = $arrF["phone1"];
		$pay_data["company"] = $arrF["company"];

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		$arrT["pay_data"] = $pay_data;

		echo json_encode($arrT);
	}

	function insertPayInfo(){
		$username = $_SESSION['user_info']['username'];
		$customer_pay = new Model("customer_pay");
		$filecontent_id = $_REQUEST['filecontent_id'];
		$arrData = Array(
			'create_name'=>$username,
			'createtime'=>date("Y-m-d H:i:s"),
			'customer_id'=>$_REQUEST['customer_id'],
			'filecontent_id'=>$filecontent_id,
			'payment_types'=>$_REQUEST['payment_types'],
			'payment_process'=>$_REQUEST['payment_process'],
			'repayment_times'=>$_REQUEST['repayment_times'],
			'repayment_amount'=>$_REQUEST['repayment_amount'],
			'pay_date'=>$_REQUEST['pay_date'],
			'remark'=>$_REQUEST['remark'],
		);
		$result = $customer_pay->data($arrData)->add();
		if ($result){
			$filecontent = new Model("filecontent");
			$arrT = $filecontent->where("id = '$filecontent_id'")->find();
			$has_also_amount = $arrT["has_also_amount"]+$_REQUEST['repayment_amount'];
			if($has_also_amount == $arrT["contract_amount"]){
				$repayment_status = "付款完成";
			}elseif($has_also_amount == "0"){
				$repayment_status = "未付款";
			}else{
				$repayment_status = "部分已付款";
			}
			$arrF = array(
				"has_also_amount"=>$has_also_amount,
				"repayment_status"=>$repayment_status,
			);
			$filecontent->data($arrF)->where("id = '$filecontent_id'")->save();

			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updatePayInfo(){
		$id = $_REQUEST['id'];
		$filecontent_id = $_REQUEST['filecontent_id'];
		$customer_pay = new Model("customer_pay");
		$arrData = Array(
			'payment_types'=>$_REQUEST['payment_types'],
			'payment_process'=>$_REQUEST['payment_process'],
			'repayment_times'=>$_REQUEST['repayment_times'],
			'repayment_amount'=>$_REQUEST['repayment_amount'],
			'pay_date'=>$_REQUEST['pay_date'],
			'remark'=>$_REQUEST['remark'],
		);
		$result = $customer_pay->data($arrData)->where("id=$id")->save();
		$arrPay = $customer_pay->where("id != '$id' AND filecontent_id='$filecontent_id'")->sum("repayment_amount");

		if ($result !== false){
			$filecontent = new Model("filecontent");
			$arrT = $filecontent->where("id = '$filecontent_id'")->find();
			$has_also_amount = $arrPay+$_REQUEST['repayment_amount'];
			if($has_also_amount == $arrT["contract_amount"]){
				$repayment_status = "付款完成";
			}elseif($has_also_amount == "0"){
				$repayment_status = "未付款";
			}else{
				$repayment_status = "部分已付款";
			}
			$arrF = array(
				"has_also_amount"=>$has_also_amount,
				"repayment_status"=>$repayment_status,
			);
			$filecontent->data($arrF)->where("id = '$filecontent_id'")->save();

			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function serviceList(){
		checkLogin();
		$this->display();
	}

	function serciveDataList(){
		$username = $_SESSION["user_info"]["username"];
		//$extension = $_SESSION["user_info"]["extension"];
		//$seat = $username.'('.$extension.')';

		$servicerecords = new Model("servicerecords");
		import('ORG.Util.Page');
		if($username == "admin"){
			$count = $servicerecords->count();
		}else{
			$count = $servicerecords->where("seat = '$username' ")->count();
		}
        $_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == "admin"){
			$serverList = $servicerecords->order("sv.createtime desc")->table("servicerecords sv")->field("c.name,sv.phone,sv.id,sv.seat,sv.createtime,sv.customer_id,sv.recording,sv.status,st.servicename")->join("customer c  on sv.customer_id = c.id")->join("servicetype st on sv.servicetype_id = st.id")->limit($page->firstRow.','.$page->listRows)->select();
		}else{
			$serverList = $servicerecords->order("sv.createtime desc")->table("servicerecords sv")->field("c.name,sv.phone,sv.id,sv.seat,sv.createtime,sv.customer_id,sv.recording,sv.status,st.servicename")->join("customer c  on sv.customer_id = c.id")->join("servicetype st on sv.servicetype_id = st.id")->limit($page->firstRow.','.$page->listRows)->where("seat = '$username' OR recipientseat = '$username'")->select();
		}

		$i = 0;
		foreach($serverList as &$val){

			$arrTmp = explode('.',$val["recording"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$val["recording"].".WAV";

			if(file_exists($WAVfile)){
				$val["recording"] = "<a  href='javascript:void(0);' onclick=\"palyRecording("."'".trim($val["recording"])."'".")\" > 播放 </a> ";
			}
			$id = $serverList[$i]["customer_id"];
			$svid = $serverList[$i]["id"];
			$serverList[$i]["name"] = "<a href='javascript:void(0);' onclick=\"service_editCustomer($id,"."'".$val["name"]."'".");\">". $val["name"] ."</a>";
			$serverList[$i]["operating"] = "<a href='javascript:void(0);'  onclick=\"openProcess($id,$svid,"."'".$val["servicename"]."'".");\">". 转交历史 ."</a>";
			$i++;
		}

		//echo $servicerecords->getLastSql();
		//dump($serverList);die;

		$rowsList = count($serverList) ? $serverList : false;
		$aryService["total"] = $count;
		$aryService["rows"] = $rowsList;

		echo json_encode($aryService);
	}

	function addProcess(){
		checkLogin();
		//$username = $_SESSION["user_info"]["username"];
		$serviceid = $_GET["id"];
		$customer_id = $_GET["customer_id"];
		$seat = $_GET["seat"];

		if($customer_id){
			$customer = new Model("customer");
			$custData = $customer->where("id = $customer_id")->find();
			$servicerecords = new Model("servicerecords");
			$serviceData = $servicerecords->where("id = $serviceid")->find();

			$content = "客户姓名：".$custData["name"]."  公司名称：".$custData["company"]."  手机：".$serviceData["phone"]."   地址：".$custData["address"]."   工单内容：".$serviceData["content"];
			//dump($content);die;
			$this->assign("content",$content);
		}
		$this->assign("serviceid",$serviceid);
		$this->assign("customer_id",$customer_id);
		$this->assign("seat",$seat);
		$users = new Model("users");
		$ulist = $users->select();
		$this->assign("ulist",$ulist);

		$this->display();
	}




	function getTree($data, $pId) {
        $tree = '';
		//dump($data);die;
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
				//dump($v['state']);
				$v['children'] = $this->getTree($data, $v['tid']);
				if ( empty($v["children"])  )  unset($v['children']) ;
				/*
				if ( empty($v["children"]) && $v['state'] =='closed')  $v['children'] =  array(array());
				*/
				if ( empty($v["children"]) && $v['state'] =='closed'){
				$v['state'] =  'open';
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	function userProcess(){
		$department = new Model("department");
		$dept = $department->field("d_name as text,d_id as tid,d_pid as pid")->select();

		$users = new Model("users");
		$userList = $users->field("username as text,d_id as pid")->select();
		//dump($userList);

		$j = 0;
		foreach($dept as $vm){
			$dept[$j]['iconCls'] = "icon-files";
			$dept[$j]['state'] = "closed";
			$j++;
		}
		foreach($userList as $val){
			$arr[$val["pid"]][] = $val;
			array_push($dept,$val);
		}
        $arrTree = $this->getTree($dept,0);
		$strJSON = json_encode($arrTree);
		echo ($strJSON);

	}



	//工单转交
	function insertProcess(){
		$username = $_SESSION["user_info"]["username"];
		$time = date("Y-m-d H:i:s");
		$service_id = $_REQUEST["serviceid"];
		$customer_id = $_REQUEST["customer_id"];
		$process = new Model("process");

		$custid = $process->where("customerid = $customer_id AND service_id = $service_id")->select();
		$customid = count($custid);
		$turn_id = $customid+1;
		//dump($turn_id);die;

		$arrData = array(
			"createtime"=>$time,
			"recipient"=>$_GET["deptuser"],    //处理人/接收人
			"noticetype"=>$_REQUEST["noticetype"],  //通知方式/类型  短信和邮件
			"modifycontent"=>$_REQUEST["modifycontent"],   //提醒内容
			"modifytime"=>$_REQUEST["modifytime"],  //提醒时间
			"turn_status"=>"待处理",

			"service_id"=>$_REQUEST["serviceid"],  //服务id
			"customerid"=>$_REQUEST["customer_id"], //客户id
			"turn_id"=>$turn_id,
			//"forwardedname"=>$_REQUEST["seat"],          //转交人
			"forwardedname"=>$username,          //转交人
		);
		//dump($arrData);die;
		$result = $process->data($arrData)->add();

		$servicerecords = new Model("servicerecords");
		$res1 = $servicerecords->where("id = $service_id")->save( array( "recipientseat"=>$_GET["deptuser"] ) );
		$serviceData = $servicerecords->where("id = $service_id")->find();
		$servicetype_id = $serviceData["servicetype_id"];

		$customer = new Model("customer");
		$custData = $customer->where("id = $customer_id")->find();

		$servicetype = new Model("servicetype");
		$stypeData = $servicetype->where("id = $servicetype_id")->find();

		//$waittitle = "[工单转交]  "."[".$stypeData["servicename"]."]  [".$custData["name"]."]";
		$waittitle = "[".$stypeData["servicename"]."] [".$custData["name"]."]";
		//dump($waittitle);
		$waitcontent = $serviceData["content"];
		$url = "agent.php?m=CustomerData&a=editCustomer&id=".$customer_id;

		$waitmatter = new Model("waitmatter");
		$arrWaitData = array(
			"createtime"=>$time,
			"title"=>$waittitle,
			"remindertime"=>$_REQUEST["modifytime"],
			"content"=>$waitcontent,
			"status"=>"N",
			"matterstype"=>"3",
			"username"=>$_REQUEST["deptuser"],
			"url"=>$url,
		);
		//dump($arrWaitData);
		$waitResult = $waitmatter->data($arrWaitData)->add();


		$noticetype = $_REQUEST["noticetype"];
		$modifycontent = $_REQUEST["modifycontent"];
		//$address =  $_SESSION["user_info"]["email"];
		$recipient = $_REQUEST["deptuser"];
		$user = new Model("users");
		$phone = $user->where("username = '$recipient'")->find();
		//dump($phone);die;
		if($noticetype == "短信方式"){
			$mobile = $phone["phone"];
			if($mobile){
				$message = $modifycontent;
				//dump($message);die;
				$result = asterSendSMS($phone,$message,$retime);
				if($result == "2000"){
					echo "<script>alert('短信发送成功');</script>";
				}else{
					echo "<script>alert('短信发送失败${result}');</script>";
				}
			}else{
				echo "<script>alert('该用户没存手机号码');</script>";
			}
			/*
			session_start();
			//include "Sms/api/sms.inc.php";
			import("ORG.Sms.Sms");
			$newclient=new SMS();
			if($newclient->ConfNull=="1"){
				//$smscontent = $_REQUEST["modifycontent"];
				$mobile = $phone["phone"];
				if($mobile){
					$message = utf2gb($modifycontent);
					$respxml=$newclient->sendSMS($mobile, $message, $time);

					$_SESSION["xml"]=$newclient->sendXML;
					$_SESSION["respxml"]=$respxml;
					$code=$newclient->getCode();
						if($code == "2000"){
							echo "<script>alert('短信发送成功');</script>";
						}
				}else{
					echo "<script>alert('该用户没存手机号码');</script>";
				}
			}else{
				$code = "<font color='red'>失败</font>";
				$ermess = "<font color='red'>你还没有配置文件Sms/api/config.inc.php</font>";
				$error = "<font color='red'>失败</font>";
			}
			*/
		}elseif($noticetype == "邮件方式"){
			$address = $phone["email"];
			if($address){
				import("ORG.Email.Phpmailer");
				$arrF = getEmailSmtp();
				$arrM = $arrF[$username];

				if($arrM["mail_host"] && $arrM["mail_from"] && $arrM["mail_password"] && $arrM["mail_enable"] == "Y"){
					$para_sys = $arrM;
				}else{
					$para_sys = readS();  //读取缓存
				}


				$userEmail = $para_sys["mail_from"];
				$emailPassword = $para_sys["mail_password"];
				$smtpHost = $para_sys["mail_host"];
				//dump($para_sys["mail_from"]);die;

				$mail = new PHPMailer(); //建立邮件发送类
				$mail->IsSMTP(); // 使用SMTP方式发送
				$mail->CharSet='UTF-8';// 设置邮件的字符编码
				$mail->Host = $smtpHost;
				$mail->SMTPAuth = true; // 启用SMTP验证功能
				$mail->Username = $userEmail;
				$mail->Password = $emailPassword;
				$mail->From = "$userEmail"; //邮件发送者email地址
				$mail->FromName = $waittitle;

				//收件人地址，可以替换成任何想要接收邮件的email信箱,格式是AddAddress("收件人email","收件人姓名")
				$mail->AddAddress($address);
				$mail->Subject = $mail->FromName; //邮件标题
				$mail->Body = $modifycontent; //邮件内容
				if(!$mail->Send()){
					echo "邮件发送失败. <p>";
					echo "错误原因: " . $mail->ErrorInfo;
					return false;
				}else{
					echo "<script>alert('邮件发送成功');</script>";
				}
			}else{
				echo "<script>alert('该用户没有在注册时没填Email');</script>";
			}
		}



		if ($result && $waitResult){
			echo json_encode(array('success'=>true,'msg'=>'服务记录添加成功'));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}

	}

	function getNormalData(){
		$customer = new Model("customer");
		$phone = $_POST["phone_num"];
		$uniqueid = $_POST["uniqueid"];

		//$where = "phone1= '$phone' OR phone2 = '$phone'";
		$where = "phone1 like '%$phone%' OR phone2 like '%$phone%'";

		$data = $customer->field("id,name,phone1,phone2")->where($where)->find();

		//回访录音相关联
		$username = $_SESSION['user_info']['username'];
		/*
		$customer_id = $data['id'];
		$_SESSION[$username."_visit"][$customer_id] = $uniqueid;
		//dump($_SESSION);die;
		*/

		//解决重复号码没有录音的方法
		$vcmData = $customer->field("id,name,phone1,phone2")->where($where)->select();
		foreach($vcmData as $val){
			$_SESSION[$username."_visit"][$val["id"]] = $uniqueid;	//回访
			$_SESSION[$username."_order"][$val["id"]] = $uniqueid;	//订单
			$_SESSION[$username."_work_order"][$val["id"]] = $uniqueid;	//工单
			$_SESSION[$username."_other"][$val["id"]] = $uniqueid;	//其它
		}

		//热线资源回访--录音
		$hotline_record = M("hotline_record");
		$arrData = $hotline_record->field("id,phone")->where("phone = 'phone'")->select();
		foreach($arrData as $val){
			$_SESSION[$username."_hotline"][$val["id"]] = $uniqueid;
		}
		echo json_encode($data);
	}

	function getWeChatData(){
		$openid = $_REQUEST["openid"];
		$customer = new Model("customer");
		$data = $customer->where("openid= '$openid'")->find();
		//echo $customer->getLastSql();
		//dump($data);die;
		echo json_encode($data);
	}

	function forwardedList(){
		checkLogin();
		$id = $_GET["id"];
		$process = new Model("process");
		import('ORG.Util.Page');
		$count = $process->where("customerid = $id")->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$processData = $process->order("p.createtime asc")->field("c.name,p.createtime,p.recipient,p.forwardedname,p.noticetype,p.modifycontent")->table("`process` p")->join("customer c on p.customerid = c.id")->join("servicerecords s on p.service_id = s.id ")->limit($page->firstRow.','.$page->listRows)->where("customerid = $id")->select();

		$rowsList = count($processData) ? $processData : false;
		$arrProcess["total"] = $count;
		$arrProcess["rows"] = $rowsList;
		$strTmp = json_encode($arrProcess);
		$this->assign("strTmp",$strTmp);

		$this->display();
	}

	function playCDR(){
		checkLogin();
		$clientphone = $_GET['phone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		header('Content-Type: text/html; charset=utf-8');
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$AudioURL = "agent.php?m=CustomerData&a=downloadCDR&uniqueid=${uniqueid}&clientphone=${clientphone}" ."&userfield=".$userfield;
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->assign("clientphone",$clientphone);

		$id = $_GET["id"];
		$servicerecords = new Model("servicerecords");
		$srdList = $servicerecords->order("createtime desc")->table("servicerecords s")->field("s.id,s.createtime,s.servicetype_id,s.phone,s.status,s.seat,s.recording,c.name,st.servicename")->join("customer c on s.customer_id = c.id")->join("servicetype st on s.servicetype_id = st.id")->limit($page->firstRow.','.$page->listRows)->where("s.id = $id")->find();
		$this->assign("srdList",$srdList);
		//echo $servicerecords->getLastSql();
		//dump($srdList);

		$this->display();
	}

	function visitPlayCDR(){
		checkLogin();
		//$clientphone = $_GET['phone'];
		$uniqueid = $_GET['uniqueid'];
		//$userfield = $_GET['userfield'];
		header('Content-Type: text/html; charset=utf-8');
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$AudioURL = "agent.php?m=CustomerData&a=downloadCDR2&uniqueid=${uniqueid}";
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->assign("clientphone",$clientphone);
		/*
		$id = $_GET["id"];
		$visit_record = new Model("visit_record");
		$visitData = $visit_record->table("visit_record v")->field("v.id,v.createtime,v.customer_id,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->where("v.id = '$id'")->find();
		$this->assign("visitData",$visitData);
		//echo $visit_record->getLastSql();
		//dump($visitData);
		*/
		$this->display();
	}

	////192.168.1.210/agent.php?m=CustomerData&a=playQualityRecord&phone=13828863511&uniqueid=1381667397.2680&workno=test8201&id=1&userfield=203001_8201_1381667397.2680
	function playQualityRecord(){
		checkLogin();
		$clientphone = $_GET['phone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		$workno = $_GET['workno'];
		$id = $_GET['id'];
		$customer_id = $_GET['customer_id'];

		header('Content-Type: text/html; charset=utf-8');
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$AudioURL = "agent.php?m=CustomerData&a=downloadCDR&uniqueid=${uniqueid}&clientphone=${clientphone}" ."&userfield=".$userfield;
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->assign("clientphone",$clientphone);
		$this->assign("workno",$workno);
		$this->assign("id",$id);

		$norm = new Model("normtype");
		$normList = $norm->select();
		$this->assign("normList",$normList);



		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				if($val['text_type'] == '1'){
					$textTpl[] = $val;
				}
				if($val['text_type'] == '2'){
					$val["field_values"] = json_decode($val["field_values"] ,true);
					$selectTpl[] = $val;
				}
				if($val['text_type'] == '3'){
					$areaTpl[] = $val;
				}
				if($val['text_type'] == '4'){
					$timeTpl[] = $val;
				}
				$arrF[] = $val['en_name'];
			}
		}
		$fields = "`".implode("`,`",$arrF)."`".",`country`,`province`,`city`,`district`";
		//dump($fields);die;
		$this->assign("textTpl",$textTpl);
		$this->assign("selectTpl",$selectTpl);
		$this->assign("areaTpl",$areaTpl);
		$this->assign("timeTpl",$timeTpl);

		$customer = new Model("customer");
		$cData = $customer->field($fields)->where("id = '$customer_id'")->find();
		$this->assign("custList",$cData);

		$this->display();
	}

	//agent.php?m=CustomerData&a=downloadCDR&uniqueid=1380939622.10&clientphone=15279129837&userfield=102024_801_1380939622.10
	function downloadCDR(){
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		$WAVfile = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp) .'/'. $userfield;
		if(file_exists($WAVfile .".WAV")){
			$WAVfile = $WAVfile .".WAV";
		}elseif(file_exists($WAVfile .".wav")){
			$WAVfile = $WAVfile .".wav";
		}
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=" .$userfield .".mp3");
        system("/usr/local/bin/sox $WAVfile -t mp3 -");

	}

	//agent.php?m=CustomerData&a=downloadCDR2&uniqueid=1382335463.12361
	function downloadCDR2(){
		$uniqueid = $_GET['uniqueid'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		if( $_GET['userfield'] ){
			$userfield = $_GET['userfield'];
		}else{
			$cdr = M("asteriskcdrdb.cdr");
			$arrT = $cdr->where("uniqueid='$uniqueid'")->find();
			$userfield = $arrT['userfield'];
		}
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		$WAVfile = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp) .'/'. $userfield;
		if(file_exists($WAVfile .".WAV")){
			$WAVfile = $WAVfile .".WAV";
		}elseif(file_exists($WAVfile .".wav")){
			$WAVfile = $WAVfile .".wav";
		}
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=" .$userfield .".mp3");
        system("/usr/local/bin/sox $WAVfile -t mp3 -");

	}

	//取得号码的省份信息
	function getPhoneInfo(){
		$phone = $_GET['phone'];
		$prefix = $_GET['prefix'];
		$field = $_GET['field'];

		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$area_table = "crm_public.area";
		}else{
			$area_table = "area";
		}
		$area = M("$area_table");
		$arr = $area->field("CONCAT(province,city) AS areaInfo")->where("$field='$prefix'")->find();
		if($arr){
			echo $arr['areaInfo'];
		}else{
			echo "未知号码";
		}
	}

	//回访记录
	function insertVisit(){
		if($_REQUEST['message_phone'] == "ms"){
			$phone = $_REQUEST['mess_phone'];
		}else{
			$phone = $_REQUEST['phone'];
		}
		$visit_id = $_REQUEST['visit_id'];
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION["user_info"]["d_id"];
		$visit_record = new Model("visit_record");
		//dump($_SESSION);die;
		$arrData = array(
			"createtime"=>date("Y-m-d H:i:s"),
			"customer_id"=>$_REQUEST['id'],
			"createname"=>$username,
			"visit_content"=>$_REQUEST['visit_content'],
			"visit_phone"=>$phone,
			"visit_type"=>$_REQUEST['visit_type'],
			"dept_id"=>$d_id,
			//"accountcode"=>empty($_REQUEST['accountcode'])?"no":$_REQUEST['accountcode'],
			//"accountcode"=>$_REQUEST['record_uniqueid'],
			"accountcode"=>$_SESSION[$username."_visit"][$_REQUEST['id']],
		);
		$result = $visit_record->data($arrData)->add();

		$visit_customer = new Model("visit_customer");
		$res = $visit_customer->where("id = '$visit_id'")->save(array('visit_status'=>'Y'));

		$customer = new Model("customer");
		$resc = $customer->where("id = '".$_REQUEST['id']."'")->save(array('recently_visittime'=>date("Y-m-d H:i:s")));

		if ($result){
			if( $_REQUEST['visit_type'] == "phone" &&  empty($_SESSION[$username."_visit"][$_REQUEST['id']]) ){
				$visit_record->where("id = '$result'")->save(array('label_phone'=>'N'));
				//echo json_encode(array('success'=>true,"msg"=>"您这次回访不是电话回访，通话录音无法关联！"));
				//exit;
			}
			unset($_SESSION[$username."_visit"][$_REQUEST['id']]);
			echo json_encode(array('success'=>true,'msg'=>'回访记录添加成功！'));
		} else {
			echo json_encode(array('msg'=>'回访记录添加失败！'));
		}
	}

	function visitRecordList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Visit Record";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		$this->display();
	}

	function visitRecordData(){
		$username = $_SESSION['user_info']['username'];
		$visit_record = new Model("visit_record");

		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$content = $_REQUEST['content'];
		$visit_name = $_REQUEST['visit_name'];

		$where = "1 ";
		$where .= empty($startime)?"":" AND v.createtime >'$startime'";
        $where .= empty($endtime)?"":" AND v.createtime <'$endtime'";
        $where .= empty($content)?"":" AND v.visit_content like '%$content%'";
		$where .= empty($visit_name)?"":" AND c.name like '%$visit_name%'";

		if($username == 'admin'){
			$count2 = $visit_record->table("visit_record v")->field("v.id,v.createtime,v.customer_id,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->group("v.id")->where($where)->select();
		}else{
			$count2 = $visit_record->table("visit_record v")->field("v.id,v.createtime,v.customer_id,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->group("v.id")->where("$where AND v.createname = '$username'")->select();
		}
		$count = count($count2);

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == 'admin'){
			//$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.id,v.createtime,v.customer_id,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number,cdr.uniqueid,cdr.userfield")->join("customer c on v.customer_id = c.id")->join("asteriskcdrdb.cdr cdr on v.accountcode = cdr.accountcode")->limit($page->firstRow.','.$page->listRows)->group("v.id")->where($where)->select();
			$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.id,v.createtime,v.customer_id,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->group("v.id")->where($where)->select();
		}else{
			$visitData = $visit_record->order("v.createtime desc")->table("visit_record v")->field("v.id,v.createtime,v.customer_id,v.visit_type,v.createname,v.visit_phone,v.visit_content,v.accountcode,c.name,c.phone1,c.phone2,c.email,c.qq_number")->join("customer c on v.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->group("v.id")->where("$where AND v.createname = '$username'")->select();
		}

		//echo $visit_record->getLastSql();
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$para_sys = readS();

		$i = 0;
		$row = array("phone"=>"电话回访","qq"=>"QQ回访");
		foreach($visitData as &$v){
			$v["accountcode"] = trim($v["accountcode"]);
			$arrTmp = explode('.',$v["accountcode"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$v["accountcode"].".WAV";

			if($v['visit_type'] == 'phone' && file_exists($WAVfile) ){
				$visitData[$i]['operations'] .= "<a href='javascript:void(0);' onclick=\"palyRecording("."'".$v["accountcode"]."'".")\" >播放</a>";
			}else{
				$visitData[$i]['operations'] = "";
			}

			$visit_type = $row[$v['visit_type']];
			$v['visit_type'] = $visit_type;

			if($para_sys['hide_rule'] == "role"){
				if($priv["hide_phone"] =="Y"){
					$visitData[$i]["phone1"] = substr($v["phone1"],0,3)."***".substr($v["phone1"],-4);
					$visitData[$i]["phone2"] = substr($v["phone2"],0,3)."***".substr($v["phone2"],-3);
				}
				if($v["qq_number"]){
					if($priv["hide_qq"] =="Y"){
						$visitData[$i]["qq_number"] = "***".substr($v["qq_number"],3);
					}
				}
			}else{
				if($para_sys["hide_phone"] =="yes"){
					$visitData[$i]["phone1"] = substr($v["phone1"],0,3)."***".substr($v["phone1"],-4);
					$visitData[$i]["phone2"] = substr($v["phone2"],0,3)."***".substr($v["phone2"],-3);
				}
				if($v["qq_number"]){
					if($para_sys["hide_qq"] =="yes"){
						$visitData[$i]["qq_number"] = "***".substr($v["qq_number"],3);
					}
				}
			}

			$i++;
		}
		unset($i);
		//dump($visitData);die;

		$rowsList = count($visitData) ? $visitData : false;
		$arrV["total"] = $count;
		$arrV["rows"] = $rowsList;

		echo json_encode($arrV);
	}

	function eidtVisit(){
		checkLogin();
		$id = $_REQUEST['id'];
		$this->assign("id",$id);
		$this->display();
	}

	function deleteVisitRecord(){
		$id = $_REQUEST["id"];
		$visit_record = new Model("visit_record");
		$result = $visit_record->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function visitDataList(){
		$id = $_REQUEST['id'];
		$username = $_SESSION['user_info']['username'];
		$visit_customer = new Model("visit_customer");
		$count = $visit_customer->where("customer_id = '$id' AND visit_status = 'N' AND visit_name='$username'")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$vsData = $visit_customer->order("visit_time asc")->limit($page->firstRow.','.$page->listRows)->where("customer_id = '$id' AND visit_status = 'N' AND visit_name='$username'")->select();

		$rowsList = count($vsData) ? $vsData : false;
		$arrVS["total"] = $count;
		$arrVS["rows"] = $rowsList;

		echo json_encode($arrVS);
	}

	function deleteVisit(){
		$id = $_REQUEST["id"];
		$visit_customer = new Model("visit_customer");
		$result = $visit_customer->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function saveTransFerees(){
		$zjtype = $_REQUEST["zjtype"];
		$dept_id = $_REQUEST["deptuser"];
		//$deptuser2 = $_REQUEST["deptuser2"];
		$department = new Model("department");
		$deptD = $department->where("d_id = '$dept_id'")->find();

		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];

		if($zjtype == 'choose'){
			$zj_name = $_REQUEST["deptuser2"];
			$customer_source = "complete";
			$autocall_visit = "Y";
			$did = $deptId_user[$_REQUEST["deptuser2"]];
		}else{
			$zj_name = $deptD["d_leader"];
			$customer_source = "incall";
			$autocall_visit = "N";
			$did = $dept_id;
		}
		if($zjtype != 'choose'){
			if(!$deptD["d_leader"]){
				echo json_encode(array('msg'=>"该部门没有设置部门领导人!"));
				exit;
			}
		}
		//dump($dept_id);die;
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST["id"];
		$arrId = explode(",",$id);
		$count = count($arrId);
		$customer = new Model("customer");
		$arrData = array(
			"createuser"=>$zj_name,
			"customer_source"=>$customer_source,
			"autocall_visit"=>$autocall_visit,
			"dept_id"=>$did,
			"transferees"=>$username."->".$zj_name,
		);

		foreach($arrId as $v){
			$tmp[$v] = array(
				'customer_id'=>$v,
				'forwardedname'=>$username,
				'recipient'=>$zj_name,
			);
		}

		for($i=0;$i<$count;$i++){
			$result[$i] = $customer->data($arrData)->where("id = $arrId[$i]")->save();
		}

		$sql = "insert into customer_transferees(forwardedname, recipient, customer_id,createtime) values ";
		$value = "";
		foreach( $tmp AS $row ){
			$str = "(";
			$str .= "'" .$row['forwardedname']. "',";
			$str .= "'" .$row['recipient']. "',";
			$str .= "'" .$row['customer_id']. "',";
			$str .= "'" .date("Y-m-d H:i:s"). "'";

			$str .= ")";
			$value .= empty($value)?"$str":",$str";
			$i++;
		}
		if( $value ){
			$sql .= $value;
			$res = $customer->execute($sql);
		}

		//转交时 将回访记录也 一起转交
		$visit_record = new Model("visit_record");
		$arrV = array(
			"createname"=>$zj_name,
		);
		for($i=0;$i<$count;$i++){
			$result[$i] = $visit_record->data($arrV)->where("customer_id = $arrId[$i] AND createname = '$username'")->save();
		}


		if ($result){
			$userArr = readU();
			$cnName = $userArr["cn_user"];
			$cmData = $customer->field("id,name,phone1")->where("id in ($id) ")->select();
			$i = 0;
			foreach($cmData as $val){
				$cmData[$i]['type'] = "AB";
				$cmData[$i]['acceptuser'] = $zj_name;  // 转交给   接收人
				$cmData[$i]['acceptuser_name'] = $cnName[$zj_name];  // 转交给   接收人姓名
				//$cmData[$i]['acceptuser_name'] = json_encode($cnName[$zj_name]);
				$cmData[$i]['giveuser'] = $username; //谁转交的    转交人
				$cmData[$i]['giveuser_name'] = $cnName[$username]; //谁转交的    转交人姓名
				//$cmData[$i]['giveuser_name'] = json_encode($cnName[$username]);
				$i++;
			}
			unset($i);

			/*
			foreach($cmData as $val){
				$msg[] = $val["giveuser"]." (".$val["giveuser_name"].") 将客户 ".$val["name"]."的资料转交给您！";
			}
			$message = implode("<br>",$msg);
			//dump($cmData);die;
			//$messages = json_encode($cmData);
			$messages = json_encode($message);
			*/
			$popData = json_encode($cmData);
			echo json_encode(array('success'=>true,'msg'=>'客户资料转交成功',"popData"=>$popData,"recipient"=>$zj_name));
		} else {
			echo json_encode(array('msg'=>'出现未知错误！'));
		}
	}

	/*
	function saveTransFerees(){
		$dept_id = $_REQUEST["deptuser"];
		$department = new Model("department");
		$deptD = $department->where("d_id = '$dept_id'")->find();
		if(!$deptD["d_leader"]){
			echo json_encode(array('msg'=>"该部门没有设置部门领导人!"));
			exit;
		}
		//dump($dept_id);die;
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST["id"];
		$arrId = explode(",",$id);
		$count = count($arrId);
		$customer = new Model("customer");
		$arrData = array(
			"createuser"=>$deptD["d_leader"],
			"customer_source"=>"incall",
			"autocall_visit"=>"N",
			"dept_id"=>$dept_id,
			"transferees"=>$username."->".$deptD["d_leader"],
		);

		foreach($arrId as $v){
			$tmp[$v] = array(
				'customer_id'=>$v,
				'forwardedname'=>$username,
				'recipient'=>$deptD["d_leader"],
			);
		}

		for($i=0;$i<$count;$i++){
			$result[$i] = $customer->data($arrData)->where("id = $arrId[$i]")->save();
		}

		$sql = "insert into customer_transferees(forwardedname, recipient, customer_id,createtime) values ";
		$value = "";
		foreach( $tmp AS $row ){
			$str = "(";
			$str .= "'" .$row['forwardedname']. "',";
			$str .= "'" .$row['recipient']. "',";
			$str .= "'" .$row['customer_id']. "',";
			$str .= "'" .date("Y-m-d H:i:s"). "'";

			$str .= ")";
			$value .= empty($value)?"$str":",$str";
			$i++;
		}
		if( $value ){
			$sql .= $value;
			$res = $customer->execute($sql);
		}

		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'客户资料转交成功'));
		} else {
			echo json_encode(array('msg'=>'出现未知错误！'));
		}
	}
	*/

	function transFereesList(){
		checkLogin();
		$this->display();
	}

	function transFereesData(){
		$username = $_SESSION['user_info']['username'];
		$optiontype = $_REQUEST['optiontype'];
		$name = $_REQUEST['name'];
		$service_start = $_REQUEST['service_start'];
		$service_end = $_REQUEST['service_end'];
		$forwardedname = $_REQUEST['forwardedname'];
		$recipient = $_REQUEST['recipient'];

		$where = "1 ";
		if($optiontype){
			$where .= empty($forwardedname)?"":" AND t.forwardedname like '%$forwardedname%'";
			$where .= empty($recipient)?"":" AND t.recipient like '%$recipient%'";
		}else{
			$where .= " AND t.forwardedname = '$username' OR t.recipient = '$username'";
		}
		$where .= empty($service_start)?"":" AND t.createtime > '$service_start'";
		$where .= empty($service_end)?"":" AND t.createtime < '$service_end'";
		$where .= empty($name)?"":" AND c.name like '%$name%'";

		//echo $where;die;

		$customer_transferees = new Model("customer_transferees");
		$count = $customer_transferees->table("customer_transferees t")->join("customer c on t.customer_id = c.id")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$transData = $customer_transferees->order("t.createtime desc")->table("customer_transferees t")->field("t.forwardedname, t.recipient, t.customer_id,t.createtime,t.transferType,c.name")->join("customer c on t.customer_id = c.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		//echo $customer_transferees->getLastSql();die;

		$userArr = readU();
		$cnName = $userArr["cn_user"];
		$row = array("in"=>"呼入平台转交","out"=>"外呼平台转交");
		foreach($transData as &$val){
			$transferType = $row[$val["transferType"]];
			$val["transferType"] = $transferType;

			$val['forwardedname_cn_name'] = $cnName[$val["forwardedname"]];
			$val['recipient_cn_name'] = $cnName[$val["recipient"]];
		}


		$rowsList = count($transData) ? $transData : false;
		$arrTrans["total"] = $count;
		$arrTrans["rows"] = $rowsList;

		echo json_encode($arrTrans);
	}


	function visitContentData(){
		$username = $_SESSION['user_info']['username'];
		$vis = new Model("visit_content");
		if($username == 'admin'){
			$count = $vis->count();
		}else{
			$count = $vis->where("createname = '$username'")->count();
		}
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($username == 'admin'){
			$visData = $vis->order("id desc")->limit($page->firstRow.','.$page->listRows)->select();
		}else{
			$visData = $vis->order("id desc")->limit($page->firstRow.','.$page->listRows)->where("createname = '$username'")->select();
		}

		$i = 0;
		foreach($visData as $val){
			$visData[$i]["message"] = "<span>".$val["title"]."</span><br />"."<span style='margin-left:25px;'>".$val["content"]."</span><br />";
			$i++;
		}
		//dump($visData);
		$rowsList = count($visData) ? $visData : false;
		$arrVis["total"] = $count;
		$arrVis["rows"] = $rowsList;

		echo json_encode($arrVis);
	}

	function contentData(){
		$id = $_REQUEST['id'];
		$vis = new Model("visit_content");
		$visD = $vis->where("id = '$id'")->find();
		echo json_encode($visD);
	}



	/*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}


	function recycleList(){
		checkLogin();
		$dept_id = $_SESSION["user_info"]["d_id"];
		$customerFields = getFieldCache();
		$i = 0;
		foreach($customerFields as $val){
			if($val['list_enabled'] == "Y"){
				if($val['en_name'] != "createuser"){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					$arrField[0][$i]['fitColumns'] = true;
				}
				if($val['field_attr'] != 'datetime'){
					if($val['text_type'] == '1'){
						$textTpl[] = $val;
					}
					if($val['text_type'] == '2'){
						$val["field_values"] = json_decode($val["field_values"] ,true);
						$selectTpl[] = $val;
					}
					if($val['text_type'] == '3'){
						$areaTpl[] = $val;
					}
					if($val['text_type'] == '4'){
						$timeTpl[] = $val;
					}
				}
				$i++;
			}
		}
		unset($i);
		$arrFd = array("field"=>"createtime","title"=>"创建时间");
		$arrFd3 = array("field"=>"createuser","title"=>"创建人工号");
		$arrFd2 = array("field"=>"ck","checkbox"=>true);
		array_unshift($arrField[0],$arrFd3);
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd2);
		//dump($customerFields);die;
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);

		$this->assign("textTpl",$textTpl);
		$this->assign("selectTpl",$selectTpl);
		$this->assign("areaTpl",$areaTpl);
		$this->assign("timeTpl",$timeTpl);
		$this->assign("dept_id",$dept_id);

		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}


	//回收站
	function customerRecycleFata(){
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];
		$searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$deptId = $_REQUEST['dept_id'];

		/*
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$dept_id);
		$deptSet = rtrim($deptst,",");

		$deptst2 = $this->getMeAndSubDeptName($arrDep,$deptId);
		$deptSet2 = rtrim($deptst2,",");
		*/

		$where = "(1 ";
		$where .= " AND recycle = 'Y'";
		$where .= empty($startime)?"":" AND createtime >'$startime'";
        $where .= empty($endtime)?"":" AND createtime <'$endtime'";



		/*
		//精确查询可以查所有的客户资料
		if($_REQUEST['searchmethod']){
			if($username != "admin"){
				if( $searchmethod == "equal"){
					$where .= " AND 1)";
				}else{
					$where .= "  AND createuser = '$username') ";
				}
			}else{
				$where .= " AND 1)";
			}
		}else{
			if($username == "admin"){
				$where .= " AND 1)";
			}else{
				$where .= "  AND createuser = '$username')";
			}
		}
		*/
		if($username == "admin"){
			$where .= " AND 1)";
		}else{
			$where .= "  AND createuser = '$username')";
		}


		//备选字段
		$para_sys = readS();
		if($para_sys["hide_field"]=='yes'){
			if(!$_REQUEST['searchmethod']){
				$where .= " AND hide_fields='Y'";
			}else{
				if($searchmethod != "equal"){
					$where .= " AND hide_fields='Y'";
				}
			}
		}

		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'  && $val['text_type'] != '3'){
				//if($val['en_name'] != "createuser"){
				$arrF[] = $val['en_name'];
				$$val['en_name'] = $_REQUEST[$val['en_name']];
				//}
			}
		}
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3'){
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
				}
			}
		}



		//dump($where);die;


		//匹配上次查询条件--start
		$search_last_sql = new Model("search_last_sql");
		if($where != "1 "){
			$search_res = $search_last_sql->where("table_name = 'customer'")->save( array('last_sql'=>$where) );
		}
		$search_data = $search_last_sql->where("table_name = 'customer'")->find();
		if($_REQUEST['search'] == "lastTime"){
			$where = $search_data['last_sql'];
		}else{
			$where = $where;
		}
		//匹配上次查询条件--end



		$customer = new Model("customer");
		import('ORG.Util.Page');


		if($username == "admin"){
			$count = $customer->where("$where  AND customer_source = 'complete' ")->count();
		}else{
			$count = $customer->where("$where AND customer_source = 'complete'")->count();
		}

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);


		$fields = "`id`,`createtime`,"."`".implode("`,`",$arrF)."`";
		if($username == "admin"){
			$cmlist = $customer->order("createtime desc")->field($fields)->limit($page->firstRow.','.$page->listRows)->where("$where ANDcustomer_source = 'complete'")->select();
		}else{
			$cmlist = $customer->order("createtime desc")->field($fields)->limit($page->firstRow.','.$page->listRows)->where("$where AND customer_source = 'complete'")->select();
		}
		//dump($customer->getLastSql());die;
		//dump($fields);die;
		$row = getSelectCache();
		//dump($row);die;
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($cmlist as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}

		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		//dump($priv);die;
		$para_sys = readS();
		$i = 0;
		foreach($cmlist as &$vm){
			$cmlist[$i]["massPhone"] = $vm['phone1'];
			$cmlist[$i]["massEmail"] = $vm['email'];
			if($para_sys['hide_rule'] == "role"){
				if($priv["hide_phone"] =="Y"){
					$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
				}
				if($vm['email']){
					if($priv["hide_email"] =="Y"){
						$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
					}
				}
			}else{
				if($para_sys["hide_phone"] =="yes"){
					$cmlist[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
				}
				if($vm['email']){
					if($para_sys["hide_email"] =="yes"){
						$cmlist[$i]["email"] = "***".strstr($vm["email"],"@");
					}
				}
			}

			$i++;
		}
		unset($i);


		//dump($cmlist);die;
		//dump($customer->getLastSql());die;
		$rowsList = count($cmlist) ? $cmlist : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
		//$this->assign("cmlist",$cmlist);
	}


	//下一条
	function nextCustomer(){
		$customer = new Model("customer");
		$sears = base64_decode($_GET["sears"]);
		//dump($sears);die;
		$id = $_GET["id"];
		$cmData = $customer->query($sears);

		foreach($cmData as $val){
			$cmId[] = $val["id"];
		}
		$srId = array_flip($cmId);
		$afterId = $cmId[$srId[$id]+1];
		$arr = $customer->field("id,phone1,name")->where("id = '$afterId'")->find();
		$title = $arr["name"];

		echo json_encode(array('id'=>$afterId,'title'=>$title));
	}

	function lastCustomer(){
		$customer = new Model("customer");
		$sears = base64_decode($_GET["sears"]);
		$id = $_GET["id"];
		$cmData = $customer->query($sears);

		foreach($cmData as $val){
			$cmId[] = $val["id"];
		}

		$srId = array_flip($cmId);
		$afterId = $cmId[$srId[$id]-1];
		$arr = $customer->field("id,phone1,name")->where("id = '$afterId'")->find();
		$title = $arr["name"];

		echo json_encode(array('id'=>$afterId,'title'=>$title));
	}
}

?>
