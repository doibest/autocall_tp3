<?php
// 本类由系统自动生成，仅供测试用途
class TelemarketingAction extends Action {

	//获取用户的任务列表
	 public function index(){
		checkLogin();
		$para_sys = readS();
		$page_rows = $para_sys["page_rows"];

		$task = M("sales_task");
		$user_info = session("user_info");
		$list = $task->where("dept_id='". $user_info["d_id"]  ."' and enable='Y'")->select();

		$customerFields = getFieldCache();

		foreach($list as $key => $val){

			$val["displayfield"] =json_decode( $val["displayfield"] ,true);
			/*
			$i = 0;
			foreach( $val["displayfield"] as $v ){

				if( $v[1] == "view" or $v[1] == "edit" ){
					$arrF[$i]["field"] = $v[2];
					$arrF[$i]["title"] = $v[0];
					$arrF[$i]["align"] = 'center';
					$i++;
				}
			}
			*/
			$i = 0;
			foreach($customerFields as $v){
				if($v['list_enabled'] == "Y" && $v['en_name'] != "createuser"){
					$arrF[$i]['field'] = $v['en_name'];
					$arrF[$i]['title'] = $v['cn_name'];
					$arrF[$i]['fitColumns'] = true;
					$i++;
				}
			}
			unset($i);
			$val["columns"] =  '['.json_encode($arrF).']';
			unset($arrF);
			//$val["columns"] = "[[{field:'test',title:'bbbaaa'},{field:'test12',title:'bbbaaa1'},{field:'test3',title:'bbbaaa2'}]]";
			$arrK["id"] = $val['id'];
			$arrK["data"] = json_encode($val);

			$arrT[] = $arrK;

		}

		$user_info = json_encode($_SESSION["user_info"]);
		$first_task_id = $list[0]["id"];
		$this->assign("first_task_id",$first_task_id);
		$this->assign("task",$list);
		$this->assign("task_json",$arrT);
		$this->assign("userinfo",$user_info);
		$this->assign("page_rows",$page_rows);

		$callres = new Model("sales_callresult");
		$callresult_data = $callres->select();

		$callresult_data = json_encode($callresult_data);
		$this->assign("callresult_data",$callresult_data);
		$this->assign("extension",$_SESSION['user_info']['extension']);
		$this->display();

	}

	//读取当前任务列表
	function getphoneList(){
		$name = $_REQUEST["name"];
		$company = $_REQUEST["company"];
		$phone = $_REQUEST["phone"];
		$phone2 = $_REQUEST["phone2"];
		$qq = $_REQUEST["qq"];
		$sex = $_REQUEST["sex"];

		//$where = " 1 ";
		$where = empty($name) ? "" : " AND name like '%$name%'";
		$where .= empty($company) ? "" : " AND company like '%$company%'";
		$where .= empty($phone) ? "" : " AND phone1 like '%$phone%'";
		$where .= empty($phone2) ? "" : " AND phone2 like '%$phone2%'";
		$where .= empty($qq) ? "" : " AND qq like '%$qq%'";
		$where .= empty($sex) ? "" : " AND sex like '%$sex%'";

		import('ORG.Util.Page');
		//获取任务信息
		$task = M("sales_task");
		$customerFields = getFieldCache();
		//第一次点项目任务或切换任务时设置session的任务id
		if( $this->_get("task_id") ){
			$task_id = $this->_get("task_id");
			$task_info = $task->where("id='". $task_id ."'")->find();
			//表字段显示
			$task_info["displayfield"] =json_decode( $task_info["displayfield"] ,true);
			//dump($task_info["displayfield"]);
			/*
			$i = 0;
			foreach( $task_info["displayfield"] as $val ){

				if( $val[1] == "view" or $val[1] == "edit" ){
					$sqlfields .= 	$sqlfields ? ",".$val[2] : 'id,'.$val[2]	;
					$arrF[$i]["field"] = $val[2];
					$arrF[$i]["title"] = $val[0];
					$arrF[$i]["fitColumns"] = true;
					$i++;
				}
			}
			$arrF[] = array("field"=>"id","title"=>"ID");
			*/

			$i = 0;
			foreach($customerFields as $val){
				if($val['list_enabled'] == "Y" && $val['en_name'] != "createuser"){
					$arrF[$i]['field'] = $val['en_name'];
					$arrF[$i]['title'] = $val['cn_name'];
					$arrF[$i]['fitColumns'] = true;
					$field[] = $val["en_name"];
					$i++;
				}
			}
			unset($i);
			$arrFd = array("field"=>"id","title"=>"ID");
			array_unshift($arrF[0],$arrFd);
			$sqlfields = "id,".implode(",",$field);

			//dump($arrF);
			unset($task_info["displayfield"]);
			$task_info["displayfield"] = $arrF;
			$task_info["sqlfields"] = $sqlfields;
			//dump($task_info);
			session("outbound_current_task",$task_info);

		}else{
			$task_info = session("outbound_current_task");
			$task_id = $task_info["id"];
		}
		//存当前任务的session
		$user_info = session("user_info");
		$source = M("sales_source_".$task_id);

		$count = $source->where("dealuser='". $user_info["username"] ."' and locked='Y' and dealresult_id=0 $where")->count();
		//$ary["sql"] = $source->getLastsql();

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$list = $source->field($task_info["sqlfields"])->where("dealuser='". $user_info["username"] ."' and locked='Y' and dealresult_id=0 $where")->limit($page->firstRow.','.$page->listRows)->order('id desc')->select();
		$ary["sql"] = $source->getLastsql();
		//echo $ary["sql"];die;

		$cmFields = getFieldCache();
		$row = getSelectCache();
		//dump($row);die;
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($list as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;
				}
			}
		}


		$list = count($list) > 0 ? $list : false;
		$ary["total"] = $count;
		$ary["rows"] = $list;
		$ary["columns"] = '['.json_encode($task_info["displayfield"]).']';
		unset($task_info["displayfield"]);
		$ary["task_info"] = $task_info;
		//dump($ary);die;
		echo json_encode($ary);

	}

	//读取当前失败单列表
	function getList(){
		import('ORG.Util.Page');
		//获取任务信息
		$user_info = session("user_info");
		$dealresult_id = $_GET["dealresult_id"];
		$task = M("sales_task");
		$task_info = session("outbound_current_task");
		$task_id = $task_info["id"];
		$source = M("sales_source_".$task_id);

		$name = $_REQUEST["name"];
		$company = $_REQUEST["company"];
		$phone = $_REQUEST["phone"];
		$phone2 = $_REQUEST["phone2"];
		$qq = $_REQUEST["qq"];
		$sex = $_REQUEST["sex"];

		//$where = " 1 ";
		$where = empty($name) ? "" : " AND name like '%$name%'";
		$where .= empty($company) ? "" : " AND company like '%$company%'";
		$where .= empty($phone) ? "" : " AND phone1 like '%$phone%'";
		$where .= empty($phone2) ? "" : " AND phone2 like '%$phone2%'";
		$where .= empty($qq) ? "" : " AND qq like '%$qq%'";
		$where .= empty($sex) ? "" : " AND sex like '%$sex%'";
		/*
		if($this->_get("phone")){
			$where .= " AND (phone1 like '%". $this->_get("phone") ."%' or phone2 like '%". $this->_get("phone") ."%'
						or name like '%". $this->_get("phone") ."%' or sex like '%". $this->_get("phone") ."%'
						or company like '%". $this->_get("phone") ."%' or city like '%". $this->_get("phone") ."%' )";
		}
		*/
		$count = $source->where("dealuser='". $user_info["username"] ."' and locked='Y' and dealresult_id=$dealresult_id $where")->count();
		//$ary["sql"] = $source->getLastsql();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);
		$list = $source->field($task_info["sqlfields"])->where("dealuser='". $user_info["username"] ."' and locked='Y' and dealresult_id='" . $dealresult_id . "' $where")->limit($page->firstRow.','.$page->listRows)->order('id desc')->select();
		$ary["sql"] = $source->getLastsql();
		//echo $ary["sql"];
		$list = count($list) > 0 ? $list : false;
		$ary["total"] = $count;
		$ary["rows"] = $list;
		$ary["columns"] = '['.json_encode($task_info["displayfield"]).']';
		unset($task_info["displayfield"]);
		$ary["task_info"] = $task_info;
		echo json_encode($ary);
	}




	function callResult(){
		$callres = new Model("sales_callresult");
		$callresult_data = $callres->field("id,name as text")->select();
		//dump($callresult_data);die;
		echo json_encode($callresult_data);
	}



	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	/*
	function addWCustomer(){
		$username = $_SESSION['user_info']['username'];
		$taskU = require 'BGCC/Conf/task_user.php';
		$task_id = $taskU[$username];
		$id = $_REQUEST['id'];
		$subtitle = $_REQUEST['subtitle'];
		$calltype = $_REQUEST['calltype'];
		$datagrid_id = $_REQUEST['datagrid_id'];
		$uniqueid = $_REQUEST['uniqueid'];
		$phone_num = $_REQUEST['phone_num'];

		//$source = new Model("sales_source_".$task_id);
		//$sourceData =  $source->where("id = '$id'")->find();
		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');
		$para_sys = readS();

		foreach($fielddTpl2 as $val){
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields" && $val["en_name"] != "sms_cust"){
					$fielddTpl[] = $val;
				}
			}else{
				if($val['field_enabled'] == 'Y'){
					$fielddTpl[] = $val;
				}
			}
		}
		//dump($cmFields);die;
		$this->assign("fielddTpl",$fielddTpl);
		$this->assign("task_id",$task_id);
		$this->assign("id",$id);
		//$this->assign("custList",$sourceData);
		$this->assign("subtitle",$subtitle);
		$this->assign("calltype",$calltype);
		$this->assign("datagrid_id",$datagrid_id);
		$this->assign("uniqueid",$uniqueid);
		$this->assign("phone_num",$phone_num);
		$this->display();
	}



	//添加客户资料
	function insertCustomer(){
		$task_id = $_REQUEST['task_id'];
		//dump($_REQUEST);die;
		$source = new Model("sales_source_".$task_id);


		$phcount = $source->where("phone1='". $_POST["phone1"] ."'")->count();
		if( $phcount>0 ){
				echo json_encode(array('msg'=>"这个手机号已存在！"));
				exit;
		}

		if($_POST["qq_number"]){
			$qqcount = $source->where("qq_number='". $_POST["qq_number"] ."'")->count();
			if( $qqcount>0 ){
					echo json_encode(array('msg'=>"这个QQ号已存在！"));
					exit;
			}
		}

		//$dept_id = $_SESSION["user_info"]["d_id"];
		$customer = new Model("customer");
		$username = $_SESSION["user_info"]["username"];
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields" && $val["en_name"] != "sms_cust"){
				$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
			}
		}

		if($_REQUEST['hide_fields']){
			$hide_fields = $_REQUEST['hide_fields'];
		}else{
			$hide_fields = "Y";
		}

		$arrData["createtime"] = date("Y-m-d H:i:s");
		$arrData["dealuser"] = $username;
		$arrData["calledflag"] = "3";
		$arrData["hide_fields"] = $hide_fields;
		//$arrData["country"] = $_REQUEST['country'];
		//$arrData["province"] = $_REQUEST['province'];
		//$arrData["city"] = $_REQUEST['city'];
		//$arrData["district"] = $_REQUEST['district'];
		//$arrData["dept_id"] = $dept_id;
		$result = $source->data($arrData)->add();
		//echo $source->getLastSql();
		if($result){
			echo json_encode(array('success'=>true,'msg'=>'客户资料添加成功!','lastid'=>$result));
		}else{
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}
	*/

	function editWCustomer(){
		checkLogin();
		$task_id = $_REQUEST['task_id'];
		$id = $_REQUEST['id'];
		$subtitle = $_REQUEST['subtitle'];
		$calltype = $_REQUEST['calltype'];
		$datagrid_id = $_REQUEST['datagrid_id'];
		$uniqueid = $_REQUEST['uniqueid'];
		$visit_id = $_REQUEST['visit_id'];
		$visitid = $_REQUEST['visitid'];
		$phone_num = $_REQUEST['phone_num'];
		$resultid = $_REQUEST['resultid'];
		$extension = $_SESSION['user_info']['extension'];

		$para_sys = readS();
		$sears = base64_decode($_GET["sears"]);
		$allSql = array_shift(explode("LIMIT",$sears));
		$searsql = base64_encode($allSql);
		if($sears){
			$this->assign("sears",$searsql);
			$this->assign("next_last","Y");
		}else{
			$this->assign("next_last","N");
		}

		if($para_sys["popNextCustomer"] == "Y"){
			$this->assign("resultid","Y");
		}else{
			if($resultid == '0'){
				$this->assign("resultid","Y");
			}else{
				$this->assign("resultid","N");
			}
		}

		$sales_task = new Model("sales_task");
		$arrTask = $sales_task->where("id = '$task_id'")->find();


		$source = new Model("sales_source_".$task_id);

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($para_sys['hide_field'] == 'yes'){
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "hide_fields" && $val["en_name"] != "sms_cust"){
					$fielddTpl[] = $val;
				}
			}else{
				if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
					$fielddTpl[] = $val;
				}
			}
		}
		//$fields = "`".implode("`,`",$arrF)."`".",`country`,`province`,`city`,`district`";
		$fields = "id,dealuser,createtime,callbacktime,calledflag,billsec,dealresult_id,openid,".implode(",",$arrF);
		//$fields = "`".implode("`,`",$arrF)."`";
		$this->assign("fielddTpl",$fielddTpl);
		//dump($fielddTpl);die;
		$sourceData =  $source->field($fields)->where("id = '$id'")->find();
		$sourceData["name"] = trim($sourceData["name"]);
		$sourceData["phone1"] = trim($sourceData["phone1"]);
		//所属客服
		$userArr = readU();
		$exten_user = $userArr["exten_user"];
		//dump($userArr);die;
		$sourceData["belongService"] = $sourceData["dealuser"]."/".$exten_user[$sourceData["dealuser"]];
		//echo $source->getLastSql();die;


		if($para_sys["editOutboundCustomer_hide"] =="phone" || $para_sys["editOutboundCustomer_hide"] == "all" ){
			$sourceData["phone"] = $sourceData["phone1"];
			$sourceData["telephone"] = $sourceData["phone2"];
			$sourceData["phone1"] = substr($sourceData["phone1"],0,3)."***".substr($sourceData["phone1"],-4);
			if($sourceData["phone2"]){
				$sourceData["phone2"] = substr($sourceData["phone2"],0,3)."***".substr($sourceData["phone2"],-3);
			}
			$this->assign("message_phone","ms");
			$this->assign("message_telephone","ms");
		}else{
			$sourceData["phone"] = $sourceData["phone1"];
			$sourceData["telephone"] = $sourceData["phone2"];
			$this->assign("message_phone","show_phone");
			$this->assign("message_telephone","show_phone");
		}
		if($sourceData["email"]){
			if($para_sys["editOutboundCustomer_hide"] =="email" || $para_sys["editOutboundCustomer_hide"] == "all" ){
				$sourceData["email2"] = $sourceData["email"];
				$sourceData["email"] = "***".strstr($sourceData["email"],"@");
				$this->assign("message_email","ms");
			}else{
				$sourceData["email2"] = $sourceData["email"];
				$this->assign("message_email","show_email");
			}
		}
		if($sourceData['qq_number']){
			$sourceData["qq_number2"] = $sourceData["qq_number"];
			if( $para_sys["editOutboundCustomer_hide"] =="qq" || $para_sys["editOutboundCustomer_hide"] == "all" ){
				$sourceData["qq_number"] = "***".substr($sourceData["qq_number"],3);
				$this->assign("message_qq","ms");
			}else{
				$this->assign("message_qq","show_qq");
			}
		}
		//dump($sourceData);die;

		if($visit_id || $visitid){
			$this->assign("visitid","Y");
		}else{
			$this->assign("visitid","N");
		}

		$this->assign("arrTask",$arrTask);
		$this->assign("task_id",$task_id);
		$this->assign("id",$id);
		$this->assign("custList",$sourceData);
		$this->assign("subtitle",$subtitle);
		$this->assign("calltype",$calltype);
		$this->assign("datagrid_id",$datagrid_id);
		$this->assign("uniqueid",$uniqueid);
		$this->assign("phone_num",$phone_num);
		$this->assign("visit_id",$visit_id);
		$this->assign("exten",$extension);
		$this->assign("outBound_close",$para_sys["outBound_close"]);
		$this->assign("outBoundResult_rand",$para_sys["outBoundResult_rand"]);

		$callres = new Model("sales_callresult");
		$callresult_data = $callres->field("id,name as text")->select();
		$this->assign("callresult",$callresult_data);

		$role = $_REQUEST['role'];
		$this->assign("role",$role);
		$username = $_SESSION["user_info"]["username"];

		$arrAdmin = getAdministratorNum();
		if( in_array($username,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$username);
		}


		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("wj",$arrAL) ){
			$wj_display = "Y";
		}else{
			$wj_display = "N";
		}
		$this->assign("wj_display",$wj_display);
		//dump($wj_disply);die;

		$this->display();
	}


	//编辑客户资料
	function updateCustomer(){
		$username = $_SESSION["user_info"]["username"];
		//dump($id);
		$task_id = $_REQUEST['task_id'];
		$id = $_REQUEST['id'];

		$source = new Model("sales_source_".$task_id);

		//$customer = new Model("customer");
		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y' && $val["en_name"] != "sms_cust"){
				if($val['en_name'] == 'phone1'){
					if($_REQUEST['message_phone'] == "ms"){
						$arrData['phone1'] = $_REQUEST['mess_phone'];
					}else{
						$arrData['phone1'] = $_REQUEST['phone1'];
					}
				}elseif($val['en_name'] == 'email'){
					if($_REQUEST['message_email'] == "ms"){
						$arrData['email'] = $_REQUEST['mess_email'];
					}else{
						$arrData['email'] = $_REQUEST['email'];
					}
				}elseif($val['en_name'] == 'phone2'){
					if($_REQUEST['message_telephone'] == "ms"){
						$arrData['phone2'] = $_REQUEST['mess_telephone'];
					}else{
						$arrData['phone2'] = $_REQUEST['phone2'];
					}
				}elseif($val['en_name'] == 'qq_number'){
					if($_REQUEST['message_qq'] == "ms"){
						$arrData['qq_number'] = $_REQUEST['mess_qq'];
					}else{
						$arrData['qq_number'] = $_REQUEST['qq_number'];
					}
				}else{
					$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
				}
			}
		}
		$arrData["modifytime"] = date("Y-m-d H:i:s");
		$openid = empty($_REQUEST["nickname"]) ? "" : $_REQUEST["openid"];
		$arrData["openid"] = $openid;

		//dump($cmFields);die;
		//dump($arrData);die;
		$result = $source->data($arrData)->where("id = $id")->save();
		//echo $source->getLastSql();die;

		$visit_customer = new Model("visit_customer");
		//改变回访状态
		if($_REQUEST['visit_id']){
			$arrVs = array(
				"visit_status"=>"Y",
			);
			$visit_customer->data($arrVs)->where("id =".$_REQUEST['visit_id'])->save();
		}

		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'客户资料修改成功!','editid'=>$id));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}

	function historyData(){
		$task_id = $_REQUEST['task_id'];
		$source_id = $_REQUEST['id'];
		//$uniqueid = $_REQUEST['uniqueid'];//wjj--edit

		$contact = new Model("sales_contact_history_".$task_id);
		$count = $contact->where("source_id=".$source_id)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$list = $contact->limit($page->firstRow.','.$page->listRows)->where("source_id=".$source_id)->order("id desc")->select();
		//echo $contact->getLastSql();die;
		//dump($list);die;
		$calltype_arr = array("preview"=>"预览外呼","autoid"=>"自动外呼");
		$dealresult_arr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");

		$callres = new Model("sales_callresult");
		$callresult_data = $callres->select();
		foreach( $callresult_data as $val ){
			$callresult_arr[$val["id"]] = $val["name"];
		}

		//$callresult_arr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$i = 0;
		foreach($list as &$v){

			$list[$i]["calltype"] = $calltype_arr[$v["calltype"]];
			$list[$i]["dealresult"] = $dealresult_arr[$v["dealresult"]];
			$list[$i]["callresult"] = $callresult_arr[$v["callresult"]];
			//$list[$i]['operations'] = "<a target='_blank' href='agent.php?m=Telemarketing&a=playTaskCDR&uniqueid=" .$v['uniqueid'] ."'> 播放 </a>  ";

			$arrTmp = explode('.',$v["uniqueid"]);
			$timestamp = $arrTmp[0];
			$dirPath = '/var/spool/asterisk/monitor/' . date('Y-m',$timestamp) .'/'. date('d',$timestamp);
			$WAVfile = $dirPath ."/".$v["uniqueid"].".WAV";

			if(file_exists($WAVfile)){
				$list[$i]['operations'] = "<a   href='javascript:void(0);' onclick=\"palyRecording(" ."'".trim($v["uniqueid"])."'".")\" > 播放 </a>  ";
			}else{
				$list[$i]['operations'] = "";
			}
			$i++;
		}
		//dump($list);die;
		$rowsList = count($list) ? $list : false;
		$arrHistory["total"] = $count;
		$arrHistory["rows"] = $rowsList;

		echo json_encode($arrHistory);
	}

	//质检是用的
	function allHistoryData(){
		$task_id = $_REQUEST['task_id'];
		$dealuser = $_REQUEST['workno'];
		$uniqueid = $_REQUEST['uniqueid'];

		$contact = new Model("sales_contact_history_".$task_id);
		$count = $contact->where("dealuser='$dealuser'")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$list = $contact->limit($page->firstRow.','.$page->listRows)->where("dealuser='$dealuser'")->order("id desc")->select();
		//echo $contact->getLastSql();die;
		//dump($list);die;
		$calltype_arr = array("preview"=>"预览外呼","autocall"=>"自动外呼");
		$dealresult_arr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");

		$callres = new Model("sales_callresult");
		$callresult_data = $callres->select();
		foreach( $callresult_data as $val ){
			$callresult_arr[$val["id"]] = $val["name"];
		}

		//$callresult_arr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$i = 0;
		foreach($list as $v){

			$list[$i]["calltype"] = $calltype_arr[$v["calltype"]];
			$list[$i]["dealresult"] = $dealresult_arr[$v["dealresult"]];
			$list[$i]["callresult"] = $callresult_arr[$v["callresult"]];
			$list[$i]['operations'] = "<a target='_blank' href='agent.php?m=Telemarketing&a=playTaskCDR&uniqueid=" .$v['uniqueid'] ."'> 播放 </a>  ";
			$i++;
		}
		//dump($list);die;
		$rowsList = count($list) ? $list : false;
		$arrHistory["total"] = $count;
		$arrHistory["rows"] = $rowsList;

		echo json_encode($arrHistory);
	}





	/****************
		获取号码

	****************/
	function lockPhone(){

		$user_info = session("user_info");
		$task_info = session("outbound_current_task");
		$task_id = $task_info["id"];

		$source = M("sales_source_".$task_id);
		$data = array("locked"=>"Y","dealuser"=>$user_info["username"]);
		$count = $source->where("locked='N' and dealresult_id=0")->order('id asc')->limit('20')->save($data);
		echo $count;
	}


	 /**
     +----------------------------------------------------------
     * 更新号码库中的常规字段字段 成功返回ok 不成功返回错误
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------

     */
	function fieldUpdateData(){
		$fields = explode('_',$this->_post("name"));
		$ary = array($fields[0]=>$this->_post("value"));
		$source = M("sales_source_".$fields[1]);
		$count = $source->where("id=".$fields[2])->save($ary);
	}

	 /**
     +----------------------------------------------------------
     * 更新号码库中的城市字段
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------
     */
	function fieldUpdateCityData(){
		$fields = explode('_',$this->_post("name"));
		$value = $_POST["value"];
		$city = $value["province"]."-".$value["city"];
		$ary = array($fields[0]=>$city);
		$source = M("sales_source_".$fields[1]);
		$count = $source->where("id=".$fields[2])->save($ary);

		echo $source->getLastsql();
	}

	function callpage(){
		checkLogin();
		$this->display();
	}

	function saveCallResult(){
		//dump($_REQUEST);die;
		//保存后示闲
		$task_id = $_POST["task_id"];
		$sales_task = new Model("sales_task");
		$taskD = $sales_task->where("id=$task_id")->find();
		$para_sys = readS();


		$username = $_SESSION['user_info']['username'];
		$trans_type = $_REQUEST['trans_type'];
		//$deptmID = require 'BGCC/Conf/dept_map.php';
		$deptId = $_SESSION['user_info']['d_id'];
		if($trans_type){
			if($trans_type == "default"){
				$d_id = $deptId;
			}else{
				$d_id = $_REQUEST['dept_id'];
			}
		}else{
			$d_id = $deptId;
		}
		//dump($d_id);die;
		$user_info = session("user_info");
		$task_info = session("outbound_current_task");
		//$task_id = $task_info["id"];

		//$dealresult_id = $_POST["dealresult_id"];
		$dealresult_id = $_POST["dealresult"];
		$callbacktime = $_POST["callbacktime"];
		$calltype = $_POST["calltype"];
		$dialphone = $_POST["phone_num"];
		$callresult_id = $_POST["callresult"];
		$id = $_POST["id"];
		/*
		if($_POST["uniqueid"] != "no"){
			$uniqueid = $_POST["uniqueid"];
		}else{
			$uniqueid = $_SESSION[$username."_task".$task_id."_history"][$id];
		}
		*/
		$uniqueid = $_SESSION[$username."_task".$task_id."_history"][$id];
		$description = $_POST["description"];

		$dialname = $_POST["dialname"];

		//dump($uniqueid);
		//dump($_SESSION);die;
		//dump($user_info);
		//dump($_POST);die;
		$arr = array("callresult_id"=>$callresult_id,
					  "dealresult_id"=>$dealresult_id,
					  "callbacktime"=>$callbacktime,
					  'locked'=>'Y',
					  "modifytime"=>date("Y-m-d H:i:s"),
					  //"dealuser"=>$user_info["username"],
					  );
		$role = $_REQUEST["role"];
		if($role != "admin"){
			$arr["dealuser"] = $user_info["username"];
		}


		$source = M("sales_source_".$task_id);

		$arrTmp = $source->field("id,phone1,callresult_id,dealresult_id")->where("id=".$id)->find();

		$count = $source->where("id=".$id)->save($arr);
			unset($arr);


		//echo $source->getLastsql();

		//if($count>0){
		if($count !== false){

			/*
			//转待办事项
			if($dealresult_id == '1'){
				$waittitle = "[外呼回访] [".$dialname."]";
				$waitmatter = new Model("waitmatter");
				$deptuser = $_SESSION["user_info"]["username"];
				$arrWaitData = array(
					"createtime"=>date("Y-m-d H:i:s"),
					"title"=>$waittitle,
					"remindertime"=>$callbacktime,
					"content"=>$description,
					"status"=>"N",
					"matterstype"=>"3",
					"username"=>$deptuser,
				);
				//dump($arrWaitData);
				$waitResult = $waitmatter->data($arrWaitData)->add();
			}
			*/

			//回访
			//添加回访
			$visit_customer = new Model("visit_customer");
			if($dealresult_id == '1'){
				$arrV = array(
					//"visit_time"=>date("Y-m-d H:i:s"),
					"visit_time"=>$_REQUEST['callbacktime'],
					"visit_status"=>"N",
					"visit_type"=>"out",
					"visit_name"=>$username,
					"task_id"=>$task_id,
					"customer_id"=>$id,
					"phone"=>$arrTmp["phone1"],
				);
				$resV = $visit_customer->data($arrV)->add();

			}
			//改变回访状态
			if($_REQUEST['visit_id']){
				$arrVs = array(
					"visit_status"=>"Y",
				);
				$visit_customer->data($arrVs)->where("id =".$_REQUEST['visit_id'])->save();
			}

			//成功单转交给自己的呼入平台客户资料
			$checkRole = getSysinfo();
			$arrAL = explode(",",$checkRole[2]);
			if( in_array("inbound",$arrAL) ){
				$wj_inbound = "Y";
			}else{
				$wj_inbound = "N";
			}

			if($dealresult_id == '3' && $para_sys["success_transfer"] == "Y" && $wj_inbound == "Y"){  //成功单
				$cmFields = getFieldCache();
				foreach($cmFields as $val){
					if($val['list_enabled'] == 'Y'){
						$arrF[] = $val['en_name'];
					}
				}
				$fields = "id,dealuser,".implode(",",$arrF);
				$source = new Model("sales_source_".$task_id);
				$arrData = $source->field($fields)->where("id = '$id' AND (brand != 'Y' OR brand IS NULL)")->find();

				$customer = new Model("customer");
				$arrD = $customer->where("phone1 = '".$arrData["phone1"]."'")->find();
				if(count($arrD)>0){

				}else{
					if($arrData){
						$sid = array_shift($arrData);
						$sdealuser = array_shift($arrData);
						$arrData["createuser"] = $username;
						$arrData["dept_id"] = $deptId;
						$arrData["createtime"] = date("Y-m-d H:i:s");
						//$customer = new Model("customer");
						$res = $customer->data($arrData)->add();

						if($res){
							$source->where("id in ($id)")->save(array("brand"=>"Y"));
							$transfer = new Model("customer_transferees");
							$arrData = array(
								"customer_id" => $res,
								"createtime" => date("Y-m-d H:i:s"),
								"transferType" => "out",
								"forwardedname" => $username, //转交人
								"recipient" => $username,  //接收人
								"task_id" => $task_id,
								"outBound_customer_id" => $id,
							);
							$transfer->data($arrData)->add();
						}
					}
				}
			}

			$arrTransferInfo = Array(); //wjj--add 转交人
			/*
			if($dealresult_id == '3'){  //成功单
				$department = new Model("department");
				$deptD = $department->where("d_id = '$d_id'")->find();
				if(!$deptD["d_leader"]){
					echo json_encode(array('msg'=>"该部门没有设置部门领导人!"));
					exit;
				}
				$arrData = $source->where("id=".$id)->find();
				foreach($arrData as $k=>$v){
					if($k != 'id' && $k != 'callresult_id' && $k != 'dealresult_id' && $k != 'callbacktime' && $k != 'locked'  && $k != 'calledflag' ){
						$arrF[$k] = $v;
					}
				}
				$arrF['createuser'] = $deptD['d_leader'];
				$arrF['dept_id'] = $d_id;
				$arrF['customer_source'] = 'autocall';
				$arrF['visit_abled'] = 'Y';
				$arrF['transferees'] = $username."->".$deptD['d_leader'];
				$customer = new Model("customer");
				$res = $customer->data($arrF)->add();
				if($res){
					$userArr = readU();
					$cnName = $userArr["cn_user"];
					$arrTransferInfo['type'] = "AB"; //wjj--add 主动转交AB，当然还有客服拒绝回退BA
					$arrTransferInfo['acceptuser'] = $arrF['createuser']; //wjj--add 转交给
					$arrTransferInfo['acceptuser_name'] = $cnName[$arrF['createuser']];  // 转交给   接收人姓名
					$arrTransferInfo['giveuser'] = $username; //wjj--add 谁转交的
					$arrTransferInfo['giveuser_name'] = $cnName[$username]; //谁转交的    转交人姓名
					$arrTransferInfo['name'] = $arrData['name']; //wjj--add 转交的客户信息
					$arrTransferInfo['phone1'] = $arrData['phone1']; //wjj--add 转交的客户信息
				}
				//echo $customer->getLastSql();
				//dump($arrF);die;
			}
			*/
			$outbound = new Model("outbound_statistics");
			$date_out = date("Y-m-d");
			$bound_count = $outbound->where("createname = '$username' AND createtime = '$date_out' ")->count();
			if($bound_count>0){
				$arrB = $outbound->where("createname = '$username'  AND createtime = '$date_out' ")->find();
				if($dealresult_id == '0'){
					$arrD = array(
						//"createtime"=>date("Y-m-d"),
						"dept_id"=>$deptId,
						"untreated_num"=>$arrB["untreated_num"]+1,
					);
				}
				if($dealresult_id == '1'){
					$arrD = array(
						//"createtime"=>date("Y-m-d"),
						"dept_id"=>$deptId,
						"visit_num"=>$arrB["visit_num"]+1,
					);
				}
				if($dealresult_id == '2'){
					$arrD = array(
						//"createtime"=>date("Y-m-d"),
						"dept_id"=>$deptId,
						"fail_num"=>$arrB["fail_num"]+1,
					);
				}
				if($dealresult_id == '3'){
					$arrD = array(
						//"createtime"=>date("Y-m-d"),
						"dept_id"=>$deptId,
						"success_num"=>$arrB["success_num"]+1,
					);
				}
				$resB = $outbound->data($arrD)->where("createname = '$username'  AND createtime = '$date_out' ")->save();
			}else{
				if($dealresult_id == '0'){
					$arrD = array(
						"createtime"=>date("Y-m-d"),
						"createname"=>$username,
						"dept_id"=>$deptId,
						"untreated_num"=>"1",
					);
				}
				if($dealresult_id == '1'){
					$arrD = array(
						"createtime"=>date("Y-m-d"),
						"createname"=>$username,
						"dept_id"=>$deptId,
						"visit_num"=>"1",
					);
				}
				if($dealresult_id == '2'){
					$arrD = array(
						"createtime"=>date("Y-m-d"),
						"createname"=>$username,
						"dept_id"=>$deptId,
						"fail_num"=>"1",
					);
				}
				if($dealresult_id == '3'){
					$arrD = array(
						"createtime"=>date("Y-m-d"),
						"createname"=>$username,
						"dept_id"=>$deptId,
						"success_num"=>"1",
					);
				}
				$resB = $outbound->data($arrD)->add();
			}


			echo json_encode(array('success'=>true,'msg'=>"保存成功！",'transfer'=>$arrTransferInfo));

			if($taskD["save_not_dnd"] == "Y"){
				$exten = $_SESSION['user_info']['extension'];
				import('ORG.Pbx.bmi');
				$bmi = new bmi();
				//先挂断电话
				if($para_sys["result_hangup"] == "Y"){
					$extension_status = $bmi->getHints();   //获取分机状态
					if($extension_status[$exten]["stat"] == "State:InUse"){
						$bmi->hangup($exten);
						sleep(1);
					}
				}
				if($para_sys["outBound_close"] != "Y"){
					//再示闲
					sleep(1);
					$bmi->delDND($exten);
				}
			}


			$cdr = new Model("sales_cdr_".$task_id);
			$arrBillsec = $cdr->field("uniqueid,disposition,billsec")->where("uniqueid = '$uniqueid'")->find();

			$arr = array( "dealresult"=>$dealresult_id,
						  "dealuser"=>$user_info["username"],
						  //"phone1"=>$dialphone,
						  "phone1"=>$arrTmp["phone1"],
						  "calltype"=>$calltype,
						  "callresult"=>$callresult_id,
						  "dealtime"=>date('Y-m-d H:i:s',time()),
						  'description'=>$description,
						  'source_id'=>$id,
						  'uniqueid'=>$uniqueid,
						  'visittime'=>$callbacktime,
						  'billsec'=>$arrBillsec["billsec"],
						  'callstatus'=>$arrBillsec["disposition"],
					);

			$contact = new Model("sales_contact_history_".$task_id);
			//dump($_SESSION);
			//dump($arr);die;
			$hisRes = $contact->data($arr)->add();

			//记录外呼结果转换状态
			if($hisRes){
				if($arrTmp['dealresult_id'] != $dealresult_id && $arrTmp['dealresult_id'] != "0"){
					$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
					$re = new Model("task_result_status_".$task_id);
					$arrTs = array(
						"createtime"=>date("Y-m-d H:i:s"),
						"username"=>$user_info["username"],
						"customer_id"=>$id,
						//"phone"=>$dialphone,
						"phone"=>$arrTmp["phone1"],
						"old_status"=>$statusArr[$arrTmp['dealresult_id']],
						"new_status"=>$statusArr[$dealresult_id],
					);
					$rst = $re->data($arrTs)->add();
				}
			}





			//成功单标记这次的录音为成功单--必须要挂断电话后执行
			if($dealresult_id == '3'){
				$sales_cdr = new Model("sales_cdr_".$task_id);
				$sales_cdr->where("uniqueid = '$uniqueid'")->save(array("success_enable"=>"Y"));
				//echo $sales_cdr->getLastSql();
			}

			$source->where("id=".$id)->save(array("dealresult_id"=>$dealresult_id));

			if($hisRes){
				unset($_SESSION[$username."_task".$task_id."_history"][$id]);
			}

		}else{
			echo json_encode(array('msg'=>'保存失败！'));
		}
	}

	function saveResultDelDND(){
		$exten = $_SESSION['user_info']['extension'];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		sleep(1);
		$bmi->delDND($exten);
	}

	function insertSuccessData($task_id,$id,$arrData){
		$task_id = "56";
		$id = "486";
		dump($task_id);die;
	}

	function getContactHistory(){
		$user_info = session("user_info");
		$task_info = session("outbound_current_task");
		$task_id = $task_info["id"];
		$source_id = $_GET["source_id"];
		$uniqueid = $_GET["uniqueid"];
		$contact = new Model("sales_contact_history_".$task_id);
		$list = $contact->where("source_id=".$source_id)->order("id desc")->select();
		//echo $contact->getLastSql();die;
		//dump($list);die;
		$calltype_arr = array("preview"=>"预览外呼","autocall"=>"自动外呼");
		$dealresult_arr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");

		$callres = new Model("sales_callresult");
		$callresult_data = $callres->select();
		foreach( $callresult_data as $val ){
			$callresult_arr[$val["id"]] = $val["name"];
		}

		//$callresult_arr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		$i = 0;
		foreach($list as $v){

			$list[$i]["calltype"] = $calltype_arr[$v["calltype"]];
			$list[$i]["dealresult"] = $dealresult_arr[$v["dealresult"]];
			$list[$i]["callresult"] = $callresult_arr[$v["callresult"]];
			$list[$i]['operations'] = "<a target='_blank' href='agent.php?m=Telemarketing&a=playTaskCDR&uniqueid=" .$v['uniqueid'] ."&clientphone=" . $v['phone1']."&userfield="."autocall_".$v["phone1"]."_".$v['uniqueid'].".mp3"."'> 播放 </a>  ";
			$i++;
		}
		//dump($list);die;
		echo json_encode($list);
	}

	function playTaskCDR(){
		checkLogin();
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		//$userfield = str_replace(".wav",".mp3",$_GET['userfield']);//lame把.wav的文件转化成了.mp3文件
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};

		$AudioURL = "agent.php?m=Telemarketing&a=downloadTaskCDR&clientphone=${clientphone}&uniqueid=${uniqueid}". "&userfield=".$userfield;
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->assign("clientphone",$clientphone);
		$this->display();
	}


	function downloadTaskCDR(){
		$clientphone = $_GET['clientphone'];
		$uniqueid = $_GET['uniqueid'];
		$userfield = $_GET['userfield'];
		if( !$uniqueid ){ echo "录音文件不存在!\n";die;};
		$arrTmp = explode('.',$uniqueid);
		$timestamp = $arrTmp[0];
		$WAVfile = "/var/spool/asterisk/monitor/" . Date("Y-m",$timestamp). "/" .Date("d",$timestamp) ."/".$userfield;
		header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(".wav",".mp3", basename($WAVfile))."");
        readfile($WAVfile);

	}

	function preview_clickcall(){
		$task_id = $_POST['task_id'];
		$trunk = $_POST['trunk'];
		$exten = $_POST['exten'];
		$phone = $_POST['phone'];
		$callerid = $_POST['callerid']?$_POST['callerid']:"";

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrHint = $bmi->getHint($exten);
		$device = $arrHint['device'];
		$bmi->preview_clickcall($device,$exten,$phone,$task_id,$trunk,$callerid);
		echo "ok";
	}


	function getPopData(){
		$source = new Model("sales_source_".$this->_get("task_id"));
		$para_sys = readS();

		$phone_id = $_REQUEST["phone_id"];
		$phone_num = $_REQUEST["phone_num"];
		if($phone_id){
			$where = "id ='$phone_id'";
		}else{
			$where = "phone1 ='$phone_num'";
		}

		$data = $source->field("id,name,phone1,phone2,qq_number,email,dealresult_id as resultid")->where($where)->find();

		$data["massPhone"] = $data['phone1'];
		if($para_sys["outboundCustomer"] == "yes"){
			if($para_sys["hide_phone"] =="yes"){
				$data["phone1"] = substr($data["phone1"],0,3)."***".substr($data["phone1"],-4);
			}
		}
		/*弹屏后示忙
		$exten = $_SESSION['user_info']['extension'];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->putDND($exten);
		*/

		$username = $_SESSION['user_info']['username'];
		$task_id = $_REQUEST["task_id"];
		$phone = $_REQUEST["phone_num"];
		$uniqueid = $_REQUEST["uniqueid"];
		$arrData = $source->where("phone1= $phone OR phone2 = $phone")->select();
		foreach($arrData as $val){
			$_SESSION[$username."_task".$task_id."_history"][$val["id"]] = $uniqueid;
		}
		//dump($arrData);
		//dump($_SESSION);die;
		echo json_encode($data);
	}

	//将号码加入黑名单中
	function DNCAdd(){
		$DNC = M("sales_dnc");
		$count = $DNC->where("phonenumber='". $_POST["phone"] ."'")->count();

		if($count>0){
			echo "号码已在黑名单列表中";
		}else{
			$arr = array("phonenumber"=>$_POST["phone"]);
			$DNC->data($arr)->add();
			echo "ok";
		}
	}


	//坐席签入到自动外呼队列
	function joinAutoCallQueue(){
		//dump($_SESSION);die;
		//签入之前,移除掉本队列成员在其它队列中的

		$exten = $_GET['exten'];
		$task_id = $_GET['task_id'];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();

		$arr = $bmi->getAllQueueMember();
		//dump($arr);die;
		foreach($arr AS $k=>$v){
			//移除队列成员
			if( in_array($exten,$v['membername']) ){
				$action = "QueueRemove";
				$parameters = Array(
					"Queue"	=>	$k,
					"Interface"	=>	"Local/$exten@BG-QueueAgent/n",
				);
				$return = $bmi->asm->send_request($action,$parameters);
			}
		}

		$action = "QueueAdd";
		$parameters = Array(
			"Queue"	=>	"AutoCall_${task_id}",
			"Interface"	=>	"Local/$exten@BG-QueueAgent/n",
			"Penalty"	=>	"0",
			"Paused"	=>	"no",
			"MemberName"	=>	"$exten",
		);
		$return = $bmi->asm->send_request($action,$parameters);

		if( 'Error' == $return['Response'] ){
			echo "Error";
		}else{
			echo "OK";
		}
		//var_dump($return);

	}

	//坐席签出自动外呼队列
	function moveAutoCallQueue(){
		//dump($_SESSION);die;
		//签入之前,移除掉本队列成员在其它队列中的

		$exten = $_GET['exten'];
		$task_id = $_GET['task_id'];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();

		$action = "QueueRemove";
		$parameters = Array(
			"Queue"	=>	"AutoCall_$task_id",
			"Interface"	=>	"Local/$exten@BG-QueueAgent/n",
		);
		$return = $bmi->asm->send_request($action,$parameters);

		if( 'Error' == $return['Response'] ){
			echo "Error";
		}else{
			echo "OK";
		}
		//var_dump($return);

	}


	//坐席外呼报表统计——按时间来统计
	function autocallReport(){
		$username = $_SESSION["user_info"]["username"];
		$taskId = $_GET['taskId'];
		$date_start = $_GET['date_start'];
		$date_end	= $_GET['date_end'];

		$table_sc = 'sales_cdr_' .$taskId;
		if($username != 'admin'){
			$where = " AND u.username = '$username'";
		}else{
			$where = " ";
		}

		//$sql = "SELECT u.username AS username,u.cn_name,dst AS agent,COUNT(*) AS answeredtimes,round(SUM(billsec)/60) AS callduration,round(AVG(billsec)) AS averageduration FROM $table_sc sc left join users u on sc.dst=u.extension  WHERE dst <> '' AND disposition='ANSWERED' AND calldate > '$date_start' $where";
		$sql = "SELECT u.username AS username,u.cn_name,dst AS agent,COUNT(*) AS answeredtimes,SUM(billsec) AS callduration,AVG(billsec) AS averageduration FROM $table_sc sc left join users u on sc.dst=u.extension  WHERE dst <> '' AND disposition='ANSWERED' AND calldate > '$date_start' $where";

		if( $date_end ){
			$sql .= "AND calldate < '$date_end' ";
		}
		$sql .= "GROUP BY agent ORDER BY agent ASC";
		//echo $sql;die;
		$sc = M($table_sc);
		//坐席——接听次数——通话总时长——平均通话时长 :四舍五入

		$arrRet = $sc->query($sql);

		foreach($arrRet as &$val){
			$val["callduration2"] = $val["callduration"];
			$val["averageduration2"] = $val["averageduration"];

			$val["callduration"] = sprintf("%02d",intval($val["callduration"]/3600)).":".sprintf("%02d",intval(($val["callduration"]%3600)/60)).":".sprintf("%02d",intval((($val[callduration]%3600)%60)));

			$val["averageduration"] = sprintf("%02d",intval($val["averageduration"]/3600)).":".sprintf("%02d",intval(($val["averageduration"]%3600)/60)).":".sprintf("%02d",intval((($val[averageduration]%3600)%60)));
		}

		//dump($sc->getLastSql());
		//dump($arrRet);die;
		$rowsList = count($arrRet) ? $arrRet : false;
		$ary["total"] = count($arrRet);
		$ary["rows"] = $rowsList;
		echo json_encode($ary);

	}

	function ttt(){

		$this->display();
	}

	function newList(){
		checkLogin();
		$exten = $_SESSION["user_info"]["extension"];
		$task = M("sales_task");
		$user_info = session("user_info");
		$list = $task->where("dept_id='". $user_info["d_id"]  ."' and enable='Y'")->order("createtime desc")->select();

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrT = $bmi->getAllQueueMember();

		$n = 0;
		foreach($list as $val){
			$id = $val["id"];
			if( array_key_exists("AutoCall_$id",$arrT) ){
				$list[$n]['queueStatus'] = $arrT["AutoCall_$id"]['membername'];
			}
			$n++;
		}
		unset($n);

		$maxTaskId = $task->where("dept_id='". $user_info["d_id"]  ."' and enable='Y'")->max('id');
		$i = 0;
		foreach($list as $val){
			if($val["calltype"] == "autocall"){
				if( in_array($exten,$val["queueStatus"]) ){
					$list[$i]["extenStatus"] = "<span id='Checked_In".$val["id"]."' style='color:red;'><span  class='checkedIn' taskId='".$val["id"]."'>已签入</span></span>";
				}else{
					$list[$i]["extenStatus"] = "<span class='checkedOut' id='Not_checked".$val["id"]."'>未签入</span>";
				}
			}
			if($val["id"] == $maxTaskId){
				$maxTaskName = $val["name"];
			}
			$task_name[$val["id"]] = $val["name"];
			$i++;
		}

		$this->assign("taskId",$maxTaskId);
		$this->assign("maxTaskName",$maxTaskName);
		$this->assign("task_name",json_encode($task_name));

		//dump($task_name);die;
		$this->assign("task",$list);

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['list_enabled'] == "Y" ){
				if($val['en_name'] != "createuser" ){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					$arrField[0][$i]['width'] = "100";
				}
				if( $val['tiptools_enabled'] == 'Y' ||  $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}

		$arrFd = array("field"=>"ck","checkbox"=>true,"hidden"=>true);
		$arrFd2 = array("field"=>"dealresult_id","title"=>"客户状态","width"=>"120");
		$arrFd3 = array("field"=>"modifytime","title"=>"最后呼叫时间","width"=>"120","sortable"=>"true");
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd2);
		array_unshift($arrField[0],$arrFd3);
		//dump($arrField);die;
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("fielddTpl",$fielddTpl);

		$areaTpl = implode(",",$areaTpl2);
		$this->assign("areaTpl",$areaTpl);
		$this->assign("extension",$_SESSION['user_info']['extension']);

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}


		$para_sys = readS();
		/*
		if($para_sys["outboundCustomer"] == "yes"){  //editOutboundCustomer_hide
			if($para_sys["hide_phone"] =="yes"){
				$this->assign("hide_phone","Y");
			}else{
				$this->assign("hide_phone","N");
			}
		}
		*/
		if($para_sys["editOutboundCustomer_hide"] =="all" || $para_sys["editOutboundCustomer_hide"] =="phone"){
			$this->assign("hide_phone","Y");
		}else{
			$this->assign("hide_phone","N");
		}

		if($para_sys["getPhone"] == "Y"){
			$this->assign("getPhone","Y");
		}else{
			$this->assign("getPhone","N");
		}

		if($para_sys["getAgainPhone"] == "Y"){
			$this->assign("getAgainPhone","Y");
		}else{
			$this->assign("getAgainPhone","N");
		}


		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("wj",$arrAL) ){
			$this->assign("Questionnaire","Y");
		}else{
			$this->assign("Questionnaire","N");
		}


		$this->display();
	}

	function outBoundList(){
		checkLogin();
		$exten = $_SESSION["user_info"]["extension"];
		$task_id = $_REQUEST["task_id"];
		$task = M("sales_task");
		$arrTK = $task->where("id = '$task_id'")->find();
		$task_name = $arrTK["name"];
		$this->assign("task_id",$task_id);
		$this->assign("task_name",$task_name);
		$this->assign("arrTK",$arrTK);

		$cmFields = getFieldCache();
		$i = 0;
		foreach($cmFields as $val){
			if($val['list_enabled'] == "Y" ){
				if($val['en_name'] != "createuser" ){
					$arrField[0][$i]['field'] = $val['en_name'];
					$arrField[0][$i]['title'] = $val['cn_name'];
					$arrField[0][$i]['width'] = "100";
				}
				if( $val['tiptools_enabled'] == 'Y' ||  $val['text_type'] == '3'){
					$areaTpl2[] = $val["en_name"];
				}
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['list_enabled'] == 'Y' && $val["en_name"] != "hide_fields"  && $val['text_type'] != '3'){
				$fielddTpl[] = $val;
			}
		}

		$arrFd = array("field"=>"ck","checkbox"=>true,"hidden"=>true);
		$arrFd2 = array("field"=>"dealresult_id","title"=>"客户状态","width"=>"120");
		$arrFd3 = array("field"=>"modifytime","title"=>"最后呼叫时间","width"=>"120","sortable"=>"true");
		array_unshift($arrField[0],$arrFd);
		array_unshift($arrField[0],$arrFd2);
		array_unshift($arrField[0],$arrFd3);
		//dump($arrField);die;
		$arrF = json_encode($arrField);
		$this->assign("fieldList",$arrF);
		$this->assign("fielddTpl",$fielddTpl);

		$areaTpl = implode(",",$areaTpl2);
		$this->assign("areaTpl",$areaTpl);
		$this->assign("extension",$_SESSION['user_info']['extension']);

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}


		$para_sys = readS();
		if($para_sys["editOutboundCustomer_hide"] =="all" || $para_sys["editOutboundCustomer_hide"] =="phone"){
			$this->assign("hide_phone","Y");
		}else{
			$this->assign("hide_phone","N");
		}

		if($para_sys["getPhone"] == "Y"){
			$this->assign("getPhone","Y");
		}else{
			$this->assign("getPhone","N");
		}

		if($para_sys["getAgainPhone"] == "Y"){
			$this->assign("getAgainPhone","Y");
		}else{
			$this->assign("getAgainPhone","N");
		}


		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("wj",$arrAL) ){
			$this->assign("Questionnaire","Y");
		}else{
			$this->assign("Questionnaire","N");
		}


		$this->display();
	}

	function outBoundData(){
		$dealresult_id = $_REQUEST['dealresult_id'];
		//dump($dealresult_id);die;
		$username = $_SESSION["user_info"]["username"];
		$searchmethod = $_REQUEST["searchmethod"];

		$where = "dealuser = '$username' AND  locked='Y'";
		if($dealresult_id == "0"){
			$where .= " AND dealresult_id ='0'";
		}else{
			$where .= empty($dealresult_id) ? "" : " AND dealresult_id ='$dealresult_id'";
		}

		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){    //&& $val['text_type'] != '3'
				$arrF[] = $val['en_name'];
				$$val['en_name'] = $_REQUEST[$val['en_name']];

				if( $val['text_type'] == '3'){
					$areaTpl[$val["en_name"]] = $val["en_name"];
				}
			}
		}
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3' && $vm['en_name'] != 'createuser'){
				$aa[] = $vm['en_name'];
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					if($vm['text_type'] == '2'){
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
					}else{
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
					}
				}
			}
		}
        $where .= empty($createuser)?"":" AND dealuser = '$createuser'";

		//dump($where);die;
		$task_id = $_REQUEST["task_id"];
		$source = new Model("sales_source_".$task_id);
		$count = $source->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$fields = "id,dealuser,createtime,callbacktime,calledflag,modifytime,dealresult_id,".implode(",",$arrF);

		$sort = $_REQUEST["sort"];
		$order = $_REQUEST["order"];
		if($sort){
			$usort = $sort." ".$order;
		}else{
			$usort = "modifytime desc";
		}

		$sourceData = $source->order($usort)->field($fields)->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$sqlSearch = $source->getLastSql();

		//echo $source->getLastSql();
		//dump($sourceData);die;
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		//$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($sourceData as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;

					$vm['cn_name'] = $cnName[$vm["dealuser"]];
				}
			}
		}

		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		foreach($sourceData as &$val){
			$val["resultid"] = $val['dealresult_id'];
			$val["massPhone"] = $val['phone1'];
			$val["massPhone2"] = $val['phone2'];
			$val["massEmail"] = $val['email'];

			$dealresult = $statusArr[$val["dealresult_id"]];
			$val["dealresult_id"] = $dealresult;

			if($para_sys["outboundCustomer"] == "yes"){
				if($para_sys["hide_phone"] =="yes"){
					$val["phone1"] = substr($val["phone1"],0,3)."***".substr($val["phone1"],-4);
					if($val["phone2"]){
						$val["phone2"] = substr($val["phone2"],0,3)."***".substr($val["phone2"],-4);
					}
				}
				if($val['email']){
					if($para_sys["hide_email"] =="yes"){
						$val["email"] = "***".strstr($val["email"],"@");
					}
				}
				if($val['qq_number']){
					if($para_sys["hide_qq"] =="yes"){
						$val["qq_number"] = "***".substr($val["qq_number"],3);
					}
				}
			}


			foreach($cmFields as &$vm){
				if($vm['list_enabled'] == 'Y'){
					if($vm["text_type"] == "4"){
						if($val[$vm["en_name"]] == "0000-00-00 00:00:00"){
							$val[$vm["en_name"]] = "";
						}
					}elseif($vm["text_type"] == "5"){
						if($val[$vm["en_name"]] == "0000-00-00"){
							$val[$vm["en_name"]] = "";
						}
					}
				}
			}


		}
		//dump($where);die;
		//dump($sourceData);die;
		$rowsList = count($sourceData) ? $sourceData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["sears"] = base64_encode($sqlSearch);

		echo json_encode($ary);
	}

	function outBoundDataBAK20160308(){
		$dealresult_id = $_REQUEST['dealresult_id'];
		//dump($dealresult_id);die;
		$username = $_SESSION["user_info"]["username"];
		$name = $_REQUEST["name"];
		$company = $_REQUEST["company"];
		$phone = $_REQUEST["phone"];
		$phone2 = $_REQUEST["phone2"];
		$qq = $_REQUEST["qq"];
		$sex = $_REQUEST["sex"];


		$where = "dealuser = '$username' AND  locked='Y'";
		$where .= " AND dealresult_id ='$dealresult_id'";
		$where .= empty($name) ? "" : " AND name like '%$name%'";
		$where .= empty($company) ? "" : " AND company like '%$company%'";
		$where .= empty($phone) ? "" : " AND phone1 like '%$phone%'";
		$where .= empty($phone2) ? "" : " AND phone2 like '%$phone2%'";
		$where .= empty($qq) ? "" : " AND qq like '%$qq%'";
		$where .= empty($sex) ? "" : " AND sex like '%$sex%'";

		$cmFields = getFieldCache();
		foreach($cmFields as $val){
			if($val['list_enabled'] == 'Y'){    //&& $val['text_type'] != '3'
				$arrF[] = $val['en_name'];
				$$val['en_name'] = $_REQUEST[$val['en_name']];

				if( $val['text_type'] == '3'){
					$areaTpl[$val["en_name"]] = $val["en_name"];
				}
			}
		}
		foreach($cmFields as $vm){
			if($vm['list_enabled'] == 'Y'  && $vm['text_type'] != '3' && $vm['en_name'] != 'createuser'){
				$aa[] = $vm['en_name'];
				if( $searchmethod == "equal"){
					 $where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
				}else{
					if($vm['text_type'] == '2'){
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." = '".$$vm['en_name']."'";
					}else{
						$where .= empty($$vm['en_name'])?"":" AND ".$vm['en_name'] ." like '%".$$vm['en_name']."%'";
					}
				}
			}
		}
        $where .= empty($createuser)?"":" AND dealuser = '$createuser'";

		//dump($where);die;
		$task_id = $_REQUEST["task_id"];
		$source = new Model("sales_source_".$task_id);
		$count = $source->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$fields = "id,dealuser,createtime,callbacktime,calledflag,modifytime,dealresult_id,".implode(",",$arrF);
		$sourceData = $source->order("modifytime desc")->field($fields)->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$sqlSearch = $source->getLastSql();

		//echo $source->getLastSql();
		//dump($sourceData);die;
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		//$deptId_user = $userArr["deptId_user"];
		$deptName_user = $userArr["deptName_user"];

		$row = getSelectCache();
		foreach($cmFields as $val){
			if($val['text_type'] == "2"){
				foreach($sourceData as &$vm){
					$status = $row[$val['en_name']][$vm[$val['en_name']]];
					$vm[$val['en_name']] = $status;

					$vm['cn_name'] = $cnName[$vm["dealuser"]];
				}
			}
		}

		$statusArr = array("0"=>"未处理","1"=>"继续跟踪","2"=>"失败单","3"=>"成功单");
		foreach($sourceData as &$val){
			$val["resultid"] = $val['dealresult_id'];
			$val["massPhone"] = $val['phone1'];
			$val["massPhone2"] = $val['phone2'];
			$val["massEmail"] = $val['email'];

			$dealresult = $statusArr[$val["dealresult_id"]];
			$val["dealresult_id"] = $dealresult;

			if($para_sys["outboundCustomer"] == "yes"){
				if($para_sys["hide_phone"] =="yes"){
					$val["phone1"] = substr($val["phone1"],0,3)."***".substr($val["phone1"],-4);
					if($val["phone2"]){
						$val["phone2"] = substr($val["phone2"],0,3)."***".substr($val["phone2"],-4);
					}
				}
				if($val['email']){
					if($para_sys["hide_email"] =="yes"){
						$val["email"] = "***".strstr($val["email"],"@");
					}
				}
				if($val['qq_number']){
					if($para_sys["hide_qq"] =="yes"){
						$val["qq_number"] = "***".substr($val["qq_number"],3);
					}
				}
			}

		}
		//dump($where);die;
		//dump($sourceData);die;
		$rowsList = count($sourceData) ? $sourceData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["sears"] = base64_encode($sqlSearch);

		echo json_encode($ary);
	}


	//获取预览式外呼号码
	function getPreviewPhone(){
		$para_sys = readS();
		$num = $para_sys["preview_limit_num"];
		$username = $_SESSION["user_info"]["username"];
		$task_id = $_REQUEST["task_id"];

		$source = M("sales_source_".$task_id);
		$data = array("locked"=>"Y","dealuser"=>$username);
		/*
		if(file_exists("BGCC/Conf/condition.php")){
			$arrF = require "BGCC/Conf/condition.php";
			if(array_key_exists($task_id,$arrF)){
				$where = $arrF[$task_id];
			}else{
				$where = "locked='N' and dealresult_id=0";
			}
		}else{
			$where = "locked='N' and dealresult_id=0";
		}
		*/
		$arrF = getCondition();
		if($arrF){
			if($arrF[$task_id]){
				$where = $arrF[$task_id];
			}else{
				$where = "locked='N' and dealresult_id=0";
			}
		}else{
			$where = "locked='N' and dealresult_id=0";
		}
		//dump($where);die;
		$where .= " AND dealuser is null";

		$count = $source->where($where)->order('id asc')->limit($num)->save($data);  //->where("locked='N' and dealresult_id=0 AND dealuser is null")
		if($count){
			echo json_encode(array('success'=>true,'msg'=>"成功获取{$count}条记录"));
		} else {
			echo json_encode(array('msg'=>'没有数据可获取'));
		}
	}

	//获取预自动外呼号码
	function getAutoCallPhone(){
		$para_sys = readS();
		$num = $para_sys["preview_limit_num"];
		$username = $_SESSION["user_info"]["username"];
		$task_id = $_REQUEST["task_id"];

		$source = M("sales_source_".$task_id);
		$data = array("locked"=>"Y","dealuser"=>$username,"dealresult_id" => "0");
		/*
		if(file_exists("BGCC/Conf/condition.php")){
			$arrF = require "BGCC/Conf/condition.php";
			if(array_key_exists($task_id,$arrF)){
				$where = $arrF[$task_id];
			}else{
				$where = "locked='N' and dealresult_id=0";
			}
		}else{
			$where = "locked='N' and dealresult_id=0";
		}
		*/

		$arrF = getCondition();
		if($arrF){
			if($arrF[$task_id]){
				$where = $arrF[$task_id];
			}else{
				$where = "locked='N' and dealresult_id=0";
			}
		}else{
			$where = "locked='N' and dealresult_id=0";
		}
		$where .= " AND dealuser is null";
		//dump($where);die;
		$count = $source->where($where)->order('id asc')->limit($num)->save($data);  //->where("locked='N' and dealresult_id=0")
		//echo $source->getLastSql();die;
		if($count){
			echo json_encode(array('success'=>true,'msg'=>"成功获取{$count}条记录"));
		} else {
			echo json_encode(array('msg'=>'没有数据可获取'));
		}
	}


	function nextCustomer(){
		$task_id = $_REQUEST["task_id"];
		$source = new Model("sales_source_".$task_id);
		$sears = base64_decode($_GET["sears"]);
		//dump($sears);die;
		$id = $_GET["id"];
		$save_notice = $_REQUEST["save_notice"];
		$cmData = $source->query($sears);

		foreach($cmData as $val){
			$cmId[] = $val["id"];
		}
		/*
		//获取下条数据
		$srId = array_flip($cmId);
		$afterId = $cmId[$srId[$id]+1];
		*/
		$rand_keys = array_rand($cmId);
		$afterId = $cmId[$rand_keys];
		$arr = $source->field("id,phone1,name")->where("id = '$afterId'")->find();
		//$title = empty($arr["name"]) ? $arr["phone1"] : $arr["name"];
		if($arr["name"]){
			$title = $arr["name"];
			$title_type = "name";
		}else{
			$title = $arr["phone1"];
			$title_type = "phone";
		}
		//$afterId = $cmId[$srId[$id]+1];
		echo json_encode(array('id'=>$afterId,'title'=>$title,'title_type'=>$title_type));
	}

	function lastCustomer(){
		$task_id = $_REQUEST["task_id"];
		$source = new Model("sales_source_".$task_id);
		$sears = base64_decode($_GET["sears"]);
		$id = $_GET["id"];
		$save_notice = $_REQUEST["save_notice"];
		$cmData = $source->query($sears);

		foreach($cmData as $val){
			$cmId[] = $val["id"];
		}

		$srId = array_flip($cmId);
		$afterId = $cmId[$srId[$id]-1];

		echo json_encode(array('id'=>$afterId));
	}

	function deleteOutBoundData(){
		$id = $_REQUEST["id"];
		$task_id = $_REQUEST["task_id"];
		$source = new Model("sales_source_".$task_id);
		$result = $source->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function getPhoneData(){
		$task_id = $_REQUEST["task_id"];
		$phone = $_REQUEST["phone"];
		$source = new Model("sales_source_".$task_id);
		$para_sys = readS();
		$data = $source->field("id,name,phone1,phone2,qq_number,email,dealresult_id as resultid")->where("phone1='$phone' OR phone2='$phone'")->find();
		$data["massPhone"] = $data['phone1'];
		if($para_sys["outboundCustomer"] == "yes"){
			if($para_sys["hide_phone"] =="yes"){
				$data["phone1"] = substr($data["phone1"],0,3)."***".substr($data["phone1"],-4);
			}
		}
		echo json_encode($data);
	}


	//客户跟进建议
	function proposalRecordData(){
		$username = $_SESSION['user_info']['username'];
		$task_id = $_REQUEST["task_id"];
		$customer_id = $_REQUEST["customer_id"];
		$proposal = new Model("sales_proposal_record_".$task_id);


		$where = "customer_id = '$customer_id' ";

		$count = $proposal->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $proposal->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		//var_export($arrData);die;
		$userArr = readU();
		$cn_user = $userArr["cn_user"];

		foreach($arrData as &$val){
			$val["proponents_name"] = $cn_user[$val["proponents"]];
			$val["replies_people_name"] = $cn_user[$val["replies_people"]];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);

	}

	function addProposal(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$task_id = $_REQUEST["task_id"];
		$customer_id = $_REQUEST["customer_id"];
		$role = $_REQUEST["role"];
		$dealuser = $_REQUEST["dealuser"];
		$phone = $_REQUEST["phone"];
		$name = $_REQUEST["name"];

		$arrAdmin = getAdministratorNum();
		if( in_array($username,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$username);
		}

		$this->assign("task_id",$task_id);
		$this->assign("customer_id",$customer_id);
		$this->assign("role",$role);
		$this->assign("dealuser",$dealuser);
		$this->assign("phone",$phone);
		$this->assign("name",$name);

		$this->display();
	}

	function insertProposal(){
		$username = $_SESSION['user_info']['username'];
		$task_id = $_REQUEST["task_id"];
		$proposal = new Model("sales_proposal_record_".$task_id);
		if($_REQUEST["role"] == "admin"){
			$arrData = array(
				"createtime" => date("Y-m-d H:i:s"),
				"proponents" => $username,
				//"replies_people" => $_REQUEST["dealuser"],
				"customer_id" => $_REQUEST["customer_id"],
				"proposal_content" => $_REQUEST["proposal_content"],
			);
		}
		$result = $proposal->data($arrData)->add();
		if ($result){
			$arrF = array(
				'id' => $result,
				'customer_id' => $_REQUEST["customer_id"],
				'task_id' => $task_id,
				'phone' => $_REQUEST["phone"],
				'name' => $_REQUEST["name"],
				'replies_people' => $_REQUEST["dealuser"],
			);
			$this->addProposalCahce($arrF);
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}

	}

	function addProposalCahce($arrF){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		if(file_exists("BGCC/Conf/crm/$db_name/proposalMessage.php")){
			$arrT = require "BGCC/Conf/crm/$db_name/proposalMessage.php";
			if(is_array($arrT)){
				array_push($arrT,$arrF);
			}else{
				$arrT[0] = $arrF;
			}
		}else{
			$arrT[0] = $arrF;
		}

		$content = "<?php\nreturn " .var_export($arrT,true) .";\n ?>";
		file_put_contents("BGCC/Conf/crm/$db_name/proposalMessage.php",$content);
	}

	function editProposalCahce($task_id,$id){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrT = require "BGCC/Conf/crm/$db_name/proposalMessage.php";
		if(is_array($arrT)){
			foreach($arrT as $val){
				if($val["id"] == $id && $val["task_id"] == $task_id){
					$arrF[] = $val;
				}else{
					$arrData[] = $val;
				}
			}
		}
		$content = "<?php\nreturn " .var_export($arrData,true) .";\n ?>";
		file_put_contents("BGCC/Conf/crm/$db_name/proposalMessage.php",$content);
	}


	function editProposal(){
		checkLogin();
		$id = $_REQUEST["id"];
		$task_id = $_REQUEST["task_id"];
		$role = $_REQUEST["role"];
		$dealuser = $_REQUEST["dealuser"];
		$username = $_SESSION['user_info']['username'];
		$proposal = new Model("sales_proposal_record_".$task_id);

		$arrF = $proposal->where("id = '$id'")->find();

		$this->assign("task_id",$task_id);
		$this->assign("arrF",$arrF);
		$this->assign("role",$role);
		$this->assign("id",$id);
		$this->assign("dealuser",$username);

		$this->display();
	}

	function updateProposal(){
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST["id"];
		$task_id = $_REQUEST["task_id"];
		$proposal = new Model("sales_proposal_record_".$task_id);

		$arrData = array(
			"replies_people" => $_REQUEST["replies_people"],
			"proposal_content" => $_REQUEST["proposal_content"],
			"reply_content" => $_REQUEST["reply_content"],
		);

		$result = $proposal->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			$this->editProposalCahce($task_id,$id);
			echo json_encode(array('success'=>true,'msg'=>'保存成功！'));
		} else {
			echo json_encode(array('msg'=>'保存失败！'));
		}
	}

	function proposalRemind(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrData = getProposalMessage();
		$sales_task = new Model("sales_task");
		$arrF = $sales_task->field("id,name")->where("dept_id='$d_id' and enable='Y'")->select();
		//dump($arrF);die;
		foreach($arrF as $val){
			$tmp[$val["id"]] = $val["name"];
			$arrID[] = $val["id"];
		}

		foreach($arrData as &$val){
			$val["msg"] = $tmp[$val["task_id"]]."任务，号码</br> ".$val["phone"]."需要回复";
			$val["name"] = "<a href='javascript:void(0);' onclick=\"openCustomer("."'".$val["id"]."','".$val["task_id"]."','".$val["customer_id"]."','".$val["phone"]."','".$val["name"]."'".")\" >".$val["msg"]."</a>";
			if($val["replies_people"] == $username){
				$arrRemind[] = $val;
			}
		}
		//dump($arrData);die;
		$rowsList = count($arrRemind) ? $arrRemind : false;
		$arrT["total"] = count($arrRemind);
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function visitRemind(){
		$visit = $_POST['visitData'];
		$para_sys = readS();

		$sales_task = new Model("sales_task");
		$arrData = $sales_task->field("id,name")->select();

		foreach($arrData as $val){
			$tmp[$val["id"]] = $val["name"];
		}
		//dump($visit);
		foreach($visit as $k=>&$v){
			if($v["visit_type"] == "out"){
				if($para_sys["windowOpen_hide"] == "Y"){
					$v['phone_hide'] = substr($v["phone"],0,3)."***".substr($v["phone"],-4);
				}else{
					$v['phone_hide'] = $v["phone"];
				}
				$v["url"] = "agent.php?m=Telemarketing&a=editWCustomer&calltype=preview&task_id=".$v['task_id']."&phone_num=".$v['phone']."&id=".$v['customer_id']."&visit_id=".$v['id'];

				$v["msg"] = "回访时间：".$v['visit_time']."  任务名称：".$tmp[$v["task_id"]]."  电话：</br> ".$v["phone_hide"]."需要回复";

				$v["name"] = "<a href='javascript:void(0);' onclick=\"openCustomer2("."'".$v["url"]."','".$v["phone_hide"]."'".")\" >".$v["msg"]."</a>";
				$arr[] = $v["name"];
			}
		}
		$msg = implode("<br><hr>",$arr);
		echo json_encode(array('success'=>true,'msg'=>$msg));
	}

	function transferOrderList(){
		//dump($_REQUEST);die;
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$task_id = $_REQUEST["task_id"];
		$id = $_REQUEST["id"];
		$phone = $_REQUEST["phone"];

		$customer = new Model("customer");
		$arrD = $customer->where("phone1 = '$phone' OR phone2 = '$phone'")->find();

		if(count($arrD)>0){
			$order_info = M("order_info");
			$arrOD = $order_info->field("id,phone,telephone,createname")->where("phone = '$phone' OR telephone = '$phone'")->find();
			if($arrOD){
				//有订单---------不让添加订单
				echo json_encode(array('id'=>$arrD["id"],'msg'=>"该客户已经下过订单！下单工号为：".$arrOD["createname"]));
				die;
			}else{
				if($username != $arrD["createuser"]){
					//有客户资料，无订单，其他坐席的资料---------不让添加订单
					echo json_encode(array('id'=>$arrD["id"],'msg'=>"客户资料已存在，不能下单，该资料的所属工号为：".$arrD["createuser"]));
					die;
				}else{
					//有客户资料，无订单，自己的资料---------可以添加订单
					echo json_encode(array('success'=>true,'id'=>$arrD["id"]));
					die;
				}
			}
		}else{
			$cmFields = getFieldCache();
			foreach($cmFields as $val){
				if($val['list_enabled'] == 'Y'){
					$arrF[] = $val['en_name'];
				}
			}
			$fields = "id,dealuser,openid,".implode(",",$arrF);
			$source = new Model("sales_source_".$task_id);
			//$arrData = $source->field($fields)->where("id = '$id' AND (brand != 'Y' OR brand IS NULL)")->find();
			$arrData = $source->field($fields)->where("id = '$id'")->find();
			if($arrData){
				$sid = array_shift($arrData);
				$sdealuser = array_shift($arrData);
				$arrData["createuser"] = $username;
				$arrData["createtime"] = date("Y-m-d H:i:s");
				//dump($arrData);die;
				$res = $customer->data($arrData)->add();

				if($res){
					$source->where("id in ($id)")->save(array("brand"=>"Y"));
					//把历史记录转到客户平台
					$history = new Model("sales_contact_history_".$task_id);
					$arrHis = $history->field("source_id,uniqueid,description,phone1,dealtime")->where("source_id = '$id'")->select();
					$sql = "insert into visit_record(createtime,customer_id,createname,visit_content,visit_phone,visit_type,accountcode,dept_id) values";
					$value = "";

					foreach($arrHis as &$val){
						if(!$val["uniqueid"]){
							$val["visit_type"] = "qq";
						}else{
							$val["visit_type"] = "phone";
						}
						$str = "(";
						$str .= "'" .$val["dealtime"]. "',";
						$str .= "'" .$res. "',";
						$str .= "'" .$username. "',";
						$str .= "'" .$val["description"]. "',";
						$str .= "'" .$val["phone1"]. "',";
						$str .= "'" .$val["visit_type"]. "',";
						$str .= "'" .$val["uniqueid"]. "',";
						$str .= "'" .$d_id. "'";
						$str .= ")";
						$value .= empty($value)?"$str":",$str";
					}
					if($value){
						$sql .= $value;
						$result = $history->execute($sql);
					}


					$transfer = new Model("customer_transferees");
					$arrData = array(
						"customer_id" => $res,
						"createtime" => date("Y-m-d H:i:s"),
						"transferType" => "out",
						"forwardedname" => $username, //转交人
						"recipient" => $username,  //接收人
						"task_id" => $task_id,
						"outBound_customer_id" => $id,
					);
					$transfer->data($arrData)->add();
				}

				echo json_encode(array('success'=>true,'id'=>$res,'msg'=>"客户资料已转交成功！"));
			}
		}
	}

	function putIdle(){
		$exten = $_SESSION['user_info']['extension'];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		sleep(1);
		$bmi->delDND($exten);
		echo json_encode(array('success'=>true,'msg'=>"示闲成功！"));
	}


}

