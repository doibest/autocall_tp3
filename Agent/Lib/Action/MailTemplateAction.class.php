<?php
class MailTemplateAction extends Action{
	function mailList(){
		checkLogin();
		$mailt = new Model("mailtemplate");
		$mailData = $mailt->where("id = 2")->find();
		$this->assign("mailData",$mailData);

		$this->display();
	}

	function updateMail(){
		$mail = new Model("mailtemplate");
		$arrData = Array(
			'addressee'=>$_POST['addressee'],
			'username'=>$_POST['username'],
			'title'=>$_POST['title'],
			'mailcontent'=>$_POST['mailcontent'],
		);
		$result = $mail->data($arrData)->where("id=2")->save();
		if(false !== $result){
			//echo "ok";
			goback("","agent.php?m=MailTemplate&a=mailList");
		}else{
			echo L('Error');
		}
	}


	function selecTemplate(){
		$mail = new Model("mailtemplate");
		$id = $_GET['id'];
		if($id){
			$sendData = $mail->where("id = $id")->find();
			echo json_encode($sendData);
		}
	}

	//发送邮件页面
	function sendMailList(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$mail = new Model("mailtemplate");
		if($username == "admin"){
			$mailDataList = $mail->select();
		}else{
			$mailDataList = $mail->where("username = '$username' OR templatetype = 'system'")->select();
		}
		$this->assign("mailDataList",$mailDataList);

		if($_REQUEST["emaillist"]){
			$para_sys = readS();
			if($para_sys["hide_email"] =="yes"){
				foreach(explode(",",$_REQUEST["emaillist"]) as $vm){
					$email[] = "***".strstr($vm,"@");
				}
				$emailData = implode(",",$email);
				$this->assign("emaillist",$emailData);
			}else{
				$this->assign("emaillist",$_REQUEST["emaillist"]);
			}
			$this->assign("emailData",$_REQUEST["emaillist"]);
		}else{
			$this->assign("message","ms");
		}

		$para_sys = readS();
		if($para_sys["hide_email"] =="yes"){
			$this->assign("message","ms");
		}


		$this->display();
	}

	function doSendMail(){
		if($_POST['message'] == "ms"){
			$address = $_POST['addressee'];
		}else{
			$address = $_POST['addressee2'];
		}
		//dump($address);
		//dump($_POST);die;
		$message = $_POST["mailcontent"];
		$title = $_POST["title"];
		$mess = str_replace("<p>","",$message);
		$content = str_replace("</p>","",$mess);
		$massAddress1 = explode(",",$address);
		$massAddress = array_filter($massAddress1);


		$username = $_SESSION['user_info']['username'];
		$arrF = getEmailSmtp();
		$arrM = $arrF[$username];

		if($arrM["mail_host"] && $arrM["mail_from"] && $arrM["mail_password"] && $arrM["mail_enable"] == "Y"){
			$sendMod = "agent";
		}else{
			$sendMod = "sys";
		}
		//dump($sendMod);die;
		if($address){
			if($sendMod == "sys"){
				foreach($massAddress as $v){
					$smail[] = sendMail($v,$title,$content);
				}
			}else{
				foreach($massAddress as $v){
					$smail[] = AgentSendMail($v,$title,$content,$arrM);
				}
			}
			if( in_array("sendmail success",$smail) ){
				$cn_name = $_SESSION['user_info']['cn_name'];
				$sendmail = new Model("sendmail");
				$arrData = array(
					"createtime"=>date("Y-m-d H:i:s"),
					"addressee"=>$address,
					"sendtitle"=>$title,
					"sendcontent"=>$content,
					"sendname"=>$cn_name,
				);
				$num = $sendmail->data($arrData)->add();

				if($num){
					//goback("邮件发送成功");
					echo "<script>alert('邮件发送成功');</script>";
				}
			}
		}
	}


	function editMail(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];

		$arrAdmin = getAdministratorNum();
		if( in_array($username,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$username);
		}


		$mail = new Model("user_mail");
		$arrData = $mail->where("agentname = '$username'")->find();
		$this->assign("arrData",$arrData);

		if($arrData){
			$this->assign("status","edit");
		}else{
			$this->assign("status","add");
		}

		$this->display();
	}

	function saveMail(){
		$username = $_SESSION['user_info']['username'];
		$mail = new Model("user_mail");
		$arrData = Array(
			'agentname'=>$_REQUEST['username'],
			'mail_host'=>$_REQUEST['mail_host'],
			'mail_from'=>$_REQUEST['mail_from'],
			'mail_password'=>$_REQUEST['mail_password'],
			'mail_enable'=>$_REQUEST['mail_enable'],
		);
		if($_REQUEST["status"] == "edit"){
			array_shift($arrData);
			//dump($arrData);die;
			$result = $mail->data($arrData)->where("agentname = '$username'")->save();
			if ($result !== false){
				$this->mailCache();
				echo json_encode(array('success'=>true,'msg'=>'保存成功！'));
			} else {
				echo json_encode(array('msg'=>'保存失败！'));
			}
		}else{
			$result = $mail->data($arrData)->add();
			if ($result){
				$this->mailCache();
				echo json_encode(array('success'=>true,'msg'=>'保存成功！'));
			} else {
				echo json_encode(array('msg'=>'保存失败！'));
			}
		}
	}


	function mailCache(){
		$mail = new Model("user_mail");
		$arrData = $mail->select();
		foreach($arrData as $key=>$val){
			$arrF[$val["agentname"]] = $val;
		}
		F("emailSmtp",$arrF,"BGCC/Conf/");
	}
}

?>
