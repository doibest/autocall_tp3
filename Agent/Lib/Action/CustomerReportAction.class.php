<?php
class CustomerReportAction extends Action{
	function serciceReport2(){
		checkLogin();
		$service = new Model("servicetype");
		$serviceList = $service->select();
		$this->assign("serviceList",$serviceList);
		$servicerecords = new Model("servicerecords");
		$year_month = date("Y-m");

		//$sql = "SELECT DATE(createtime) as date,SUM(CASE WHEN servicetype_id=1 THEN 1 ELSE 0 END) AS consulting,SUM(CASE WHEN servicetype_id=2 THEN 1 ELSE 0 END) AS complaints,SUM(CASE WHEN servicetype_id=3 THEN 1 ELSE 0 END) AS inquiry,SUM(CASE WHEN servicetype_id=4 THEN 1 ELSE 0 END) AS proposal FROM servicerecords GROUP BY DATE(createtime)";
		$username = $_SESSION["user_info"]["username"];
		if($username == "admin"){
			$sql = "SELECT DATE(createtime) as date,SUM(CASE WHEN servicetype_id=1 THEN 1 ELSE 0 END) AS consulting,SUM(CASE WHEN servicetype_id=2 THEN 1 ELSE 0 END) AS complaints,SUM(CASE WHEN servicetype_id=3 THEN 1 ELSE 0 END) AS inquiry,SUM(CASE WHEN servicetype_id=4 THEN 1 ELSE 0 END) AS proposal FROM servicerecords WHERE createtime LIKE '%$year_month%' GROUP BY DATE(createtime)";
		}else{
			$sql = "SELECT DATE(createtime) as date,SUM(CASE WHEN servicetype_id=1 THEN 1 ELSE 0 END) AS consulting,SUM(CASE WHEN servicetype_id=2 THEN 1 ELSE 0 END) AS complaints,SUM(CASE WHEN servicetype_id=3 THEN 1 ELSE 0 END) AS inquiry,SUM(CASE WHEN servicetype_id=4 THEN 1 ELSE 0 END) AS proposal FROM servicerecords WHERE createtime LIKE '%$year_month%' AND seat='$username'  GROUP BY DATE(createtime)";
		}


		$serviceData = $servicerecords->query($sql,$parse=true);
		//dump($serviceData);die;
		$timestamp = strtotime($year_month . "-10 00:00:00");
		$days = date('t',$timestamp);
		$arrLAST = Array();
		for($i=0;$i<$days;$i++){
			if( $i< 9){
				$day = "0" . ($i+1);
			}else{
				$day = $i+1;
			}
			$current_day = $year_month .'-' .$day;
			$arrLAST[$i] = Array(
				'date' => $current_day,
				"consulting" => '0',
				"complaints" => '0',
				"inquiry" => '0',
				"proposal" => '0',
				"resciveday" => $i+1,
			);
		}
		//dump($arrLAST);die;
		foreach($serviceData AS $v){
			$arrTemp = explode('-',$v['date']);
			$index = (int)($arrTemp[2]);
			//echo "---------$index---</br>";
			$arrLAST[$index-1] = $v;
		}
		//dump($arrLAST);die;

		foreach($arrLAST as $vm){
			$date[] = $vm["date"];
			$consulting[] = $vm["consulting"];
			$complaints[] = $vm["complaints"];
			$inquiry[] = $vm["inquiry"];
			$proposal[] = $vm["proposal"];
			$resciveday[] = $vm["resciveday"];
		}
		$shijian = "'".implode("','",$date)."'";
		$resciveday = "'".implode("','",$resciveday)."'";
		$zx = implode(",",$consulting);
		$ts = implode(",",$complaints);
		$cx = implode(",",$inquiry);
		$jy = implode(",",$proposal);
		$sj = implode(",",$resciveday);
		//dump($resciveday);die;
		$count = count($arrLAST);


		$thisyear = date('Y');
		$currentYearMonth = date('Y-m');
		$yearmonth = Array(
			$thisyear .'-01',
			$thisyear .'-02',
			$thisyear .'-03',
			$thisyear .'-04',
			$thisyear .'-05',
			$thisyear .'-06',
			$thisyear .'-07',
			$thisyear .'-08',
			$thisyear .'-09',
			$thisyear .'-10',
			$thisyear .'-11',
			$thisyear .'-12',
		);
		$tpl_yearmonth = Array();
		for($i=0;$i<=12;$i++){
			$tpl_yearmonth[$i] = Array(
				'value' => $yearmonth[$i],
				'selected' => $yearmonth[$i]==$currentYearMonth?'selected="selected"':'',
				'display' => $yearmonth[$i],
			);
		}
		unset($i);


		//dump($yearmonth);
		//$this->assign("option",$option);
		$this->assign("yearmonth",$tpl_yearmonth);
		$this->assign("currentYearMonth",$currentYearMonth);
		$this->assign("shijian",$shijian);
		$this->assign("sj",$sj);
		$this->assign("zx",$zx);
		$this->assign("ts",$ts);
		$this->assign("cx",$cx);
		$this->assign("jy",$jy);
		$this->assign("time",date("Y-m"));

		$this->assign("arrLAST",$arrLAST);
		//dump($arrLAST);die;

		$types = $_REQUEST["types"];
		if($types){
			$this->assign("types","Y");
		}else{
			$this->assign("types","N");
		}

		$this->display();
	}




	function serciceReportData(){
		$types = $_REQUEST["types"];
		$servicerecords = new Model("servicerecords");

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$createtime = $_REQUEST["createtime"];
		if($createtime){
			$year_month = $_REQUEST["createtime"];
		}else{
			$year_month = date("Y-m");
		}

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";

		if($username != "admin"){
			if($types == "Y"){
				$where = " AND seat in ($deptUser)";
			}else{
				$where =  "AND seat='$username' ";
			}
		}else{
			$where = " ";
		}

		$username = $_SESSION["user_info"]["username"];
		if($username == "admin"){
			$sql = "SELECT DATE(createtime) as date,SUM(CASE WHEN servicetype_id=1 THEN 1 ELSE 0 END) AS consulting,SUM(CASE WHEN servicetype_id=2 THEN 1 ELSE 0 END) AS complaints,SUM(CASE WHEN servicetype_id=3 THEN 1 ELSE 0 END) AS inquiry,SUM(CASE WHEN servicetype_id=4 THEN 1 ELSE 0 END) AS proposal FROM servicerecords WHERE createtime LIKE '%$year_month%' GROUP BY DATE(createtime)";
		}else{
			$sql = "SELECT DATE(createtime) as date,SUM(CASE WHEN servicetype_id=1 THEN 1 ELSE 0 END) AS consulting,SUM(CASE WHEN servicetype_id=2 THEN 1 ELSE 0 END) AS complaints,SUM(CASE WHEN servicetype_id=3 THEN 1 ELSE 0 END) AS inquiry,SUM(CASE WHEN servicetype_id=4 THEN 1 ELSE 0 END) AS proposal FROM servicerecords WHERE createtime LIKE '%$year_month%' $where GROUP BY DATE(createtime)";
		}

		$serviceData = $servicerecords->query($sql);
		//echo $servicerecords->getLastSql();die;


		//dump($year_month);die;
		$timestamp = strtotime($year_month . "-10 00:00:00");
		$days = date('t',$timestamp);
		$arrLAST = Array();
		for($i=0;$i<$days;$i++){
			if( $i< 10){
				$day = "0" . ($i+1);
			}else{
				$day = $i+1;
			}
			$current_day = $year_month .'-' .$day;
			$arrLAST[$i] = Array(
				'date' => $current_day,
				"consulting" => '0',
				"complaints" => '0',
				"inquiry" => '0',
				"proposal" => '0',
				"resciveday" => $i+1,
			);
		}
		//dump($arrLAST);die;
		foreach($serviceData AS $v){
			$arrTemp = explode('-',$v['date']);
			$index = (int)($arrTemp[2]);
			//echo "---------$index---</br>";
			$arrLAST[$index-1] = $v;
		}
		//dump($arrLAST);

		$count = count($arrLAST);

		$rowsList = count($arrLAST) ? $arrLAST : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);

	}

	function serciceReport(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order reports";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		$year_month = date("Y-m");


		$types = $_REQUEST["types"];
		if($types){
			$this->assign("types","Y");
		}else{
			$this->assign("types","N");
		}

		$month = date("Y-m");
		$this->assign("month",$month);

		$arrF = $this->tableFields();
		$i = 0;
		$fields = "";
		foreach($arrF as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
			$arrTF[] = $val["fields"];
			$arr_fields[] = array(
				"fields" => $val["fields"],
				"display" => $val["display"],
				"color" => $val["color"],
			);
			if($val["color"]){
				$arrColor[] = $val["color"];
			}

			$arrField[0][$i]['field'] = $val['fields'];
			$arrField[0][$i]['title'] = $val['display'];
			$arrField[0][$i]['width'] = "100";
			$i++;
		}
		//dump($arrF);
		$arrFd = array("field"=>"date","title"=>"日期","width"=>"130");
		array_unshift($arrField[0],$arrFd);

		$arrFs = json_encode($arrField);
		$this->assign("fieldList",$arrFs);
		$this->assign("arr_fields",$arr_fields);

		$str_color = "'".implode("','",$arrColor)."'";
		$this->assign("str_color",$str_color);
		//dump($str_color);die;



		$this->display();
	}

	function tableFields(){
		$service = new Model("servicetype");
		$arrData = $service->field("id as value,servicename as display,color")->select();

		$i = 1;
		foreach($arrData as &$val){
			$val["fields"] = "servicetype_".$i;
			$val["name"] = "servicetype_id";
			$i++;
		}
		unset($i);
		//dump($arrData);die;
		return $arrData;
	}



	function serciceReportData2(){
		header("Content-Type:text/html; charset=utf-8");
		$types = $_REQUEST["types"];
		$search_type = $_REQUEST["search_type"];
		$servicerecords = new Model("servicerecords");

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$month = $_REQUEST["month"];
		if($month){
			$year_month = $_REQUEST["month"];
		}else{
			$year_month = date("Y-m");
		}

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";
		//echo $deptUser;die;

		if($username != "admin"){
			if($types == "Y"){
				$where = " AND seat in ($deptUser)";
			}else{
				$where =  "AND seat='$username' ";
			}
		}else{
			$where = " ";
		}

		$fields = "";
		$field_arr = "";
		$arrField = array("date");
		$arrTitle = array("日期");
		$arrF = $this->tableFields();
		foreach($arrF as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
			$str2 = "'".$val["fields"]."' => '0'";
			$field_arr .= empty($field_arr)?"$str2":",$str2";
			$arrFT[] = $val["fields"];

			$arrTF[] = $val["fields"];
			$arrTF2[] = $val["fields"]."[]";
			$arr_fields[] = array(
				"fields" => $val["fields"],
				"display" => $val["display"],
				"color" => $val["color"],
			);

			$arrField[] = $val["fields"];
			$arrTitle[] = $val["display"];
		}

		//dump($field_arr);die;

		if($username == "admin"){
			$sql = "SELECT $fields,DATE(createtime) as date FROM servicerecords WHERE createtime LIKE '%$year_month%' GROUP BY DATE(createtime)";
		}else{
			$sql = "SELECT $fields,DATE(createtime) as date FROM servicerecords WHERE createtime LIKE '%$year_month%' $where GROUP BY DATE(createtime)";
		}

		$serviceData = $servicerecords->query($sql);
		//echo $servicerecords->getLastSql();die;


		//dump($year_month);die;
		$timestamp = strtotime($year_month . "-10 00:00:00");
		$days = date('t',$timestamp);
		$arrLAST = Array();
		for($i=0;$i<$days;$i++){
			if( $i< 9){
				$day = "0" . ($i+1);
			}else{
				$day = $i+1;
			}
			$current_day = $year_month .'-' .$day;
			$arrLAST[$i] = Array(
				'date' => $current_day,
				"resciveday" => $i+1,
			);
		}
		//添加servicetype_*字段
		foreach($arrLAST as &$val){
			foreach($arrTF as $vm){
				$val[$vm] = "0";
			}
		}

		//dump($arrLAST);die;
		foreach($serviceData AS $v){
			$arrTemp = explode('-',$v['date']);
			$index = (int)($arrTemp[2]);
			//echo "---------$index---</br>";
			$arrLAST[$index-1] = $v;
		}


		//=========页脚
		foreach($arr_fields as &$val){
			foreach($arrLAST as &$vm){
				$arrFoot[$val["fields"]][] = $vm[$val["fields"]];
				$tt[] = $vm[$val["fields"]];
			}
		}

		foreach($arr_fields as &$val){
			$arrSum[$val["fields"]] = array_sum($arrFoot[$val["fields"]]);
		}
		$arrSum["date"] = "总计";
		$footer = array($arrSum);

		if($search_type == "xls"){
			$xls_count = count($arrField);
			$excelTiele = "服务工单报表".date("Y-m-d");
			array_push($arrLAST,$arrSum);
			//dump($arrData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$arrLAST,$excelTiele);
			die;
		}

		$count = count($arrLAST);

		$rowsList = count($arrLAST) ? $arrLAST : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["arr_fields"] = $arr_fields;
		$ary["arrField"] = $arrTF;
		$ary["footer"] = $footer;

		echo json_encode($ary);

	}


	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }




	function serciceAgentReport(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		checkLogin();
		//分配增删改的权限
		$menuname = "Work Order Agent Reports";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);
		$year_month = date("Y-m");


		$types = $_REQUEST["types"];
		if($types){
			$this->assign("types","Y");
		}else{
			$this->assign("types","N");
		}

		$month = date("Y-m");
		$this->assign("month",$month);

		$arrF = $this->tableFields();
		$i = 0;
		$fields = "";
		foreach($arrF as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
			$arrTF[] = $val["fields"];
			$arr_fields[] = array(
				"fields" => $val["fields"],
				"display" => $val["display"],
				"color" => $val["color"],
			);
			if($val["color"]){
				$arrColor[] = $val["color"];
			}

			$arrField[0][$i]['field'] = $val['fields'];
			$arrField[0][$i]['title'] = $val['display'];
			$arrField[0][$i]['width'] = "100";
			$i++;
		}
		//dump($arrF);
		$arrFd = array("field"=>"seat","title"=>"工号","width"=>"80");
		$arrFd2 = array("field"=>"cn_name","title"=>"姓名","width"=>"80");
		array_unshift($arrField[0],$arrFd2);
		array_unshift($arrField[0],$arrFd);

		$arrFs = json_encode($arrField);
		$this->assign("fieldList",$arrFs);
		$this->assign("arr_fields",$arr_fields);

		$str_color = "'".implode("','",$arrColor)."'";
		$this->assign("str_color",$str_color);
		//dump($str_color);die;



		$this->display();
	}


	function serciceAgentReportData(){
		header("Content-Type:text/html; charset=utf-8");
		$types = $_REQUEST["types"];
		$search_type = $_REQUEST["search_type"];
		$servicerecords = new Model("servicerecords");

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";
		//echo $deptUser;die;

		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_end = Date('Y-m-d',$_GET['ts_end']) ." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){//上个月的起始时间
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1)) ." 00:00:00";
			}else{
				$date_start = Date('Y-m-d',$_GET['ts_start']) ." 00:00:00";
			}
		}else{
			$date_start = $_REQUEST["start_time"];
			$date_end = $_REQUEST["end_time"];
		}
		if(! $date_end){$date_end = Date('Y-m-d H:i:s');}

		$where = "1 ";
		if($username != "admin"){
			if($types == "Y"){
				$where .= " AND seat in ($deptUser)";
			}else{
				$where .=  "AND seat='$username' ";
			}
		}
		$nowdate = date("Y-m-d")." 00:00:00";
		$where .= empty($date_start) ? " AND createtime >= '$nowdate'" : " AND createtime >= '$date_start'";
		$where .= empty($date_end) ? "" : " AND createtime <= '$date_end'";

		$fields = "";
		$field_arr = "";
		$arrField = array("seat","cn_name");
		$arrTitle = array("工号","姓名");
		$arrF = $this->tableFields();
		foreach($arrF as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
			$str2 = "'".$val["fields"]."' => '0'";
			$field_arr .= empty($field_arr)?"$str2":",$str2";
			$arrFT[] = $val["fields"];

			$arrTF[] = $val["fields"];
			$arrTF2[] = $val["fields"]."[]";
			$arr_fields[] = array(
				"fields" => $val["fields"],
				"display" => $val["display"],
				"color" => $val["color"],
			);

			$arrField[] = $val["fields"];
			$arrTitle[] = $val["display"];

		}

		//dump($field_arr);die;

		//$sql = "SELECT $fields,seat FROM servicerecords WHERE  $where GROUP BY seat";
		//$serviceData = $servicerecords->query($sql);

		$arrCount = $servicerecords->Distinct(true)->field('seat')->select();
		$count = count($arrCount);
		if($search_type == "xls"){
			$serviceData = $servicerecords->field("$fields,seat")->where($where)->group("seat")->select();
		}else{
			$serviceData = $servicerecords->field("$fields,seat")->limit($page->firstRow.','.$page->listRows)->where($where)->group("seat")->select();
		}
		//echo $servicerecords->getLastSql();
		//dump($serviceData);die;

		//=========页脚
		$userArr = readU();
		$cnName = $userArr["cn_user"];
		foreach($arr_fields as &$val){
			foreach($serviceData as &$vm){
				$arrFoot[$val["fields"]][] = $vm[$val["fields"]];
				$tt[] = $vm[$val["fields"]];

				$vm["cn_name"] = $cnName[$vm["seat"]];
			}
		}

		foreach($arr_fields as &$val){
			$arrSum[$val["fields"]] = array_sum($arrFoot[$val["fields"]]);
		}
		$arrSum["seat"] = "本页总计";
		$footer = array($arrSum);


		if($search_type == "xls"){
			$xls_count = count($arrField);
			$excelTiele = "服务工单坐席报表".date("Y-m-d");
			array_push($serviceData,$arrSum);
			//dump($arrField);
			//dump($serviceData);die;
			exportDataFunction($xls_count,$arrField,$arrTitle,$serviceData,$excelTiele);
			die;
		}

		$rowsList = count($serviceData) ? $serviceData : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		$ary["arr_fields"] = $arr_fields;
		$ary["arrField"] = $arrTF;
		$ary["footer"] = $footer;
		if($date_start){
			$ary["date_start"] = $date_start;
		}else{
			$ary["date_start"] = $nowdate;
		}
		if($date_end){
			$ary["date_end"] = $date_end;
		}

		echo json_encode($ary);

	}

}
?>
