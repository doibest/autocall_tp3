<?php
class ReceptionWaitMatterAction extends Action{
	function  waitmatterAll(){
		checkLogin();
		$this->display();
	}

	function updateWait(){
		$id = $_POST["id"];
		//dump($id);
		$wait = new Model("waitmatter");
		$arrData = array(
			'status' =>$_POST['status'],
		);
		$result = $wait->where("id = $id")->save($arrData);
		if($result !== false){
			//goback("修改成功","agent.php?m=Index&a=panelDisplay");
			goback("","agent.php?m=Index&a=index");
		}
	}

	function waitmatterContent(){
		checkLogin();
		$id = $_GET['id'];
		$wait = new Model("waitmatter");
		if($id){
			$this->assign("id",$id);
			$wmList = $wait->where("id = $id")->find();
			$url = $wmList["url"];
			if($url){
				$this->assign("url","url");
			}
			$this->assign("wmList",$wmList);
			$matterstype = $wmList["matterstype"];
			$status = $wmList["status"];
			$this->assign("matterstype",$matterstype);
			$this->assign("stas",$status);
		}
		$this->display();
	}

	function  waitmatterAllList(){
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$username = $_SESSION["user_info"]["username"];
		$wait = new Model("waitmatter");
		import('ORG.Util.Page');
		$count = $wait->where("username = '$username' ")->count();

        $_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$waitList = $wait->order("createtime desc")->table("waitmatter w")->field("w.id,w.createtime,w.username,w.status,w.remindertime,w.title,w.url,t.waitmattername")->join("waitmattertype t on w.matterstype = t.id")->limit($page->firstRow.','.$page->listRows)->where("username = '$username' ")->select();

		//$row = array('0'=>'pbx事项','1'=>'系统事项','2'=>'站内事项','3'=>'工单查询');
		//$nn = '<a style="display: inline;" class="editable editable-click" href="#" id="status" data-type="select" data-pk="1" data-original-title="Enter status">'.'已办'.'</a>';

		$status_row = array('Y'=>'<span style="color:red;">已办</span>','N'=>'未办');
		$i =0;
		foreach($waitList as &$val){
			//$type = $row[$val['matterstype']];
			//$val['matterstype'] = $type;
			$status = $status_row[$val["status"]];
			$val["status"] = $status;
			$waitList[$i]["title"] = "<a target='_blank' style='color:#0066CC' href='".$val['url']."'>". $val["title"] ."</a>";
			$i++;
		}


		//dump($waitList);
		$rowsList = count($waitList) ? $waitList : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
	}


	function waitUpdateData(){
		$fields = explode('_',$this->_post("name"));
		$ary = array($fields[0]=>$this->_post("value"));
		$wait = new Model("waitmatter");
		$count = $wait->where("id=".$fields[1])->save($ary);
	}

}

?>
