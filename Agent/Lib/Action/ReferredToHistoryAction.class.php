<?php
class ReferredToHistoryAction extends Action{
	function forwardedList(){
		checkLogin();
		$id = $_GET["id"];
		$this->assign("cid",$id);
		$calldate_start = $_REQUEST['calldate_start'];
        $calldate_end = $_REQUEST['calldate_end'];
        $name = $_REQUEST['name'];
        $recipient = $_REQUEST['recipient'];
        $forwardedname = $_REQUEST['forwardedname'];
        $noticetype = $_REQUEST['noticetype'];
        $modifycontent = $_REQUEST['modifycontent'];
        $cid = $_REQUEST['cid'];
        $searchmethod = isset($_REQUEST['searchmethod'])?$_REQUEST['searchmethod']:"equal";


		$where = "1 ";
        $where .= empty($calldate_start)?"":" AND createtime >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND createtime <'$calldate_end'";
        if( $searchmethod == "equal"){
            $where .= empty($name)?"":" AND name ='$name'";
            $where .= empty($recipient)?"":" AND recipient ='$recipient'";
            $where .= empty($forwardedname)?"":" AND forwardedname  ='$forwardedname'";
            $where .= empty($noticetype)?"":" AND noticetype ='$noticetype'";
            $where .= empty($modifycontent)?"":" AND modifycontent ='$modifycontent'";
        }else{
            $where .= empty($name)?"":" AND name like '%$name%'";
            $where .= empty($recipient)?"":" AND recipient like '%$recipient%'";
            $where .= empty($forwardedname)?"":" AND forwardedname  like '%$forwardedname%'";
            $where .= empty($noticetype)?"":" AND noticetype like '%$noticetype%'";
            $where .= empty($modifycontent)?"":" AND modifycontent like '%$modifycontent%'";
        }
		//$where .= empty($cid)?"":" AND customerid ='$cid'";
		if($cid){
			$where .= " AND customerid = $cid";
		}else{
			$where .= " AND customerid = $id";
		}

		//dump($where);
		$process = new Model("process");
		import('ORG.Util.Page');
		$count = $process->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$processData = $process->order("p.createtime desc")->field("c.name,p.createtime,p.recipient,p.forwardedname,p.noticetype,p.modifycontent")->table("`process` p")->join("customer c on p.customerid = c.id")->join("servicerecords s on p.service_id = s.id ")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		//dump($processData);

		//$processData = $process->order("createtime desc")->field("c.name as name,p.createtime as createtime,p.recipient as recipient,p.forwardedname as forwardedname,p.noticetype as noticetype,p.modifycontent as modifycontent")->table("`process` p")->join("customer c on p.customerid = c.id")->join("servicerecords s on p.service_id = s.id ")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $process->getLastSql();die;

		$rowsList = count($processData) ? $processData : false;
		$arrProcess["total"] = $count;
		$arrProcess["rows"] = $rowsList;
		$strTmp = json_encode($arrProcess);
		$this->assign("strTmp",$strTmp);

		$this->display();
	}
}

?>
