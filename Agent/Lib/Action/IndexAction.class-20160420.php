<?php

class IndexAction extends Action {

    function index(){

		//检测是否有登录
		checkLogin();

		/*
		if($_GET["l"]){
			setcookie("lang",$_GET["l"]);
			setcookie("think_language",$_GET["l"]);
		}else{
			//dump($_COOKIE);die;
			setcookie("lang",'zh_CN');
			setcookie("think_language",'zh_CN');
		}
		*/
		$para_sys = readS();
		setcookie("lang",$para_sys["language"]);
		setcookie("think_language",$para_sys["language"]);


		$today = date("Y年m月d日",time());
		$this->assign("user_info",$_SESSION["user_info"]);
		$this->assign("today",$today);
		$this->assign("serverIP",$_SERVER["SERVER_ADDR"]);

		//求出初始化登陆进去的状态
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$arrHint = $bmi->getHint($_SESSION["user_info"]['extension']);
		$DND_status = $bmi->getDNDAll();
		$dnd = $DND_status[$_SESSION["user_info"]['extension']];
		if($dnd){
			$this->assign("hint","PutDND");
		}else{
			$this->assign("hint",$arrHint["stat"]);
		}
		//dump($dnd);die;
		//dump($arrHint["stat"]);die;
		//dump($_SESSION);die;
		//$this->assign("isPaused",$_SESSION["user_info"]['servicepause']);

		$this->assign("system",$para_sys);

		$logo_replace = new Model("logo_replace");
		$logo = $logo_replace->where("id = '2'")->find();
		$this->assign("logo",$logo['logoname']);

		//dump($_COOKIE);die;
		//从数据库中读取菜单
		$tpl_menu = Array();
		$menu = new Model('menu');
		$mainMenu = $menu->table("menu me")->field("me.id AS id,name,me.icon,moduleclass,order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=0 AND enabled='Y'")->order("`order` ASC")->select();
		//dump($mainMenu);die;

		$_SESSION['menu'] = Array();//存放子菜单的父菜单，为以后判断子菜单的权限的方便
		foreach( $mainMenu AS $row ){
			$mainId = $row['id'];
			//$subMenu = $menu->where("pid=$mainId")->order("`order` ASC")->select();
			$subMenu = $menu->table("menu me")->field("me.id AS id,me.icon as iconCls,name,moduleclass, order,url,me.pid AS pid")->join("left join module mo on me.moduleclass=mo.modulename")->where("me.pid=$mainId AND enabled='Y'")->order("`order` ASC")->select();
			if( 'admin'==$_SESSION["user_info"]['username'] ){//如果是管理员，默认显示所有模块加载的菜单
				$tpl_menu[$row['name']]['thisMenu'] = $row['name'];
				$tpl_menu[$row['name']]['iconCls'] = $row['icon'];
				$tpl_menu[$row['name']]['subMenu'] = $subMenu;


				foreach( $subMenu AS $arrRow ){
					if( array_key_exists($arrRow['name'],$_SESSION["user_priv"][$row['name']]) ){
						$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
						$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
					}
				}


			}else{//非管理员
				//$_SESSION["user_priv"]是权限数组
				//dump($_SESSION["user_priv"]);die;

				if( array_key_exists($row['name'],$_SESSION["user_priv"]) ){
					$tpl_menu[$row['name']]['thisMenu'] = $row['name'];//一级菜单应该显示
					$tpl_menu[$row['name']]['iconCls'] = $row['icon'];//一级菜单应该显示
					//下面查询一级菜单下面的二级菜单有哪些应该显示

					foreach( $subMenu AS $arrRow ){
						if( array_key_exists($arrRow['name'],$_SESSION["user_priv"][$row['name']]) ){
							$tpl_menu[$row['name']]['subMenu'][] = $arrRow;
							$_SESSION['menu'][$arrRow['name']] = $row['name'];//子菜单始终指向父菜单
						}
					}
				}
			}
		}
		$this->assign("menu",$tpl_menu);
		//dump($tpl_menu);die;


		$menuname = "Seating monitoring";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("agent_manage",$priv['agent_manage']);
		//dump($priv['agent_manage']);die;



		$checkRole = getSysinfo();
		/*
		if( strstr($checkRole[2],"wechat") ){
			$checkRole[2] = trim(str_replace(",wechat","",$checkRole[2]));
			$this->assign("wechat","Y");
		}else{
			$this->assign("wechat","N");
		}
		if( $checkRole[2] == "inbound" ){
			$this->assign("sys_type","inbound");
		}elseif( $checkRole[2] == "outbound" ){
			$this->assign("sys_type","outbound");
		}elseif( $checkRole[2] == "outbound,inbound" ){
			$this->assign("sys_type","all");
		}elseif( $checkRole[2] == "IPPBX" ){
			$this->assign("sys_type","IPPBX");
		}
		*/

		$arrAL = explode(",",$checkRole[2]);
		if( in_array("inbound",$arrAL) ){
			$wj_in = "Y";
		}else{
			$wj_in = "N";
		}
		if( in_array("outbound",$arrAL) ){
			$wj_out = "Y";
		}else{
			$wj_out = "N";
		}
		if( in_array("IPPBX",$arrAL) ){
			$wj_IPPBX = "Y";
		}else{
			$wj_IPPBXt = "N";
		}
		if($wj_in == "Y" && $wj_out == "Y"){
			$sys_type = "all";
		}elseif($wj_in == "Y"){
			$sys_type = "inbound";
		}elseif($wj_out == "Y"){
			$sys_type = "outbound";
		}elseif($wj_IPPBX == "Y"){
			$sys_type = "IPPBX";
		}
		$this->assign("sys_type",$sys_type);


		if( in_array("wf",$arrAL) ){
			$this->assign("Workflow","Y");
		}else{
			$this->assign("Workflow","N");
		}

		if( in_array("ks",$arrAL) ){
			$this->assign("ExamTraining","Y");
		}else{
			$this->assign("ExamTraining","N");
		}


		$order_info = M("order_info");
		$start_time1 = date("Y-m-d")." 00:00:00";
		$end_time1 = date("Y-m-d")." 23:59:59";
		$where_order = "createtime>='$start_time1' AND createtime<='$end_time1'";
		$arrOrderData = $order_info->order("sum(goods_amount) desc")->field("createname,sum(goods_amount) as goods_amount,count(*) as order_number")->where($where_order)->group("createname")->limit("3")->select();
		$i = 1;
		foreach($arrOrderData as &$val){
			$val["order_sort"] = $i;
			$i++;
		}
		//echo $order_info->getLastSql();die;
		$this->assign("arrOrderData",$arrOrderData);



		$this->display();

	}

	function extenStat(){
		//求出初始化登陆进去的状态
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$bmi->loadAGI();
		$arrHint = $bmi->getHint($_SESSION["user_info"]['extension']);
		$DND_status = $bmi->getDNDAll();
		$dnd = $DND_status[$_SESSION["user_info"]['extension']];
		if($arrHint["stat"] != "InUse"){
			if($dnd){
				$arrHint["stat"] = "PutDND";
			}
		}
		//dump($arrHint);die;
		echo json_encode(array('success'=>true,'stauts'=>$arrHint["stat"]));
	}

	//心跳同步，检测用户在线。顺便把用户待办事项也查询出来
	function onlineCheck(){
		//求出初始化登陆进去的状态
		$username = $_SESSION['user_info']['username'];
		$task_id = $_SESSION['outbound_current_task']['id'];

		$now = Date('Y-m-d H:i:s');
		$table = M('report_agent_login');
		$arrTmp = $table->where("username='$username' AND status='登录中'")->find();
		$logintime = $arrTmp['logintime'];
		if( $arrTmp ){
			$arr = Array(
				'lastheartbeat'	=> $now,
				'last_task_id'	=>	$task_id,
				'during'	=>	strtotime($now) - strtotime($logintime),
			);
			$result = $table->where("username='$username' AND logintime='$logintime'")->data($arr)->save();

			$users = new Model("users");
			$users->where("username='$username'")->save(array("login_status"=>"Y"));
			$this->loginUserCaChe();
		}


		//待办事项---------提前30分钟提醒用户用哪些事情急需办理
		//$username = $_POSt['username'];
		$waitmatter = M('waitmatter');
		$arr = $waitmatter->field("username,remindertime,title,content")->where("username='$username' AND remindertime >= '$now'")->select();
		//dump($waitmatter->getLastSql());die;
		//dump($arr);

		foreach($arr as $val){
			$minuter = ceil( (strtotime($val['remindertime'])-strtotime($now))/60 );
			if( $minuter >=0 && $minuter <=30 ){
				$arrTmp[] = array(
					'title'	=> $val['title'],
					'remindertime'	=> $val['remindertime'],
					'content'	=> $val['content'],
				);
				$message[] = "办理时间: ".$val['remindertime']."<br>事项标题: ".$val['title']."<br> 事项内容：".strip_tags($val['content'],'<a>')."<br>";
			}
		}
		$msg = implode("<br>",$message);
		echo json_encode(array('success'=>true,'msg'=>$msg));
	}

	function loginUserCaChe(){
		$users = new Model("users");
		$arrData = $users->field("username,login_status")->where("login_status='Y'")->select();
		foreach($arrData as $val){
			$tmp[] = $val["username"];
		}
		F('loginUser',$tmp,"BGCC/Conf/");
	}

	function extenStatus(){
		import('ORG.Util.Page');
		import('ORG.Pbx.bmi');

		$bmi = new bmi();
		$extension_status = $bmi->getHints();   //获取分机状态

		$cf_status = $bmi->getCFAll("cf");	 //转接
		$DND_status = $bmi->getDNDAll();
		//$channels = $bmi->getAllChannelsInfo("CallerID");
        $users=new Model("Users");
        $list = $users->field("username,extension")->where("extension <> ' '")->select();
		//echo $users->getLastSql();
		$i = 0;
		foreach( $list As $val ){
			$ext = $val["extension"];
			$list[$i]['stat'] = $extension_status[$ext]['stat'];
			//$list[$i]['Billsec'] = $channels[$ext]['Duration'];
			$dn = $DND_status[$ext];
			if($dn){
				$list[$i]['stat'] = 'DND';
			}
			$cf_tpl = $cf_status[$ext];
			if($cf_tpl){
				$list[$i]['stat'] = 'cf';
			}
			$i++;

		}
		unset($i);
		dump($list);die;

	}

	function colseBrowser(){
		$username = $_REQUEST["user"];
		$users = new Model("users");
		$users->where("username='$username'")->save(array("login_status"=>"N"));
		$this->loginUserCaChe();
	}

	function visitRemind(){
		$visit = $_POST['visitData'];
		$para_sys = readS();

		$sales_task = new Model("sales_task");
		$arrData = $sales_task->field("id,name")->select();

		foreach($arrData as $val){
			$tmp[$val["id"]] = $val["name"];
		}

		foreach($visit as $k=>$v){
			if($v["visit_type"] == "in"){
				$cid[] = $v['customer_id'];
				$vData[$v['customer_id']] = $v;
			}else{
				if($para_sys["windowOpen_hide"] == "Y"){
					$v['phone_hide'] = substr($v["phone"],0,3)."***".substr($v["phone"],-4);
				}else{
					$v['phone_hide'] = $v["phone"];
				}
				$outbound_message[] = "回访时间：".$v['visit_time']."<span style='padding-right:12px'></span> <a  target='_blank' href='agent.php?m=Telemarketing&a=editWCustomer&calltype=preview&task_id=".$v['task_id']."&phone_num=".$v['phone']."&id=".$v['customer_id']."&visit_id=".$v['id']."'>任务名称  ".$tmp[$v['task_id']]."</a> <span style='padding-right:12px'></span> 电话：".$v['phone_hide'];
			}
			$tid[] = $v['task_id'];
		}

		if(!$outbound_message){
			$outbound_message = array();
		}

		$id = implode(",",$cid);
		$task_id = $tid[0];
		$customer = new Model("customer");
		$customerData = $customer->field("id as cid,name,phone1,phone2")->where("id in ($id)")->select();

		foreach($customerData as $k=>$v){
			$cmData[$v['cid']] = $v;
		}
		foreach($cmData as $key=>$value) {
			foreach($value as $k=>$v) {
			  $vData[$key][$k] = $v;
			}
		}
		foreach($vData as $val){
			if($para_sys["windowOpen_hide"] == "Y"){
				$val['phone1'] = substr($val["phone1"],0,3)."***".substr($val["phone1"],-4);
			}

			$message[] = "回访时间：".$val['visit_time']."<span style='padding-right:12px'></span> <a  target='_blank' href='agent.php?m=CustomerData&a=editCustomer&id=".$val['customer_id']."&visit_id=".$val['id']."'>姓名：".$val['name']."</a> <span style='padding-right:12px'></span> 电话：".$val['phone1'];
		}
		if(!$message){
			$message = array();
		}
		$msg_tmp = array_merge($message,$outbound_message);
		$msg = implode("<br>",$msg_tmp);
		echo json_encode(array('success'=>true,'msg'=>$msg));

	}

	function visitOutboundRemind(){
		$visit = $_POST['visitData'];
		$sales_task = new Model("sales_task");
		$arrData = $sales_task->field("id,name")->select();

		foreach($arrData as $val){
			$tmp[$val["id"]] = $val["name"];
		}
		foreach($visit as $val){
			if($val["visit_type"] == "out"){
				$message[] = "回访时间：".$val['visit_time']."<span style='padding-right:12px'></span> <a  target='_blank' href='agent.php?m=Telemarketing&a=editWCustomer&calltype=preview&task_id=".$val['task_id']."&phone_num=".$val['phone']."&id=".$val['customer_id']."&visit_id=".$val['id']."'>任务名称  ".$tmp[$val['task_id']]."</a> <span style='padding-right:12px'></span> 电话：".$val['phone'];
			}
		}
		$msg = implode("<br>",$message);
		echo json_encode(array('success'=>true,'msg'=>$msg));
	}



	function panelDisplay(){
		checkLogin();
		$para_sys = readS();
		$no_visit = $para_sys["no_visit"];
		$this->assign("no_visit",$no_visit);

		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$username = $_SESSION["user_info"]["username"];
		$extension = $_SESSION["user_info"]["extension"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$r_id = $_SESSION["user_info"]["r_id"];
		//dump($_SESSION["user_info"]);die;

		$date = date("Y-m-d H:i:s");
		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$now = date("Y-m-d H:i:s");

		//条件
		$where_wait = "status ='N' ";
		$where_visit = "visit_time<='$end_time' AND visit_time>='$start_time' AND visit_status = 'N' AND visit_type='in' ";
		$where_visit2 = "visit_time<='$end_time' AND visit_time>='$start_time' AND visit_status = 'N' AND visit_type='out' ";
		$where_gg = "endtime >='$start_time'  AND starttime <= '$end_time' AND an_enabeld = 'Y' AND starttime < '$now'";
		$where_noVisit = "1 ";
		$where_noVisit .= " AND (TIMESTAMPDIFF(DAY,'$date',recently_visittime) >= '$no_visit' OR TIMESTAMPDIFF(DAY,recently_visittime,'$date') >= '$no_visit' ) ";
		$where_purchase = "TIMESTAMPDIFF(DAY,'$date',o.purchase_time) <= '$no_visit' AND o.purchase_time > '$date'";
		$where_order_status = "view_enable = 'N' AND order_status in (1,2,4,7,8)";
		$where_hotline = "1 ";

		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$d_id = $_SESSION['user_info']['d_id'];
				$arrDep = $this->getDepTreeArray();
				$deptst = $this->getMeAndSubDeptName12($arrDep,$d_id);
				$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
				$userArr = readU();
				$deptUser2 = "";
				foreach($deptSet as $val){
					$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
				}
				$deptUser = rtrim($deptUser2,",'',")."'";

				$where_wait .= " AND username in ($deptUser)" ;
				$where_visit .= " AND visit_name in ($deptUser) ";
				$where_visit2 .= " AND visit_name in ($deptUser) ";
				$where_gg .= " AND find_in_set('$d_id',department_id) ";
				$where_noVisit .= " AND createuser in ($deptUser) ";
				$where_purchase .= " AND o.createname in ($deptUser) ";
				$where_order_status .= " AND createname in ($deptUser) ";
			}else{
				$where_wait .= " AND username = '$username'";
				$where_visit .= " AND visit_name = '$username'";
				$where_visit2 .= " AND visit_name = '$username'";
				$where_gg .= " AND find_in_set('$d_id',department_id) ";
				$where_noVisit .= " AND createuser = '$username' ";
				$where_purchase .= " AND o.createname = '$username' ";
				$where_order_status .= " AND createname = '$username' ";
				$where_hotline .= " AND create_user = '$username' AND visit_status = 'N'";
			}

		}
		//dump($deptSet);die;

		//待办事项
		$wait = new Model("waitmatter");
		$waitcount = $wait->where($where_wait)->count();
		//echo $wait->getLastSql();die;
		$this->assign("waitcount",$waitcount);


		//今日回访任务
		$visit_customer = new Model("visit_customer");
		$visit_count = $visit_customer->where($where_visit)->count();
		$this->assign("visit_count",$visit_count);

		//今日外呼回访任务
		$visit_outbound_count = $visit_customer->where($where_visit2)->count();
		//echo $visit_customer->getLastSql();die;
		$this->assign("visit_outbound_count",$visit_outbound_count);

		//公告
		$gg = new Model("announcement");
		$ggcount = $gg->where($where_gg)->count();
		//echo $gg->getLastSql();die;
		$this->assign("ggcount",$ggcount);

		//未接来电
		$cdr = new Model('asteriskcdrdb.Cdr');
		if($username != "admin"){
			$cdrcount = $cdr->where("dst='$extension' AND disposition !='ANSWERED'")->count();
		}else{
			$cdrcount = $cdr->where("disposition !='ANSWERED'")->count();
		}
		$this->assign("cdrcount",$cdrcount);

		//最近未联系客户
		$visit_record = new Model("customer");
		$novisitCount = $visit_record->where($where_noVisit)->count();
		//echo $visit_record->getLastSql();die;
		$this->assign("novisitCount",$novisitCount);

		//最近可能需要复购的资料
		$order_info = new Model("order_info");
		$purchaseCount = $order_info->table("order_info o")->field("o.id,o.purchase_time,o.order_num,o.customer_id,c.phone1,c.name")->join("customer c on (o.customer_id = c.id)")->where($where_purchase)->count();
		//echo $order_info->getLastSql();
		$this->assign("purchaseCount",$purchaseCount);


		//订单状态更变提醒
		$count_order_status = $order_info->where($where_order_status)->count();
		$this->assign("count_order_status",$count_order_status);

		//热线资源
		$hotline_record = M("hotline_record");
		$hotline_count = $hotline_record->where($where_hotline)->count();
		$this->assign("hotline_count",$hotline_count);



		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("inbound",$arrAL) ){
			$wj_in = "Y";
		}else{
			$wj_in = "N";
		}
		if( in_array("outbound",$arrAL) ){
			$wj_out = "Y";
		}else{
			$wj_out = "N";
		}
		if( in_array("IPPBX",$arrAL) ){
			$wj_IPPBX = "Y";
		}else{
			$wj_IPPBXt = "N";
		}
		if($wj_in == "Y" && $wj_out == "Y"){
			$sys_type = "all";
		}elseif($wj_in == "Y"){
			$sys_type = "inbound";
		}elseif($wj_out == "Y"){
			$sys_type = "outbound";
		}elseif($wj_IPPBX == "Y"){
			$sys_type = "IPPBX";
		}
		$this->assign("sys_type",$sys_type);

		//dump($web_type);die;

		$this->display();
	}


	/*
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}*/


	function getMeAndSubDeptName12($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName12($arrDep,$id);
			}
		}
		return $str;

	}

	function getMeAndSubDeptName($arrDep,$dept_id){
		global $dep_str;
		$dep_str .= "'" . $dept_id . "',";
		$pid = $arrDep[$dept_id]['pid'];

		foreach($arrDep as $key=>$val){
			if($key == $pid){
				$this->getMeAndSubDeptName($arrDep,$pid);
			}
		}
		//dump($dep_str);die;
		return $dep_str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }

	function alterPassword(){
		checkLogin();
		$username = $_SESSION["user_info"]["username"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$en_name = $_SESSION["user_info"]["en_name"];

		$arrAdmin = getAdministratorNum();
		if( in_array($username,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$username);
		}

		$this->assign("cn_name",$cn_name);
		$this->assign("en_name",$en_name);
		$users = new Model("users");
		$userList = $users->where("username = '$username'")->find();
		$pwd = $userList["password"];
		$this->assign("pwd",$pwd);
		$this->display();
	}

	function doAlterPWD(){
		$username = $_SESSION["user_info"]["username"];
		$users = new Model("users");
		$arrData = array(
			"password"=>$_POST['newpwd'],
			"cn_name"=>$_POST['cn_name'],
			"en_name"=>$_POST['en_name'],
		);
		$result = $users->data($arrData)->where("username = '$username'")->save();
		if($result){
			echo json_encode(array('success'=>true,'msg'=>'密码修改成功!'));
		}else{
			echo json_encode(array('msg'=>'密码修改失败！'));
		}
	}

   //调试
	function debug_normal_call(){
		$username = $_SESSION["user_info"]["username"];
		$exten = $_SESSION["user_info"]["extension"];
      $postURL = "http://localhost/msgInterface/pub?user=E$exten";
      $uniqueid = time() . '.001';
      $caller = $_POST['info'];
      $postData = "Normalcall!$caller!$exten!SIP/$exten!$uniqueid";
      //wget --post-data='Normalcall!13828863511!11001!3333!3423!51255327' 'http://localhost/da/pub?daID=111'
      $cmd = "wget   --post-data='$postData'  '$postURL' 2>/dev/null";
      syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'$cmd= '.$cmd); //调试
      exec($cmd);
		echo 'ok';
	}

	function debug_auto_call(){
		$username = $_SESSION["user_info"]["username"];
		$exten = $_SESSION["user_info"]["extension"];
      $postURL = "http://localhost/msgInterface/pub?user=E$exten";
      $uniqueid = time() . '.001';
      $caller = $_POST['info'];
      $task_id = $_GET['task_id'];
      $postData = "Autocall!$caller!$exten!SIP/$exten!$task_id!$uniqueid!!1"; //默认 phone_id=1;
      $cmd = "wget   --post-data='$postData'  '$postURL' 2>/dev/null";
      syslog(LOG_WARNING, Date('Y-m-d H:i:s') .'$cmd= '.$cmd); //调试
      exec($cmd);
		echo 'ok';
	}

	function editHookSms(){
		checkLogin();
		$username = $_SESSION["user_info"]["username"];
		$extension = $_SESSION["user_info"]["extension"];
		$hangupsms = new Model("hangupsms");
		$hkData = $hangupsms->where("exten = '$extension'")->find();
		if($hkData){
			$this->assign("status","edit");
		}else{
			$this->assign("status","add");
		}
		$this->assign("extension",$extension);
		$this->assign("hkData",$hkData);

		$this->display();
	}

	function saveHookSms(){
		$status = isset($_REQUEST['in_enabled']) ? "ON" : "OFF";
		$exten = $_REQUEST["exten"];
		$hangupsms = new Model("hangupsms");
		$arrData = array(
			"exten"=>$_REQUEST['exten'],
			"status"=>$status,
			"msg"=>$_REQUEST['content'],
		);
		if($_REQUEST["status"] == "edit"){
			$result = $hangupsms->data($arrData)->where("exten = '$exten'")->save();
			if ($result !== false){
				echo json_encode(array('success'=>true,'msg'=>'保存成功！'));
			} else {
				echo json_encode(array('msg'=>'保存失败！'));
			}
		}else{
			$result = $hangupsms->data($arrData)->add();
			if ($result){
				echo json_encode(array('success'=>true,'msg'=>'保存成功！'));
			} else {
				echo json_encode(array('msg'=>'保存失败！'));
			}
		}
	}



	function trainRemind(){
		$trainData = $_REQUEST["trainData"];
		foreach($trainData as $val){
			$arrID[] = $val["customer_id"];
		}
		$ids = implode(",",$arrID);

		$ks_train = new Model("ks_train");
		$fields = "t.id,t.create_time,t.create_user,t.dept_id,t.curriculum_id,t.courseware_id,t.training_task_name,t.training_address,t.training_task_description,t.training_date,t.training_start_time,t.training_end_time,t.whether_exam,t.ks_id,t.participants,t.lectures_people,c.courseware_name,kc.curriculum_name,e.exam_plan_name";
		$arrData = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->join("ks_examination_program e on (t.ks_id = e.id)")->where("t.id in ($ids)")->select();

		foreach($arrData as &$val){
			$val["training_time"] = $val["training_start_time"]."~".$val["training_end_time"];
			$message[] = "您接下来要去参加培训。培训时间：".$val["training_time"]."，培训课程：".$val["curriculum_name"]."，培训地址：".$val["training_address"]."，请您调整好时间，准备参加培训。"."<a href='javascript:void(0);' onclick=\"viewCourseware("."'".$val["courseware_id"]."'".")\" >查看课件</a>";;
		}


		//dump($arrData);die;
		$msg = implode("<br>",$message);
		echo json_encode(array('success'=>true,'msg'=>$msg));
	}

	function examRemind(){
		$examData = $_REQUEST["examData"];
		foreach($examData as $val){
			$arrID[] = $val["customer_id"];
		}
		$ids = implode(",",$arrID);
		$ks_examination_program = new Model("ks_examination_program");
		$fields = "e.id,e.create_time,e.create_user,e.dept_id,e.paper_id,e.curriculum_id,e.exam_plan_name,e.exam_plan_description,e.exam_plan_address,e.exam_date,e.start_exam_time,e.end_exam_time,e.exam_type,e.are_published,e.examiners,e.reviwers,e.passing_score,e.examination_staff,p.papers_name,c.curriculum_name";
		$arrData = $ks_examination_program->order("e.create_time desc")->table("ks_examination_program e")->field($fields)->join("ks_papers p on (e.paper_id = p.id)")->join("ks_curriculum c on (e.curriculum_id = c.id)")->where("e.id in ($ids)")->select();
		$exam_type_row = array('Y'=>'开卷','N'=>'闭卷');
		foreach($arrData as &$val){
			$exam_type = $exam_type_row[$val['exam_type']];
			$val['exam_type'] = $exam_type;

			$val["exam_time"] = $val["start_exam_time"]."~".$val["end_exam_time"];
			$message[] = "您接下来要去考试。考试时间：".$val["exam_time"]."，考试课程：".$val["curriculum_name"]."，考试地址：".$val["exam_plan_address"]."，考试类型：".$val["exam_type"]."，请您调整好时间，准备<a href='#' onclick='exams();'>参加考试</a>。";
		}


		$msg = implode("<br>",$message);
		//dump($ids);die;
		echo json_encode(array('success'=>true,'msg'=>$msg));
	}


}
