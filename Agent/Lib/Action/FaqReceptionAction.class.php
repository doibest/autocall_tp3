<?php
class FaqReceptionAction extends Action{
	/*
	function faqContent(){
		checkLogin();
		$id = $_GET['id'];
		$faq = new Model("faq_content");
		if($id){
			$this->assign("id",$id);
			$faqList = $faq->where("id = $id")->find();
			$this->assign("faqList",$faqList);
		}
		$this->display();
	}
	*/

	function faqContent(){
		checkLogin();
		$id = $_GET['id'];
		$faq = new Model("faq_content");
		if($id){
			$this->assign("id",$id);
			$faqList = $faq->where("id = $id")->find();
			$faqList["file_path_name"] = $faqList["file_path"].$faqList["faq_file"];
			$arrF = explode(".",$faqList["faq_file"]);
			$filename = $arrF[0];
			$suffix = $arrF[1];
			$suffix_old = $suffix;
			$arrPdf = array("doc","docx","xls","xlsx","ppt","pptx");
			if($suffix == "txt"){
				$faqList["file_content"] = file_get_contents($faqList["file_path_name"]);
				$faqList["file_content"] = str_replace("\r\n","<br/>",$faqList["file_content"]);
				//$faqList["file_content"] = gb2utf($faqList["file_content"]);
				if( mb_detect_encoding($faqList["file_content"],"UTF-8, ISO-8859-1, GBK") != "UTF-8" ){
					$faqList["file_content"] = gb2utf($faqList["file_content"]);
				}

			}else{
				$faqList["file_content"] = "";
			}

			if( in_array($suffix,$arrPdf) ){
				//$suffix = "pdf";
				$suffix = "word";
				//$faqList["file_path_name"] = $faqList["file_path"].$filename.".pdf";
			}

			$this->assign("faqList",$faqList);
			$this->assign("suffix",$suffix);

			$this->assign("suffix_old",$suffix_old);
			$CTI_IP = $_SERVER["SERVER_ADDR"];
			$this->assign("CTI_IP",$CTI_IP);

			if(file_exists($faqList["file_path_name"])){
				$this->assign("annex","Y");
			}else{
				$this->assign("annex","N");
			}
			//dump($arrF);
			//dump($suffix);
			//dump($faqList);die;
			$click_num = $faqList["click_num"]+1;
			$faq->where("id = $id")->save(array("click_num"=>$click_num));
		}
		//dump($faqList);die;
		$this->display();
	}


	function faqContentView(){
		checkLogin();
		$id = $_GET['id'];
		$faq = new Model("faq_content");
		if($id){
			$this->assign("id",$id);
			$faqList = $faq->where("id = $id")->find();
			$faqList["file_path_name"] = $faqList["file_path"].$faqList["faq_file"];
			$arrF = explode(".",$faqList["faq_file"]);
			$filename = $arrF[0];
			$suffix = $arrF[1];
			$suffix_old = $suffix;
			$arrPdf = array("doc","docx","xls","xlsx","ppt","pptx");
			if($suffix == "txt"){
				$faqList["file_content"] = file_get_contents($faqList["file_path_name"]);
				$faqList["file_content"] = str_replace("\r\n","<br/>",$faqList["file_content"]);
				if( mb_detect_encoding($faqList["file_content"],"UTF-8, ISO-8859-1, GBK") != "UTF-8" ){
					$faqList["file_content"] = gb2utf($faqList["file_content"]);
				}

			}else{
				$faqList["file_content"] = "";
			}

			if( in_array($suffix,$arrPdf) ){
				//$suffix = "pdf";
				$suffix = "word";
				//$faqList["file_path_name"] = $faqList["file_path"].$filename.".pdf";
			}

			$browser = browserType();
			if( $browser =="fire" ){
				$keyword = $_GET['keyword'];
			}
			if( $browser == "ie360" ){
				$keyword2 = $_GET['keyword'];
				$keyword = gb2utf($keyword2);
			}

			$strcolor = "<span class='faqcolor'>".$keyword."</span>";
			$faqList["content"] = str_ireplace($keyword,$strcolor,$faqList['content']);
			$faqList["title"] = str_ireplace($keyword,$strcolor,$faqList['title']);


			if(file_exists($faqList["file_path_name"])){
				$this->assign("annex","Y");
			}else{
				$this->assign("annex","N");
			}

			//视频
			$arrVideo = array("mp4","webm","ogv");
			if( in_array($suffix,$arrVideo) ){
				$suffix = "video";
			}
			//音频
			$arrAudio = array("mp3","wav");
			if( in_array($suffix,$arrAudio) ){
				$suffix = "audio";
			}

			//图片
			$arrPicture = array('jpg', 'gif', 'png', 'jpeg');
			if( in_array($suffix,$arrPicture) ){
				$suffix = "picture";
			}

			if($suffix_old == "mp4"){
				$media_type =  "m4v";
			}elseif($suffix_old == "webm"){
				$media_type =  "webmv";
			}elseif($suffix_old == "ogv"){
				$media_type =  "ogv";
			}elseif($suffix_old == "mp3"){
				$media_type =  "mp3";
			}elseif($suffix_old == "wav"){
				$media_type =  "wav";
			}else{
				$media_type =  "";
			}

			$this->assign("faqList",$faqList);
			$this->assign("suffix",$suffix);
			$this->assign("suffix_old",$suffix_old);
			$this->assign("media_type",$media_type);
			$CTI_IP = $_SERVER["SERVER_ADDR"];
			$this->assign("CTI_IP",$CTI_IP);


			//dump($arrF);
			//dump($suffix);
			//dump($faqList);die;
			$click_num = $faqList["click_num"]+1;
			$faq->where("id = $id")->save(array("click_num"=>$click_num));
		}
		//dump($faqList);die;
		$this->display();
	}


	function faqAll(){
		checkLogin();
		$this->display();
	}


	function faqView(){
		$id = $_GET['id'];
		//$keyword = $_GET['keyword'];
		$browser = browserType();
		if( $browser =="fire" ){
			$keyword = $_GET['keyword'];
		}
		if( $browser == "ie360" ){
			$keyword2 = $_GET['keyword'];
			$keyword = gb2utf($keyword2);
		}

		//dump($keyword);die;
		$faq = new Model("faq_content");
		if($id){
			$this->assign("id",$id);
			$faq = $faq->where("id = $id")->find();
			$strcolor = "<span class='faqcolor'>".$keyword."</span>";
			$faq["content"] = str_ireplace($keyword,$strcolor,$faq['content']);
			$faq["title"] = str_ireplace($keyword,$strcolor,$faq['title']);

			echo json_encode($faq);
			//$this->assign("faqList",$faq);
		}
	}

    function getTree($data, $pId) {
        $tree = '';
		//dump($data);die;
        foreach($data as $k =>$v) {
            if($v['pid'] == $pId)    {
				//dump($v['state']);
				$v['children'] = $this->getTree($data, $v['tid']);
				if ( empty($v["children"])  )  unset($v['children']) ;
				/*
				if ( empty($v["children"]) && $v['state'] =='closed')  $v['children'] =  array(array());
				*/
				if ( empty($v["children"]) && $v['state'] =='closed')  $v['state'] =  'open';
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	function faqtree(){
		$faq=new Model("faq_type");
		$types = $faq->field("name as text,id as tid,pid")->select();

		$faq_content = new Model("faq_content");
		$faqList = $faq_content->order("createtime desc")->field("id,title as text,type_id as pid")->select();

		$j = 0;
		foreach($types as $vm){
			$types[$j]['iconCls'] = "icon-files";
			$types[$j]['state'] = "closed";
			$j++;
		}

		foreach($faqList as $val){
			$arr[$val["pid"]][] = $val;
			array_push($types,$val);
		}

        $arrTree = $this->getTree($types,0);

		//dump($arrTree);die;

		$strJSON = json_encode($arrTree);
		echo ($strJSON);
	}

	function faqSearch(){
		$firebox_IE = $_SERVER["HTTP_USER_AGENT"];  //判断浏览器类型
		$fire = strpos($firebox_IE,"Gecko");   //火狐浏览器
		$google = strpos($firebox_IE,"WebKit");   //谷歌浏览器
		$IE = strpos($firebox_IE,"Trident");   //IE、360浏览器
		$faq = new Model("faq_content");
		if( $fire || $google ){
			$keyword = $_GET['keyword'];
		}
		if( $IE ){
			$keyword2 = $_GET['keyword'];
			$keyword = gb2utf($keyword2);
		}
		//dump($fire);die;
		//$keyword2 = gb2utf($keyword);
		//dump($title);die;
        if( $keyword ){
			$where = "title like '%$keyword%' or content like '%$keyword%' ";
			$count = $faq->where($where)->count();
			//echo $faq->getLastSql();die;
			if($count <= 0){
				goback("没有找到相关主题","","");
			}
			$searchfaq = $faq->table("faq_content fc")->order("fc.createtime desc")->field("ft.name,fc.title,fc.id")->join("faq_type ft on (fc.type_id = ft.id)")->where($where)->select();
		}

		//echo $faq->getLastSql();die;
		echo json_encode($searchfaq);
	}

	function faqAlliframe(){
		checkLogin();
		$this->display();
	}

	function waitUpdateData(){
		$fields = explode('_',$this->_post("name"));
		$ary = array($fields[0]=>$this->_post("value"));
		$wait = new Model("waitmatter");
		$count = $wait->where("id=".$fields[1])->save($ary);
	}

}

?>
