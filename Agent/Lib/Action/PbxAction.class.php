<?php

class PbxAction extends Action {


	//普通crm的点击拨号
	//调用实例: http://192.168.1.69/agent.php?m=Pbx&a=normal_clickcall&exten=816&phone=837

	function normal_clickcall(){
		//点击拨号前期的检测过滤
		if(empty($_GET['exten']))$_GET['exten']=$_SESSION['user_info']['extension'];
		if( empty($_GET['phone']) ){echo "被叫号码不存在";die;}
		if(! is_numeric($_GET['phone'])){echo "Illegal_numbers";die;}

		$dnc = M('sales_dnc');
		$count = $dnc->where("phonenumber='{$_GET['phone']}'")->count();
		if($count > 0){echo "Blacklist";die;}

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrHint = $bmi->getHint($_GET['exten']);
		if( $arrHint['stat'] == "Idle" ){
			$callerid = "";
			$bmi->normal_clickcall($arrHint['device'],$_GET['exten'],$_GET['phone'],$callerid);
			//echo "成功发起呼叫!";
			echo "success";
		}else{
			echo "呼叫失败，请检测坐席状态!";
		}
	}


	//预览式外呼的点击拨号
	//调用实例: http://192.168.1.69/agent.php?m=Pbx&a=preview_clickcall&exten=816&phone=879&task_id=13&trunk=SIP/69to58&callerid=9999
	//exten: 坐席号码,首先接听的一端
	//phone: 外面客户手机号码
	//callerid:可选参数,伪造给外面客户的来电号码
    function preview_clickcall(){
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$task_id = $_GET['task_id']; //根据它来写cdr
		$exten = $_GET['exten'];
		$phone = $_GET['phone'];

		$source = new Model("sales_source_".$task_id);
		$arr = $source->field("phone1,id")->where("phone1 = '$phone' OR phone2 = '$phone'")->find();
		$customer_id = $arr["id"];
		if(!$customer_id){
			$phone1 = substr($phone,1);
			$arr = $source->field("phone1,id")->where("phone1 = '$phone1' OR phone2 = '$phone'")->find();
			$customer_id = $arr["id"];
		}

		$sales_task = new Model("sales_task");
		$arrTask = $sales_task->where("id = '$task_id'")->find();
		$trunk = $arrTask['trunk'];
		$callout_pre = $arrTask['callout_pre'];
		$callerid = $arrTask['callerid']?$arrTask['callerid']:"";

    /*
    //2015--预览示外呼：一机一号
    $t1 = new Model("asterisk.exten_trunk_cid");
		$arr1 = $t1->where("exten = '$exten'")->find();
		$callerid = empty($arr1["number"])?$callerid:$arr1["number"];
    */

		/*
		$trunk = $_GET['trunk'];
		$callerid = $_GET['callerid']?$_GET['callerid']:"";
		*/
		//拨号前期的检测过滤
		if(empty($phone)){echo "被叫号码不存在";die;}
		if(! is_numeric($phone)){echo "Illegal_numbers";die;}
		$dnc = M('sales_dnc');
		$count = $dnc->where("phonenumber='$phone'")->count();
		if($count > 0){echo "Blacklist";die;}

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrHint = $bmi->getHint($exten);
		$device = $arrHint['device'];

		$bmi->preview_clickcall($device,$exten,$phone,$task_id,$trunk,$callerid,$callout_pre,$customer_id,$db_name);
		echo "ok";
	}


	//通话转移
	function transfer(){
		$exten = $_GET['exten'];
		$phone = $_GET['phone']; //转接的号码
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrHint = $bmi->getHint($exten);
		$device = $arrHint['device'];
		//dump($device);die;
		$ret = $bmi->transfer($device,$phone);
		return $ret;
	}

	//三方通话
	function nwaycall(){
		$exten = $_GET['exten'];
		$phone = $_GET['phone']; //第3方的号码
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrHint = $bmi->getHint($exten);
		$device = $arrHint['device'];
		$ret = $bmi->nway2($device,$phone);
	}


	//密码验证
	function memberpin(){
		$exten = $_GET['exten'];

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrHint = $bmi->getHint($exten);
		$device = $arrHint['device'];
		$ret = $bmi->memberpin($device,$exten);
	}

	//通话暂停
	function callPause(){
		$exten = $_GET['exten'];

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrHint = $bmi->getHint($exten);
		$device = $arrHint['device'];
		//dump($device);die;
		//$parkedExten = $bmi->callPause($exten,$device);
		$parkedChannel = $bmi->callPause($exten,$device);
		//dump($parkedExten);die;
		//$_SESSION['parkedExten'] = $parkedExten;
		$_SESSION['parkedChannel'] = $parkedChannel;
	}

	//通话恢复
	function callRestore(){
		//$parkedExten = $_SESSION['parkedExten'];
		$parkedChannel = $_SESSION['parkedChannel'];
		//dump($parkedExten);die;
		$exten = $_GET['exten'];

		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrHint = $bmi->getHint($exten);
		$device = $arrHint['device'];

		//$bmi->restoreCall($device,$parkedExten);
		$bmi->restoreCall($exten,$parkedChannel);
		echo "ok";
	}


	function insertPauseReason(){
		$exten = $_GET['exten'];
		$reason = $_GET['reason'];
		$now = Date('Y-m-d H:i:s');
		$username = $_SESSION['user_info']['username'];
		$table = M('servicepause_reason');

		$res = $table->add(Array(
			'username'	=>	$username,
			'starttime'	=>	$now,
			'reason'	=>	$reason,
		));

		if( $res ){
			echo "ok";
		}
	}

	function updatePauseState(){
		//dump($_SESSION);die;
		$username = $_GET['username'];
		$exten = $_GET['exten'];
		$table = M('users');
		$res = $table->where("username='$username'")->data(Array('servicepause'=>'Y'))->save();

		if( false !== $res ){
			echo "ok";
		}
		//同时还要修改两个AST相关变量——DND和队列中的pause
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		//$arrHint = $bmi->getHint($exten);
		//$device = $arrHint['device'];
		//dump($device);die;
		$bmi->putDND($exten);
		$bmi->asm->send_request('QueuePause', Array('Interface'=>"Local/$exten@BG-QueueAgent",'Paused'=>'yes'));
	}


	function unlock(){
		$password = $_GET['password'];
		$username = $_GET['username'];
		$exten = $_GET['exten'];

		$users = M('users');
		$count = $users->where("username='$username' AND password='$password'")->count();
		if($count > 0){
			//更改状态
			$users->where("username='$username'")->data(Array("servicepause"=>'N'))->save();

			//生成暂停服务时长
			$table = M('servicepause_reason');
			$row = $table->where("username='$username'")->order("starttime DESC")->find();
			//dump($row);die;
			$starttime = $row['starttime'];
			$during = time() - strtotime($starttime);
			$table->where("username='$username' AND starttime='$starttime'")->data(Array('during'=>$during))->save();

			//取消ast示忙
			import('ORG.Pbx.bmi');
			$bmi = new bmi();
			$arrHint = $bmi->getHint($exten);
			$device = $arrHint['device'];
			$bmi->delDND($exten);
			$bmi->asm->send_request('QueuePause', Array('Interface'=>"Local/$exten@BG-QueueAgent",'Paused'=>'no'));

			echo "ok";
		}else{
			echo "fail";
		}
	}


	//话后手工示闲或者保存工单后示闲
	function aftercallunlock(){
		$password = $_GET['password'];
		$username = $_GET['username'];
		$exten = $_GET['exten'];

		$users = M('users');
		//更改状态
		$users->where("username='$username'")->data(Array("servicepause"=>'N'))->save();
		//生成暂停服务时长
		$table = M('servicepause_reason');
		$row = $table->where("username='$username'")->order("starttime DESC")->find();
		//dump($row);die;
		$starttime = $row['starttime'];
		$during = time() - strtotime($starttime);
		$table->where("username='$username' AND starttime='$starttime'")->data(Array('during'=>$during))->save();

		//取消ast示忙
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrHint = $bmi->getHint($exten);
		$device = $arrHint['device'];
		$bmi->delDND($exten);
		$bmi->asm->send_request('QueuePause', Array('Interface'=>"Local/$exten@BG-QueueAgent",'Paused'=>'no'));

		echo "ok";
	}

	function isServicePause(){
		$username = $_GET['username'];
		//手工示忙
		$users = M('users');
		$count = $users->where("username='$username' AND servicepause='Y'")->count();
		//数据库中示忙
		//$exten = $_SESSION['user_info']['extension'] = '803';
		//import('ORG.Pbx.bmi');
		//$bmi = new bmi();
		//$arrHint = $bmi->getHint($exten);
		//dump($arrHint);die;
		//$device = $arrHint['stat'];
		if($count >0){
			$d = M('servicepause_reason');
			$PauseType = $d->field("reason")->where("username='$username'")->order("starttime desc")->find();
			if($PauseType["reason"]  == "话后"){
				echo 2;
			}else{
				echo 1;
			}
		}
	}


	//服务评分
	function serviceScore(){
		$admin_extension=$_SESSION["user_info"]["extension"];
		$workNo = $_SESSION["user_info"]["username"];
		import('ORG.Pbx.bmi');
		$bmi = new bmi();
		$arrChannels = $bmi->getExtenChannelList();
		//dump($arrChannels);die;
		//dump($workNo);die;
		//dump($arrChannels[$admin_extension]['bridgedto']);die;
		if(! isset($arrChannels[$admin_extension]) ){
			goBack("通话不存在!","exit");
		}
		if(! isset($arrChannels[$admin_extension]['bridgedto']) ){
			goBack("已经挂机!","exit");
		}
		//只需要把通道重定向就可以完成服务评分了
		//$bmi->servicescore();//其实里面就只需要设置一个通道变量
		$bmi->loadAGI();
		$bmi->asm->SetVar($arrChannels[$admin_extension]['bridgedto'],"workNo",$workNo);
		$bmi->asm->Redirect($arrChannels[$admin_extension]['bridgedto'],"","s","bangian-servicescore","1");
		$bmi->Close();
		echo "本次通话完成,请挂机!";
	}



}
