<?php
class VoiceMailAction extends Action{
	function voicemailAll(){
		checkLogin();
		$this->display();
	}

	function voicemailAllList(){

		$extension = $_SESSION["user_info"]["extension"];
		//dump($extension);
		$archivos=array();
		if( $extension ){
			$path = "/var/spool/asterisk/voicemail/default";
			$folder = "INBOX";
			$voicemailPath = "$path/$extension/$folder/";
		}else{
			echo "分机不存在!";
		}

		$count = 0;
		$arrFile = array();
		if(file_exists($voicemailPath)) {
			if ($handle = opendir($voicemailPath)) {
				while (false !== ($file = readdir($handle) )) {
					if ($file!="." && $file!=".." && ereg("(.+)\.[txt|TXT]",$file,$regs))
					{
						$arrFile[] = $file;
						$count ++;
					}
				}
				closedir($handle);
			}
		}
		rsort($arrFile);
		//dump($arrFile);die;

		import('ORG.Util.Page');
		$count = count($arrFile);
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = array();
		$indexStart = $page->firstRow;
		$indexEnd = $page->firstRow + $page->listRows;
		if($indexEnd > $count -1 ){$indexEnd = $count;}//数组中最后一个
		$d = 0;
		for($i=$indexStart;$i<$indexEnd;$i++){
			$file = $arrFile[$i];
			$arrTmp = explode(".",$file);
			//$vmfile = $arrTmp['0'];
			$vmfile = $d;
			$vmname = $arrTmp['0'];
			$msgfile = $voicemailPath.$file;
			//echo $msgfile."<br>";
			$arrTmp = parse_ini_file($msgfile);
			$arrTmp['origtime'] = Date('Y-m-d H:i:s', $arrTmp['origtime']);
			$arrTmp['vmfile'] = $vmname;

			$arrTmp['operations'] ="<a target='_blank' href='agent.php?m=VoiceMail&a=playVoiceMail&mailbox=$extension&vmfile=$vmname'>"."播放  "."</a>"."<a href='agent.php?m=VoiceMail&a=downloadVoiceMail&mailbox=$extension&vmfile=$vmname'>"."   下载   "."</a>"."<a href='agent.php?m=VoiceMail&a=deleteVoiceMail&mailbox=$extension&vmfile=$vmname'>"."   删除   "."</a>";

			//$arrTmp['operations'] ="<a href='javascript:void(0);' onclick='playVoiceMail();' vmfile='$vmname' mailbox='$extension'>"."播放  "."</a>"."<a href='agent.php?m=VoiceMail&a=downloadVoiceMail&mailbox=$extension&vmfile=$vmname'>"."   下载   "."</a>"."<a href='agent.php?m=VoiceMail&a=deleteVoiceMail&mailbox=$extension&vmfile=$vmname'>"."   删除   "."</a>";  //播放页面弹窗的代码



			//$arrTmp['playUrl'] = "agent.php?m=VoiceMail&a=playVoiceMail&mailbox=$extension&vmfile=$vmname";
			$arrData[$vmfile] = $arrTmp;
			$d++;
		}
		//dump($arrData);die;
		//dump($arrTmp);die;

		$rowsVmList = count($arrData) ? $arrData : false;
		$voicemailData["total"] = count($arrData);
		$voicemailData["rows"] = $rowsVmList;

		echo json_encode($voicemailData);
	}

	function playVoiceMail(){
		checkLogin();
		$mvfile = $_GET['vmfile'];
		//dump($mvfile);die;
		$mailbox = $_GET['mailbox'];
		$path = "/var/spool/asterisk/voicemail/default";
		$folder = "INBOX";
		$voicemailPath = "$path/$mailbox/$folder/";
		$msgfile = $voicemailPath.$mvfile.".txt";
		if(!file_exists($msgfile)){
			$this->error("File $msgfile is not exist!");
		}
		//echo $msgfile."<br>";
		$tpl_Msg = parse_ini_file($msgfile);
		$tpl_Msg['origtime'] = Date('Y-m-d H:i:s', $tpl_Msg['origtime']);

		$this->assign("Msg",$tpl_Msg);
		$this->assign("vmbox",$extension);

		$AudioURL = "agent.php?m=VoiceMail&a=downloadVoiceMail&mailbox=$mailbox&vmfile=$mvfile";
		$this->assign("PlayURL",urlencode($AudioURL));//这里必须用urlencode加密url否则会与flashvars=冲突。
		$this->assign("AudioURL",$AudioURL);
		$this->display();
	}

	function downloadVoiceMail(){
		$mvfile = $_GET['vmfile'];
		$mailbox = $_GET['mailbox'];
		$path = "/var/spool/asterisk/voicemail/default";
		$folder = "INBOX";
		$voicemailPath = "$path/$mailbox/$folder/";
		$WAVfile = $voicemailPath.$mvfile.".wav";
		if(!file_exists($WAVfile)){
			//$this->error("File $WAVfile is not exist!");
			goback("File $WAVfile is not exist!","agent.php?m=VoiceMail&a=voicemailAll");
		}

        header('HTTP/1.1 200 OK');
        //header('Accept-Ranges: bytes');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        //header("Content-Length: " . (string)(filesize($WAVfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=" .str_replace(".wav",".mp3",basename($WAVfile)));
        //readfile($WAVfile);
		system('/usr/bin/lame -b 96 -t -F -m m --bitwidth 16 --quiet ' .$WAVfile ." -");
	}

	function deleteVoiceMail(){
		$mvfile = $_GET['vmfile'];
		$mailbox = $_GET['mailbox'];
		//dump($mvfile);die;
		$path = "/var/spool/asterisk/voicemail/default";
		$folder = "INBOX";
		$voicemailPath = "$path/$mailbox/$folder/";

		$msgfile = $voicemailPath.$mvfile.".txt";
		$WAVfile = $voicemailPath.$mvfile.".wav";
		//dump($msgfile);
		//dump($WAVfile);die;
		$return = false;
		if( file_exists( $msgfile ) || file_exists( $WAVfile ) ){
			//echo "aaaaaaaaaaaa";
			$ret1 = unlink( $msgfile );
			$ret2 = unlink( $WAVfile);
			$return = $ret1 || $ret2;
		}
		//dump($ret1);
		if( $return ){
			//echo "<script>alert('删除成功！')</script>";
			goback("删除成功！","agent.php?m=VoiceMail&a=voicemailAll");
		}else{
			//$this->error("Delete Field!");
			echo "<script>alert('Delete Field!')</script>";
		}
	}


}

?>

