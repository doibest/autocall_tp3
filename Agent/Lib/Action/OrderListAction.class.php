<?php
class OrderListAction extends Action{
	function orderList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Order Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$customer_id = $_REQUEST["customer_id"];
		$this->assign("customer_id",$customer_id);

		$this->display();
	}

	function orderData(){
		$username = $_SESSION["user_info"]["username"];

		$calldate_start = $_REQUEST['calldate_start'];
		$calldate_end = $_REQUEST['calldate_end'];
        $order_num = $_REQUEST['order_num'];
        $consignee = $_REQUEST['consignee'];
        $email = $_REQUEST['email'];
        $phone = $_REQUEST['phone'];
        $telephone = $_REQUEST['telephone'];
        //$delivery_address = $_REQUEST['delivery_address'];
        $zipcode = $_REQUEST['zipcode'];
		$country = $_REQUEST['country'];
        $province = $_REQUEST['province'];
        $city = $_REQUEST['city'];
        $district = $_REQUEST['district'];
        $street = $_REQUEST['street'];
        $order_status = $_REQUEST['order_status'];
        $pay_status = $_REQUEST['pay_status'];
        $shipping_status = $_REQUEST['shipping_status'];
		$createname = $_REQUEST['createname'];
		$logistics_state = $_REQUEST['logistics_state'];
		$customer_id = $_REQUEST['customer_id'];

		$where = "1 ";
        $where .= empty($calldate_start)?"":" AND createtime >'$calldate_start'";
        $where .= empty($calldate_end)?"":" AND createtime <'$calldate_end'";
		$where .= empty($order_num)?"":" AND order_num like '%$order_num%'";
		$where .= empty($consignee)?"":" AND consignee like '%$consignee%'";
		$where .= empty($email)?"":" AND email like '%$email%'";
		$where .= empty($phone)?"":" AND phone like '%$phone%'";
		$where .= empty($telephone)?"":" AND telephone like '%$telephone%'";
		$where .= empty($createname)?"":" AND createname like '%$createname%'";
		$where .= empty($zipcode)?"":" AND zipcode like '%$zipcode%'";
		$where .= empty($country)?"":" AND country = '$country'";
		$where .= empty($province)?"":" AND province = '$province'";
		$where .= empty($city)?"":" AND city = '$city'";
		$where .= empty($district)?"":" AND district = '$district'";
		$where .= empty($street)?"":" AND street = '$street'";
		//$where .= empty($order_status)?"":" AND order_status like '%$order_status%'";
		if($order_status == "0"){
			$where .= " AND order_status = '0'";
		}else{
			$where .= empty($order_status)?"":" AND order_status = '$order_status'";
		}
		$where .= empty($pay_status)?"":" AND pay_status like '%$pay_status%'";
		//$where .= empty($shipping_status)?"":" AND shipping_status like '%$shipping_status%'";
		if($shipping_status == "0"){
			$where .= " AND shipping_status = '0'";
		}else{
			$where .= empty($shipping_status)?"":" AND shipping_status = '$shipping_status'";
		}
		if($logistics_state == "0"){
			$where .= " AND logistics_state = '$logistics_state'";
		}elseif($logistics_state == "all"){
			$where .= " AND logistics_state != '' AND logistics_state is not null";
		}else{
			$where .= empty($logistics_state)?"":" AND logistics_state = '$logistics_state'";
		}
		$where .= empty($customer_id) ? " AND createname ='$username'" : " AND customer_id = '$customer_id'";


		$order_info = new Model("order_info");
		$count = $order_info->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrOrderData = $order_info->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $order_info->getLastSql();die;

		$row_order_status = array('0'=>'未确认','1'=>'已确认','2'=>'<span style="color:#FF6600;">取消</span>','3'=>'<span style="color:#FF6600;">待审核</span>','4'=>'<span style="color:red;font-weight:bold;">退货</span>','5'=>'已分单','6'=>'部分分单');
		$row_pay_status = array('N'=>'未付款','M'=>'付款中','Y'=>'<span style="color:#9933CC;font-weight:bold;">已付款</span>');
		$row_shipping_status = array('0'=>'<span style="color:#FF9933;font-weight:bold;">未发货</sapn>','1'=>'已发货','2'=>'<span style="color:#0000FF;font-weight:bold;">收货确认</span>','3'=>'配货中','4'=>'已发货(部分商品)','5'=>'发货中');

		$row_logistics_state = array("0"=>"在途","1"=>"揽件","2"=>"疑难","3"=>"签收","4"=>"退签","5"=>"派件","6"=>"退回","7"=>"转投");

		$i = 0;
		foreach($arrOrderData as &$val){
			$order_status = $row_order_status[$val['order_status']];
			$val['order_status'] = $order_status;

			$pay_status = $row_pay_status[$val['pay_status']];
			$val['pay_status'] = $pay_status;

			$shipping_status = $row_shipping_status[$val['shipping_status']];
			$val['shipping_status'] = $shipping_status;
			$arrOrderData[$i]['status'] = $val['order_status']."  ".$val['pay_status']."   ".$val['shipping_status'];
			$val["logistics_state"] = $row_logistics_state[$val["logistics_state"]];

			if($val["logistics_account"] && $val["logistics_state"]){
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"viewLogisticsInfo("."'".$val["logistics_account"]."','".$val["logistics_state"]."','".$val["logistics_Info"]."'".")\" >查看物流</a>";
			}else{
				$val["operating"] = "";
			}

			$i++;
		}
		unset($i);
		//echo $order_info->getLastSql();die;
		$rowsList = count($arrOrderData) ? $arrOrderData : false;
		$arrOrder["total"] = $count;
		$arrOrder["rows"] = $rowsList;

		echo json_encode($arrOrder);
	}

	function addOrder(){
		checkLogin();
		$cid = $_REQUEST['customer_id'];
		$para_sys = readS();
		if($cid){
			$customer = new Model("customer");
			$cData = $customer->where("id = '$cid'")->find();



			if($para_sys["editOrder_hide"] =="phone" || $para_sys["editOrder_hide"] == "all" ){
				$cData["phone_ms"] = $cData["phone1"];
				$cData["telephone_ms"] = $cData["phone2"];
				$cData["phone1"] = substr($cData["phone1"],0,3)."***".substr($cData["phone1"],-4);
				if($cData["phone2"]){
					$cData["phone2"] = substr($cData["phone2"],0,3)."***".substr($cData["phone2"],-3);
				}
				$this->assign("message_phone","ms");
				$this->assign("message_telephone","ms");
			}else{
				$cData["phone_ms"] = $cData["phone1"];
				$cData["telephone_ms"] = $cData["phone2"];
				$this->assign("message_phone","show_phone");
				$this->assign("message_telephone","show_phone");
			}

			if($cData["email"]){
				if($para_sys["editOrder_hide"] =="email" || $para_sys["editOrder_hide"] == "all" ){
					$cData["email_ms"] = $cData["email"];
					$cData["email"] = "***".strstr($cData["email"],"@");
					$this->assign("message_email","ms");
				}else{
					$cData["email_ms"] = $cData["email"];
					$this->assign("message_email","show_email");
				}
			}
			//dump($cData);die;

			$this->assign("cData",$cData);
		}else{
			if($para_sys["editOrder_hide"] =="phone" || $para_sys["editOrder_hide"] == "all" ){
				$this->assign("message_phone","ms");
			}else{
				$this->assign("message_phone","show_phone");
			}
		}
		$times = date("Y-m-d H:i:s");
		$this->assign("times",$times);


		if($_REQUEST['type']){
			$this->assign("type","Y");
		}else{
			$this->assign("type","N");
		}

		$this->display();
	}

	function insertOrder(){
		//dump($_REQUEST);die;
		$username = $_SESSION["user_info"]["username"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$order_num = date("YmdHis").$username;
		$info = explode(",",$_REQUEST['info']);

		foreach($info as $vm){
			$goodInfo[] = explode("-",$vm);
		}

		foreach($goodInfo as $v){
			$price[] = $v[3];
			$g_id[] = $v[0];
			//$g_name[] = $v[4]."-".$v[2];
			//$g_name[] = $v[0]."`*`".$v[1]."`*`".$v[2]."`*`".$v[3]."`*`".$v[4]."`*`".$v[5]."`*`".$v[6]."`*`".$v[7];  //商品id-商品数量-商品单价-商品总价格-商品名称-商品货号-商品分类-商品品牌
			$g_name[] = array(
				"id"=>$v[0],			//商品id
				"goods_num"=>$v[1],		//商品数量
				"price"=>$v[2],			//商品单价
				"goods_money"=>$v[3],	//商品总价格
				"good_name"=>$v[4],		//商品名称
				"good_num"=>$v[5],		//商品货号
				"cat_name"=>$v[6],		//商品分类
				"brand_name"=>$v[7],	//商品品牌
			);
			$nums[] = $v[1];
			$g_num[] = $v[4]."[".$v[5]."]*".$v[1];  //商品名称[商品货号]*商品数量
		}
		$goods_total = array_sum($nums);
		$goods_amount = array_sum($price);
		$goods_id = implode(",",$g_id);
		//$goods_name = implode(",",$g_name);
		$goods_name = json_encode($g_name);
		$order_goods_num = implode(",",$g_num);

		$shipping_fee = isset($_REQUEST['shipping_fee']) ? $_REQUEST['shipping_fee'] : "0.00";

		$cope_money = $goods_amount - $shipping_fee;
		//dump($cope_money);
		//dump($goodInfo);die;

		if($_REQUEST['message_phone'] == "ms"){
			$phone = $_REQUEST['mess_phone'];
		}else{
			$phone = $_REQUEST['phone'];
		}
		if($_REQUEST['message_telephone'] == "ms"){
			$telephone = $_REQUEST['mess_telephone'];
		}else{
			$telephone = $_REQUEST['telephone'];
		}
		if($_REQUEST['message_email'] == "ms"){
			$email = $_REQUEST['mess_email'];
		}else{
			$email = $_REQUEST['email'];
		}

		$customer_id = $_REQUEST['cid'];

		$order_info = new Model("order_info");
		//$order_type = implode(",",$_REQUEST["order_type"]);
		$order_type = $_REQUEST["order_type"];
		$arrData = array(
			//"order_num"=>$_REQUEST['order_num'],
			"order_num"=>$order_num,
			"goods_id"=>$goods_id,
			"goods_total"=>$goods_total,
			"goods_name"=>$goods_name,
			//"customer_id"=>$_REQUEST['customer_id'],
			"customer_id"=>$customer_id,
			"consignee"=>$_REQUEST['consignee'],
			"createname"=>$username,
			"dept_id"=>$d_id,
			"createtime"=>$_REQUEST['createtime'],
			//"paytime"=>$_REQUEST['paytime'],
			//"deliverytime"=>$_REQUEST['deliverytime'],
			//"receipttime"=>$_REQUEST['receipttime'],
			//"canceltime"=>$_REQUEST['canceltime'],
			"goods_amount"=>$goods_amount,
			"cope_money"=>$cope_money,
			"shipping_fee"=>$shipping_fee,
			"order_status"=>'3',           //状态
			"pay_status"=>'N',
			"shipping_status"=>'0',
			//"country"=>$_REQUEST['country'],
			"country"=>"1",
			"province"=>$_REQUEST['province'],
			"city"=>$_REQUEST['city'],
			"district"=>$_REQUEST['district'],
			"street"=>$_REQUEST['street'],
			"delivery_address"=>$_REQUEST['delivery_address'],
			"zipcode"=>$_REQUEST['zipcode'],
			"description"=>$_REQUEST['description'],
			"telephone"=>$telephone,
			"phone"=>$phone,
			"email"=>$email,
			//"shopping_name"=>$_REQUEST['shopping_name'],
			//"payment"=>$_REQUEST['payment'],
			"purchase_time"=>$_REQUEST['purchase_time'],
			"order_type"=>$order_type,
			"order_goods_num"=>$order_goods_num
		);
		//dump($_REQUEST);
		//dump($arrData);die;
		$result = $order_info->data($arrData)->add();
		if($result){
			$order_goods = new Model("order_goods");
			$sql = "insert into order_goods(goods_id,goods_num,goods_price,goods_money,order_id) values";
			$value = "";
			foreach($goodInfo as $row){
				$str = "(";
				$str .= "'" .$row[0]. "',";  	 //商品id
				$str .= "'" .$row[1]. "',";  	 //商品数量
				$str .= "'" .$row[2]. "',";  	 //商品价格
				$str .= "'" .$row[3]. "',";  	 //商品总价格
				$str .= "'" .$result. "'";	 //订单id---最后一个不需要","

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
			if( $value ){
				$sql .= $value;
				//dump($sql);die;
				$res = $order_goods->execute($sql);
			}

			/*
			//预存
			if($_REQUEST['stored_value_enable'] == "Y"){
				$customer = M("customer");
				$customer->where("id = '".$_REQUEST['cid']."'")->save(array("stored_value"=>$_REQUEST['stored_values']));
			}
			*/

			//添加订单时，添加一条跟踪信息,记录录音
			$order_action = new Model("order_action");
			$arrF = array(
				"order_id" => $result,
				"action_time" => date("Y-m-d H:i:s"),
				"action_user" => $username,
				"order_status" => "T",
				"pay_status" => "N",
				"shipping_status" => "0",
				"action_description" => "添加订单",
				"uniqueid"=>$_SESSION[$username."_order"][$customer_id],
			);
			//dump($arrF);die;
			$res = $order_action->data($arrF)->add();
			if($res){
				unset($_SESSION[$username."_order"][$customer_id]);
			}


		}
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'订单添加成功！'));
		} else {
			echo json_encode(array('msg'=>'订单添加失败！'));
		}
	}

	function editOrder(){
		checkLogin();
		$id = $_REQUEST['id'];
		$order_info = new Model("order_info");
		$sql = "SHOW COLUMNS FROM order_info";
		$arrF = $order_info->query($sql);
		foreach($arrF as $val){
			$arrT[] = "o.".$val["Field"];
		}
		$fields = implode(",",$arrT).",c.name";

		$orlist = $order_info->table("order_info o")->field($fields)->join("customer c on (o.customer_id=c.id)")->where("o.id = '$id'")->find();
		//$orlist = $order_info->where("id = '$id'")->find();

		$para_sys = readS();
		if($para_sys["editOrder_hide"] =="phone" || $para_sys["editOrder_hide"] == "all" ){
			$orlist["phone_ms"] = $orlist["phone"];
			$orlist["telephone_ms"] = $orlist["telephone"];
			$orlist["phone"] = substr($orlist["phone"],0,3)."***".substr($orlist["phone"],-4);
			if($orlist["telephone"]){
				$orlist["telephone"] = substr($orlist["telephone"],0,3)."***".substr($orlist["telephone"],-3);
			}
			$this->assign("message_phone","ms");
			$this->assign("message_telephone","ms");
		}else{
			$orlist["phone_ms"] = $orlist["phone"];
			$orlist["telephone_ms"] = $orlist["telephone"];
			$this->assign("message_phone","show_phone");
			$this->assign("message_telephone","show_phone");
		}

		if($orlist["email"]){
			if($para_sys["editOrder_hide"] =="email" || $para_sys["editOrder_hide"] == "all" ){
				$orlist["email_ms"] = $orlist["email"];
				$orlist["email"] = "***".strstr($orlist["email"],"@");
				$this->assign("message_email","ms");
			}else{
				$orlist["email_ms"] = $orlist["email"];
				$this->assign("message_email","show_email");
			}
		}
		//dump($orlist);die;

		if($orlist["purchase_time"] == "0000-00-00 00:00:00"){
			$orlist["purchase_time"] = "";
		}
		$this->assign('orlist',$orlist);
		$this->assign('id',$id);

		$this->display();
	}

	function updateOrder(){
		$id = $_REQUEST['id'];
		$username = $_SESSION["user_info"]["username"];
		/*
		$goods_id = $_REQUEST['goods_id'];
		$goods_name = $_REQUEST['goods_name'];
		$name = explode(",",$goods_name);
		foreach($name as $v){
			$price[] = explode("-",$v);
		}
		foreach($price as $vm){
			$prices[] = $vm[1];
		}
		$goods_amount = array_sum($prices);


		$cope_money = $goods_amount - $shipping_fee;
		*/

		if($_REQUEST['message_phone'] == "ms"){
			$phone = $_REQUEST['mess_phone'];
		}else{
			$phone = $_REQUEST['phone'];
		}
		if($_REQUEST['message_telephone'] == "ms"){
			$telephone = $_REQUEST['mess_telephone'];
		}else{
			$telephone = $_REQUEST['telephone'];
		}
		if($_REQUEST['message_email'] == "ms"){
			$email = $_REQUEST['mess_email'];
		}else{
			$email = $_REQUEST['email'];
		}

		$shipping_fee = isset($_REQUEST['shipping_fee']) ? $_REQUEST['shipping_fee'] : "0.00";
		$order_type = $_REQUEST["order_type"];
		//dump($cope_money);
		//dump($name);die;
		$order_info = new Model("order_info");
		$arrData = array(
			//"customer_id"=>$_REQUEST['customer_id'],
			//"consignee"=>$_REQUEST['consignee'],
			"shipping_fee"=>$shipping_fee,
			//"country"=>$_REQUEST['country'],
			"country"=>"1",
			"province"=>$_REQUEST['province'],
			"city"=>$_REQUEST['city'],
			"district"=>$_REQUEST['district'],
			"street"=>$_REQUEST['street'],
			"delivery_address"=>$_REQUEST['delivery_address'],
			"zipcode"=>$_REQUEST['zipcode'],
			"description"=>$_REQUEST['description'],
			"telephone"=>$telephone,
			"phone"=>$phone,
			"email"=>$email,
			"purchase_time"=>$_REQUEST['purchase_time'],
			"order_type"=>$order_type
			//"logistics_account"=>$_REQUEST['logistics_account'],  //物流号
		);
		//dump($arrData);die;
		$result = $order_info->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteOrder(){
		$id = $_REQUEST["id"];
		$order_info = new Model("order_info");
		$result = $order_info->where("id in ($id)")->delete();
		$order_goods = new Model("order_goods");
		$res = $order_goods->where("order_id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败！'));
		}
	}


	function goodsData(){
		$goods_id = $_REQUEST['goods_id'];
		$order_id = $_REQUEST['order_id'];
		$goods = new Model("shop_goods");
		$count = $goods->where("id in ($goods_id)")->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$goodsData = $goods->table("shop_goods g")->order("g.good_order desc")->field("g.id,g.good_name,g.good_num,g.good_Inventory,g.price,g.cate_id,g.brand_id,g.simple_description,g.good_description,g.good_order,g.good_enabled,g.original_img,g.thumb_path,g.good_img,g.img_path,c.cat_name,b.brand_name,og.goods_num,og.goods_money")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->limit($page->firstRow.','.$page->listRows)->join("order_goods og on g.id = og.goods_id")->join("order_info o on o.id = og.order_id")->where("g.id in ($goods_id) AND og.order_id = $order_id")->select();
		//echo $goods->getLastSql();die;
		$rowsList = count($goodsData) ? $goodsData : false;
		$arrProduct["total"] = $count;
		$arrProduct["rows"] = $rowsList;
		//dump($arrProduct);die;
		echo json_encode($arrProduct);
	}

	/*
	function goodsData(){
		$goods_id = $_REQUEST['goods_id'];
		$goods = new Model("shop_goods");
		$count = $goods->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$goodsData = $goods->table("shop_goods g")->order("g.good_order desc")->field("g.id,g.good_name,g.good_num,g.good_Inventory,g.price,g.cate_id,g.brand_id,g.simple_description,g.good_description,g.good_order,g.good_enabled,g.original_img,g.thumb_path,g.good_img,g.img_path,c.cat_name,b.brand_name")->join("shop_category c on g.cate_id = c.id")->join("shop_brand b on g.brand_id = b.id")->limit($page->firstRow.','.$page->listRows)->where("g.id in ($goods_id)")->select();

		$rowsList = count($goodsData) ? $goodsData : false;
		$arrProduct["total"] = $count;
		$arrProduct["rows"] = $rowsList;
		//dump($arrProduct);die;
		echo json_encode($arrProduct);
	}
	*/
	function customerCombox(){
		$username = $_SESSION["user_info"]["username"];
		$dept_id = $_SESSION["user_info"]["d_id"];

		$customer = new Model("customer");
		$custData = $customer->order("createtime desc")->field("id,name as text,phone1")->where("createuser = '$username' OR dept_id = '$dept_id' OR dealuser = '$username' ")->select();
		echo json_encode($custData);
	}

	function shoppingCombox(){
		$goods = new Model("shop_goods");
		$goodsData = $goods->field("id,good_name,price")->select();
		$i = 0;
		foreach($goodsData as $val){
			$goodsData[$i]['text'] = $val['good_name']."-".$val['price'];
			$i++;
		}
		unset($i);
		echo json_encode($goodsData);
	}
}

?>
