<?php
class SalesFunnelAction extends Action{
	//销售漏斗
	function salesFunList(){
		checkLogin();
		header("Content-Type:text/html; charset=utf-8");
		$selectArr = getSelectCache();
		$arrF = $selectArr["grade"];

		foreach($arrF as $key=>$val){
			$arrField[] = array(
				"field"=>"grade_".$key,
				"title"=>$val,
				"width"=>"100",
			);
		}
		$arrFd = array("field"=>"probability_map","title"=>"概率图","width"=>"100");
		$arrFd1 = array("field"=>"name","title"=>"阶段名称","width"=>"100");
		$arrFd2 = array("field"=>"probability","title"=>"所占比例","width"=>"100");
		array_unshift($arrField,$arrFd1);
		array_unshift($arrField,$arrFd);
		$arrT = json_encode($arrField);

		$role = empty($_REQUEST["role"]) ? "Y" : $_REQUEST["role"];
		$this->assign("role",$role);

		$menuname = "Sales Funnel";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function SalesFunnelData(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION["user_info"]["username"];
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];

		$customer = new Model("customer");
		$selectArr = getSelectCache();
		$arrF = $selectArr["grade"];

		foreach($arrF as $key=>$val){
			$arrField[] = "SUM(CASE WHEN grade = '".$key."' THEN 1 ELSE 0 END) AS grade_".$key;
		}
		$field = "SUM(CASE WHEN grade is null OR grade = '' THEN 1 ELSE 0 END) AS grade_0,".implode(",",$arrField).", count(*) as totalNum";

		if($username != "admin"){
			$where = "createuser = '$username' ";
		}else{
			$where = "1 ";
		}

		$where .= " AND recycle = 'N'";
		$where .= empty($start_time)?"":" AND createtime >= '$start_time'";
		$where .= empty($end_time)?"":" AND createtime <= '$end_time'";

		$arrData = $customer->field($field)->where($where)->select();
		$num = array_pop($arrData[0]);
		//echo $customer->getLastSql();die;

		$arrF[0] ="无";
		foreach($arrData[0] as $key=>&$val){
			$tt[] = $val;
			$arrTD[] = array(
				"grade_id"=>array_pop(explode("_",$key)),
				"name"=>$arrF[array_pop(explode("_",$key))],
				"number_share"=>$val,
				$key=>$val,
				"total"=>$num,
				"probability_map"=>sprintf("%.2f", ($val/$num)*100),  //所占比例图  四舍五入
				"probability"=>sprintf("%.2f", ($val/$num)*100)."%",  //所占比例  四舍五入
				//"probability2"=>($val/$num)*100,  //所占比例
				//"probability3"=>number_format(($val/$num)*100,2),  //所占比例 去两位小数
			);
		}
		$rowsList = count($arrTD) ? $arrTD : false;
		$arrT["total"] = count($arrTD);
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);

	}

	function customerData(){
		$grade = $_REQUEST["grade"];
		$name = $_REQUEST["name"];
		$phone = $_REQUEST["phone"];

		$para_sys = readS();

		$customer = new Model("customer");
		if($grade == "0"){
			$where = "(grade is null OR grade = '')";
		}else{
			$where = "grade = '$grade'";
		}
		$where .= " AND recycle = 'N'";
		$username = $_SESSION["user_info"]["username"];
		if($username != "admin"){
			$where .= " AND createuser = '$username' ";
		}
		$where .= empty($name) ? "" : " AND name like '%$name%'";
		$where .= empty($phone) ? "" : " AND phone1 = '$phone'";

		//dump($where);die;
		$count = $customer->where($where)->count();
		import("ORG.Util.Page");
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $customer->field("id,name,phone1,phone2,grade")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$i = 0;
		foreach($arrData as $vm){
			$arrData[$i]["massPhone"] = $vm['phone1'];
			$arrData[$i]["massPhone2"] = $vm['phone2'];
			if($para_sys['hide_rule'] == "role"){
				if($priv["hide_phone"] =="Y"){
					$arrData[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
					if($vm["phone2"]){
						$arrData[$i]["phone2"] = substr($vm["phone2"],0,3)."***".substr($vm["phone2"],-4);
					}
				}
			}else{
				if($para_sys["hide_phone"] =="yes"){
					$arrData[$i]["phone1"] = substr($vm["phone1"],0,3)."***".substr($vm["phone1"],-4);
					if($vm["phone2"]){
						$arrData[$i]["phone2"] = substr($vm["phone2"],0,3)."***".substr($vm["phone2"],-4);
					}
				}
			}
			$i++;
		}



		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

}
?>
