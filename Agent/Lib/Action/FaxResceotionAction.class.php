<?php
class FaxResceotionAction extends Action{
	function faxAll(){
		checkLogin();
		$fax_num = $_SESSION["user_info"]["fax"];
		//dump($_SESSION["user_info"]);
		$fax = new Model("fax_recvq");
		$faxlist = $fax->order("date")->limit($page->firstRow.','.$page->listRows)->where("company_fax='$fax_num'")->select();
		$this->assign("faxlist",$faxlist);
		$this->display();
	}

	function faxAllList(){
		$fax_num = $_SESSION["user_info"]["fax"];

		import('ORG.Util.Page');
		$fax = new Model("fax_recvq");
		$faxcount = $fax->table("fax_recvq r")->field("r.id as id,r.date,r.pdf_file,r.type,r.company_name,r.company_fax,r.faxpath")->join("fax_fax f on r.fax_destiny_id=f.id")->where("f.extension='$fax_num'")->order("date DESC")->select();
		$count = count($faxcount);
		//dump($fax->getLastSql());die;

        $_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$faxlist = $fax->table("fax_recvq r")->field("r.id as id,r.date,r.pdf_file,r.type,r.company_name,r.company_fax,r.faxpath")->join("fax_fax f on r.fax_destiny_id=f.id")->where("f.extension='$fax_num'")->order("date DESC")->limit($page->firstRow.','.$page->listRows)->select();
		//dump($fax->getLastSql());die;
		$status_row = array('in'=>'接收','out'=>'发送');
		$i = 0;
		foreach($faxlist as &$val){
			$status = $status_row[$val["type"]];
			$val["type"] = $status;
			$id = $val["id"];
			$faxpath = $val['faxpath'];
			//$pdf_file = $val['pdf_file'];
			$pdf_file = "fax.pdf";
			$path = "/var/www/faxes/$faxpath/$pdf_file";
			$faxlist[$i]['operations'] = "<a href='agent.php?m=FaxResceotion&a=downloadFax&f={$val['pdf_file']}&path=$path'>"."下载  "."</a>";
			$faxlist[$i]['operations'] .= "<a href='agent.php?m=FaxResceotion&a=deleteFax&f={$val['pdf_file']}&path=$path&id=".$val['id']."'>删除</a>";
			$i++;
		}
		//dump($faxlist);die;
		$rowsList = count($faxlist) ? $faxlist : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
	}

	function deleteFax(){
		$filename = $_GET['f'];
		$recFilePath = $_GET['path'];
		$id = $_GET["id"];
		//dump($recFilePath);die;
		if(file_exists($recFilePath)){
			$msg = unlink($recFilePath);
		}
		$fax = new Model("fax_recvq");
		$result = $fax->where("id = '$id'")->delete();
		if($result){
			goback("删除成功！","","pre");
		}
	}


	function downloadFax(){
        $filename = $_GET['f'];
		$recFilePath = $_GET['path'];

        $timestamp = explode('.',$uniqueid);
        $timestamp = $timestamp[0];
        //$recFilePath = 'c:\\bbb.gif';
        header('HTTP/1.1 200 OK');
        //header('Accept-Ranges: bytes');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download"); // changed to force download
        header("Content-Length: " . (string)(filesize($recFilePath)));
        header("Content-Transfer-Encoding: Binary"); // added
        header("Content-Disposition: attachment;filename=$filename");
        readfile($recFilePath);
    }

	function faxContent(){
		checkLogin();
		$id = $_GET['id'];
		$fax = new Model("Fax_recvq");
		if($id){
			$this->assign("id",$id);
			$faxData = $fax->where("id = $id")->find();
			$this->assign("faxData",$faxData);
		}
		//dump($status);
		$this->display();
	}

	//发送传真的页面
	function sendFax(){
		checkLogin();
		//dump($_SESSION);die;
		$faxExten = $_SESSION['user_info']['fax'];
		if( isset($_POST['to']) ){
			import('ORG.Pbx.SendFax');
			$SendFax = new SendFax();
			$faxExten = $_POST['from']?$_POST['from']:$_SESSION['user_info']['fax'];
			//$faxDevice = "ttyIAX11";
			$faxDevice = exec("grep -l $faxExten /etc/iaxmodem/iaxmodem-cfg.ttyIAX*|cut -d. -f2");
			$faxDest = $_POST['to'];
			$data_content = $_POST['body'];
			if(empty($faxExten)){goBack("该用户没有分配传真分机，请联系管理员");}

			$file_to_send = null;
			if($_POST["option_fax"]=="by_file"){//附件作为传真

				if(is_uploaded_file($_FILES['file_record']['tmp_name'])){
					$file_to_send = $_FILES['file_record']['tmp_name'];

				}else{
					goBack("请选择文件！");
				}
			}else{//文本内容
				if(empty($data_content)){
					goBack("内容为空！");
				}else{
					$file_to_send = $SendFax->generarArchivoTextoPS($data_content);
					if (is_null($file_to_send)) {
						goBack("转换文本失败！");
					}
				}

			}
			//开始发送传真
			$jobid = $SendFax->send($faxDevice, $faxDest, $file_to_send);
			if (is_null($jobid)) {
				$sendMsg = '传真发送失败: '. $SendFax->errMsg;
			}else{
				$sendMsg = '发送成功！';
			}
			$this->assign("faxDest",$faxDest);
			$this->assign("data_content",$data_content);
			$this->assign("faxDisplay",$faxExten);
			if(file_exists($file_to_send)) unlink($file_to_send);
			goBack("发送成功!");
		}else{
			if(empty($faxExten)){
				$this->assign("faxDisplay",'<span class="required">该用户没有分配传真分机</span>');
			}else{
				$this->assign("faxDisplay",$faxExten);
			}

		}

		$this->assign("faxExten",$faxExten);
		$this->display();
	}

}

?>
