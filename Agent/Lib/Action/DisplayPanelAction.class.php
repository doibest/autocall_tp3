<?php
//前台展示面板
class DisplayPanelAction extends Action{
	//待办事项
	function waitmatterList(){
		checkLogin();
		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$this->display();
	}

	function waitmatterData(){
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$username = $_SESSION["user_info"]["username"];
		$wait = new Model("waitmatter");

		$status = $_REQUEST["status"];
		$web_type = $_REQUEST["web_type"];
		$where = "1 ";
		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$deptUser = $this->getDeptUserName();
				$where .= " AND w.username in ($deptUser) ";
			}else{
				$where .= " AND w.username = '$username' ";
			}
		}
		$where .= empty($status) ? "" : " AND status = '$status'";
		//dump($where);die;
		import('ORG.Util.Page');
		$count = $wait->where($where)->count();

        $_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$waitList = $wait->order("createtime desc")->table("waitmatter w")->field("w.id,w.createtime,w.username,w.status,w.remindertime,w.title,w.url,t.waitmattername")->join("waitmattertype t on w.matterstype = t.id")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $wait->getLastSql();die;

		$status_row = array('Y'=>'<span style="color:red;">已办</span>','N'=>'未办');
		$i =0;
		foreach($waitList as &$val){
			$status = $status_row[$val["status"]];
			$val["status"] = $status;
			$waitList[$i]["title"] = "<a target='_blank' style='color:#0066CC' href='".$val['url']."'>". $val["title"] ."</a>";

			$val["operating"] = "<a href='javascript:void(0);' onclick=\"viewWait("."'".$val["id"]."'".")\" >查看</a>";

			$i++;
		}


		//dump($waitList);
		$rowsList = count($waitList) ? $waitList : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
	}

	//查找自己部门及下级部门的工号
	function getDeptUserName(){
		$d_id = $_SESSION['user_info']['d_id'];
		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);
		$deptSet = explode(",",str_replace("'","",rtrim($deptst,",")));
		$userArr = readU();
		$deptUser2 = "";
		foreach($deptSet as $val){
			$deptUser2 .= "'".implode("','",$userArr["deptIdUser"][$val])."',";
		}
		$deptUser = rtrim($deptUser2,",'',")."'";
		//dump($deptSet);die;
		return $deptUser;
	}

	function viewWaitContent(){
		checkLogin();
		$id = $_GET['id'];
		$wait = new Model("waitmatter");
		if($id){
			$this->assign("id",$id);
			$wmList = $wait->where("id = $id")->find();
			$url = $wmList["url"];
			if($url){
				$this->assign("url","url");
			}
			$this->assign("wmList",$wmList);
			$matterstype = $wmList["matterstype"];
			$status = $wmList["status"];
			$this->assign("matterstype",$matterstype);
			$this->assign("stas",$status);

			$waitmattertype = M("waitmattertype");
			$arrF = $waitmattertype->select();
			foreach($arrF as $val){
				$arrT[$val["id"]] = $val["waitmattername"];
			}
			$waitType = $arrT[$matterstype];
			$this->assign("waitType",$waitType);

			//dump($wmList);die;
		}
		$this->display();
	}

	function updateWait(){
		$id = $_REQUEST['id'];
		$waitmatter = M("waitmatter");
		$arrData = array(
			'status' =>$_POST['status'],
		);
		$result = $waitmatter->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	//今日回访
	function visitList(){
		checkLogin();
		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$this->display();
	}


	function visitData(){
		$username = $_SESSION["user_info"]["username"];
		$visittime = date("Y-m-d ")."23:59:59";
		$visittime2 = date("Y-m-d ")."00:00:00";  //只显示今天的回访任务
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$visit_status = $_REQUEST['visit_status'];
		$web_type = $_REQUEST['web_type'];

		$where = "1 ";
		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$deptUser = $this->getDeptUserName();
				$where .= " AND v.visit_name in ($deptUser) ";
			}else{
				$where .= " AND v.visit_name = '$username'";
			}
		}
		$where .= " AND v.visit_time<'$visittime'  AND v.visit_time>'$visittime2'  AND v.visit_type='in' ";
		$where .= empty($startime)?"":" AND v.visit_time >'$startime'";
        $where .= empty($endtime)?"":" AND v.visit_time <'$endtime'";
        $where .= empty($visit_status)?"":" AND v.visit_status = '$visit_status'";


		$visit_customer = new Model("visit_customer");

		$count = $visit_customer->table("visit_customer v")->order("v.visit_time asc")->field("v.id,v.visit_name,v.visit_time,v.customer_id,v.visit_status,c.name,c.phone1,c.phone2,c.email")->join('customer c on v.customer_id = c.id')->where($where)->count();

		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrVisitData = $visit_customer->table("visit_customer v")->order("v.visit_time asc")->field("v.id,v.visit_name,v.visit_time,v.customer_id,v.visit_status,c.name,c.phone1,c.phone2,c.email")->join('customer c on v.customer_id = c.id')->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$i = 0;
		$status_row = array('Y'=>'已回访','N'=>'未回访');
		foreach($arrVisitData as &$val){
			$status = $status_row[$val['visit_status']];
			$val['visit_status'] = $status;

			if($val["name"]){
				$val["name"] = "<a href='javascript:void(0);' onclick=\"openCustomer("."'".$val["customer_id"]."','".$val["id"]."'".")\" >".$val["name"]."</a>";
			}else{
				$val["name"] = "<a href='javascript:void(0);' onclick=\"openCustomer("."'".$val["customer_id"]."','".$val["id"]."'".")\" >查看</a>";
			}
			if($username != "admin"){
				if($para_sys["hide_phone"] =="yes"){
					$val["phone1"] = substr($val["phone1"],0,3)."***".substr($val["phone1"],-4);
					if($val["phone2"]){
						$val["phone2"] = substr($val["phone2"],0,3)."***".substr($val["phone2"],-4);
					}
				}
			}

			//$val["operating"] = "<a href='javascript:void(0);' onclick=\"openCustomer("."'".$val["customer_id"]."','".$val["id"]."'".")\" >查看</a>";
			$i++;
		}

		//echo $visit_customer->getLastSql();
		//dump($arrVisitData);die;
		$rowsList = count($arrVisitData) ? $arrVisitData : false;
		$arrVisit["total"] = $count;
		$arrVisit["rows"] = $rowsList;

		echo json_encode($arrVisit);
	}



	//今日外呼回访任务
	function visitOutboundList(){
		checkLogin();
		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$this->display();
	}

	function visitOutboundData(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION["user_info"]["username"];
		$visittime = date("Y-m-d ")."23:59:59";
		$visittime2 = date("Y-m-d ")."00:00:00";  //只显示今天的回访任务
		$startime = $_REQUEST['startime'];
		$endtime = $_REQUEST['endtime'];
		$visit_status = $_REQUEST['visit_status'];

		$where = "1 ";
		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$deptUser = $this->getDeptUserName();
				$where .= " AND visit_name in ($deptUser) ";
			}else{
				$where .= " AND visit_name = '$username'";
			}
		}
		$where .= " AND visit_time<'$visittime' AND visit_time>'$visittime2' AND visit_type='out'";
		$where .= empty($startime)?"":" AND  visit_time >'$startime'";
        $where .= empty($endtime)?"":" AND  visit_time <'$endtime'";
        $where .= empty($visit_status)?"":" AND  visit_status = '$visit_status'";

		$visit_customer = new Model("visit_customer");
		$count = $visit_customer->where($where)->count();


		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrVisitOutboundData = $visit_customer->order("visit_time desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();


		$sales_task = new Model("sales_task");
		$arrTask = $sales_task->where("enable='Y'")->select();
		foreach($arrTask as $key=>$val){
			$taskName[$val["id"]] = $val["name"];
		}

		$rows = array("Y"=>"已回访","N"=>"未回访");
		$i = 0;
		foreach($arrVisitOutboundData as &$val){
			//$arrVisitOutboundData[$i]["phone"] = "<a target='_blank' style='color:#0066CC' href='agent.php?m=Telemarketing&a=editWCustomer&calltype=preview&task_id=".$val['task_id']."&phone_num=".$val['phone']."&id=".$val['customer_id']."&visit_id=".$val['id']."'>". $val["phone"] ."</a>";
			$arrVisitOutboundData[$i]["visit_status"] = $rows[$val["visit_status"]];
			$arrVisitOutboundData[$i]["task_name"] = $taskName[$val["task_id"]];


			if($username != "admin"){
				if($para_sys["hide_phone"] =="yes"){
					$val["phone1"] = substr($val["phone"],0,3)."***".substr($val["phone"],-4);
				}
			}


			$val["phone"] = "<a href='javascript:void(0);' onclick=\"openOutVisitCustomer("."'".$val["task_id"]."','".$val["phone"]."','".$val["customer_id"]."','".$val["id"]."'".")\" >".$val["phone1"]."</a>";

			$i++;
		}
		unset($i);

		//echo $visit_customer->getLastSql();
		//dump($arrVisitOutboundData);die;
		$rowsList = count($arrVisitOutboundData) ? $arrVisitOutboundData : false;
		$arrVisit["total"] = $count;
		$arrVisit["rows"] = $rowsList;

		echo json_encode($arrVisit);
	}



	//公告
	function noticeList(){
		checkLogin();
		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$this->display();
	}

	function noticeData(){
		$gg = new Model("announcement");
		import('ORG.Util.Page');

		$username = $_SESSION["user_info"]["username"];
		$cn_name = $_SESSION["user_info"]["cn_name"];
		$d_id = $_SESSION["user_info"]["d_id"];
		$r_id = $_SESSION["user_info"]["r_id"];
		$start_time = date("Y-m-d")." 00:00:00";
		$end_time = date("Y-m-d")." 23:59:59";
		$now = date("Y-m-d H:i:s");
		$web_type = $_REQUEST["web_type"];

		$where = "a.endtime >='$start_time'  AND a.starttime <= '$end_time' AND a.an_enabeld = 'Y' AND a.starttime < '$now'";
		if($username != "admin"){
			$where .= " AND find_in_set('$d_id',a.department_id) ";

		}

		$count = $gg->table("announcement a")->field("a.id,a.createtime,a.starttime,a.endtime,a.department_id as dept_id,a.content,a.an_enabeld,a.name,a.noticetype,a.title, t.announcementname")->join("announcementtype t on (a.noticetype = t.id)")->where($where)->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$ggList = $gg->order("a.createtime desc")->table("announcement a")->field("a.id,a.createtime,a.starttime,a.endtime,a.department_id as dept_id,a.content,a.an_enabeld,a.name,a.noticetype,a.title, t.announcementname")->join("announcementtype t on (a.noticetype = t.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		foreach($ggList as &$val){

			$val["operating"] = "<a href='javascript:void(0);' onclick=\"viewNotice("."'".$val["id"]."'".")\" >查看</a>";
		}

		$rowsList = count($ggList) ? $ggList : false;
		$ary["total"] = $count;
		$ary["rows"] = $rowsList;

		echo json_encode($ary);
	}


	function noticeContentList(){
		checkLogin();
		$id = $_GET['id'];
		$gg = new Model("announcement");
		if($id){
			$this->assign("id",$id);
			$ggList = $gg->where("id = $id")->find();
			$this->assign("ggList",$ggList);
		}
		$this->display();
	}


	function getSuperiorsDeptName($arrDep,$dept_id){
		global $dep_str;
		$dep_str .= "'" . $dept_id . "',";
		$pid = $arrDep[$dept_id]['pid'];

		foreach($arrDep as $key=>$val){
			if($key == $pid){
				$this->getSuperiorsDeptName($arrDep,$pid);
			}
		}
		//dump($dep_str);die;
		return $dep_str;

	}
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}
    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
			//dump($arrDepTree);die;
        }
        return $arrDepTree;
    }



	//最近未联系客户
	function noVisitCustomerList(){
		checkLogin();
		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$this->display();
	}

	function noVisitCustomerData(){
		$web_type = $_REQUEST["web_type"];
		$visit_record = new Model("customer");
		$para_sys = readS();
		$noVist_num = $para_sys['no_visit'];
		$username = $_SESSION["user_info"]["username"];
		$where = "1 ";
		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$deptUser = $this->getDeptUserName();
				$where .= " AND createuser in ($deptUser) ";
			}else{
				$where .= " AND createuser = '$username'";
			}
		}
		$visR = $visit_record->order("recently_visittime desc")->field("id,createuser,name,recently_visittime,createtime")->where($where)->select();
		$dttime = date("Y-m-d H:i:s");
		//echo $visit_record->getLastSql();
		//dump($visR);die;
		$i = 0;
		foreach($visR as &$vm){
			if($vm["name"]){
				$vm["name"] = "<a href='javascript:void(0);' onclick=\"viewNoVisit("."'".$vm["id"]."'".")\" >".$vm["name"]."</a>";
			}else{
				$vm["name"] = "<a href='javascript:void(0);' onclick=\"viewNoVisit("."'".$vm["id"]."'".")\" >查看</a>";
			}
			$visR[$i]['interval'] = round( (strtotime($dttime) - strtotime($vm['recently_visittime']) )/3600/24);
			//$visR[$i]["name"] = "<a target='_blank' style='color:#0066CC' href='agent.php?m=CustomerData&a=editCustomer&id=".$vm['id']."'>". $vm["name"] ."</a>";
			if(!$vm["recently_visittime"]){
				$visR[$i]["recently_visittime"] = $vm["createtime"];
			}
			$i++;
		}
		unset($i);
		foreach($visR as $val){
			if($val['interval'] >= $noVist_num){
				$noVisitData[] = $val;
			}
		}
		//dump($noVisitData);die;
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($noVisitData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;

		$disNoVisitData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($noVisitData AS &$v){
			if($i >= $start && $j < $length){
				$disNoVisitData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}
		//dump($disNoVisitData);die;
		$rowsList = count($disNoVisitData) ? $disNoVisitData : false;

		$ary["total"] = $count;
		$ary["rows"] = $rowsList;
		echo json_encode($ary);

	}


	//复购
	function purchaseList(){
		checkLogin();
		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$this->display();
	}

	function purchaseTimeData(){
		$username = $_SESSION['user_info']['username'];
		$para_sys = readS();
		$noVist_num = $para_sys['no_visit'];
		$web_type = $_REQUEST["web_type"];

		$date = date("Y-m-d H:i:s");
		$where = "1 ";
		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$deptUser = $this->getDeptUserName();
				$where .= " AND o.createname in ($deptUser) ";
			}else{
				$where .= " AND o.createname = '$username' ";
			}
		}
		$where .= " AND TIMESTAMPDIFF(DAY,'$date',o.purchase_time) <= '$noVist_num' AND o.purchase_time > '$date'";

		$order_info = new Model("order_info");
		$count = $order_info->table("order_info o")->field("o.id,o.createname,o.purchase_time,o.order_num,o.customer_id,c.phone1,c.name")->join("customer c on (o.customer_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $order_info->table("order_info o")->field("o.id,o.createname,o.purchase_time,o.order_num,o.customer_id,c.phone1,c.name")->join("customer c on (o.customer_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		foreach($arrData as &$val){
			$val["operating"] = "<a href='javascript:void(0);' style='color:red;' onclick=\"viewCustomer("."'".$val["customer_id"]."'".")\" >查看</a>";
		}
		//echo $order_info->getLastSql();
		//dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	//订单
	function orderStatusList(){
		checkLogin();
		$web_type = empty($_REQUEST["web_type"]) ? "agent" : $_REQUEST["web_type"];
		$this->assign("web_type",$web_type);

		$this->display();
	}
	function orderStatusData(){
		$username = $_SESSION['user_info']['username'];
		$web_type = $_REQUEST["web_type"];

		$order_info = M("order_info");
		$where = "view_enable = 'N' ";
		//$where .= " AND ( (order_status in (1,2,4,7,8) AND createname = '$username') OR (stored_value_enable = 'Y' AND approval_enable = 'Y') )";
		$where .= " AND order_status in (1,2,4,7,8)";
		if($username != "admin"){
			if($web_type == "back"){
				//查找自己部门及下级部门的工号
				$deptUser = $this->getDeptUserName();
				$where .= " AND createname in ($deptUser) ";
			}else{
				$where .= " AND createname = '$username' ";
			}
		}
		$count = $order_info->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $order_info->field("id,createname,order_num,order_status,logistics_state")->limit($page->firstRow.','.$page->listRows)->where($where)->select();  //,stored_sk

		$row_order_status = array('1'=>'已通过','2'=>'<span style="color:#FF6600;">取消</span>','3'=>'<span style="color:#FF6600;">待审核</span>','4'=>'<span style="color:red;font-weight:bold;">退货</span>','5'=>'已分单','6'=>'部分分单','7'=>"未通过",'8'=>"问题单");
		$row_stored_sk = array("Y"=>"<span style='color:red;'>通过</span>",""=>"未通过");
		$row_logistics_state = array("0"=>"在途","1"=>"揽件","2"=>"疑难","3"=>"签收","4"=>"退签","5"=>"派件","6"=>"退回","7"=>"转投");

		foreach($arrData as &$val){
			$val["order_status"] = $row_order_status[$val["order_status"]];
			$val["logistics_state"] = $row_logistics_state[$val["logistics_state"]];
			if($val["logistics_state"]){
				$val["order_status"] = $val["order_status"]."/".$val["logistics_state"];
			}
			//$val["stored_sk"] = $row_stored_sk[$val["stored_sk"]];  //预存审批状态
			$val["operating"] = "<a href='javascript:void(0);' style='color:red;' onclick=\"viewOrder("."'".$val["id"]."'".")\" >查看</a>";
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function updateOrderView(){
		$id = $_REQUEST['id'];
		$order_info = new Model("order_info");
		$arrData = array(
			'view_enable'=>"Y"
		);
		$result = $order_info->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	//客户资料
	function getCustomer(){
		$username = $_SESSION['user_info']['username'];
		$customer = new Model("customer");

		$where = "recycle = 'N' ";
		$where .= " AND fenpei_status = 'Y' AND createuser = '$username'";
		$where .= " AND (modifytime is null OR visit_type = 'N')";
		$count = $customer->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		$para_sys = readS();
		if(!$_REQUEST["rows"]){
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $customer->field("id,createuser,phone1,name,sex")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$row_sex = array("man"=>"男","woman"=>"女");
		$menuname = "Customer Data";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		foreach($arrData as &$val){
			$val["sex"] = $row_sex[$val["sex"]];
			$val["massPhone"] = $vm['phone1'];
			$val["massEmail"] = $vm['email'];
			$val["massPhone2"] = $vm['phone2'];


			if($para_sys['hide_rule'] == "role"){
				if($priv["hide_phone"] =="Y"){
					$val["phone1"] = substr($val["phone1"],0,3)."***".substr($val["phone1"],-4);
					if($val["phone2"]){
						$val["phone2"] = substr($val["phone2"],0,3)."***".substr($val["phone2"],-4);
					}
				}
			}else{
				if($para_sys["hide_phone"] =="yes"){
					$val["phone1"] = substr($val["phone1"],0,3)."***".substr($val["phone1"],-4);
					if($val["phone2"]){
						$val["phone2"] = substr($val["phone2"],0,3)."***".substr($val["phone2"],-4);
					}
				}
			}

			$val["operating"] = "<a href='javascript:void(0);' style='color:red;' onclick=\"viewCustomer("."'".$val["id"]."'".")\" >查看</a>";
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}
}
?>
