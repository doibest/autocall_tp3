<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2012 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// $Id: common.php 2799 2012-03-05 07:18:06Z liu21st $

/**
  +------------------------------------------------------------------------------
 * Think 基础函数库
  +------------------------------------------------------------------------------
 * @category   Think
 * @package  Common
 * @author   liu21st <liu21st@gmail.com>
 * @version  $Id: common.php 2799 2012-03-05 07:18:06Z liu21st $
  +------------------------------------------------------------------------------
 */

// 记录和统计时间（微秒）
function G($start,$end='',$dec=4) {
    static $_info = array();
    if(is_float($end)) { // 记录时间
        $_info[$start]  =  $end;
    }elseif(!empty($end)){ // 统计时间
        if(!isset($_info[$end])) $_info[$end]   =  microtime(TRUE);
        return number_format(($_info[$end]-$_info[$start]),$dec);
    }else{ // 记录时间
        $_info[$start]  =  microtime(TRUE);
    }
}

// 设置和获取统计数据
function N($key, $step=0) {
    static $_num = array();
    if (!isset($_num[$key])) {
        $_num[$key] = 0;
    }
    if (empty($step))
        return $_num[$key];
    else
        $_num[$key] = $_num[$key] + (int) $step;
}

/**
  +----------------------------------------------------------
 * 字符串命名风格转换
 * type
 * =0 将Java风格转换为C的风格
 * =1 将C风格转换为Java的风格
  +----------------------------------------------------------
 * @access protected
  +----------------------------------------------------------
 * @param string $name 字符串
 * @param integer $type 转换类型
  +----------------------------------------------------------
 * @return string
  +----------------------------------------------------------
 */
function parse_name($name, $type=0) {
    if ($type) {
        return ucfirst(preg_replace("/_([a-zA-Z])/e", "strtoupper('\\1')", $name));
    } else {
        return strtolower(trim(preg_replace("/[A-Z]/", "_\\0", $name), "_"));
    }
}

// 优化的require_once
function require_cache($filename) {
    static $_importFiles = array();
    if (!isset($_importFiles[$filename])) {
        if (file_exists_case($filename)) {
            require $filename;
            $_importFiles[$filename] = true;
        } else {
            $_importFiles[$filename] = false;
        }
    }
    return $_importFiles[$filename];
}

// 区分大小写的文件存在判断
function file_exists_case($filename) {
    if (is_file($filename)) {
        if (IS_WIN && C('APP_FILE_CASE')) {
            if (basename(realpath($filename)) != basename($filename))
                return false;
        }
        return true;
    }
    return false;
}

/**
  +----------------------------------------------------------
 * 导入所需的类库 同java的Import
 * 本函数有缓存功能
  +----------------------------------------------------------
 * @param string $class 类库命名空间字符串
 * @param string $baseUrl 起始路径
 * @param string $ext 导入的文件扩展名
  +----------------------------------------------------------
 * @return boolen
  +----------------------------------------------------------
 */
function import($class, $baseUrl = '', $ext='.class.php') {
    static $_file = array();
    $class = str_replace(array('.', '#'), array('/', '.'), $class);
    if ('' === $baseUrl && false === strpos($class, '/')) {
        // 检查别名导入
        return alias_import($class);
    }
    if (isset($_file[$class . $baseUrl]))
        return true;
    else
        $_file[$class . $baseUrl] = true;
    $class_strut = explode('/', $class);
    if (empty($baseUrl)) {
        if ('@' == $class_strut[0] || APP_NAME == $class_strut[0]) {
            //加载当前项目应用类库
            $baseUrl = dirname(LIB_PATH);
            $class = substr_replace($class, basename(LIB_PATH).'/', 0, strlen($class_strut[0]) + 1);
        }elseif ('think' == strtolower($class_strut[0])){ // think 官方基类库
            $baseUrl = CORE_PATH;
            $class = substr($class,6);
        }elseif (in_array(strtolower($class_strut[0]), array('org', 'com'))) {
            // org 第三方公共类库 com 企业公共类库
            $baseUrl = LIBRARY_PATH;
        }else { // 加载其他项目应用类库
            $class = substr_replace($class, '', 0, strlen($class_strut[0]) + 1);
            $baseUrl = APP_PATH . '../' . $class_strut[0] . '/'.basename(LIB_PATH).'/';
        }
    }
    if (substr($baseUrl, -1) != '/')
        $baseUrl .= '/';
    $classfile = $baseUrl . $class . $ext;
    if (!class_exists(basename($class),false)) {
        // 如果类不存在 则导入类库文件
        return require_cache($classfile);
    }
}

/**
  +----------------------------------------------------------
 * 基于命名空间方式导入函数库
 * load('@.Util.Array')
  +----------------------------------------------------------
 * @param string $name 函数库命名空间字符串
 * @param string $baseUrl 起始路径
 * @param string $ext 导入的文件扩展名
  +----------------------------------------------------------
 * @return void
  +----------------------------------------------------------
 */
function load($name, $baseUrl='', $ext='.php') {
    $name = str_replace(array('.', '#'), array('/', '.'), $name);
    if (empty($baseUrl)) {
        if (0 === strpos($name, '@/')) {
            //加载当前项目函数库
            $baseUrl = COMMON_PATH;
            $name = substr($name, 2);
        } else {
            //加载ThinkPHP 系统函数库
            $baseUrl = EXTEND_PATH . 'Function/';
        }
    }
    if (substr($baseUrl, -1) != '/')
        $baseUrl .= '/';
    require_cache($baseUrl . $name . $ext);
}

// 快速导入第三方框架类库
// 所有第三方框架的类库文件统一放到 系统的Vendor目录下面
// 并且默认都是以.php后缀导入
function vendor($class, $baseUrl = '', $ext='.php') {
    if (empty($baseUrl))
        $baseUrl = VENDOR_PATH;
    return import($class, $baseUrl, $ext);
}

// 快速定义和导入别名
function alias_import($alias, $classfile='') {
    static $_alias = array();
    if (is_string($alias)) {
        if(isset($_alias[$alias])) {
            return require_cache($_alias[$alias]);
        }elseif ('' !== $classfile) {
            // 定义别名导入
            $_alias[$alias] = $classfile;
            return;
        }
    }elseif (is_array($alias)) {
        $_alias   =  array_merge($_alias,$alias);
        return;
    }
    return false;
}

/**
  +----------------------------------------------------------
 * D函数用于实例化Model 格式 项目://分组/模块
 +----------------------------------------------------------
 * @param string name Model资源地址
  +----------------------------------------------------------
 * @return Model
  +----------------------------------------------------------
 */
function D($name='') {
    if(empty($name)) return new Model;
    static $_model = array();
    if(isset($_model[$name]))
        return $_model[$name];
    if(strpos($name,'://')) {// 指定项目
        $name   =  str_replace('://','/Model/',$name);
    }else{
        $name   =  C('DEFAULT_APP').'/Model/'.$name;
    }
    import($name.'Model');
    $class   =   basename($name.'Model');
    if(class_exists($class)) {
        $model = new $class();
    }else {
        $model  = new Model(basename($name));
    }
    $_model[$name]  =  $model;
    return $model;
}

/**
  +----------------------------------------------------------
 * M函数用于实例化一个没有模型文件的Model
  +----------------------------------------------------------
 * @param string name Model名称 支持指定基础模型 例如 MongoModel:User
 * @param string tablePrefix 表前缀
 * @param mixed $connection 数据库连接信息
  +----------------------------------------------------------
 * @return Model
  +----------------------------------------------------------
 */
function M($name='', $tablePrefix='',$connection='') {
    static $_model = array();
    if(strpos($name,':')) {
        list($class,$name)    =  explode(':',$name);
    }else{
        $class   =   'Model';
    }
    if (!isset($_model[$name . '_' . $class]))
        $_model[$name . '_' . $class] = new $class($name,$tablePrefix,$connection);
    return $_model[$name . '_' . $class];
}

/**
  +----------------------------------------------------------
 * A函数用于实例化Action 格式：[项目://][分组/]模块
  +----------------------------------------------------------
 * @param string name Action资源地址
  +----------------------------------------------------------
 * @return Action
  +----------------------------------------------------------
 */
function A($name) {
    static $_action = array();
    if(isset($_action[$name]))
        return $_action[$name];
    if(strpos($name,'://')) {// 指定项目
        $name   =  str_replace('://','/Action/',$name);
    }else{
        $name   =  '@/Action/'.$name;
    }
    import($name.'Action');
    $class   =   basename($name.'Action');
    if(class_exists($class,false)) {
        $action = new $class();
        $_action[$name]  =  $action;
        return $action;
    }else {
        return false;
    }
}

// 远程调用模块的操作方法
// URL 参数格式 [项目://][分组/]模块/操作 
function R($url,$vars=array()) {
    $info =  pathinfo($url);
    $action  =  $info['basename'];
    $module =  $info['dirname'];
    $class = A($module);
    if($class)
        return call_user_func_array(array(&$class,$action),$vars);
    else
        return false;
}

// 获取和设置语言定义(不区分大小写)
function L($name=null, $value=null) {
    static $_lang = array();
    // 空参数返回所有定义
    if (empty($name))
        return $_lang;
    // 判断语言获取(或设置)
    // 若不存在,直接返回全大写$name
    if (is_string($name)) {
        $name = strtoupper($name);
        if (is_null($value))
            return isset($_lang[$name]) ? $_lang[$name] : $name;
        $_lang[$name] = $value; // 语言定义
        return;
    }
    // 批量定义
    if (is_array($name))
        $_lang = array_merge($_lang, array_change_key_case($name, CASE_UPPER));
    return;
}

// 获取配置值
function C($name=null, $value=null) {
    static $_config = array();
    // 无参数时获取所有
    if (empty($name))   return $_config;
    // 优先执行设置获取或赋值
    if (is_string($name)) {
        if (!strpos($name, '.')) {
            $name = strtolower($name);
            if (is_null($value))
                return isset($_config[$name]) ? $_config[$name] : null;
            $_config[$name] = $value;
            return;
        }
        // 二维数组设置和获取支持
        $name = explode('.', $name);
        $name[0]   =  strtolower($name[0]);
        if (is_null($value))
            return isset($_config[$name[0]][$name[1]]) ? $_config[$name[0]][$name[1]] : null;
        $_config[$name[0]][$name[1]] = $value;
        return;
    }
    // 批量设置
    if (is_array($name)){
        return $_config = array_merge($_config, array_change_key_case($name));
    }
    return null; // 避免非法参数
}

// 处理标签扩展
function tag($tag, &$params=NULL) {
    // 系统标签扩展
    $extends = C('extends.' . $tag);
    // 应用标签扩展
    $tags = C('tags.' . $tag);
    if (!empty($tags)) {
        if(empty($tags['_overlay']) && !empty($extends)) { // 合并扩展
            $tags = array_unique(array_merge($extends,$tags));
        }elseif(isset($tags['_overlay'])){ // 通过设置 '_overlay'=>1 覆盖系统标签
            unset($tags['_overlay']);
        }
    }elseif(!empty($extends)) {
        $tags = $extends;
    }
    if($tags) {
        if(APP_DEBUG) {
            G($tag.'Start');
            Log::record('Tag[ '.$tag.' ] --START--',Log::INFO);
        }
        // 执行扩展
        foreach ($tags as $key=>$name) {
            if(!is_int($key)) { // 指定行为类的完整路径 用于模式扩展
                $name   = $key;
            }
            B($name, $params);
        }
        if(APP_DEBUG) { // 记录行为的执行日志
            Log::record('Tag[ '.$tag.' ] --END-- [ RunTime:'.G($tag.'Start',$tag.'End',6).'s ]',Log::INFO);
        }
    }else{ // 未执行任何行为 返回false
        return false;
    }
}

// 动态添加行为扩展到某个标签
function add_tag_behavior($tag,$behavior,$path='') {
    $array   =  C('tags.'.$tag);
    if(!$array) {
        $array   =  array();
    }
    if($path) {
        $array[$behavior] = $path;
    }else{
        $array[] =  $behavior;
    }
    C('tags.'.$tag,$array);
}

// 过滤器方法
function filter($name, &$content) {
    $class = $name . 'Filter';
    require_cache(LIB_PATH . 'Filter/' . $class . '.class.php');
    $filter = new $class();
    $content = $filter->run($content);
}

// 执行行为
function B($name, &$params=NULL) {
    $class = $name.'Behavior';
    G('behaviorStart');
    $behavior = new $class();
    $behavior->run($params);
    if(APP_DEBUG) { // 记录行为的执行日志
        G('behaviorEnd');
        Log::record('Run '.$name.' Behavior [ RunTime:'.G('behaviorStart','behaviorEnd',6).'s ]',Log::INFO);
    }
}

// 渲染输出Widget
function W($name, $data=array(), $return=false) {
    $class = $name . 'Widget';
    require_cache(LIB_PATH . 'Widget/' . $class . '.class.php');
    if (!class_exists($class))
        throw_exception(L('_CLASS_NOT_EXIST_') . ':' . $class);
    $widget = Think::instance($class);
    $content = $widget->render($data);
    if ($return)
        return $content;
    else
        echo $content;
}

// 去除代码中的空白和注释
function strip_whitespace($content) {
    $stripStr = '';
    //分析php源码
    $tokens = token_get_all($content);
    $last_space = false;
    for ($i = 0, $j = count($tokens); $i < $j; $i++) {
        if (is_string($tokens[$i])) {
            $last_space = false;
            $stripStr .= $tokens[$i];
        } else {
            switch ($tokens[$i][0]) {
                //过滤各种PHP注释
                case T_COMMENT:
                case T_DOC_COMMENT:
                    break;
                //过滤空格
                case T_WHITESPACE:
                    if (!$last_space) {
                        $stripStr .= ' ';
                        $last_space = true;
                    }
                    break;
                case T_START_HEREDOC:
                    $stripStr .= "<<<THINK\n";
                    break;
                case T_END_HEREDOC:
                    $stripStr .= "THINK;\n";
                    for($k = $i+1; $k < $j; $k++) {
                        if(is_string($tokens[$k]) && $tokens[$k] == ';') {
                            $i = $k;
                            break;
                        } else if($tokens[$k][0] == T_CLOSE_TAG) {
                            break;
                        }
                    }
                    break;
                default:
                    $last_space = false;
                    $stripStr .= $tokens[$i][1];
            }
        }
    }
    return $stripStr;
}

// 循环创建目录
function mk_dir($dir, $mode = 0777) {
    if (is_dir($dir) || @mkdir($dir, $mode))
        return true;
    if (!mk_dir(dirname($dir), $mode))
        return false;
    return @mkdir($dir, $mode);
}

//[RUNTIME]
// 编译文件
function compile($filename) {
    $content = file_get_contents($filename);
    // 替换预编译指令
    $content = preg_replace('/\/\/\[RUNTIME\](.*?)\/\/\[\/RUNTIME\]/s', '', $content);
    $content = substr(trim($content), 5);
    if ('?>' == substr($content, -2))
        $content = substr($content, 0, -2);
    return $content;
}

// 根据数组生成常量定义
function array_define($array,$check=true) {
    $content = "\n";
    foreach ($array as $key => $val) {
        $key = strtoupper($key);
        if($check)   $content .= 'defined(\'' . $key . '\') or ';
        if (is_int($val) || is_float($val)) {
            $content .= "define('" . $key . "'," . $val . ');';
        } elseif (is_bool($val)) {
            $val = ($val) ? 'true' : 'false';
            $content .= "define('" . $key . "'," . $val . ');';
        } elseif (is_string($val)) {
            $content .= "define('" . $key . "','" . addslashes($val) . "');";
        }
        $content    .= "\n";
    }
    return $content;
}
//[/RUNTIME]

/*-------------------------------
bgcc common文件
--------------------------------*/
//登录页面 提示跳转函数
Function goBack($msg, $url = '',$type = '') {
	echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">\n";
    if ($url == "exit"){
        //echo "<script language=javascript>\n";
		echo "<script type=\"text/javascript\" charset=\"UTF-8\">\n";
        echo "window.alert('$msg');";
        echo "</script>\n";
        echo $msg;
        exit;
    }
    //echo "<script language=javascript>\n";
	echo "<script type=\"text/javascript\" charset=\"UTF-8\">\n";
    if ($msg != '') {
        echo "window.alert('$msg');";
		//echo "--window.alert('jjlkkk错误ddd');";
    }
    if ($url == "close"){
        echo "self.close();";
    } elseif ($url != '') {
        if (strstr($url, '|')) {
            $url_ary = explode('|', $url);
            echo $url_ary[0] . ".location.href='" . $url_ary[1] . "'\n";
        } else {
            if($type == ""){
                echo "document.location.href='$url'\n";
			}elseif($type == "pre"){
				echo "window.parent.location.reload();";
			}elseif($type == "myself"){
				echo "window.location.reload();";
			}else{
                echo $type.".window.location.href='$url'\n";
			}
        }
    } else {
        echo "window.history.go(-1);";
    }
    echo "</script>\n";
	echo "</head></html>\n";
    exit();
}

function checkPermission($str){
	$user_info = session("user_info");
	$arr = explode(",",$user_info["action_list"]);
	return (in_array($str,$arr));
}

function check_reload() {
	global $amp_conf;	
	$db = M("asterisk.admin");	
	$arr = $db->field("value")->where("variable='need_reload'")->find();
	return ($arr['value'] == 'true' );
}

//重载
function reloadAdd(){
	$db = M("asterisk.admin");	
	$arr = $db->field("value")->where("variable='need_reload'")->find();
	if(!$arr['value'] == 'true' )
		$arr = $db-> where("variable='need_reload'")->setField('value','true');
	else
		$arr = true;
	return $arr;

}

//写文件
function f_xwrite($fpath, $buf, $length = -1) {
    $ret = 0;
    
    if ($length < 0) $length = strlen($buf);
    if ($length == 0) {
        @unlink($fpath);
         return 1;
    }
    
    $fd = @fopen($fpath, "r+");
    if (!$fd) $fd = @fopen($fpath, "w");
    if (!$fd) return $ret;
    flock($fd, LOCK_EX);
    fseek($fd, 0, SEEK_SET);
    if (fwrite($fd, $buf, $length)) {
        ftruncate($fd, $length);
        $ret = 1;
    }
    flock($fd, LOCK_UN);
    fclose($fd); 
    return $ret;
}

//读取部门后按did索引
function getDepartmentsByid(){
	$db = M("bgcrm.department");	
	$arr = $db->select();
	
	
	foreach($arr as $val){
		$tmp[$val["d_id"]] = $val;	
	}
	return $tmp;
}

//读取部门后按dname索引
function getDepartmentsByname(){
	$db = M("bgcrm.department");	
	$arr = $db->select();
	
	
	foreach($arr as $val){
		$tmp[$val["d_name"]] = $val;	
	}
	//dump($tmp['销售部']['d_id']);die;
	return $tmp;
}

//读取角色后按rid索引
function getRoleByid(){
	$db = M("bgcrm.role");	
	$arr = $db->select();
	
	
	foreach($arr as $val){
		$tmp[$val["r_id"]] = $val;	
	}
	return $tmp;
}

//读取角色后按rid索引
function getRoleByname(){
	$db = M("bgcrm.role");	
	$arr = $db->select();
	
	
	foreach($arr as $val){
		$tmp[$val["r_name"]] = $val;	
	}
	return $tmp;
}



//SCCP重载，在添加和更新SCCP话机
function sccpdeviceReload()
{
	$db = M("users");	
	$arr = $db->where("extension_type='SCCP' AND extension_mac !='' AND extension_model")->select();
				
	$g = '[general]
 regcontext=sccpregistration
 servername = Asterisk
 keepalive = 60
 debug = core,event,device,channel
 dateFormat = D.M.Y
 bindaddr = {ipaddress}
 port = 2000
 firstdigittimeout = 16
 digittimeout = 2
 autoanswer_ring_time = 1
 transfer_tone = 0
 callwaiting_tone=0x2d
 musicclass=default
 language=en
 echocancel = on
 silencesuppression = off
 callanswerorder=oldestfirst
 meetme = on
 meetme = qxd
 protocolversion = 11
 regcontext                                                     
 context= from-internal'."\n";
 
	$content = '[SEP{mac}]
 description = {exten}
 addon = {exten}
 devicetype = {exten_model}
 park = off
 button = line,{exten}                                 
 cfwdall = off
 type = device
 keepalive = 60
 transfer = on
 park = on
 cfwdall = off
 cfwdbusy = off
 cfwdnoanswer = off
 pickupexten = off
 pickupcontext =from-internal
 pickupmodeanswer = on
 dtmfmode = inband
 imageversion = P00405000700
 nat = on
 directrtp = on
 earlyrtp = none
 private = on
 mwilamp = on
 mwioncall = off
 meetme = on
 meetmeopts = qd
 setvar=testvar=value
 cfwdall = on
 callwaiting_zone=0
 context=from-internal
 [{exten}]
 id = 1000
 type = line
 pin = 1234
 label = {name}
 description = Line {exten}
 mailbox = {exten}
 cid_name = {name}
 cid_num = {exten}
 accountcode={exten}
 callgroup=1,3-4
 pickupgroup=1,3-5
 incominglimit = 2
 transfer = on
 vmnum = 7000
 meetmenum = 700
 trnsfvm = 1000
 secondary_dialtone_digits = 9
 secondary_dialtone_tone = 0x22
 musicclass =default
 language = en
 audio_tos = 0xB8
 audio_cos = 6
 video_tos = 0x88
 video_cos = 5
 echocancel = on
 silencesuppression = on'."\n";
		 
	import('ORG.Net.NetworkSet');
	
	$vNet = new NetworkSet();
	$arrEths = $vNet->obtener_interfases_red_fisicas();
	$ip = $arrEths["eth0"];
	$g = str_replace("{ipaddress}",$ip["Inet Addr"],$g);
	//print_r($arr);
	foreach($arr as $val){
		$val["extension_mac"]  = strtoupper(str_replace(":","",$val["extension_mac"]));
		$p = str_replace('{name}',$val["en_name"],$content);
		$p = str_replace('{mac}',$val["extension_mac"],$p);
		$p = str_replace('{exten}',$val["extension"],$p);
		$p = str_replace('{exten_model}',$val["extension_model"],$p);
		
		$s = $s ."\r\n".$p;
	}
	
	$buf = $g."\r\n".$s;
			 
	if(f_xwrite("/etc/asterisk/sccp.conf", $buf, $length = -1)){
		echo "ok";
	} else{
		echo "error";
	}
	
	require_once "/var/lib/asterisk/agi-bin/phpagi-asmanager.php";
	$astman = new AGI_AsteriskManager();
	if (!$astman->connect("127.0.0.1", 'admin' , AMIAdmin())){
		$this->errMsg = "Error connect AGI_AsteriskManager";
	}
	$arr = $astman->command("sccp reload");
	
	return $arr;
}

function AMIAdmin($ruta_base='')
{
    import('ORG.Pbx.pbxConfig');
    $pConfig = new paloConfig("/etc", "elastix.conf", "=", "[[:space:]]*=[[:space:]]*");
    $listaParam = $pConfig->leer_configuracion(FALSE);
    if(isset($listaParam["amiadminpwd"]))
        return $listaParam["amiadminpwd"]['valor'];
    else
        return "elastix456";
}

/*
 *导出Excel有关的函数
 * */
function xlsBOF() {
    echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
    return;
}
function xlsEOF() {
    echo pack("ss", 0x0A, 0x00);
    return;
}
function xlsWriteNumber($Row, $Col, $Value) {
    echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);
    echo pack("d", $Value);
    return;
}
function xlsWriteLabel($Row, $Col, $Value ) {
    $L = strlen($Value);
    echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
    echo $Value;
    return;
}
function utf2gb($str){
    return iconv("UTF-8","gb2312",$str);
}

function gb2utf($str){
	return iconv("gb2312","UTF-8",$str);
}

//分页
function BG_Page($count,$size){
	import('ORG.Util.Page');
	$page = new Page($count,$size);
	$page->setConfig("header",L("Records"));
	$page->setConfig("prev",L("Prev"));
	$page->setConfig("next",L("Next"));
	$page->setConfig("first",L("First"));
	$page->setConfig("last",L("Last"));
	$page->setConfig('theme','<span>  %totalRow%%header% &nbsp;  %nowPage%/%totalPage%</span>   &nbsp;%first%  &nbsp;  %upPage%  &nbsp; %linkPage%  &nbsp;  %downPage%  &nbsp;   %end%');
	
	//连mysql中的查询的limit字段也给初始化一次
	$page->limit = $page->firstRow.','.$page->listRows;
	return $page;
}
//AutoCall全局检查是否登录或者登录超时
function checkLogin(){
	if(!$_SESSION["user_info"]){			
		//header("Location: index.php?m=Index&a=Login");
		echo "<script>top.location.href='index.php?m=Index&a=Login';</script>";
	}
}
function checkPurview($menu,$action){
	//if()
	//dump($_SESSION["user_priv"]);die;
	$priv = $_SESSION["user_priv"];
	foreach($priv as $val){
		$actionPriv = $val[$menu];
	}
	return $actionPriv;
	//dump($actionPriv);
}

//将ini格式中的内容读取出来，按纯粹字符串的方式读取出来,不容许做任何修改。其中个#和;表示注释
//返回二维数组
//BUG:不支持有两个等号的行，比如setvar=id=123，这样的解析会出现断章取义
function BG_read_from_ini_format($iniFile){
	$arr = Array();
	$fh = fopen($iniFile,'r') or die("open $iniFile error!");
	
	$key = "";
	$regexp = '/^\s*([^;#]*)\s*[;#]*.*/';
	while( !feof($fh) ){
		$line = trim( fgets($fh) );
		$line = preg_replace($regexp,'$1',$line);
		if(preg_match('/\[.*\]/',$line)){
			$key = preg_replace('/\[(.*)\]/','$1',$line);
			$arr["$key"] = Array();
		}else{
			if(FALSE !== strpos($line,'=')){
				$tmpArr = preg_split('/\s*=\s*/',$line);
				$arr["$key"][$tmpArr[0]] = $tmpArr[1];
			}
		}
	}
	return $arr;
}

//将二维数组写入到ini文件中
function BG_write_to_ini_format($iniFile,$arr){
	$str = "";
	foreach($arr AS $k=>$v){
		$str .= "[$k]\n";
		foreach($v AS $key=>$val){
			$str .= "$key=$val\n";
		}
		$str .= "\n";
	}
	$fh = fopen($iniFile,'w');
	fwrite($fh,$str);
	fclose($fh);
}

//将秒数转化那成"xx分xx秒"的格式
function second2MS($count){
        $m =  floor($count / 60);
        $s =  $count % 60;
        //return $h ."Hour" .$m ."Minute" .$s ."Secend";
		return $m ."分" .$s ."秒";

}

//将秒数转化那成"xx时xx分xx秒"的格式
function second2HMS($count){
        $h =  floor($count / 3600);
        $h_last =  $count % 3600;
        $m =  floor($h_last / 60);
        $m_last =  $h_last % 60;
        $s =  $m_last;
        //return $h ."Hour" .$m ."Minute" .$s ."Secend";
		return $h ."时" .$m ."分" .$s ."秒";

}

//生成用户和部门的缓存文件
function generateArrConf(){
	//工号缓存
	$U = M('users');
	$res_U = $U->field("username,extension,d_id,cn_name")->where('extension IS NOT NULL')->select();
	$arrCacheWorkNo = Array();
	foreach( $res_U AS $user ){
		$key = (string)($user['username']);
		$arrCacheWorkNo[$key] = Array();
		$arrCacheWorkNo[$key]['cn_name'] = $user['cn_name']; //姓名
		$arrCacheWorkNo[$key]['extension'] = $user['extension']; //分机
		$arrCacheWorkNo[$key]['dept_id'] = $user['d_id']; //所属部门id
	}
	
	//部门缓存
	$D = M('department');
	$res_D = $D->table('department a')->field("a.d_id AS dept_id ,a.d_name AS dept_name ,a.d_pid AS dept_pid ,b.d_name AS dept_pname ,a.d_leader AS dept_leader")->join("LEFT JOIN department b ON a.d_pid=b.d_id")->select();
	$arrCacheDept = Array();
	foreach( $res_D AS $dept ){
		$key = (string)($dept['dept_id']);
		$arrCacheDept[$key] = Array();
		$arrCacheDept[$key]['dept_name'] = $dept['dept_name']; //部门名称
		$arrCacheDept[$key]['dept_pid'] = $dept['dept_pid']; //部门父ID
		$arrCacheDept[$key]['dept_pname'] = $dept['dept_pname']; //部门父ID名称
		$arrCacheDept[$key]['dept_leader'] = $dept['dept_leader']; //部门领导
	}
	//dump($arrCacheWorkNo);dump($arrCacheDept);die;
	$strOutput = '<?php' ."\n\n" .'/*工号信息缓存*/' ."\n" .'$arrCacheWorkNo = ' .var_export($arrCacheWorkNo,true) .";\n\n\n" .'/*部门信息缓存*/' ."\n" .'$arrCacheDept = ' .var_export($arrCacheDept,true) ."\n" .'?>';
	
	$db_name = $_SESSION['db_name'];
	file_put_contents("/var/www/html/BGCC/Conf/crm/$db_name/arrCache.php",$strOutput);
	//file_put_contents("/var/www/html/BGCC/Conf/arrCache.php",$strOutput);
}

//把数组转换成，生成SQL查询语句中的IN的字符串的函数。
// $elementType = 'string' or 'number'
function gen_SQL_IN($arr,$elementType='string'){
    if($elementType=='string'){
        $s = "'";
    }elseif($elementType='number'){
        $s = "";
    }
    $str = " (" . $s;
    $str .= implode("$s,$s",$arr);
    $str .= $s .") ";
    return $str;
}

//发送邮件
function sendMail($fromemail,$title,$content){
	import("ORG.Email.Phpmailer");
	$para_sys = readS();  //读取缓存
	$userEmail = $para_sys["mail_from"];
	$emailPassword = $para_sys["mail_password"];
	$smtpHost = $para_sys["mail_host"];
	$mail_user = $para_sys["mail_user"];
	$mail_fromname = $para_sys["mail_fromname"];

	$mail = new PHPMailer(); //建立邮件发送类
	$mail->IsSMTP(); // 使用SMTP方式发送
	$mail->CharSet='UTF-8';// 设置邮件的字符编码
	$mail->Host = $smtpHost;
	$mail->SMTPAuth = true; // 启用SMTP验证功能
	$mail->Username = $mail_user;   //邮件服务器用户  发件人地址
	$mail->Password = $emailPassword; //发件人邮件密码
	$mail->From = "$userEmail"; //邮件发送者email地址
	$mail->FromName = $mail_fromname;   //邮件发送者名称
	$mail->AddAddress($fromemail);   //收件人地址
	//$mail->AddAttachment('BGCC/Conf/1.3万条数据.xls','1.3万条数据.xls'); // 添加附件,并指定名称 
	$mail->IsHTML(true); //支持html格式内容     如果要发送附件这个选项必须为false【$mail->IsHTML(false);】
	//$mail->AddEmbeddedImage("logo.jpg", "my-attach", "logo.jpg"); //设置邮件中的图片 

	$mail->Subject = $title; //邮件标题
	$mail->Body = $content; //邮件内容
	if(!$mail->Send()){
		echo "邮件发送失败. <p>";
		echo "错误原因: " . $mail->ErrorInfo;
		$ms = $mail->ErrorInfo;
		$mgs = "User not found: ".$fromemail;
		$mess = substr_count($ms,$mgs);
		if( $mess ){
			echo "<script>alert('找不到该邮件地址，请填写有效地邮件地址！');</script>";
		}
		return false;
	}else{
		return "sendmail success";
	}
}


//发送邮件
function AgentSendMail($fromemail,$title,$content,$para_sys){
	import("ORG.Email.Phpmailer");
	$userEmail = $para_sys["mail_from"];
	$emailPassword = $para_sys["mail_password"];
	$smtpHost = $para_sys["mail_host"];
	
	$mail = new PHPMailer(); //建立邮件发送类
	$mail->IsSMTP(); // 使用SMTP方式发送
	$mail->CharSet='UTF-8';// 设置邮件的字符编码
	$mail->Host = $smtpHost;
	$mail->SMTPAuth = true; // 启用SMTP验证功能
	$mail->Username = $userEmail;   //发件人地址
	$mail->Password = $emailPassword; //发件人邮件密码
	$mail->From = "$userEmail"; //邮件发送者email地址
	$mail->FromName = $title;
	$mail->AddAddress($fromemail);   //收件人地址

	$mail->Subject = $mail->FromName; //邮件标题
	$mail->Body = $content; //邮件内容
	if(!$mail->Send()){
		echo "邮件发送失败. <p>";
		echo "错误原因: " . $mail->ErrorInfo;
		$ms = $mail->ErrorInfo;
		$mgs = "User not found: ".$fromemail;
		$mess = substr_count($ms,$mgs);
		if( $mess ){
			echo "<script>alert('找不到该邮件地址，请填写有效地邮件地址！');</script>";
		}
		return false;
	}else{
		return "sendmail success";
	}
}

//判断浏览器类型
function browserType(){
	$firebox_IE = $_SERVER["HTTP_USER_AGENT"];  //判断浏览器类型
	$fire = strpos($firebox_IE,"Gecko");   //火狐浏览器
	$google = strpos($firebox_IE,"WebKit");   //谷歌浏览器
	$IE = strpos($firebox_IE,"Trident");   //IE、360浏览器
	$faq = new Model("faq_content");
	if( $fire || $google ){
		return "fire";
	}
	if( $IE ){
		return "ie360";
	}
}


//发送短信
/*
$code的值：
2000 "Command complateted successfully" //操作成功
3000 "Datebase error"//数据库错误(插入\删除等)
4000 "Client error"//客户未知错误
4001 "Xml parse error"//xml错误
4002 "Authorization error"//授权错误(用户不存在、密码错误、权限不足等)
4003 "No data post to server"
4004 "Object does not exist"//函数不存在
4300 "Account error"//财务错误
4400 "Parameter error"//参数错误
5000 "Server error"
5001 "Unable connet to remote server"
5002 "Server no data return"
5003 "Server other error"
6000 "optation error"//操作错误，如不能添加、删除文件等
*/
function asterSendSMS($mobile, $message2, $time){
	session_start();
	$message = str_replace(array("\r\n","\n\r", "\r", "\n"), "", $message2);;
	import("ORG.Sms.Sms");
	$newclient=new SMS();
	//dump($time)
	if($newclient->ConfNull=="1"){
		//$apitype = $_POST[apitype];; // $apitype 通道选择 0：默认通道； 2：通道2； 3：即时通道；
		//$respxml=$newclient->sendSMS($mobile, $message, $time, $apitype);
		$respxml=$newclient->sendSMS($mobile, $message, $time);
		
		$_SESSION["xml"]=$newclient->sendXML;
		$_SESSION["respxml"]=$respxml;
		$code=$newclient->getCode();
		
		if($code == "2000"){
			return "2000";
		}elseif($code == "4002"){
			return "4002";
			//goback("短信发送失败,短信平台授权错误(用户不存在、密码错误、权限不足等)","agent.php?m=SmsSetting&a=sendSmsData");
		}else{
			return $code;
		}
	}else{
		$code = "<font color='red'>失败</font>";
		$ermess = "<font color='red'>你还没有配置文件Sms/api/config.inc.php</font>";
		$error = "<font color='red'>失败</font>";
	}
}



//短信用户余额查询
function asterAccountSMS(){
	session_start();
	import("ORG.Sms.Sms");
	$newclient=new SMS();

	$respxml=$newclient->infoSMSAccount();

	$_SESSION["xml"]=$newclient->sendXML;
	$_SESSION["respxml"]=$respxml;
	$code=$newclient->getCode();
	$respArr=$newclient->toArray();
	return $respArr;
}


function secendHMS($intSec){
   return sprintf("%02d",intval($intSec/3600)).":".sprintf("%02d",intval(($intSec%3600)/60)).":".sprintf("%02d",intval((($intSec%3600)%60)));
}


function exportDataFunction($count,$field,$title,$arrData,$excelTiele){
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Type:text/html;charset=UTF-8");
	$filename = iconv("utf-8","gb2312",$excelTiele);
	header("Content-Disposition: attachment;filename=$excelTiele.xls ");
	header("Content-Transfer-Encoding: binary ");

	xlsBOF();
	$start_row	=	0;

	//设置表 标题内容
	for($i=0;$i<$count;$i++){
		xlsWriteLabel($start_row,$i,utf2gb($title[$i]));
	}
	
	$start_row++;
	foreach($arrData as &$val){
		for($j=0;$j<$count;$j++){
			 xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
		}
		$start_row++;
	}
	xlsEOF();
}



//问卷内容答案
function getQuestionnaireBankContent(){
	$wj_questionnaire_bank = new Model("wj_questionnaire_bank");
	$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,p.topic_name,p.topic_type";
	$fieldData = $wj_questionnaire_bank->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where("p.topic_type in (1,2,3,6)")->select();
	
	foreach($fieldData as $val){
		$val["questionnaire_content"] = json_decode($val["questionnaire_content"] ,true);
		$selectTpl[] = $val;
	}
	foreach($selectTpl as $v){
		foreach($v['questionnaire_content'] as $vm){
			$tmp[$v['id']][] = array($vm[0]=>$vm[1]);
		}
	}
	foreach($tmp As $key=>&$val){
		$arr = array();
		foreach($val AS $value){
			foreach($value as $k=>$v){
				$arr[$k] = $v;
			}
		}
		$val = $arr;
	} 
	
	return $tmp;
}

//根据id获取问卷名称
function getQuestionnaireIDName(){
	$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
	$arrData = $wj_questionnaire_generation->select();
	foreach($arrData as $key=>$val){
		$arrF[$val["id"]] = $val["questionnaire_name"];
	}
	
	return $arrF;
}


//获取流程节点所显示的表单字段
function getCustomerField(){
	$work_process_design_node = new Model("work_process_design_node");
	$arrData = $work_process_design_node->select();
	
	foreach($arrData as &$val){
		$val["writable_fields"] = json_decode($val["writable_fields"],true);
		$val["num"] = $val["process_design_id"]."_".$val["id"]."_".$val["order_num"];   //流程id_流程节点id_节点序号
		$arrF[$val["num"]] = $val["writable_fields"];
	}
	
	return $arrF;
}

//表单数量
function getFormNum(){
	$work_form_design = new Model("work_form_design");
	$arrData = $work_form_design->field("form_category_id,count(*) as num")->group("form_category_id")->select();
	foreach($arrData as $val){
		$arrF[$val["form_category_id"]] = $val["num"];
	}
	return $arrF;	
}

function groupBy($arr, $key_field){
	$ret = array();
	foreach ($arr as $row){
		$key = $row[$key_field];
		$ret[$key][] = $row;
	}
	return $ret;
}

//获取表单字段
function getFormField(){
	//header("Content-Type:text/html; charset=utf-8");
	$custom_fields = new Model("custom_fields");
	$fieldData = $custom_fields->order("field_order asc")->select();
	$arrData = groupBy($fieldData,"table_name");
	//dump($arrData);die;
	
	return $arrData;
}

//物流方式
function logisticsMode(){
	//header("Content-Type:text/html; charset=utf-8");
	$logistics_mode = M("logistics_mode");
	$arrData = $logistics_mode->order("id asc")->field("id,logistics_name as name")->select();
	foreach($arrData as $val){
		$arrF[$val["id"]] = $val["name"];
	}
	return $arrF;
}


/*
* 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
*/
function getDepTreeData(){
	$DepTree = array();//一维数组
	$dep = M('Department');
	$arr = $dep->select();
	foreach($arr AS $v){
		$currentId = $v['d_id'];
		$arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
		$strId = "$currentId";
		foreach($arrSonId AS $row){
			$strId .= "," . $row['d_id'];
		}
		$arrDepTree[$currentId] = Array(
			"id" => $v['d_id'],
			"pid" => $v['d_pid'],
			"name"=> $v['d_name'],
			"meAndSonId"=>$strId,
		);
	}
	//dump($arrDepTree);die;
	return $arrDepTree;
}
function getMeAndSubDeptID($arrDep,$dept_id){
	$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
	//$str = "'" . $arrDep[$dept_id]['name'] . "',";
	$str = "'" . $arrDep[$dept_id]['id'] . "',";
	if( array_shift($arrId) ){
		foreach( $arrId AS $id ){
			$str .= getMeAndSubDeptID($arrDep,$id);
		}
	}
	return $str;
	
}


//读取缓存中的变量
function readS(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$config = array();
	if(file_exists("BGCC/Conf/crm/$db_name/system.php")){
		$config = require "BGCC/Conf/crm/$db_name/system.php";	
	}
	return $config;	
}
//读取用户缓存
function readU(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$config = array();
	if(file_exists("BGCC/Conf/crm/$db_name/users.php")){
		$config = require "BGCC/Conf/crm/$db_name/users.php";	
	}
	return $config;	
}
function getArrCache(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$arrConfig = array();	
	if(file_exists("BGCC/Conf/crm/$db_name/arrCache.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/arrCache.php";	
	}
	return $arrConfig;	
}

function getEmailSmtp(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$arrConfig = array();	
	if(file_exists("BGCC/Conf/crm/$db_name/emailSmtp.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/emailSmtp.php";	
	}	
	return $arrConfig;	
}

function getFieldCache(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$arrConfig = array();	
	if(file_exists("BGCC/Conf/crm/$db_name/fieldCache.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/fieldCache.php";	
	}	
	return $arrConfig;	
}

function getSelectCache(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$arrConfig = array();	
	if(file_exists("BGCC/Conf/crm/$db_name/selectCache.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/selectCache.php";	
	}	
	return $arrConfig;	
}

function getMark(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$arrConfig = array();	
	if(file_exists("BGCC/Conf/crm/$db_name/mark.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/mark.php";	
	}	
	return $arrConfig;	
}

function getWeChatSet(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];	
	$arrConfig = array();	
	if(file_exists("BGCC/Conf/crm/$db_name/weChatSet.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/weChatSet.php";	
	}	
	return $arrConfig;	
}

function getShiftDefinition(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];	
	$arrConfig = array();	
	if(file_exists("BGCC/Conf/crm/$db_name/shiftDefinition.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/shiftDefinition.php";	
	}	
	return $arrConfig;	
}

function getShiftSet(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];	
	$arrConfig = array();	
	if(file_exists("BGCC/Conf/crm/$db_name/shiftSet.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/shiftSet.php";	
	}	
	return $arrConfig;	
}

function getProposalMessage(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];	
	$arrConfig = array();	
	if(file_exists("BGCC/Conf/crm/$db_name/proposalMessage.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/proposalMessage.php";	
	}	
	return $arrConfig;	
}

function getCondition(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	if(file_exists("BGCC/Conf/crm/$db_name/condition.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/condition.php";	
	}else{
		$arrConfig = "";
	}
	return $arrConfig;	
}
function getLoginUser(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	if(file_exists("BGCC/Conf/crm/$db_name/loginUser.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/loginUser.php";	
	}else{
		$arrConfig = "";
	}
	return $arrConfig;	
}
function getWeChatAnswer(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	if(file_exists("BGCC/Conf/crm/$db_name/weChatAnswer.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/weChatAnswer.php";	
	}else{
		$arrConfig = "";
	}
	return $arrConfig;	
}
function getQuestionnaireID(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$arrConfig = array();
	if(file_exists("BGCC/Conf/crm/$db_name/questionnaireID.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/questionnaireID.php";	
	}else{
		$arrConfig = "";
	}
	return $arrConfig;	
}
function getConcurrentTask(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$arrConfig = array();
	if(file_exists("BGCC/Conf/crm/$db_name/concurrentTask.php")){
		$arrConfig = require "BGCC/Conf/crm/$db_name/concurrentTask.php";	
	}else{
		$arrConfig = "";
	}
	return $arrConfig;	
}
function getLogoImg(){
	$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
	$arrConfig = array();
	if(file_exists("BGCC/Conf/logo_img.php")){
		$arrConfig = require "BGCC/Conf/logo_img.php";	
	}else{
		$arrConfig = "";
	}
	return $arrConfig;	
}

//获取企业超级管理员
function getAdministratorNum(){
	$arrData = array('admin');
	$tenants = M("bgcrm.tenants");
	$arrF = $tenants->field("id,enterprise_number")->select();
	foreach($arrF as $val){
		$arrData[] = $val["enterprise_number"];
	}
	return $arrData;
}



//获取租户信息
function getTenantsInfo(){
	$tenants = M("bgcrm.tenants");
	$arrData = $tenants->select();
	foreach($arrData as $key=>$val){
		$arrF[$val["enterprise_number"]] = array(
			"lease_time"=>array(
				"start_time"=>$val["start_lease_time"],
				"end_time"=>$val["end_lease_time"]
			),
			"working_period"=>array(
				"start_working_period"=>$val["start_work_number"],
				"end_working_period"=>$val["end_work_number"]
			),
			"extension_period"=>array(
				"start_extension_period"=>$val["start_extension"],
				"end_extension_period"=>$val["end_extension"]
			),
			//"role_list" => $arrm[$val["role_id"]],
			"role_id" => $val["role_id"],
			"max_concurrent" => $val["max_concurrent"],
			"id" => $val["id"],
			"company_name" => $val["company_name"],
		);
	}
	//dump($arrF);die;
	return $arrF;
}

function readBalance(){
	$file_name = "/var/tmp/tenant/stored_value";
	$arrF = array();
	if(file_exists($file_name)){
		$arrF = file_get_contents($file_name);
		import('ORG.Util.EncryptionToDecrypt');
		$pwdencode = new PwdEncode();
		$key = md5("sai");
		$arrD = $pwdencode->authcode($arrF,'DECODE',$key,0);
		$arrData = json_decode($arrD,true);
		//dump($arrD);die;
	}else{
		$arrData = array();
	}
	return $arrData;
}


function getHttpPost($url,$param){
	$oCurl = curl_init();
	if(stripos($url,"https://")!==FALSE){
		curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
	}
	if (is_string($param)) {
		$strPOST = $param;
	} else {
		$aPOST = array();
		foreach($param as $key=>$val){
			$aPOST[] = $key."=".urlencode($val);
		}
		$strPOST =  join("&", $aPOST);
	}
	curl_setopt($oCurl, CURLOPT_URL, $url);
	curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt($oCurl, CURLOPT_POST,true);
	curl_setopt($oCurl, CURLOPT_POSTFIELDS,$strPOST);
	$sContent = curl_exec($oCurl);
	$aStatus = curl_getinfo($oCurl);
	//dump($sContent);
	//dump($aStatus);
	curl_close($oCurl);
	if(intval($aStatus["http_code"])==200){
		return $sContent;
	}else{
		return false;
	}
}



function array_sort($arr,$keys,$type='asc',$old_key="yes"){
	$keysvalue = $new_array = array();
	foreach ($arr as $k=>$v){
		$keysvalue[$k] = $v[$keys];
	}
	if($type == 'asc'){
		asort($keysvalue);
	}else{
		arsort($keysvalue);
	}
	reset($keysvalue);
	foreach ($keysvalue as $k=>$v){
		if($old_key == "yes"){
			$new_array[$k] = $arr[$k];
		}else{
			$new_array[] = $arr[$k];
		}
	}
	return $new_array;
} 

//创建多级目录
function mkdirs($dir){
	if(!is_dir($dir)){
		if(!mkdirs(dirname($dir))){
			return false;
		}
		if(!mkdir($dir,0777)){
			return false;
		}
	}
	return true;
}


//获取坐席权限
function getTenantRoles(){
	$mod = M("bgcrm.role");
	$arrData = $mod->select();
	$arrF = array();
	foreach($arrData as &$val){
		$val["action_list"] = json_decode($val["action_list"],true);
		foreach($val["action_list"] as $k=>$v){
			$arrK[] = $k;
			foreach($val["action_list"][$k] as $km=>$vm){
				$arrK[] = $km;
			}
		}
		$val["action_key"] = $arrK;
		$arrF[$val["r_id"]] = $val["action_key"];
		unset($arrK);
	}
	//dump($arrF);die;
	return $arrF;
}
