<?php
class SendFax {
    var $errMsg;

    function SendFax(&$pDB)
    {
    }

    function generarArchivoTextoPS(&$data_content)
    {
        // Si el contenido es ASCII se escribe directamente al archivo
        $bEsAscii = TRUE;
        foreach (str_split($data_content) as $c) if (ord($c) >= 127) {
             $bEsAscii = FALSE; break;
        }
        if ($bEsAscii) {
            $ruta_archivo = tempnam('/tmp', 'data_');
            file_put_contents($ruta_archivo, $data_content);
            return $ruta_archivo;
        }
        
        /* El contenido a escribir no es ASCII. Ya que la p��gina web emite 
         * UTF-8, se asumir�� que el contenido est�� tambi��n codificado en UTF-8
         * (verificado en Firefox 16 e Internet Explorer 6). 
         * 
         * El c��digo de abajo es necesario debido a que
         * 1) /usr/bin/sendfax no reconoce como texto un archivo en codificaci��n
         *    distinta de ASCII
         * 2) /usr/sbin/textfmt s��lo puede convertir desde una fuente ISO-8859-15
         */
        $ruta_temp = tempnam('/tmp', 'data_');
        file_put_contents($ruta_temp, iconv('UTF-8', 'ISO-8859-15//TRANSLIT', $data_content));
        $ruta_archivo = tempnam('/tmp', 'data_');
        $output = $retval = NULL;
        exec('/usr/sbin/textfmt -B -f Courier-Bold -Ml=0.4in -p11 < '.
            escapeshellarg($ruta_temp).' > '.escapeshellarg($ruta_archivo),
            $output, $retval);
        unlink($ruta_temp);

        return ($retval == 0) ? $ruta_archivo : NULL;
    }

    /*HERE YOUR FUNCTIONS*/
    function send($faxexten, $destine, $data)
    {
        $faxhost = escapeshellarg("$faxexten@127.0.0.1");
        $destine = escapeshellarg($destine);
        $data = escapeshellarg($data);
        $output = $retval = NULL;
		//dump("sendfax -D -h $faxhost -n -d $destine $data 2>&1");die;
        exec("sendfax -D -h $faxhost -n -d $destine $data 2>&1", $output, $retval);
        $regs = NULL;
        if ($retval != 0 || !preg_match('/request id is (\d+)/', implode('', $output), $regs)) {
            $this->errMsg = implode('<br/>', $output);
            return NULL;
        }
        return $regs[1];
    }
}
?>