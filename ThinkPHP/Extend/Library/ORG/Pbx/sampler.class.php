<?php


class Sampler {

    var $rutaDB;
    var $errMsg;
    var $_db;

    function Sampler()
    {
        global $arrConf;
        $this->rutaDB = $arrConf['dsn']['samples'];
        $pDB = new paloDB($this->rutaDB);
	    if(!empty($pDB->errMsg)) {
        	echo "$pDB->errMsg <br>";
		}else{
			$this->_db = $pDB;
		}
    }

    function insertSample($idLine, $timestamp, $value)
    {
        $this->errMsg='';
        $sqliteError = '';
        $query = "INSERT INTO samples (id_line, timestamp, value) values ($idLine, '$timestamp', '$value')";
        $bExito = $this->_db->genQuery($query);
        if (!$bExito) {
            $this->errMsg = $this->_db->errMsg;
        }
    }

    function getSamplesByLineId($idLine) 
    {
        $this->errMsg='';
        $query = "SELECT timestamp, value FROM samples WHERE id_line='$idLine'";
		//$query = "SELECT timestamp, value FROM samples WHERE id_line='$idLine' ORDER BY `timestamp` desc";
        $arrayResult = $this->_db->fetchTable($query, TRUE);
        if (!$arrayResult){
            $this->errMsg = $this->_db->errMsg;
            return array();
        }
        return $arrayResult;
    }

    function getGraphLinesById($idGraph)
    {
        $this->errMsg='';
        $arrReturn=array();
        $sqliteError='';
        $query  = "SELECT l.id as id, l.name as name, l.color as color, l.line_type as line_type ";
        $query .= " FROM graph_vs_line as gl, line as l WHERE gl.id_line=l.id AND gl.id_graph='$idGraph'";

        $arrayResult = $this->_db->fetchTable($query, TRUE);
        if (!$arrayResult){
            $this->errMsg = "It was not possible to obtain information about the graph - ".$this->_db->errMsg;
            return array();
        }
        return $arrayResult;
    }

    function getGraphById($idGraph)
    {
        $this->errMsg='';
        $sqliteError='';
        $query  = "SELECT name FROM graph WHERE id='$idGraph'";

        $arrayResult = $this->_db->getFirstRowQuery($query, TRUE);
        if (!$arrayResult){
            $this->errMsg = "It was not possible to obtain information about the graph - ".$this->_db->errMsg;
            return array();
        }
        return $arrayResult;
    }

    function deleteDataBeforeThisTimestamp($timestamp)
    {
        $this->errMsg='';
        $sqliteError='';
        if(empty($timestamp)) return false;
        $query = "DELETE FROM samples WHERE timestamp<=$timestamp";
        $bExito = $this->_db->genQuery($query);
        if (!$bExito) {
        	$this->errMsg = $this->_db->errMsg;
        	return false;
        }
		return true;
    }
}
?>
