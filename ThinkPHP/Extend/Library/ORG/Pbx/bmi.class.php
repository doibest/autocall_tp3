<?php
/*
+------------------------------------------------------------------------------------------
| 统一工具类,取代框架中其他所有的语音接口操作，本项在具体实施过程将致于加密，并植入phplib中
| ============================
| create by bangian_YangZhang(2008-09-14)
| 2008-09-24增加拔打电话的方法call
| 2008-09-25  重写弹屏接口
|
+------------------------------------------------------------------------------------------
*/
Class bmi {
	var $asm;
	var $soap;
	function Close()
	{
		global $asm;
		$this->asm->disconnect();
		unset($asm);
	}

	function Exec($str)
	{
		$this->loadAGI();
		return $this->asm->Command($str);
	}

	function loadMemCache($debug = false)
	{
		global $mc;
        if (!isset($mc)) {
            //include_once ("/var/www/html/bgcrm/lib/memcached-client.php");
			$options = array(
			    'servers' => array('127.0.0.1:11211'), //memcached 服务的地址、端口，可用多个数组元素表示多个 memcached 服务
			    'debug' => $debug,  //是否打开 debug
			    'compress_threshold' => 10240,  //超过多少字节的数据时进行压缩
			    'persistant' => false  //是否使用持久连接
		    );
			//创建 memcached 对象实例
			$mc = new memcached($options);
        }
        return $mc;
	}

/**
 * 载入agi
 *
 * zhangtuo added
 * 2010-3-1
 */
	function loadAGI(){
		global $asm;
		if (!isset($asm)) {
						if( $this->soap == "yes" ){ //soap调用
            	include_once ("/var/lib/asterisk/agi-bin/phpagi-asmanager.php");

            }else{
            	include_once ("/var/lib/asterisk/agi-bin/phpagi-asmanager.php");
            }

            $this->asm = new AGI_AsteriskManager();
            if(!$this->asm->connect(C('PBX_HOST'), C('PBX_USER'),C('PBX_PWD')))
            {
                echo "无法连接管理接口，退出.";
                return NULL;
            }
        }
	}


	function getSipTrunkStatus()
	{
		$this->loadAGI();
		if($status == 0){ //示忙
            $rs = $this->asm->Command("sip show registry");
			$lines 	= explode("\n", $rs['data']);

			foreach($lines as $key => $line)
			{

					if(!preg_match("/dnsmgr/", $line))
					{
						$trunkStatus[] =  preg_split("/\s+/",$line);
						//

					}
			}
		}
		array_shift($trunkStatus);
		array_pop($trunkStatus);
		array_pop($trunkStatus);
		//dump($trunkStatus);
		return $trunkStatus;
	}

	function getIaxTrunkStatus()
	{
		$this->loadAGI();
		if($status == 0){ //示忙
            $rs = $this->asm->Command("iax2 show registry");
			$lines 	= explode("\n", $rs['data']);

			foreach($lines as $key => $line)
			{

					if(!preg_match("/dnsmgr/", $line))
					{
						$trunkStatus[] =  preg_split("/\s+/",$line);
						//

					}
			}
		}
		array_shift($trunkStatus);
		array_pop($trunkStatus);
		array_pop($trunkStatus);
		//dump($trunkStatus);
		return $trunkStatus;
	}

	function setDND($extension,$username, $status ,$db)
	{
		$this->loadAGI();
		if($status == 0){ //示忙
            $this->asm->Command("database put DND ". $extension ." YES");
            $_SESSION['userinfo']['dnd'] = 1;
           	$time =time();
			$sql = "INSERT INTO asteriskcdrdb.report_dnd SET username='". $username  ."',extension='". $extension  ."',starttime='". $time ."'";
			$db->Execute($sql);
            $this->asm->Command("database put BG_DND ". $extension ." ".$time);
        }else{  //取消示忙
			$rs = $this->asm->Command("database get DND $extension ");
        	if(strstr($rs['data'],"YES"))
        	{
	        	$this->asm->Command("database del DND ". $extension ." ");
	            $_SESSION['userinfo']['dnd'] = 0;
	            $rs2 = $this->asm->Command("database get BG_DND  ".$extension );

      			  $arr = explode('Value:', $rs2['data']);
        		$t = trim($arr[1]);

        		if(isset($t) && $t != "")
        		{
					$sql = "UPDATE asteriskcdrdb.report_dnd SET endtime='". time() ."',longtime=endtime-starttime WHERE username='". $username ."'  AND starttime='". $t ."'ORDER BY starttime DESC LIMIT 1";
					$db->Execute($sql);
	        	$this->asm->Command("database del BG_DND ". $extension ." ");
        		}
        	}

        }

	}


	//示忙
	function putDND($extension)
	{
		$this->loadAGI();
		$this->asm->Command("database put DND ". $extension ." YES");
		$_SESSION['userinfo']['dnd'] = 1;

	}
	//取消示忙
	function delDND($extension)
	{
		$this->loadAGI();
		$this->asm->Command("database del DND ". $extension);
		$_SESSION['userinfo']['dnd'] = 0;

	}

	/*
	//强拆
	function hangup($exten)
	{
		//goBack($exten,"exit");
		$mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");
		$extenChannelList = $mc->get("extenChannelList");

		if(is_array($extensList))
		{

			if($extensList[$exten]["State"]!="InUse")
				goBack(c("通话不存在"),"exit");
			//$channellisk=$this->getExtenChannelList();
			$channelId=$extenChannelList[$exten]["channel"];

			$this->loadAGI();
			$this->asm->hangup($channelId);
		}


	}
	*/

	//强拆
	function hangup($exten)
	{
		$extensList = $this->getHints();
		$CallerID = $this->getAllChannelsInfo('CallerID');
		//$CallerID = $this->getAllChannelsInfo('Extension');
		//var_dump($CallerID);die;
		if(is_array($extensList))
		{
			if($extensList[$exten]["stat"]!="State:InUse")
					goBack(c("通话不存在"),"exit");
			if($CallerID[$exten]["Channel"]){
					$channelId=$CallerID[$exten]["Channel"];
			}else{
				foreach($CallerID AS $k=>$v){
					if(preg_match("/\/$exten\-/",$v["Channel"])){
							$channelId=$v["Channel"];
							break;
					}
				}
			}
			$this->loadAGI();
			$this->asm->hangup($channelId);
		}
	}
	//强拆
	function hangup2($exten)
	{
		//goBack($exten,"exit");
		$extensList = $this->getHints();
		$CallerID = $this->getAllChannelsInfo('CallerID');
		//var_dump($CallerID);die;
		if(is_array($extensList))
		{

			if($extensList[$exten]["stat"]!="State:InUse")
				return "channel not exist";
			//$channellisk=$this->getExtenChannelList();
			//$channelId=$CallerID[$exten]["Channel"];
			if($CallerID[$exten]["Channel"]){
					$channelId=$CallerID[$exten]["Channel"];
			}else{
				foreach($CallerID AS $k=>$v){
					if(preg_match("/\/$exten\-/",$v["Channel"])){
							$channelId=$v["Channel"];
							break;
					}
				}
			}
			$this->loadAGI();
			$this->asm->hangup($channelId);
				return "ok";
		}else{
			return "channel not exist";

		}
	}

	//强挂
	function forceHangup($exten)
	{
		$extensList = $this->getHints();
		$CallerID = $this->getAllChannelsInfo('CallerID');
		if(is_array($extensList))
		{
			if($extensList[$exten]["stat"]!="State:InUse")
					 {echo "通话不存在";die;}
			if($CallerID[$exten]["Channel"]){
					$channelId=$CallerID[$exten]["Channel"];
			}else{
				foreach($CallerID AS $k=>$v){
					if(preg_match("/\/$exten\-/",$v["Channel"])){
							$channelId=$v["Channel"];
							break;
					}
				}
			}
			$this->loadAGI();
			$this->asm->Redirect($channelId,'','h','discard',1);
		}
	}

	/**
	 *振铃
	 *主叫：exten,被叫：toid,显示名：callername
	 **/
	function call($exten,$toid,$callername)
	{
		$this->loadAGI();
		$callerid = "$callername <". $toid .">";
		$device = $this->getDevices();
		$dial = $device['dial'][$device['exten'][$exten]];
		$r = $this->asm->Originate($dial,$toid,"from-internal","1",NULL,NULL,NULL,$callerid,NULL,NULL,true,true);
		sleep(1);
		return $dial;
	}

	function call2($exten,$toid,$callername)
	{
		$this->loadAGI();
		$callerid = "$callername <". $callername .">";
		$device = $this->getDevices();
		$dial = $device['dial'][$device['exten'][$exten]];
		$r = $this->asm->Originate($dial,$toid,"from-internal","1",NULL,NULL,NULL,$callerid,NULL,NULL,true,true);
		sleep(1);
		return $dial;
	}

	/**
	 *   反振铃
	 **/
	function recall($exten,$toid,$callername,$context)
	{
		$this->loadAGI();
		$callerid = "$callername <". $toid .">";
		$device = $this->getDevices();
		$dial = $device['dial'][$device['exten'][$exten]];
		$r = $this->asm->Originate($dial,$toid,$context,"1",NULL,NULL,NULL,$callerid,NULL,NULL,NULL,NULL);
		sleep(1);
		return $dial;
	}

	/**
	 *
	 * @param $monitor_exten 被 监听 的分机  如  801
	 * @param $monitor_dial 被监听的分机的dial  如  SIP/801
	 * zhangtuo
	 * 2010-8-14
	 */
	function monitor($monitor_exten,$monitor_dial)
	{
        $admin_dial=$_SESSION["user_info"]["dial"];
	    $admin_extension =$_SESSION["user_info"]["extension"];

	    //$mc = $this->loadMemCache();
		$extensList = $this->getHints();
		$extensionsDndState = $this->getDNDAll();

		if(is_array($extensList))
		{
			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$admin_extension]=="YES")
					goBack(c("您的分机已经被手动置忙，暂时不能使用"),"exit");
			}

			if($extensList[$admin_extension]['stat']=="State:Unavailable")
				goBack(c("您的分机不在线，暂时不能使用"),"exit");
			if($extensList[$admin_extension]['stat']=="State:InUse")
				goBack("您的分机正在通话中，暂时不能使用","exit");
			if($extensList[$admin_extension]["stat"]=="State:Ringing")
				goBack(c("您的分机正在振铃，暂时不能使用"),"exit");

			if($extensList[$monitor_exten]["stat"]!="State:InUse")
				goBack(c("被监听的通话不存在"),"exit");

			$m_d="557$monitor_exten";
			$this->call($admin_extension,$m_d,"monitor_$monitor_exten");
			return true;
		}
	}

	//Ajax调用
	function ajax_monitor($monitor_exten,$monitor_dial)
	{
            $admin_dial=$_SESSION["user_info"]["dial"];
	    $admin_extension =$_SESSION["user_info"]["extension"];
		if(! $admin_extension){
			die($_SESSION["user_info"]['username'] ." had not assigned a extension");
		}
		$extensList = $this->getHints();
		$extensionsDndState = $this->getDNDAll();

		if(is_array($extensList))
		{
			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$admin_extension]=="YES")
					//goBack(c("您的分机已经被手动置忙，暂时不能使用"),"exit");
					return "a";
			}

			if($extensList[$admin_extension]['stat']=="State:Unavailable")
				//goBack(c("您的分机不在线，暂时不能使用"),"exit");
				return "b";
			if($extensList[$admin_extension]['stat']=="State:InUse")
				//goBack("您的分机正在通话中，暂时不能使用","exit");
				return "c";
			if($extensList[$admin_extension]["stat"]=="State:Ringing")
				//goBack(c("您的分机正在振铃，暂时不能使用"),"exit");
				return "d";

			if($extensList[$monitor_exten]["stat"]!="State:InUse")
				//goBack(c("被监听的通话不存在"),"exit");
				return "e";

			$m_d="557$monitor_exten";
			$this->call($admin_extension,$m_d,"monitor_$monitor_exten");
			return "f";
		}
	}


	//接口调用监控
	function interface_monitor($extension,$monitor_exten)
	{
	    $admin_extension = $extension;
		$extensList = $this->getHints();
		$extensionsDndState = $this->getDNDAll();

		if(is_array($extensList))
		{
			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$admin_extension]=="YES")
					//goBack(c("您的分机已经被手动置忙，暂时不能使用"),"exit");
					return "a";
			}

			if($extensList[$admin_extension]['stat']=="State:Unavailable")
				//goBack(c("您的分机不在线，暂时不能使用"),"exit");
				return "b";
			if($extensList[$admin_extension]['stat']=="State:InUse")
				//goBack("您的分机正在通话中，暂时不能使用","exit");
				return "c";
			if($extensList[$admin_extension]["stat"]=="State:Ringing")
				//goBack(c("您的分机正在振铃，暂时不能使用"),"exit");
				return "d";

			if($extensList[$monitor_exten]["stat"]!="State:InUse")
				//goBack(c("被监听的通话不存在"),"exit");
				return "e";

			$m_d="557$monitor_exten";
			$this->call($admin_extension,$m_d,"monitor_$monitor_exten");
			return "f";
		}
	}

	//Ajax调用
	function ajax_chanspy($monitor_exten,$monitor_dial)
	{
        $admin_dial=$_SESSION["user_info"]["dial"];
	    $admin_extension =$_SESSION["user_info"]["extension"];

	    //$mc = $this->loadMemCache();
		$extensList = $this->getHints();
		$extensionsDndState = $this->getDNDAll();

		if(is_array($extensList))
		{
			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$admin_extension]=="YES")
					//goBack(c("您的分机已经被手动置忙，暂时不能使用"),"exit");
					return "a";
			}

			if($extensList[$admin_extension]["stat"]=="State:Unavailable")
				//goBack(c("您的分机不在线，暂时不能使用"),"exit");
				return "b";
			if($extensList[$admin_extension]["stat"]=="State:InUse")
				//goBack(c("您的分机正在通话中，暂时不能使用"),"exit");
				return "c";
			if($extensList[$admin_extension]["stat"]=="State:Ringing")
				//goBack(c("您的分机正在振铃，暂时不能使用"),"exit");
				return "d";

			if($extensList[$monitor_exten]["stat"]!="State:InUse")
				//goBack(c("被监听的通话不存在"),"exit");
				return "e";

			$m_d="558$monitor_exten";
			$this->call($admin_extension,$m_d,"chanSpy_$monitor_exten");
			//return true;
			return "f";
		}
	}

	//接口调用监控
	function interface_chanspy($extension,$monitor_exten)
	{
	    $admin_extension = $extension;
		$extensList = $this->getHints();
		$extensionsDndState = $this->getDNDAll();

		if(is_array($extensList))
		{
			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$admin_extension]=="YES")
					//goBack(c("您的分机已经被手动置忙，暂时不能使用"),"exit");
					return "a";
			}

			if($extensList[$admin_extension]['stat']=="State:Unavailable")
				//goBack(c("您的分机不在线，暂时不能使用"),"exit");
				return "b";
			if($extensList[$admin_extension]['stat']=="State:InUse")
				//goBack("您的分机正在通话中，暂时不能使用","exit");
				return "c";
			if($extensList[$admin_extension]["stat"]=="State:Ringing")
				//goBack(c("您的分机正在振铃，暂时不能使用"),"exit");
				return "d";

			if($extensList[$monitor_exten]["stat"]!="State:InUse")
				//goBack(c("被监听的通话不存在"),"exit");
				return "e";

			$m_d="558$monitor_exten";
			$this->call($admin_extension,$m_d,"spy_$monitor_exten");
			return "f";
		}
	}

	/**
	 * 强插
	 * @param $monitor_exten 被强插的分机  如  801
	 * @param $monitor_dial 被强插的分机的dial  如  SIP/801
	 * zhangtuo
	 * 2010-8-14
	 */
	function chanspy($monitor_exten,$monitor_dial)
	{
        $admin_dial=$_SESSION["user_info"]["dial"];
	    $admin_extension =$_SESSION["user_info"]["extension"];

	    //$mc = $this->loadMemCache();
		$extensList = $this->getHints();
		$extensionsDndState = $this->getDNDAll();

		if(is_array($extensList))
		{
			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$admin_extension]=="YES")
					goBack(c("您的分机已经被手动置忙，暂时不能使用"),"exit");
			}

			if($extensList[$admin_extension]["stat"]=="State:Unavailable")
				goBack(c("您的分机不在线，暂时不能使用"),"exit");
			if($extensList[$admin_extension]["stat"]=="State:InUse")
				goBack(c("您的分机正在通话中，暂时不能使用"),"exit");
			if($extensList[$admin_extension]["stat"]=="State:Ringing")
				goBack(c("您的分机正在振铃，暂时不能使用"),"exit");

			if($extensList[$monitor_exten]["stat"]!="State:InUse")
				goBack(c("被监听的通话不存在"),"exit");

			$m_d="558$monitor_exten";
			$this->call($admin_extension,$m_d,"chanSpy_$monitor_exten");
			return true;
		}
	}

	/**
	*
	**/
	function nway($myexten,$targetexten)
	{
	//	return $myexten.$targetexten ;
		$mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");

		if(is_array($extensList))
		{

			if($extensList[$myexten]["State"]!="InUse")
			{
				return c("通话不存在");
				exit;
			}else
			{
				$extenChannelList = $mc->get("extenChannelList");
				$myChannelid=$extenChannelList[$myexten]["channel"];
				$YourChanerid=$this->getYourChaneridByMyChannelid($myChannelid);
				 //return $YourChanerid;
				if($myChannelid)
				{
			  	$this->loadAGI();
					$this->asm->SetVar($myChannelid,"nwaytoexten",$targetexten);
					$this->asm->Redirect2($myChannelid,$YourChanerid,"8600", "bangian-nway", "1");
		      return c("正在呼叫$targetexten,待接通第三方后请告知并按*11进行三方通话");
				}
			}
		}

	}

	//wjj-----3方通话
	function nway2($admin_dial,$number3)
	{
		if( !$admin_dial ){
			goBack("分机不存在!");
		}
		if( !$number3 ){
			goBack("请输入邀请加入会议室的号码!");
		}
		$this->loadAGI();
		$channels = $this->getChannels();
		$channelMe = $channels[$admin_dial]["channel_id"];
		if(!$channelMe){
			goBack("通话不存在!");
		}
		$channelYou=$this->getYourChaneridByMyChannelid($channelMe);
		$roomNum = (string)(rand(8900,8999));
		//goBack($channelMe."--".$channelYou);
		$parameters = array(
                	'Channel'=>"$channelMe",
                	'ExtraChannel'=>"$channelYou",
                	'Exten'=>$roomNum,
                	'ExtraExten'=>$roomNum,
                	'Context'=>'bangian-3way',
                	'ExtraContext'=>'bangian-3way',
                	'Priority'=>'1',
                	'ExtraPriority'=>'1',
        );
		$this->asm->SetVar($channelMe,"number3",$number3);//定义呼叫的第3个号码
		//$this->asm->SetVar($channelMe,"roomNum",$roomNum);
		//$this->asm->SetVar($channelMe,"Meet_Call",'YES');
		$this->asm->send_request("Redirect",$parameters);
	}

	//autocall-----IVR号码验证
	function memberpin($admin_dial,$exten)
	{
		if( !$admin_dial ){
			goBack("分机不存在!");
		}

		$this->loadAGI();
		$channels = $this->getChannels();
		$channelMe = $channels[$admin_dial]["channel_id"];
		if(!$channelMe){
			goBack("通话不存在!");
		}
		$channelYou=$this->getYourChaneridByMyChannelid($channelMe);


		$this->asm->SetVar($channelMe,"channelYou",$channelYou);
		$this->asm->SetVar($channelMe,"channelExten",$exten);
		$this->asm->SetVar($channelYou,"channelYou",$channelMe);
		$this->asm->SetVar($channelYou,"channelExten",$exten);
		$parameters = array(
                	'Channel'=>"$channelMe",
                	'ExtraChannel'=>"$channelYou",
                	'Exten'=>"s",
                	'ExtraExten'=>888666,
                	'Context'=>'bangian-wait',
                	'ExtraContext'=>'bangian-inputpin',
                	'Priority'=>'1',
                	'ExtraPriority'=>'1',
					"Async"	=>	"true",
        );

		$this->asm->send_request("Redirect",$parameters);
	}

	//autocall-----IVR号码验证
	function restorepin($admin_dial,$number3)
	{
		if( !$admin_dial ){
			goBack("分机不存在!");
		}

		$this->loadAGI();
		$channels = $this->getChannels();
		$channelMe = $channels[$admin_dial]["channel_id"];
		if(!$channelMe){
			goBack("通话不存在!");
		}
		$channelYou=$this->getYourChaneridByMyChannelid($channelMe);

		$parameters = array(
                	'Channel'=>"$channelMe",
                	'ExtraChannel'=>"$channelYou",
                	'Exten'=>70,
                	'ExtraExten'=>888666,
                	'Context'=>'from-internal',
                	'ExtraContext'=>'bangian-inputpin',
                	'Priority'=>'1',
                	'ExtraPriority'=>'1',
        );

		$this->asm->send_request("Redirect",$parameters);
	}


	/**
	 *$fromid为dial值,$toid为工号或者说分机号
	 **/
	function transferCall($admin_dial,$admin_extension,$exten)
	{

		$mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");
		$extensionsDndState = $mc->get("extensionsDndState");

		if(is_array($extensList))
		{
			if($extensList[$admin_extension]["State"]!="InUse")
				goBack(c("通话不存在"),"exit");

			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$exten]=="YES")
					goBack(c("您要转的分机已经被手动置忙，暂时不能使用"),"exit");
			}

			if($extensList[$exten]["State"]=="Unavailable")
				goBack(c("您要转的分机不在线，暂时不能使用"),"exit");
			if($extensList[$exten]["State"]=="InUse")
				goBack(c("您要转的分机正在通话中，暂时不能使用"),"exit");
			if($extensList[$exten]["State"]=="Ringing")
				goBack(c("您要转的分机正在振铃，暂时不能使用"),"exit");

			$this->loadAGI();
			//$channellisk=$this->getExtenChannelList();
			//$mychannelId=$channellisk[$admin_extension]["channel"];
			$extenChannelList = $mc->get("extenChannelList");
			$myChannelid=$extenChannelList[$admin_extension]["channel"];
			$YourChanerid=$this->getYourChaneridByMyChannelid($myChannelid);
			$this->asm->Redirect($YourChanerid, $exten, "from-internal", "1");
    	 goBack(c("电话已转移成功"),"exit");
		}

	}

	//wjj--三方通话--最新版本--拨号计划 [ft-3way]
	function multiCall($exten,$thirdPhone)
	{
			$this->loadAGI();
			$channels = $this->getAllChannelsByCid();
//var_dump($channels);die;
			$MyChanerid=$channels[$exten]['Channel'];
			if(!$MyChanerid){
					return false;
			}
			$YourChanerid=$channels[$exten]['BridgedTo'];
//--1
			$roomNum = rand(1000,9999); //随机生成会议室号码
		  $parameters = array(
				  'Channel'=>$MyChanerid,
				  'ExtraChannel'=>$YourChanerid,
				  'Exten'=>$roomNum,
				  'ExtraExten'=>$roomNum,
				  'Context'=>'ft-3way',
				  'ExtraContext'=>'ft-3way',
				  'Priority'=>'1',
				  'ExtraPriority'=>'1',
		  );
		  $this->asm->send_request("Redirect",$parameters);
//--2
			$p = Array(
				  "Channel"       =>      "Local/$thirdPhone@from-internal",
				  "Exten"         =>      $roomNum,
				  "Context"       =>      "ft-3way",
				  "Priority"      =>      "1",
				  "Timeout"       =>      60000,
				  "Variable"      =>      "__thirdPhone=$thirdPhone",
			);
		  $this->asm->send_request("Originate",$p);
			return true;
	}

	//剔除三方通话
	function kickMultiCall($thirdPhone)
	{

		$this->loadAGI();
		$channels = $this->getAllChannelsByCid();
		$MyChanerid=$channels[$thirdPhone]['Channel'];
		if(!$MyChanerid){
			return false;
		}
		$parameters = array(
				  'Channel'=>$MyChanerid,
				  'Exten'=>'h',
				  'Context'=>'discard',
				  'Priority'=>'1',
		);
        $this->asm->send_request("Redirect",$parameters);
		return true;
	}

	//wjj----rewrite--transfer-----for--BGCC
	//$admin_dial = "SIP/868";
	function transfer($admin_dial,$exten)
	{

		$this->loadAGI();
		$channels = $this->getChannels();
		$YourChanerid=$this->getYourChaneridByMyChannelid($channels[$admin_dial]["channel_id"]);
		$this->asm->Redirect($YourChanerid, "","$exten", "from-internal", "1");
	}

	//wjj----转到ivr
	function transferIVR($exten,$ivr_id)
	{
		$admin_dial = "SIP/".$exten;
		$this->loadAGI();
		$channels = $this->getChannels();
		$YourChanerid=$this->getYourChaneridByMyChannelid($channels[$admin_dial]["channel_id"]);
		$this->asm->Redirect($YourChanerid, "","s", "ivr-".$ivr_id, "1");
	}

	/**
	 * 服务评分
	 * @param unknown_type $dial
	 * @param unknown_type $userexten
	 * @param unknown_type $db
	 * @param unknown_type $feedkey
	 */
	function feedback($admin_dial,$admin_extension,$admin_username,$db,$feedkey)
	{
		$this->loadAGI();
		$channels = $this->getChannels();
		if( strlen($channels[$admin_dial]["channel_id"])<1 ){
			goBack(c("通话不存在".$admin_dial),"exit");
    		exit;
		}else
		{
			$YourChanerid=$this->getYourChaneridByMyChannelid($channels[$admin_dial]["channel_id"]);
			$clid = $this->asm->GetVar($YourChanerid,"CALLERID(num)");
			$exten = $this->asm->GetVar($YourChanerid,"EXTEN");
			$context = $this->asm->GetVar($YourChanerid,"CONTEXT");
			$uniqueid = $this->asm->GetVar($YourChanerid,"UNIQUEID");
			$billsec = time() - intval($uniqueid['Value']);
			$this->asm->Command("database put feedback ". $uniqueid['Value'] ." ".$admin_extension);
			$db->Execute("INSERT INTO asteriskcdrdb.report_feedback SET username='". $admin_username ."',extension='". $admin_extension ."',callerid='". $clid['Value'] ."',calltime='".date('Y-m-d H:i:s')."', uniqueid='". $uniqueid['Value'] ."'");
			$this->asm->Redirect(trim($YourChanerid), $feedkey, "from-internal", "1");
			goBack(c("已向用户播放评分录音，请挂机或准备接听下一个电话"), "exit");
			//$this->asm->Command("database del feedback ". $uniqueid['Value']);
		}
	}

	/**********************
	 *电话挂起
	 **********************/
	function holdOn($admin_extension,$admin_dial){
		$this->loadAGI();
		$channels = $this->getChannels();
		//dump($channels);die;
		if( strlen($channels[$admin_dial]["channel_id"])<1 ){
			goBack(c("通话不存在".$admin_dial),"exit");
    		exit;
		}else
		{
			//$r = $this->asm->Originate($admin_dial,"70","from-internal","1",NULL,NULL,NULL,"test",NULL,NULL,NULL,NULL);
			$YourChanerid=$this->getYourChaneridByMyChannelid($channels[$admin_dial]["channel_id"]);
			$this->asm->Redirect($YourChanerid, 70, "from-internal", "1");
			sleep(2);
			$rs = $this->asm->Command("show parkedcalls");
			$parkeds = $this->getparkedChannels($rs["data"]);
			//$this->asm->Redirect($channels[$admin_dial]["channel_id"], 70, "from-internal", "1");
			//unset($_SESSION["parkeds"]);
			//$_SESSION["parkeds"] = $parkeds;
			$this->asm->disconnect();
			if($parkeds[$YourChanerid]["num"])
				return $parkeds[$YourChanerid]["num"];
			else
				return $parkeds[$admin_extension]["num"];
		}
	}


	function holdClose($admin_extension,$admin_dial,$parknum){
		$this->loadAGI();

		//$rs = $this->asm->Command("show parkedcalls");
		//$parkeds = $this->getparkedChannels($rs["data"]);

		//$park_num=$parkeds[$admin_extension]['num'];
		//goBack(c("通话不存在".$admin_dial),"exit");
		$this->asm->Originate($admin_dial,$parknum,"from-internal","1",NULL,NULL,NULL,"UnHold_".$parknum,NULL,NULL,NULL,NULL);
		return 0;
	}

	function holdState($admin_extension){
		$this->loadAGI();

		$rs = $this->asm->Command("show parkedcalls");
		$parkeds = $this->getparkedChannels($rs["data"]);

		$park_num=$parkeds[$admin_extension]['num'];

		if($park_num=="")
			return 0;
		else
			return 1;
		}

	function intoMeeting($phone,$room,$prefix)
	{
		$this->loadAGI();
        if( $prefix == NULL ){
			$hints = $this->getHints();
			if( $hints[$phone]['stat']!="State:Idle" ) goBack(c("分机暂时不能通话，请检查分机状态"),"exit");
			$dervice = $this->getDevices();
			$dial = $dervice['dial'][$dervice['exten'][$phone]];
			$callerid = "meeting $phone";
            $this->asm->Originate($dial,$room,"from-internal","1",NULL,NULL,NULL,$callerid,NULL,NULL,NULL,NULL);
            goBack(c($phone.'已加入会议中!'),"exit");
        }else{

			$callerid = $phone;
			$tragetnum = "Local/".$phone."@from-internal";
			//call($exten,$toid,$event)
			//$this->call($room,$phone,"meeting");
			$r = $this->asm->Originate($tragetnum,$room,"from-internal","1",NULL,NULL,NULL,$callerid,NULL,NULL,NULL,NULL);
            goBack(c('非坐席加入语音会议室，进入总机后选择相应的功能键进入语音室!'),"exit");
        }
	}

	function listMeeting($room)
	{
		$this->loadAGI();
		$res = $this->asm->Command('meetme list '.$room);
		$line= split("\n", $res['data']);
		$nbuser=0;
		foreach ($line as $myline){
		if (substr($myline,0,9)=='Privilege'){
		 }elseif (substr($myline,0,4)=='User'){
				$linevalue= preg_split("/[\s,]+/", $myline);
				$meetmechannel [$nbuser][0] = number_format($linevalue[2]);
				$meetmechannel [$nbuser][1] = $linevalue[6];
				$meetmechannel [$nbuser][2] = $room;
				$meetmechannel [$nbuser][5] = $linevalue[4]." ".$linevalue[3];
				//$meetmechannel [$nbuser][6] = $linevalue[3];
				//$meetmechannel [$nbuser][7] = $linevalue[4];
				$pos = strpos($myline, 'Muted');
				if ($pos===false) $meetmechannel [$nbuser][3] = c('正常');
				else $meetmechannel [$nbuser][3] = c('静音');

				$pos = strpos($myline, 'Admin');
				if ($pos===false) $meetmechannel [$nbuser][4] = 'User';
				else $meetmechannel [$nbuser][4] = 'Admin';
				$nbuser++;
			}else{
				break;
			}
		}
		return $meetmechannel;
	}

	//wjj---add---for---AST1.8---meeting
	function listAllMeetings(){
		$this->loadAGI();
		$res = $this->asm->Command('meetme list concise');
		$line= split("\n", $res['data']);
		$arrMeeting = Array();
		foreach ($line as $myline){
			if (substr($myline,0,9)=='Privilege'){

			}elseif (strlen($myline)>5){
					$arr = explode('!',$myline);
					$arrMeeting[$arr[0]] = $arr;
			}else{
					break;
			}
		}
		return $arrMeeting;
	}

	function listMeeting2($room)
	{
		$this->loadAGI();
		$res = $this->asm->Command('meetme list '.$room);
		$line= split("\n", $res['data']);
		$nbuser=0;
		foreach ($line as $myline){
		if (substr($myline,0,9)=='Privilege'){
		 }elseif (substr($myline,0,4)=='User'){
				$regexp = "^User #: (\d+)\s+(\d+) (\w+)\s+Channel: (.*) (\(.*\))?  (\((.* Muted)\) )? \((.*)\) (.*)$";
				$linevalue= preg_match("/$regexp/", $myline, $matches);
				//var_dump($matches);die;
				$meetmechannel [$nbuser]['user_id'] = number_format($matches[1]);
				$meetmechannel [$nbuser]['extension'] = $matches[2];
				$meetmechannel [$nbuser]['name'] = $matches[3];
				$meetmechannel [$nbuser]['channel'] = $matches[4];
				$meetmechannel [$nbuser]['state'] = $matches[7];
				$meetmechannel [$nbuser]['during'] = $matches[9];//加入时长
				$nbuser++;
			}else{
				break;
			}
		}
		return $meetmechannel;
	}

	function getQueueInfo($queue)
	{
		$this->loadAGI();
		$hint_ary = $this->getHints();
        $rinfo = $this->asm->Command("queue ". $queue);
        $rinfo = explode('Members:', $rinfo['data']);
        $rinfo = strstr($rinfo[1],"No Callers") ? explode('No Callers', $rinfo[1]) : explode('Callers:', $rinfo[1]);
        $agent = trim($rinfo[0]);
        $waiters = trim($rinfo[1]); //队列中等待电话数
        $r['waiters'] = preg_split('/\n/',$waiters,-1,PREG_SPLIT_NO_EMPTY);

        //分析坐席中的状态
        $agent = explode("\n", $agent);
        $agents = count($agent); //队列中坐席总数
        $list = array();
		$devices = $this->getDevices();
        foreach( $agent as $val ){
            $tmpline =  preg_split("/\(/",$val);
			$pdial = trim($tmpline[0]);
            if(strstr(strtolower($tmpline[0]),'zap')){
			    //$agentlist['agent'] = $this->getSrcId(trim($tmpline[0]));
		    }else{
                $tmpline = explode('@', $tmpline[0]);
				$tmpline = explode('/', $tmpline[0]);
                $agentlist['agent'] = trim($tmpline[1]);
            }
            $agentlist['agent'] = trim($agentlist['agent']);
			$agentlist['device'] = $devices['exten'][$agentlist['agent']];
            $tmpline =  preg_split("/@/",$val);
            $tmpline = explode('/', $tmpline[0]);
            $agentlist['type'] = c($hint_ary[$agentlist['agent']]['type']);
            $agentlist['stat'] = getStat($hint_ary[$agentlist['agent']]['stat'],$agentlist['agent']) ? c(getStat($hint_ary[$agentlist['agent']]['stat'],$agentlist['agent'])) : c("未登录设备");
            $last = preg_split("/was/",trim($val));
		    if(!empty($last[1])){
			    $last = explode(' ' ,trim($last[1]));
			    $agentlist['lastcall'] = c($this->sec2hour($last[0]));
		    }
            preg_match('/taken (.*) calls/',$val,$take) 	;
		    $agentlist['calls'] =  $take[1] != 'no' ? $take[1] : 0 ;
            $r['list'][] = $agentlist;
            unset($agentlist);
        }
		return $r;

	}

	//支持Asterisk 1.8 取得通道的相关成员以及状态
	function getDynamicQueueMember($queue)
	{
		$this->loadAGI();
        $arrRet = $this->asm->Command("queue show $queue");

        $arrMember = explode("\n", $arrRet['data']);
		$count = count($arrMember);
		//echo $count;die;
		$memberInfo = Array();
		$regExp = '/\s+(\d+) \((.*)\) .*\(dynamic\)( \((paused)\))? \((.*use)\) has taken (.*) calls/';
		//dump($arrMember);die;
		for($j=0,$i=3;$i<$count;$i++,$j++){
			if( false === strpos($arrMember[$i],"Callers") ){
				preg_match($regExp,$arrMember[$i],$arrTmp);
				$memberInfo[$j]['membername'] = $arrTmp[1];
				$memberInfo[$j]['interface'] = $arrTmp[2];
				$memberInfo[$j]['paused'] = $arrTmp[4];    //如果是"paused" 表示暂停状态，如果是 空，表示非暂停
				$memberInfo[$j]['state'] = $arrTmp[5];      //"Not in use" 表示空闲 , "In use" 表示正在通话和振铃
				$memberInfo[$j]['answeredcalls'] = $arrTmp[6];  //接听电话数: 'no' 表示没有接听
			}else{
				break;
			}
		}
		//dump($memberInfo);
		return $memberInfo;
	}

	//支持Asterisk 1.8 得到所有队列成员--数量
	function getAllQueueMember()
	{
		$this->loadAGI();
        $arrRet = $this->asm->Command("queue show");
		preg_match('/^Privilege: Command\r\n(.*)\n\n$/s',$arrRet['data'],$arrTmp); //正则要加上s，表示匹配换行符
		//dump($arrTmp);die;
        $arrAll = explode("\n\n", $arrTmp[1]);
		//dump($arrAll);die;
		$regExp = '/\s+(\d+) \((.*)\) .*\(dynamic\)( \((paused)\))? \((.*use)\) has taken (.*) calls/';
		$all = Array();
		foreach( $arrAll AS $v ){
			$arr = Array();
			$arrMember = explode("\n", $v);
			$tmp = explode(" has",$arrMember[0]);
			$arr['name'] = $tmp[0];
			//求坐席数量
			$arr['agentcount'] = -2; //开头两行不算成员
			//dump($arrMember);die;
			$arr['membername'] = Array();
			foreach( $arrMember AS $val ){
				if( false === strpos($val,"Callers") ){
					//var_dump($val);
					$arr['agentcount']++;
					if( preg_match($regExp,$val,$arrTmp) ){
						//dump($regExp);dump($val);die;
						//echo "xxx---";
						//echo $arrTmp[1];
						array_push($arr['membername'],$arrTmp[1]);
						//dump($arr);die;
					}
				}else{
					break;
				}
			}
			//$arr['agentcount'] = count($arrMember) - 2 ;
			//dump($arr);die;
			$all[$tmp[0]] = $arr;
			//dump($all);die;
		}
		//dump($all);die;
		return $all;
	}


	/**
	 * 获得CF设置
	 * @param  $extension 分机
	 * @param  $type 类型，CF、CFU、CFB
	 */
	function getCF($extension,$type = "CF")
	{
		$this->loadAGI();
        $rs = $this->asm->Command("database get $type $extension ");
        $arr = explode('Value:', $rs['data']);
        return trim($arr[1]);
	}

	/**
	 * 获得CF设置
	 * @param  $extension 分机
	 * @param  $type 类型，CF、CFU、CFB
	 */
	function getCFAll($type = "CF")
	{
		$this->loadAGI();
        $rs = $this->asm->Command("database show $type");
		$lines=explode("\n", str_replace("\r\n", "\n", $rs["data"]));
		foreach($lines as $val){

			$tmp = explode(':',$val);
			$tmp[0] = trim($tmp[0]);
			$tmp[0] = str_replace("/$type/",'',$tmp[0]);
			if(count($tmp)>1 && $tmp[0] != "Privilege"){
				$cf[$tmp[0]] = trim($tmp[1]);
			}
		}
        return $cf;
	}



	/**
	 * 设置CF
	 * @param  $extension 分机
	 * @param  $value 值
	 * @param  $type 类型，CF、CFU、CFB
	 */
	function setCF($extension,$value,$type = "CF")
	{
		$this->loadAGI();
		if( $value ){
            $this->asm->Command("database put $type $extension  $value");
        }else{
            $this->asm->Command("database del $type $extension ");
        }
	}


	function getDND($user)
	{
		$this->loadAGI();
        $rs = $this->asm->Command("database get DND $user");
        return $rs['data'];
	}

	function getDNDAll($user)
	{
		$this->loadAGI();
        $rs = $this->asm->Command("database show DND");
		$lines=explode("\n", str_replace("\r\n", "\n", $rs["data"]));
		foreach($lines as $val){

			$tmp = explode(':',$val);
			$tmp[0] = trim($tmp[0]);
			$tmp[0] = str_replace('/DND/','',$tmp[0]);
			if(count($tmp)>1 && $tmp[0] != "Privilege"){
				$DND[$tmp[0]] = trim($tmp[1]);
			}
		}
        return $DND;
	}

	function DBGet($family,$key){
		$this->loadAGI();
		$rs = $this->asm->database_get($family,$key);
		return $rs;
	}
	/**
	 *通过通道号查来电号码(checkIncoming专用)
	 */
	function getClidFromChannel($channel) {
		$rs = $this->asm->Command("database show CALLER " . $channel );
		$clid = explode(':', $rs['data']);
		$clid = trim($clid[2]);
		return $clid ;
	}

	/**
	 * 弹窗处理中取得来电号码
	 **/
	function getCallerID($uniqueid,$curid){
		global $db;
		$uniqueid = trim($uniqueid);
		$query  = "SELECT * FROM asterisk.`events` WHERE
				   ((event LIKE '%Source: SS7%Destination%DestUniqueID: $uniqueid%') OR (event LIKE '%Source: SIP%Destination%DestUniqueID: $uniqueid%') OR (event LIKE '%Source: IAX%Destination%DestUniqueID: $uniqueid%') OR (event LIKE '%Source: Local%Destination%DestUniqueID: $uniqueid%') OR (event LIKE '%Source: Dahdi%Destination%DestUniqueID: $uniqueid%') OR (event LIKE '%Newcallerid %Dahdi%Uniqueid: $uniqueid%') OR (event LIKE '%Newcallerid %mISDN%Uniqueid: $uniqueid%'))  AND id > " . $curid . " ORDER BY id DESC LIMIT 0,1";

		$res = $db->GetRow($query);
		if (is_array($res)){
			$event = $res['event'];
			echo $event;
			$flds = split("  ",$event);
			print_r($flds);
			foreach ($flds as $myFld) {
				if (strstr($myFld,"CallerID:")){
					$rows['clid'] =  substr($myFld,9);
				}
				if (strstr($myFld,"CallerIDName")){
					$rows['CallerIDName'] = substr($myFld,13);
				}
				if(strstr($myFld,"SrcUniqueID:")){
				    $rows['uniqueid'] = substr($myFld,13);
				}
			}
			if(!empty($rows['clid'])) return $rows;
		}
		return 0;
	}
	/**
	 * 弹窗处理监听
	 * $extension 工号3001
	 * 返回$call
	 */
	function waitingCalls($extension,$db){
	//	global $db,$config,$POPURL;
		$event_id = $_SESSION['event_id']? $_SESSION['event_id'] : 0;
		$call = $this->checkIncoming($extension);
//		if ($call['status'] == ''){
//			$status	= 'waiting';
//			$call['event_id'] = $event_id;
//			$direction	= '';
//		} elseif ($call['status'] == 'incoming'){	//incoming calls here
//			$title	= $call['callerid'];
//			$stauts	= 'ringing';
//			$direction	= 'in';
//			//弹窗处理
//			/*
//			if ($call['callerid'] != '<unknown>'){
//				return false;
//			}*/
//		}
		return $call;
	}
/**
 *
 *
 * @param  $extension 用户的工号
 *
 * @return call数字，格式如下
 * 呼出如下
 Array (
 [CallerID] => 013683620513
 [CallerIDName] =>
 [Uniqueid] => 1267779773.238
 [CallType] => OutCall
 )
 * //呼入如下
 Array (
 [CallerID] => 01062605566
 [CallerIDName] => 01062605566
 [Uniqueid] => 1267779773.238
 [CallType] => InCall
 )
 *
 */

	function checkIncomingForPopPage($extension)
	{
		$extension=trim($extension);
		$dial="SIP/".$extension;
		if(empty($extension) ||empty($dial))
		{
			echo "<br/>The extension to be check is null,Please tell me why,extension=$extension,dial=$dial";
			exit;
		}
		//echo "extension=$extension";
		//获取当前工号的设备号
/*		$dervice = $this->getDevices();
		//print_r ($dervice);
		$dial = $dervice['dial'][$dervice['exten'][$extension]]; //SIP/801*/
		if(empty($dial))
		{
			echo "<br/>Can not get dial for $extension";
			exit;
		}
		global $db;
		$event_id = $_SESSION['event_id']? $_SESSION['event_id'] : 0;
		//echo "exten_id=".$event_id;
		//查询数据库
		//获取当前分机最新来(去)电事件信息
		//echo "<br/>checkIncoming,if there is Dial event for $dial  ";
		$query = "SELECT * FROM asterisk.`events` WHERE
				(event LIKE 'Event: Dial% %  Source: %".$dial."%' or event LIKE 'Event: Dial% %  Destination: %".$dial."%')
				AND id > " . $event_id . " order by id DESC LIMIT 0,1";
		$res = $db->GetRow($query);
		//echo "<br/>query=$query";
		if($res)//数据库中有Dial 事件,即有最新来(去)电
		{
			//echo "<br/>there is Dial event";
			$id        = $res['id'];
			$timestamp = $res['timestamp'];
			$event     = $res['event'];

			//echo "<br/>$event";
			//echo "<br/>$id";
			$_SESSION['event_id']=$id;	// 给session赋值，以免它弹屏不停

			if(strstr($event,"Destination: $dial"))//表示呼入Destination: SIP/801-08a66350
			{
			//	echo "<br/> in";
				//echo "<br/> $event";
				$CallType="InCall"; //电话类型，呼入

				$eventlist = split("  ",$event);
				//print_r($eventlist);
//				Array (
//				[0] => Event: Dial
//				[1] => Privilege: call,all
//				[2] => Source: Local/3001@from-internal-0a60,2
//				[3] => Destination: SIP/801-08a59500
//				[4] => CallerID: 3002
//				[5] => CallerIDName: 3002
//				[6] => SrcUniqueID: 1267779773.238
//				[7] => DestUniqueID: 1267779773.239
//				 )
				$CallerID=trim(substr($eventlist[4],9));
				$CallerIDName=trim(substr($eventlist[5],13));
				$Uniqueid=trim(substr($eventlist[6],12));
			}elseif(strstr($event,"Source: $dial"))  //表示呼出 Source: SIP/801-08a59500
			{
			//	echo "<br/>out";
				$CallType="OutCall"; //电话类型，呼出
				$eventlist = split("  ",$event);
				//print_r($eventlist);
//				Array (
//				[0] => Event: Dial
//				[1] => Privilege: call,all
//				[2] => Source: SIP/801-08a57d48
//				[3] => Destination: SIP/802-08a5ae80
//				[4] => CallerID: 3001
//				[5] => CallerIDName: 3001
//				[6] => SrcUniqueID: 1267781621.266
//				[7] => DestUniqueID: 1267781622.267
//				)
 				$Destination=trim(substr($eventlist[3],12));
 				$DestUniqueID=trim(substr($eventlist[7],13));
 				$query = "SELECT * FROM asterisk.`events` WHERE
				(event LIKE 'Event: Newcallerid% %Channel: %".$Destination."% %  Uniqueid: %".$DestUniqueID."%')
				AND id > " . $event_id . " order by id DESC LIMIT 0,1";
				$res = $db->GetRow($query);
				//echo "<br/>query=$query";
				if($res)//数据库中有Dial 事件,即有最新来(去)电
				{
					$id        = $res['id'];
					$timestamp = $res['timestamp'];
					$event     = $res['event'];

					$eventlist = split("  ",$event);
					//print_r($eventlist);
//					Array (
//					[0] => Event: Newcallerid
//					[1] => Privilege: call,all
//					[2] => Channel: SIP/802-09148ca0
//					[3] => CallerID: 3002
//					[4] => CallerIDName:
//					[5] => Uniqueid: 1268015401.39
//					[6] => CID-CallingPres: 0 (Presentation Allowed, Not Screened)
//					)
					$CallerID=trim(substr($eventlist[3],9));
					$CallerIDName=trim(substr($eventlist[4],13));
					$Uniqueid=trim(substr($eventlist[5],9));
				}
			}

			$call["CallerID"]=$CallerID;
			$call["CallerIDName"]=$CallerIDName;
			$call["Uniqueid"]=$Uniqueid;
			$call["CallType"]=$CallType;
		}
		//print_r($call);
		return $call;
	}




	function checkIncomingForCrm_old($extension)
	{
		$extension=trim($extension);
		if(empty($extension))
		{
			//echo "<br/>The extension to be check is null,Please tell me why";
			exit;
		}
	//	echo "extension=$extension";
		//获取当前工号的设备号
		$dervice = $this->getDevices();
		//print_r ($dervice);
		$dial = $dervice['dial'][$dervice['exten'][$extension]]; //SIP/801
		if(empty($dial))
		{
		//	echo "<br/>Can not get dial for $extension";
			exit;
		}
		global $db;
		$event_id = $_SESSION['event_id']? $_SESSION['event_id'] : 0;

		//查询数据库
		//获取当前分机最新来(去)电事件信息
		//echo "<br/>checkIncoming,if there is Dial event for $dial  ";
		$query = "SELECT * FROM asterisk.`events` WHERE
				(event LIKE 'Event: Dial% %  Source: %".$dial."%' or event LIKE 'Event: Dial% %  Destination: %".$dial."%')
				AND id > " . $event_id . " order by id DESC LIMIT 0,1";
		$res = $db->GetRow($query);
	//	echo "<br/>query=$query";
		if($res)//数据库中有Dial 事件,即有最新来(去)电
		{
		//	echo "<br/>there is Dial event";
			$id        = $res['id'];
			$timestamp = $res['timestamp'];
			$event     = $res['event'];

		//	echo "<br/>$event";
		//	echo "<br/>$id";
			$_SESSION['event_id']=$id;	// 给session赋值，以免它弹屏不停

			if(strstr($event,"Destination: $dial"))//表示呼入Destination: SIP/801-08a66350
			{
		///		echo "<br/> in";
				//echo "<br/> $event";
				$CallType="InCall"; //电话类型，呼入

				$eventlist = split("  ",$event);
		//		print_r($eventlist);
//				Array (
//				[0] => Event: Dial
//				[1] => Privilege: call,all
//				[2] => Source: Local/3001@from-internal-0a60,2
//				[3] => Destination: SIP/801-08a59500
//				[4] => CallerID: 3002
//				[5] => CallerIDName: 3002
//				[6] => SrcUniqueID: 1267779773.238
//				[7] => DestUniqueID: 1267779773.239
//				 )
				$CallerID=trim(substr($eventlist[4],9));
				$CallerIDName=trim(substr($eventlist[5],13));
				$Uniqueid=trim(substr($eventlist[6],12));
			}elseif(strstr($event,"Source: $dial"))  //表示呼出 Source: SIP/801-08a59500
			{
			//	echo "<br/>out";
				$CallType="OutCall"; //电话类型，呼出
				$eventlist = split("  ",$event);
			//	print_r($eventlist);
//				Array (
//				[0] => Event: Dial
//				[1] => Privilege: call,all
//				[2] => Source: SIP/801-08a57d48
//				[3] => Destination: SIP/802-08a5ae80
//				[4] => CallerID: 3001
//				[5] => CallerIDName: 3001
//				[6] => SrcUniqueID: 1267781621.266
//				[7] => DestUniqueID: 1267781622.267
//				)
 				$Destination=trim(substr($eventlist[3],12));
 				$DestUniqueID=trim(substr($eventlist[7],13));
 				$query = "SELECT * FROM asterisk.`events` WHERE
				(event LIKE 'Event: Newcallerid% %Channel: %".$Destination."% %  Uniqueid: %".$DestUniqueID."%')
				AND id > " . $event_id . " order by id DESC LIMIT 0,1";
				$res = $db->GetRow($query);
			//	echo "<br/>query=$query";
				if($res)//数据库中有Dial 事件,即有最新来(去)电
				{
					$id        = $res['id'];
					$timestamp = $res['timestamp'];
					$event     = $res['event'];

					$eventlist = split("  ",$event);
				//	print_r($eventlist);
//					Array (
//					[0] => Event: Newcallerid
//					[1] => Privilege: call,all
//					[2] => Channel: SIP/802-09148ca0
//					[3] => CallerID: 3002
//					[4] => CallerIDName:
//					[5] => Uniqueid: 1268015401.39
//					[6] => CID-CallingPres: 0 (Presentation Allowed, Not Screened)
//					)
					$CallerID=trim(substr($eventlist[3],9));
					$CallerIDName=trim(substr($eventlist[4],13));
					$Uniqueid=trim(substr($eventlist[5],9));
				}
			}

			$call["CallerID"]=$CallerID;
			$call["CallerIDName"]=$CallerIDName;
			$call["Uniqueid"]=$Uniqueid;
			$call["CallType"]=$CallType;
		}
		//print_r($call);
		return $call;
	}
/**
 *
 *
 * @param  $extension 用户的工号
 *
 * @return call数字，格式如下
 * 呼出如下
 Array (
 [CallerID] => 013683620513
 [CallerIDName] =>
 [Uniqueid] => 1267779773.238
 [CallType] => OutCall
 )
 * //呼入如下
 Array (
 [CallerID] => 01062605566
 [CallerIDName] => 01062605566
 [Uniqueid] => 1267779773.238
 [CallType] => InCall
 )
 *
 */
	function checkIncoming($extension,$dial)
	{
		$extension=trim($extension);
		$dial=trim($dial);
		if(empty($extension) ||empty($dial))
		{
			echo "<br/>The extension to be check is null,Please tell me why,extension=$extension,dial=$dial";
			exit;
		}
		//echo "extension=$extension";
		//获取当前工号的设备号
/*		$dervice = $this->getDevices();
		//print_r ($dervice);
		$dial = $dervice['dial'][$dervice['exten'][$extension]]; //SIP/801*/
		if(empty($dial))
		{
			echo "<br/>Can not get dial for $extension";
			exit;
		}
		global $db;
		$event_id = $_SESSION['event_id']? $_SESSION['event_id'] : 0;
		//echo "exten_id=".$event_id;
		//查询数据库
		//获取当前分机最新来(去)电事件信息
		//echo "<br/>checkIncoming,if there is Dial event for $dial  ";
		$query = "SELECT * FROM asterisk.`events` WHERE
				(event LIKE 'Event: Dial% %  Source: %".$dial."%' or event LIKE 'Event: Dial% %  Destination: %".$dial."%')
				AND id > " . $event_id . " order by id DESC LIMIT 0,1";
		$res = $db->GetRow($query);
		//echo "<br/>query=$query";
		if($res)//数据库中有Dial 事件,即有最新来(去)电
		{
			//echo "<br/>there is Dial event";
			$id        = $res['id'];
			$timestamp = $res['timestamp'];
			$event     = $res['event'];

			//echo "<br/>$event";
			//echo "<br/>$id";
			$_SESSION['event_id']=$id;	// 给session赋值，以免它弹屏不停

			if(strstr($event,"Destination: $dial"))//表示呼入Destination: SIP/801-08a66350
			{
			//	echo "<br/> in";
				//echo "<br/> $event";
				$CallType="InCall"; //电话类型，呼入

				$eventlist = split("  ",$event);
				//print_r($eventlist);
//				Array (
//				[0] => Event: Dial
//				[1] => Privilege: call,all
//				[2] => Source: Local/3001@from-internal-0a60,2
//				[3] => Destination: SIP/801-08a59500
//				[4] => CallerID: 3002
//				[5] => CallerIDName: 3002
//				[6] => SrcUniqueID: 1267779773.238
//				[7] => DestUniqueID: 1267779773.239
//				 )
				$CallerID=trim(substr($eventlist[4],9));
				$CallerIDName=trim(substr($eventlist[5],13));
				$Uniqueid=trim(substr($eventlist[6],12));
			}elseif(strstr($event,"Source: $dial"))  //表示呼出 Source: SIP/801-08a59500
			{
			//	echo "<br/>out";
				$CallType="OutCall"; //电话类型，呼出
				$eventlist = split("  ",$event);
				//print_r($eventlist);
//				Array (
//				[0] => Event: Dial
//				[1] => Privilege: call,all
//				[2] => Source: SIP/801-08a57d48
//				[3] => Destination: SIP/802-08a5ae80
//				[4] => CallerID: 3001
//				[5] => CallerIDName: 3001
//				[6] => SrcUniqueID: 1267781621.266
//				[7] => DestUniqueID: 1267781622.267
//				)
 				$Destination=trim(substr($eventlist[3],12));
 				$DestUniqueID=trim(substr($eventlist[7],13));
 				$query = "SELECT * FROM asterisk.`events` WHERE
				(event LIKE 'Event: Newcallerid% %Channel: %".$Destination."% %  Uniqueid: %".$DestUniqueID."%')
				AND id > " . $event_id . " order by id DESC LIMIT 0,1";
				$res = $db->GetRow($query);
				//echo "<br/>query=$query";
				if($res)//数据库中有Dial 事件,即有最新来(去)电
				{
					$id        = $res['id'];
					$timestamp = $res['timestamp'];
					$event     = $res['event'];

					$eventlist = split("  ",$event);
					//print_r($eventlist);
//					Array (
//					[0] => Event: Newcallerid
//					[1] => Privilege: call,all
//					[2] => Channel: SIP/802-09148ca0
//					[3] => CallerID: 3002
//					[4] => CallerIDName:
//					[5] => Uniqueid: 1268015401.39
//					[6] => CID-CallingPres: 0 (Presentation Allowed, Not Screened)
//					)
					$CallerID=trim(substr($eventlist[3],9));
					$CallerIDName=trim(substr($eventlist[4],13));
					$Uniqueid=trim(substr($eventlist[5],9));
				}
			}

			$call["CallerID"]=$CallerID;
			$call["CallerIDName"]=$CallerIDName;
			$call["Uniqueid"]=$Uniqueid;
			$call["CallType"]=$CallType;
		}
		//print_r($call);
		return $call;
	}
	function debug_echo($msg,$debug=0)
	{
		if(!$debug)
		{
			echo $msg;
		}
	}
	 /**
     *通过分机号查设备号(checkIncoming专用)
     */
	function getSrcExt($asm,$exten) {
		$rs = $this->asm->Command("database show USER " . $exten );
		$dial = explode(':', $rs['data']);
		$dial = trim($dial[2]);
		return $dial ;
	}
	/**
	 * 弹窗处理事件log
	 */
	function events($event = null){
		if(LOG_ENABLED){
			$now = date("Y-M-d H:i:s");

			$fd = fopen (FILE_LOG,'a');
			$log = $now." ".$_SERVER["REMOTE_ADDR"] ." - $event \n";
			fwrite($fd,$log);
			fclose($fd);
		}
	}
	//秒数转小时
    function sec2hour($sec)
    {
        $m = intval( $sec / 60 );
        $ysec = $sec % 60;
        return   $m."分".$ysec ."秒";
    }

    /**
     * *
     * 得到所有的ampuser信息
     * 如果没有指定工号，就返回所有的
     * @param 工号 $user  例如3001
     * @return 返回数组，如下格式
     * Array ( [3001] => Array ( [cidname] => 3001 [cidnum] => 3001 [noanswer] => [outboundcid] => [password] => 3001 [recording] => out=Adhoc|in=Adhoc [ringtimer] => 0 [voicemail] => novm ) [3002] => Array ( [cidname] => 3002 [cidnum] => 3002 [device] => &802 [noanswer] => [outboundcid] => [password] => 3002 [recording] => out=Always|in=Always [ringtimer] => 0 [voicemail] => novm ) [] => Array ( [] => ) )
     * 调用方法，如下
     * $ary=$bmi->getAmpusers()
     * $ary['3001']['recording']
     */
    function getAmpusers($user="")
    {
    	$ary=array();
    	$this->loadAGI();
    	if(empty($user))
    	{
    		$ainfo = $this->asm->Command("database show AMPUSER");
    	}else
    	{
    		$ainfo = $this->asm->Command("database show AMPUSER/$user");
    	}
    	$lines 	= explode("\n", $ainfo['data']);
		unset($lines[0]);
        foreach($lines as $key => $line)
        {
			$line = explode(":", $line);
			$type = explode('/',$line[0]);
			$ary[trim($type[2])][trim($type[3])] = trim($line[1]);
        }
		return $ary;
    }

	//获取单个分机状态
	//获取单个分机状态
	function getHint($exten)
    {
		if(!$exten)return false;
		$this->loadAGI();
        $ainfo = $this->asm->Command("core show hint $exten");
        $lines 	= explode("\n", $ainfo['data']);
        $regExp = "/\s+$exten@ext-local\s+: (([^ ]+)\/\d+)\s+State:([^ ]+)/";
		foreach($lines AS $line){
			preg_match($regExp, $line, $matches);
			if($matches)break;
		}
        $arrHint['type'] = $matches[1];
		$arrHint['stat'] = $matches[3];
		$arrHint['device'] = $matches[1];
		$arrHint['user'] = $exten;
        return $arrHint;
    }

	//获取所以分机状态
    function getHints()
    {
        $this->loadAGI();
        $ainfo = $this->asm->Command("core show hints");
        $lines 	= explode("\n", $ainfo['data']);

        foreach($lines as $key => $line)
        {

                if(preg_match("/State:/", $line))
                {
                    $array =  preg_split("/\s+/",$line);
                    if (strstr($array[1],"@"))
                    {
                       $tmp=explode( "@",$array[1] );
                       $user=$tmp[0];
                    }
										$device = explode('/', $array[3]);
										$exten = $device[1];
                    $type = $device[0];
                    $hints[$user]['type'] = trim($type);
                    $hints[$user]['stat'] = trim($array[4]);
                    $hints[$user]['device'] = trim($array[3]);
					$hints[$user]['user'] = trim($user);
                }
        }
		//dump($hints);die;
        return $hints;
    }


    /**
     * 得到设备信息
     *
     * zhangtuo edit
     *
     * 最后修改 2010-3-1
     *
     * @return 返回如下
     * Array ( [dial] => Array ( [801] => SIP/801 [802] => SIP/802 )
     * 		   [user] => Array ( [801] => 3001 [802] => 3002 )
     * 		   [exten] => Array ( [3001] => 801 [3002] => 802 ) )
     * 使用方法如下
     * $ary=$bmi->getDevices();
     * $ary['dial']['801']  值是SIP/801
     * $ary['user']['801']  值是SIP/3001
     * $ary['exten']['3001']  值是801
     */
	function getDevices()
	{
		$this->loadAGI();
        $ainfo = $this->asm->Command("database show device");
        $lines 	= explode("\n", $ainfo['data']);
		unset($lines[0]);
        foreach($lines as $key => $line)
        {
			$line = explode(":", $line);
			$type = explode('/',$line[0]);
			$type[3] = trim($type[3]);
			if( strlen( $type[2] )>1 ){
				switch ( $type[3] ){
					case "user" :
						$ary["user"][$type[2]] = trim($line[1]);
						$ary["exten"][trim($line[1])] = $type[2] ;
					    break;
					case "dial":
						$ary["dial"][$type[2]] = trim($line[1]);
						break;
				}
			}
			unset($type);
        }
		return $ary;
	}


	function getChannels()
	{
		$rs = $this->asm->Command("core show channels");
		$lines = explode("<br />", nl2br($rs["data"]));
		$types = array("iax","sip","zap");
		$ckey = array(0=>"channel_id",1=>"Location",2=>"State",3=>"Application");
		foreach( $lines as $val ){
			$val = trim($val);
			$type =strtolower($val);
			$type = substr($type,0,3);
			if( in_array($type,$types) ){
				$thisline = explode(" ",$val);
				$i=0;
				$dial = explode("-",$thisline[0]);
				$dial[0] = trim(strtoupper($dial[0]));
				foreach($thisline as $v){

					if(trim($v)!=''){
						if($i<3){
							$c[$dial[0]][$ckey[$i]] = trim($v);
						}else{
							$c[$dial[0]][$ckey[3]] .= " ".$v;
						}
						$i++;
					}
				}
			}
		}
		return $c;
	}

	/**
	 *
	 * @param  $lines
	 *
	 * Array ( [801] => Array ( [num] => 71 [channel] => SIP/701-0a26e870 [context] => from-internal [extension] => 801 ) )
	 */
	function getparkedChannels($lines)
	{
		$lines = str_replace('(', '',$lines);
		$lines = str_replace(')', '',$lines);
		$lines = explode("<br />", nl2br($lines));

		$types = array("iax","sip","zap");
		$ckey = array(0=>"num",1=>"channel",2=>"context",3=>"extension");
		foreach( $lines as $val ){
			$val = trim($val);
			$parkednum =strtolower($val);
			$parkednum = substr($parkednum,0,2);
			if( is_numeric($parkednum) && ($parkednum >70)){
				$thisline = explode(' ',$val);
				$i=0;
				foreach($thisline as $v){
					if(trim($v)!=''){
						if($i<3){
							$list[$ckey[$i]] = trim($v);
						}else{
							$list[$ckey[3]] .= $v;
						}
						$i++;
					}
					if($list['extension']) break;
				}
				if ($list['extension'] == "s"  ) $list["extension"] = $list["channel"];
				$c[trim($list['extension'])] = $list;
				unset($list);
			}
		}
		return $c	;
	}
	//打对方分机
	function toexten( $trunk_name, $fom_id,$exten_type, $to_id,$to_id_exten=" ", $context,$waittime )
	{
		$str = "Channel: $trunk_name/$to_id\nCallerid: $fom_id\nMaxRetries: 5\nRetryTime: $waittime\nWaitTime: $waittime\nContext: $context\nSetVar: fromdial=$exten_type/$fom_id\nSetVar: toexten=$to_id_exten\nExtension: s\nPriority: 1";
		$filename = "/var/spool/asterisk/outgoing/".time()."_".$from_id.".call";
		fileWrite($filename, $str,"w");
	}

	Function fileWrite($fFileName, $fContent, $fTag = 'w') {
		ignore_user_abort (TRUE);		// 忽略用户关闭游览器动作，程式继续执行
		$fp = fopen($fFileName, $fTag);
		if (flock($fp, LOCK_EX)) {
			fwrite($fp, $fContent);
			flock($fp, LOCK_UN);
		}
		fclose($fp);
		ignore_user_abort (FALSE);		// 关闭忽略用户关闭游览器动作，程式随用户中止防问而停止
		return;
	}
	/**
	*查找谁与我相连呢
	* zhangtuo added
	* 2009
	*/
	function getYourChaneridByMyChannelid($MyChannelid )
	{
		if(empty($MyChannelid))
			return null;

		$channels = $this->getAllChannelsInfo('Channel');
		//$channels=$this->getAllChannelsInfo();
		if(empty($channels))
			return null;
		$YourChanerid=$channels[$MyChannelid]["BridgedTo"]; //Local/0016@from-internal-b874,1
		$arr=explode(",",$YourChanerid);
		if(count($arr)==2 and ($arr[1]==1 or $arr[1]==2))
		{
			if($arr==1)
				$YourChanerid=$channels[$arr[0].",2"]["BridgedTo"];
			else
			  $YourChanerid=$channels[$arr[0].",1"]["BridgedTo"];
		}
		//echo $YourChanerid;die;
		return $YourChanerid;

	}

	/*
	**获得通道设置组变量的所有通道
	*/
	function getGroupChannelsInfo($group,$category)
	{
		$rs = $this->asm->Command("group show channels");
		if(count($rs) == 0 )
		{
			return null;
		}
		else
		{
			$channels = Array();
			$lines=explode("\n", str_replace("\r\n", "\n", trim($rs["data"])));
			foreach($lines as $line)
			{

				$arr=preg_split('/\s+/',$line);
				if($arr[1]==$group && $arr[2]==$category )
				{
					//dump($arr);//die;
					$channels[] = $arr[0];
				}
			}
			return $channels;
		}
	}

	/**
	*获得所有的通道信息
	*返回数组
	*				$channels[$Channel]["Channel"]=$Channel; //
	*				$channels[$Channel]["Context"]=$Context; //
	*				$channels[$Channel]["Extension"]=$Extension;
	*				$channels[$Channel]["Priority"]=$Priority;
	*				$channels[$Channel]["State"]=$State;
	*				$channels[$Channel]["Application"]=$Application;
	*				$channels[$Channel]["Data"]=$Data;
	*				$channels[$Channel]["CallerID"]=$CallerID;
	*				$channels[$Channel]["Accountcode"]=$Accountcode;
	*				$channels[$Channel]["Amaflags"]=$Amaflags;
	*				$channels[$Channel]["Duration"]=$Duration;
	*				$channels[$Channel]["BridgedTo"]=$BridgedTo;
	*如果空则返回null
	*/
	function getAllChannelsInfo($str)
	{
		$rs = $this->asm->Command("core show channels concise");
		if(count($rs) == 0 )
		{

			return null;
		}
		else
		{

			$lines=explode("\n", str_replace("\r\n", "\n", $rs["data"]));

			foreach($lines as $line)
			{
				$arr=split("!",$line);
				if(count($arr)>10)
				{
					$ary['Channel']=$arr[0];
					$ary['Context']=$arr[1];
					$ary['Extension']=$arr[2];
					$ary['Priority']=$arr[3];
					$ary['State']=$arr[4];
					$ary['Application']=$arr[5];
					$ary['Data']=$arr[6];
					$ary['CallerID']=$arr[7];
					$ary['Accountcode']=$arr[8];
					$ary['Amaflags']=$arr[9];
					//$ary['Billsec']=$arr[11];//没有通话时长，这个参数不准确!
					$ary['Duration']=$arr[11];
					$ary['BridgedTo']=$arr[12];
					$key = $ary[$str];

					$channels[$key]=$ary;
				}
			}
			return $channels;
		}
	}

	function getChannelsInfo($channel_id){
		$rs = $this->asm->Command("core show channel $channel_id");

		$lines = explode('CDR Variables:',$rs["data"]);
		$cdrs = explode("\n", str_replace("\r\n", "\n", $lines[1]));
		foreach($cdrs as $val)
		{
			$val = substr( $val,9);
			$val = explode("=", $val);
			$list[$val[0]] = $val[1];
		}

		$lines = explode('Variables:',$lines[0]);
		$variables = explode("\n", str_replace("\r\n", "\n", $lines[1]));
		foreach($variables as $val)
		{
			$val = explode("=", $val);
			$list[trim($val[0])] = trim($val[1]);
		}



		return $list;
	}

	/**
	 * 获得所有分机和通道的对应关系
	 * zhangtuo
	 * 2010-8-14
	 * 返回如下
	 * Array
	(
	    [701] => Array
	        (
	            [channel] => SIP/701-08b63610
	            [bridgedto] => Local/701@from-internal-9d1c,2
	        )

	    [801] => Array
	        (
	            [channel] => IAX2/801-8466
	            [bridgedto] => Local/701@from-internal-9d1c,1
	        )

	)
	 */
	function getExtenChannelList()
	{
		$this->loadAGI();
		$rs = $this->asm->Command("core show channels concise");
		if(empty($rs))
		{
			return null;
		}
		else
		{
			$lines=explode("\n", str_replace("\r\n", "\n", $rs["data"]));
			foreach($lines as $line)
			{
				$arr=split("!",$line);
				//echo count($arr),"<br>";
				if(count($arr)==14)
				{

					$Channel=$arr[0];
					$bridgedto =$arr[12];
/*					$arr1=explode(",",$bridgedto);
					if(count($arr1)==2 and ($arr1[1]==1 or $arr1[1]==2))
					{
						if($arr1[1]==1)
							$bridgedto=$arr1[0].",2";
						else
						  	$bridgedto=$arr1[0].",1";
					}*/
					$extenChannelList[$arr[7]]["channel"]=$arr[0];
					$extenChannelList[$arr[7]]["bridgedto"]=$bridgedto;
				}
			}
			return $extenChannelList;
		}
	}


	function getProductInfo()
	{
		if(file_exists("/var/www/html/bgcrm/include/config/_register.inc.php"))
			include "/var/www/html/bgcrm/include/config/_register.inc.php";
    	$id=trim($_register["bangian_id"]);
    	$key=trim($_register["bangian_key"]);
    	$sn=trim($_register["bangian_sn"]);

    	$s_uncode=$this->authcode($sn,"DECODE",$key);
    	//return $s_uncode;
    	//id=UBA800110318025,time=2011-03-03|2026-03-31,mac=9A:96:3D:43:38:35|9A:96:3D:43:38:31|9A:96:3D:43:38:35,status=2,num=12|8|2|30|5|2|12|12
	    $arr=split(",",$s_uncode);
    	foreach($arr as $v)
    	{
    		$arr_v=split("=",$v);
    		{
    			$list[trim($arr_v[0])]=trim($arr_v[1]);
    		}
    	}

    	$arr_t = explode("|",$list['time']);
    	$arr_m = explode("|",$list['mac']);
    	$arr_n = explode("|",$list['num']);

    	$arr_return = array();
    	$arr_retrun['id']				= $list['id'];
    	$arr_retrun['key']				= $key;
    	$arr_retrun['status']			= $list['status'];
     	$arr_retrun['reg_time']			= $_register["reg_time"];
      	$arr_retrun['sale_time']		= $arr_t[0];
      	$arr_retrun['end_time']			= $arr_t[1];
    	$arr_retrun['mac1']				= $arr_m[0];
    	$arr_retrun['mac2']				= $arr_m[1];
    	$arr_retrun['mac3']				= $arr_m[2];
    	$arr_retrun['num_trunk']		= $arr_n[0];
    	$arr_retrun['num_sip']			= $arr_n[1];
    	$arr_retrun['num_iax2']			= $arr_n[2];
      	$arr_retrun['num_channel']		= $arr_n[3];
    	$arr_retrun['num_voicemail']	= $arr_n[4];
    	$arr_retrun['num_fax']			= $arr_n[5];
    	$arr_retrun['num_user']			= $arr_n[6];
     	$arr_retrun['num_outcall']		= $arr_n[7];


    	//dump($arr_retrun);
		return $arr_retrun;
	}

	function checkSn(){

		if(file_exists("/var/www/html/bgcrm/include/config/_register.inc.php"))
			include "/var/www/html/bgcrm/include/config/_register.inc.php";
    	$id=trim($_register["bangian_id"]);
    	$key=trim($_register["bangian_key"]);
    	$sn=trim($_register["bangian_sn"]);

    	if($id=="" or $key =="" or $sn=="")
    		goBack(c("您的产品尚未注册，请根据下面的提示注册"),"index.php?module=auto&action=register"); //产品没有注册的话，就跳到注册页面

    	//接下来开始验证

    	$list = $this->getProductInfo();
    	if(!is_array($list) || $list == null)
    	{
   	    	goBack(c("系统初始化异常，有可能是产品已过期，请与管理员联系，重新注册"),"exit");
    	}


    	//接下来开始验证序列号，和有效期，以及mac地址是否正确
    	//id不相等，很可能是用户自己改了id，或者盗用别人的id
    	if($list["id"]!=$id)
    		goBack(c("系统初始化异常，id错误，请与管理员联系"),"exit");

    	//mac不相等，有可能是用户换了网卡，或者盗用别人的sn
    	$mac=getMac();

    	if($list["mac1"]!=$mac && $list["mac2"]!=$mac & $list["mac3"]!=$mac)
    		goBack(c("系统初始化异常，mac错误，请与管理员联系"),"exit");

    	$sale_time=strtotime($list["sale_time"]);
    	$end_time=strtotime($list["end_time"]);
    	$now_time  = time();
    	$tommorrow_time = date("Y-m-d",time()+86400);
    	//goBack($tommorrow_time,"exit");

    	if($end_time < $now_time)
    		goBack(c("您的产品已经过期，请与管理员联系"),"exit");
    	if($sale_time > $now_time)
    		goBack(c("系统初始化异常，time 错误，请与管理员联系"),"exit");

		$filename = "/var/www/html/bgcrm/data/logs/report_update.html";
		if(file_exists($filename))
		{
				$handle  = fopen ($filename, "r");
				while (!feof ($handle))
				{
				    $buffer  = fgets($handle, 4096);

				    $line = trim($buffer);
				    if(strstr($line,$tommorrow_time))
				    {
				    	goBack(c("系统初始化异常，date 错误，请与管理员联系"),"exit");
				    }
				}
				fclose ($handle);
		}

	}

	function authcode($string, $operation = "DECODE", $key = '', $expiry = 0) {
	    $ckey_length = 4;
	    $key = md5($key ? $key : $GLOBALS['discuz_auth_key']);
	    $keya = md5(substr($key, 0, 16));
	    $keyb = md5(substr($key, 16, 16));
	    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';
	    $cryptkey = $keya.md5($keya.$keyc);
	    $key_length = strlen($cryptkey);
	    $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
	    $string_length = strlen($string);
	    $result = '';
	    $box = range(0, 255);
	    $rndkey = array();
	    for($i = 0; $i <= 255; $i++) {
	        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
	    }
	    for($j = $i = 0; $i < 256; $i++) {
	        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
	        $tmp = $box[$i];
	        $box[$i] = $box[$j];
	        $box[$j] = $tmp;
	    }
	    for($a = $j = $i = 0; $i < $string_length; $i++) {
	        $a = ($a + 1) % 256;
	        $j = ($j + $box[$a]) % 256;
	        $tmp = $box[$a];
	        $box[$a] = $box[$j];
	        $box[$j] = $tmp;
	        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
	    }
	    if($operation == 'DECODE') {
	        if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
	            return substr($result, 26);
	        } else {
	            return '';
	        }
	    } else {
	        return $keyc.str_replace('=', '', base64_encode($result));
	    }
	}

	/**
	 * 挂断电话
	 * 为cci准备的
	 * 张拓
	 * 2010-9-28
	 * @param $exten
	 */
	function cci_hangup($exten)
	{
		//goBack($exten,"exit");
		$mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");
		if(is_array($extensList))
		{

			if($extensList[$exten]["State"]!="InUse")
			{
				return c("通话不存在");
				//exit;
			}
			$channellisk=$this->getExtenChannelList();
			$channelId=$channellisk[$exten]["channel"];
			if(!$channelId)
			{
				return c("通道不存在");
			}
			$this->loadAGI();
			$this->asm->hangup($channelId);
			return c("已挂断");
		}
	}

	/**
	 * 分机示忙,示闲，返回操作完成后的状态
	 * 为cci准备的
	 * 张拓
	 * 2010-9-28
	 * @param $exten
	 */
	function cci_setDND($exten,$db)
	{
		@include_once ("/var/www/html/bgcrm/include/cache/cache_userexten.php");
		$admin_username = $_userexten[$exten]["username"];
		$this->loadAGI();
        $rs = $this->asm->Command("database get DND ". $exten);
        if(strstr($rs['data'],"YES"))
        {
        	 $this->asm->Command("database del DND ". $exten);
        	 $sql = "UPDATE asteriskcdrdb.report_dnd SET endtime='". time() ."',longtime=endtime-starttime WHERE username='". $admin_username ."' ORDER BY starttime DESC LIMIT 1";
			 $db->Execute($sql);
        	 return c("空闲");
        }else
        {
        	 $this->asm->Command("database put DND ". $exten ." YES");
        	 $sql = "INSERT INTO asteriskcdrdb.report_dnd SET username='". $admin_username  ."',extension='". $exten  ."',starttime='". time() ."'";
        	 $db->Execute($sql);
        	 return c("忙");
        }
	}

	/**
	 * 分机示忙,示闲，返回操作完成后的状态
	 * 为cci准备的
	 * 张拓
	 * 2010-9-28
	 * @param $exten
	 * @param $type  1 示忙，2 取消示忙
	 * @param $db
	 */
	function cci_setExtensionDND($exten,$type)
	{
		//@include_once ("/var/www/html/bgcrm/include/cache/cache_userexten.php");
		//$admin_username = $_userexten[$exten]["username"];
		$this->loadAGI();
		if($type == "1" ) // 分机示忙
		{
           $this->asm->Command("database put DND ". $exten ." YES");
        	 //$sql = "INSERT INTO asteriskcdrdb.report_dnd SET username='". $admin_username  ."',extension='". $exten  ."',starttime='". time() ."'";
        	 //$db->Execute($sql);
        	 return 1;
		}elseif ($type == "2")  // 取消示忙
		{
        	 $this->asm->Command("database del DND ". $exten);
        	 //$sql = "UPDATE asteriskcdrdb.report_dnd SET endtime='". time() ."',longtime=endtime-starttime WHERE username='". $admin_username ."' ORDER BY starttime DESC LIMIT 1";
					 //$db->Execute($sql);
						return 1;
		}

	}
	function test()
	{
				$this->loadAGI();
	//	$admin_dial = "SIP/".$exten;
		$rs = $this->asm->Command("show parkedcalls ");
		print_r($rs);
		$parkeds = $this->getparkedChannels($rs["data"]);
		//$rs =111;
		return $parkeds ;
	}
	/**
	 * 通话暂停,恢复通话，返回操作完成后的状态
	 * 为cci准备的
	 * 张拓
	 * 2010-9-28
	 * @param $exten
	 */
	function cci_setHold($exten){
		$this->loadAGI();
		$admin_dial = "SIP/".$exten;
		$rs = $this->asm->Command("show parkedcalls");
		$parkeds = $this->getparkedChannels($rs["data"]);
		//return $rs ;
		$park_num=$parkeds[$exten]['num'];
		session_start();
		$channels = $this->getChannels();
		if( strlen($channels[$admin_dial]["channel_id"])<1 ){ //通话不存在
			if($park_num =="" && $_SESSION["cci_parknum"] =="")
			{
				return c("通话不存在".$admin_dial);
				exit;
			}else //恢复通话
			{
					$n = $parknum != ""? $parknum: $_SESSION["cci_parknum"];
					$this->asm->Originate($admin_dial,$n,"from-internal","1",NULL,NULL,NULL,"UnHold_".$n,NULL,NULL,NULL,NULL);
					unset($_SESSION["cci_parknum"]);
					return c("通话已恢复");
			}
		}else //通话存在
		{
				$YourChanerid=$this->getYourChaneridByMyChannelid($channels[$admin_dial]["channel_id"]);
				$this->asm->Redirect($YourChanerid, 70, "from-internal", "1");
				sleep(2);
				$rs = $this->asm->Command("show parkedcalls");
				$parkeds = $this->getparkedChannels($rs["data"]);
				$this->asm->disconnect();
				if($parkeds[$YourChanerid]["num"])
					$parknum = $parkeds[$YourChanerid]["num"];
				else
					$parknum = $parkeds[$exten]["num"];
				 $_SESSION["cci_parknum"]=$parknum;
				 return c("通话已暂停，恢复通话号码是".$parknum);
		}

	}

	function holdCall($exten){
		$this->loadAGI();
		$admin_dial = "SIP/".$exten;
		$rs = $this->asm->Command("show parkedcalls");
		$parkeds = $this->getparkedChannels($rs["data"]);
		$park_num=$parkeds[$exten]['num'];
		$channels = $this->getChannels();

				$YourChanerid=$this->getYourChaneridByMyChannelid($channels[$admin_dial]["channel_id"]);
				$this->asm->Redirect($YourChanerid,"","7070", "from-internal", "1");
				return $YourChanerid;
	}

	function resumeHoldedCall($exten,$holdedChannel){
		$this->loadAGI();
		$YourChanerid=$this->getYourChaneridByMyChannelid($channels[$admin_dial]["channel_id"]);
		$this->asm->Redirect($holdedChannel, $exten, "from-internal", "1");
		// return $YourChanerid;
	}


	/**
	 * 通话 转接
	 * 为cci准备的
	 * 张拓
	 * 2010-9-28
	 * @param $exten
	 */
	function cci_transferCall($admin_extension,$exten)
	{
		$admin_dial = "SIP/".$admin_extension;
		$extensList = $this->getHints();
		$extensionsDndState = $this->getDNDAll();

		if(is_array($extensList))
		{
			if($extensList[$admin_extension]["stat"]!="State:InUse")
				return c("通话不存在");

			//$channellisk=$this->getExtenChannelList();
			//$mychannelId=$channellisk[$admin_extension]["channel"];
			//echo $mychannelId;die;
			$CallerID = $this->getAllChannelsInfo('CallerID');
			if($CallerID[$admin_extension]["Channel"]){
					$mychannelId=$CallerID[$admin_extension]["Channel"];
			}else{
				foreach($CallerID AS $k=>$v){
					if(preg_match("/\/$admin_extension\-/",$v["Channel"])){
							$mychannelId=$v["Channel"];
							break;
					}
				}
			}
			$YourChanerid = $this->getYourChaneridByMyChannelid($mychannelId);
			$this->asm->Redirect($YourChanerid, "",$exten, "from-internal", "1");

            return c("电话已转移成功$YourChanerid");
		}
	}
	 /*
	function cci_transferCall($admin_extension,$exten)
	{
		$admin_dial = "SIP/".$admin_extension;

		$mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");
		$extensionsDndState = $mc->get("extensionsDndState");

		if(is_array($extensList))
		{
			if($extensList[$admin_extension]["State"]!="InUse")
				return c("通话不存在");

			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$exten]=="YES")
					return c("您要转的分机已经被手动置忙，暂时不能使用");
			}

			if($extensList[$exten]["State"]=="Unavailable")
				return c("您要转的分机不在线，暂时不能使用");
			if($extensList[$exten]["State"]=="InUse")
				return c("您要转的分机正在通话中，暂时不能使用");
			if($extensList[$exten]["State"]=="Ringing")
				return c("您要转的分机正在振铃，暂时不能使用");

			$channellisk=$this->getExtenChannelList();
			$mychannelId=$channellisk[$admin_extension]["channel"];
			$YourChanerid=$this->getYourChaneridByMyChannelid($mychannelId);
			$this->asm->Redirect($YourChanerid, $exten, "from-internal", "1");

            return c("电话已转移成功$YourChanerid aaa");
		}
	}
	*/

	/**
	 *
	 * @param $monitor_exten 被监听的分机  如  801
	 * @param $admin_extension 管理分机
	 * zhangtuo
	 * 2010-8-14
	 */
	function cci_monitor($admin_extension,$monitor_exten)
	{
    $mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");
		$extensionsDndState = $mc->get("extensionsDndState");

		if(is_array($extensList))
		{
			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$admin_extension]=="YES")
					return c("您的分机已经被手动置忙，暂时不能使用");
			}

			if($extensList[$admin_extension]["State"]=="Unavailable")
				return c("您的分机不在线，暂时不能使用");
			if($extensList[$admin_extension]["State"]=="InUse")
				return c("您的分机正在通话中，暂时不能使用");
			if($extensList[$admin_extension]["State"]=="Ringing")
				 return c("您的分机正在振铃，暂时不能使用");

			if($extensList[$monitor_exten]["State"]!="InUse")
				 return c("被监听的通话不存在");

			$m_d="557$monitor_exten";
			$this->call($admin_extension,$m_d,"monitor_$monitor_exten");
			return c("操作完成，注意接电话");
		}
	}

	/**
	 * 强插
	 * @param $monitor_exten 被强插的分机  如  801
	 * @param $monitor_dial 被强插的分机的dial  如  SIP/801
	 * zhangtuo
	 * 2010-8-14
	 */
	function cci_chanspy($admin_exten,$monitor_exten)
	{
	  $mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");
		$extensionsDndState = $mc->get("extensionsDndState");

		if(is_array($extensList))
		{
			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$admin_exten]=="YES")
					return c("您的分机已经被手动置忙，暂时不能使用");
			}

			if($extensList[$admin_exten]["State"]=="Unavailable")
				return c("您的分机不在线，暂时不能使用");
			if($extensList[$admin_exten]["State"]=="InUse")
				return c("您的分机正在通话中，暂时不能使用");
			if($extensList[$admin_exten]["State"]=="Ringing")
				return c("您的分机正在振铃，暂时不能使用");
			if($extensList[$monitor_exten]["State"]!="InUse")
				return c("被强插的通话不存在");

			$m_d="558$monitor_exten";
			$this->call($admin_exten,$m_d,"chanSpy_$monitor_exten");
			return c("操作完成，注意接电话");
		}
	}

	/**
	 * 服务评分
	 * 为cci准备的
	 * 张拓
	 * 2010-9-28
	 * @param $exten
	 */
	function cci_feedback($admin_extension,$db)
	{
		//@include_once ("/var/www/html/bgcrm/include/cache/cache_userexten.php");
		$admin_username = $_userexten[$admin_extension]["username"];
		$admin_dial = "SIP/".$admin_extension;
		$this->loadAGI();
		$channels = $this->getChannels();
		if( strlen($channels[$admin_dial]["channel_id"])<1 ){
			return c("通话不存在".$admin_dial);
    		exit;
		}else
		{
			$sql = "SELECT defaultcode, customcode, enabled FROM asterisk.featurecodes WHERE featurename='feedback'";
			$finfo = $db->GetRow($sql);
			$feedkey = empty($finfo['customcode']) ? $finfo['defaultcode'] : $finfo['customcode'];
			$finfo['enabled'] == "enabled";
			if(!$finfo['enabled'] or !$feedkey) {
				$db->Close();
				return c("该功能未启用,请与管理员联系");
			}else
			{
				$YourChanerid=$this->getYourChaneridByMyChannelid($channels[$admin_dial]["channel_id"]);
				$clid = $this->asm->GetVar($YourChanerid,"CALLERID(num)");
				$uniqueid = $this->asm->GetVar($YourChanerid,"UNIQUEID");
				$this->asm->Command("database put feedback ". $uniqueid['Value'] ." ".$admin_extension);
				$db->Execute("INSERT INTO asteriskcdrdb.report_feedback SET username='". $admin_username ."',extension='". $admin_extension ."',callerid='". $clid['Value'] ."',calltime='".date('Y-m-d H:i:s')."', uniqueid='". $uniqueid['Value'] ."'");
				$this->asm->Redirect(trim($YourChanerid), $feedkey, "from-internal", "1");
				return c("已向用户播放评分录音，请挂机或准备接听下一个电话");
			}
		}
	}


	//asterisk1.8 手机打打手机【坐席分机为手机】--先呼坐席手机再打客户号码
	function clickCallPhoneToPhone($agent_phone,$client_phone,$trunk_name){
		$this->loadAGI();
		$num = "bangain".date("His");
		$request_parameters = Array(
			"Channel"	=>	"$trunk_name/$agent_phone",  //6658617中继名称
			"Exten"		=>	"$agent_phone",
			"Context"	=>	'bangian-phone2phone',
			"Priority"	=>	"1",
			"Timeout"	=>	"50000",
			//"CallerID"	=>	"\"$agent_phone\" <$agent_phone>",	#####这里是伪造的坐席呼叫中心对外的电话号码
			"CallerID"	=>	"\"$num\" <$num>",	#####这里是伪造的坐席呼叫中心对外的电话号码
			"Variable"	=>	"__clickcall=Y,__agent_phone=$num,__client_phone=$client_phone", ####增加通道变量clickcall,为了让被叫知道CID
			"Async"	=>	"true",
		);
		//dump($request_parameters);die;
		$this->asm->send_request('Originate',$request_parameters);

	}
	//asterisk1.8 普通的点击拨号，其cdr记录到普通的cdr表中
	function normal_clickcall($device,$exten,$phone,$callerid){
		$this->loadAGI();
		if(empty($callerid)){
			$callerid = $exten;
		}

		$request_parameters = Array(
			"Channel"	=>	$device,
			"Exten"		=>	$phone,
			"Context"	=>	'from-internal',
			"Priority"	=>	"1",
			"Timeout"	=>	"50000",
			"CallerID"	=>	"\"$callerid\" <$callerid>",	#####这里是伪造的坐席呼叫中心对外的电话号码
			"Variable"	=>	"__clickcall=Y,__task_id=$task_id,__clientphone=$phone", ####增加通道变量clickcall,为了让被叫知道CID
			"Async"	=>	"true",
		);
		//dump($request_parameters);die;
		$this->asm->send_request('Originate',$request_parameters);

	}

	//asterisk1.8 预览式外呼的点击拨号，走的dialplan和普通的点击拨号的不同
	function preview_clickcall($device,$exten,$phone,$task_id,$trunk,$callerid="",$callout_pre,$customer_id,$db_name){
		$this->loadAGI();
		if(empty($callerid)){
			$callerid = $exten;
		}

		$request_parameters = Array(
			"Channel"	=>	$device,
			"Exten"		=>	$phone,
			"Context"	=>	'BG-Preview',
			"Priority"	=>	"1",
			"Timeout"	=>	"50000",
			"CallerID"	=>	"\"$callerid\" <$callerid>",	#####这里是伪造的cid,如果可以透传的话
			"Variable"	=>	"__clickcall=Y,__cid=$exten,__task_id=$task_id,__clientphone=$phone,__trunk=$trunk,callerid=$callerid,__callout_pre=$callout_pre,__customer_id=$customer_id,__db_name=$db_name", ####增加通道变量clickcall,为了让被叫知道CID
			"Async"	=>	"true",
		);
		//dump($request_parameters);die;
		$this->asm->send_request('Originate',$request_parameters);

	}

	//代接电话
	//pickup("879","331"){
	function pickup($exten,$phone){
		$arrHint = $this->getHint($exten);
		$device = $arrHint['device'];
		$this->loadAGI();
		$callerid = "pickup-".$exten;

		$request_parameters = Array(
			"Channel"	=>	$device,
			"Exten"		=>	"**".$phone,
			"Context"	=>	'from-internal',
			"Priority"	=>	"1",
			"Timeout"	=>	"50000",
			"CallerID"	=>	"\"$callerid\" <$callerid>",
			"Async"	=>	"true",
		);
		$this->asm->send_request('Originate',$request_parameters);
	}


	//获取parked分机,返回暂停的分机列表数组
	function getParkedExten(){
		$this->loadAGI();
		$arr = $this->asm->command('parkedcalls show');
		$lines = explode("\n",$arr['data']);

		$arrParkedExten = Array();
		$j=0;
		$regExp = '/(\d+)\s+([^\s]+)\s+\(/';
		for($i=3;1;$i++){
			//dump($lines[$i]);die;
			if( preg_match($regExp,$lines[$i],$arrMatch) ){
				$arrParkedExten[$arrMatch[2]] = array(
					'Num' => $arrMatch[1],
					'Channel' => $arrMatch[2],
					'Exten' => $arrMatch[1],
				);
			}else{
				break;
			}
			$i++;
		}
		//dump($arrParkedExten);die;
		return $arrParkedExten;
	}

	//通话暂停
	function callPause($admin_extension,$admin_dial){
		$this->loadAGI();
		$channels = $this->getChannels();
		$YourChanerid=$this->getYourChaneridByMyChannelid($channels[$admin_dial]["channel_id"]);
		//dump($YourChanerid);die;
		$this->asm->Redirect($YourChanerid, "","7070", "from-internal", "1");
		sleep(2);
		//求出停靠的分机点
		$arrParkedExten = $this->getParkedExten();
		//return $arrParkedExten[$YourChanerid]['Num'];
		return $YourChanerid;
	}

	//通话恢复
	//function restoreCall($device,$parkedExten){
	function restoreCall($exten,$parkedChannel){
		$this->loadAGI();
		/*
		//$callerid = "restoreCall";
		$callerid = $_SESSION['user_info']['extension'];
		$request_parameters = Array(
			"Channel"	=>	$device,
			"Exten"		=>	$parkedExten,
			"Context"	=>	'from-internal',
			"Priority"	=>	"1",
			"Timeout"	=>	"50000",
			"CallerID"	=>	"\"$callerid\" <$callerid>",
			"Async"	=>	"true",
		);
		//dump($request_parameters);die;
		$this->asm->send_request('Originate',$request_parameters);
		*/
		$this->asm->Redirect($parkedChannel, "",$exten, "from-internal", "1");
	}



	function cci_clickCallCid($admin_extension,$phone,$cid,$resid,$prjid)
	{
		$admin_dial = "SIP/".$admin_extension;

		$mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");
		$extensionsDndState = $mc->get("extensionsDndState");

		if(is_array($extensList))
		{

			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$exten]=="YES")
					return c("您的".$admin_extension."分机已经被手动置忙，暂时不能使用");
			}

			if($extensList[$admin_extension]["State"]=="Unavailable")
				return c("您的".$admin_extension."分机不在线，暂时不能使用");
			if($extensList[$admin_extension]["State"]=="InUse")
				return c("您的".$admin_extension."分机正在通话中，暂时不能使用");
			if($extensList[$admin_extension]["State"]=="Ringing")
				return c("您的".$admin_extension."分机正在振铃，暂时不能使用");
			$this->loadAGI();
			$this->asm->Command("database put AMPUSER ".$admin_extension."/outboundcid  ".$cid);
			$r = $this->asm->Originate($admin_dial,$phone,"from-internal","1",NULL,NULL,NULL,"clickcall-".$admin_extension."<". $phone .">","BG_RESID=".$resid."|BG_PRJID=".$prjid."|BG_EXTEN=".$admin_extension."|BG_CID=".$cid,NULL,NULL,NULL);
			//sleep(1);
            return c("正在呼叫，请稍候");
		}
	}

		//点击拨号
	function cci_clickCall($admin_extension,$phone)
	{
		$admin_dial = "SIP/".$admin_extension;

		$mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");
		$extensionsDndState = $mc->get("extensionsDndState");

		if(is_array($extensList))
		{

			if(is_array($extensionsDndState))
			{
				if($extensionsDndState[$exten]=="YES")
					return c("您的".$admin_extension."分机已经被手动置忙，暂时不能使用");
			}

			if($extensList[$admin_extension]["State"]=="Unavailable")
				return c("您的".$admin_extension."分机不在线，暂时不能使用");
			if($extensList[$admin_extension]["State"]=="InUse")
				return c("您的".$admin_extension."分机正在通话中，暂时不能使用");
			if($extensList[$admin_extension]["State"]=="Ringing")
				return c("您的".$admin_extension."分机正在振铃，暂时不能使用");
			$this->loadAGI();
			$r = $this->asm->Originate($admin_dial,$phone,"from-internal","1",NULL,NULL,NULL,"clickcall-".$admin_extension."<". $phone .">",NULL,NULL,NULL,NULL);
			//$r = $this->asm->Originate($admin_dial,$phone,"from-internal","1",NULL,NULL,NULL,"61115900 <$phone>",NULL,NULL,NULL,NULL);
			//sleep(1);
            return c("正在呼叫，请稍候");
		}
	}

	/**
	 * 通话转移到某个功能键
	 * 为cci准备的
	 * 张拓
	 * 2010-9-28
	 * @param $exten
	 */
	function cci_transfer2featurecode($admin_extension,$code)
	{
		$admin_dial = "SIP/".$admin_extension;

		$mc = $this->loadMemCache();
		$extensList = $mc->get("extensList");
		$extensionsDndState = $mc->get("extensionsDndState");

		if(is_array($extensList))
		{
			if($extensList[$admin_extension]["State"]!="InUse")
				return c("通话不存在");
			$channellisk=$this->getExtenChannelList();
			$mychannelId=$channellisk[$admin_extension]["channel"];
			$YourChanerid=$this->getYourChaneridByMyChannelid($mychannelId);
			$this->asm->Redirect($YourChanerid, $code, "from-internal", "1");
            return c("电话已成功转移到".$code);
		}
	}

	/*
	lch: 获取黑名单
	*/
	function getBlackList(){
		$this->loadAGI();
		if($status == 0){ //示忙
            $rs = $this->asm->Command("database show blacklist");
			$lines 	= explode("\n", $rs['data']);
			//dump($lines);
			foreach($lines as $key => $line){
				if(!preg_match("/dnsmgr/", $line)){
					$arrBlack[] =  preg_split("/\s+/",str_replace("/blacklist/","",$line));
				}
			}
		}
		array_shift($arrBlack);
		array_pop($arrBlack);
		array_pop($arrBlack);
		array_pop($arrBlack);
		return $arrBlack;
	}
	/*
	lch: 添加黑名单
	*/
	function insertBlack($phone){
		$this->loadAGI();
		$result = $this->asm->Command("database put blacklist ".$phone." 1 ");
		//dump($result);die;
		return $result;
	}
	/*
	lch: 移除黑名单
	*/
	function delBlack($phone){
		$this->loadAGI();
		$result = $this->asm->Command("database del blacklist ".$phone);
		//dump($result);die;
		return $result;
	}


	/*
	lch: 等待队列
	*/
	function waitingQueueInfo($task_id){
		$this->loadAGI();
		if($status == 0){
            $rs = $this->asm->Command("queue show AutoCall_".$task_id);
			$arr 	= explode("Callers", $rs['data']);
			$arrF = explode("\n",trim($arr[1]));
			array_shift($arrF);
			foreach($arrF as $val){
				$arrD[] = explode("(",trim($val));
			}

			foreach($arrD as $val){
				$arrW2[] = trim(array_pop(explode(".",$val[0])));
				$arrR[trim(array_pop(explode(".",$val[0])))] = trim(str_replace("wait:","",array_shift(explode(",",$val[1]))));
			}
			$arrData = $this->getAllChannels();

			foreach($arrData as $val){
				if( in_array($val["Channel"],$arrW2) ){
					$val["waitTimes"] = $arrR[$val["Channel"]];
					$arrT[] = $val;
				}
			}


			//var_export($arrT);
			//dump($arrData);
		}
		return $arrT;
	}

	function getAllChannels(){
		$rs = $this->asm->Command("core show channels concise");
		if(count($rs) == 0 )
		{

			return null;
		}
		else
		{

			$lines=explode("\n", str_replace("\r\n", "\n", $rs["data"]));
			foreach($lines as $line)
			{
				$arr=split("!",$line);

				if(count($arr)>10)
				{
					$ary['Channel']=$arr[0];
					$ary['Context']=$arr[1];
					$ary['Extension']=$arr[2];
					$ary['Priority']=$arr[3];
					$ary['State']=$arr[4];
					$ary['Application']=$arr[5];
					$ary['Data']=$arr[6];
					$ary['CallerID']=$arr[7];
					$ary['Accountcode']=$arr[8];
					$ary['Amaflags']=$arr[9];
					//$ary['Billsec']=$arr[11];//没有通话时长，这个参数不准确!
					$ary['Duration']=$arr[11];
					$ary['BridgedTo']=$arr[12];
					$key = $ary['Channel'];

					$channels[$key]=$ary;
					unset($ary);
				}
			}
			return $channels;
		}
	}

	//wjj--add
	function getAllChannelsByCid(){
			$rs = $this->asm->Command("core show channels concise");
			if(count($rs) == 0 )
			{
					return null;
			} else
			{
					$channels = Array();
					$arr = Array();
					$match = Array();
					$lines=explode("\n", str_replace("\r\n", "\n", $rs["data"]));
					foreach($lines as $line)
					{
							$arr=split("!",$line);
							//if( preg_match('/\/([^/]+)-[\da-z]+$/',$arr[0],$match) )
							if( preg_match('/\/(\d+)-[\da-z]+$/',$arr[0],$match) )
							{
									$ary['Channel']=$arr[0];
									$ary['Context']=$arr[1];
									$ary['Extension']=$arr[2];
									$ary['Priority']=$arr[3];
									$ary['State']=$arr[4];
									$ary['Application']=$arr[5];
									$ary['Data']=$arr[6];
									$ary['CallerID']=$arr[7];
									$ary['Accountcode']=$arr[8];
									$ary['Amaflags']=$arr[9];
									//$ary['Billsec']=$arr[11];//没有通话时长，这个参数不准确!
									$ary['Duration']=$arr[11];
									$ary['BridgedTo']=$arr[12];

									$channels[$match[1]]=$ary;
							}
					}
//var_dump( $channels);die;
					return $channels;
			}
	}

}

