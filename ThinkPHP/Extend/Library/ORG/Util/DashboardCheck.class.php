<?php
class Dashboard{
	//获取linux服务器性能CPU、内存、硬盘等使用率 PHP
	function obtener_info_de_sistema(){
		$arrInfo=array();
		$arrExec=array();
		$arrParticiones=array();
		$varExec="";

		if($fh=fopen("/proc/meminfo", "r")) {
			while($linea=fgets($fh, "4048")) {
				// Aqui parseo algunos parametros
				if(preg_match("/^MemTotal:[[:space:]]+([[:digit:]]+) kB/", $linea, $arrReg)) {
					$arrInfo["MemTotal"]=trim($arrReg[1]);
				}
				if(preg_match("/^MemFree:[[:space:]]+([[:digit:]]+) kB/", $linea, $arrReg)) {
					$arrInfo["MemFree"]=trim($arrReg[1]);
				}
				if(preg_match("/^Buffers:[[:space:]]+([[:digit:]]+) kB/", $linea, $arrReg)) {
					$arrInfo["MemBuffers"]=trim($arrReg[1]);
				}
				if(preg_match("/^SwapTotal:[[:space:]]+([[:digit:]]+) kB/", $linea, $arrReg)) {
					$arrInfo["SwapTotal"]=trim($arrReg[1]);
				}
				if(preg_match("/^SwapFree:[[:space:]]+([[:digit:]]+) kB/", $linea, $arrReg)) {
					$arrInfo["SwapFree"]=trim($arrReg[1]);
				}
				if(preg_match("/^Cached:[[:space:]]+([[:digit:]]+) kB/", $linea, $arrReg)) {
					$arrInfo["Cached"]=trim($arrReg[1]);
				}
			}
			fclose($fh);
		}

		if($fh=fopen("/proc/cpuinfo", "r")) {
			while($linea=fgets($fh, "4048")) {
				// Aqui parseo algunos parametros
				if(preg_match("/^model name[[:space:]]+:[[:space:]]+(.*)$/", $linea, $arrReg)) {
					$arrInfo["CpuModel"]=trim($arrReg[1]);
				}
				if(preg_match("/^vendor_id[[:space:]]+:[[:space:]]+(.*)$/", $linea, $arrReg)) {
					$arrInfo["CpuVendor"]=trim($arrReg[1]);
				}
				if(preg_match("/^cpu MHz[[:space:]]+:[[:space:]]+(.*)$/", $linea, $arrReg)) {
					$arrInfo["CpuMHz"]=trim($arrReg[1]);
				}
			}
			fclose($fh);
		}


		if($fh=fopen("/proc/stat", "r")) {
			while($linea=fgets($fh, "4048")) {
				if(preg_match("/^cpu[[:space:]]+([[:digit:]]+)[[:space:]]+([[:digit:]]+)[[:space:]]+([[:digit:]]+)" .
						"[[:space:]]+([[:digit:]]+)[[:space:]]+([[:digit:]]+)[[:space:]]+([[:digit:]]+)" .
						"[[:space:]]+([[:digit:]]+)[[:space:]]?/", $linea, $arrReg)) {
					$cpuActivo=$arrReg[1]+$arrReg[2]+$arrReg[3]+$arrReg[5]+$arrReg[6]+$arrReg[7];
					$cpuTotal=$cpuActivo+$arrReg[4];
					if($cpuTotal>0 and $cpuActivo>=0) {
						$arrInfo["CpuUsage"]=$cpuActivo/$cpuTotal;
					} else {
						$arrInfo["CpuUsage"]="";
					}

				}
			}

			fclose($fh);
		}

		exec("/usr/bin/uptime", $arrExec, $varExec);

		if($varExec=="0") {
			//if(ereg(" up[[:space:]]+([[:digit:]]+ days,)?([[:space:]]+[[:digit:]]{2}:[[:digit:]]{2}), ", $arrExec[0], $arrReg)) {
			if(preg_match("/up[[:space:]]+([[:digit:]]+ days?,)?(([[:space:]]*[[:digit:]]{1,2}:[[:digit:]]{1,2}),?)?([[:space:]]*[[:digit:]]+ min)?/",
					$arrExec[0],$arrReg)) {
				if(!empty($arrReg[3]) and empty($arrReg[4])) {
					list($uptime_horas, $uptime_minutos) = explode(":", $arrReg[3]);
					$arrInfo["SysUptime"]=$arrReg[1] . " $uptime_horas hour(s), $uptime_minutos minute(s)";
				} else if (empty($arrReg[3]) and !empty($arrReg[4])) {
					// Esto lo dejo asi
					$arrInfo["SysUptime"]=$arrReg[1].$arrReg[3].$arrReg[4];
				} else {
					$arrInfo["SysUptime"]=$arrReg[1].$arrReg[3].$arrReg[4];
				}
			}
		}


		// Infomacion de particiones
		//- TODO: Aun no se soportan lineas quebradas como la siguiente:
		//-       /respaldos/INSTALADORES/fedora-1/disco1.iso
		//-                              644864    644864         0 100% /mnt/fc1/disc1

		exec("/bin/df -P /etc/fstab", $arrExec, $varExec);

		if($varExec=="0") {
			foreach($arrExec as $lineaParticion) {
				if(preg_match("/^([\/-_\.[:alnum:]|-]+)[[:space:]]+([[:digit:]]+)[[:space:]]+([[:digit:]]+)[[:space:]]+([[:digit:]]+)" .
						"[[:space:]]+([[:digit:]]{1,3}%)[[:space:]]+([\/-_\.[:alnum:]]+)$/", $lineaParticion, $arrReg)) {
					$arrTmp="";
					$arrTmp["fichero"]=$arrReg[1];
					$arrTmp["num_bloques_total"]=$arrReg[2];
					$arrTmp["num_bloques_usados"]=$arrReg[3];
					$arrTmp["num_bloques_disponibles"]=$arrReg[4];
					$arrTmp["uso_porcentaje"]=$arrReg[5];
					$arrTmp["punto_montaje"]=$arrReg[6];
					$arrInfo["particiones"][]=$arrTmp;
				}
			}
		}
		return $arrInfo;
	}
	
	
	//获取linux服务器性能CPU、内存、硬盘等使用率 
	function get_used_status(){
	  $fp = popen('top -b -n 2 | grep -E "^(Cpu|Mem|Tasks)"',"r");//获取某一时刻系统cpu和内存使用情况
	  $rs = "";
	  while(!feof($fp)){
	   $rs .= fread($fp,1024);
	  }
	  pclose($fp);
	  $sys_info = explode("\n",$rs);
	  $tast_info = explode(",",$sys_info[3]);//进程 数组
	  $cpu_info = explode(",",$sys_info[4]);  //CPU占有量  数组
	  $mem_info = explode(",",$sys_info[5]); //内存占有量 数组
	  //正在运行的进程数
	  $tast_running = trim(trim($tast_info[1],'running')); 
	  
	  
	  //CPU占有量
	  $cpu_usage = trim(trim($cpu_info[0],'Cpu(s): '),'%us');  //百分比
	  
	  //内存占有量
	  $mem_total = trim(trim($mem_info[0],'Mem: '),'k total');  
	  $mem_used = trim($mem_info[1],'k used');
	  $mem_usage = round(100*intval($mem_used)/intval($mem_total),2);  //百分比
	  
	  /*硬盘使用率 begin*/
	  $fp = popen('df -lh | grep -E "^(/)"',"r");
	  $rs = fread($fp,1024);
	  pclose($fp);
	  $rs = preg_replace("/\s{2,}/",' ',$rs);  //把多个空格换成 “_”
	  $hd = explode(" ",$rs);
	  $hd_avail = trim($hd[3],'G'); //磁盘可用空间大小 单位G
	  $hd_usage = trim($hd[4],'%'); //挂载点 百分比
	  //print_r($hd);
	  /*硬盘使用率 end*/  
	  
	  //检测时间
	  $fp = popen("date +\"%Y-%m-%d %H:%M\"","r");
	  $rs = fread($fp,1024);
	  pclose($fp);
	  $detection_time = trim($rs);
	  
	  /*获取IP地址  begin*/
	  /*
	  $fp = popen('ifconfig eth0 | grep -E "(inet addr)"','r');
	  $rs = fread($fp,1024);
	  pclose($fp);
	  $rs = preg_replace("/\s{2,}/",' ',trim($rs));  //把多个空格换成 “_”
	  $rs = explode(" ",$rs);
	  $ip = trim($rs[1],'addr:');
	  */
	  /*获取IP地址 end*/
	  /*
	  $file_name = "/tmp/data.txt"; // 绝对路径: homedata.dat  
	  $file_pointer = fopen($file_name, "a+"); // "w"是一种模式，详见后面 
	  fwrite($file_pointer,$ip); // 先把文件剪切为0字节大小， 然后写入 
	  fclose($file_pointer); // 结束
	  */
	  
	  return array('cpu_usage'=>$cpu_usage,'mem_usage'=>$mem_usage,'hd_avail'=>$hd_avail,'hd_usage'=>$hd_usage,'tast_running'=>$tast_running,'detection_time'=>$detection_time);
	 }
}

?>