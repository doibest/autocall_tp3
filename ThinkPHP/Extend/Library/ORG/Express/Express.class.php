<?php
/**
 * Express.class.php 快递查询类 v1.0
 *
 * @copyright        福星高照
 * @license          http://www.25531.com
 * @lastmodify       2014-08-22
 */
class Express
{
    /*
     * 网页内容获取方法
    */
    private function getcontent($url)
    {
        if (function_exists("file_get_contents")) {
            $file_contents = file_get_contents($url);
        } else {
            $ch      = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $file_contents = curl_exec($ch);
            curl_close($ch);
        }
        return $file_contents;
    }
	
    private function getcontent2($url){
		$oCurl = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
		}
		curl_setopt($oCurl, CURLOPT_URL, $url);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
		$sContent = curl_exec($oCurl);
		$aStatus = curl_getinfo($oCurl);
		curl_close($oCurl);
		if(intval($aStatus["http_code"])==200){
			return $sContent;
		}else{
			return false;
		}
    }

    /*
     * 获取对应名称和对应传值的方法
    */
    private function expressname($order)
    {
        $name   = json_decode($this->getcontent("http://www.kuaidi100.com/autonumber/auto?num={$order}"), true);
		//echo "<pre>";
		//var_dump($name);die;
        $result = $name[0]['comCode'];
        if (empty($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /*
     * 返回$data array      快递数组查询失败返回false
     * @param $order        快递的单号
     * $data['ischeck'] ==1 已经签收
     * $data['data']        快递实时查询的状态 array
	 
		字段名称    　　字段含义
		com    物流公司编号
		nu    物流单号
		time    每条跟踪信息的时间
		context    每条跟综信息的描述
		state    快递单当前的状态 ：　
		0：在途，即货物处于运输过程中；
		1：揽件，货物已由快递公司揽收并且产生了第一条跟踪信息；
		2：疑难，货物寄送过程出了问题；
		3：签收，收件人已签收；
		4：退签，即货物由于用户拒签、超区等原因退回，而且发件人已经签收；
		5：派件，即快递正在进行同城派件；
		6：退回，货物正处于退回发件人的途中；
		该状态还在不断完善中，若您有更多的参数需求，欢迎发邮件至 kuaidi@kingdee.com 提出。
		status    查询结果状态：
		0：物流单暂无结果，
		1：查询成功，
		2：接口出现异常，
		message    无意义，请忽略
		condition    无意义，请忽略
		ischeck    无意义，请忽略
    */
    public function getorder($order)
    {
        $keywords = $this->expressname($order);
        if (!$keywords) {
            return false;
        } else {
            $result = $this->getcontent("http://www.kuaidi100.com/query?type={$keywords}&postid={$order}");
            $data   = json_decode($result, true);
            return $data;
        }
    }
}
?>
