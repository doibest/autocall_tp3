<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2012 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// $Id: ThinkPHP.php 2791 2012-02-29 10:08:57Z liu21st $

// ThinkPHP 入口文件

//记录开始运行时间
$GLOBALS['_beginTime'] = microtime(TRUE);
// 记录内存初始使用
define('MEMORY_LIMIT_ON',function_exists('memory_get_usage'));
if(MEMORY_LIMIT_ON) $GLOBALS['_startUseMems'] = memory_get_usage();
defined('APP_PATH') or define('APP_PATH', dirname($_SERVER['SCRIPT_FILENAME']).'/');
defined('RUNTIME_PATH') or define('RUNTIME_PATH','/tmp/Runtime/');//wjj--modify 勿删:移除缓存文件,防止主程序过于庞大.SVN 规范的时候这个地方最好改为这样
defined('TMP_UPLOAD') or define('TMP_UPLOAD','/tmp/IPPBX_Tmp_Upload/');//wjj--add 勿删:临时上载文件夹，比如导入号码等。防止主程序垃圾文件过多过于庞大。
defined('APP_DEBUG') or define('APP_DEBUG',false); // 是否调试模式
$runtime = defined('MODE_NAME')?'~'.strtolower(MODE_NAME).'_runtime.php':'~runtime.php';
defined('RUNTIME_FILE') or define('RUNTIME_FILE',RUNTIME_PATH.$runtime);

$al = '/var/www/html/data/system/robot.al';
if( !file_exists($al) ){
    shell_exec('/opt/bin/inital');
}

if(!APP_DEBUG && is_file(RUNTIME_FILE)) {
    // 部署模式直接载入运行缓存
    require RUNTIME_FILE;
}else{
    // 系统目录定义
    defined('THINK_PATH') or define('THINK_PATH', dirname(__FILE__).'/');
    // 加载运行时文件
    require THINK_PATH.'Common/runtime.php';
}

