<?php
//if (!defined('THINK_PATH'))	exit();

$globalConf = parse_ini_file("/etc/BG.conf", TRUE);
$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];

return array(
    'DB_DEPLOY_TYPE'=>1,
	'DEFAULT_MODULE' =>	'Index',
	'DB_TYPE'=>'mysql',
    'DB_HOST'=>$globalConf['mysql']['dbhost'],
    //'DB_NAME'=>$globalConf['mysql']['dbname'],
    'DB_NAME'=>$db_name,
    'DB_USER'=>$globalConf['mysql']['dbuser'],
    'DB_PWD'=>$globalConf['mysql']['dbpass'],
    'DB_PORT'=>$globalConf['mysql']['dbport'],
	'PBX_HOST'=>$globalConf['asterisk']['amihost'],
	'PBX_PORT'=>$globalConf['asterisk']['amiport'],
	'PBX_USER'=>$globalConf['asterisk']['amiuser'],
	'PBX_PWD'=>$globalConf['asterisk']['amipass'],
	'LANG_AUTO_DETECT'=>true,
	'LANG_SWITCH_ON'=> true,
	'DEFAULT_LANG' => 'zh_CN', // Ä¬ÈÏÓïÑÔ
	'LANG_LIST'=>'zh_CN,en_ES',
	'DB_CONFIG1' => array(
	    'db_type'  => 'mysql',
	    'db_user'  => $globalConf['mysql']['dbuser'],
	    'db_pwd'   => $globalConf['mysql']['dbpass'],
	    'db_host'  => $globalConf['asterisk']['amihost'],
	    'db_port'  => '3306',
	    'db_name'  => $db_name,
	    'db_charset'=>    'utf8',
	),
	//配置asterisk数据库
	'DB_CONFIG2' => array(
	    'db_type'  => 'mysql',
	    'db_user'  => $globalConf['mysql']['dbuser'],
	    'db_pwd'   => $globalConf['mysql']['dbpass'],
	    'PBX_HOST'  => $globalConf['mysql']['dbhost'],
	    'db_port'  => $globalConf['mysql']['dbport'],
	    'db_name'  => 'asterisk',
	    'db_charset'=>    'utf8',
	),

	);
?>
