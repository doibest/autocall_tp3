<?php
class WorkforceManagementAction extends Action{
	//班次定义
	function shiftDefinition(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Shift Definition";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function shiftDefinitionData(){
		$username = $_SESSION['user_info']['username'];
		$shift_definition = new Model("pb_shift_definition");
		$count = $shift_definition->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $shift_definition->limit($page->firstRow.','.$page->listRows)->select();
		$row_status = array('Y'=>'启用','N'=>'禁用');
		foreach($arrData as &$val){
			$shift_status = $row_status[$val['shift_status']];
			$val['shift_status'] = $shift_status;

			$val["working_hours"] = $val["start_working_hours"]."~".$val["end_working_hours"];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addShift(){
		checkLogin();

		$this->display();
	}

	function insertShift(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$shift_definition = new Model("pb_shift_definition");

		$shift_background = empty($_REQUEST['shift_background']) ? "#ffffff" : $_REQUEST['shift_background'];
		$subsidy = empty($_REQUEST['subsidy']) ? "0" : $_REQUEST['subsidy'];
		$arrData = Array(
			'createtime'=>date("Y-m-d H:i:s"),
			'createname'=>$username,
			'dept_id'=>$d_id,
			'shift_name'=>$_REQUEST['shift_name'],
			'start_working_hours'=>$_REQUEST['start_working_hours'],
			'end_working_hours'=>$_REQUEST['end_working_hours'],
			'hours'=>$_REQUEST['hours'],
			'shift_background'=>$shift_background,
			'subsidy'=>$subsidy,
			'shift_status'=>$_REQUEST['shift_status'],
			'description'=>$_REQUEST['description'],
		);
		//dump($arrData);die;
		$result = $shift_definition->data($arrData)->add();
		if ($result){
			$this->shiftSetCache();
			$this->shiftDefinitionCahce();
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function editShift(){
		checkLogin();
		$id = $_REQUEST["id"];
		$shift_definition = new Model("pb_shift_definition");
		$arrData = $shift_definition->where("id = '$id'")->find();
		$this->assign("id",$id);
		$this->assign("arrData",$arrData);

		$this->display();
	}

	function updateShift(){
		$id = $_REQUEST["id"];

		$shift_definition = new Model("pb_shift_definition");
		$arrData = Array(
			'modify_time'=>date("Y-m-d H:i:s"),
			'shift_name'=>$_REQUEST['shift_name'],
			'start_working_hours'=>$_REQUEST['start_working_hours'],
			'end_working_hours'=>$_REQUEST['end_working_hours'],
			'hours'=>$_REQUEST['hours'],
			'shift_background'=>$_REQUEST['shift_background'],
			'subsidy'=>$_REQUEST['subsidy'],
			'shift_status'=>$_REQUEST['shift_status'],
			'description'=>$_REQUEST['description'],
		);
		//dump($arrData);die;
		$result = $shift_definition->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			$this->shiftSetCache();
			$this->shiftDefinitionCahce();
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteShift(){
		$id = $_REQUEST["id"];
		$shift_definition = new Model("pb_shift_definition");
		$result = $shift_definition->where("id in ($id)")->delete();
		if ($result){
			$this->shiftSetCache();
			$this->shiftDefinitionCahce();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function shiftDefinitionCahce(){
		//header("Content-Type:text/html; charset=utf-8");
		$shift_definition = new Model("pb_shift_definition");
		$arrData = $shift_definition->select();     //->where("shift_status = 'Y'")
		foreach($arrData as $key=>&$val){
			$val["working_hours"] = $val["start_working_hours"]."~".$val["end_working_hours"];
			$arrIDName[$val["id"]] = $val["shift_name"];
			$arrIDBackgroud[$val["id"]] = $val["shift_background"];
			$arrWorkingHours[$val["id"]] = $val["working_hours"];
		}
		$arrF = array(
			"IDName" => $arrIDName,
			"IDBackgroud" => $arrIDBackgroud,
			"WorkingHours" => $arrWorkingHours,
		);
		//dump($arrF);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F("shiftDefinition",$arrF,"BGCC/Conf/crm/$db_name/");
		//F("shiftDefinition",$arrF,"BGCC/Conf/");
	}


	//班次设置
	function shiftSetList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Shift Set";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function shiftSetData(){
		$username = $_SESSION['user_info']['username'];

		$shift_set = new Model("pb_shift_set");
		$fields = "st.createtime,st.createname,st.dept_id,st.shift_name,st.start_working_hours,st.end_working_hours,st.hours,st.shift_background,st.subsidy,st.max_classes,st.limited_duty,st.description,st.shift_status,s.id,s.shift_id,s.work_day,s.shift_num";

		$work_day = $_REQUEST["work_day"];
		$where = "1 ";
		if($work_day != "all"){
			$where .= empty($work_day) ? "" : " AND s.work_day = '$work_day'";
		}
		$count = $shift_set->table("pb_shift_set s")->field($fields)->join("pb_shift_definition st on (s.shift_id = st.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $shift_set->order("s.id asc,s.shift_id asc")->table("pb_shift_set s")->field($fields)->join("pb_shift_definition st on (s.shift_id = st.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$week_row = array("monday"=>"星期一","tuesday"=>"星期二","wednesday"=>"星期三","thursday"=>"星期四","friday"=>"星期五","saturday"=>"星期六","sunday"=>"星期日");
		foreach($arrData as &$val){
			$work_day = $week_row[$val["work_day"]];
			$val["work_day"] = $work_day;

			$val["working_hours"] = $val["start_working_hours"]."~".$val["end_working_hours"];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertShiftset(){
		$shift_set = new Model("pb_shift_set");
		/*
		$del = $shift_set->where("shift_id = '$shift_id'")->delete();
		*/
		$work_day = $_REQUEST['work_day'];
		$count = count($work_day);
		$shift_id = $_REQUEST['shift_id'];
		for($i=0;$i<$count;$i++){
			$res[$i] = $shift_set->where("shift_id = '$shift_id' AND work_day = '".$work_day[$i]."'")->delete();
		}
		$sql = "insert into pb_shift_set(`shift_id`,`work_day`,`shift_num`) values";
		$value = "";
		foreach($work_day as $val){
			$str = "(";
			$str .= "'" .$_REQUEST['shift_id']. "',";
			$str .= "'" .$val. "',";
			$str .= "'" .$_REQUEST['shift_num']. "'";
			$str .= ")";
			$value .= empty($value) ? "$str" : ",$str";
		}
		if( $value ){
			$sql .= $value;
			$result = $shift_set->execute($sql);
		}
		if ($result){
			$this->shiftSetCache();
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function editShiftSet(){
		checkLogin();
		$id = $_REQUEST["id"];
		$shift_set = new Model("pb_shift_set");
		$arrData = $shift_set->where("id = '$id'")->find();
		$week_row = array("monday"=>"星期一","tuesday"=>"星期二","wednesday"=>"星期三","thursday"=>"星期四","friday"=>"星期五","saturday"=>"星期六","sunday"=>"星期日");
		$arrData["work_day"] = $week_row[$arrData["work_day"]];
		$this->assign("id",$id);
		$this->assign("arrData",$arrData);

		$this->display();
	}

	function updateShiftset(){
		$id = $_REQUEST['id'];
		$shift_set = new Model("pb_shift_set");
		$arrData = Array(
			'shift_num'=>$_REQUEST['shift_num'],
		);
		$result = $shift_set->data($arrData)->where("id=$id")->save();
		if ($result !== false){
			$this->shiftSetCache();
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteShiftset(){
		$id = $_REQUEST["id"];
		$shift_set = new Model("pb_shift_set");
		$result = $shift_set->where("id in ($id)")->delete();
		if ($result){
			$this->shiftSetCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function deleteAllShiftset(){
		$id = $_REQUEST["id"];
		$shift_set = new Model("pb_shift_set");
		$result = $shift_set->execute("truncate table pb_shift_set");
		if ($result !== false){
			$this->shiftSetCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function shiftSetCache(){
		header("Content-Type:text/html; charset=utf-8");
		$shift_set = new Model("pb_shift_set");
		$fields = "st.createtime,st.createname,st.dept_id,st.shift_name,st.start_working_hours,st.end_working_hours,st.hours,st.shift_background,st.subsidy,st.max_classes,st.limited_duty,st.description,st.shift_status,s.id,s.shift_id,s.work_day,s.shift_num";

		$arrData = $shift_set->order("s.id asc,s.shift_id asc")->table("pb_shift_set s")->field($fields)->join("pb_shift_definition st on (s.shift_id = st.id)")->where("st.shift_status = 'Y'")->select();

		$week_row = array("monday"=>"星期一","tuesday"=>"星期二","wednesday"=>"星期三","thursday"=>"星期四","friday"=>"星期五","saturday"=>"星期六","sunday"=>"星期日");
		foreach($arrData as $key=>&$val){
			$val["en_work_day"] = $val["work_day"];
			$work_day = $week_row[$val["work_day"]];
			$val["work_day"] = $work_day;
			$val["working_hours"] = $val["start_working_hours"]."~".$val["end_working_hours"];

			$arrIDName[$val["shift_id"]] = $val["shift_name"];
			$arrIDBackgroud[$val["shift_id"]] = $val["shift_background"];
			$arrWorkingHours[$val["shift_id"]] = $val["working_hours"];
			$arrNameBackground[$val["shift_name"]] = $val["shift_background"];
		}
		$arrT = $this->groupBy($arrData,"shift_id");
		$arrT2 = $this->groupBy2($arrData,"en_work_day");

		$arrF = array(
			"IDName" => $arrIDName,		//班次ID=》班次名称
			"IDBackgroud" => $arrIDBackgroud,		//班次ID=》背景色
			"NameBackground" => $arrNameBackground,		//班次名称=》背景色
			"WorkingHours" => $arrWorkingHours,		//班次ID=》工作时间段
			"IDWeekNum" => $arrT,		//班次ID=》每周的排班人数
			"WeekIDNum" => $arrT2,		//日期=》没班人数
		);

		//dump($arrF);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F("shiftSet",$arrF,"BGCC/Conf/crm/$db_name/");
		//F("shiftSet",$arrF,"BGCC/Conf/");
	}

	function groupBy2($arr, $key_field){
		$ret = array();
		foreach ($arr as $val){
			$key = $val[$key_field];
			$ret[$key][] = $val;
		}
		$row = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
		foreach($ret as $key=>&$val){
			foreach($ret[$key] as $v){
				$arrWeekNum[$v["shift_id"]] = $v["shift_num"];
			}

			$arrF[$key] = $arrWeekNum;
		}

		//dump($arrF);die;
		return $arrF;
	}

	function groupBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $val){
			$key = $val[$key_field];
			$ret[$key][] = $val;
		}
		$row = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
		foreach($ret as $key=>&$val){
			foreach($ret[$key] as $v){
				$arrWeekNum[$v["en_work_day"]] = $v["shift_num"];
			}
			foreach($row as $vm){
				if(!$arrWeekNum[$vm]){
					$arrWeekNum[$vm] = "0";
				}
			}
			$arrF[$key] = $arrWeekNum;
		}

		//dump($arrF);die;
		return $arrF;
	}


	//排班表
	function schedulingList(){
		header("Content-Type:text/html; charset=utf-8");
		checkLogin();
		//分配增删改的权限
		$menuname = "Scheduling Table";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$date=date('Y-m-d');
		//$date="2015-02-01";
		$fieldList = $this->getTabTitle($date);
		$this->assign("fieldList",$fieldList["field"]);
		$this->assign("date",$date);


		//$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrShift = getShiftSet();
		$IDName = $arrShift["IDName"];
		$IDBackgroud = $arrShift["IDBackgroud"];
		$NameBackground = $arrShift["NameBackground"];
		foreach($NameBackground as $key=>$val){
			$arrF[] = array(
				"name"=>$key,
				"color"=>$val,
			);
		}
		$arrL = array("name"=>"<span style='color:red;font-weight:bold;'>请假</span>","color"=>"#FFFFFF");
		$arrL = array("name"=>"<span style='font-weight:bold;'>调班</span>","color"=>"#407fe4");
		$arrL2 = array("name"=>"休息","color"=>"#FFFFFF");
		array_unshift($arrF,$arrL);
		array_unshift($arrF,$arrL2);
		$this->assign("NameBackground",json_encode($arrF));
		//dump($arrF);die;


		$arrWeek = $this->getWeek($date);
		$this->assign("start_time",$arrWeek["this_week_start"]);
		$this->assign("end_time",$arrWeek["this_week_end"]);
		//dump($arrWeek);die;

		$this->display();
	}


	function userGroupBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $val){
			$key = $val[$key_field];
			$ret[$key][] = $val;
		}
		$row = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
		foreach($ret as $key=>$val){
			foreach($ret[$key] as $v){
				$arrWeekID[$v["week_day"]] = $v["shift_id"];
				$arrWeekID["user_name"] = $key;
				$arrWeekID["shift_id"] = $v["shift_id"];

			}
			foreach($row as $vm){
				if(!$arrWeekID[$vm]){
					$arrWeekID[$vm] = "0";
				}
			}
			$arrF[$key] = $arrWeekID;
			unset($arrWeekID);
		}
		//dump($ret[1002]);
		//dump($arrF);die;
		return $arrF;
	}

	function schedulingData(){
		header("Content-Type:text/html; charset=utf-8");
		$arrWeek = $this->getWeek($date);
		$start = $arrWeek["this_week_start"];
		$end = $arrWeek["this_week_end"];

		$scheduling = new Model("pb_scheduling");
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$where = " 1";
		$where .= empty($start_time) ? " AND scheduling_dates >= '$start'" : " AND scheduling_dates >= '$start_time'";
		$where .= empty($end_time) ? " AND scheduling_dates <= '$end'" : " AND scheduling_dates <= '$end_time'";

		$arrData = $scheduling->order("user_name desc")->field("user_name,shift_id,week_day")->where($where)->select();

		//echo $scheduling->getLastSql();
		$arrF3 = $this->userGroupBy($arrData,"user_name");
		//sort($arrF3);
		$arrF = $this->array_sort($arrF3,'user_name','asc');

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrShift = require "BGCC/Conf/crm/$db_name/shiftSet.php";
		$arrShift = getShiftSet();
		$arrF2 = getShiftDefinition();
		$IDName = $arrShift["IDName"];
		$IDName[-1] = "<span style='color:red;font-weight:bold;'>请假</span>";
		$IDName[-2] = "<span style='font-weight:bold;'>调班</span>";
		$IDName[0] = "休息";
		$IDBackgroud = $arrShift["IDBackgroud"];
		$IDBackgroud[-1] = "#FFFFFF";
		$IDBackgroud[-2] = "#407fe4";
		$IDBackgroud[0] = "#FFFFFF";

		foreach($arrF as &$val){
			$val["monday"] = $IDName[$val["monday"]];
			$val["tuesday"] = $IDName[$val["tuesday"]];
			$val["wednesday"] = $IDName[$val["wednesday"]];
			$val["thursday"] = $IDName[$val["thursday"]];
			$val["friday"] = $IDName[$val["friday"]];
			$val["saturday"] = $IDName[$val["saturday"]];
			$val["sunday"] = $IDName[$val["sunday"]];

			$val["shift_background"] = $IDBackgroud[$val["shift_id"]];
		}

		//dump($arrF);die;

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrF);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrData = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrF AS &$v){
			if($i >= $start && $j < $length){
				$arrData[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}


	//生成排班表
	function generationScheduling(){
		set_time_limit(0);
		@ini_set('memory_limit','-1');
		header("Content-Type:text/html; charset=utf-8");
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = getShiftSet();
		$arrF2 = getShiftDefinition();

		$scheduling = new Model("pb_scheduling");
		$date = $_REQUEST["day"];
		//$date=date('Y-m-d');
		$datecount = $scheduling->where("scheduling_dates = '$date'")->count();
		if($datecount){
			echo json_encode(array('msg'=>'该周的排班表已经存在，如要重新生成，请先清除本周的排班表！'));
			die;
		}
		$users = new Model("users");
		$arrU = $users->field("username,d_id,start_working_hours,end_working_hours,job_state,fixed_closed")->where("job_state = 'Y'")->select();

		$WorkingHours = $arrF2["WorkingHours"];
		$arrWork = array_flip($WorkingHours);
		foreach($arrU as &$val){
			$val["working_hours"] = $val["start_working_hours"]."~".$val["end_working_hours"];
			$val["shift_id"] = $arrWork[$val["working_hours"]];
			if(!$val["shift_id"] && !$val["start_working_hours"] && !$val["end_working_hours"]){
				$val["shift_id"] = "all";
			}
			//时间与班次定义不同的也归为all
			if(!$val["shift_id"]){
				$val["shift_id"] = "all";
			}
		}
		//dump($arrU);die;
		$arrT = $this->groupByUser($arrU,"shift_id");
		$arrNum = $arrF["IDWeekNum"];
		$arrNum2 = $arrF["WeekIDNum"];
		//dump($arrNum);die;
		$row = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
		//计算固定上那个班次的坐席有多少个
		foreach($arrT as $key=>$val){
			if($key != "all"){
				$arrTN[$key] = count($arrT[$key]);
			}
		}
		//dump($arrTN);die;

		//取人数
		$arrTALL = $arrT["all"];
		//$arrTALL = array_slice($arrT["all"],0,array_sum($arrNum2["monday"]));  //无效
		//dump($arrTALL);die;
		foreach($arrNum2 as $key=>$val){
			shuffle($arrTALL);
			$n = 0;
			foreach($arrNum2[$key] as $k=>$vm){
				$slice_num = $vm-$arrTN[$k];
				$arrN[$n] = $slice_num;
				if($n == 0){
					$arrNum2[$key][$k] = array_slice($arrTALL,0,$slice_num);
				}else{
					$arrNS = array_slice($arrN,0,$n);
					$totalNum = array_sum($arrNS);
					$arrNum2[$key][$k] = array_slice($arrTALL,$totalNum,$slice_num);
				}
				if($arrTN){
					if($arrT[$k]){
						$arrNum2[$key][$k] = array_merge($arrNum2[$key][$k],$arrT[$k]);
					}
				}

				$n++;
			}
			unset($slice_num);
		}
		//dump($arrNUM);die;

		$arrWeek = $this->getWeek($date);
		$thisWeek = $arrWeek["thisWeek"];
		$week_row = array("monday"=>"1","tuesday"=>"2","wednesday"=>"3","thursday"=>"4","friday"=>"5","saturday"=>"6","sunday"=>"7");

		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];
		$sql = "insert into pb_scheduling(`user_name`,`shift_id`,`scheduling_dates`,`week_day`,`dept_id`) values";
		$value = "";
		foreach($arrNum2 as $key=>&$val){
			foreach($arrNum2[$key] as $k=>$vm){
				foreach($arrNum2[$key][$k] as $v){
					$str = "(";
					$str .= "'" .$v. "',";
					$str .= "'" .$k. "',";
					$str .= "'" .$thisWeek[$week_row[$key]]. "',";
					$str .= "'" .$key. "',";
					$str .= "'" .$deptId_user[$v]. "'";
					$str .= ")";
					$value .= empty($value) ? "$str" : ",$str";
					//$value[] .= empty($value) ? "$str" : ",$str";
				}
			}
		}
		//dump($value);die;

		if( $value ){
			$sql .= $value;
			$result = $users->execute($sql);
		}
		if ($result){
			$start_date = $arrWeek["this_week_start"];
			$end_date = $arrWeek["this_week_end"];
			$this->deleteLeaveScheduling($start_date,$end_date);  //标记请假人员
			echo json_encode(array('success'=>true,'msg'=>'操作成功！'));
		} else {
			echo json_encode(array('msg'=>'操作失败！'));
		}

	}

	//排班表标记请假人员
	function deleteLeaveScheduling($start_date,$end_date){
		//请假的员工
		$attendance = new Model("pb_attendance");
		//$start_date = "2015-03-23";
		//$end_date = "2015-03-29";
		$where = "whether_leave = 'Y' AND create_date >= '$start_date' AND create_date <= '$end_date'";
		$arrLeave = $attendance->field("user_name,create_date,whether_leave")->where($where)->select();
		if($arrLeave){
			$pb_scheduling = new Model("pb_scheduling");
			$users = new Model("users");

			$week_row = array("1"=>"monday","2"=>"tuesday","3"=>"wednesday","4"=>"thursday","5"=>"friday","6"=>"saturday","0"=>"sunday");
			$i = 0;
			foreach($arrLeave as $val){
				$where = "user_name= '".$val["user_name"]."' AND scheduling_dates = '".$val["create_date"]."'";
				$arrSf = $pb_scheduling->where($where)->find();
				$result[] = $pb_scheduling->where($where)->save(array("shift_id"=>"-1"));


				$arrData = $pb_scheduling->field("user_name,scheduling_dates")->where("scheduling_dates = '".$val["create_date"]."'")->select();
				foreach($arrData as $vm){
					$arrU[] = $vm["user_name"];
				}
				$str = "'".implode("','",$arrU)."'";
				$count = $attendance->where("whether_leave = 'Y' AND create_date = '".$val["create_date"]."'")->count();
				$arrF = $users->field("username,en_name")->where("username not in ($str)")->limit($count)->select();
				$w=date('w',strtotime($val["create_date"]));
				foreach($arrF as $v){
					$arrUser[] = $v["username"];


					$arr = array(
						"user_name"=>$v["username"],
						"dept_id"=>"1",
						//"shift_id"=>$arrSf["shift_id"],
						"shift_id"=>"-2",
						"scheduling_dates"=>$val["create_date"],
						"week_day"=>$week_row[$w],
					);
				}
				$i++;
				$pb_scheduling->data($arr)->add();
			}
		}
		//dump($tt);die;
	}


	//生成排班表
	function generationSchedulingBak_20160218(){
		set_time_limit(0);
		@ini_set('memory_limit','-1');
		header("Content-Type:text/html; charset=utf-8");
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = getShiftSet();
		$arrF2 = getShiftDefinition();

		$scheduling = new Model("pb_scheduling");
		$date = $_REQUEST["day"];
		//$date=date('Y-m-d');
		$datecount = $scheduling->where("scheduling_dates = '$date'")->count();
		if($datecount){
			echo json_encode(array('msg'=>'该周的排班表已经存在，如要重新生成，请先清除本周的排班表！'));
			die;
		}
		$users = new Model("users");
		$arrU = $users->field("username,d_id,start_working_hours,end_working_hours,job_state,fixed_closed")->where("job_state = 'Y'")->select();

		$WorkingHours = $arrF2["WorkingHours"];
		$arrWork = array_flip($WorkingHours);
		foreach($arrU as &$val){
			$val["working_hours"] = $val["start_working_hours"]."~".$val["end_working_hours"];
			$val["shift_id"] = $arrWork[$val["working_hours"]];
			if(!$val["shift_id"] && !$val["start_working_hours"] && !$val["end_working_hours"]){
				$val["shift_id"] = "all";
			}
			//时间与班次定义不同的也归为all
			if(!$val["shift_id"]){
				$val["shift_id"] = "all";
			}
		}
		//dump($arrU);die;
		$arrT = $this->groupByUser($arrU,"shift_id");
		$arrTALL = $arrT["all"];
		$arrNum = $arrF["IDWeekNum"];
		//dump($arrNum);die;
		$row = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
		//取人数
		foreach($arrNum as $key=>&$val){
			foreach($row as $v){
				//=====================避免在不同班次在同一天取到相同的工号/避免获取到重复的工号======开始==============
				if($arrNum[$key][$v]){
					if($arrNum[$key][$v]-count($arrT[$key])>0){
						$arrC[$key] = $arrNum[$key][$v]-count($arrT[$key]);
					}else{
						$arrV[$key] = array();
					}
				}
				//shuffle($arrTALL);
				$n = 0;
				foreach($arrC as $km=>$vm){
					$arrN[$n] = $vm;
					if($n == 0){
						$arrV[$km] = array_slice($arrTALL,0,$vm);
					}else{
						$arrNS = array_slice($arrN,0,$n);
						$num = array_sum($arrNS);
						//$arrV[$km] = array_slice($arrT["all"],$arrN[$n-1],$vm);
						$arrV[$km] = array_slice($arrTALL,$num,$vm);
					}
					$n++;

				}
				unset($n);
				//=====================避免在不同班次在同一天取到相同的工号/避免获取到重复的工号======结束==============

				if(!$arrT[$key]){
					$arrT[$key] = array();
				}
				//$arrM = array_merge($arrT[$key],$arrT["all"]);    //获取到重复的工号
				$arrM = array_merge($arrT[$key],$arrV[$key]);		//避免获取到重复的工号
				$arrNum[$key][$v] = array_slice($arrM,0,$arrNum[$key][$v]);
				//$arrNum[$key][$v] = array_slice($arrT[$key],0,$arrNum[$key][$v]);
			}
		}
		//dump($arrS);
		//dump($arrV);
		//dump($arrC);
		//dump($arrN);die;
		//dump($arrNum);die;
		//$date=date('Y-m-d');
		$arrWeek = $this->getWeek($date);
		$thisWeek = $arrWeek["thisWeek"];
		$week_row = array("monday"=>"1","tuesday"=>"2","wednesday"=>"3","thursday"=>"4","friday"=>"5","saturday"=>"6","sunday"=>"7");

		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];
		$sql = "insert into pb_scheduling(`user_name`,`shift_id`,`scheduling_dates`,`week_day`,`dept_id`) values";
		$value = "";
		foreach($arrNum as $key=>&$val){
			foreach($row as $vm){
				foreach($arrNum[$key][$vm] as $v){
					//$tt[] = $arrNum[$key][$vm];
					$str = "(";
					$str .= "'" .$v. "',";
					$str .= "'" .$key. "',";
					$str .= "'" .$thisWeek[$week_row[$vm]]. "',";
					$str .= "'" .$vm. "',";
					$str .= "'" .$deptId_user[$v]. "'";
					$str .= ")";
					$value .= empty($value) ? "$str" : ",$str";
					//$value[] = $str;
				}
			}

		}
		//('8001','1','','Array','1')
		//dump($value);die;

		if( $value ){
			$sql .= $value;
			$result = $users->execute($sql);
		}
		if ($result){
			$start_date = $arrWeek["this_week_start"];
			$end_date = $arrWeek["this_week_end"];
			$this->deleteLeaveScheduling($start_date,$end_date);  //标记请假人员
			echo json_encode(array('success'=>true,'msg'=>'操作成功！'));
		} else {
			echo json_encode(array('msg'=>'操作失败！'));
		}

	}

	//排班表标记请假人员----没有添加请假空缺的位置
	function deleteLeaveSchedulingOld($start_date,$end_date){
		//请假的员工
		$attendance = new Model("pb_attendance");
		$where = "whether_leave = 'Y' AND create_date >= '$start_date' AND create_date <= '$end_date'";
		$arrLeave = $attendance->field("user_name,create_date,whether_leave")->where($where)->select();

		if($arrLeave){
			$pb_scheduling = new Model("pb_scheduling");
			foreach($arrLeave as $val){
				$where = "user_name= '".$val["user_name"]."' AND scheduling_dates = '".$val["create_date"]."'";
				$result[] = $pb_scheduling->where($where)->save(array("shift_id"=>"-1"));
			}
		}
	}

	function editScheduling(){
		checkLogin();
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$user_name = $_REQUEST["user_name"];

		$this->assign("start_time",$start_time);
		$this->assign("end_time",$end_time);
		$this->assign("user_name",$user_name);

		$this->display();
	}

	function updateScheduling(){
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$user_name = $_REQUEST["user_name"];
		$work_day = $_REQUEST["work_day"];
		$shift_id = $_REQUEST["shift_id"];

		$scheduling = new Model("pb_scheduling");
		$arrData = array(
			"shift_id"=>$shift_id
		);
		//dump($work_day);
		//dump($user_name);die;
		$count = count($work_day);
		for($i=0;$i<$count;$i++){
			$result[$i] = $scheduling->data($arrData)->where("user_name = '$user_name' AND week_day = '".$work_day[$i]."' AND scheduling_dates >= '$start_time' AND scheduling_dates <= '$end_time'")->save();
		}
		//echo $scheduling->getLastSql();die;
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	function deleteScheduling(){
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];

		$date = date("Y-m-d");
		$diff = (strtotime($date) - strtotime($end_time))/3600/24;
		if($diff > 0){
			echo json_encode(array('msg'=>'不能删除上礼拜的数据！'));
			die;
		}

		$scheduling = new Model("pb_scheduling");
		$result = $scheduling->where("scheduling_dates >= '$start_time' AND scheduling_dates <= '$end_time'")->delete();

		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function groupByUser($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row["username"];
		}
		foreach($ret as $key=>$val){
			$arrF[$key] = count($ret[$key]);
		}
		return $ret;
	}

	function getTabTitle($date){
		//header("Content-Type:text/html; charset=utf-8");
		$arrF = $this->getWeek($date);
		$arrThisWeeks = $arrF["thisWeek"];
		$row_week = array("1"=>"星期一","2"=>"星期二","3"=>"星期三","4"=>"星期四","5"=>"星期五","6"=>"星期六","7"=>"星期日");
		$row_week2 = array("1"=>"monday","2"=>"tuesday","3"=>"wednesday","4"=>"thursday","5"=>"friday","6"=>"saturday","7"=>"sunday");

		foreach($arrThisWeeks as $key=>&$val){
			$arrField[0][] = array(
				"field" => $row_week2[$key],
				"title" => $row_week[$key]."(".$val.")",
				"width" => "130",
				"align" => "center",
				//"styler" => "setTabColor",
			);
		}
		$arrFd = array("field"=>"user_name","title"=>"工号","width"=>"100");
		array_unshift($arrField[0],$arrFd);
		$arrTitle = json_encode($arrField);
		$result = str_replace('"setTabColor"','setTabColor',$arrTitle);
		//dump($result);die;
		$arrT = array(
			"field"=>$result,
			"arrField"=>$arrField,
		);
		return $arrT;
	}

	function getWeek($date){
		//$date=date('Y-m-d');  //当前日期
		//$date="2015-02-01";  //当前日期
		$first=1; //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
		$w=date('w',strtotime($date));  //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
		$now_start=date('Y-m-d',strtotime("$date -".($w ? $w - $first : 6).' days')); //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
		$now_end=date('Y-m-d',strtotime("$now_start +6 days"));  //本周结束日期

		$last_start=date('Y-m-d',strtotime("$now_start - 7 days"));  //上周开始日期
		$last_end=date('Y-m-d',strtotime("$now_start - 1 days"));  //上周结束日期

		$next_start = date('Y-m-d',strtotime("$now_start + 7 days"));
		$next_end = date('Y-m-d',strtotime("$now_end + 7 days"));

		$arrThisWeek[1] = $now_start;
		for($i=1;$i<=6;$i++){
			$arrThisWeek[$i+1] = date('Y-m-d',strtotime("$now_start +".$i." days"));
		}

		$arrLastWeek[1] = $last_start;
		for($i=1;$i<=6;$i++){
			$arrLastWeek[$i+1] = date('Y-m-d',strtotime("$now_start -".$i." days"));
		}
		sort($arrLastWeek);
		for($i=0;$i<=6;$i++){
			$arrLastWeek2[$i+1] = $arrLastWeek[$i];
		}

		$arrNextWeek[1] = $next_start;
		for($i=1;$i<=6;$i++){
			$arrNextWeek[$i+1] = date('Y-m-d',strtotime("$now_end +".($i+1)." days"));
		}

		$arrData = array(
			"lastWeek" => $arrLastWeek2,
			"thisWeek" => $arrThisWeek,
			"nextWeek" => $arrNextWeek,
			"last_week_start" => $last_start,
			"last_week_end" => $last_end,
			"this_week_start" => $now_start,
			"this_week_end" => $now_end,
			"next_week_start" => $next_start,
			"next_week_end" => $next_end,
		);

		return $arrData;
	}

	function getWeekDate(){
		$date = $_REQUEST["day"];
		$week_day = $_REQUEST["week_day"];
		$first=1; //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
		$w=date('w',strtotime($date));  //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
		$now_start=date('Y-m-d',strtotime("$date -".($w ? $w - $first : 6).' days')); //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
		$now_end=date('Y-m-d',strtotime("$now_start +6 days"));  //本周结束日期

		$last_start=date('Y-m-d',strtotime("$now_start - 7 days"));  //上周开始日期
		$last_end=date('Y-m-d',strtotime("$now_start - 1 days"));  //上周结束日期

		$next_start = date('Y-m-d',strtotime("$now_start + 7 days"));
		$next_end = date('Y-m-d',strtotime("$now_end + 7 days"));

		$arrData = array(
			"last_week_start" => $last_start,
			"last_week_end" => $last_end,
			"this_week_start" => $now_start,
			"this_week_end" => $now_end,
			"next_week_start" => $next_start,
			"next_week_end" => $next_end,
		);

		$date2 = date("Y-m-d");
		$thisWeek = $this->getWeek($date2);

		if($week_day == "nextWeek"){
			$arrF = array(
				"start_time"=>$next_start,
				"end_time"=>$next_end,
			);
		}elseif($week_day == "lastWeek"){
			$arrF = array(
				"start_time"=>$last_start,
				"end_time"=>$last_end,
			);
		}else{
			/*
			$arrF = array(
				"start_time"=>$now_start,
				"end_time"=>$now_end,
			);
			*/
			$arrF = array(
				"start_time"=>$thisWeek["this_week_start"],
				"end_time"=>$thisWeek["this_week_end"],
			);
		}
		$fieldList = $this->getTabTitle($arrF["start_time"]);
		$field = json_decode($fieldList["field"],true);
		$arr = json_encode(array('success'=>true,'msg'=>$arrF,'fieldList'=>$fieldList["arrField"]));

		$result = str_replace('"setTabColor"','setTabColor',$arr);
		echo $arr;
	}

	//话务员考勤管理
	function attendanceList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Attendance Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function attendanceData(){
		$username = $_SESSION['user_info']['username'];
		$attendance = new Model("pb_attendance");

		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$are_late = $_REQUEST["are_late"];
		$leave_early = $_REQUEST["leave_early"];
		$user_name = $_REQUEST["user_name"];
		$date = date("Y-m-d");

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND create_date >= '$start_time'";
		$where .= empty($end_time) ? "  AND create_date <= '$date'" : " AND create_date <= '$end_time'";
		$where .= empty($are_late) ? "" : " AND are_late = '$are_late'";
		$where .= empty($leave_early) ? "" : " AND leave_early = '$leave_early'";
		$where .= empty($user_name) ? "" : " AND user_name = '$user_name'";

		$count = $attendance->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $attendance->order("create_date desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$are_late_row = array('Y'=>'是','N'=>'否','T'=>'漏打或休息');
		$leave_early_row = array('Y'=>'是','N'=>'否');
		$whether_leave_row = array('Y'=>'是','N'=>'否');
		$week_row = array("monday"=>"星期一","tuesday"=>"星期二","wednesday"=>"星期三","thursday"=>"星期四","friday"=>"星期五","saturday"=>"星期六","sunday"=>"星期日");
		$userArr = readU();
		$cnName = $userArr["cn_user"];

		foreach($arrData as &$val){
			$are_late = $are_late_row[$val['are_late']];
			$val['are_late'] = $are_late;

			$leave_early = $leave_early_row[$val['leave_early']];
			$val['leave_early'] = $leave_early;

			$week_day = $week_row[$val['week_day']];
			$val['week_day'] = $week_day;

			$whether_leave = $whether_leave_row[$val['whether_leave']];
			$val['whether_leave'] = $whether_leave;

			$val['cn_name'] = $cnName[$val["user_name"]];
		}


		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertAttendance(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$attendance = new Model("pb_attendance");

		$date = date("Y-m-d");
		$w=date('w',strtotime($date));
		$week_row = array("1"=>"monday","2"=>"tuesday","3"=>"wednesday","4"=>"thursday","5"=>"friday","6"=>"saturday","0"=>"sunday");
		$para_sys = readS();
		$count = strtotime(date("H:i:s")) - strtotime($para_sys["go_to_work_time"]);
		if( $count > 0 ){
			$are_late = "Y";
		}else{
			$are_late = "N";
		}
		$arrF = $attendance->where("user_name = '$username' AND create_date='$date'")->find();
		//dump($arrF);
		if($arrF){
			if($arrF["start_time"]){
				echo json_encode(array('msg'=>'您今天已经登记过了，无需再次登记！'));
				die;
			}
			$arrData = Array(
				'start_time'=>date("H:i:s"),
				'are_late'=>$are_late,
			);
			$result = $attendance->data($arrData)->where("user_name = '$username' AND create_date='$date'")->save();
			if ($result){
				echo json_encode(array('success'=>true,'msg'=>'操作成功！'));
			} else {
				echo json_encode(array('msg'=>'操作失败！'));
			}
		}else{
			$arrData = Array(
				'user_name'=>$username,
				'dept_id'=>$d_id,
				'create_date'=>$date,
				'start_time'=>date("H:i:s"),
				'are_late'=>$are_late,
				'week_day'=>$week_row[$w],
			);
			$result = $attendance->data($arrData)->add();
			if ($result){
				echo json_encode(array('success'=>true,'msg'=>'操作成功！'));
			} else {
				echo json_encode(array('msg'=>'操作失败！'));
			}
		}
	}

	function updateAttendance(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$attendance = new Model("pb_attendance");


		$date = date("Y-m-d");
		$work_count = $attendance->where("user_name = '$username' AND create_date='$date'")->count();
		$w=date('w',strtotime($date));
		$week_row = array("1"=>"monday","2"=>"tuesday","3"=>"wednesday","4"=>"thursday","5"=>"friday","6"=>"saturday","0"=>"sunday");
		//dump($work_count);die;
		$para_sys = readS();
		$count = strtotime(date("H:i:s")) - strtotime($para_sys["go_off_work_time"]);
		if( $count > 0 ){
			$leave_early = "N";
		}else{
			$leave_early = "Y";
		}
		if($work_count > 0){
			$arrData = Array(
				'end_time'=>date("H:i:s"),
				'leave_early'=>$leave_early,
			);
			$result = $attendance->data($arrData)->where("user_name = '$username' AND create_date='$date'")->save();
			if ($result !== false){
				echo json_encode(array('success'=>true,'msg'=>'操作成功！'));
			} else {
				echo json_encode(array('msg'=>'操作失败！'));
			}
		}else{
			$arrData = Array(
				'user_name'=>$username,
				'dept_id'=>$d_id,
				'create_date'=>$date,
				'end_time'=>date("H:i:s"),
				'are_late'=>"T",
				'leave_early'=>$leave_early,
				'week_day'=>$week_row[$w],
			);
			$result = $attendance->data($arrData)->add();
			if ($result){
				echo json_encode(array('success'=>true,'msg'=>'操作成功！'));
			} else {
				echo json_encode(array('msg'=>'操作失败！'));
			}
		}
	}

	//请假管理
	function leaveList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Leave Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$web_type = $_REQUEST["web_type"];
		if($web_type == "agent"){
			$this->assign("web_type","agent");
		}else{
			$this->assign("web_type","back");
		}

		$this->display();
	}

	function leaveData(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$leave_table = new Model("pb_leave_table");

		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$web_type = $_REQUEST["web_type"];
		$where = "1 ";
		if($web_type == "agent"){
			if($username != "admin"){
				$where .= " AND leave_of_absence = '$username'";
			}
		}else{
			if($username != "admin"){
				$where .= " AND dept_id in ($deptSet)";
			}
		}

		$count = $leave_table->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $leave_table->order("createtime desc")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $leave_table->getLastSql();die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addLeave(){
		checkLogin();
		$this->display();
	}

	function insertLeave(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$leave_table = new Model("pb_leave_table");
		//$leave_day = (strtotime($_REQUEST['end_leave_time']) - strtotime($_REQUEST['start_leave_time']) )/3600/24;
		$arrData = Array(
			'createtime'=>date("Y-m-d H:i:s"),
			'leave_of_absence'=>$username,
			'dept_id'=>$d_id,
			'start_leave_time'=>$_REQUEST['start_leave_time'],
			'end_leave_time'=>$_REQUEST['end_leave_time'],
			'leave_type'=>$_REQUEST['leave_type'],
			'reason_for_leave'=>$_REQUEST['reason_for_leave'],
			//'leave_day'=>$_REQUEST['leave_day'],
		);
		//dump($leave_day);die;
		$result = $leave_table->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function editLeave(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST["id"];
		$leave_table = new Model("pb_leave_table");
		$arrData = $leave_table->where("id = '$id'")->find();

		$this->assign("id",$id);
		$this->assign("arrData",$arrData);
		if($username == $arrData["leave_of_absence"]){
			$this->assign("update_user","self");   //请假人自己执行编辑操作
		}else{
			$this->assign("update_user","other");   //批假人执行编辑操作
		}

		$this->display();
	}

	function updateLeave(){
		$id = $_REQUEST["id"];
		$update_user = $_REQUEST["update_user"];
		$username = $_SESSION['user_info']['username'];

		$leave_table = new Model("pb_leave_table");
		if($update_user == "self"){
			$arrData = Array(
				'start_leave_time'=>$_REQUEST['start_leave_time'],
				'end_leave_time'=>$_REQUEST['end_leave_time'],
				'leave_type'=>$_REQUEST['leave_type'],
				'reason_for_leave'=>$_REQUEST['reason_for_leave'],
			);
		}else{
			$arrData = Array(
				'leave_day'=>$_REQUEST['leave_day'],
				'leave_results'=>$_REQUEST['leave_results'],
				'no_leave_reason'=>$_REQUEST['no_leave_reason'],
				'grant_leave'=>date("Y-m-d H:i:s"),
				'batch_dummies'=>$username,
				'whether_approval'=>"Y",
			);
		}
		//dump($_REQUEST);die;
		$result = $leave_table->data($arrData)->where("id='$id'")->save();
		if ($result !== false){
			//同意请假
			if($_REQUEST['leave_results'] == "Y" && $update_user != "self"){
				$this->leaveOperating($_REQUEST['leave_of_absence'],$_REQUEST['start_leave_time'],$_REQUEST['end_leave_time'],$id);
			}
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function leaveOperating($leave_of_absence,$start_leave_time,$end_leave_time,$id){
		/*
		$leave_of_absence = "8001";
		$start_leave_time = "2015-03-18 09:06:50";
		$end_leave_time = "2015-03-25 11:06:50";
		//$leave_day = "3";
		*/
		$leave_day = ceil((strtotime($end_leave_time) - strtotime($start_leave_time) )/3600/24);

		//date("Y-m-d",strtotime("+1 day"))
		for($i=0;$i<$leave_day;$i++){
			$arrDate[] = date('Y-m-d',strtotime("$start_leave_time +".$i." days"));
		}

		$row_week = array("1"=>"monday","2"=>"tuesday","3"=>"wednesday","4"=>"thursday","5"=>"friday","6"=>"saturday","0"=>"sunday");
		foreach($arrDate as $val){
			$arrF[] = array(
				"create_date"=>$val,
				"week_day"=>$row_week[date('w',strtotime($val))],
			);
		}

		//请假时去掉周六、周日的日期
		$arrData = array();
		foreach($arrF as $key=>$val){
			if($val["week_day"] != "saturday" && $val["week_day"] != "sunday"){
				$arrData[] = $val;
				$arrDt[] = $val["create_date"];
			}
		}
		//dump($arrData);die;
		$attendance = new Model("pb_attendance");
		$start_time = date("Y-m-d",strtotime($start_leave_time));
		$end_time = date("Y-m-d",strtotime($end_leave_time));
		$del = $attendance->where("user_name = '$leave_of_absence' AND create_date >= '$start_time' AND create_date <= '$end_time' AND whether_leave='Y'")->delete();
		//echo $attendance->getLastSql();die;
		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];
		$sql = "insert into pb_attendance(`user_name`,`dept_id`,`create_date`,`week_day`,`whether_leave`) values";
		$value = "";
		foreach($arrData as $val){
			$str = "(";
			$str .= "'" .$leave_of_absence. "',";
			$str .= "'" .$deptId_user[$leave_of_absence]. "',";
			$str .= "'" .$val['create_date']. "',";
			$str .= "'" .$val['week_day']. "',";
			$str .= "'Y'";
			$str .= ")";
			$value .= empty($value) ? "$str" : ",$str";
		}
		if( $value ){
			$sql .= $value;
			$result = $attendance->execute($sql);

			$leave_table = new Model("pb_leave_table");
			$res = $leave_table->where("id='$id'")->save(array("leave_dates"=>json_encode($arrDt)));
		}
	}


    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}

	//节假日设定
	function holidayList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Holidays Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function holidayData(){
		$username = $_SESSION['user_info']['username'];
		$pb_holidays = new Model("pb_holidays");

		//$fields = "h.id,h.create_time,h.create_user,h.dept_id,h.holiday_name,h.holiday_description,hd.holiday_date";
		$count = $pb_holidays->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $pb_holidays->limit($page->firstRow.','.$page->listRows)->select();
		foreach($arrData as &$val){
			$val["holiday_date"] = implode("<br >",json_decode($val["holiday_dates"],true));
			$val["view"] = "聚焦查看";
		}

		//dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function groupByDate($arr, $key_field, $value_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row;
		}
		foreach($ret as $key=>$val){
			$i = 0;
			foreach($ret[$key] as $v){
				$arrWeekNum[$i] = $v[$value_field];
				$i++;
			}
			$arrF[$key] = $arrWeekNum;
		}
		//dump($arrF);die;
		return $arrF;
	}

	function addHoliday(){
		checkLogin();

		$this->display();
	}

	function insertHoliday(){
		$username = $_SESSION['user_info']['username'];
		$dept_id = $_SESSION['user_info']['d_id'];
		$pb_holidays = new Model("pb_holidays");

		$holiday_date = $_REQUEST['holiday_date'];
		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$dept_id,
			'holiday_name'=>$_REQUEST['holiday_name'],
			'holiday_description'=>$_REQUEST['holiday_description'],
			'holiday_dates'=>json_encode($holiday_date),
		);
		//dump($holiday_date);die;
		$result = $pb_holidays->data($arrData)->add();
		if ($result){
			$sql = "insert into pb_holiday_date(`holiday_id`,`holiday_date`) values";
			$value = "";
			foreach($holiday_date as $val){
				$str = "('".$result."','".$val."')";
				$value .= empty($value) ? "$str" : ",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $pb_holidays->execute($sql);
			}
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}


	function editHoliday(){
		checkLogin();
		$id = $_REQUEST["id"];
		$pb_holidays = new Model("pb_holidays");
		$arrData = $pb_holidays->where("id = '$id'")->find();
		$this->assign("arrData",$arrData);
		$this->assign("id",$id);

		$pb_holiday_date = new Model("pb_holiday_date");
		$arrF = $pb_holiday_date->order("holiday_date desc")->where("holiday_id = '$id'")->select();
		$i = 0;
		foreach($arrF as &$val){
			$val["order"] = $i;
			$i++;
		}
		unset($i);
		$max = count($arrF)+1;
		$this->assign("arrF",$arrF);
		$this->assign("max",$max);

		$this->display();
	}

	function updateHoliday(){
		$id = $_REQUEST["id"];
		$pb_holidays = new Model("pb_holidays");

		$holiday_date = $_REQUEST['holiday_date'];
		$arrData = Array(
			'holiday_name'=>$_REQUEST['holiday_name'],
			'holiday_description'=>$_REQUEST['holiday_description'],
			'holiday_dates'=>json_encode($holiday_date),
		);
		//dump($holiday_date);die;
		$result = $pb_holidays->data($arrData)->where("id='$id'")->save();
		if ($result !== false){
			$pb_holiday_date = new Model("pb_holiday_date");
			$del = $pb_holiday_date->where("holiday_id = '$id'")->delete();
			$sql = "insert into pb_holiday_date(`holiday_id`,`holiday_date`) values";
			$value = "";
			foreach($holiday_date as $val){
				$str = "('".$id."','".$val."')";
				$value .= empty($value) ? "$str" : ",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $pb_holidays->execute($sql);
			}
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteHoliday(){
		$id = $_REQUEST["id"];
		$pb_holidays = new Model("pb_holidays");
		$result = $pb_holidays->where("id in ($id)")->delete();
		if ($result){
			$pb_holiday_date = new Model("pb_holiday_date");
			$del = $pb_holiday_date->where("holiday_id = '$id'")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//突发事件管理
	function eventList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Incident Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function eventData(){
		$username = $_SESSION['user_info']['username'];
		$pb_event = new Model("pb_event");
		$count = $pb_event->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $pb_event->order("create_time desc,event_date desc")->limit($page->firstRow.','.$page->listRows)->select();
		foreach($arrData as &$val){
			$val["event_time"] = $val["event_start_time"]."~".$val["event_end_time"];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertEvent(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$pb_event = new Model("pb_event");

		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'event_name'=>$_REQUEST['event_name'],
			'event_date'=>$_REQUEST['event_date'],
			'event_start_time'=>$_REQUEST['event_start_time'],
			'event_end_time'=>$_REQUEST['event_end_time'],
			'traffic_rate'=>$_REQUEST['traffic_rate'],
			'traffic_abs'=>$_REQUEST['traffic_abs'],
		);
		$result = $pb_event->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateEvent(){
		$id = $_REQUEST['id'];
		$pb_event = new Model("pb_event");
		$arrData = Array(
			'event_name'=>$_REQUEST['event_name'],
			'event_date'=>$_REQUEST['event_date'],
			'event_start_time'=>$_REQUEST['event_start_time'],
			'event_end_time'=>$_REQUEST['event_end_time'],
			'traffic_rate'=>$_REQUEST['traffic_rate'],
			'traffic_abs'=>$_REQUEST['traffic_abs'],
		);
		$result = $pb_event->data($arrData)->where("id='$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteEvent(){
		$id = $_REQUEST["id"];
		$pb_event = new Model("pb_event");
		$result = $pb_event->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

}
?>
