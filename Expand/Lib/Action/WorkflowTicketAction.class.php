<?php
/*
工作流工单
*/
class WorkflowTicketAction extends Action{
	//工作列表
	function workList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Workflow Ticket";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function workData(){
		$username = $_SESSION['user_info']['username'];
		$work_workflow_ticket = new Model("work_workflow_ticket");

		$fields = "w.id,w.create_time,w.applicant,w.working_name,w.form_design_id,w.form_id,w.process_design_id,w.process_node_id,w.receiving_time,w.process_node_list_id,w.first_step,w.user_name,w.work_status,w.work_result,p.process_name,n.step_name";

		$where = "1 ";
		if($username != "admin"){
			$where .= " AND (w.user_name like '%$username%' OR w.applicant = '$username')";
		}

		$count = $work_workflow_ticket->table("work_workflow_ticket w")->field($fields)->join("work_process_design p on (w.process_design_id = p.id)")->join("work_process_design_node n on (w.process_node_list_id = n.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $work_workflow_ticket->order("w.create_time desc")->table("work_workflow_ticket w")->field($fields)->join("work_process_design p on (w.process_design_id = p.id)")->join("work_process_design_node n on (w.process_node_list_id = n.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $work_workflow_ticket->getLastSql();die;

		$row_work_result = array("Y"=>"审核通过","N"=>"审核不通过","T"=>"未知[办理中]");
		foreach($arrData as &$val){
			$work_result = $row_work_result[$val["work_result"]];
			$val["work_result"] = $work_result;

			if($val["first_step"] == "Y"){
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"openFirstTrans("."'".$val["id"]."','".$val["process_design_id"]."','".$val["form_design_id"]."','".$val["process_node_id"]."'".")\" >转交</a>";
			}else{
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"openTrans("."'".$val["id"]."','".$val["process_design_id"]."','".$val["form_design_id"]."','".$val["process_node_id"]."'".")\" >转交</a>";
			}
		}


		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	//获取流程分类下的流程
	function getProcess(){
		//header("Content-Type:text/html; charset=utf-8");
		$work_process_category=new Model("work_process_category");
		$arrData = $work_process_category->field("category_name as text,id as tid,category_pid as pid")->select();

		$work_process_design = new Model("work_process_design");
		$arrF = $work_process_design->order("process_sort asc")->field("id,process_name as text,process_category_id as pid")->select();

		$j = 0;
		foreach($arrData as $vm){
			$arrData[$j]['attributes'] = "pid";
			$arrData[$j]['iconCls'] = "icon-files";
			$arrData[$j]['state'] = "closed";
			$j++;
		}

		foreach($arrF as &$val){
			$val["attributes"] = "son_id";
			$arr[$val["pid"]][] = $val;
			array_push($arrData,$val);
		}

        //$arrTree = $this->getTree($arrData,0);
        $arrTree = $this->getTree($arrData,0,"pid","tid");


		$arr = array("id"=>"","text"=>"请选择...");
		array_unshift($arrTree,$arr);
		//dump($arrTree);die;
		echo  json_encode($arrTree);
	}


	function getTree($data, $pId,$field_pid,$field_id) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v[$field_pid] == $pId)    {
				$v['children'] = $this->getTree($data, $v[$field_id],$field_pid,$field_id);

				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				/*
				if ( empty($v["children"]) && $v['state'] =='closed'){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}
				if ( $v["children"]  && $v['state'] =='closed'){
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				*/
				if ( empty($v["children"])){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}else{
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }

	function addWork(){
		checkLogin();
		$time = date("Y-m-d H:i:s");
		$this->assign("time",$time);

		$username = $_SESSION['user_info']['username'];
		$this->assign("username",$username);

		$this->display();
	}

	function addForm(){
		checkLogin();

		$d_id = $_SESSION['user_info']['d_id'];
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = readU();
		$arrDeptName = $arrF["deptId_name"];
		$arrDeptName[0] = "所有部门";
		$dept_name = $arrDeptName[$d_id];

		$this->assign("d_id",$d_id);
		$this->assign("dept_name",$dept_name);

		$username = $_SESSION['user_info']['username'];
		$this->assign("username",$username);


		$node = $_REQUEST["node"];
		$working_name = $_REQUEST["working_name"];
		$process_design_id = $_REQUEST["process_design_id"];

		$work_process_design = new Model("work_process_design");
		$arrData = $work_process_design->where("id = '$process_design_id'")->find();

		$form_id = $arrData["form_design_id"];
		$table_name = "work_form_$form_id";

		//$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrFields = require "BGCC/Conf/crm/$db_name/order_fieldCache.php";
		$arrFields = getFormField();
		$cmFields = $arrFields[$table_name];
		//dump($cmFields);die;
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		//$formField = require "BGCC/Conf/crm/$db_name/customerField.php";
		$formField = getCustomerField();
		$node_num = $arrData["id"]."_".$arrData["first_node_id"]."_".$arrData["first_node_order_num"];    //流程id_流程节点id_节点序号
		$arrField = $formField[$node_num];
		//dump($arrField);die;

		foreach($fielddTpl2 as $val){
			if($val['field_enabled'] == 'Y'){
				if( in_array($val["id"],$arrField)){
					$fielddTpl[] = $val;
				}else{
					$fieldHidden[] = $val;
				}
			}
		}
		$this->assign("fielddTpl",$fielddTpl);
		$this->assign("fieldHidden",$fieldHidden);


		$this->assign("arrData",$arrData);
		$this->assign("working_name",$working_name);


		$this->display();
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	function insertForm(){
		$form_design_id = $_REQUEST["form_design_id"];
		$process_design_id = $_REQUEST["process_design_id"];
		$process_node_id = $_REQUEST["process_node_id"];
		$first_node_order_num = $_REQUEST["first_node_order_num"];

		$table_name = "work_form_".$form_design_id;
		$work_form = new Model($table_name);


		//$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrFields = require "BGCC/Conf/crm/$db_name/order_fieldCache.php";
		$arrFields = getFormField();
		$cmFields = $arrFields[$table_name];
		//dump($cmFields);die;
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		//$formField = require "BGCC/Conf/crm/$db_name/customerField.php";
		$formField = getCustomerField();
		$node_num = $process_design_id."_".$process_node_id."_".$first_node_order_num;    //流程id_流程节点id_节点序号
		$arrField = $formField[$node_num];
		//dump($arrField);die;

		foreach($fielddTpl2 as $val){
			if($val['field_enabled'] == 'Y'){
				if( in_array($val["id"],$arrField)){
					$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
				}else{
					$fieldHidden[] = $val;
				}
			}
		}

		$arrData["create_time"] = date("Y-m-d H:i:s");
		$arrData["process_design_id"] = $process_design_id;
		$arrData["process_node_id"] = $process_node_id;

		//dump($arrData);die;

		$result = $work_form->data($arrData)->add();
		if ($result){
			$arrW = array(
				"create_time"=>date("Y-m-d H:i:s"),
				"receiving_time"=>date("Y-m-d H:i:s"),
				"applicant"=>$_REQUEST["create_user"],
				"working_name"=>$_REQUEST["working_name"],
				"form_design_id"=>$form_design_id,
				"form_id"=>$result,
				"process_design_id"=>$process_design_id,
				"process_node_id"=>$process_node_id,
				"process_node_list_id"=>$process_node_id
			);
			$this->insertWork($arrW);
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function insertWork($arrData){
		$username = $_SESSION['user_info']['username'];
		$work_workflow_ticket = new Model("work_workflow_ticket");

		$arrData["work_status"] = "办理中";
		$result = $work_workflow_ticket->data($arrData)->add();
		if ($result){
			$work_workflow_ticket_none = new Model("work_workflow_ticket_none");
			$arrF = array(
				"work_workflow_id"=>$result,
				"create_user"=>$username,
				"create_time"=>date("Y-m-d H:i:s"),
				"process_node_ids"=>$arrData["process_node_id"],
				"process_status"=>"办理中",
			);
			$res = $work_workflow_ticket_none->data($arrF)->add();
			return $result;
		} else {
			return false;
		}
	}

	function insertFileForm(){
		$form_design_id = $_REQUEST["form_design_id"];
		$process_design_id = $_REQUEST["process_design_id"];
		$process_node_id = $_REQUEST["process_node_id"];
		$first_node_order_num = $_REQUEST["first_node_order_num"];

		$table_name = "work_form_".$form_design_id;
		$work_form = new Model($table_name);


		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrFields = require "BGCC/Conf/crm/$db_name/order_fieldCache.php";
		$arrFields = getFormField();
		$cmFields = $arrFields[$table_name];
		//dump($cmFields);die;
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		//$formField = require "BGCC/Conf/crm/$db_name/customerField.php";
		$formField = getCustomerField();
		$node_num = $process_design_id."_".$process_node_id."_".$first_node_order_num;    //流程id_流程节点id_节点序号
		$arrField = $formField[$node_num];
		//dump($arrField);die;

		foreach($fielddTpl2 as $val){
			if($val['field_enabled'] == 'Y'){
				if( in_array($val["id"],$arrField)){
					$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
				}else{
					$fieldHidden[] = $val;
				}
			}
		}

		$arrData["create_time"] = date("Y-m-d H:i:s");
		$arrData["process_design_id"] = $process_design_id;
		$arrData["process_node_id"] = $process_node_id;

		$username = $_SESSION['user_info']['username'];

		$file_path = "include/data/workflowTicket/$table_name/";
		$this->mkdirs($file_path);

		//判断文件类型
		$tmp_file = $_FILES["filename"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["filename"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));
		$arrExt = array("php","sql");
		if( in_array($suffix,$arrExt)){
			echo json_encode(array('msg'=>"上传文件类型不允许！"));
			die;
		}

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$upload->savePath= $file_path;  //上传路径
		$upload->saveRule="uniqid";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖


		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){


				$result = $work_form->data($arrData)->add();
				if ($result){
					$arrW = array(
						"create_time"=>date("Y-m-d H:i:s"),
						"receiving_time"=>date("Y-m-d H:i:s"),
						"applicant"=>$_REQUEST["create_user"],
						"working_name"=>$_REQUEST["working_name"],
						"form_design_id"=>$form_design_id,
						"form_id"=>$result,
						"process_design_id"=>$process_design_id,
						"process_node_id"=>$process_node_id,
						"process_node_list_id"=>$process_node_id
					);
					$workflow_id = $this->insertWork($arrW);
					$arrMsg = array(
						"workflow_id"=>$workflow_id,
						"process_design_id"=>$process_design_id,
						"form_design_id"=>$form_design_id,
						"process_node_id"=>$process_node_id,
					);
					echo json_encode(array('success'=>true,'msg'=>'添加成功！','arrMsg'=>$arrMsg));
				} else {
					echo json_encode(array('msg'=>'添加失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData["file_attachment"] = $info[0]["savepath"]."/".$info[0]["savename"];
			$arrData["native_name"] = $info[0]["name"];

			$result = $work_form->data($arrData)->add();
			if ($result){
				$arrW = array(
					"create_time"=>date("Y-m-d H:i:s"),
					"receiving_time"=>date("Y-m-d H:i:s"),
					"applicant"=>$_REQUEST["create_user"],
					"working_name"=>$_REQUEST["working_name"],
					"form_design_id"=>$form_design_id,
					"form_id"=>$result,
					"process_design_id"=>$process_design_id,
					"process_node_id"=>$process_node_id,
					"process_node_list_id"=>$process_node_id
				);
				$workflow_id = $this->insertWork($arrW);
				$arrMsg = array(
					"workflow_id"=>$workflow_id,
					"process_design_id"=>$process_design_id,
					"form_design_id"=>$form_design_id,
					"process_node_id"=>$process_node_id,
				);
				echo json_encode(array('success'=>true,'msg'=>'添加成功！','arrMsg'=>$arrMsg));
			} else {
				echo json_encode(array('msg'=>'添加失败！'));
			}
		}

	}


	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}

	function editWork(){
		checkLogin();
		$id = $_REQUEST["id"];
		$work_workflow_ticket = new Model("work_workflow_ticket");
		$arrData = $work_workflow_ticket->where("id = '$id'")->find();

		$this->assign("arrData",$arrData);
		$this->assign("id",$id);

		$this->display();
	}

	function updateWork(){
		$id = $_REQUEST['id'];
		$work_workflow_ticket = new Model("work_workflow_ticket");
		$arrData = array(
			'process_design_id'=>$_REQUEST['process_design_id'],
			'working_name'=>$_REQUEST['working_name']
		);
		$result = $work_workflow_ticket->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}



	function editForm(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		checkLogin();
		$workflow_id = $_REQUEST['workflow_id'];
		$work_workflow_ticket = new Model("work_workflow_ticket");
		$arrData = $work_workflow_ticket->where("id = '$workflow_id'")->find();
		$arrU = $work_workflow_ticket->where("id = '$workflow_id' AND user_name like '%|$username|%'")->count();
		if($arrU > 0 && $arrData["work_result"] == "T"){
			$this->assign("buttom","Y");
		}else{
			$this->assign("buttom","N");
		}
		//dump($arrData);die;
		$this->assign("arrData",$arrData);

		$work_process_design_node = new Model("work_process_design_node");
		$arrNowNode = $work_process_design_node->where("id = '".$arrData["process_node_id"]."'")->find();
		$arrN = $work_process_design_node->where("process_design_id = '".$arrData["process_design_id"]."' AND order_num = '".$arrNowNode["next_step_num"]."'")->find();
		if($arrN){
			$this->assign("arrN",$arrN);
		}else{
			$this->assign("arrN",$arrNowNode);
		}
		//echo $work_process_design_node->getLastSql();
		//dump($arrN);die;


		$form_id = $arrData["form_design_id"];
		$table_name = "work_form_$form_id";

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrFields = require "BGCC/Conf/crm/$db_name/order_fieldCache.php";
		$arrFields = getFormField();
		$cmFields = $arrFields[$table_name];
		//dump($cmFields);die;
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		//$formField = require "BGCC/Conf/crm/$db_name/customerField.php";
		$formField = getCustomerField();
		$node_num = $arrData["process_design_id"]."_".$arrN["id"]."_".$arrN["order_num"];    //流程id_流程节点id_当前节点序号
		$arrField = $formField[$node_num];
		//dump($node_num);die;

		foreach($fielddTpl2 as $val){
			if($val['field_enabled'] == 'Y'){
				$fielddTpl[] = $val;
				if( !in_array($val["id"],$arrField)){
					$fieldHidden[] = $val;
				}else{
					//工号字段默认当前工号
					if($val["user_enabled"] == "Y"){
						$arrDU = $val["en_name"];
					}
				}
			}
		}
		$this->assign("fielddTpl",$fielddTpl);
		$this->assign("fieldHidden",$fieldHidden);

		//dump($arrDU);die;

		$work_form = new Model($table_name);
		$arrWD = $work_form->where("id = '".$arrData["form_id"]."'")->find();

		$this->assign("arrWD",$arrWD);
		$this->assign("username",$username);
		$this->assign("arrDU",$arrDU);

		//dump($arrWD);die;

		$this->display();
	}

	function updateFileForm(){
		$form_design_id = $_REQUEST["form_design_id"];
		$process_design_id = $_REQUEST["process_design_id"];
		$process_node_id = $_REQUEST["process_node_id"];
		$node_order_num = $_REQUEST["node_order_num"];
		$form_id = $_REQUEST["form_id"];

		$table_name = "work_form_".$form_design_id;
		$work_form = new Model($table_name);


		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrFields = require "BGCC/Conf/crm/$db_name/order_fieldCache.php";
		$arrFields = getFormField();
		$cmFields = $arrFields[$table_name];
		//dump($cmFields);die;
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		//$formField = require "BGCC/Conf/crm/$db_name/customerField.php";
		$formField = getCustomerField();
		$node_num = $process_design_id."_".$process_node_id."_".$node_order_num;    //流程id_流程节点id_当前节点序号
		$arrField = $formField[$node_num];
		//dump($arrField);die;

		foreach($fielddTpl2 as $val){
			if($val['field_enabled'] == 'Y'){
				if( in_array($val["id"],$arrField)){
					$arrData[$val['en_name']] = $_REQUEST[$val['en_name']];
				}else{
					$fieldHidden[] = $val;
				}
			}
		}

		//$arrData["process_design_id"] = $process_design_id;
		//$arrData["process_node_id"] = $process_node_id;

		$username = $_SESSION['user_info']['username'];

		$file_path = "include/data/workflowTicket/$table_name/";
		$this->mkdirs($file_path);

		//判断文件类型
		$tmp_file = $_FILES["filename"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["filename"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));
		$arrExt = array("php","sql");
		if( in_array($suffix,$arrExt)){
			echo json_encode(array('msg'=>"上传文件类型不允许！"));
			die;
		}

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$upload->savePath= $file_path;  //上传路径
		$upload->saveRule="uniqid";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖


		$workflow_id = $_REQUEST["workflow_id"];
		$work_result = $_REQUEST["work_result"];
		if($work_result == "N"){
			//不同意操作
			$work_workflow_ticket = new Model("work_workflow_ticket");
			$arrW = $work_workflow_ticket->where("id = '$workflow_id'")->find();
			$push_user = $arrW["applicant"];
			$arr = array(
						"work_result"=>"N",
						"work_status"=>"已办结",
						"user_name"=>"|".$push_user."|"
					);
			$work_workflow_ticket->data($arr)->where("id = '$workflow_id'")->save();
		}else{
			$push_user = "";
		}
		//dump($work_result);die;

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){


				$result = $work_form->data($arrData)->where("id = '$form_id'")->save();
				if ($result !== false){
					echo json_encode(array('success'=>true,'msg'=>'更新成功！','user_name'=>$push_user,'workflow_id'=>$workflow_id,'working_name'=>$arrW["working_name"]));
				} else {
					echo json_encode(array('msg'=>'更新失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData["file_attachment"] = $info[0]["savepath"]."/".$info[0]["savename"];
			$arrData["native_name"] = $info[0]["name"];

			$result = $work_form->data($arrData)->where("id = '$form_id'")->save();
			if ($result !== false){
				echo json_encode(array('success'=>true,'msg'=>'更新成功！','user_name'=>$push_user,'workflow_id'=>$workflow_id,'working_name'=>$arrW["working_name"]));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}

	}


	function transferNext(){
		header("Content-Type:text/html; charset=utf-8");
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$workflow_id = $_REQUEST["workflow_id"];
		$process_design_id = $_REQUEST["process_design_id"];
		$form_design_id = $_REQUEST["form_design_id"];
		$process_node_id = $_REQUEST["process_node_id"];
		$node_order_num = $_REQUEST["node_order_num"];
		$form_id = $_REQUEST["form_id"];

		$this->assign("workflow_id",$workflow_id);
		$this->assign("process_design_id",$process_design_id);
		$this->assign("form_design_id",$form_design_id);
		$this->assign("process_node_id",$process_node_id);
		$this->assign("node_order_num",$node_order_num);
		$this->assign("form_id",$form_id);


		$work_workflow_ticket = new Model("work_workflow_ticket");
		$arrData = $work_workflow_ticket->where("id = '$workflow_id'")->find();
		$this->assign("arrData",$arrData);

		$work_process_design_node = new Model("work_process_design_node");
		//当前步骤
		$arrNP = $work_process_design_node->where("id = '$process_node_id'")->find();
		//echo $work_process_design_node->getLastSql();
		$this->assign("arrNP",$arrNP);
		$this->assign("username",$username);

		//下一步骤
		$arrNext = $work_process_design_node->where("process_design_id = '$process_design_id' AND order_num = '".$arrNP["next_step_num"]."'")->find();
		//echo $work_process_design_node->getLastSql();
		$this->assign("arrNext",$arrNext);
		//dump($arrNP);
		//dump($arrNext);die;
		if($arrNext){
			$arrUser = json_decode($arrNext["authorized_work_num"],true);
			$userArr = readU();
			$cnName = $userArr["cn_user"];
			foreach($arrUser as &$val){
				$arrU2[] = array(
					"username"=>$val,
					"cn_name"=>$val."/".$cnName[$val],
				);
			}
			$arrU = $this->array_sort($arrU2,'username','asc','no');
		}else{
			$arrU[0] =  array(
				"username"=>$arrData["applicant"],
				"cn_name"=>$arrData["applicant"]."/".$cnName[$arrData["applicant"]],
			);
		}
		$this->assign("arrUser",$arrU);
		//dump($arrU);die;
		$this->display();
	}


	function saveTransferNext(){
		$username = $_SESSION['user_info']['username'];
		$workflow_id = $_REQUEST["workflow_id"];
		$process_design_id = $_REQUEST["process_design_id"];
		$process_node_id = $_REQUEST["process_node_id"];
		$next_node_id = $_REQUEST["next_node_id"];
		$arrU = $_REQUEST["user"];
		$user_name = json_encode($_REQUEST["user"]);

		$work_process_design_node = new Model("work_process_design_node");
		//下一步骤
		$arrF = $work_process_design_node->where("process_design_id = '$process_design_id' AND id = '$next_node_id'")->find();
		$next_order_num = $arrF["order_num"];

		$arrPdn = $work_process_design_node->order("id asc")->where("process_design_id = '$process_design_id'")->select();
		foreach($arrPdn as $key=>$val){
			$arrN[$val["order_num"]] = $val["next_step_num"];
		}

		//dump($next_order_num);
		//dump($process_node_id);
		//dump($next_node_id);
		//dump($arrN);die;

		if($arrN[$next_order_num] || $arrN[$next_order_num] == "0"){
			$process_status = "办理中";
			$work_result = "T";
		}else{
			$process_status = "已办结";
			$work_result = "Y";
		}

		$work_workflow_ticket_none = new Model("work_workflow_ticket_none");
		$arrData = array(
			"create_time"=>date("Y-m-d H:i:s"),
			"work_workflow_id"=>$workflow_id,
			"create_user"=>$username,
			"process_node_ids"=>$process_node_id,
			"process_status"=>$process_status,
			"user_name"=>$user_name
		);
		//dump($arrData);
		//dump($work_result);die;
		$result = $work_workflow_ticket_none->data($arrData)->add();
		if ($result){
			$work_workflow_ticket = new Model("work_workflow_ticket");
			if($next_node_id){
				$process_node_list_id = $next_node_id;
			}else{
				$process_node_list_id = $process_node_id;
			}
			$str = "|".implode("|,|",$arrU)."|";
			$arrW = array(
				"receiving_time"=>date("Y-m-d H:i:s"),
				"process_node_id"=>$process_node_id,
				"process_node_list_id"=>$process_node_list_id,
				"first_step"=>"N",
				"work_status"=>$process_status,
				"work_result"=>$work_result,
				"user_name"=>$str
			);
			$res = $work_workflow_ticket->data($arrW)->where("id = '$workflow_id'")->save();

			//请假流程
			if($work_result == "Y"){
				$para_sys = readS();
				$arrTime = explode(",",$para_sys["leave_name"]);
				$start_leave_time = $arrTime[0];
				$end_leave_time = $arrTime[1];
				$arrWFT = $work_workflow_ticket->where("id = '$workflow_id'")->find();
				$table_name = "work_form_".$arrWFT["form_design_id"];   //表单记录表
				$work_form = M($table_name);
				$arrWD = $work_form->where("id = '".$arrWFT["form_id"]."'")->find();
				if($arrWFT["process_design_id"] == $para_sys["leave_process_id"]){
					$this->leaveOperating($arrWFT["applicant"],$arrWD[$start_leave_time],$arrWD[$end_leave_time]);
				}
			}

			echo json_encode(array('success'=>true,'msg'=>'转交成功！','user_name'=>$_REQUEST["user"]));
		} else {
			echo json_encode(array('msg'=>'转交失败！'));
		}
	}

	//请假日期添加到考勤表
	function leaveOperating($leave_of_absence,$start_leave_time,$end_leave_time){
		/*
		$leave_of_absence = "8001";
		$start_leave_time = "2015-03-18 09:06:50";
		$end_leave_time = "2015-03-25 11:06:50";
		*/
		$leave_day = ceil((strtotime($end_leave_time) - strtotime($start_leave_time) )/3600/24);

		//date("Y-m-d",strtotime("+1 day"))
		for($i=0;$i<$leave_day;$i++){
			$arrDate[] = date('Y-m-d',strtotime("$start_leave_time +".$i." days"));
		}

		$row_week = array("1"=>"monday","2"=>"tuesday","3"=>"wednesday","4"=>"thursday","5"=>"friday","6"=>"saturday","0"=>"sunday");
		foreach($arrDate as $val){
			$arrF[] = array(
				"create_date"=>$val,
				"week_day"=>$row_week[date('w',strtotime($val))],
			);
		}

		//请假时去掉周六、周日的日期
		$arrData = array();
		foreach($arrF as $key=>$val){
			if($val["week_day"] != "saturday" && $val["week_day"] != "sunday"){
				$arrData[] = $val;
				$arrDt[] = $val["create_date"];
			}
		}
		//dump($arrData);die;
		$attendance = new Model("pb_attendance");
		$start_time = date("Y-m-d",strtotime($start_leave_time));
		$end_time = date("Y-m-d",strtotime($end_leave_time));
		$del = $attendance->where("user_name = '$leave_of_absence' AND create_date >= '$start_time' AND create_date <= '$end_time' AND whether_leave='Y'")->delete();
		//echo $attendance->getLastSql();die;
		$userArr = readU();
		$deptId_user = $userArr["deptId_user"];
		$sql = "insert into pb_attendance(`user_name`,`dept_id`,`create_date`,`week_day`,`whether_leave`) values";
		$value = "";
		foreach($arrData as $val){
			$str = "(";
			$str .= "'" .$leave_of_absence. "',";
			$str .= "'" .$deptId_user[$leave_of_absence]. "',";
			$str .= "'" .$val['create_date']. "',";
			$str .= "'" .$val['week_day']. "',";
			$str .= "'Y'";
			$str .= ")";
			$value .= empty($value) ? "$str" : ",$str";
		}
		if( $value ){
			$sql .= $value;
			$result = $attendance->execute($sql);
		}
	}

	function deleteWork(){
		$id = $_REQUEST["id"];
		$work_workflow_ticket = new Model("work_workflow_ticket");
		$arrF = $work_workflow_ticket->where("id = '$id'")->find();
		$result = $work_workflow_ticket->where("id in ($id)")->delete();
		if ($result){
			$work_workflow_ticket_none = new Model("work_workflow_ticket_none");
			$del = $work_workflow_ticket_none->where("work_workflow_id in ($id)")->delete();

			$work_form = new Model("work_form_".$arrF["form_design_id"]);
			$del2 = $work_form->where("id = '".$arrF["form_id"]."'")->delete();

			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}
}

?>
