<?php
/*
考试管理
*/
class ExamManagementAction extends Action{
	//题库管理
	function questionList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Question Bank Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function questionData(){
		$username = $_SESSION['user_info']['username'];
		$ks_question_bank = new Model("ks_question_bank");
		$fields = "q.id,q.create_time,q.create_user,q.dept_id,q.curriculum_id,q.question_title,q.topic_answers,q.type_questions,q.question_content,q.term,q.start_validity_time,q.end_validity_time,q.easy_type,q.examination_scores,c.curriculum_name";

		$curriculum_id = $_REQUEST["curriculum_id"];
		$question_title = $_REQUEST["question_title"];
		$type_questions = $_REQUEST["type_questions"];
		$easy_type = $_REQUEST["easy_type"];

		$where = "1 ";
		$where .= empty($curriculum_id) ? "" : " AND q.curriculum_id = '$curriculum_id'";
		$where .= empty($question_title) ? "" : " AND q.question_title like '%$question_title%'";
		$where .= empty($type_questions) ? "" : " AND q.type_questions = '$type_questions'";
		$where .= empty($easy_type) ? "" : " AND q.easy_type = '$easy_type'";

		$count = $ks_question_bank->table("ks_question_bank q")->field($fields)->join("ks_curriculum c on (q.curriculum_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_question_bank->table("ks_question_bank q")->field($fields)->join("ks_curriculum c on (q.curriculum_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$term_row = array('Y'=>'是','N'=>'否');
		$type_questions_row = array('1'=>'单选题','2'=>'多选题','3'=>'判断题','4'=>'填空题','5'=>'问答题');
		$easy_type_row =  array('1'=>'容易','2'=>'中等','3'=>'较难');
		foreach($arrData as &$val){
			$term = $term_row[$val['term']];
			$val['term'] = $term;

			$type_questions = $type_questions_row[$val['type_questions']];
			$val['type_questions'] = $type_questions;

			$easy_type = $easy_type_row[$val['easy_type']];
			$val['easy_type'] = $easy_type;

			if($val["start_validity_time"] == "0000-00-00 00:00:00"){
				$val["start_validity_time"] = "";
			}
			if($val["end_validity_time"] == "0000-00-00 00:00:00"){
				$val["end_validity_time"] = "";
			}
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addQuestion(){
		checkLogin();
		$this->display();
	}

	function insertQuestion(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ks_question_bank = new Model("ks_question_bank");
		$fieldValue = $_REQUEST["field_value"];
		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'curriculum_id'=>$_REQUEST['curriculum_id'],
			'question_title'=>$_REQUEST['question_title'],
			'type_questions'=>$_REQUEST['type_questions'],
			'topic_answers'=>$_REQUEST['topic_answers'],
			'term'=>$_REQUEST['term'],
			'start_validity_time'=>$_REQUEST['start_validity_time'],
			'end_validity_time'=>$_REQUEST['end_validity_time'],
			'easy_type'=>$_REQUEST['easy_type'],
			'examination_scores'=>$_REQUEST['examination_scores'],
			'question_content'=>json_encode($fieldValue),
		);
		//dump($fieldValue);die;
		$result = $ks_question_bank->data($arrData)->add();
		if($_REQUEST["type_questions"] == '1' || $_REQUEST["type_questions"] == '2' || $_REQUEST["type_questions"] == '3' ){
			if ($result){
				$ks_question_bank_select = new Model("ks_question_bank_select");
				$sql = "insert into ks_question_bank_select(select_value, select_name, select_enabled,question_id ,select_order) values ";
				$value = "";
				$i = 0;
				foreach( $fieldValue AS $row ){
					$str = "(";
					$str .= "'" .$row[0]. "',";  	 //选择框value值
					//$str .= "'" .$row[1]. "',";		//选择框的值
					$str .= "'" .str_replace("'",'"',$row[1]). "',";		//选择框的值
					$str .= "'" .$row[2]. "',";		//是否显示
					$str .= "'" .$result. "',";		//字段id
					//$str .= "'" .$_REQUEST["en_name"]. "',";		//字段id
					$str .= "'" .$i. "'";

					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $ks_question_bank_select->execute($sql);
				}
				if($res){
					//$this->fieldCache();
					//$this->selectCache();
					echo json_encode(array('success'=>true,'msg'=>'字段添加成功！'));
				} else {
					echo json_encode(array('msg'=>'字段添加失败！'));
				}
			} else {
				echo json_encode(array('msg'=>'添加失败！'));
			}
		}else{
			if($result){
				//$this->fieldCache();
				echo json_encode(array('success'=>true,'msg'=>'字段添加成功！'));
			} else {
				echo json_encode(array('msg'=>'字段添加失败！'));
			}
		}
	}



	function fieldCache(){
		$ks_question_bank = new Model("ks_question_bank");
		$fieldData = $ks_question_bank->order("create_time asc")->select();
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("question_bank_$db_name",$fieldData,"BGCC/Conf/crm/");
		F("question_bank",$fieldData,"BGCC/Conf/crm/$db_name/");
	}
	function selectCache(){
		$ks_question_bank = new Model("ks_question_bank");
		$fieldData = $ks_question_bank->order("create_time asc")->where("type_questions in (1,2,3)")->select();

		foreach($fieldData as $val){
			$val["question_content"] = json_decode($val["question_content"] ,true);
			$selectTpl[] = $val;
		}
		foreach($selectTpl as $v){
			foreach($v['question_content'] as $vm){
				$tmp[$v['id']][] = array($vm[0]=>$vm[1]);
			}
		}
		foreach($tmp As $key=>&$val){
			$arr = array();
			foreach($val AS $value){
				foreach($value as $k=>$v){
					$arr[$k] = $v;
				}
			}
			$val = $arr;
		}
		//dump($tmp);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("question_bank_content_$db_name",$tmp,"BGCC/Conf/crm/");
		F("question_bank_content",$tmp,"BGCC/Conf/crm/$db_name/");
	}


	function editQuestion(){
		checkLogin();
		$id = $_REQUEST["id"];
		$ks_question_bank = new Model("ks_question_bank");
		$arrData = $ks_question_bank->where("id = $id")->find();

		if($arrData["start_validity_time"] == "0000-00-00 00:00:00"){
			$arrData["start_validity_time"] = "";
		}
		if($arrData["end_validity_time"] == "0000-00-00 00:00:00"){
			$arrData["end_validity_time"] = "";
		}

		$this->assign("id",$id);
		$this->assign("arrData",$arrData);

		if($arrData['type_questions'] == '1' || $arrData['type_questions'] == '2' || $arrData['type_questions'] == '3' ){
			$ks_question_bank_select = new Model("ks_question_bank_select");
			$selectData = $ks_question_bank_select->order("select_order asc")->where("question_id = '$id'")->select();
			$max = $ks_question_bank_select->where("question_id = '$id'")->max("select_order");
			$max2 = $max+1;
			$this->assign("max",$max2);
			$i = 0;
			foreach($selectData as &$val){
				$val["select_order"] = $i;
				$i++;
			}
			$this->assign("selectData",$selectData);
		}
		//dump($max);
		//dump($selectData);die;
		$this->display();
	}

	function updateQuestion(){
		$id = $_REQUEST['id'];
		$ks_question_bank = new Model("ks_question_bank");
		$fieldValue = $_REQUEST["field_value"];
		//dump($fieldValue);die;
		$arrData = Array(
			'curriculum_id'=>$_REQUEST['curriculum_id'],
			'question_title'=>$_REQUEST['question_title'],
			'type_questions'=>$_REQUEST['type_questions'],
			'topic_answers'=>$_REQUEST['topic_answers'],
			'term'=>$_REQUEST['term'],
			'start_validity_time'=>$_REQUEST['start_validity_time'],
			'end_validity_time'=>$_REQUEST['end_validity_time'],
			'easy_type'=>$_REQUEST['easy_type'],
			'examination_scores'=>$_REQUEST['examination_scores'],
			'question_content'=>json_encode($fieldValue),
		);
		$result = $ks_question_bank->data($arrData)->where("id = '$id'")->save();
		if($_REQUEST["type_questions"] == '1' || $_REQUEST["type_questions"] == '2' || $_REQUEST["type_questions"] == '3' ){
			if ($result !== false){
				$ks_question_bank_select = new Model("ks_question_bank_select");
				$resDel = $ks_question_bank_select->where("question_id = '$id'")->delete();
				$sql = "insert into ks_question_bank_select(select_value, select_name, select_enabled,question_id ,select_order) values ";
				$value = "";
				$i = 0;
				foreach( $fieldValue AS $row ){
					$str = "(";
					$str .= "'" .$row[0]. "',";  	 //选择框value值
					$str .= "'" .str_replace("'",'"',$row[1]). "',";		//选择框的值
					//$str .= "'" .$row[1]. "',";		//选择框的值
					$str .= "'" .$row[2]. "',";		//是否显示
					$str .= "'" .$id. "',";		//字段id
					//$str .= "'" .$_REQUEST["en_name"]. "',";		//字段id
					$str .= "'" .$i. "'";

					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $ks_question_bank_select->execute($sql);
				}
				if($res){
					//$this->fieldCache();
					//$this->selectCache();
					echo json_encode(array('success'=>true,'msg'=>'试题添加成功！'));
				} else {
					echo json_encode(array('msg'=>'字段添加失败！'));
				}
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}else{
			if ($result !== false){
				echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}
	}

	function deleteQuestions(){
		$id = $_REQUEST["id"];
		$ks_question_bank = new Model("ks_question_bank");
		$result = $ks_question_bank->where("id in ($id)")->delete();
		if ($result){
			$ks_question_bank_select = new Model("ks_question_bank_select");
			$resDel = $ks_question_bank_select->where("question_id in ($id)")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//试卷管理
	function paperList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Paper Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function paperData(){
		$username = $_SESSION['user_info']['username'];
		$ks_papers = new Model("ks_papers");

		$fields = "p.id,p.create_time,p.create_user,p.dept_id,p.curriculum_id,p.papers_name,p.score_papers,p.topics_way,p.questions_ids,p.papers_describe,p.true_score,c.curriculum_name";

		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$curriculum_id = $_REQUEST["curriculum_id"];
		$topics_way = $_REQUEST["topics_way"];
		$papers_name = $_REQUEST["papers_name"];
		$papers_describe = $_REQUEST["papers_describe"];

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND p.create_time >= '$start_time'";
		$where .= empty($end_time) ? "" : " AND p.create_time <= '$end_time'";
		$where .= empty($curriculum_id) ? "" : " AND p.curriculum_id = '$curriculum_id'";
		$where .= empty($topics_way) ? "" : " AND p.topics_way = '$topics_way'";
		$where .= empty($papers_name) ? "" : " AND p.papers_name like '%$papers_name%'";
		$where .= empty($papers_describe) ? "" : " AND p.papers_describe like '%$papers_describe%'";

		$count = $ks_papers->table("ks_papers p")->field($fields)->join("ks_curriculum c on (p.curriculum_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_papers->order("p.create_time desc")->table("ks_papers p")->field($fields)->join("ks_curriculum c on (p.curriculum_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();


		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		if(file_exists("BGCC/Conf/crm/$db_name/examPaperID.php")){
			$arrP = require "BGCC/Conf/crm/$db_name/examPaperID.php";
		}else{
			$arrP = array();
		}
		//dump($arrP);die;
		$topics_way_row = array("1"=>"人工选题","2"=>"随机选题");
		foreach($arrData as &$val){
			$topics_way = $topics_way_row[$val["topics_way"]];
			$val["topics_way"] = $topics_way;

			$val["questions_ids"] = implode(",",json_decode($val["questions_ids"],true));

			if(in_array($val["id"],$arrP)){
				$val["haved_exam"] = "Y";
			}else{
				$val["haved_exam"] = "N";
			}

			if($val["questions_ids"]){
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"openPaper("."'".$val["questions_ids"]."','".$val["papers_name"]."','".$val["id"]."','".$val["curriculum_id"]."'".")\" >查看试卷</a>";
			}
		}
		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addPaper(){
		checkLogin();
		$this->display();
	}

	function insertPaper(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ks_papers = new Model("ks_papers");
		$topics_way = $_REQUEST['topics_way'];
		if($topics_way == "1"){
			$questions_id = explode(",",$_REQUEST['questions_id']);
			$questions_ids = json_encode($questions_id);
			$policy_id = "";
			$true_score = $_REQUEST['score_papers'];
		}else{
			$policy_id = $_REQUEST["policy_id"];
			$questions_id2 = $this->getQuestionsID($policy_id);
			$questions_id = $questions_id2["id"];
			$questions_ids = json_encode($questions_id);
			$true_score = array_sum($questions_id2["score"]);
		}
		//dump($true_score);die;
		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'curriculum_id'=>$_REQUEST['curriculum_id'],
			'papers_name'=>$_REQUEST['papers_name'],
			'score_papers'=>$_REQUEST['score_papers'],
			'topics_way'=>$topics_way,
			'policy_id'=>$policy_id,
			'true_score'=>$true_score,
			'questions_ids'=>$questions_ids,
			'papers_describe'=>$_REQUEST['papers_describe'],
		);
		//dump($questions_id);
		//dump($arrData);die;
		$result = $ks_papers->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function getQuestionsID($policy_id){
		//$policy_id = "1";
		$ks_policy_scores = new Model("ks_policy_scores");
		$arrData = $ks_policy_scores->order("type_questions asc")->where("policy_id = '$policy_id'")->select();

		//dump($arrData);
		$ks_question_bank = new Model("ks_question_bank");
		$now_time = date("Y-m-d H:i:s");
		$where = "(term='N' OR (start_validity_time<='$now_time' AND end_validity_time>='$now_time')) AND ";     //调试的时候出现问题，少了括号
		foreach($arrData as &$val){
			//$val["remainder_5"] = $val["scores"]%5;
			$val["integer_5"] = floor($val["scores"]/5);
			//$val["remainder_10"] = $val["scores"]%10;
			$val["integer_10"] = floor($val["scores"]/10);
			$val["count_5"] = $ks_question_bank->field("id,type_questions,examination_scores")->where("$where type_questions = '".$val["type_questions"]."' AND examination_scores = '5'")->count();
			$val["count_10"] = $ks_question_bank->field("id,type_questions,examination_scores")->where("$where type_questions = '".$val["type_questions"]."' AND examination_scores = '10'")->count();
			$val["count_20"] = $ks_question_bank->field("id,type_questions,examination_scores")->where("$where type_questions = '".$val["type_questions"]."' AND examination_scores = '20'")->count();


			if($val["count_5"] > 0){
				if($val["count_5"] >= $val["integer_5"]){
					//全是5分的
					$val["id"] = $ks_question_bank->field("id,examination_scores")->where("$where type_questions = '".$val["type_questions"]."' AND examination_scores = '5'")->limit($val["integer_5"])->select();
				}else{
					$val["diff"] = ceil(($val["integer_5"]-$val["count_5"])/2);
					if($val["count_20"] > 0){
						$val["diff_20"] = ceil(($val["integer_5"]-$val["count_5"])/4);
						$val["id_5_num"] = $val["integer_5"]-$val["diff_20"]*4;
						$val["arr_5"] = $ks_question_bank->field("id,examination_scores")->where("$where type_questions = '".$val["type_questions"]."' AND examination_scores = '5'")->limit($val["id_5_num"])->select();
						$val["arr_10"] = $ks_question_bank->field("id,examination_scores")->where("$where type_questions = '".$val["type_questions"]."' AND examination_scores = '20'")->limit($val["diff_20"])->select();
						$val["id"] = array_merge($val["arr_5"],$val["arr_10"]);

					}else{
						//if($val["count_10"]>=$val["diff"]){
							//十分的题目足够多
							$val["id_5_num"] = $val["integer_5"]-$val["diff"]*2;
							$val["arr_5"] = $ks_question_bank->field("id,examination_scores")->where("$where type_questions = '".$val["type_questions"]."' AND examination_scores = '5'")->limit($val["id_5_num"])->select();
							$val["arr_10"] = $ks_question_bank->field("id,examination_scores")->where("$where type_questions = '".$val["type_questions"]."' AND examination_scores = '10'")->limit($val["diff"])->select();
							$val["id"] = array_merge($val["arr_5"],$val["arr_10"]);
						//}
					}
				}
			}else{
				$val["id"] = $ks_question_bank->field("id,examination_scores")->where("$where type_questions = '".$val["type_questions"]."' AND examination_scores = '10'")->limit($val["integer_10"])->select();
			}

			//$arrID[] = $val["id"];
		}
		//echo $ks_question_bank->getLastSql();
		foreach($arrData as &$val){
			$arrID[] = $val["id"];
		}

		foreach($arrID as $key=>&$val){
			foreach($arrID[$key] as $vm){
				$arrF[] = $vm["id"];
				$arrScore[] = $vm["examination_scores"];
			}
		}
		//$result = implode(",",$arrF);
		$arrT = array(
			"id"=>$arrF,
			"score"=>$arrScore
		);
		//dump($arrData);
		//dump($arrID);
		//dump($arrT);die;
		return $arrT;
	}


	function editPaper(){
		checkLogin();
		$id = $_REQUEST['id'];
		$ks_papers = new Model("ks_papers");
		$arrData = $ks_papers->where("id = '$id'")->find();

		$this->assign("arrData",$arrData);
		$this->assign("id",$id);

		$this->display();
	}

	function updatePaper(){
		$id = $_REQUEST['id'];
		$ks_papers = new Model("ks_papers");

		$topics_way = $_REQUEST['topics_way'];
		if($topics_way == "1"){
			$questions_id = explode(",",$_REQUEST['questions_id']);
			$questions_ids = json_encode($questions_id);
			$policy_id = "";
			$true_score = $_REQUEST['score_papers'];
		}else{
			$policy_id = $_REQUEST["policy_id"];
			$questions_id2 = $this->getQuestionsID($policy_id);
			$questions_id = $questions_id2["id"];
			$questions_ids = json_encode($questions_id);
			$true_score = array_sum($questions_id2["score"]);
		}
		$arrData = Array(
			'curriculum_id'=>$_REQUEST['curriculum_id'],
			'papers_name'=>$_REQUEST['papers_name'],
			'score_papers'=>$_REQUEST['score_papers'],
			'topics_way'=>$_REQUEST['topics_way'],
			'papers_describe'=>$_REQUEST['papers_describe'],
			'policy_id'=>$policy_id,
			'true_score'=>$true_score,
			'questions_ids'=>$questions_ids,
		);
		$result = $ks_papers->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deletePaper(){
		$id = $_REQUEST["id"];
		$ks_papers = new Model("ks_papers");
		$result = $ks_papers->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//试题策略配置
	function policyConfigList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Questions Strategy";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function policyConfigData(){
		$username = $_SESSION['user_info']['username'];
		$ks_policy_configuration = new Model("ks_policy_configuration");
		$count = $ks_policy_configuration->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_policy_configuration->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addPolicy(){
		checkLogin();

		$this->display();
	}

	function insertPolicy(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ks_policy_configuration = new Model("ks_policy_configuration");
		$fieldValue = $_REQUEST["field_value"];
		foreach($fieldValue as $val){
			$arr[] = $val[2];
			$val["remainder_5"] = $val[2]%5;
			if($val["remainder_5"] != "0"){
				$arrY[] = $val["remainder_5"];
			}
		}
		if($arrY){
			echo json_encode(array('msg'=>'您所填写的分数中存在不是5的倍数的，请重新填写，分数必须是5的倍数！'));
			die;
		}
		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'policy_name'=>$_REQUEST['policy_name'],
			'policy_description'=>$_REQUEST['policy_description'],
			'policy_content'=>json_encode($fieldValue),
			'policy_score'=>array_sum($arr),
		);
		//dump($arr);die;
		$result = $ks_policy_configuration->data($arrData)->add();
		if ($result){
			$ks_policy_scores = new Model("ks_policy_scores");
			$sql = "insert into ks_policy_scores(type_questions,scores,policy_id) values ";
			$value = "";
			$i = 0;
			foreach( $fieldValue AS $row ){
				$str = "(";
				$str .= "'" .$row[1]. "',";
				$str .= "'" .$row[2]. "',";
				$str .= "'" .$result. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
				$i++;
			}
			unset($i);
			if( $value ){
				$sql .= $value;
				$res = $ks_policy_scores->execute($sql);
			}
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function editPolicy(){
		checkLogin();
		$id = $_REQUEST['id'];
		$ks_policy_configuration = new Model("ks_policy_configuration");
		$arrData = $ks_policy_configuration->where("id='$id'")->find();

		$this->assign("id",$id);
		$this->assign("arrData",$arrData);

		$ks_policy_scores = new Model("ks_policy_scores");
		$selectData = $ks_policy_scores->order("type_questions asc")->where("policy_id = '$id'")->select();
		$max = count($selectData)+1;
		$this->assign("max",$max);
		$i = 0;
		foreach($selectData as &$val){
			$val["select_order"] = $i;
			$i++;
		}
		$this->assign("selectData",$selectData);

		$this->display();
	}

	function updatePolicy(){
		$id = $_REQUEST['id'];
		$ks_policy_configuration = new Model("ks_policy_configuration");
		$fieldValue = $_REQUEST["field_value"];
		foreach($fieldValue as &$val){
			$arr[] = $val[2];
			$val["remainder_5"] = $val[2]%5;
			if($val["remainder_5"] != "0"){
				$arrY[] = $val["remainder_5"];
			}
		}
		if($arrY){
			echo json_encode(array('msg'=>'您所填写的分数中存在不是5的倍数的，请重新填写，分数必须是5的倍数！'));
			die;
		}
		//dump($fieldValue);die;
		$arrData = Array(
			'policy_name'=>$_REQUEST['policy_name'],
			'policy_description'=>$_REQUEST['policy_description'],
			'policy_content'=>json_encode($fieldValue),
			'policy_score'=>array_sum($arr),
		);
		$result = $ks_policy_configuration->data($arrData)->where("id = '$id'")->save();
		//dump($arrData);die;
		if ($result !== false){
			$ks_policy_scores = new Model("ks_policy_scores");
			$del = $ks_policy_scores->where("policy_id = '$id'")->delete();
			$sql = "insert into ks_policy_scores(type_questions,scores,policy_id) values ";
			$value = "";
			$i = 0;
			foreach( $fieldValue AS $row ){
				$str = "(";
				$str .= "'" .$row[1]. "',";
				$str .= "'" .$row[2]. "',";
				$str .= "'" .$id. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
				$i++;
			}
			unset($i);
			if( $value ){
				$sql .= $value;
				$res = $ks_policy_scores->execute($sql);
			}
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deletePolicy(){
		$id = $_REQUEST["id"];
		$ks_policy_configuration = new Model("ks_policy_configuration");
		$result = $ks_policy_configuration->where("id in ($id)")->delete();
		if ($result){
			$ks_policy_scores = new Model("ks_policy_scores");
			$del = $ks_policy_scores->where("policy_id = '$id'")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//考试计划管理
	function examPlanList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Examination Program";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function examPlanData(){
		$username = $_SESSION['user_info']['username'];
		$ks_examination_program = new Model("ks_examination_program");

		$fields = "e.id,e.create_time,e.create_user,e.dept_id,e.paper_id,e.curriculum_id,e.exam_plan_name,e.exam_plan_description,e.exam_plan_address,e.exam_date,e.start_exam_time,e.end_exam_time,e.exam_type,e.are_published,e.examiners,e.reviwers,e.passing_score,e.examination_staff,p.papers_name,c.curriculum_name";

		$start_exam_date = $_REQUEST["start_exam_date"];
		$end_exam_date = $_REQUEST["end_exam_date"];
		$exam_plan_name = $_REQUEST["exam_plan_name"];
		$exam_type = $_REQUEST["exam_type"];
		$curriculum_id = $_REQUEST["curriculum_id"];
		$paper_id = $_REQUEST["paper_id"];

		$where = "1 ";
		$where .= empty($start_exam_date) ? "" : " AND e.exam_date >= '$start_exam_date'";
		$where .= empty($end_exam_date) ? "" : " AND e.exam_date <= '$end_exam_date'";
		$where .= empty($exam_plan_name) ? "" : " AND e.exam_plan_name like '%$exam_plan_name%'";
		$where .= empty($exam_type) ? "" : " AND e.exam_type = '$exam_type'";
		$where .= empty($curriculum_id) ? "" : " AND e.curriculum_id = '$curriculum_id'";
		$where .= empty($paper_id) ? "" : " AND e.paper_id = '$paper_id'";

		$count = $ks_examination_program->order("e.create_time desc")->table("ks_examination_program e")->field($fields)->join("ks_papers p on (e.paper_id = p.id)")->join("ks_curriculum c on (e.curriculum_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_examination_program->order("e.create_time desc")->table("ks_examination_program e")->field($fields)->join("ks_papers p on (e.paper_id = p.id)")->join("ks_curriculum c on (e.curriculum_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $ks_examination_program->getLastSql();die;

		$exam_type_row = array('Y'=>'开卷','N'=>'闭卷');
		$are_published_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$exam_type = $exam_type_row[$val['exam_type']];
			$val['exam_type2'] = $exam_type;

			$are_published = $are_published_row[$val['are_published']];
			$val['are_published2'] = $are_published;


			$val["exam_time"] = $val["start_exam_time"]."~".$val["end_exam_time"];
			$val["examination_staff"] = json_decode($val["examination_staff"],true);
			$val["examination_staff2"] = implode(",",$val["examination_staff"]);


			if($val["examination_staff2"]){
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"openUsers("."'".$val["id"]."'".")\" >查看考试人员</a>";
			}

		}
		//dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertExamPlan(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$ks_examination_program = new Model("ks_examination_program");
		$examination_staff = $_REQUEST['examination_staff'];
		$arrF = explode(",",$examination_staff);
		$exam_start_time = $_REQUEST['exam_date']." ".$_REQUEST['start_exam_time'].":00";

		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'exam_plan_name'=>$_REQUEST['exam_plan_name'],
			'exam_plan_address'=>$_REQUEST['exam_plan_address'],
			'examiners'=>$_REQUEST['examiners'],
			'reviwers'=>$_REQUEST['reviwers'],
			'curriculum_id'=>$_REQUEST['curriculum_id'],
			'paper_id'=>$_REQUEST['paper_id'],
			'exam_date'=>$_REQUEST['exam_date'],
			'start_exam_time'=>$_REQUEST['start_exam_time'],
			'end_exam_time'=>$_REQUEST['end_exam_time'],
			'are_published'=>$_REQUEST['are_published'],
			'passing_score'=>$_REQUEST['passing_score'],
			'exam_plan_description'=>$_REQUEST['exam_plan_description'],
			'exam_type'=>$_REQUEST['exam_type'],
			'examination_staff'=>json_encode($arrF),
			'exam_start_time'=>$exam_start_time,
		);
		$result = $ks_examination_program->data($arrData)->add();
		if ($result){
			$ks_examination_staff = new Model("ks_examination_staff");
			//$del = $ks_examination_staff->where("examination_id = '$result'")->delete();
			$sql = "insert into ks_examination_staff(`examination_id`,`trainers`) values";
			$value = "";
			foreach($arrF as $val){
				$str = "('".$result."','".$val."')";
				$value .= empty($value) ? "$str" : ",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $ks_examination_staff->execute($sql);
				if($_REQUEST['are_published'] == "Y"){
					if($res){
						$this->insertPushData($arrF,$result,$exam_start_time);
					}
				}
			}

			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateExamPlan(){
		$id = $_REQUEST['id'];
		$ks_examination_program = new Model("ks_examination_program");
		$examination_staff = $_REQUEST['examination_staff'];
		$arrF = explode(",",$examination_staff);
		$exam_start_time = $_REQUEST['exam_date']." ".$_REQUEST['start_exam_time'].":00";
		$arrData = Array(
			'exam_plan_name'=>$_REQUEST['exam_plan_name'],
			'exam_plan_address'=>$_REQUEST['exam_plan_address'],
			'examiners'=>$_REQUEST['examiners'],
			'reviwers'=>$_REQUEST['reviwers'],
			'curriculum_id'=>$_REQUEST['curriculum_id'],
			'paper_id'=>$_REQUEST['paper_id'],
			'exam_date'=>$_REQUEST['exam_date'],
			'start_exam_time'=>$_REQUEST['start_exam_time'],
			'end_exam_time'=>$_REQUEST['end_exam_time'],
			'are_published'=>$_REQUEST['are_published'],
			'passing_score'=>$_REQUEST['passing_score'],
			'exam_plan_description'=>$_REQUEST['exam_plan_description'],
			'exam_type'=>$_REQUEST['exam_type'],
			'examination_staff'=>json_encode($arrF),
			'exam_start_time'=>$exam_start_time,
		);
		$result = $ks_examination_program->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			$ks_examination_staff = new Model("ks_examination_staff");
			$del = $ks_examination_staff->where("examination_id = '$id'")->delete();
			$sql = "insert into ks_examination_staff(`examination_id`,`trainers`) values";
			$value = "";
			foreach($arrF as $val){
				$str = "('".$id."','".$val."')";
				$value .= empty($value) ? "$str" : ",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $ks_examination_staff->execute($sql);
				if($_REQUEST['are_published'] == "Y"){
					if($res){
						$this->insertPushData($arrF,$id,$exam_start_time);
					}
				}
			}

			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function insertPushData($arrF,$id,$push_time){
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$push_data_table = "crm_public.push_data";
		}else{
			$push_data_table = "push_data";
		}
		$push_data = new Model("$push_data_table");
		$time = date("Y-m-d",strtotime("-1 day"))." 23:59:59";
		//echo $time;die;
		$del = $push_data->where("(push_type='exam' AND customer_id='$id') OR push_time < '$time'")->delete();
		$sql = "insert into $push_data_table(`customer_id`,`push_user`,`push_time`,`push_type`) values";
			$value = "";
			foreach($arrF as $val){
				$str = "(";
				$str .= "'" .$id. "',";
				$str .= "'" .$val. "',";
				$str .= "'" .$push_time. "',";
				$str .= "'exam'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $push_data->execute($sql);
			}
	}

	function deleteExamPlan(){
		$id = $_REQUEST["id"];
		$ks_examination_program = new Model("ks_examination_program");
		$result = $ks_examination_program->where("id in ($id)")->delete();
		if ($result){
			$ks_examination_staff = new Model("ks_examination_staff");
			$del = $ks_examination_staff->where("examination_id = '$id'")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function shiftUserData($date,$start_time,$end_time){
		//$date = "2015-03-20";
		//$start_time = "15:30";
		//$end_time = "17:30";
		$start = $date." ".$start_time;
		$end = $date." ".$end_time;

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrShift = require "BGCC/Conf/crm/$db_name/shiftSet.php";
		$arrShift = getShiftSet();
		$WorkingHours = $arrShift["WorkingHours"];

		foreach($WorkingHours as $key=>&$val){
			$arrF[] = array(
				"shift_id"=>$key,
				"start_time"=>$date." ".array_shift(explode("~",$val)),
				"end_time"=>$date." ".array_pop(explode("~",$val)),
			);
		}

		foreach($arrF as &$val){
			/*
			班次开始时间 - 培训开始时间 >0   表示开始培训的时候 就上班次了
			班次开始时间 - 培训结束时间 <0  表示开始培训的时候 就上班次了
			*/
			$val["start_diff"] = strtotime($start)-strtotime($val["start_time"]);
			//$val["end_diff"] = strtotime($end)-strtotime($val["end_time"]);
			$val["end_diff"] = strtotime($start)-strtotime($val["end_time"]);

			if( (strtotime($val["start_time"]) >= strtotime($start) && strtotime($val["start_time"])<= strtotime($end)) ||  ( strtotime($val["end_time"]) >= strtotime($start) && strtotime($val["end_time"])<= strtotime($end) ) || ($val["start_diff"]>0 && $val["end_diff"]<0) ){
				$arrID[] = $val["shift_id"];
			}

		}
		$id = implode(",",$arrID);
		$pb_scheduling = new Model("pb_scheduling");
		$arrData = $pb_scheduling->where("scheduling_dates = '$date' AND shift_id in ($id)")->select();

		foreach($arrData as $val){
			$arrU[] = $val["user_name"];
			$arrID[$val["user_name"]] = $val["shift_id"];
		}
		$str = "'".implode("','",$arrU)."'";
		//dump($str);
		//dump($arrF);die;

		$result = array(
			"string" => $str,
			"arrUser" => $arrU,
			"arrID" => $arrID,
		);

		return $result;
	}

	//用户搜索
	function userData(){
		$users = new Model("users");
		$name = $_REQUEST['name'];

		$date = $_REQUEST['date'];
		$start_time = $_REQUEST['start_time'];
		$end_time = $_REQUEST['end_time'];
		$arrT = $this->shiftUserData($date,$start_time,$end_time);
		$str_user = $arrT["string"];
		$arr = $arrT["arrUser"];
		$arrID = $arrT["arrID"];


		$where = "1 ";
		$where .= " AND username not in ($str_user)";
		$where .= empty($name)?"":" AND ( username like '%$name%' OR cn_name like '%$name%'  OR extension like '%$name%' )";

		$count = $users->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$userData = $users->field("username,extension,d_id,r_id,cn_name,en_name,email,fax,extension_type,extension_mac,phone,out_pin,routeid")->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		foreach($userData as &$val){
			if(in_array($val["username"],$arr)){
				$val["shift_id"] = $arrID[$val["username"]];
			}else{
				$val["shift_id"] = " ";
			}
			$val["cn_name"] = $val["username"]."/".$val["cn_name"];
		}

		//dump($userData);die;

		$rowsList = count($userData) ? $userData : false;
		$arrU["total"] = $count;
		$arrU["rows"] = $rowsList;

		echo json_encode($arrU);
	}


	//考试列表
	function examList(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$this->assign("username",$username);
		$this->display();
	}

	function examData(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$ks_exam_scores = new Model("ks_exam_scores");
		$ks_examination_program = new Model("ks_examination_program");

		$fields = "e.id,e.paper_id,e.curriculum_id,e.exam_plan_name,e.exam_plan_description,e.exam_plan_address,e.exam_date,e.start_exam_time,e.end_exam_time,e.exam_type,e.examiners,e.reviwers,e.passing_score,s.trainers,c.curriculum_name,p.questions_ids,p.papers_name";

		$date = date("Y-m-d");
		$where = "e.exam_date = '$date' AND e.are_published = 'Y'";
		//$where = "e.exam_date = '$date' AND s.trainers = '$username'";
		if($username != 'admin'){
			$where .= " AND s.trainers = '$username'";
		}

		$count = $ks_examination_program->table("ks_examination_program e")->field($fields)->join("ks_examination_staff s on (e.id = s.examination_id)")->join("ks_curriculum c on (e.curriculum_id = c.id)")->join("ks_papers p on (e.paper_id = p.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_examination_program->table("ks_examination_program e")->field($fields)->join("ks_examination_staff s on (e.id = s.examination_id)")->join("ks_curriculum c on (e.curriculum_id = c.id)")->join("ks_papers p on (e.paper_id = p.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();


		$time = date("Y-m-d H:i");
		$exam_type_row = array("Y"=>"开卷","N"=>"闭卷");
		foreach($arrData as &$val){
			$exam_type = $exam_type_row[$val["exam_type"]];
			$val["exam_type2"] = $exam_type;

			$val["questions_ids"] = implode(",",json_decode($val["questions_ids"],true));
			$val["exam_time"] = $val["start_exam_time"]."~".$val["end_exam_time"];
			$val["start_time"] = $val["exam_date"]." ".$val["start_exam_time"];
			$val["end_time"] = $val["exam_date"]." ".$val["end_exam_time"];
			if( strtotime($time) >= strtotime($val["start_time"]) && strtotime($time) <= strtotime($val["end_time"]) ){
				$val["examTime"] = "Y";
			}else{
				$val["examTime"] = "N";
			}
			//$val["count"] = $ks_exam_scores->where("paper_id = '".$val["paper_id"]."' AND create_user = '$username'")->count();
			$val["count"] = $ks_exam_scores->where("paper_id = '".$val["paper_id"]."' AND create_user = '".$val["trainers"]."' AND DATE(create_time)='".$val["exam_date"]."'")->count();

			if($val["count"] > 0){
				$val["have"] = "Y";    //是否考过 已经考过
			}else{
				$val["have"] = "N";
			}

			$val["operating"] = "<a href='javascript:void(0);' onclick=\"openExamPaper("."'".$val["questions_ids"]."','".$val["papers_name"]."','".$val["paper_id"]."','".$val["examTime"]."','".$val["curriculum_id"]."','".$val["have"]."','".$val["exam_type"]."','".$val["trainers"]."','".$val["id"]."'".")\" >考试</a>";

		}

		//echo $ks_examination_program->getLastSql();
		//dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//查看试卷、进入考试
	function viewPaper(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST["id"];
		$paper_id = $_REQUEST["paper_id"];
		$papers_name = $_REQUEST["papers_name"];
		$curriculum_id = $_REQUEST["curriculum_id"];
		$program_id = $_REQUEST["program_id"];
		$have = $_REQUEST["have"];   //$have等于Y表示已经存在，再次进入考试【编辑试卷】
		$ks_question_bank = new Model("ks_question_bank");
		$arrData = $ks_question_bank->order("type_questions asc")->field("id,question_title,topic_answers,type_questions,question_content,examination_scores")->where("id in ($id)")->select();


		$i = 1;
		foreach($arrData as &$val){
			$val["order"] = $i;
			$val["question_content"] = json_decode($val["question_content"],true);
			/*
			foreach($val["question_content"] as &$vm){
				if(strstr($val["topic_answers"],$vm[0])){
					$vm["checked"] = "checked";
				}else{
					$vm["checked"] = "";
				}
			}
			*/
			$i++;
		}

		$this->assign("id",$id);
		$this->assign("paper_id",$paper_id);
		$this->assign("curriculum_id",$curriculum_id);
		$this->assign("program_id",$program_id);
		$this->assign("papers_name",$papers_name);

		//dump($arrData);die;

		$web_type = $_REQUEST["web_type"];
		if($web_type == "exam"){
			$this->assign("web_type",$web_type);
		}else{
			$this->assign("web_type","view");
		}
		$date = date("Y-m-d");
		$ks_exam_scores = new Model("ks_exam_scores");
		$arr = $ks_exam_scores->where("paper_id = '$paper_id' AND create_user = '$username' AND DATE(create_time)='$date'")->find();

		if($arr){
			$ks_exam_scores_detail = new Model("ks_exam_scores_detail");
			$exam_scores_id = $arr["id"];
			$arrF = $ks_exam_scores_detail->where("exam_scores_id = '$exam_scores_id'")->select();
			foreach($arrF as $key=>&$val){
				$arrFID[$val["question_id"]] = $val["fill_answer"];

				if($val["question_type"] == "1"){
					$val["name"] = "radio_".$val["question_id"];
				}elseif($val["question_type"] == "2"){
					$val["fill_answer"] = json_encode(explode(" ",$val["fill_answer"]));
					$val["name"] = "multiple_".$val["question_id"];
				}elseif($val["question_type"] == "3"){
					$val["name"] = "judge_".$val["question_id"];
				}elseif($val["question_type"] == "4"){
					$val["name"] = "fill_".$val["question_id"];
				}elseif($val["question_type"] == "5"){
					$val["name"] = "questionsAnswers_".$val["question_id"];
				}
			}
			if($web_type == "exam"){
				foreach($arrData as &$val){
					$val["fill_answer"] = $arrFID[$val["id"]];
					if($val["type_questions"] == "2"){
						foreach($val["question_content"] as &$vm){
							if(strstr($val["fill_answer"],$vm[0])){
								$vm["checked"] = "checked";
							}else{
								$vm["checked"] = "";
							}
						}
					}
				}
			}

			$this->assign("exam_scores_id",$exam_scores_id);
			$this->assign("arrF",$arrF);

		}


		$this->assign("have",$have);
		$this->assign("arrData",$arrData);

		//dump($arrF);
		//dump($arrData);die;
		$this->display();
	}


	function insertExamPapers(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ids = $_REQUEST['id'];
		$ks_question_bank = new Model("ks_question_bank");
		$arrQ = $ks_question_bank->field("id,topic_answers,type_questions,examination_scores")->where("id in ($ids)")->select();
		foreach($arrQ as $key=>&$val){
			$arrQB[$val["id"]] = $val["topic_answers"];
			$arrQBTY[$val["id"]] = $val["type_questions"];
			$arrQBS[$val["id"]] = $val["examination_scores"];
		}

		$arrF = $_REQUEST;
		foreach($arrF as $key=>&$val){
			if($key != "id" && $key != "paper_id" && $key != "curriculum_id"  && $key != "program_id" && $key != "__hash__")
			$arr[] = array(
				"id"=>array_pop(explode("_",$key)),
				"value"=>$val,
				"topic_answers"=>$arrQB[array_pop(explode("_",$key))],
				"type_questions"=>$arrQBTY[array_pop(explode("_",$key))],
				"examination_scores"=>$arrQBS[array_pop(explode("_",$key))]
			);
		}

		foreach($arr as &$val){
			//单选、判断
			if($val["type_questions"] == "1" || $val["type_questions"] == "3"){
				if($val["value"] == $val["topic_answers"]){
					$val["correct"] = "Y";
				}else{
					$val["correct"] = "N";
				}
			}
			//多选
			if($val["type_questions"] == "2"){
				//$val["tt"] = explode(" ",$val["topic_answers"]);
				foreach(explode(" ",$val["topic_answers"]) as $v){
					if($v != ""){
						$val["answers"][] = $v;
					}
				}
				$val["cmp"] = array_diff($val["answers"],$val["value"]);
				if(!$val["cmp"]){
					$val["correct"] = "Y";
				}else{
					$val["correct"] = "N";   //暂时还没有办法判断答对一半的
				}

				$val["value2"] = implode(" ",$val["value"]);
			}else{
				$val["value2"] = $val["value"];
			}

			if($val["correct"] == "Y"){
				$val["scores"] = $val["examination_scores"];
			}else{
				$val["scores"] = "0";
			}

		}

		//dump($arr);
		//dump($arrF);die;
		$ks_exam_scores = new Model("ks_exam_scores");
		$arrData = array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'paper_id'=>$_REQUEST['paper_id'],
			'curriculum_id'=>$_REQUEST['curriculum_id'],
			'program_id'=>$_REQUEST['program_id'],
		);
		$result = $ks_exam_scores->data($arrData)->add();
		if ($result){
			$this->scorePaperIDCache();
			$ks_exam_scores_detail = new Model("ks_exam_scores_detail");
			$sql = "insert into ks_exam_scores_detail(question_id,question_type,fill_answer,answer_correct,scores,exam_scores_id) values ";
			$value = "";
			$i = 0;
			foreach( $arr AS &$val ){
				$str = "(";
				$str .= "'" .$val["id"]. "',";
				$str .= "'" .$val["type_questions"]. "',";
				$str .= "'" .$val["value2"]. "',";
				$str .= "'" .$val["correct"]. "',";
				$str .= "'" .$val["scores"]. "',";
				$str .= "'" .$result. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
				$i++;
			}
			unset($i);
			if( $value ){
				$sql .= $value;
				$res = $ks_exam_scores_detail->execute($sql);
			}
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}

	}

	function scorePaperIDCache(){
		$ks_exam_scores = new Model("ks_exam_scores");
		$arrData = $ks_exam_scores->group("paper_id")->select();
		foreach($arrData as $val){
			$arrF[] = $val["paper_id"];
		}

		//dump($arrF);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("examPaperID_$db_name",$arrF,"BGCC/Conf/crm/");
		F("examPaperID",$arrF,"BGCC/Conf/crm/$db_name/");
	}

	function updateExamPapers(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ids = $_REQUEST['id'];
		$ks_question_bank = new Model("ks_question_bank");
		$arrQ = $ks_question_bank->field("id,topic_answers,type_questions,examination_scores")->where("id in ($ids)")->select();
		foreach($arrQ as $key=>&$val){
			$arrQB[$val["id"]] = $val["topic_answers"];
			$arrQBTY[$val["id"]] = $val["type_questions"];
			$arrQBS[$val["id"]] = $val["examination_scores"];
		}

		$arrF = $_REQUEST;
		foreach($arrF as $key=>&$val){
			if($key != "id" && $key != "paper_id" && $key != "curriculum_id"  && $key != "program_id"  && $key != "exam_scores_id" && $key != "__hash__")
			$arr[] = array(
				"id"=>array_pop(explode("_",$key)),
				"value"=>$val,
				"topic_answers"=>$arrQB[array_pop(explode("_",$key))],
				"type_questions"=>$arrQBTY[array_pop(explode("_",$key))],
				"examination_scores"=>$arrQBS[array_pop(explode("_",$key))]
			);
		}

		foreach($arr as &$val){
			//单选、判断
			if($val["type_questions"] == "1" || $val["type_questions"] == "3"){
				if($val["value"] == $val["topic_answers"]){
					$val["correct"] = "Y";
				}else{
					$val["correct"] = "N";
				}
			}
			//多选
			if($val["type_questions"] == "2"){
				//$val["tt"] = explode(" ",$val["topic_answers"]);
				foreach(explode(" ",$val["topic_answers"]) as $v){
					if($v != ""){
						$val["answers"][] = $v;
					}
				}
				$val["cmp"] = array_diff($val["answers"],$val["value"]);
				if(!$val["cmp"]){
					$val["correct"] = "Y";
				}else{
					$val["correct"] = "N";   //暂时还没有办法判断答对一半的
				}

				$val["value2"] = implode(" ",$val["value"]);
			}else{
				$val["value2"] = $val["value"];
			}

			if($val["correct"] == "Y"){
				$val["scores"] = $val["examination_scores"];
			}else{
				$val["scores"] = "0";
			}

		}

		//dump($arr);
		//dump($arrF);die;
		$ks_exam_scores = new Model("ks_exam_scores");
		$arrData = array(
			'modification_time'=>date("Y-m-d H:i:s")
		);
		$exam_scores_id = $_REQUEST['exam_scores_id'];
		$result = $ks_exam_scores->data($arrData)->where("id = '$exam_scores_id'")->save();
		if ($result !== false){
			$this->scorePaperIDCache();
			$ks_exam_scores_detail = new Model("ks_exam_scores_detail");
			$del = $ks_exam_scores_detail->where("exam_scores_id = '$exam_scores_id'")->delete();
			$sql = "insert into ks_exam_scores_detail(question_id,question_type,fill_answer,answer_correct,scores,exam_scores_id) values ";
			$value = "";
			$i = 0;
			foreach( $arr AS &$val ){
				$str = "(";
				$str .= "'" .$val["id"]. "',";
				$str .= "'" .$val["type_questions"]. "',";
				$str .= "'" .$val["value2"]. "',";
				$str .= "'" .$val["correct"]. "',";
				$str .= "'" .$val["scores"]. "',";
				$str .= "'" .$exam_scores_id. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
				$i++;
			}
			unset($i);
			if( $value ){
				$sql .= $value;
				$res = $ks_exam_scores_detail->execute($sql);
			}
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	//试卷查看、评估
	function assessmentPapers(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Assessment Papers";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$function_type = $_REQUEST["function_type"];
		$this->assign("function_type",$function_type);


		$web_type = $_REQUEST["web_type"];
		if($web_type == "agent" && $web_type){
			$this->assign("web_type","agent");
		}else{
			$this->assign("web_type","back");
		}

		$this->display();
	}

	function assessmentPapersData(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ks_exam_scores = new Model("ks_exam_scores");

		$feilds = "s.id,s.create_time,s.create_user,s.modification_time,s.paper_id,s.curriculum_id,s.program_id,s.exam_score,s.complaint_content,s.processing_agents,s.processing_results,s.processing_content,p.papers_name,p.score_papers,p.true_score,p.questions_ids,c.curriculum_name,e.examiners,e.reviwers,e.passing_score,e.exam_plan_name";


		$arrDep = $this->getDepTreeArray();
		$deptst = $this->getMeAndSubDeptName($arrDep,$d_id);  //取上级部门
		$deptSet = rtrim($deptst,",");

		$function_type = $_REQUEST["function_type"];
		$web_type = $_REQUEST["web_type"];
		$start_time = $_REQUEST["start_time"];
		$emd_time = $_REQUEST["emd_time"];
		$paper_id = $_REQUEST["paper_id"];
		$curriculum_id = $_REQUEST["curriculum_id"];
		$exam_score = $_REQUEST["exam_score"];
		$create_user = $_REQUEST["create_user"];

		$where = "1 ";
		if($web_type == "agent"){
			if($username != "admin"){
				$where .= " AND s.create_user = '$username'";
			}
		}else{
			if($username != "admin"){
				//$where .= " AND s.dept_id in ($deptSet)";
				$where .= " AND (s.dept_id in ($deptSet) OR e.reviwers = '$username')";
			}
		}
		$where .= empty($start_time) ? "" : " AND s.create_time >= '$start_time'";
		$where .= empty($emd_time) ? "" : " AND s.create_time <= '$emd_time'";
		$where .= empty($paper_id) ? "" : " AND s.paper_id = '$paper_id'";
		$where .= empty($curriculum_id) ? "" : " AND s.curriculum_id = '$curriculum_id'";
		if($exam_score == "Y"){
			$where .= empty($exam_score) ? "" : " AND s.exam_score IS NOT NULL";
		}else{
			$where .= empty($exam_score) ? "" : " AND s.exam_score IS NULL";
		}
		$where .= empty($create_user) ? "" : " AND s.create_user = '$create_user'";

		$count = $ks_exam_scores->table("ks_exam_scores s")->field($feilds)->join("ks_papers p on (s.paper_id = p.id)")->join("ks_curriculum c on (s.curriculum_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_exam_scores->order("s.create_time desc")->table("ks_exam_scores s")->field($feilds)->join("ks_papers p on (s.paper_id = p.id)")->join("ks_curriculum c on (s.curriculum_id = c.id)")->join("ks_examination_program e on (s.program_id = e.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $ks_exam_scores->getLastSql();die;

		$menuname = "Assessment Papers";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		foreach($arrData as &$val){
			$val["operating"] = "";
			$val["questions_ids"] = implode(",",json_decode($val["questions_ids"],true));
			if($username == $val["examiners"] || $username == "admin" || $priv["refer"] == "Y" || $function_type == "score"){
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"openMarking("."'".$val["questions_ids"]."','".$val["papers_name"]."','".$val["paper_id"]."','".$val["curriculum_id"]."','".$val["examiners"]."','".$val["reviwers"]."','".$val["id"]."'".")\" >阅卷&nbsp;&nbsp;&nbsp;</a>";
			}

			if(($username == $val["examiners"] || $username == "admin" || $priv["evaluation"] == "Y")  && $function_type != "score"){
				$val["operating"] .= "<a href='javascript:void(0);' onclick=\"markExamPapers("."'".$val["questions_ids"]."','".$val["papers_name"]."','".$val["paper_id"]."','".$val["curriculum_id"]."','".$val["examiners"]."','".$val["reviwers"]."','".$val["id"]."'".")\" >评卷</a>";
			}
		}

		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	//阅卷、评卷
	function viewPaper2(){
		header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST["id"];
		$paper_id = $_REQUEST["paper_id"];
		$papers_name = $_REQUEST["papers_name"];
		$curriculum_id = $_REQUEST["curriculum_id"];
		$examiners = $_REQUEST["examiners"];    //阅卷人
		$reviwers = $_REQUEST["reviwers"];		//评卷人
		$ks_question_bank = new Model("ks_question_bank");
		$arrData = $ks_question_bank->order("type_questions asc")->field("id,question_title,topic_answers,type_questions,question_content,examination_scores")->where("id in ($id)")->select();


		$i = 1;
		foreach($arrData as &$val){
			$val["order"] = $i;
			$val["question_content"] = json_decode($val["question_content"],true);
			$i++;
		}

		$this->assign("id",$id);
		$this->assign("paper_id",$paper_id);
		$this->assign("curriculum_id",$curriculum_id);
		$this->assign("papers_name",$papers_name);

		//dump($arrData);die;

		$web_type = $_REQUEST["web_type"];
		if($web_type == "reviwer"){
			$this->assign("web_type",$web_type);
		}else{
			$this->assign("web_type","view");
		}


		$exam_scores_id = $_REQUEST["exam_scores_id"];
		$ks_exam_scores = new Model("ks_exam_scores");
		$arrES = $ks_exam_scores->where("id = '$exam_scores_id'")->find();

		$ks_exam_scores_detail = new Model("ks_exam_scores_detail");
		$arrF = $ks_exam_scores_detail->where("exam_scores_id = '$exam_scores_id'")->select();
		foreach($arrF as $key=>&$val){
			$arrFID[$val["question_id"]] = $val["fill_answer"];

			if($val["question_type"] == "1"){
				$val["name"] = "radioVal_".$val["question_id"];
				$val["score_name"] = "radioScore_".$val["question_id"];
				$val["proposal_name"] = "radioProposal_".$val["question_id"];
			}elseif($val["question_type"] == "2"){
				$val["fill_answer"] = json_encode(explode(" ",$val["fill_answer"]));
				$val["name"] = "multipleVal_".$val["question_id"];
				$val["score_name"] = "multipleScore_".$val["question_id"];
				$val["proposal_name"] = "multipleProposal_".$val["question_id"];
			}elseif($val["question_type"] == "3"){
				$val["name"] = "judgeVal_".$val["question_id"];
				$val["score_name"] = "judgeScore_".$val["question_id"];
				$val["proposal_name"] = "judgeProposal_".$val["question_id"];
			}elseif($val["question_type"] == "4"){
				$val["name"] = "fillVal_".$val["question_id"];
				$val["score_name"] = "fillScore_".$val["question_id"];
				$val["proposal_name"] = "fillProposal_".$val["question_id"];

				if($val["scores"] == "0" && $arrES["whether_marking"] == "N"){
					$val["scores"] = "";
				}
			}elseif($val["question_type"] == "5"){
				$val["name"] = "questionsAnswersVal_".$val["question_id"];
				$val["score_name"] = "questionsAnswersScore_".$val["question_id"];
				$val["proposal_name"] = "questionsAnswersProposal_".$val["question_id"];

				if($val["scores"] == "0" && $arrES["whether_marking"] == "N"){
					$val["scores"] = "";
				}
			}
		}
		foreach($arrData as &$val){
			$val["fill_answer"] = $arrFID[$val["id"]];
			if($val["type_questions"] == "2"){
				foreach($val["question_content"] as &$vm){
					if(strstr($val["fill_answer"],$vm[0])){
						$vm["checked"] = "checked";
					}else{
						$vm["checked"] = "";
					}
				}
			}
		}

		$this->assign("exam_scores_id",$exam_scores_id);
		$this->assign("arrF",$arrF);

		$this->assign("arrData",$arrData);
		$this->assign("username",$username);
		$this->assign("arrES",$arrES);

		$menuname = "Assessment Papers";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];
		$this->assign("priv",$priv);

		if($username == "admin" || $priv["evaluation"] == "Y"){
			$this->assign("complaint2","Y");
		}else{
			$this->assign("complaint2","N");
		}


		//dump($arrF);die;
		//dump($arrData);die;
		$this->display();
	}


	//评卷
	function markExamPapers(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ids = $_REQUEST['id'];
		$ks_question_bank = new Model("ks_question_bank");
		$arrQ = $ks_question_bank->field("id,topic_answers,type_questions,examination_scores")->where("id in ($ids)")->select();
		foreach($arrQ as $key=>&$val){
			$arrQB[$val["id"]] = $val["topic_answers"];
			$arrQBTY[$val["id"]] = $val["type_questions"];
			$arrQBS[$val["id"]] = $val["examination_scores"];
		}

		$arrF = $_REQUEST;
		foreach($arrF as $key=>&$val){
			if($key != "id" && $key != "paper_id" && $key != "curriculum_id"  && $key != "program_id"  && $key != "exam_scores_id" && $key != "__hash__")
			if(strstr($key,"Val")){
				$arr[] = array(
					"id"=>array_pop(explode("_",$key)),
					"value"=>$val,
					"topic_answers"=>$arrQB[array_pop(explode("_",$key))],
					"type_questions"=>$arrQBTY[array_pop(explode("_",$key))],
					"examination_scores"=>$arrQBS[array_pop(explode("_",$key))]
				);
			}elseif(strstr($key,"Score")){
				$arr2[] =  array(
					"id"=>array_pop(explode("_",$key)),
					"value"=>$val
				);
			}elseif(strstr($key,"Proposal")){
				$arr3[] =  array(
					"id"=>array_pop(explode("_",$key)),
					"value"=>$val
				);
			}
		}

		foreach($arr2 as $key=>$val){
			$arrScore[$val["id"]] = $val["value"];
		}
		foreach($arr3 as $key=>$val){
			$arrProposal[$val["id"]] = $val["value"];
		}

		foreach($arr as &$val){
			//单选、判断
			if($val["type_questions"] == "1" || $val["type_questions"] == "3"){
				if($val["value"] == $val["topic_answers"]){
					$val["correct"] = "Y";
				}else{
					$val["correct"] = "N";
				}
			}
			//多选
			if($val["type_questions"] == "2"){
				//$val["tt"] = explode(" ",$val["topic_answers"]);
				foreach(explode(" ",$val["topic_answers"]) as $v){
					if($v != ""){
						$val["answers"][] = $v;
					}
				}
				$val["cmp"] = array_diff($val["answers"],$val["value"]);
				if(!$val["cmp"]){
					$val["correct"] = "Y";
				}else{
					$val["correct"] = "N";   //暂时还没有办法判断答对一半的
				}

				$val["value2"] = implode(" ",$val["value"]);
			}else{
				$val["value2"] = $val["value"];
			}

			if($val["type_questions"] == "4" || $val["type_questions"] == "5"){
				$val["scores"] = $arrScore[$val["id"]];
				$val["reviewers_comments"] = $arrProposal[$val["id"]];
			}else{
				if($val["correct"] == "Y"){
					$val["scores"] = $val["examination_scores"];
				}else{
					$val["scores"] = "0";
				}
			}

			$arrS[] = $val["scores"];

		}

		//dump($arr);
		//dump($arrF);die;
		$ks_exam_scores = new Model("ks_exam_scores");
		$arrData = array(
			'exam_score'=>array_sum($arrS),
			'whether_marking'=>"Y"
		);
		$exam_scores_id = $_REQUEST['exam_scores_id'];
		$result = $ks_exam_scores->data($arrData)->where("id = '$exam_scores_id'")->save();
		if ($result !== false){
			$ks_exam_scores_detail = new Model("ks_exam_scores_detail");
			$del = $ks_exam_scores_detail->where("exam_scores_id = '$exam_scores_id'")->delete();
			$sql = "insert into ks_exam_scores_detail(question_id,question_type,fill_answer,answer_correct,scores,reviewers_comments,exam_scores_id) values ";
			$value = "";
			$i = 0;
			foreach( $arr AS &$val ){
				$str = "(";
				$str .= "'" .$val["id"]. "',";
				$str .= "'" .$val["type_questions"]. "',";
				$str .= "'" .$val["value2"]. "',";
				$str .= "'" .$val["correct"]. "',";
				$str .= "'" .$val["scores"]. "',";
				$str .= "'" .$val["reviewers_comments"]. "',";
				$str .= "'" .$exam_scores_id. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
				$i++;
			}
			unset($i);
			if( $value ){
				$sql .= $value;
				$res = $ks_exam_scores_detail->execute($sql);
			}
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}

	}

	//对有异议的成绩可发起申诉
	function saveComplaint(){
		$username = $_SESSION['user_info']['username'];
		$exam_scores_id = $_REQUEST['exam_scores_id'];
		$ks_exam_scores = new Model("ks_exam_scores");
		if($_REQUEST["processing_results"] && $_REQUEST["create_user"] != $username){
			$arrData = array(
				"processing_agents"=>$username,
				"processing_results"=>$_REQUEST["processing_results"],
				"processing_content"=>$_REQUEST["processing_content"]
			);
		}else{
			$arrData = array(
				"complaint_content"=>$_REQUEST["complaint_content"]
			);
		}
		//dump($arrData);die;
		$result = $ks_exam_scores->data($arrData)->where("id = '$exam_scores_id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"操作成功！"));
		} else {
			echo json_encode(array('msg'=>'操作失败！'));
		}
	}

    /*
    * 获得一个数组，该数组是一维数组，存放id，名称，id和下一级别部门的id
    */
    function getDepTreeArray(){
        $DepTree = array();//一维数组
        $dep = M('Department');
        $arr = $dep->select();
        foreach($arr AS $v){
            $currentId = $v['d_id'];
            $arrSonId = $dep->field('d_id')->where("d_pid=$currentId")->select();
            $strId = "$currentId";
            foreach($arrSonId AS $row){
                $strId .= "," . $row['d_id'];
            }
            $arrDepTree[$currentId] = Array(
                "id" => $v['d_id'],
                "pid" => $v['d_pid'],
                "name"=> $v['d_name'],
                "meAndSonId"=>$strId,
            );
        }
		//dump($arrDepTree);die;
        return $arrDepTree;
    }
	function getMeAndSubDeptName($arrDep,$dept_id){
		$arrId = explode(',',$arrDep[$dept_id]['meAndSonId']);
		//$str = "'" . $arrDep[$dept_id]['name'] . "',";
		$str = "'" . $arrDep[$dept_id]['id'] . "',";
		if( array_shift($arrId) ){
			foreach( $arrId AS $id ){
				$str .= $this->getMeAndSubDeptName($arrDep,$id);
			}
		}
		return $str;

	}

}
?>
