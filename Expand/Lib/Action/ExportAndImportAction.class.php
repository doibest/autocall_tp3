<?php
/*
导出导入管理
*/
class ExportAndImportAction extends Action{
	//导出培训计划
	function exportTrainData(){
		$username = $_SESSION['user_info']['username'];
		$ks_train = new Model("ks_train");

		$fields = "t.id,t.create_time,t.create_user,t.dept_id,t.curriculum_id,t.courseware_id,t.training_task_name,t.training_address,t.training_task_description,t.training_date,t.training_start_time,t.training_end_time,t.whether_exam,t.ks_id,t.participants,t.lectures_people,c.courseware_name,kc.curriculum_name,e.exam_plan_name";

		$start_training_date = $_REQUEST["start_training_date"];
		$end_training_date = $_REQUEST["end_training_date"];
		$training_task_name = $_REQUEST["training_task_name"];
		$curriculum_id = $_REQUEST["curriculum_id"];
		$courseware_id = $_REQUEST["courseware_id"];

		$where = "1 ";
		$where .= empty($start_training_date) ? "" : " AND t.training_date >= '$start_training_date'";
		$where .= empty($end_training_date) ? "" : " AND t.training_date <= '$end_training_date'";
		$where .= empty($training_task_name) ? "" : " AND t.training_task_name like '%$training_task_name%'";
		$where .= empty($curriculum_id) ? "" : " AND t.curriculum_id = '$curriculum_id'";
		$where .= empty($courseware_id) ? "" : " AND t.courseware_id = '$courseware_id'";

		$arrData = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->join("ks_examination_program e on (t.ks_id = e.id)")->where($where)->select();
		$whether_exam_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$whether_exam = $whether_exam_row[$val['whether_exam']];
			$val['whether_exam2'] = $whether_exam;

			$val["training_time"] = $val["training_start_time"]."~".$val["training_end_time"];
			$val["participants"] = implode(",",json_decode($val["participants"],true));
		}

		//dump($arrData);die;

		$field = array("create_time","create_user","training_task_name","lectures_people","training_address","curriculum_name","courseware_name","training_date","training_time","whether_exam2","exam_plan_name","participants");
		$title = array("创建时间","创建人","培训任务名称","讲课人","培训计划地址","课程名称","课件名称","开课日期","开课时间段","培训后是否考试","考试计划名称","参加人员");
		$count = count($field);
		$excelTiele = "培训计划".date("Y-m-d");

		//$this->exportDataFunction($count,$field,$title,$arrData,$excelTiele);
		$this->exportDataToPHPExcel($count,$field,$title,$arrData,$excelTiele);
	}


	//获取导入培训计划的字段
	function getTrainField(){
		$field = array("training_task_name","curriculum_id","courseware_id","ks_id","lectures_people","training_address","training_date","training_start_time","training_end_time","whether_exam");   //,"participants"
		$title = array("培训任务名称","课程名称","课件名称","考试计划名称","讲课人","培训计划地址","开课日期","开课时间","结束时间","培训后是否考试");   //,"参加人员"

		$arrData = array(
			"field"=>$field,
			"title"=>$title
		);
		//dump($arrData);
		return $arrData;
	}

	//下载培训计划模版
	function downloadTrainTemplates(){
		$arrF = $this->getTrainField();
		$count = count($arrF["field"]);
		$field = $arrF["field"];
		$title = $arrF["title"];
		$arrData = array();
		$excelTiele = "导入培训计划模版";
		$this->exportDataFunction($count,$field,$title,$arrData,$excelTiele);
	}

	//导入培训计划
	function importTrainData(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$table = "ks_train";
		$source = new Model("ks_train");

		$arrF = $this->getTrainField();
		$field = $arrF["field"];
		$count = count($field)+1;
		$title = $arrF["title"];
		$fields = "`".implode("`,`",$field)."`".",`create_time`,`create_user`,`dept_id`";
		$field_key = array_flip($field);

		//判断文件类型
		$tmp_file = $_FILES["trian_name"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["trian_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		vendor("PHPExcel176.PHPExcel");
		//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array();
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($_FILES["trian_name"]["tmp_name"]);

		$arrData = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组
		/*
		//$indata2 = $objPHPExcel->getSheet(1)->toArray();   //多个sheet
		$sheet = $objPHPExcel->getSheetCount();  //excel  sheet个数
		$sheetInfo = $objPHPExcel->getSheet(0);
		$highestRow = $sheetInfo->getHighestRow(); // 取得总行数   包括第一行标题
		$highestColumn = $sheetInfo->getHighestColumn(); // 取得总列数
		*/
		array_shift($arrData);   //去掉第

		//$avg = ceil(count($arrData)/8);
		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		//==========================选择框开始===============================
		$ttField = array('curriculum_id','courseware_id','ks_id','whether_exam');   //需要转换的字段【选择框】
		$selectValue = $this->getSelectValue();
		foreach($selectValue as $key=>$val){
			$selectKey[$key] = array_flip($val);
		}

		foreach($selectKey as $key=>$val){
			if(in_array($key,$ttField)){
				$arrK[$key] = $val;
			}
		}
		$selectKey = $arrK;
		//dump($selectKey);die;
		$selectF = array_flip($selectField);
		$i = 0;
		foreach($selectKey as $key=>$val){
			$selectKey[$key]['index'] = $key;
			$i++;
		}
		foreach($selectKey as &$val){
			$val['index'] = $field_key[$val['index']];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $val['index'] = $field_key[$val['index']]+1;
			$selectKey2[$val['index']] = $val;
		}
		foreach($selectKey2 as $val){
			$index[] = $val['index'];
		}


		foreach($arrK as $k=>$vm){
			$cm_select[] = $field_key[$k];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $cm_select[] = $field_key[$k]+1;
		}


		foreach($cm_select as $vm){
			foreach($arrData as &$val){
				$tt = $selectKey2[$vm][$val[$vm]];
				$val[$vm] = $tt;
			}
		}
		//==========================选择框结束==============================



		$sql = "insert into $table($fields) values ";
		$value = "";
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>&$val ){
			//$val[$field_key["participants"]] = json_encode(explode(",",$val[$field_key["participants"]]));

			$str = "(";
			for($i=0;$i<=($count-2);$i++){
				$str .= "'" .str_replace("'","",$val[$i]). "',";
			}
			$str .= "'" .date("Y-m-d H:i:s"). "',";
			$str .= "'" .$username. "',";
			$str .= "'" .$d_id. "'";
			$str .= ")";

			if($count_rows >=8){
				if($key>$avg && $key<=$avg*2){
					$value2 .= empty($value2)?$str:",$str";
				}elseif($key>$avg*2 && $key<=$avg*3){
					$value3 .= empty($value3)?$str:",$str";
				}elseif($key>$avg*3 && $key<=$avg*4){
					$value4 .= empty($value4)?$str:",$str";
				}elseif($key>$avg*4 && $key<=$avg*5){
					$value5 .= empty($value5)?$str:",$str";
				}elseif($key>$avg*5 && $key<=$avg*6){
					$value6.= empty($value6)?$str:",$str";
				}elseif($key>$avg*6 && $key<=$avg*7){
					$value7 .= empty($value7)?$str:",$str";
				}elseif($key>$avg*7 && $key<=$avg*8){
					$value8 .= empty($value8)?$str:",$str";
				}elseif($key>$avg*8 && $key<=$avg*9){
					$value9 .= empty($value9)?$str:",$str";
				}elseif($key<=$avg){
					$value .= empty($value)?$str:",$str";
				}
			}else{
				$value .= empty($value)?$str:",$str";
			}
		}
		//dump($arrData);die;

		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			//echo $sql;die;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"成功导入${num}条数据！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}


	}

	function getSelectValue(){
		$ks_curriculum = new Model("ks_curriculum");
		$arrKc = $ks_curriculum->field("id,curriculum_name")->select();
		foreach($arrKc as $key=>$val){
			$arrKcn[$val["id"]] = $val["curriculum_name"];
		}

		$ks_courseware = new Model("ks_courseware");
		$arrKj = $ks_courseware->field("id,courseware_name")->select();
		foreach($arrKj as $key=>$val){
			$arrKjn[$val["id"]] = $val["courseware_name"];
		}


		$ks_examination_program = new Model("ks_examination_program");
		$arrEp = $ks_examination_program->field("id,exam_plan_name")->select();
		foreach($arrEp as $key=>$val){
			$arrEpn[$val["id"]] = $val["exam_plan_name"];
		}
		$whether_exam = array('Y'=>'是','N'=>'否');

		$arrF = array(
			"curriculum_id"=>$arrKcn,
			"courseware_id"=>$arrKjn,
			"ks_id"=>$arrEpn,
			"whether_exam"=>$whether_exam
		);

		//dump($arrF);die;
		return $arrF;
	}








	function exportDataToPHPExcel($count,$field,$title,$arrData,$excelTiele){
		vendor("PHPExcel176.PHPExcel");
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			//$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setWrapText(true);    //自动换行
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  //水平居中
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($arrData as &$val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
			}
			$start_row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$filename = iconv("utf-8","gb2312",$excelTiele);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}


	function exportDataFunction($count,$field,$title,$arrData,$excelTiele){
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type:text/html;charset=UTF-8");
		$filename = iconv("utf-8","gb2312",$excelTiele);
        header("Content-Disposition: attachment;filename=$excelTiele.xls ");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();
        $start_row	=	0;

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
			xlsWriteLabel($start_row,$i,utf2gb($title[$i]));
		}

        $start_row++;
		foreach($arrData as &$val){
			for($j=0;$j<$count;$j++){
				 xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
			}
			$start_row++;
		}
        xlsEOF();
	}


	//导入FAQ的字段
	function getFaqField(){
		$field = array("type_id","title","content","term","start_validity_time","end_validity_time");
		$title = array("名称","标题","内容","是否有期限","有效期开始","有效期结束");
		$arrData = array(
			"field"=>$field,
			"title"=>$title
		);
		//dump($arrData);
		return $arrData;
	}

	//下载FAQ模版
	function downloadFaqTemplates(){
		$arrF = $this->getFaqField();
		$count = count($arrF["field"]);
		$field = $arrF["field"];
		$title = $arrF["title"];
		$arrData = array();
		$excelTiele = "FAQ模版";
		$this->exportDataFunction($count,$field,$title,$arrData,$excelTiele);
	}

	function getFaqSelectValue(){
		$faq_type = new Model("faq_type");
		$arrF = $faq_type->select();
		foreach($arrF as $val){
			$arrT[$val["id"]] = $val["name"];
		}

		$arrData = array(
			"type_id"=>$arrT,
			"term"=>array("Y"=>"是","N"=>"否")
		);
		//dump($arrData);die;
		return $arrData;
	}

	//导入FAQ
	function importFaqData(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$table = "faq_content";
		$source = new Model($table);

		$arrF = $this->getFaqField();
		$field = $arrF["field"];
		$count = count($field)+1;
		$title = $arrF["title"];
		$fields = "`".implode("`,`",$field)."`".",`createtime`,`create_user`";
		$field_key = array_flip($field);

		//判断文件类型
		$tmp_file = $_FILES["trian_name"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["trian_name"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));

		vendor("PHPExcel176.PHPExcel");
		//设定缓存模式为经gzip压缩后存入cache（还有多种方式请百度）
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array();
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($_FILES["trian_name"]["tmp_name"]);

		$arrData = $objPHPExcel->getSheet(0)->toArray();  //内容转换为数组
		array_shift($arrData);   //去掉第

		//$avg = ceil(count($arrData)/8);
		$count_rows = count($arrData);
		$avg = ceil($count_rows/8);
		//dump($arrData);die;

		//==========================选择框开始===============================
		$ttField = array('type_id','term');   //需要转换的字段【选择框】
		$selectValue = $this->getFaqSelectValue();
		//dump($selectValue);die;
		foreach($selectValue as $key=>$val){
			$selectKey[$key] = array_flip($val);
		}

		foreach($selectKey as $key=>$val){
			if(in_array($key,$ttField)){
				$arrK[$key] = $val;
			}
		}
		$selectKey = $arrK;
		//dump($selectKey);die;
		$selectF = array_flip($selectField);
		$i = 0;
		foreach($selectKey as $key=>$val){
			$selectKey[$key]['index'] = $key;
			$i++;
		}
		foreach($selectKey as &$val){
			$val['index'] = $field_key[$val['index']];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $val['index'] = $field_key[$val['index']]+1;
			$selectKey2[$val['index']] = $val;
		}
		foreach($selectKey2 as $val){
			$index[] = $val['index'];
		}


		foreach($arrK as $k=>$vm){
			$cm_select[] = $field_key[$k];  //看$arrData下标是从0还是1开始的 从1开始的的 用 $cm_select[] = $field_key[$k]+1;
		}


		foreach($cm_select as $vm){
			foreach($arrData as &$val){
				$tt = $selectKey2[$vm][$val[$vm]];
				$val[$vm] = $tt;
			}
		}
		//==========================选择框结束==============================



		$sql = "insert into $table($fields) values ";
		$value = "";
		$total = $black = $repeat = $valid = $invalid = 0;
		foreach( $arrData AS $key=>&$val ){

			if(!$val[$field_key["term"]]){
				$val[$field_key["term"]] = "N";
			}


			$str = "(";
			for($i=0;$i<=($count-2);$i++){
				$str .= "'" .str_replace("'","",$val[$i]). "',";
			}
			$str .= "'" .date("Y-m-d H:i:s"). "',";
			$str .= "'" .$username. "'";
			$str .= ")";

			if($count_rows >=8){
				if($key>$avg && $key<=$avg*2){
					$value2 .= empty($value2)?$str:",$str";
				}elseif($key>$avg*2 && $key<=$avg*3){
					$value3 .= empty($value3)?$str:",$str";
				}elseif($key>$avg*3 && $key<=$avg*4){
					$value4 .= empty($value4)?$str:",$str";
				}elseif($key>$avg*4 && $key<=$avg*5){
					$value5 .= empty($value5)?$str:",$str";
				}elseif($key>$avg*5 && $key<=$avg*6){
					$value6.= empty($value6)?$str:",$str";
				}elseif($key>$avg*6 && $key<=$avg*7){
					$value7 .= empty($value7)?$str:",$str";
				}elseif($key>$avg*7 && $key<=$avg*8){
					$value8 .= empty($value8)?$str:",$str";
				}elseif($key>$avg*8 && $key<=$avg*9){
					$value9 .= empty($value9)?$str:",$str";
				}elseif($key<=$avg){
					$value .= empty($value)?$str:",$str";
				}
			}else{
				$value .= empty($value)?$str:",$str";
			}
		}
		//dump($arrData);die;

		//导入动作，执行SQL语句
		$result = false;
		if( $value ){
			$sql .= $value;
			//echo $sql;die;
			$result = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value2 ){
			$sql .= ltrim($value2,",");
			$result2 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value3 ){
			$sql .= ltrim($value3,",");
			$result3 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value4 ){
			$sql .= ltrim($value4,",");
			$result4 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value5 ){
			$sql .= ltrim($value5,",");
			$result5 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value6 ){
			$sql .= ltrim($value6,",");
			$result6 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value7 ){
			$sql .= ltrim($value7,",");
			$result7 = $source->execute($sql);
		}
		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value8 ){
			$sql .= ltrim($value8,",");
			$result8 = $source->execute($sql);
		}

		unset($sql);
		$sql = "insert into $table($fields) values ";
		if( $value9 ){
			$sql .= ltrim($value9,",");
			$result9 = $source->execute($sql);
		}

		//判断导入结果
		if( $result || $result2 || $result3 || $result4 || $result5 || $result6 || $result7 || $result8 || $result9){
			//$total = count($arrPhones);
			$num = $result+$result2+$result3+$result4+$result5+$result6+$result7+$result8+$result9;
			$fail = $count_rows - $num;
			echo json_encode(array('success'=>true,'msg'=>"成功导入${num}条数据！"));
		}else{
			echo json_encode(array('msg'=>'您导入了重复的号码或者导入号码出现未知错误!'));
		}


	}
}

?>
