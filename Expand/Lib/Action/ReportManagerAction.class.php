<?php
/*
报表管理
导出排班相关资料
*/
class ReportManagerAction extends Action{
	//班次报表
	function shiftReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Shift Reports";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$arrF = $this->getTableTitle();

		$i = 0;
		foreach($arrF as $key=>$val){
			$arrField[0][$i]['field'] = $val['fields'];
			$arrField[0][$i]['title'] = $val['display'];
			$arrField[0][$i]['width'] = "100";
			$i++;
		}

		$arrFd = array("field"=>"user_name","title"=>"工号","width"=>"100");
		array_unshift($arrField[0],$arrFd);


		$arrFs = json_encode($arrField);
		$this->assign("fieldList",$arrFs);
		//dump($arrFs);die;

		$this->display();
	}


	function shiftReportData(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_start = Date('Y-m-d',$_GET['ts_start']);
			$date_end = Date('Y-m-d',$_GET['ts_end']);
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1));
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}

		$first_date = date("Y-m-d",mktime(0, 0 , 0,date("m"),1,date("Y")));
		$now_date = date("Y-m-d");

		$where = "1  ";
		$where .= empty($date_start) ? " AND scheduling_dates >= '$first_date'" : " AND scheduling_dates >= '$date_start'";
		$where .= empty($date_end) ? " AND scheduling_dates <= '$now_date'" : " AND scheduling_dates <= '$date_end'";

		$username = $_SESSION['user_info']['username'];
		$pb_scheduling = new Model("pb_scheduling");

		$fields = "";
		$arrF = $this->getTableTitle();
		foreach($arrF as $key=>$val){
			$str = "sum(case when ".$val["name"]."='".str_replace("_","-",$val["value"])."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
		}
		//dump($fields);die;

		$arrData = $pb_scheduling->order("user_name desc")->field("$fields,user_name")->where($where)->group("user_name")->select();
		//echo $pb_scheduling->getLastSql();
		//dump($arrData);die;

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrD = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrData AS &$v){
			if($i >= $start && $j < $length){
				$arrD[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		//dump($arrD);die;

		$rowsList = count($arrD) ? $arrD : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($date_start){
			$arrT["date_start"] = $date_start;
		}
		if($date_end){
			$arrT["date_end"] = $date_end;
		}

		echo json_encode($arrT);
	}

	function getTableTitle(){
		$shift_definition = new Model("pb_shift_definition");
		$arrF = $shift_definition->field("id,shift_name")->select();
		$arrFd = array("id"=>"0","shift_name"=>"休息");
		$arrFd2 = array("id"=>"_1","shift_name"=>"请假");
		$arrFd3 = array("id"=>"_2","shift_name"=>"调班");
		//array_push($arrF,$arrFd);
		array_push($arrF,$arrFd2);
		array_push($arrF,$arrFd3);
		foreach($arrF as $val){
			$arr[] = array(
				"value" => $val["id"],
				"display" => $val["shift_name"],
				"fields" => "shift_name_".$val["id"],
				"name" => "shift_id",
			);
		}

		//dump($arrF);die;
		return $arr;
	}


	//工作流坐席报表【根据工号分类】
	function workFlowAgentReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "WorkFlow Agent Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$arrF = $this->getWorkFlowTitle();

		$i = 0;
		$arrField[0] = array();
		foreach($arrF as $key=>$val){
			$arrField[0][$i]['field'] = $val['fields'];
			$arrField[0][$i]['title'] = $val['display'];
			$arrField[0][$i]['width'] = "100";
			$i++;
		}

		$arrFd = array("field"=>"applicant","title"=>"工号","width"=>"100");
		array_unshift($arrField[0],$arrFd);


		$arrFs = json_encode($arrField);
		$this->assign("fieldList",$arrFs);
		//dump($arrField);die;

		$this->display();
	}

	function getWorkFlowTitle(){
		header("Content-Type:text/html; charset=utf-8");
		$work_process_design = new Model("work_process_design");
		$arrData = $work_process_design->field("id as value,process_name as display")->select();
		$i = 1;
		foreach($arrData as &$val){
			$val["fields"] = "process_name_".$i;
			$val["name"] = "process_design_id";
			$i++;
		}
		unset($i);
		//dump($arrData);die;
		return $arrData;
	}

	function workFlowAgentReportData(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_start = Date('Y-m-d',$_GET['ts_start'])." 00:00:00";
			$date_end = Date('Y-m-d',$_GET['ts_end'])." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1))." 00:00:00";
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}

		$first_date = date("Y-m-d",mktime(0, 0 , 0,date("m"),1,date("Y")))." 00:00:00";  //这个月的第一天
		$now_date = date("Y-m-d H:i:s");
		$search_type = $_REQUEST["search_type"];

		$username = $_SESSION['user_info']['username'];
		$work_workflow_ticket = new Model("work_workflow_ticket");

		$arrF = $this->getWorkFlowTitle();
		$fields = "applicant";
		foreach($arrF as $key=>$val){
			$arrField[] = $val["fields"];
			$arrTitle[] = $val["display"];
			$str = "sum(case when ".$val["name"]."='".$val["value"]."' then 1 else 0 end) as ".$val["fields"];
			$fields .= empty($fields)?"$str":",$str";
		}

		$where = "1 ";
		$where .= empty($date_start) ? " AND create_time >= '$first_date'" : " AND create_time >= '$date_start'";
		$where .= empty($date_end) ? " AND create_time <= '$now_date'" : " AND create_time <= '$date_end'";

		$arrData = $work_workflow_ticket->field($fields)->where($where)->group("applicant")->select();


		if($search_type == "xls"){
			array_unshift($arrField,"applicant");
			array_unshift($arrTitle,"工号");
			$count = count($arrField);
			$excelTiele = "工作流坐席报表".date("Y-m-d");
			$this->exportDataFunction($count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}
		//echo $work_workflow_ticket->getLastSql();die;
		//dump($arrData);die;

		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrD = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrData AS &$v){
			if($i >= $start && $j < $length){
				$arrD[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}

		$rowsList = count($arrD) ? $arrD : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($date_start){
			$arrT["date_start"] = $date_start;
		}
		if($date_end){
			$arrT["date_end"] = $date_end;
		}

		echo json_encode($arrT);

	}




	//工作流流程报表【根据流程分类】
	function workFlowProcessReport(){
		checkLogin();
		//分配增删改的权限
		$menuname = "WorkFlow Process Report";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function workFlowProcessReportData(){
		if( $_GET['ts_start'] && $_GET['ts_end'] ){//js脚本传过来的
			$date_start = Date('Y-m-d',$_GET['ts_start'])." 00:00:00";
			$date_end = Date('Y-m-d',$_GET['ts_end'])." 23:59:59";
			if("lastmonth_start" == $_GET['ts_start']){
				$day = Date('d',$_GET['ts_end']);
				$date_start = Date('Y-m-d',$_GET['ts_end'] - 86400*($day-1))." 00:00:00";
			}
		}else{
			$date_start = $_GET['date_start'];
			$date_end = $_GET['date_end'];
		}

		$first_date = date("Y-m-d",mktime(0, 0 , 0,date("m"),1,date("Y")))." 00:00:00";  //这个月的第一天
		$now_date = date("Y-m-d H:i:s");
		$search_type = $_REQUEST["search_type"];

		$username = $_SESSION['user_info']['username'];
		$work_workflow_ticket = new Model("work_workflow_ticket");

		$where = "1 ";
		$where .= empty($date_start) ? " AND w.create_time >= '$first_date'" : " AND w.create_time >= '$date_start'";
		$where .= empty($date_end) ? " AND w.create_time <= '$now_date'" : " AND w.create_time <= '$date_end'";

		$arrData = $work_workflow_ticket->table("work_workflow_ticket w")->field("w.process_design_id,count(*) as num,p.process_name")->join("work_process_design p on (w.process_design_id = p.id)")->where($where)->group("w.process_design_id")->select();


		if($search_type == "xls"){
			$arrField = array('process_name','num');
			$arrTitle = array('流程名称','数量');
			$count = count($arrField);
			$excelTiele = "工作流流程报表".date("Y-m-d");
			$this->exportDataFunction($count,$arrField,$arrTitle,$arrData,$excelTiele);
			die;
		}

		//echo $work_workflow_ticket->getLastSql();die;


		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrD = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrData AS &$v){
			if($i >= $start && $j < $length){
				$arrD[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		//dump($arrD);die;

		$rowsList = count($arrD) ? $arrD : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;
		if($date_start){
			$arrT["date_start"] = $date_start;
		}
		if($date_end){
			$arrT["date_end"] = $date_end;
		}
		echo json_encode($arrT);
	}


	function exportDataFunction($count,$field,$title,$arrData,$excelTiele){
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Type:text/html;charset=UTF-8");
		$filename = iconv("utf-8","gb2312",$excelTiele);
		header("Content-Disposition: attachment;filename=$excelTiele.xls ");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();
		$start_row	=	0;

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
			xlsWriteLabel($start_row,$i,utf2gb($title[$i]));
		}

		$start_row++;
		foreach($arrData as &$val){
			for($j=0;$j<$count;$j++){
				 xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
			}
			$start_row++;
		}
		xlsEOF();
	}


	function exportDataToPHPExcel($count,$field,$title,$arrData,$excelTiele){
		vendor("PHPExcel176.PHPExcel");
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_serialized;
		$cacheSettings = array('memoryCacheSize'=>'64MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod,$cacheSettings);
		$objPHPExcel = new PHPExcel();

		for($lt=A;$lt<=ZZ;$lt++){
			$tt[] = $lt."1";
			$yy[] = $lt;
		}
		$letters = array_slice($tt,0,$count);
		$letters2 = array_slice($yy,0,$count);
		$lm = $letters2[$count-1];

		// Set properties
		$objPHPExcel->getProperties()->setCreator("ctos")
			->setLastModifiedBy("ctos")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Test result file");

		//设置单元格（列）的宽度 水平居中
		for($n='A';$n<=$lm;$n++){
			$objPHPExcel->getActiveSheet()->getColumnDimension($n)->setWidth(20);
			//$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setWrapText(true);    //自动换行
			$objPHPExcel->getActiveSheet()->getStyle($n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  //水平居中
		}

		//$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');

		//设置表 标题内容
		for($i=0;$i<$count;$i++){
		$objPHPExcel->setActiveSheetIndex()
			->setCellValue($letters[$i], $title[$i]);
		}

		$start_row = 2;
		foreach($arrData as &$val){
			for($j=0;$j<$count;$j++){
				//xlsWriteLabel($start_row,$j,utf2gb($val[$field[$j]]));
				$objPHPExcel->getActiveSheet()->setCellValue($letters2[$j].$start_row, $val[$field[$j]]);
			}
			$start_row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$filename = iconv("utf-8","gb2312",$excelTiele);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'('.date('Y-m-d').').xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}

	//导出答卷
	function answersData(){
		header("Content-Type:text/html;charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$wj_questionnaire_answers = new Model("wj_questionnaire_answers_".$task_id);
		$task_id = $_REQUEST["task_id"];
		$customer_id = $_REQUEST["customer_id"];
		$answer_class = $_REQUEST["answer_class"];
		$answer_code = $_REQUEST["answer_code"];
		$search_type = $_REQUEST["search_type"];

		$fields = "a.id,a.create_time,a.create_user,a.questionnaire_generation_id,a.questionnaire_type_id,a.answer_code,a.task_id,a.customer_id,a.answer_class,g.questionnaire_name,g.whether_to_audit,g.questionnaire_description,g.topics_ids,t.questionnaire_type_name,t.questionnaire_type_description,s.name,s.sex,s.phone1";

		$where = "1 ";
		if($task_id != "cm"){
			$where .= empty($task_id) ? "" : " AND a.task_id = '$task_id'";
			$cm_table = "sales_source_$task_id";
		}else{
			$cm_table = "customer";
		}
		$where .= empty($customer_id) ? "" : " AND a.customer_id = '$customer_id'";
		$where .= empty($answer_class) ? "" : " AND a.answer_class = '$answer_class'";
		$where .= empty($answer_code) ? "" : " AND a.answer_code like '%$answer_code%'";

		$arrData = $wj_questionnaire_answers->order("a.create_time desc")->table("wj_questionnaire_answers_$task_id a")->field($fields)->join("wj_questionnaire_generation g on (a.questionnaire_generation_id = g.id)")->join("wj_questionnaire_type t on (a.questionnaire_type_id = t.id)")->join("$cm_table s on (a.customer_id = s.id)")->where($where)->select();


		$answer_class_row = array("1"=>"有效答卷","2"=>"无效答卷","3"=>"删除的答卷");
		foreach($arrData as &$val){
			$answer_class = $answer_class_row[$val["answer_class"]];
			$val["answer_class"] = $answer_class;

			$val["content"] = $this->answersDetailData($task_id,$val["id"]);

			$arrT[] = "客户姓名：".$val["name"]." 号码：".$val["phone1"]."  答卷人：".$val["create_user"]." \r\n问卷内容：\r\n".$val["content"];
		}

		//dump($arrT);die;
		$filename = "问卷答卷".date("Y-m-d");
		$content = implode("\r\n\r\n",$arrT);
		$this->exportText($filename,$content);
		die;
	}

	function exportText($filename,$content){
		Header( "Content-type:   application/octet-stream ");
		Header( "Accept-Ranges:   bytes ");
		header( "Content-Disposition:   attachment;   filename=$filename.txt ");
		header( "Expires:   0 ");
		header( "Cache-Control:   must-revalidate,   post-check=0,   pre-check=0 ");
		header( "Pragma:   public ");
		echo $content;
		die;
	}


	function answersDetailData($task_id,$questionnaire_answers_id){
		//header("Content-Type:text/html; charset=utf-8");
		$wj_questionnaire_answers_detail = new Model("wj_questionnaire_answers_detail_".$task_id);

		$fields = "d.questionnaire_answers_id,d.questionnaire_exam_id,d.questionnaire_topic_id,d.question_type,d.fill_answer,a.create_user,a.questionnaire_generation_id,a.questionnaire_type_id,b.questionnaire_topics,count(*) as total";

		$where = "1 ";
		$where .= empty($task_id) ? "" : " AND a.task_id = '$task_id'";
		$where .= empty($questionnaire_answers_id) ? "" : " AND d.questionnaire_answers_id in ($questionnaire_answers_id)";
		//dump($topics_ids);die;

		$arrData = $wj_questionnaire_answers_detail->table("wj_questionnaire_answers_detail_$task_id d")->field($fields)->join("wj_questionnaire_answers_$task_id a on (d.questionnaire_answers_id = a.id)")->join("wj_questionnaire_bank b on (d.questionnaire_exam_id = b.id)")->where($where)->group("d.questionnaire_exam_id,d.fill_answer")->select();
		//echo $wj_questionnaire_answers_detail->getLastSql();die;

		$arrP = getQuestionnaireBankContent();
		$arrQn = getQuestionnaireIDName();

		foreach($arrData as &$val){
			if($val["question_type"] == "1" || $val["question_type"] == "3" ){
				$val["name"] = $val["fill_answer"]."：".$arrP[$val["questionnaire_exam_id"]][$val["fill_answer"]];
			}elseif($val["question_type"] == "2"){
				$val["answers"] = explode(" ",$val["fill_answer"]);
				foreach($val["answers"] as $vm){
					$val["name"] .= $vm."：".$arrP[$val["questionnaire_exam_id"]][$vm]."   ";
				}
			}elseif($val["question_type"] == "6"){
				for($i=0;$i<strlen($val["fill_answer"]);$i++){
					$val["answers"][] = substr($val["fill_answer"],$i,1);
				}
				foreach($val["answers"] as $vm){
					$val["name"] .= $vm."：".$arrP[$val["questionnaire_exam_id"]][$vm]."  ";
				}
			}else{
				$val["name"] = $val["fill_answer"];
			}
			$val["questionnaire_name"] = $arrQn[$val["questionnaire_generation_id"]];  //问卷名称

			$arrF[] = "问题：".$val["questionnaire_topics"]."   答案：".$val["name"]."\r\n";
		}
		$string = implode("\n",$arrF);
		//dump($string);die;
		return $string;

	}

}
?>
