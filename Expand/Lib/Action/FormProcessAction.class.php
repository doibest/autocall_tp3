<?php
/*
工作流设置
表单设计
分类设计
*/
class FormProcessAction extends Action{
	//表单分类
	function formCategoryLst(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Form Classification";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$this->display();
	}


	function categoryData(){
		$category = new Model("work_form_category");
		import('ORG.Util.Page');
		$count = $category->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $category->order("sort_orderid desc")->field("id,category_name as text,category_pid,sort_orderid,category_enabled,dept_id")->limit($page->firstRow.','.$page->listRows)->select();


		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = readU();
		$arrDeptName = $arrF["deptId_name"];
		$arrDeptName[0] = "所有部门";

		/*
		if(file_exists("BGCC/Conf/crm/$db_name/formNum.php")){
			$arrForm = require "BGCC/Conf/crm/$db_name/formNum.php";
		}else{
			$arrForm = array();
		}
		*/
		$arrForm = getFormNum();

		$i = 0;
		$row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			if($arrForm[$val["id"]]){
				$val["form_num"] = $arrForm[$val["id"]];
			}else{
				$val["form_num"] = "0";
			}
			$type = $row[$val['category_enabled']];
			$val['category_enabled2'] = $type;
			$arrData[$i]['state'] = "closed";
			$arrData[$i]['dept_name'] = $arrDeptName[$val["dept_id"]];
			$i++;
		}
		unset($i);

		//dump($arrDeptName);die;

		$arrTree = $this->getTree($arrData,0,"category_pid","id");

		$j = 0;
		foreach($arrTree as $v){
			//if( $arrTree[$j]["category_pid"] == "0" && !$arrTree[$j]["children"]){
			if( $arrTree[$j]["category_pid"] == "0"){
				$arrTree[$j]['iconCls'] = "shop";  //顶级分类的图标，叶子节点的图标在getTree()方法中设置。
			}
			$j++;
		}
		unset($j);

		/*
		//打开时只有顶级部门中 有子部门的 是闭合状态，其他的都是是打开状态
		$i = 0;
		foreach($arrTree as $v){
			if( is_array($arrTree[$i]['children'])==true ){
				$arrTree[$i]['state'] = "closed";
			}
			$i++;
		}
		unset($i);
		*/

		//dump($arrTree);die;
		//$strJSON = json_encode($arrTree);
		//echo ($strJSON);

		$rowsList = count($arrTree) ? $arrTree : false;
		$arrCat["total"] = $count;
		$arrCat["rows"] = $rowsList;
		//dump($arrTree);die;
		echo json_encode($arrCat);
	}

	function insertCategory(){
		$category = new Model("work_form_category");
		$arrData = array(
			"category_name" => $_REQUEST["text"],
			"category_pid" => $_REQUEST["category_pid"],
			"sort_orderid" => $_REQUEST["sort_orderid"],
			"dept_id" => $_REQUEST["dept_id"]
		);
		$result = $category->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'分类添加成功！'));
		} else {
			echo json_encode(array('msg'=>'分类添加失败！'));
		}
	}

	function updateCategory(){
		$id = $_REQUEST['id'];
		$category = new Model("work_form_category");
		$arrData = array(
			"category_name" => $_REQUEST["text"],
			"category_pid" => $_REQUEST["category_pid"],
			"sort_orderid" => $_REQUEST["sort_orderid"],
			"dept_id" => $_REQUEST["dept_id"]
		);
		$result = $category->data($arrData)->where("id = $id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteCategory(){
		$id = $_REQUEST["id"];
		$category = new Model("work_form_category");
		$result = $category->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function getTree($data, $pId,$field_pid,$field_id) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v[$field_pid] == $pId)    {
				$v['children'] = $this->getTree($data, $v[$field_id],$field_pid,$field_id);

				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				/*
				if ( empty($v["children"]) && $v['state'] =='closed'){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}
				if ( $v["children"]  && $v['state'] =='closed'){
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				*/
				if ( empty($v["children"])){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}else{
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }


	function cateTree(){
		$category = new Model("work_form_category");

		$categoryData = $category->order("sort_orderid desc")->field("id,category_name as text,category_pid,category_enabled")->select();
		$nodeStatus = $_GET['nodeStatus'];
		if($nodeStatus == 'colse'){
			$i = 0;
			foreach($categoryData as $v){
				$categoryData[$i]['state'] = "closed";
				$i++;
			}
			unset($i);
		}

		$arrTree = $this->getTree($categoryData,0,"category_pid","id");
		if( $nodeStatus != 'other' && $nodeStatus !== "fitting"){
			$allCate = array(
				"text"=>"顶级分类",
				"id"=>"0",
				"category_pid"=>"0",
				"iconCls"=>"shop",
			);
			array_unshift($arrTree,$allCate);
		}
		if($nodeStatus == "fitting"){
			$allCate = array(
				"text"=>"所有分类",
				"id"=>"0",
				"category_pid"=>"0",
				"iconCls"=>"shop",
			);
			array_unshift($arrTree,$allCate);
		}

		if($nodeStatus == "open"){
			$j = 0;
			foreach($arrTree as $v){
				//if( $arrTree[$j]["category_pid"] == "0" && !$arrTree[$j]["children"]){
				if( $arrTree[$j]["category_pid"] == "0"){
					$arrTree[$j]['iconCls'] = "shop";
				}
				$j++;
			}
			unset($j);
		}
		//dump($categoryData);
		$arr = array("id"=>"","text"=>"请选择...");
		array_unshift($arrTree,$arr);
		echo  json_encode($arrTree);
	}



	//流程分类
	function processCategoryLst(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Process Classification";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$this->display();
	}

	function processCategoryData(){
		$category = new Model("work_process_category");
		import('ORG.Util.Page');
		$count = $category->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $category->order("sort_orderid desc")->field("id,category_name as text,category_pid,sort_orderid,category_enabled,dept_id")->limit($page->firstRow.','.$page->listRows)->select();

		//dump($arrData);die;

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = readU();
		$arrDeptName = $arrF["deptId_name"];
		$arrDeptName[0] = "所有部门";

		if(file_exists("BGCC/Conf/crm/$db_name/processNum.php")){
			$arrProcess = require "BGCC/Conf/crm/$db_name/processNum.php";
		}else{
			$arrProcess = array();
		}

		$i = 0;
		$row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			if($arrProcess[$val["id"]]){
				$val["form_num"] = $arrProcess[$val["id"]];
			}else{
				$val["form_num"] = "0";
			}
			$type = $row[$val['category_enabled']];
			$val['category_enabled2'] = $type;
			$arrData[$i]['state'] = "closed";
			$arrData[$i]['dept_name'] = $arrDeptName[$val["dept_id"]];
			$i++;
		}
		unset($i);

		$arrTree = $this->getTree($arrData,0,"category_pid","id");

		$j = 0;
		foreach($arrTree as $v){
			//if( $arrTree[$j]["category_pid"] == "0" && !$arrTree[$j]["children"]){
			if( $arrTree[$j]["category_pid"] == "0"){
				$arrTree[$j]['iconCls'] = "shop";  //顶级分类的图标，叶子节点的图标在getTree()方法中设置。
			}
			$j++;
		}
		unset($j);

		/*
		//打开时只有顶级部门中 有子部门的 是闭合状态，其他的都是是打开状态
		$i = 0;
		foreach($arrTree as $v){
			if( is_array($arrTree[$i]['children'])==true ){
				$arrTree[$i]['state'] = "closed";
			}
			$i++;
		}
		unset($i);
		*/

		//dump($arrTree);die;
		//$strJSON = json_encode($arrTree);
		//echo ($strJSON);

		$rowsList = count($arrTree) ? $arrTree : false;
		$arrCat["total"] = $count;
		$arrCat["rows"] = $rowsList;
		//dump($arrTree);die;
		echo json_encode($arrCat);
	}

	function insertProcessCategory(){
		$category = new Model("work_process_category");
		$arrData = array(
			"category_name" => $_REQUEST["text"],
			"category_pid" => $_REQUEST["category_pid"],
			"sort_orderid" => $_REQUEST["sort_orderid"],
			"dept_id" => $_REQUEST["dept_id"]
		);
		$result = $category->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'分类添加成功！'));
		} else {
			echo json_encode(array('msg'=>'分类添加失败！'));
		}
	}

	function updateProcessCategory(){
		$id = $_REQUEST['id'];
		$category = new Model("work_process_category");
		$arrData = array(
			"category_name" => $_REQUEST["text"],
			"category_pid" => $_REQUEST["category_pid"],
			"sort_orderid" => $_REQUEST["sort_orderid"],
			"dept_id" => $_REQUEST["dept_id"]
		);
		$result = $category->data($arrData)->where("id = $id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteProcessCategory(){
		$id = $_REQUEST["id"];
		$category = new Model("work_process_category");
		$result = $category->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}



	function cateProcessTree(){
		$category = new Model("work_process_category");

		$categoryData = $category->order("sort_orderid desc")->field("id,category_name as text,category_pid,category_enabled")->select();
		$nodeStatus = $_GET['nodeStatus'];
		if($nodeStatus == 'colse'){
			$i = 0;
			foreach($categoryData as $v){
				$categoryData[$i]['state'] = "closed";
				$i++;
			}
			unset($i);
		}

		$arrTree = $this->getTree($categoryData,0,"category_pid","id");
		if( $nodeStatus != 'other' && $nodeStatus !== "fitting"){
			$allCate = array(
				"text"=>"顶级分类",
				"id"=>"0",
				"category_pid"=>"0",
				"iconCls"=>"shop",
			);
			array_unshift($arrTree,$allCate);
		}
		if($nodeStatus == "fitting"){
			$allCate = array(
				"text"=>"所有分类",
				"id"=>"0",
				"category_pid"=>"0",
				"iconCls"=>"shop",
			);
			array_unshift($arrTree,$allCate);
		}

		if($nodeStatus == "open"){
			$j = 0;
			foreach($arrTree as $v){
				//if( $arrTree[$j]["category_pid"] == "0" && !$arrTree[$j]["children"]){
				if( $arrTree[$j]["category_pid"] == "0"){
					$arrTree[$j]['iconCls'] = "shop";
				}
				$j++;
			}
			unset($j);
		}
		//dump($categoryData);
		$arr = array("id"=>"","text"=>"请选择...");
		array_unshift($arrTree,$arr);
		echo  json_encode($arrTree);
	}

	//设计表单
	function designFormList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Design Form";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function designFormData(){
		$username = $_SESSION['user_info']['username'];
		$work_form_design = new Model("work_form_design");

		$fields = "d.id,d.create_time,d.create_user,d.dept_id,d.form_name,d.form_category_id,d.form_description,c.category_name";

		$count = $work_form_design->table("work_form_design d")->field($fields)->join("work_form_category c on (d.form_category_id = c.id)")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $work_form_design->table("work_form_design d")->field($fields)->join("work_form_category c on (d.form_category_id = c.id)")->limit($page->firstRow.','.$page->listRows)->select();

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = readU();
		$arrDeptName = $arrF["deptId_name"];
		$arrDeptName[0] = "所有部门";
		foreach($arrData as &$val){
			$val["dept_name"] = $arrDeptName[$val["dept_id"]];
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	function addDesignForm(){
		checkLogin();
		$this->display();
	}

	function insertDesignForm(){
		$username = $_SESSION['user_info']['username'];
		$work_form_design = new Model("work_form_design");
		$arrData = array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'form_name'=>$_REQUEST['form_name'],
			'form_category_id'=>$_REQUEST['form_category_id'],
			'dept_id'=>$_REQUEST['dept_id'],
			'form_description'=>$_REQUEST['form_description']
		);
		$result = $work_form_design->data($arrData)->add();
		if ($result){
			//创建对应的表单
			$tableName = "work_form_$result";
			$res = $work_form_design->execute("create table $tableName like work_form");
			//dump($res);die;
			if($res !== false){
				$sql = "INSERT INTO `custom_fields` (`table_name`,`cn_name`,`en_name`,`field_attr`,`field_order`,`text_type`,`field_width`,`field_enabled`,`list_enabled`,`required_enabled`,`tiptools_enabled`,`field_values`) VALUES ('$tableName','申请人工号','create_user','varchar','1','1','50','Y','Y','N','N','[[\"\",\"\",\"Y\"]]'),('$tableName','申请人部门','dept_id','int','2','1','11','Y','Y','N','N','[[\"\",\"\",\"Y\"]]')";
				$work_form_design->execute($sql);
			}
			//$this->fieldCache();
			//$this->formNumCahce();
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function formNumCahce(){
		$work_form_design = new Model("work_form_design");
		$arrData = $work_form_design->field("form_category_id,count(*) as num")->group("form_category_id")->select();
		foreach($arrData as $val){
			$arrF[$val["form_category_id"]] = $val["num"];
		}
		//dump($arrF);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("formNum_$db_name",$arrF,"BGCC/Conf/crm/");
		F("formNum",$arrF,"BGCC/Conf/crm/$db_name/");
	}

	function fieldCache(){
		//header("Content-Type:text/html; charset=utf-8");
		$custom_fields = new Model("custom_fields");
		$fieldData = $custom_fields->order("field_order asc")->select();
		$arrData = $this->groupBy($fieldData,"table_name");
		//dump($arrData);die;

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("order_fieldCache_$db_name",$arrData,"BGCC/Conf/crm/");
		F("order_fieldCache",$arrData,"BGCC/Conf/crm/$db_name/");
	}

	function groupBy($arr, $key_field){
		$ret = array();
		foreach ($arr as $row){
			$key = $row[$key_field];
			$ret[$key][] = $row;
		}
		return $ret;
	}

	function editDesignForm(){
		checkLogin();
		$id = $_REQUEST["id"];
		$work_form_design = new Model("work_form_design");
		$arrData = $work_form_design->where("id = '$id'")->find();

		$this->assign("id",$id);
		$this->assign("arrData",$arrData);

		$this->display();
	}

	function updateDesignForm(){
		$id = $_REQUEST['id'];
		$work_form_design = new Model("work_form_design");
		$arrData = array(
			'form_name'=>$_REQUEST['form_name'],
			'form_category_id'=>$_REQUEST['form_category_id'],
			'dept_id'=>$_REQUEST['dept_id'],
			'form_description'=>$_REQUEST['form_description']
		);
		$result = $work_form_design->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteDesignForm(){
		$id = $_REQUEST["id"];
		$work_form_design = new Model("work_form_design");
		$result = $work_form_design->where("id in ($id)")->delete();
		if ($result){
			//$this->formNumCahce();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//查看表单
	function viewForm(){
		$d_id = $_SESSION['user_info']['d_id'];
		$form_id = $_REQUEST["form_id"];
		$table_name = "work_form_$form_id";

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrFields = require "BGCC/Conf/crm/$db_name/order_fieldCache.php";
		$arrFields = getFormField();
		$cmFields = $arrFields[$table_name];
		//dump($cmFields);die;
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['field_enabled'] == 'Y'){
				$fielddTpl[] = $val;
			}
		}
		$this->assign("fielddTpl",$fielddTpl);

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = readU();
		$arrDeptName = $arrF["deptId_name"];
		$arrDeptName[0] = "所有部门";
		$dept_name = $arrDeptName[$d_id];

		$this->assign("d_id",$d_id);
		$this->assign("dept_name",$dept_name);

		$this->display();
	}

	function array_sort($arr,$keys,$type='asc',$old_key="yes"){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			if($old_key == "yes"){
				$new_array[$k] = $arr[$k];
			}else{
				$new_array[] = $arr[$k];
			}
		}
		return $new_array;
	}

	//获取表单分类下的表单
	function getForm(){
		//header("Content-Type:text/html; charset=utf-8");
		$work_form_category=new Model("work_form_category");
		$arrData = $work_form_category->field("category_name as text,id as tid,category_pid as pid")->select();

		$work_form_design = new Model("work_form_design");
		$arrF = $work_form_design->order("create_time desc")->field("id,form_name as text,form_category_id as pid")->select();

		$j = 0;
		foreach($arrData as $vm){
			$arrData[$j]['attributes'] = "pid";
			$arrData[$j]['iconCls'] = "icon-files";
			$arrData[$j]['state'] = "closed";
			$j++;
		}

		foreach($arrF as &$val){
			$val["attributes"] = "son_id";
			$arr[$val["pid"]][] = $val;
			array_push($arrData,$val);
		}

        //$arrTree = $this->getTree($arrData,0);
        $arrTree = $this->getTree($arrData,0,"pid","tid");


		$arr = array("id"=>"","text"=>"请选择...");
		array_unshift($arrTree,$arr);
		//dump($arrTree);die;
		echo  json_encode($arrTree);
	}

	//获取表单分类下的流程
	function getProcessDesign(){
		//header("Content-Type:text/html; charset=utf-8");
		$work_form_category=new Model("work_form_category");
		$arrData = $work_form_category->field("category_name as text,id as tid,category_pid as pid")->select();

		/*
		$work_form_design = new Model("work_form_design");
		$arrF = $work_form_design->order("create_time desc")->field("id,form_name as text,form_category_id as pid")->select();
		*/

		$work_process_design = M("work_process_design");
		$arrF = $work_process_design->order("p.create_time desc")->table("work_process_design p")->field("f.id,p.id as process_design_id,p.process_name as text,f.form_name,f.form_category_id as pid")->join("work_form_design f on (p.form_design_id = f.id)")->select();

		//echo $work_process_design->getLastSql();
		//dump($arrF);die;

		$j = 0;
		foreach($arrData as $vm){
			$arrData[$j]['attributes'] = "pid";
			$arrData[$j]['iconCls'] = "icon-files";
			$arrData[$j]['state'] = "closed";
			$j++;
		}

		foreach($arrF as &$val){
			//$val["attributes"] = "son_id";
			$val["attributes"] = $val["process_design_id"];
			$arr[$val["pid"]][] = $val;
			array_push($arrData,$val);
		}

        //$arrTree = $this->getTree($arrData,0);
        $arrTree = $this->getTree($arrData,0,"pid","tid");


		$arr = array("id"=>"","text"=>"请选择...");
		array_unshift($arrTree,$arr);
		//dump($arrTree);die;
		echo  json_encode($arrTree);
	}


	//流程设计
	function designProcessList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Design Flow";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function designProcessData(){
		$username = $_SESSION['user_info']['username'];
		$work_process_design = new Model("work_process_design");

		$fields = "p.id,p.create_time,p.create_user,p.dept_id,p.process_name,p.form_design_id,p.process_category_id,p.process_type,p.process_sort,p.allow_attachment,p.allow_circulated,p.circulated_people,p.process_description,p.filename,p.native_name,f.form_name,c.category_name";

		$count = $work_process_design->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $work_process_design->table("work_process_design p")->field($fields)->join("work_form_design f on (p.form_design_id = f.id)")->join("work_process_category c on (p.process_category_id = c.id)")->limit($page->firstRow.','.$page->listRows)->select();
		//echo $work_process_design->getLastSql();die;


		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrF = readU();
		$arrDeptName = $arrF["deptId_name"];
		$arrDeptName[0] = "所有部门";

		foreach($arrData as &$val){
			$val["dept_name"] = $arrDeptName[$val["dept_id"]];
		}

		//dump($arrData);die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addDesignProcess(){
		checkLogin();
		$this->display();
	}

	function insertDesignProcess(){
		$username = $_SESSION['user_info']['username'];

		$file_path = "include/data/workflow/";
		$this->mkdirs($file_path);

		//判断文件类型
		$tmp_file = $_FILES["filename"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["filename"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));
		$arrExt = array("php","sql");
		if( in_array($suffix,$arrExt)){
			echo json_encode(array('msg'=>"上传文件类型不允许！"));
			die;
		}

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$upload->savePath= $file_path;  //上传路径
		$upload->saveRule="uniqid";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖

		//dump($suffix);die;
		$work_process_design = new Model("work_process_design");
		$arrT = $_REQUEST['circulated_people'];
		$arrF = explode(",",$arrT);
		$circulated_people = json_encode($arrF);
		//dump($_REQUEST);die;
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = array(
					'create_time'=>date("Y-m-d H:i:s"),
					'create_user'=>$username,
					'process_name'=>$_REQUEST['process_name'],
					'form_design_id'=>$_REQUEST['form_design_id'],
					'dept_id'=>$_REQUEST['dept_id'],
					'process_category_id'=>$_REQUEST['process_category_id'],
					'process_sort'=>$_REQUEST['process_sort'],
					'allow_attachment'=>$_REQUEST['allow_attachment'],
					'allow_circulated'=>$_REQUEST['allow_circulated'],
					'circulated_people'=>$circulated_people,
					'process_description'=>$_REQUEST['process_description'],
				);
				$result = $work_process_design->data($arrData)->add();
				if ($result){
					$this->processNumCahce();
					echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
				} else {
					echo json_encode(array('msg'=>'添加失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData = array(
				'create_time'=>date("Y-m-d H:i:s"),
				'create_user'=>$username,
				'process_name'=>$_REQUEST['process_name'],
				'form_design_id'=>$_REQUEST['form_design_id'],
				'dept_id'=>$_REQUEST['dept_id'],
				'process_category_id'=>$_REQUEST['process_category_id'],
				'process_sort'=>$_REQUEST['process_sort'],
				'allow_attachment'=>$_REQUEST['allow_attachment'],
				'allow_circulated'=>$_REQUEST['allow_circulated'],
				'circulated_people'=>$circulated_people,
				'process_description'=>$_REQUEST['process_description'],
				'filename'=>$info[0]["savepath"]."/".$info[0]["savename"],
				'native_name'=>$info[0]["name"],
			);
			$result = $work_process_design->data($arrData)->add();
			if ($result){
					$this->processNumCahce();
				echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
			} else {
				echo json_encode(array('msg'=>'添加失败！'));
			}
		}
	}

	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}

	function editDesignProcess(){
		checkLogin();
		$id = $_REQUEST["id"];
		$work_process_design = new Model("work_process_design");
		$arrData = $work_process_design->where("id = '$id'")->find();

		$this->assign("arrData",$arrData);
		$this->assign("id",$id);

		$this->display();

	}

	function updateDesignProcess(){
		$id = $_REQUEST["id"];
		$username = $_SESSION['user_info']['username'];

		$file_path = "include/data/workflow/";
		$this->mkdirs($file_path);

		//判断文件类型
		$tmp_file = $_FILES["filename"]["tmp_name"];
		$tmpArr = explode(".",$_FILES["filename"]["name"]);
		$suffix = strtolower(array_pop($tmpArr));
		$arrExt = array("php","sql");
		if( in_array($suffix,$arrExt)){
			echo json_encode(array('msg'=>"上传文件类型不允许！"));
			die;
		}

		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		$upload->savePath= $file_path;  //上传路径
		$upload->saveRule="uniqid";    //上传文件的文件名保存规则  time uniqid  com_create_guid  uniqid
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖

		//dump($suffix);die;
		$work_process_design = new Model("work_process_design");
		$arrP = $work_process_design->where("id = '$id'")->find();
		$arrT = $_REQUEST['circulated_people'];
		$arrF = explode(",",$arrT);
		$circulated_people = json_encode($arrF);
		//dump($_REQUEST);die;
		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = array(
					'process_name'=>$_REQUEST['process_name'],
					'form_design_id'=>$_REQUEST['form_design_id'],
					'dept_id'=>$_REQUEST['dept_id'],
					'process_category_id'=>$_REQUEST['process_category_id'],
					'process_sort'=>$_REQUEST['process_sort'],
					'allow_attachment'=>$_REQUEST['allow_attachment'],
					'allow_circulated'=>$_REQUEST['allow_circulated'],
					'circulated_people'=>$circulated_people,
					'process_description'=>$_REQUEST['process_description'],
				);
				$result = $work_process_design->data($arrData)->where("id = '$id'")->save();
				if ($result !== false){
					echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
				} else {
					echo json_encode(array('msg'=>'更新失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrData = array(
				'process_name'=>$_REQUEST['process_name'],
				'form_design_id'=>$_REQUEST['form_design_id'],
				'dept_id'=>$_REQUEST['dept_id'],
				'process_category_id'=>$_REQUEST['process_category_id'],
				'process_sort'=>$_REQUEST['process_sort'],
				'allow_attachment'=>$_REQUEST['allow_attachment'],
				'allow_circulated'=>$_REQUEST['allow_circulated'],
				'circulated_people'=>$circulated_people,
				'process_description'=>$_REQUEST['process_description'],
				'filename'=>$info[0]["savepath"]."/".$info[0]["savename"],
				'native_name'=>$info[0]["name"],
			);
			$result = $work_process_design->data($arrData)->where("id = '$id'")->save();
			if ($result !== false){
				if($arrP["filename"]){
					unlink($arrP["filename"]);
				}
				echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}
	}

	function deleteDesignProcess(){
		$id = $_REQUEST["id"];
		$work_process_design = new Model("work_process_design");
		$arrP = $work_process_design->where("id = '$id'")->find();
		$result = $work_process_design->where("id in ($id)")->delete();
		if ($result){
			$this->processNumCahce();
			if($arrP["filename"]){
				unlink($arrP["filename"]);
			}
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	function processNumCahce(){
		$work_process_design = new Model("work_process_design");
		$arrData = $work_process_design->field("process_category_id,count(*) as num")->group("process_category_id")->select();
		foreach($arrData as $val){
			$arrF[$val["process_category_id"]] = $val["num"];
		}
		//dump($arrF);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("processNum_$db_name",$arrF,"BGCC/Conf/crm/");
		F("processNum",$arrF,"BGCC/Conf/crm/$db_name/");
	}

	//流程步骤
	function designFlowList(){
		header("Content-Type:text/html; charset=utf-8");
		checkLogin();
		$process_design_id = $_REQUEST["process_design_id"];
		$this->assign("process_design_id",$process_design_id);

		$work_process_design = new Model("work_process_design");
		$arrF = $work_process_design->where("id='$process_design_id'")->find();

		$process_name = $_REQUEST["process_name"];
		$this->assign("process_name",$arrF["process_name"]);
		//dump($arrF["process_name"]);die;
		$form_design_id = $_REQUEST["form_design_id"];
		$this->assign("form_design_id",$form_design_id);

		//分配增删改的权限
		$menuname = "Design Flow";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function designFlowData(){
		$username = $_SESSION['user_info']['username'];
		$work_process_design_node = new Model("work_process_design_node");

		$fields = "n.id,n.create_user,n.create_time,n.process_design_id,n.order_num,n.step_name,n.next_step_num,n.writable_fields,n.authorized_personnel,n.authorize_dept_ids,n.authorize_role_id,p.process_name,p.form_design_id,p.process_category_id,p.process_type,p.process_sort,p.allow_attachment,p.allow_circulated,p.circulated_people,p.process_description,p.filename,p.native_name";

		$process_design_id = $_REQUEST["process_design_id"];
		$where = "1 ";
		$where .= empty($process_design_id) ? "" : " AND n.process_design_id = '$process_design_id'";
		$count = $work_process_design_node->table("work_process_design_node n")->field($fields)->join("work_process_design p on (n.process_design_id = p.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $work_process_design_node->order("n.order_num asc")->table("work_process_design_node n")->field($fields)->join("work_process_design p on (n.process_design_id = p.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();

		$arrF = array(
			"order_num"=>"",
			"step_name"=>"[结束流程]",
			"next_step_num"=>"",
		);
		if($arrData){
			array_push($arrData,$arrF);
		}
		//dump($arrData);die;


		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	function addDesignFlow(){
		checkLogin();
		$process_design_id = $_REQUEST["process_design_id"];
		$this->assign("process_design_id",$process_design_id);

		$process_name = $_REQUEST["process_name"];
		$this->assign("process_name",$process_name);
		/*
		$work_process_design = new Model("work_process_design");
		$arrData = $work_process_design->where("id = '$process_design_id'")->find();
		$form_id = $arrData["form_design_id"];
		*/
		$form_id = $_REQUEST["form_design_id"];
		$table_name = "work_form_$form_id";
		//$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrFields = require "BGCC/Conf/crm/$db_name/order_fieldCache.php";
		$arrFields = getFormField();
		$cmFields = $arrFields[$table_name];
		//dump($cmFields);die;
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['field_enabled'] == 'Y'){
				$fielddTpl[] = $val;
			}
		}
		$this->assign("fielddTpl",$fielddTpl);

		$this->display();
	}

	function insertDesignFlow(){
		$username = $_SESSION['user_info']['username'];
		$work_process_design_node = new Model("work_process_design_node");

		$arrTR = $_REQUEST['authorize_role_id'];
		$arrFR = explode(",",$arrTR);
		$authorize_role_id = json_encode($arrFR);

		$arrTD = $_REQUEST['authorize_dept_ids'];
		$arrFD = explode(",",$arrTD);
		$authorize_dept_ids = json_encode($arrFD);

		$arrTP = $_REQUEST['authorized_personnel'];
		$arrFP = explode(",",$arrTP);
		$authorized_personnel = json_encode($arrFP);


		$field = $_REQUEST["field"];
		$customer_fields = json_encode($field);

		$users = new Model("users");
		$para_sys = readS();
		$where = "1 ";
		if($para_sys["collection_types"] == "and"){
			if($arrTP){
				$user = "'".implode("','",$arrFP)."'";
				$where .= " AND username in ($user) ";
			}
			if($arrTD){
				$dept_id = "'".implode("','",$arrFD)."'";
				$where .= " AND d_id in ($dept_id) ";
			}
			if($arrTR){
				$rid = "'".implode("','",$arrFR)."'";
				$where .= " AND r_id in ($rid) ";
			}
		}else{
			if($arrTP){
				$user = "'".implode("','",$arrFP)."'";
				$where .= " OR username in ($user) ";
			}
			if($arrTD){
				$dept_id = "'".implode("','",$arrFD)."'";
				$where .= " OR d_id in ($dept_id) ";
			}
			if($arrTR){
				$rid = "'".implode("','",$arrFR)."'";
				$where .= " OR r_id in ($rid) ";
			}
		}
		$arrAW = $users->field("username,d_id,r_id")->where($where)->select();
		foreach($arrAW as $val){
			$arrU[] = $val["username"];
		}
		$authorized_work_num = json_encode($arrU);
		//dump($arrAW);die;

		$arrData = array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'order_num'=>$_REQUEST['order_num'],
			'step_name'=>$_REQUEST['step_name'],
			'next_step_num'=>$_REQUEST['next_step_num'],
			'authorize_role_id'=>$authorize_role_id,
			'authorize_dept_ids'=>$authorize_dept_ids,
			'authorized_personnel'=>$authorized_personnel,
			'writable_fields'=>$customer_fields,
			'authorized_work_num'=>$authorized_work_num,
			'process_design_id'=>$_REQUEST['process_design_id']
		);
		//dump($field);die;
		$process_design_id = $_REQUEST['process_design_id'];
		$count = $work_process_design_node->where("process_design_id = '$process_design_id'")->count();

		//dump($count);die;

		$result = $work_process_design_node->data($arrData)->add();
		if ($result){
			if($count == "0"){
				$work_process_design = new Model("work_process_design");
				$res = $work_process_design->where("id='$process_design_id'")->save(array("first_node_order_num"=>$_REQUEST['order_num'],"first_node_id"=>$result));
			}
			//$this->customerFieldCache();
			if($arrTP){
				$sql = "insert into work_process_design_node_role(design_node_id,user_name) values";
				$value = "";
				foreach( $arrFP AS $val ){
					$str = "(";
					$str .= "'" .$result. "',";
					$str .= "'" .$val. "'";
					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $work_process_design_node->execute($sql);
				}
			}
			unset($sql);
			unset($value);
			if($arrTD){
				$sql = "insert into work_process_design_node_role(design_node_id,dept_id) values";
				$value = "";
				foreach( $arrFD AS $val ){
					$str = "(";
					$str .= "'" .$result. "',";
					$str .= "'" .$val. "'";
					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $work_process_design_node->execute($sql);
				}
			}
			unset($sql);
			unset($value);
			if($arrTR){
				$sql = "insert into work_process_design_node_role(design_node_id,role_id) values";
				$value = "";
				foreach( $arrFR AS $val ){
					$str = "(";
					$str .= "'" .$result. "',";
					$str .= "'" .$val. "'";
					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $work_process_design_node->execute($sql);
				}
			}
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}


	function customerFieldCache(){
		$work_process_design_node = new Model("work_process_design_node");
		$arrData = $work_process_design_node->select();

		foreach($arrData as &$val){
			$val["writable_fields"] = json_decode($val["writable_fields"],true);
			$val["num"] = $val["process_design_id"]."_".$val["id"]."_".$val["order_num"];   //流程id_流程节点id_节点序号
			$arrF[$val["num"]] = $val["writable_fields"];
		}

		//dump($arrF);die;

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("customerField_$db_name",$arrF,"BGCC/Conf/crm/");
		F("customerField",$arrF,"BGCC/Conf/crm/$db_name/");
	}

	function editDesignFlow(){
		checkLogin();
		$process_design_id = $_REQUEST["process_design_id"];
		$this->assign("process_design_id",$process_design_id);

		$process_name = $_REQUEST["process_name"];
		$this->assign("process_name",$process_name);

		$id = $_REQUEST["id"];
		$this->assign("id",$id);

		$work_process_design_node = new Model("work_process_design_node");
		$arrData = $work_process_design_node->where("id = '$id'")->find();
		$arrT = json_decode($arrData["writable_fields"],true);
		$this->assign("arrData",$arrData);


		$form_id = $_REQUEST["form_design_id"];
		$table_name = "work_form_$form_id";
		//$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrFields = require "BGCC/Conf/crm/$db_name/order_fieldCache.php";
		$arrFields = getFormField();
		$cmFields = $arrFields[$table_name];
		//dump($cmFields);die;
		$i = 0;
		foreach($cmFields as $val){
			if($val['field_enabled'] == 'Y'){
				$arrF[] = $val['en_name'];
			}
			if($val['text_type'] == '2'){
				$cmFields[$i]["field_values"] = json_decode($val["field_values"] ,true);
			}
			$i++;
		}
		$fielddTpl2 = $this->array_sort($cmFields,'field_order','asc','no');

		foreach($fielddTpl2 as $val){
			if($val['field_enabled'] == 'Y'){
				$fielddTpl[] = $val;
			}
		}


		foreach($fielddTpl as &$val){
			if( in_array($val["id"],$arrT) ){
				$val["checked"] = "checked";
			}else{
				$val["checked"] = "";
			}
		}
		$this->assign("fielddTpl",$fielddTpl);

		$this->display();
	}

	function updateDesignFlow(){
		$id = $_REQUEST["id"];
		$work_process_design_node = new Model("work_process_design_node");

		$arrTR = $_REQUEST['authorize_role_id'];
		$arrFR = explode(",",$arrTR);
		$authorize_role_id = json_encode($arrFR);

		$arrTD = $_REQUEST['authorize_dept_ids'];
		$arrFD = explode(",",$arrTD);
		$authorize_dept_ids = json_encode($arrFD);

		$arrTP = $_REQUEST['authorized_personnel'];
		$arrFP = explode(",",$arrTP);
		$authorized_personnel = json_encode($arrFP);


		$field = $_REQUEST["field"];
		$customer_fields = json_encode($field);

		$users = new Model("users");
		$para_sys = readS();
		$where = "1 ";
		if($para_sys["collection_types"] == "and"){
			if($arrTP){
				$user = "'".implode("','",$arrFP)."'";
				$where .= " AND username in ($user) ";
			}
			if($arrTD){
				$dept_id = "'".implode("','",$arrFD)."'";
				$where .= " AND d_id in ($dept_id) ";
			}
			if($arrTR){
				$rid = "'".implode("','",$arrFR)."'";
				$where .= " AND r_id in ($rid) ";
			}
		}else{
			if($arrTP){
				$user = "'".implode("','",$arrFP)."'";
				$where .= " OR username in ($user) ";
			}
			if($arrTD){
				$dept_id = "'".implode("','",$arrFD)."'";
				$where .= " OR d_id in ($dept_id) ";
			}
			if($arrTR){
				$rid = "'".implode("','",$arrFR)."'";
				$where .= " OR r_id in ($rid) ";
			}
		}
		$arrAW = $users->field("username,d_id,r_id")->where($where)->select();
		foreach($arrAW as $val){
			$arrU[] = $val["username"];
		}
		$authorized_work_num = json_encode($arrU);
		//dump($arrAW);die;

		$arrData = array(
			'order_num'=>$_REQUEST['order_num'],
			'step_name'=>$_REQUEST['step_name'],
			'next_step_num'=>$_REQUEST['next_step_num'],
			'authorize_role_id'=>$authorize_role_id,
			'authorize_dept_ids'=>$authorize_dept_ids,
			'authorized_personnel'=>$authorized_personnel,
			'writable_fields'=>$customer_fields,
			'authorized_work_num'=>$authorized_work_num,
			'process_design_id'=>$_REQUEST['process_design_id']
		);
		//dump($field);die;
		$result = $work_process_design_node->where("id = '$id'")->data($arrData)->save();
		if ($result !== false){
			//$this->customerFieldCache();
			$work_process_design_node_role = new Model("work_process_design_node_role");
			$del = $work_process_design_node_role->where("design_node_id = '$id'")->delete();
			if($arrTP){
				$sql = "insert into work_process_design_node_role(design_node_id,user_name) values";
				$value = "";
				foreach( $arrFP AS $val ){
					$str = "(";
					$str .= "'" .$id. "',";
					$str .= "'" .$val. "'";
					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $work_process_design_node->execute($sql);
				}
			}
			unset($sql);
			unset($value);
			if($arrTD){
				$sql = "insert into work_process_design_node_role(design_node_id,dept_id) values";
				$value = "";
				foreach( $arrFD AS $val ){
					$str = "(";
					$str .= "'" .$id. "',";
					$str .= "'" .$val. "'";
					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $work_process_design_node->execute($sql);
				}
			}
			unset($sql);
			unset($value);
			if($arrTR){
				$sql = "insert into work_process_design_node_role(design_node_id,role_id) values";
				$value = "";
				foreach( $arrFR AS $val ){
					$str = "(";
					$str .= "'" .$id. "',";
					$str .= "'" .$val. "'";
					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $work_process_design_node->execute($sql);
				}
			}
			echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteDesignFlow(){
		$id = $_REQUEST["id"];
		$work_process_design_node = new Model("work_process_design_node");
		$result = $work_process_design_node->where("id in ($id)")->delete();
		if ($result){
			//$this->customerFieldCache();
			$work_process_design_node_role = new Model("work_process_design_node_role");
			$del = $work_process_design_node_role->where("design_node_id = '$id'")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

}

?>
