<?php
/*
问卷管理
*/
class QuestionnaireManagementAction extends Action{
	//问卷类型
	function questionnaireType(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Questionnaire Type";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}


	function questionnaireTypeData(){
		$wj_questionnaire_type = new Model("wj_questionnaire_type");
		import('ORG.Util.Page');
		$count = $wj_questionnaire_type->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wj_questionnaire_type->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertQuestionnaireType(){
		$wj_questionnaire_type = new Model("wj_questionnaire_type");
		$arrData = array(
			"questionnaire_type_name"=>$_REQUEST["questionnaire_type_name"],
			"questionnaire_type_description"=>$_REQUEST["questionnaire_type_description"]
		);
		$result = $wj_questionnaire_type->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateQuestionnaireType(){
		$id = $_REQUEST["id"];
		$wj_questionnaire_type = new Model("wj_questionnaire_type");
		$arrData = array(
			"questionnaire_type_name"=>$_REQUEST["questionnaire_type_name"],
			"questionnaire_type_description"=>$_REQUEST["questionnaire_type_description"]
		);
		$result = $wj_questionnaire_type->data($arrData)->where("id = $id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteQuestionnaireType(){
		$id = $_REQUEST["id"];
		$wj_questionnaire_type = new Model("wj_questionnaire_type");
		$result = $wj_questionnaire_type->where("id = $id")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}




	//问卷题型
	function topicList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Questionnaire Topic";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function topicData(){
		$wj_questionnaire_topic = new Model("wj_questionnaire_topic");
		import('ORG.Util.Page');
		$count = $wj_questionnaire_topic->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wj_questionnaire_topic->limit($page->firstRow.','.$page->listRows)->select();

		$topic_type_row = array("1"=>"单选题","2"=>"多选题","3"=>"判断题","4"=>"填空题","5"=>"问答题","6"=>"排序题");
		foreach($arrData as &$val){
			$topic_type = $topic_type_row[$val["topic_type"]];
			$val["topic_type2"] = $topic_type;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertTopic(){
		$wj_questionnaire_topic = new Model("wj_questionnaire_topic");
		$arrData = array(
			"topic_name"=>$_REQUEST["topic_name"],
			"topic_type"=>$_REQUEST["topic_type"]
		);
		$result = $wj_questionnaire_topic->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateTopic(){
		$id = $_REQUEST["id"];
		$wj_questionnaire_topic = new Model("wj_questionnaire_topic");
		$arrData = array(
			"topic_name"=>$_REQUEST["topic_name"],
			"topic_type"=>$_REQUEST["topic_type"]
		);
		$result = $wj_questionnaire_topic->data($arrData)->where("id = $id")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteTopic(){
		$id = $_REQUEST["id"];
		$wj_questionnaire_topic = new Model("wj_questionnaire_topic");
		$result = $wj_questionnaire_topic->where("id = $id")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'Some errors occured.'));
		}
	}




	//问卷题库
	function questionnaireBankList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Questionnaire Bank Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function questionnaireData(){
		$username = $_SESSION['user_info']['username'];
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");

		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,t.questionnaire_type_name,t.questionnaire_type_description,p.topic_name,p.topic_type";

		$start_time = $_REQUEST["start_time"];
		$emd_time = $_REQUEST["emd_time"];
		$questionnaire_type_id = $_REQUEST["questionnaire_type_id"];
		$questionnaire_topic_id = $_REQUEST["questionnaire_topic_id"];
		$questionnaire_topics = $_REQUEST["questionnaire_topics"];

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND q.create_time >= '$start_time'";
		$where .= empty($emd_time) ? "" : " AND q.create_time <= '$emd_time'";
		$where .= empty($questionnaire_type_id) ? "" : " AND q.questionnaire_type_id = '$questionnaire_type_id'";
		$where .= empty($questionnaire_topic_id) ? "" : " AND q.questionnaire_topic_id = '$questionnaire_topic_id'";
		$where .= empty($questionnaire_topics) ? "" : " AND q.questionnaire_topics like '%$questionnaire_topics%'";

		$count = $wj_questionnaire_bank->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wj_questionnaire_bank->order("q.create_time desc")->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $wj_questionnaire_bank->getLastSql();die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addQuestionnaire(){
		checkLogin();

		$this->display();
	}

	function insertQuestionnaire(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");
		$fieldValue = $_REQUEST["field_value"];
		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'questionnaire_type_id'=>$_REQUEST['questionnaire_type_id'],
			'questionnaire_topics'=>$_REQUEST['questionnaire_topics'],
			'questionnaire_topic_id'=>$_REQUEST['questionnaire_topic_id'],
			'questionnaire_content'=>json_encode($fieldValue),
		);
		//dump($fieldValue);die;
		$result = $wj_questionnaire_bank->data($arrData)->add();
		if($_REQUEST["topic_type"] == '1' || $_REQUEST["topic_type"] == '2' || $_REQUEST["topic_type"] == '3' || $_REQUEST["topic_type"] == '6' ){
			if ($result){
				$wj_questionnaire_bank_select = new Model("wj_questionnaire_bank_select");
				$sql = "insert into wj_questionnaire_bank_select(select_value, select_name, select_enabled,questionnaire_exam_id ,select_order) values ";
				$value = "";
				$i = 0;
				foreach( $fieldValue AS $row ){
					$str = "(";
					$str .= "'" .$row[0]. "',";  	 //选择框value值
					//$str .= "'" .$row[1]. "',";		//选择框的值
					$str .= "'" .str_replace("'",'"',$row[1]). "',";		//选择框的值
					$str .= "'" .$row[2]. "',";		//是否显示
					$str .= "'" .$result. "',";		//字段id
					//$str .= "'" .$_REQUEST["en_name"]. "',";		//字段id
					$str .= "'" .$i. "'";

					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $wj_questionnaire_bank_select->execute($sql);
				}
				if($res){
					//$this->fieldCache();
					//$this->selectCache();
					echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
				} else {
					echo json_encode(array('msg'=>'添加失败！'));
				}
			} else {
				echo json_encode(array('msg'=>'添加失败！'));
			}
		}else{
			if($result){
				//$this->fieldCache();
				echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
			} else {
				echo json_encode(array('msg'=>'添加失败！'));
			}
		}
	}


	function fieldCache(){
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");
		$fieldData = $wj_questionnaire_bank->order("create_time asc")->select();
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("questionnaire_bank_$db_name",$fieldData,"BGCC/Conf/crm/");
		F("questionnaire_bank",$fieldData,"BGCC/Conf/crm/$db_name/");
	}
	function selectCache(){
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");
		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,p.topic_name,p.topic_type";
		$fieldData = $wj_questionnaire_bank->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where("p.topic_type in (1,2,3,6)")->select();

		foreach($fieldData as $val){
			$val["questionnaire_content"] = json_decode($val["questionnaire_content"] ,true);
			$selectTpl[] = $val;
		}
		foreach($selectTpl as $v){
			foreach($v['questionnaire_content'] as $vm){
				$tmp[$v['id']][] = array($vm[0]=>$vm[1]);
			}
		}
		foreach($tmp As $key=>&$val){
			$arr = array();
			foreach($val AS $value){
				foreach($value as $k=>$v){
					$arr[$k] = $v;
				}
			}
			$val = $arr;
		}
		//dump($tmp);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("questionnaire_bank_content_$db_name",$tmp,"BGCC/Conf/crm/");
		F("questionnaire_bank_content",$tmp,"BGCC/Conf/crm/$db_name/");
	}

	function editQuestionnaire(){
		checkLogin();
		$id = $_REQUEST["id"];
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");
		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,p.topic_name,p.topic_type";
		$arrData = $wj_questionnaire_bank->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where("q.id = $id")->find();

		if($arrData["start_validity_time"] == "0000-00-00 00:00:00"){
			$arrData["start_validity_time"] = "";
		}
		if($arrData["end_validity_time"] == "0000-00-00 00:00:00"){
			$arrData["end_validity_time"] = "";
		}

		$this->assign("id",$id);
		$this->assign("arrData",$arrData);

		if($arrData['topic_type'] == '1' || $arrData['topic_type'] == '2' || $arrData['topic_type'] == '3' || $arrData['topic_type'] == '6' ){
			$wj_questionnaire_bank_select = new Model("wj_questionnaire_bank_select");
			$selectData = $wj_questionnaire_bank_select->order("select_order asc")->where("questionnaire_exam_id = '$id'")->select();
			$max = $wj_questionnaire_bank_select->where("questionnaire_exam_id = '$id'")->max("select_order");
			$max2 = $max+1;
			$this->assign("max",$max2);
			$i = 0;
			foreach($selectData as &$val){
				$val["select_order"] = $i;
				$i++;
			}
			$this->assign("selectData",$selectData);
		}
		//dump($max);
		//dump($selectData);die;
		$this->display();
	}

	function updateQuestionnaire(){
		$id = $_REQUEST['id'];
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");
		$fieldValue = $_REQUEST["field_value"];
		//dump($fieldValue);die;
		$arrData = array(
			'questionnaire_type_id'=>$_REQUEST['questionnaire_type_id'],
			'questionnaire_topics'=>$_REQUEST['questionnaire_topics'],
			'questionnaire_topic_id'=>$_REQUEST['questionnaire_topic_id'],
			'questionnaire_content'=>json_encode($fieldValue),
		);
		$result = $wj_questionnaire_bank->data($arrData)->where("id = '$id'")->save();
		//echo $wj_questionnaire_bank->getLastSql();
		if($_REQUEST["topic_type"] == '1' || $_REQUEST["topic_type"] == '2' || $_REQUEST["topic_type"] == '3' || $_REQUEST["topic_type"] == '6' ){
			if ($result !== false){
				$wj_questionnaire_bank_select = new Model("wj_questionnaire_bank_select");
				$resDel = $wj_questionnaire_bank_select->where("questionnaire_exam_id = '$id'")->delete();
				$sql = "insert into wj_questionnaire_bank_select(select_value, select_name, select_enabled,questionnaire_exam_id ,select_order) values ";
				$value = "";
				$i = 0;
				foreach( $fieldValue AS $row ){
					$str = "(";
					$str .= "'" .$row[0]. "',";  	 //选择框value值
					$str .= "'" .str_replace("'",'"',$row[1]). "',";		//选择框的值
					//$str .= "'" .$row[1]. "',";		//选择框的值
					$str .= "'" .$row[2]. "',";		//是否显示
					$str .= "'" .$id. "',";		//字段id
					//$str .= "'" .$_REQUEST["en_name"]. "',";		//字段id
					$str .= "'" .$i. "'";

					$str .= ")";
					$value .= empty($value)?"$str":",$str";
					$i++;
				}
				unset($i);
				if( $value ){
					$sql .= $value;
					$res = $wj_questionnaire_bank_select->execute($sql);
				}
				if($res){
					//$this->fieldCache();
					//$this->selectCache();
					echo json_encode(array('success'=>true,'msg'=>'更新成功！'));
				} else {
					echo json_encode(array('msg'=>'添加失败！'));
				}
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}else{
			if ($result !== false){
				//$this->fieldCache();
				echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}
	}

	function deleteQuestionnaire(){
		$id = $_REQUEST["id"];
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");
		$result = $wj_questionnaire_bank->where("id in ($id)")->delete();
		if ($result){
			$wj_questionnaire_bank_select = new Model("wj_questionnaire_bank_select");
			$resDel = $wj_questionnaire_bank_select->where("questionnaire_exam_id in ($id)")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}



	//生成问卷
	function questionnaireList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Generating Questionnaire";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function generateQuestionnaireData(){
		$username = $_SESSION['user_info']['username'];
		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");

		$fields = "g.id,g.create_time,g.create_user,g.questionnaire_type_id,g.questionnaire_name,g.whether_to_audit,g.topics_ids,g.questionnaire_description,t.questionnaire_type_name,t.questionnaire_type_description";

		$start_time = $_REQUEST["start_time"];
		$emd_time = $_REQUEST["emd_time"];
		$questionnaire_type_id = $_REQUEST["questionnaire_type_id"];
		$questionnaire_name = $_REQUEST["questionnaire_name"];
		$questionnaire_description = $_REQUEST["questionnaire_description"];

		$where = "1 ";
		$where .= empty($start_time) ? "" : " AND g.create_time >= '$start_time'";
		$where .= empty($emd_time) ? "" : " AND g.create_time <= '$emd_time'";
		$where .= empty($questionnaire_type_id) ? "" : " AND g.questionnaire_type_id = '$questionnaire_type_id'";
		$where .= empty($questionnaire_name) ? "" : " AND g.questionnaire_name like '%$questionnaire_name%'";
		$where .= empty($questionnaire_description) ? "" : " AND g.questionnaire_description like '%$questionnaire_description%'";

		$count = $wj_questionnaire_generation->table("wj_questionnaire_generation g")->field($fields)->join("wj_questionnaire_type t on (g.questionnaire_type_id = t.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wj_questionnaire_generation->order("g.create_time desc")->table("wj_questionnaire_generation g")->field($fields)->join("wj_questionnaire_type t on (g.questionnaire_type_id = t.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $wj_questionnaire_generation->getLastSql();die;

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//dump($db_name);die;
		/*
		if(file_exists("BGCC/Conf/questionnaireID.php")){
			$arrP = require "BGCC/Conf/questionnaireID.php";
		}else{
			$arrP = array();
		}
		*/
		$arrP = getQuestionnaireID();

		$whether_to_audit_row = array("Y"=>"是","N"=>"否");
		foreach($arrData as &$val){
			$whether_to_audit = $whether_to_audit_row[$val["whether_to_audit"]];
			$val["whether_to_audit2"] = $whether_to_audit;

			$val["topics_ids"] = json_decode($val["topics_ids"],true);
			$val["topics_ids2"] = implode(",",$val["topics_ids"]);

			if(in_array($val["id"],$arrP)){
				$val["haved_survey"] = "Y";
			}else{
				$val["haved_survey"] = "N";
			}
			if($val["topics_ids2"]){
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"openQuestionnaire("."'".$val["id"]."'".")\" >查看问卷</a>";
				/*
				if($val["haved_survey"] == "Y"){
					$val["operating"] .= "<a href='javascript:void(0);' onclick=\"openAnswerStatistical("."'".$val["id"]."'".")\" >&nbsp;&nbsp;&nbsp;答案统计</a>";
				}
				*/
			}
		}


		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertGenerateQuestionnaire(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
		$topics_ids = json_encode(explode(",",$_REQUEST['topics_ids']));
		//dump($topics_ids);die;
		$arrData = array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'questionnaire_type_id'=>$_REQUEST['questionnaire_type_id'],
			'questionnaire_name'=>$_REQUEST['questionnaire_name'],
			'whether_to_audit'=>$_REQUEST['whether_to_audit'],
			'questionnaire_description'=>$_REQUEST['questionnaire_description'],
			'topics_ids'=>$topics_ids,
		);
		$result = $wj_questionnaire_generation->data($arrData)->add();
		if ($result){
			$this->questionnaireIDNameCache();
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}


	function questionnaireIDNameCache(){
		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
		$arrData = $wj_questionnaire_generation->select();
		foreach($arrData as $key=>$val){
			$arrF[$val["id"]] = $val["questionnaire_name"];
		}

		//dump($arrF);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//F("questionnaireIDName_$db_name",$arrF,"BGCC/Conf/crm/");
		F("questionnaireIDName",$arrF,"BGCC/Conf/crm/$db_name/");
	}

	function updateGenerateQuestionnaire(){
		$id = $_REQUEST['id'];
		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
		$topics_ids = json_encode(explode(",",$_REQUEST['topics_ids']));
		//dump($topics_ids);die;
		$arrData = array(
			'questionnaire_type_id'=>$_REQUEST['questionnaire_type_id'],
			'questionnaire_name'=>$_REQUEST['questionnaire_name'],
			'whether_to_audit'=>$_REQUEST['whether_to_audit'],
			'questionnaire_description'=>$_REQUEST['questionnaire_description'],
			'topics_ids'=>$topics_ids,
		);
		$result = $wj_questionnaire_generation->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			$this->questionnaireIDNameCache();
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteGenerateQuestionnaire(){
		$id = $_REQUEST["id"];
		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
		$result = $wj_questionnaire_generation->where("id in ($id)")->delete();
		if ($result){
			$this->questionnaireIDNameCache();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//查看、编辑问卷
	function viewQuestionnaire(){
		header("Content-Type:text/html; charset=utf-8");
		$id = $_REQUEST["id"];
		$customer_id = $_REQUEST["customer_id"];
		$task_id = $_REQUEST["task_id"];
		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
		$arrT = $wj_questionnaire_generation->where("id = '$id'")->find();
		$ids = implode(",",json_decode($arrT["topics_ids"],true));

		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");

		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,t.questionnaire_type_name,t.questionnaire_type_description,p.topic_name,p.topic_type";

		$arrData = $wj_questionnaire_bank->order("p.topic_type asc")->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where("q.id in ($ids)")->select();
		$i = 1;
		foreach($arrData as &$val){
			$val["order"] = $i;
			$val["questionnaire_content"] = json_decode($val["questionnaire_content"],true);
			$i++;
		}

		//dump($arrData);die;
		$this->assign("arrData",$arrData);
		$this->assign("questionnaire_generation_id",$id);
		$this->assign("questionnaire_type_id",$arrT["questionnaire_type_id"]);
		$this->assign("arrT",$arrT);
		$this->assign("ids",$ids);
		$this->assign("customer_id",$customer_id);
		$this->assign("task_id",$task_id);

		$web_type = empty($_REQUEST["web_type"]) ? "view" : $_REQUEST["web_type"];
		if($web_type == "survey"){
			$this->assign("web_type",$web_type);
		}else{
			$this->assign("web_type","view");
		}
		//dump($web_type);die;

		$this->display();
	}

	//查看、编辑问卷
	function viewQuestionnaire2(){
		header("Content-Type:text/html; charset=utf-8");
		$id = $_REQUEST["generation_id"];	//问卷id
		$customer_id = $_REQUEST["customer_id"];
		$task_id = $_REQUEST["task_id"];

		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
		$arrT = $wj_questionnaire_generation->where("id = '$id'")->find();
		$ids = implode(",",json_decode($arrT["topics_ids"],true));
		//dump($arrT);
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");

		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,t.questionnaire_type_name,t.questionnaire_type_description,p.topic_name,p.topic_type";

		$arrData = $wj_questionnaire_bank->order("p.topic_type asc")->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where("q.id in ($ids)")->select();
		$i = 1;
		foreach($arrData as &$val){
			$val["order"] = $i;
			$val["questionnaire_content"] = json_decode($val["questionnaire_content"],true);
			$i++;
		}

		//dump($arrData);die;
		$this->assign("questionnaire_generation_id",$id);
		$this->assign("questionnaire_type_id",$arrT["questionnaire_type_id"]);
		$this->assign("arrT",$arrT);
		$this->assign("ids",$ids);
		$this->assign("customer_id",$customer_id);
		$this->assign("task_id",$task_id);


		$questionnaire_answers_id = $_REQUEST["answers_id"];
		$wj_questionnaire_answers_detail = new Model("wj_questionnaire_answers_detail_".$task_id);
		$arrF = $wj_questionnaire_answers_detail->where("questionnaire_answers_id = '$questionnaire_answers_id'")->select();
		//echo $wj_questionnaire_answers_detail->getLastSql();
		foreach($arrF as $key=>&$val){
			$arrFID[$val["questionnaire_exam_id"]] = $val["fill_answer"];

			if($val["question_type"] == "1"){
				$val["name"] = "radio_".$val["questionnaire_exam_id"];
			}elseif($val["question_type"] == "2"){
				$val["fill_answer"] = json_encode(explode(" ",$val["fill_answer"]));
				$val["name"] = "multiple_".$val["questionnaire_exam_id"];
			}elseif($val["question_type"] == "3"){
				$val["name"] = "judge_".$val["questionnaire_exam_id"];
			}elseif($val["question_type"] == "4"){
				$val["name"] = "fill_".$val["questionnaire_exam_id"];
			}elseif($val["question_type"] == "5"){
				$val["name"] = "questionsAnswers_".$val["questionnaire_exam_id"];
			}elseif($val["question_type"] == "6"){
				$val["name"] = "order_".$val["questionnaire_exam_id"];
			}
		}
		foreach($arrData as &$val){
			$val["fill_answer"] = $arrFID[$val["id"]];
			if($val["topic_type"] == "2"){
				foreach($val["questionnaire_content"] as &$vm){
					if(strstr($val["fill_answer"],$vm[0])){
						$vm["checked"] = "checked";
					}else{
						$vm["checked"] = "";
					}
				}
			}

		}

		$this->assign("questionnaire_answers_id",$questionnaire_answers_id);
		$this->assign("arrF",$arrF);


		$this->assign("arrData",$arrData);
		//dump($arrData);die;


		$web_type = empty($_REQUEST["web_type"]) ? "view" : $_REQUEST["web_type"];
		if($web_type == "edit"){
			$this->assign("web_type",$web_type);
		}else{
			$this->assign("web_type","view");
		}

		//分配增删改的权限--------审核答卷
		$menuname = "Respondents Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		$wj_questionnaire_answers = new Model("wj_questionnaire_answers_".$task_id);
		$arrA = $wj_questionnaire_answers->where("id = '$questionnaire_answers_id'")->find();
		//echo $wj_questionnaire_answers->getLastSql();
		$this->assign("arrA",$arrA);
		//dump($arrA);die;

		$this->display();
	}

	//查看、编辑问卷
	function viewQuestionnaire2Bak(){
		header("Content-Type:text/html; charset=utf-8");
		$id = $_REQUEST["id"];	//问卷id
		$customer_id = $_REQUEST["customer_id"];
		$task_id = $_REQUEST["task_id"];
		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
		$arrT = $wj_questionnaire_generation->where("id = '$id'")->find();
		$ids = implode(",",json_decode($arrT["topics_ids"],true));

		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");

		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,t.questionnaire_type_name,t.questionnaire_type_description,p.topic_name,p.topic_type";

		$arrData = $wj_questionnaire_bank->order("p.topic_type asc")->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where("q.id in ($ids)")->select();
		$i = 1;
		foreach($arrData as &$val){
			$val["order"] = $i;
			$val["questionnaire_content"] = json_decode($val["questionnaire_content"],true);
			$i++;
		}

		//dump($arrData);die;
		$this->assign("questionnaire_generation_id",$id);
		$this->assign("questionnaire_type_id",$arrT["questionnaire_type_id"]);
		$this->assign("arrT",$arrT);
		$this->assign("ids",$ids);
		$this->assign("customer_id",$customer_id);
		$this->assign("task_id",$task_id);


		$username = $_SESSION['user_info']['username'];
		$date = date("Y-m-d");
		$wj_questionnaire_answers = new Model("wj_questionnaire_answers");
		$where = "questionnaire_generation_id = '$id' AND create_user = '$username' AND DATE(create_time)='$date'";
		$arr = $wj_questionnaire_answers->where($where)->find();


		if($arr){
			$wj_questionnaire_answers_detail = new Model("wj_questionnaire_answers_detail");
			$questionnaire_answers_id = $arr["id"];
			$arrF = $wj_questionnaire_answers_detail->where("questionnaire_answers_id = '$questionnaire_answers_id'")->select();
			foreach($arrF as $key=>&$val){
				$arrFID[$val["questionnaire_exam_id"]] = $val["fill_answer"];

				if($val["question_type"] == "1"){
					$val["name"] = "radio_".$val["questionnaire_exam_id"];
				}elseif($val["question_type"] == "2"){
					$val["fill_answer"] = json_encode(explode(" ",$val["fill_answer"]));
					$val["name"] = "multiple_".$val["questionnaire_exam_id"];
				}elseif($val["question_type"] == "3"){
					$val["name"] = "judge_".$val["questionnaire_exam_id"];
				}elseif($val["question_type"] == "4"){
					$val["name"] = "fill_".$val["questionnaire_exam_id"];
				}elseif($val["question_type"] == "5"){
					$val["name"] = "questionsAnswers_".$val["questionnaire_exam_id"];
				}elseif($val["question_type"] == "6"){
					$val["name"] = "order_".$val["questionnaire_exam_id"];
				}
			}
			//if($web_type == "exam"){
				foreach($arrData as &$val){
					$val["fill_answer"] = $arrFID[$val["id"]];
					if($val["topic_type"] == "2"){
						foreach($val["questionnaire_content"] as &$vm){
							if(strstr($val["fill_answer"],$vm[0])){
								$vm["checked"] = "checked";
							}else{
								$vm["checked"] = "";
							}
						}
					}

				}
			//}

			$this->assign("questionnaire_answers_id",$questionnaire_answers_id);
			$this->assign("arrF",$arrF);

		}

		$this->assign("arrData",$arrData);
		//dump($arrData);die;

		$this->display();
	}

	//添加答卷
	function insertQuestionnaireAnswers(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ids = $_REQUEST['ids'];

		$task_id = $_REQUEST['task_id'];
		$customer_id = $_REQUEST['customer_id'];

		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");
		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,t.questionnaire_type_name,t.questionnaire_type_description,p.topic_name,p.topic_type";
		$arrQ = $wj_questionnaire_bank->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where("q.id in ($ids)")->select();
		foreach($arrQ as $key=>&$val){
			$arrTopicType[$val["id"]] = $val["topic_type"];
			$arrTopicID[$val["id"]] = $val["questionnaire_topic_id"];
		}


		$arrF = $_REQUEST;
		foreach($arrF as $key=>&$val){
			if($key != "ids" && $key != "questionnaire_generation_id" && $key != "questionnaire_type_id"  && $key != "questionnaire_answers_id" && $key != "task_id" && $key != "customer_id" && $key != "__hash__")
			$arr[] = array(
				"id"=>array_pop(explode("_",$key)),
				"value"=>$val,
				"topic_type"=>$arrTopicType[array_pop(explode("_",$key))],
				"questionnaire_topic_id"=>$arrTopicID[array_pop(explode("_",$key))]
			);
		}

		foreach($arr as &$val){
			if($val["topic_type"] == "2"){
				$val["value2"] = implode(" ",$val["value"]);
			}else{
				$val["value2"] = $val["value"];
			}
		}

		$wj_questionnaire_answers = new Model("wj_questionnaire_answers_".$task_id);
		$arrData = array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			//'answer_code'=>$username.date("YmdHis"),
			'dept_id'=>$d_id,
			'questionnaire_generation_id'=>$_REQUEST['questionnaire_generation_id'],
			'questionnaire_type_id'=>$_REQUEST['questionnaire_type_id'],
			'task_id'=>$_REQUEST['task_id'],
			'customer_id'=>$_REQUEST['customer_id']
		);

		//dump($arr);
		//dump($arrData);die;
		$result = $wj_questionnaire_answers->data($arrData)->add();
		if ($result){
			$this->questionnaireIDCache();
			$wj_questionnaire_answers_detail = new Model("wj_questionnaire_answers_detail_".$task_id);
			$sql = "insert into wj_questionnaire_answers_detail_$task_id(questionnaire_exam_id,questionnaire_topic_id,question_type,fill_answer,questionnaire_generation_id,questionnaire_answers_id) values ";
			$value = "";
			$questionnaire_generation_id = $_REQUEST['questionnaire_generation_id'];
			foreach( $arr AS &$val ){
				$str = "(";
				$str .= "'" .$val["id"]. "',";
				$str .= "'" .$val["questionnaire_topic_id"]. "',";
				$str .= "'" .$val["topic_type"]. "',";
				$str .= "'" .$val["value2"]. "',";
				$str .= "'" .$questionnaire_generation_id. "',";
				$str .= "'" .$result. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $wj_questionnaire_answers_detail->execute($sql);
			}
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateQuestionnaireAnswers(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ids = $_REQUEST['ids'];
		$questionnaire_answers_id = $_REQUEST['questionnaire_answers_id'];

		$task_id = $_REQUEST['task_id'];
		$customer_id = $_REQUEST['customer_id'];

		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");
		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,t.questionnaire_type_name,t.questionnaire_type_description,p.topic_name,p.topic_type";
		$arrQ = $wj_questionnaire_bank->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where("q.id in ($ids)")->select();
		foreach($arrQ as $key=>&$val){
			$arrTopicType[$val["id"]] = $val["topic_type"];
			$arrTopicID[$val["id"]] = $val["questionnaire_topic_id"];
		}


		$arrF = $_REQUEST;
		foreach($arrF as $key=>&$val){
			if($key != "ids" && $key != "questionnaire_generation_id" && $key != "questionnaire_type_id"  && $key != "questionnaire_answers_id" && $key != "task_id" && $key != "customer_id" && $key != "__hash__")
			$arr[] = array(
				"id"=>array_pop(explode("_",$key)),
				"value"=>$val,
				"topic_type"=>$arrTopicType[array_pop(explode("_",$key))],
				"questionnaire_topic_id"=>$arrTopicID[array_pop(explode("_",$key))]
			);
		}

		foreach($arr as &$val){
			if($val["topic_type"] == "2"){
				$val["value2"] = implode(" ",$val["value"]);
			}else{
				$val["value2"] = $val["value"];
			}
		}

		$wj_questionnaire_answers = new Model("wj_questionnaire_answers_".$task_id);
		$arrData = array(
			'modification_time'=>date("Y-m-d H:i:s")
		);
		//dump($arr);
		//dump($arrData);die;
		$result = $wj_questionnaire_answers->where("id = '$questionnaire_answers_id'")->data($arrData)->save();
		if ($result !== false){
			$this->questionnaireIDCache();
			$wj_questionnaire_answers_detail = new Model("wj_questionnaire_answers_detail_".$task_id);
			$del = $wj_questionnaire_answers_detail->where("questionnaire_answers_id = '$questionnaire_answers_id'")->delete();
			$sql = "insert into wj_questionnaire_answers_detail_$task_id(questionnaire_exam_id,questionnaire_topic_id,question_type,fill_answer,questionnaire_generation_id,questionnaire_answers_id) values ";
			$value = "";
			$questionnaire_generation_id = $_REQUEST['questionnaire_generation_id'];
			foreach( $arr AS &$val ){
				$str = "(";
				$str .= "'" .$val["id"]. "',";
				$str .= "'" .$val["questionnaire_topic_id"]. "',";
				$str .= "'" .$val["topic_type"]. "',";
				$str .= "'" .$val["value2"]. "',";
				$str .= "'" .$questionnaire_generation_id. "',";
				$str .= "'" .$questionnaire_answers_id. "'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $wj_questionnaire_answers_detail->execute($sql);
			}
			echo json_encode(array('success'=>true,'msg'=>'保存成功！'));
		} else {
			echo json_encode(array('msg'=>'保存失败！'));
		}
	}

	function questionnaireIDCache(){
		$wj_questionnaire_answers = new Model("wj_questionnaire_answers");
		$arrData = $wj_questionnaire_answers->group("questionnaire_generation_id")->select();
		foreach($arrData as $val){
			$arrF[] = $val["questionnaire_generation_id"];
		}

		//dump($arrF);die;
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		F("questionnaireID",$arrF,"BGCC/Conf/crm/$db_name/");
		//F("questionnaireID",$arrF,"BGCC/Conf/");
	}

	//答卷分类
	function updateAnswersClass(){
		$id = $_REQUEST['id'];
		$customer_id = $_REQUEST["customer_id"];
		$task_id = $_REQUEST["task_id"];

		$wj_questionnaire_answers = new Model("wj_questionnaire_answers_".$task_id);
		$arrData = array(
			'answer_class'=>$_REQUEST['answer_class'],
			'answer_code'=>$_REQUEST['answer_code']
		);
		$result = $wj_questionnaire_answers->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	//答卷编码
	function updateAnswersCoding(){
		$id = $_REQUEST['id'];
		$customer_id = $_REQUEST["customer_id"];
		$task_id = $_REQUEST["task_id"];

		$wj_questionnaire_answers = new Model("wj_questionnaire_answers_".$task_id);
		$arrData = array(
			'answer_code'=>$_REQUEST['answer_code']
		);
		$result = $wj_questionnaire_answers->data($arrData)->where("id in ($id)")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}


	//答卷管理列表
	function answersList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Respondents Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);


		//$task_id = $_REQUEST["task_id"];
		$task_id = empty($_REQUEST["task_id"]) ? "Y" : $_REQUEST["task_id"];
		$customer_id = $_REQUEST["customer_id"];
		$this->assign("task_id",$task_id);
		$this->assign("customer_id",$customer_id);

		$this->display();
	}

	function answersData(){
		header("Content-Type:text/html;charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$wj_questionnaire_answers = new Model("wj_questionnaire_answers_".$task_id);
		$task_id = $_REQUEST["task_id"];
		$customer_id = $_REQUEST["customer_id"];
		$answer_class = $_REQUEST["answer_class"];
		$answer_code = $_REQUEST["answer_code"];
		$search_type = $_REQUEST["search_type"];

		$fields = "a.id,a.create_time,a.create_user,a.questionnaire_generation_id,a.questionnaire_type_id,a.answer_code,a.task_id,a.customer_id,a.answer_class,g.questionnaire_name,g.whether_to_audit,g.questionnaire_description,g.topics_ids,t.questionnaire_type_name,t.questionnaire_type_description,s.name,s.sex,s.phone1";

		$where = "1 ";
		if($task_id != "cm"){
			$where .= empty($task_id) ? "" : " AND a.task_id = '$task_id'";
			$cm_table = "sales_source_$task_id";
		}else{
			$cm_table = "customer";
		}
		$where .= empty($customer_id) ? "" : " AND a.customer_id = '$customer_id'";
		$where .= empty($answer_class) ? "" : " AND a.answer_class = '$answer_class'";
		$where .= empty($answer_code) ? "" : " AND a.answer_code like '%$answer_code%'";

		$count = $wj_questionnaire_answers->table("wj_questionnaire_answers_$task_id a")->field($fields)->join("wj_questionnaire_generation g on (a.questionnaire_generation_id = g.id)")->join("wj_questionnaire_type t on (a.questionnaire_type_id = t.id)")->join("$cm_table s on (a.customer_id = s.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		if($search_type == "xls"){
			$arrData = $wj_questionnaire_answers->order("a.create_time desc")->table("wj_questionnaire_answers_$task_id a")->field($fields)->join("wj_questionnaire_generation g on (a.questionnaire_generation_id = g.id)")->join("wj_questionnaire_type t on (a.questionnaire_type_id = t.id)")->join("$cm_table s on (a.customer_id = s.id)")->where($where)->select();
		}else{
			$arrData = $wj_questionnaire_answers->order("a.create_time desc")->table("wj_questionnaire_answers_$task_id a")->field($fields)->join("wj_questionnaire_generation g on (a.questionnaire_generation_id = g.id)")->join("wj_questionnaire_type t on (a.questionnaire_type_id = t.id)")->join("$cm_table s on (a.customer_id = s.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		}


		$answer_class_row = array("1"=>"有效答卷","2"=>"无效答卷","3"=>"删除的答卷");
		foreach($arrData as &$val){
			$answer_class = $answer_class_row[$val["answer_class"]];
			$val["answer_class"] = $answer_class;

			$val["operating"] = "<a href='javascript:void(0);' onclick=\"openQuestionnaire("."'".$val["id"]."','".$val["questionnaire_generation_id"]."'".")\" >查看答卷</a>";
		}

		if($search_type == "xls"){
			$field = array("create_time","create_user","name","phone1","questionnaire_name","questionnaire_type_name","answer_class","answer_code");
			$title = array("创建时间","答卷人","客户名称","客户号码","问卷名称","问卷类型","答卷分类","答卷编码");
			$title_count = count($field);
			$excelTiele = "问卷答卷".date("Y-m-d");
			//dump($arrData);die;

			exportDataFunction($title_count,$field,$title,$arrData,$excelTiele);
			die;
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function deleteAnswers(){
		$id = $_REQUEST["id"];
		$task_id = $_REQUEST["task_id"];
		$customer_id = $_REQUEST["customer_id"];

		$wj_questionnaire_answers = new Model("wj_questionnaire_answers_".$task_id);
		$result = $wj_questionnaire_answers->where("id in ($id)")->delete();
		if ($result){
			$wj_questionnaire_answers_detail = new Model("wj_questionnaire_answers_detail_".$task_id);
			$del = $wj_questionnaire_answers_detail->where("questionnaire_answers_id in ($id)")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}


	//题目答案统计
	function statisticalTopicAnswers(){
		checkLogin();
		$generation_id = $_REQUEST["generation_id"];
		$this->assign("generation_id",$generation_id);

		$this->display();
	}

	function statisticalTopicAnswersData(){
		//header("Content-Type:text/html; charset=utf-8");
		$wj_questionnaire_answers_detail = new Model("wj_questionnaire_answers_detail");

		$fields = "d.questionnaire_answers_id,d.questionnaire_exam_id,d.questionnaire_topic_id,d.question_type,d.fill_answer,a.create_user,a.questionnaire_generation_id,a.questionnaire_type_id,b.questionnaire_topics,count(*) as total";

		$questionnaire_generation_id = $_REQUEST["generation_id"];
		$where = "1 ";
		$where .= empty($questionnaire_generation_id) ? "" : " AND a.questionnaire_generation_id = '$questionnaire_generation_id'";

		$arrData = $wj_questionnaire_answers_detail->table("wj_questionnaire_answers_detail d")->field($fields)->join("wj_questionnaire_answers a on (d.questionnaire_answers_id = a.id)")->join("wj_questionnaire_bank b on (d.questionnaire_exam_id = b.id)")->where($where)->group("d.questionnaire_exam_id,d.fill_answer")->select();

		/*
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		$arrP = require "BGCC/Conf/crm/$db_name/questionnaire_bank_content.php";
		$arrQn = require "BGCC/Conf/crm/$db_name/questionnaireIDName.php";
		*/

		$arrP = getQuestionnaireBankContent();
		$arrQn = getQuestionnaireIDName();

		foreach($arrData as &$val){
			if($val["question_type"] == "1" || $val["question_type"] == "3" ){
				$val["name"] = $val["fill_answer"]."：".$arrP[$val["questionnaire_exam_id"]][$val["fill_answer"]];
			}elseif($val["question_type"] == "2"){
				$val["answers"] = explode(" ",$val["fill_answer"]);
				foreach($val["answers"] as $vm){
					$val["name"] .= $vm."：".$arrP[$val["questionnaire_exam_id"]][$vm]."  &nbsp;&nbsp; ";
				}
			}elseif($val["question_type"] == "6"){
				for($i=0;$i<strlen($val["fill_answer"]);$i++){
					$val["answers"][] = substr($val["fill_answer"],$i,1);
				}
				foreach($val["answers"] as $vm){
					$val["name"] .= $vm."：".$arrP[$val["questionnaire_exam_id"]][$vm]."  &nbsp;&nbsp; ";
				}
			}else{
				$val["name"] = $val["fill_answer"];
			}
			$val["questionnaire_name"] = $arrQn[$val["questionnaire_generation_id"]];  //问卷名称
		}
		//dump($arrData);die;


		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrF = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrData AS &$v){
			if($i >= $start && $j < $length){
				$arrF[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		$rowsList = count($arrF) ? $arrF : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//题目答案统计---外呼
	function statisticalTopicAnswersList(){
		checkLogin();
		$generation_id = $_REQUEST["generation_id"];
		$this->assign("generation_id",$generation_id);

		//$task_id = $_REQUEST["task_id"];
		$task_id = empty($_REQUEST["task_id"]) ? "Y" : $_REQUEST["task_id"];
		$this->assign("task_id",$task_id);

		$this->display();
	}

	function statisticalTopicAnswersListData(){
		//header("Content-Type:text/html; charset=utf-8");
		$task_id = $_REQUEST["task_id"];
		$wj_questionnaire_answers_detail = new Model("wj_questionnaire_answers_detail_".$task_id);

		$fields = "d.questionnaire_answers_id,d.questionnaire_exam_id,d.questionnaire_topic_id,d.question_type,d.fill_answer,a.create_user,a.questionnaire_generation_id,a.questionnaire_type_id,b.questionnaire_topics,count(*) as total";

		$questionnaire_generation_id = $_REQUEST["generation_id"];
		$topics_ids = $_REQUEST["topics_ids"];
		$where = "1 ";
		$where .= empty($questionnaire_generation_id) ? "" : " AND a.questionnaire_generation_id = '$questionnaire_generation_id'";
		$where .= empty($task_id) ? "" : " AND a.task_id = '$task_id'";
		$where .= empty($topics_ids) ? "" : " AND d.questionnaire_exam_id in ($topics_ids)";
		//dump($topics_ids);die;

		$arrData = $wj_questionnaire_answers_detail->table("wj_questionnaire_answers_detail_$task_id d")->field($fields)->join("wj_questionnaire_answers_$task_id a on (d.questionnaire_answers_id = a.id)")->join("wj_questionnaire_bank b on (d.questionnaire_exam_id = b.id)")->where($where)->group("d.questionnaire_exam_id,d.fill_answer")->select();
		//echo $wj_questionnaire_answers_detail->getLastSql();die;
		//dump($arrData);die;

		/*
		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		if(file_exists("BGCC/Conf/crm/$db_name/questionnaire_bank_content.php")){
			$arrP = require "BGCC/Conf/crm/$db_name/questionnaire_bank_content.php";
		}else{
			$arrP = array();
		}

		if(file_exists("BGCC/Conf/crm/$db_name/questionnaireIDName.php")){
			$arrQn = require "BGCC/Conf/crm/$db_name/questionnaireIDName.php";
		}else{
			$arrQn = array();
		}

		*/
		$arrP = getQuestionnaireBankContent();
		$arrQn = getQuestionnaireIDName();

		foreach($arrData as &$val){
			if($val["question_type"] == "1" || $val["question_type"] == "3" ){
				$val["name"] = $val["fill_answer"]."：".$arrP[$val["questionnaire_exam_id"]][$val["fill_answer"]];
			}elseif($val["question_type"] == "2"){
				$val["answers"] = explode(" ",$val["fill_answer"]);
				foreach($val["answers"] as $vm){
					$val["name"] .= $vm."：".$arrP[$val["questionnaire_exam_id"]][$vm]."  &nbsp;&nbsp; ";
				}
			}elseif($val["question_type"] == "6"){
				for($i=0;$i<strlen($val["fill_answer"]);$i++){
					$val["answers"][] = substr($val["fill_answer"],$i,1);
				}
				foreach($val["answers"] as $vm){
					$val["name"] .= $vm."：".$arrP[$val["questionnaire_exam_id"]][$vm]."  &nbsp;&nbsp; ";
				}
			}else{
				$val["name"] = $val["fill_answer"];
			}
			$val["questionnaire_name"] = $arrQn[$val["questionnaire_generation_id"]];  //问卷名称
		}
		//dump($arrData);die;


		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$count = count($arrData);
		$page = BG_Page($count,$page_rows);
		$start = $page->firstRow;
		$length = $page->listRows;
		//dump($length);die;
		$arrF = Array(); //转换成显示的
		$i = $j = 0;
		foreach($arrData AS &$v){
			if($i >= $start && $j < $length){
				$arrF[$j] = $v;
				$j++;
			}
			if( $j >= $length) break;
			$i++;
		}


		$rowsList = count($arrF) ? $arrF : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


}
?>



