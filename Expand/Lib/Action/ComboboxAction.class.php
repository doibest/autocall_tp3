<?php
class ComboboxAction extends Action{

	//获取角色
	function getRole(){
		$role=new Model('role');
		$arrData = $role->field("r_id as id,r_name as name,interface")->select();

		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}

	//获取班次
	function getShiftData(){
		$shift_definition = new Model('pb_shift_definition');
		$arrData = $shift_definition->field("id,shift_name as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}

	//获取课件
	function getCourseware(){
		$ks_courseware = new Model("ks_courseware");
		$arrData = $ks_courseware->field("id,courseware_name as name,curriculum_id")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...","curriculum_id"=>"");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择...","curriculum_id"=>""));
		}
		echo json_encode($arrData);
	}

	//获取课程
	function getCurriculum(){
		$ks_curriculum = new Model("ks_curriculum");
		$arrData = $ks_curriculum->field("id,curriculum_name as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}


	//培训计划
	function getTrain(){
		$username = $_SESSION['user_info']['username'];
		$ks_train = new Model("ks_train");

		$name = $_REQUEST['name'];

		$where = "1 ";
		$where .= empty($name)?"":" AND ( t.training_task_name like '%$name%' OR t.training_address like '%$name%'  OR c.courseware_name like '%$name%' OR kc.curriculum_name like '%$name%' )";

		$fields = "t.id,t.create_time,t.create_user,t.dept_id,t.curriculum_id,t.courseware_id,t.training_task_name,t.training_address,t.training_task_description,t.training_date,t.training_start_time,t.training_end_time,t.whether_exam,t.ks_id,t.participants,c.courseware_name,kc.curriculum_name";


		$count = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$whether_exam_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$whether_exam = $whether_exam_row[$val['whether_exam']];
			$val['whether_exam2'] = $whether_exam;

			$val["training_time"] = $val["training_start_time"]."~".$val["training_end_time"];
			$val["participants"] = json_decode($val["participants"],true);
		}
		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//获取题库管理
	function getPaperData(){
		$username = $_SESSION['user_info']['username'];
		$ks_question_bank = new Model("ks_question_bank");
		$fields = "q.id,q.create_time,q.create_user,q.dept_id,q.curriculum_id,q.question_title,q.topic_answers,q.type_questions,q.question_content,q.term,q.start_validity_time,q.end_validity_time,q.easy_type,q.examination_scores,c.curriculum_name";

		$curriculum_id = $_REQUEST["curriculum_id"];
		$question_title = $_REQUEST["question_title"];
		$type_questions = $_REQUEST["type_questions"];
		$easy_type = $_REQUEST["easy_type"];

		$now_time = date("Y-m-d H:i:s");
		$where = "1 ";
		$where .= " AND q.term='N' OR (q.start_validity_time<='$now_time' AND q.end_validity_time>='$now_time')";
		$where .= empty($curriculum_id) ? "" : " AND q.curriculum_id = '$curriculum_id'";
		$where .= empty($question_title) ? "" : " AND q.question_title like '%$question_title%'";
		$where .= empty($type_questions) ? "" : " AND q.type_questions = '$type_questions'";
		$where .= empty($easy_type) ? "" : " AND q.easy_type = '$easy_type'";

		$count = $ks_question_bank->table("ks_question_bank q")->field($fields)->join("ks_curriculum c on (q.curriculum_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_question_bank->table("ks_question_bank q")->field($fields)->join("ks_curriculum c on (q.curriculum_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $ks_question_bank->getLastSql();die;
		$term_row = array('Y'=>'是','N'=>'否');
		$type_questions_row = array('1'=>'单选题','2'=>'多选题','3'=>'判断题','4'=>'填空题','5'=>'问答题');
		$easy_type_row =  array('1'=>'容易','2'=>'中等','3'=>'较难');
		foreach($arrData as &$val){
			$term = $term_row[$val['term']];
			$val['term'] = $term;

			$type_questions = $type_questions_row[$val['type_questions']];
			$val['type_questions'] = $type_questions;

			$easy_type = $easy_type_row[$val['easy_type']];
			$val['easy_type'] = $easy_type;

			if($val["start_validity_time"] == "0000-00-00 00:00:00"){
				$val["start_validity_time"] = "";
			}
			if($val["end_validity_time"] == "0000-00-00 00:00:00"){
				$val["end_validity_time"] = "";
			}
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	//获取试题策略配置
	function getPolicy(){
		$ks_policy_configuration = new Model("ks_policy_configuration");
		$arrData = $ks_policy_configuration->field("id,policy_name as name,policy_score")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...","policy_score"=>"");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择...","policy_score"=>""));
		}
		echo json_encode($arrData);
	}

	//获取试卷
	function getPapers(){
		$curriculum_id = $_REQUEST["curriculum_id"];
		$ks_papers = new Model("ks_papers");

		$where = "1 ";
		$where .= " AND curriculum_id = '$curriculum_id'";

		$arrData = $ks_papers->field("id,papers_name as name")->where($where)->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}


	//获取考试计划
	function getExamProgram(){
		$curriculum_id = $_REQUEST["curriculum_id"];
		$training_date = $_REQUEST["training_date"];
		$training_start_time = $_REQUEST["training_start_time"];
		$training_end_time = $_REQUEST["training_end_time"];

		$ks_examination_program = new Model("ks_examination_program");

		$where = "1 ";
		$where .= empty($curriculum_id) ? "" : " AND curriculum_id = '$curriculum_id'";
		$where .= empty($training_date) ? "" : " AND exam_date = '$training_date'";
		$where .= empty($training_end_time) ? "" : " AND start_exam_time >= '$training_end_time'";

		$arrData = $ks_examination_program->field("id,exam_plan_name as name")->where($where)->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		echo json_encode($arrData);
	}


	//获取考试计划
	function getExamPlanData(){
		$username = $_SESSION['user_info']['username'];
		$ks_examination_program = new Model("ks_examination_program");

		$fields = "e.id,e.create_time,e.create_user,e.dept_id,e.paper_id,e.curriculum_id,e.exam_plan_name,e.exam_plan_description,e.exam_plan_address,e.exam_date,e.start_exam_time,e.end_exam_time,e.exam_type,e.are_published,e.examiners,e.reviwers,e.passing_score,e.examination_staff,p.papers_name,c.curriculum_name";


		$curriculum_id = $_REQUEST["curriculum_id"];
		$training_date = $_REQUEST["training_date"];
		$training_start_time = $_REQUEST["training_start_time"];
		$training_end_time = $_REQUEST["training_end_time"];

		$where = "1 ";
		$where .= empty($curriculum_id) ? "" : " AND e.curriculum_id = '$curriculum_id'";
		$where .= empty($training_date) ? "" : " AND e.exam_date = '$training_date'";
		$where .= empty($training_end_time) ? "" : " AND e.start_exam_time >= '$training_end_time'";


		$count = $ks_examination_program->order("e.create_time desc")->table("ks_examination_program e")->field($fields)->join("ks_papers p on (e.paper_id = p.id)")->join("ks_curriculum c on (e.curriculum_id = c.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_examination_program->order("e.create_time desc")->table("ks_examination_program e")->field($fields)->join("ks_papers p on (e.paper_id = p.id)")->join("ks_curriculum c on (e.curriculum_id = c.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $ks_examination_program->getLastSql();die;

		$exam_type_row = array('Y'=>'开卷','N'=>'闭卷');
		$are_published_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$exam_type = $exam_type_row[$val['exam_type']];
			$val['exam_type2'] = $exam_type;

			$are_published = $are_published_row[$val['are_published']];
			$val['are_published2'] = $are_published;


			$val["exam_time"] = $val["start_exam_time"]."~".$val["end_exam_time"];
			$val["examination_staff"] = json_decode($val["examination_staff"],true);
		}

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//获取问卷题型
	function getQuestionnaireTopic(){
		$wj_questionnaire_topic = new Model("wj_questionnaire_topic");
		$arrData = $wj_questionnaire_topic->field("id,topic_name as name,topic_type")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...","topic_type"=>"");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择...","topic_type"=>""));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}

	//获取问卷类型
	function getQuestionnaireType(){
		$wj_questionnaire_type = new Model("wj_questionnaire_type");
		$arrData = $wj_questionnaire_type->field("id,questionnaire_name as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}


	//获取问卷题目
	function questionnaireTopicData(){
		$username = $_SESSION['user_info']['username'];
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");

		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,t.questionnaire_type_name,t.questionnaire_type_description,p.topic_name,p.topic_type";

		$questionnaire_type_id = $_REQUEST["questionnaire_type_id"];
		$where = "1 ";
		$where .= empty($questionnaire_type_id) ? "" : " AND q.questionnaire_type_id = '$questionnaire_type_id'";

		$count = $wj_questionnaire_bank->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wj_questionnaire_bank->order("q.create_time desc")->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where($where)->select(); //->limit($page->firstRow.','.$page->listRows)
		//echo $wj_questionnaire_bank->getLastSql();die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//获取问卷题目2
	function questionnaireTopicData2(){
		$username = $_SESSION['user_info']['username'];
		$wj_questionnaire_bank = new Model("wj_questionnaire_bank");

		$fields = "q.id,q.create_time,q.create_user,q.questionnaire_type_id,q.questionnaire_topics,q.questionnaire_topic_id,q.topic_answers,q.questionnaire_content,t.questionnaire_type_name,t.questionnaire_type_description,p.topic_name,p.topic_type";

		$questionnaire_type_id = $_REQUEST["questionnaire_type_id"];
		$name = $_REQUEST["name"];

		$where = "1 ";
		$where .= empty($questionnaire_type_id) ? "" : " AND q.questionnaire_type_id = '$questionnaire_type_id'";
		$where .= empty($name) ? "" : " AND ( q.questionnaire_topics like '%$name%' OR t.questionnaire_type_name like '%$name%' OR p.topic_name like '%$name%' )";

		$count = $wj_questionnaire_bank->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $wj_questionnaire_bank->order("q.create_time desc")->table("wj_questionnaire_bank q")->field($fields)->join("wj_questionnaire_type t on (q.questionnaire_type_id = t.id)")->join("wj_questionnaire_topic p on (q.questionnaire_topic_id = p.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select(); //->limit($page->firstRow.','.$page->listRows)
		//echo $wj_questionnaire_bank->getLastSql();die;

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//获取问卷列表
	function getQuestionnaireList(){
		$wj_questionnaire_generation = new Model("wj_questionnaire_generation");
		$arrData = $wj_questionnaire_generation->field("id,questionnaire_name as name")->select();
		if($arrData){
			$arr = array("id"=>"","name"=>"请选择...");
			array_unshift($arrData,$arr);
		}else{
			$arrData = array(array("id"=>"","name"=>"请选择..."));
		}
		//dump($arrData);die;
		echo json_encode($arrData);
	}


}

?>
