<?php
//话务预测
class TrafficForecastAction extends Action{
	//话务量预测
	function trafficList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Traffic Forecast";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function trafficData(){
		$username = $_SESSION['user_info']['username'];
		$pb_traffic_forecast = new Model("pb_traffic_forecast");
		$count = $pb_traffic_forecast->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $pb_traffic_forecast->limit($page->firstRow.','.$page->listRows)->select();

		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function addTraffic(){
		$this->display();
	}

	function insertTraffic(){
		$start_time = $_REQUEST["start_time"];
		$end_time = $_REQUEST["end_time"];
		$sub_type = $_REQUEST["sub_type"];

		$cdr = new Model("asteriskcdrdb.cdr");
		$arrF = $cdr->field("sum(billsec) as talk_time,sum(if(disposition='ANSWERED' AND calltype='IN',`billsec`,0)) as inbound_answered_time,  sum(if( calltype='IN',`billsec`,0)) as inbound_time,  sum(if(disposition!='ANSWERED' AND calltype='IN',`billsec`,0)) as inbound_not_answer_time,  sum(if( calltype='OUT',`billsec`,0)) as outbound_time,  sum(if(disposition='ANSWERED' AND calltype='OUT',`billsec`,0)) as outbound_answered_time,  sum(if(disposition!='ANSWERED' AND calltype='OUT',`billsec`,0)) as outbound_not_answer_time,  count(*) as call_num,avg(billsec) as average_time, sum(case when disposition='ANSWERED' then 1 else 0 end) as turn_volume, sum(case when disposition!='ANSWERED' then 1 else 0 end) as not_turn_volume,  sum(case when disposition='ANSWERED' AND calltype='IN' then 1 else 0 end) as inbound_turn_volume,  sum(case when disposition!='ANSWERED' AND calltype='IN' then 1 else 0 end) as inbound_not_turn_volume,  sum(case when disposition='ANSWERED' AND calltype='OUT' then 1 else 0 end) as outbound_turn_volume,  sum(case when disposition!='ANSWERED' AND calltype='OUT' then 1 else 0 end) as outbound_not_turn_volume,  sum(case when calltype='IN' then 1 else 0 end) as inbound_num,  sum(case when calltype='OUT' then 1 else 0 end) as outbound_num")->where("calldate >= '$start_time' AND calldate <= '$end_time'")->select();

		foreach($arrF as &$val){
			$val["average_time"] = ceil($val["average_time"]);
		}

		if($sub_type && $sub_type == "get_info"){
			foreach($arrF as &$val){
				$val["talk_time"] = sprintf("%02d",intval($val["talk_time"]/3600)).":".sprintf("%02d",intval(($val["talk_time"]%3600)/60)).":".sprintf("%02d",intval((($val[talk_time]%3600)%60)));

				$val["inbound_time"] = sprintf("%02d",intval($val["inbound_time"]/3600)).":".sprintf("%02d",intval(($val["inbound_time"]%3600)/60)).":".sprintf("%02d",intval((($val[inbound_time]%3600)%60)));

				$val["inbound_answered_time"] = sprintf("%02d",intval($val["inbound_answered_time"]/3600)).":".sprintf("%02d",intval(($val["inbound_answered_time"]%3600)/60)).":".sprintf("%02d",intval((($val[inbound_answered_time]%3600)%60)));

				$val["inbound_not_answer_time"] = sprintf("%02d",intval($val["inbound_not_answer_time"]/3600)).":".sprintf("%02d",intval(($val["inbound_not_answer_time"]%3600)/60)).":".sprintf("%02d",intval((($val[inbound_not_answer_time]%3600)%60)));

				$val["outbound_time"] = sprintf("%02d",intval($val["outbound_time"]/3600)).":".sprintf("%02d",intval(($val["outbound_time"]%3600)/60)).":".sprintf("%02d",intval((($val[outbound_time]%3600)%60)));

				$val["outbound_answered_time"] = sprintf("%02d",intval($val["outbound_answered_time"]/3600)).":".sprintf("%02d",intval(($val["outbound_answered_time"]%3600)/60)).":".sprintf("%02d",intval((($val[outbound_answered_time]%3600)%60)));

				$val["outbound_not_answer_time"] = sprintf("%02d",intval($val["outbound_not_answer_time"]/3600)).":".sprintf("%02d",intval(($val["outbound_not_answer_time"]%3600)/60)).":".sprintf("%02d",intval((($val[outbound_not_answer_time]%3600)%60)));

				$val["average_time"] = sprintf("%02d",intval($val["average_time"]/3600)).":".sprintf("%02d",intval(($val["average_time"]%3600)/60)).":".sprintf("%02d",intval((($val[average_time]%3600)%60)));
			}
			echo json_encode(array('success'=>true,'msg'=>$arrF[0]));
			die;
			//dump($arrF);die;
		}


		$pb_traffic_forecast = new Model();

		$call_type = $_REQUEST["call_type"];
		$answering_status = $_REQUEST["answering_status"];
		$service_goals = $_REQUEST["service_goals"];
		$arrData = $arrF[0];
		if($call_type == "ALL" && $answering_status == "ALL"){
			$total_time = $arrData["talk_time"];
		}elseif($call_type == "ALL" && $answering_status == "ANSWERED"){
			$total_time = $arrData["inbound_answered_time"]+$arrData["outbound_answered_time"];
		}elseif($call_type == "ALL" && $answering_status == "NO ANSWER"){
			$total_time = $arrData["inbound_not_answer_time"]+$arrData["outbound_not_answer_time"];
		}elseif($call_type == "IN" && $answering_status == "ALL"){
			$total_time = $arrData["inbound_time"];
		}elseif($call_type == "IN" && $answering_status == "ANSWERED"){
			$total_time = $arrData["inbound_answered_time"];
		}elseif($call_type == "IN" && $answering_status == "NO ANSWER"){
			$total_time = $arrData["inbound_not_answer_time"];
		}elseif($call_type == "OUT" && $answering_status == "ALL"){
			$total_time = $arrData["outbound_time"];
		}elseif($call_type == "OUT" && $answering_status == "ANSWERED"){
			$total_time = $arrData["outbound_answered_time"];
		}elseif($call_type == "OUT" && $answering_status == "NO ANSWER"){
			$total_time = $arrData["outbound_not_answer_time"];
		}

		$arrData["start_prediction_time"] = $_REQUEST["start_prediction_time"];
		$arrData["end_prediction_time"] = $_REQUEST["start_prediction_time"];
		//$arrData["number_forecast"] = ceil(($total_time/$service_goals)/3600);
		$arrData["number_forecast"] = ceil($total_time/$service_goals);

		echo $cdr->getLastSql();
		dump($total_time);
		dump($arrData);
		die;
	}

}
?>
