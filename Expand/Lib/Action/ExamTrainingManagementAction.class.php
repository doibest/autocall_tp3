<?php
/*
培训管理
*/
class ExamTrainingManagementAction extends Action{
	//1.课程管理
	function courseList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Course Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function courseData(){
		header("Content-Type:text/html; charset=utf-8");
		$ks_curriculum = new Model("ks_curriculum");
		import('ORG.Util.Page');
		$count = $ks_curriculum->count();
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_curriculum->order("create_time desc,sort_orderid desc")->field("id,curriculum_name as text,course_pid,curriculum_description,term,course_start_time,course_end_time,approval_status,auditors,review_time")->limit($page->firstRow.','.$page->listRows)->select();

		$term_row = array("Y"=>"是",""=>"否");
		$approval_status_row = array("1"=>"待审核","2"=>"审核不通过","3"=>"审核通过");
		foreach($arrData as &$val){
			$term = $term_row[$val["term"]];
			$val["term"] = $term;

			$approval_status = $approval_status_row[$val["approval_status"]];
			$val["approval_status2"] = $approval_status;

			if($val["course_start_time"] == "0000-00-00 00:00:00"){
				$val["course_start_time"] = "";
			}
			if($val["course_end_time"] == "0000-00-00 00:00:00"){
				$val["course_end_time"] = "";
			}

			$val['state'] = "closed";
		}
		unset($i);

		//dump($arrData);
		$arrTree = $this->getTree($arrData,0,"course_pid","id");

		$j = 0;
		foreach($arrTree as $v){
			//if( $arrTree[$j]["course_pid"] == "0" && !$arrTree[$j]["children"]){
			if( $arrTree[$j]["course_pid"] == "0"){
				$arrTree[$j]['iconCls'] = "shop";  //顶级分类的图标，叶子节点的图标在getTree()方法中设置。
			}
			$j++;
		}
		unset($j);

		$rowsList = count($arrTree) ? $arrTree : false;
		$arrCat["total"] = $count;
		$arrCat["rows"] = $rowsList;
		//dump($arrTree);die;
		echo json_encode($arrCat);
	}

	function addCourse(){
		checkLogin();
		$this->display();
	}

	function insertCourse(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];
		$ks_curriculum = new Model("ks_curriculum");

		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'curriculum_name'=>$_REQUEST['text'],
			'course_pid'=>$_REQUEST['course_pid'],
			'sort_orderid'=>$_REQUEST['sort_orderid'],
			'term'=>$_REQUEST['term'],
			'course_start_time'=>$_REQUEST['course_start_time'],
			'course_end_time'=>$_REQUEST['course_end_time'],
			'curriculum_description'=>$_REQUEST['curriculum_description'],
			'approval_status'=>"1",
		);
		$result = $ks_curriculum->data($arrData)->add();
		if ($result){
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function editCourse(){
		checkLogin();
		$id = $_REQUEST["id"];
		$ks_curriculum = new Model("ks_curriculum");
		$arrData = $ks_curriculum->where("id = '$id'")->find();

		$this->assign("id",$id);
		$this->assign("arrData",$arrData);

		$this->display();
	}

	function updateCourse(){
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST['id'];
		$ks_curriculum = new Model("ks_curriculum");
		$arrData = Array(
			'curriculum_name'=>$_REQUEST['text'],
			'course_pid'=>$_REQUEST['course_pid'],
			'sort_orderid'=>$_REQUEST['sort_orderid'],
			'term'=>$_REQUEST['term'],
			'course_start_time'=>$_REQUEST['course_start_time'],
			'course_end_time'=>$_REQUEST['course_end_time'],
			'curriculum_description'=>$_REQUEST['curriculum_description'],
		);
		$result = $ks_curriculum->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function approvalCourse(){
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST['id'];
		$ks_curriculum = new Model("ks_curriculum");
		$arrData = Array(
			'approval_status'=>$_REQUEST["approval_status"],
			'auditors'=>$username,
			'review_time'=>date("Y-m-d H:i:s"),
		);
		$result = $ks_curriculum->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteCourse(){
		$id = $_REQUEST["id"];
		$ks_curriculum = new Model("ks_curriculum");
		$result = $ks_curriculum->where("id in ($id)")->delete();
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function getTree($data, $pId,$field_pid,$field_id) {
        $tree = '';
        foreach($data as $k =>$v) {
            if($v[$field_pid] == $pId)    {
				$v['children'] = $this->getTree($data, $v[$field_id],$field_pid,$field_id);

				if ( empty($v["children"])  ) {
					unset($v['children']) ;
				}
				/*
				if ( empty($v["children"]) && $v['state'] =='closed'){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}
				if ( $v["children"]  && $v['state'] =='closed'){
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				*/
				if ( empty($v["children"])){
					$v['state'] =  'open'; //让叶子节点展开
					$v['iconCls'] =  'door';   //叶子节点的图标
				}else{
					$v['iconCls'] =  'shop';  //不是顶级分类，但有叶子节点的分类的图标
				}
				$tree[] = $v;     //unset($data[$k]);
            }
        }
        return $tree;
    }


	function courseTree(){
		$ks_curriculum = new Model("ks_curriculum");

		$arrData = $ks_curriculum->order("sort_orderid desc")->field("id,curriculum_name as text,course_pid,curriculum_description")->select();
		$nodeStatus = $_GET['nodeStatus'];
		if($nodeStatus == 'colse'){
			$i = 0;
			foreach($arrData as $v){
				$arrData[$i]['state'] = "closed";
				$i++;
			}
			unset($i);
		}

		$arrTree = $this->getTree($arrData,0,"course_pid","id");
		if( $nodeStatus != 'other' && $nodeStatus !== "fitting"){
			$allCate = array(
				"text"=>"顶级课程",
				"id"=>"0",
				"course_pid"=>"0",
				"iconCls"=>"shop",
			);
			array_unshift($arrTree,$allCate);
		}
		if($nodeStatus == "fitting"){
			$allCate = array(
				"text"=>"所有分类",
				"id"=>"0",
				"course_pid"=>"0",
				"iconCls"=>"shop",
			);
			array_unshift($arrTree,$allCate);
		}

		if($nodeStatus == "open"){
			$j = 0;
			foreach($arrTree as $v){
				//if( $arrTree[$j]["course_pid"] == "0" && !$arrTree[$j]["children"]){
				if( $arrTree[$j]["course_pid"] == "0"){
					$arrTree[$j]['iconCls'] = "shop";
				}
				$j++;
			}
			unset($j);
		}
		//dump($arrData);
		$arr = array("id"=>"","text"=>"请选择...");
		array_unshift($arrTree,$arr);
		echo  json_encode($arrTree);
	}



	//2.课件管理
	function coursewareList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Courseware Management";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function coursewareData(){
		$username = $_SESSION['user_info']['username'];
		$ks_courseware = new Model("ks_courseware");

		$fields = "kj.id,kj.curriculum_id,kj.create_time,kj.create_user,kj.courseware_name,kj.courseware_description,kj.file_path_name,kj.file_type,kc.curriculum_name,kc.course_pid";

		$curriculum_id = $_REQUEST["curriculum_id"];
		$courseware_name = $_REQUEST["courseware_name"];
		$courseware_description = $_REQUEST["courseware_description"];

		$where = "1 ";
		$where .= empty($curriculum_id) ? "" : " AND kj.curriculum_id = '$curriculum_id'";
		$where .= empty($courseware_name) ? "" : " AND kj.courseware_name like '%$courseware_name%'";
		$where .= empty($courseware_description) ? "" : " AND kj.courseware_description like '%$courseware_description%'";

		$count = $ks_courseware->table("ks_courseware kj")->field($fields)->join("ks_curriculum kc on (kj.curriculum_id = kc.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_courseware->order("kj.create_time desc")->table("ks_courseware kj")->field($fields)->join("ks_curriculum kc on (kj.curriculum_id = kc.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();


		foreach($arrData as &$val){
			$val["operating"] = "<a href='#' onclick='viewCourseware(" .$val['id'] .")'>查看内容</a>" ;
			if($val["file_path_name"]){
				$val["operating"] .= " | " ."<a href='expand.php?m=ExamTrainingManagement&a=DownloadFj&name=".$val['file_path_name']."'>下载附件</a>";
			}
		}


		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}


	//下载文件
	function DownloadFj(){
		$realfile = $_GET['name'];
		if(!file_exists($realfile)){
			echo goback("没有找到 $realfile 文件","expand.php?m=ExamTrainingManagement&a=coursewareList");
		}
        header('HTTP/1.1 200 OK');
        header('Date: ' . date("D M j G:i:s T Y"));
        header('Last-Modified: ' . date("D M j G:i:s T Y"));
        header("Content-Type: application/force-download");
        header("Content-Length: " . (string)(filesize($realfile)));
        header("Content-Transfer-Encoding: Binary");
        header("Content-Disposition: attachment;filename=".str_replace(" ", "", basename($realfile))."");
        readfile($realfile);
	}


	function insertCourseware(){
		//$enterprise_number = $_SESSION['user_info']['enterprise_number'];
		//$file_path = "include/data/courseware/$enterprise_number/";
		$file_path = "include/data/courseware/";
		$this->mkdirs($file_path);
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		//$upload->maxSize = "9000000000";
		//$upload->savePath= "include/data/courseware/";  //上传路径
		$upload->savePath= $file_path;  //上传路径

		$upload->saveRule="uniqid";
		$upload->suffix="";
		$upload->uploadReplace=true;
		//$upload->allowExts=array('al');     //准许上传的文件后缀

		$username = $_SESSION['user_info']['username'];
		$content = str_replace("\\","",$_REQUEST["content"]);
		$ks_courseware=new Model('ks_courseware');

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData = array(
					"create_time" =>date("Y-m-d H:i:s"),
					"create_user" =>$username,
					"courseware_name" =>$_REQUEST["courseware_name"],
					"courseware_description" =>$content,
					"curriculum_id" =>$_REQUEST["curriculum_id"],
					"file_type" =>$_REQUEST["file_type"],
				);
				$result = $ks_courseware->data($arrData)->add();
				if ($result){
					echo json_encode(array('success'=>true,'msg'=>'faq内容添加成功！'));
				} else {
					echo json_encode(array('msg'=>'faq内容添加失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$content = $content."<br/><br/><br/>点击<a href='expand.php?m=ExamTrainingManagement&a=DownloadFj&name=".$info[0]["savepath"].$info[0]["savename"]."'>下载附件</a>";
			//dump(content);die;
			$arrData = array(
				"create_time" =>date("Y-m-d H:i:s"),
				"create_user" =>$username,
				"courseware_name" =>$_REQUEST["courseware_name"],
				"courseware_description" =>$content,
				"curriculum_id" =>$_REQUEST["curriculum_id"],
				"file_type" =>$_REQUEST["file_type"],
				"file_path_name" =>$info[0]["savepath"].$info[0]["savename"],
			);
			$result = $ks_courseware->data($arrData)->add();
			if ($result){
				echo json_encode(array('success'=>true,'msg'=>'faq内容添加成功！'));
			} else {
				echo json_encode(array('msg'=>'faq内容添加失败！'));
			}
		}


	}

	//创建多级目录
	function mkdirs($dir){
		if(!is_dir($dir)){
			if(!$this->mkdirs(dirname($dir))){
				return false;
			}
			if(!mkdir($dir,0777)){
				return false;
			}
		}
		return true;
	}

	function updateCourseware(){
		//$enterprise_number = $_SESSION['user_info']['enterprise_number'];
		$file_path = "include/data/courseware/";
		$this->mkdirs($file_path);
		import("ORG.Net.UploadFile");
		$upload = new UploadFile();
		$upload->savePath= $file_path;  //上传路径

		$upload->saveRule="uniqid";
		$upload->suffix="";
		$upload->uploadReplace=true;     //如果存在同名文件是否进行覆盖
		//$upload->allowExts=array('al');     //准许上传的文件后缀

		$id = $_REQUEST["id"];
		$content = str_replace("\\","",$_POST["content"]);
		$ks_courseware=new Model('ks_courseware');
		$arrData = $ks_courseware->where("id = '$id'")->find();

		if(!$upload->upload()){ // 上传错误提示错误信息
			$mess = $upload->getErrorMsg();
			if($mess == "没有选择上传文件"){
				$arrData2 = array(
					"courseware_name" =>$_REQUEST["courseware_name"],
					"courseware_description" =>$content,
					"curriculum_id" =>$_REQUEST["curriculum_id"],
					"file_type" =>$_REQUEST["file_type"],
				);
				$result = $ks_courseware->where("id = $id")->save($arrData2);
				if ($result !== false){
					echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
				} else {
					echo json_encode(array('msg'=>'更新失败！'));
				}
			}else{
				echo json_encode(array('msg'=>$mess));
			}
		}else{
			$info=$upload->getUploadFileInfo();
			$arrC = explode("点击",$content);
			$downloadFile = array_pop($arrC);
			if( strpos($downloadFile,"DownloadFj") && strpos($downloadFile,"ExamTrainingManagement")){
				$file = $info[0]["savepath"].$info[0]["savename"];
				if($arrData['file_path_name'] !== $file){
					$content = str_replace($arrData['file_path_name'],$file,$content);
				}else{
					$content = $content;
				}
			}else{
				$content = $content."<br/><br/><br/>点击<a href='expand.php?m=ExamTrainingManagement&a=DownloadFj&name=".$info[0]["savepath"].$info[0]["savename"]."'>下载附件</a>";
			}
			//dump($content);die;
			$arrData2 = array(
				"courseware_name" =>$_REQUEST["courseware_name"],
				"courseware_description" =>$content,
				"curriculum_id" =>$_REQUEST["curriculum_id"],
				"file_type" =>$_REQUEST["file_type"],
				"file_path_name" =>$info[0]["savepath"].$info[0]["savename"],
			);
			$result = $ks_courseware->where("id = $id")->save($arrData2);
			if ($result !== false){
				unlink($arrData["file_path_name"]);
				echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
			} else {
				echo json_encode(array('msg'=>'更新失败！'));
			}
		}

	}

	function deleteCourseware(){
		$id = $_REQUEST["id"];
		$ks_courseware=new Model('ks_courseware');
		$arrData = $ks_courseware->where("id = '$id'")->find();
		$result = $ks_courseware->where("id in ($id)")->delete();
		if ($result){
			unlink($arrData["file_path_name"]);
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function viewCourseware(){
		$id = $_REQUEST['id'];
		$ks_courseware = new Model("ks_courseware");
		$arrData = $ks_courseware->where("id = '$id'")->find();
		$arrF = explode(".",$arrData["file_path_name"]);
		$filename = $arrF[0];
		$suffix = $arrF[1];
		$suffix_old = $suffix;
		$arrPdf = array("doc","docx","xls","xlsx","ppt","pptx");
		if($suffix == "txt"){
			$arrData["file_content"] = file_get_contents($arrData["file_path_name"]);
			$arrData["file_content"] = str_replace("\r\n","<br/>",$arrData["file_content"]);
		}else{
			$arrData["file_content"] = "";
		}

		if( in_array($suffix,$arrPdf) ){
			//$suffix = "pdf";
			//$arrData["file_path_name"] = $filename.".pdf";
			$suffix = "word";
		}

		//视频
		$arrVideo = array("mp4","webm","ogv");
		if( in_array($suffix,$arrVideo) ){
			$suffix = "video";
		}
		//音频
		$arrAudio = array("mp3","wav");
		if( in_array($suffix,$arrAudio) ){
			$suffix = "audio";
		}

		//图片
		$arrPicture = array('jpg', 'gif', 'png', 'jpeg');
		if( in_array($suffix,$arrPicture) ){
			$suffix = "picture";
		}

		if($suffix_old == "mp4"){
			$media_type =  "m4v";
		}elseif($suffix_old == "webm"){
			$media_type =  "webmv";
		}elseif($suffix_old == "ogv"){
			$media_type =  "ogv";
		}elseif($suffix_old == "mp3"){
			$media_type =  "mp3";
		}elseif($suffix_old == "wav"){
			$media_type =  "wav";
		}else{
			$media_type =  "";
		}

		$this->assign("arrData",$arrData);
		$this->assign("suffix",$suffix);
		$this->assign("suffix_old",$suffix_old);
		$this->assign("media_type",$media_type);
		$CTI_IP = $_SERVER["SERVER_ADDR"];
		$this->assign("CTI_IP",$CTI_IP);

		if(file_exists($arrData["file_path_name"])){
			$this->assign("annex","Y");
		}else{
			$this->assign("annex","N");
		}
		//dump($suffix);
		//dump($arrData);die;

		$this->display();
	}

	function viewCoursewareBAK_20160224(){
		$id = $_REQUEST['id'];
		$ks_courseware = new Model("ks_courseware");
		$arrData = $ks_courseware->where("id = '$id'")->find();
		$arrF = explode(".",$arrData["file_path_name"]);
		$filename = $arrF[0];
		$suffix = $arrF[1];
		$suffix_old = $suffix;
		$arrPdf = array("doc","docx","xls","xlsx","ppt","pptx");
		if($suffix == "txt"){
			$arrData["file_content"] = file_get_contents($arrData["file_path_name"]);
			$arrData["file_content"] = str_replace("\r\n","<br/>",$arrData["file_content"]);
		}else{
			$arrData["file_content"] = "";
		}

		if( in_array($suffix,$arrPdf) ){
			//$suffix = "pdf";
			//$arrData["file_path_name"] = $filename.".pdf";
			$suffix = "word";
		}

		$this->assign("arrData",$arrData);
		$this->assign("suffix",$suffix);
		$this->assign("suffix_old",$suffix_old);
		$CTI_IP = $_SERVER["SERVER_ADDR"];
		$this->assign("CTI_IP",$CTI_IP);

		if(file_exists($arrData["file_path_name"])){
			$this->assign("annex","Y");
		}else{
			$this->assign("annex","N");
		}
		//dump($suffix);
		//dump($arrData);die;

		$this->display();
	}



	//培训计划管理
	function trainList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Training Program";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function trainData(){
		$username = $_SESSION['user_info']['username'];
		$ks_train = new Model("ks_train");

		$fields = "t.id,t.create_time,t.create_user,t.dept_id,t.curriculum_id,t.courseware_id,t.training_task_name,t.training_address,t.training_task_description,t.training_date,t.training_start_time,t.training_end_time,t.whether_exam,t.ks_id,t.participants,t.lectures_people,c.courseware_name,kc.curriculum_name,e.exam_plan_name";

		$start_training_date = $_REQUEST["start_training_date"];
		$end_training_date = $_REQUEST["end_training_date"];
		$training_task_name = $_REQUEST["training_task_name"];
		$curriculum_id = $_REQUEST["curriculum_id"];
		$courseware_id = $_REQUEST["courseware_id"];

		$where = "1 ";
		$where .= empty($start_training_date) ? "" : " AND t.training_date >= '$start_training_date'";
		$where .= empty($end_training_date) ? "" : " AND t.training_date <= '$end_training_date'";
		$where .= empty($training_task_name) ? "" : " AND t.training_task_name like '%$training_task_name%'";
		$where .= empty($curriculum_id) ? "" : " AND t.curriculum_id = '$curriculum_id'";
		$where .= empty($courseware_id) ? "" : " AND t.courseware_id = '$courseware_id'";

		$count = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->join("ks_examination_program e on (t.ks_id = e.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->join("ks_examination_program e on (t.ks_id = e.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$whether_exam_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$whether_exam = $whether_exam_row[$val['whether_exam']];
			$val['whether_exam2'] = $whether_exam;

			$val["training_time"] = $val["training_start_time"]."~".$val["training_end_time"];
			$val["participants"] = json_decode($val["participants"],true);
			$val["participants2"] = implode(",",$val["participants"]);

			if($val["participants2"]){
				$val["operating"] = "<a href='javascript:void(0);' onclick=\"openUsers("."'".$val["id"]."'".")\" >查看培训人员</a>";
			}
		}
		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertTrain(){
		$username = $_SESSION['user_info']['username'];
		$d_id = $_SESSION['user_info']['d_id'];

		$participants = $_REQUEST['participants'];
		$arrF = explode(",",$participants);
		$training_start_time2 = $_REQUEST['training_date']." ".$_REQUEST['training_start_time'].":00";
		$ks_train = new Model("ks_train");
		$arrData = Array(
			'create_time'=>date("Y-m-d H:i:s"),
			'create_user'=>$username,
			'dept_id'=>$d_id,
			'training_task_name'=>$_REQUEST['training_task_name'],
			'training_address'=>$_REQUEST['training_address'],
			'courseware_id'=>$_REQUEST['courseware_id'],
			'curriculum_id'=>$_REQUEST['curriculum_id'],
			'training_date'=>$_REQUEST['training_date'],
			'training_start_time'=>$_REQUEST['training_start_time'],
			'training_end_time'=>$_REQUEST['training_end_time'],
			'whether_exam'=>$_REQUEST['whether_exam'],
			'lectures_people'=>$_REQUEST['lectures_people'],
			'ks_id'=>$_REQUEST['ks_id'],
			'participants'=>json_encode($arrF),
			'training_start_time2'=>$training_start_time2,
		);
		if($_REQUEST['whether_exam'] == "Y"){
			$ks_examination_program = new Model("ks_examination_program");
			$where = "id = '".$_REQUEST['ks_id']."' AND curriculum_id = '".$_REQUEST['curriculum_id']."' AND exam_date = '".$_REQUEST['training_date']."' AND start_exam_time >= '".$_REQUEST['training_end_time']."'";
			$count = $ks_examination_program->where($where)->count();
			if($count == "0"){
				echo json_encode(array('msg'=>'没有找到符合条件的考试计划，请您选择一条符合这个培训计划的考试计划，如果没有，请您添加！'));
				die;
			}
		}
		$result = $ks_train->data($arrData)->add();
		if ($result){
			$train_staff = new Model("ks_train_staff");
			//$del = $train_staff->where("train_id = '$result'")->delete();
			$sql = "insert into ks_train_staff(`train_id`,`trainers`) values";
			$value = "";
			foreach($arrF as $val){
				$str = "('".$result."','".$val."')";
				$value .= empty($value) ? "$str" : ",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $train_staff->execute($sql);
				if($res){
					$this->insertPushData($arrF,$result,$training_start_time2);
				}
			}
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function updateTrain(){
		$id = $_REQUEST['id'];
		$ks_train = new Model("ks_train");
		$participants = $_REQUEST['participants'];
		$arrF = explode(",",$participants);
		$training_start_time2 = $_REQUEST['training_date']." ".$_REQUEST['training_start_time'].":00";
		$arrData = Array(
			'training_task_name'=>$_REQUEST['training_task_name'],
			'training_address'=>$_REQUEST['training_address'],
			'courseware_id'=>$_REQUEST['courseware_id'],
			'curriculum_id'=>$_REQUEST['curriculum_id'],
			'training_date'=>$_REQUEST['training_date'],
			'training_start_time'=>$_REQUEST['training_start_time'],
			'training_end_time'=>$_REQUEST['training_end_time'],
			'whether_exam'=>$_REQUEST['whether_exam'],
			'ks_id'=>$_REQUEST['ks_id'],
			'lectures_people'=>$_REQUEST['lectures_people'],
			'participants'=>json_encode($arrF),
			'training_start_time2'=>$training_start_time2,
		);
		if($_REQUEST['whether_exam'] == "Y"){
			$ks_examination_program = new Model("ks_examination_program");
			$where = "id = '".$_REQUEST['ks_id']."' AND curriculum_id = '".$_REQUEST['curriculum_id']."' AND exam_date = '".$_REQUEST['training_date']."' AND start_exam_time >= '".$_REQUEST['training_end_time']."'";
			$count = $ks_examination_program->where($where)->count();
			if($count == "0"){
				echo json_encode(array('msg'=>'没有找到符合条件的考试计划，请您选择一条符合这个培训计划的考试计划，如果没有，请您添加！'));
				die;
			}
		}

		//dump($count);die;
		//dump($participants);die;
		$result = $ks_train->data($arrData)->where("id = '$id'")->save();
		if ($result !== false){
			$train_staff = new Model("ks_train_staff");
			$del = $train_staff->where("train_id = '$id'")->delete();
			$sql = "insert into ks_train_staff(`train_id`,`trainers`) values";
			$value = "";
			foreach($arrF as $val){
				$str = "('".$id."','".$val."')";
				$value .= empty($value) ? "$str" : ",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $train_staff->execute($sql);
				if($res){
					$this->insertPushData($arrF,$id,$training_start_time2);
				}
			}

			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}


	function insertPushData($arrF,$id,$push_time){
		$checkRole = getSysinfo();
		$arrAL = explode(",",$checkRole[2]);
		if( in_array("zh",$arrAL) ){
			$push_data_table = "crm_public.push_data";
		}else{
			$push_data_table = "push_data";
		}
		$push_data = new Model("$push_data_table");
		$time = date("Y-m-d",strtotime("-1 day"))." 23:59:59";
		//echo $time;die;
		$del = $push_data->where("(push_type='exam' AND customer_id='$id') OR push_time < '$time'")->delete();
		$sql = "insert into $push_data_table(`customer_id`,`push_user`,`push_time`,`push_type`) values";
			$value = "";
			foreach($arrF as $val){
				$str = "(";
				$str .= "'" .$id. "',";
				$str .= "'" .$val. "',";
				$str .= "'" .$push_time. "',";
				$str .= "'train'";

				$str .= ")";
				$value .= empty($value)?"$str":",$str";
			}
			if( $value ){
				$sql .= $value;
				$res = $push_data->execute($sql);
			}
	}

	function deleteTrain(){
		$id = $_REQUEST["id"];
		$ks_train = new Model("ks_train");
		$result = $ks_train->where("id in ($id)")->delete();
		if ($result){
			$train_staff = new Model("ks_train_staff");
			$del = $train_staff->where("train_id = '$id'")->delete();
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	function shiftUserData($date,$start_time,$end_time){
		//$date = "2015-03-20";
		//$start_time = "15:30";
		//$end_time = "17:30";
		$start = $date." ".$start_time;
		$end = $date." ".$end_time;

		$db_name = empty($_SESSION["db_name"]) ? "bgcrm" : $_SESSION["db_name"];
		//$arrShift = require "BGCC/Conf/crm/$db_name/shiftSet.php";
		$arrShift = getShiftSet();
		$WorkingHours = $arrShift["WorkingHours"];

		foreach($WorkingHours as $key=>&$val){
			$arrF[] = array(
				"shift_id"=>$key,
				"start_time"=>$date." ".array_shift(explode("~",$val)),
				"end_time"=>$date." ".array_pop(explode("~",$val)),
			);
		}

		foreach($arrF as &$val){
			/*
			班次开始时间 - 培训开始时间 >0   表示开始培训的时候 就上班次了
			班次开始时间 - 培训结束时间 <0  表示开始培训的时候 就上班次了
			*/
			$val["start_diff"] = strtotime($start)-strtotime($val["start_time"]);
			//$val["end_diff"] = strtotime($end)-strtotime($val["end_time"]);
			$val["end_diff"] = strtotime($start)-strtotime($val["end_time"]);

			if( (strtotime($val["start_time"]) >= strtotime($start) && strtotime($val["start_time"])<= strtotime($end)) ||  ( strtotime($val["end_time"]) >= strtotime($start) && strtotime($val["end_time"])<= strtotime($end) ) || ($val["start_diff"]>0 && $val["end_diff"]<0) ){
				$arrID[] = $val["shift_id"];
			}

		}
		$id = implode(",",$arrID);
		$pb_scheduling = new Model("pb_scheduling");
		$arrData = $pb_scheduling->where("scheduling_dates = '$date' AND shift_id in ($id)")->select();

		foreach($arrData as $val){
			$arrU[] = $val["user_name"];
			$arrID[$val["user_name"]] = $val["shift_id"];
		}
		$str = "'".implode("','",$arrU)."'";
		//dump($str);
		//dump($arrF);die;

		$result = array(
			"string" => $str,
			"arrUser" => $arrU,
			"arrID" => $arrID,
		);

		return $result;
	}

	//用户搜索
	function userData(){
		$users = new Model("users");
		$name = $_REQUEST['name'];

		$date = $_REQUEST['date'];
		$start_time = $_REQUEST['start_time'];
		$end_time = $_REQUEST['end_time'];
		$arrT = $this->shiftUserData($date,$start_time,$end_time);
		$str_user = $arrT["string"];
		$arr = $arrT["arrUser"];
		$arrID = $arrT["arrID"];


		$where = "1 ";
		$where .= " AND username not in ($str_user)";
		$where .= empty($name)?"":" AND ( username like '%$name%' OR cn_name like '%$name%'  OR extension like '%$name%' )";

		$count = $users->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$userData = $users->field("username,extension,d_id,r_id,cn_name,en_name,email,fax,extension_type,extension_mac,phone,out_pin,routeid")->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		foreach($userData as &$val){
			if(in_array($val["username"],$arr)){
				$val["shift_id"] = $arrID[$val["username"]];
			}else{
				$val["shift_id"] = " ";
			}
			$val["cn_name"] = $val["username"]."/".$val["cn_name"];
		}

		//dump($userData);die;

		$rowsList = count($userData) ? $userData : false;
		$arrU["total"] = $count;
		$arrU["rows"] = $rowsList;

		echo json_encode($arrU);
	}

	function trainUsersList(){
		checkLogin();
		$id = $_REQUEST["id"];
		$web_type = $_REQUEST["web_type"];

		if($web_type == "train"){
			$ks_train = new Model("ks_train");
			$arrF = $ks_train->where("id = '$id'")->find();
			$this->assign("arrF",$arrF);
		}else{
			$ks_examination_program = new Model("ks_examination_program");
			$arrF = $ks_examination_program->field("id,exam_date as training_date,start_exam_time as training_start_time,end_exam_time as training_end_time")->where("id = '$id'")->find();
			$this->assign("arrF",$arrF);
		}
		$this->assign("id",$id);
		$this->assign("web_type",$web_type);

		$this->display();
	}

	function trainUsersData(){
		$id = $_REQUEST["id"];
		$ks_train_staff = new Model("ks_train_staff");
		$count = $ks_train_staff->table("ks_train_staff t")->field("t.train_id,t.trainers,u.username,u.en_name,u.cn_name,u.extension")->join("users u on (t.trainers = u.username)")->where("t.train_id = '$id'")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_train_staff->table("ks_train_staff t")->field("t.train_id,t.trainers,u.username,u.en_name,u.cn_name,u.extension")->join("users u on (t.trainers = u.username)")->where("t.train_id = '$id'")->limit($page->firstRow.','.$page->listRows)->select();


		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function shiftUsersData(){
		$users = new Model("users");
		$name = $_REQUEST['name'];

		$date = $_REQUEST['date'];
		$start_time = $_REQUEST['start_time'];
		$end_time = $_REQUEST['end_time'];
		$arrT = $this->shiftUserData($date,$start_time,$end_time);
		//dump($arrT);die;
		$str_user = $arrT["string"];


		$where = "1 ";
		$where .= " AND username in ($str_user)";
		$count = $users->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$userData = $users->field("username,extension,d_id,r_id,cn_name,en_name,email,fax,extension_type,extension_mac,phone,out_pin,routeid")->where($where)->limit($page->firstRow.','.$page->listRows)->select();

		//dump($userData);die;

		$rowsList = count($userData) ? $userData : false;
		$arrU["total"] = $count;
		$arrU["rows"] = $rowsList;

		echo json_encode($arrU);
	}


	function examUsersData(){
		$id = $_REQUEST["id"];
		$ks_examination_staff = new Model("ks_examination_staff");
		$count = $ks_examination_staff->table("ks_examination_staff e")->field("e.examination_id,e.trainers,u.username,u.en_name,u.cn_name,u.extension")->join("users u on (e.trainers = u.username)")->where("e.examination_id = '$id'")->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_examination_staff->table("ks_examination_staff e")->field("e.examination_id,e.trainers,u.username,u.en_name,u.cn_name,u.extension")->join("users u on (e.trainers = u.username)")->where("e.examination_id = '$id'")->limit($page->firstRow.','.$page->listRows)->select();
		//echo  $ks_examination_staff->getLastSql();die;


		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	//自选课程
	function electiveList(){
		checkLogin();
		//分配增删改的权限
		$menuname = "Elective Seats";
		$p_menuname = $_SESSION['menu'][$menuname]; //父菜单
		$priv = $_SESSION["user_priv"][$p_menuname][$menuname];

		$user_name2 = $_SESSION['user_info']['username'];
		$arrAdmin = getAdministratorNum();
		if( in_array($user_name2,$arrAdmin) ){
			$this->assign("username","admin");
		}else{
			$this->assign("username",$user_name2);
		}

		$this->assign("priv",$priv);

		$this->display();
	}

	function electiveData(){
		//header("Content-Type:text/html; charset=utf-8");
		$username = $_SESSION['user_info']['username'];
		$ks_train = new Model("ks_train");

		$start_training_date = $_REQUEST["start_training_date"];
		$end_training_date = $_REQUEST["end_training_date"];
		$training_task_name = $_REQUEST["training_task_name"];
		$curriculum_id = $_REQUEST["curriculum_id"];
		$trainers = $_REQUEST["trainers"];

		$where = "t.participants is not null AND t.participants != 'null'";
		if($username != "admin"){
			$where .= " AND ts.trainers='$username'";
		}
		$where .= empty($start_training_date) ? "" : " AND t.training_date >= '$start_training_date'";
		$where .= empty($end_training_date) ? "" : " AND t.training_date <= '$end_training_date'";
		$where .= empty($training_task_name) ? "" : " AND t.training_task_name like '%$training_task_name%'";
		$where .= empty($curriculum_id) ? "" : " AND t.curriculum_id = '$curriculum_id'";
		$where .= empty($trainers) ? "" : " AND ts.trainers = '$trainers'";

		$fields = "t.id,t.create_time,t.create_user,t.dept_id,t.curriculum_id,t.courseware_id,t.training_task_name,t.training_address,t.training_task_description,t.training_date,t.training_start_time,t.training_end_time,t.whether_exam,t.ks_id,t.participants,c.courseware_name,kc.curriculum_name,ts.trainers";


		$count = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->join("ks_train_staff ts on (t.id = ts.train_id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->join("ks_train_staff ts on (t.id = ts.train_id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		$whether_exam_row = array('Y'=>'是','N'=>'否');
		foreach($arrData as &$val){
			$whether_exam = $whether_exam_row[$val['whether_exam']];
			$val['whether_exam2'] = $whether_exam;

			$val["training_time"] = $val["training_start_time"]."~".$val["training_end_time"];
			$val["participants"] = json_decode($val["participants"],true);
		}
		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

	function insertElective(){
		$id = $_REQUEST['id'];
		$username = $_SESSION['user_info']['username'];
		$train_staff = new Model("ks_train_staff");
		$count = $train_staff->where("train_id = '$id' AND trainers = '$username'")->count();
		if($count > 0){
			echo json_encode(array('msg'=>'您已经参加了这个培训任务！'));
			die;
		}
		$arrData = Array(
			'train_id'=>$id,
			'trainers'=>$username,
		);
		$result = $train_staff->data($arrData)->add();
		if ($result){
			$this->saveTrainers($id);
			echo json_encode(array('success'=>true,'msg'=>'添加成功！'));
		} else {
			echo json_encode(array('msg'=>'添加失败！'));
		}
	}

	function saveTrainers($id){
		$train_staff = new Model("ks_train_staff");
		$arrData = $train_staff->where("train_id = '$id'")->select();
		foreach($arrData as $val){
			$arrF[] = $val["trainers"];
		}
		$ks_train = new Model("ks_train");
		$result = $ks_train->where("id = '$id'")->save(array("participants"=>json_encode($arrF)));
	}

	function updateElective(){
		$username = $_SESSION['user_info']['username'];
		$id = $_REQUEST['id'];
		$trainers = $_REQUEST['trainers'];
		if($username != "admin"){
			if($username != $trainers){
				echo json_encode(array('msg'=>'不是本人操作,无法修改！'));
				die;
			}
		}
		$train_staff = new Model("ks_train_staff");
		$count = $train_staff->where("train_id = '$id' AND trainers = '$trainers'")->count();
		if($count > 0){
			echo json_encode(array('msg'=>'您已经参加了这个培训任务！'));
			die;
		}
		$arrData = Array(
			'train_id'=>$id,
			//'trainers'=>$username,
		);
		$result = $train_staff->data($arrData)->where("train_id = '$id' AND trainers = '$trainers'")->save();
		if ($result !== false){
			$this->saveTrainers($id);
			echo json_encode(array('success'=>true,'msg'=>"更新成功！"));
		} else {
			echo json_encode(array('msg'=>'更新失败！'));
		}
	}

	function deleteElective(){
		$id = $_REQUEST['id'];
		$trainers = $_REQUEST['trainers'];
		$train_staff = new Model("ks_train_staff");
		$result = $train_staff->where("train_id = '$id' AND trainers = '$trainers'")->delete();
		//echo $train_staff->getLastSql();die;
		if ($result){
			$this->saveTrainers($id);
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('msg'=>'删除失败'));
		}
	}

	//培训列表
	function agentTrainingList(){
		checkLogin();
		$username = $_SESSION['user_info']['username'];
		$this->assign("username",$username);
		$this->display();
	}

	function agentTrainingData(){
		$username = $_SESSION['user_info']['username'];
		$ks_train = new Model("ks_train");

		$fields = "t.id,t.create_time,t.create_user,t.dept_id,t.curriculum_id,t.courseware_id,t.training_task_name,t.training_address,t.training_task_description,t.training_date,t.training_start_time,t.training_end_time,t.whether_exam,t.ks_id,t.participants,t.lectures_people,c.courseware_name,kc.curriculum_name,s.trainers";


		$date = date("Y-m-d");
		$where = "t.training_date = '$date'";
		if($username != 'admin'){
			$where .= " AND s.trainers = '$username'";
		}

		$count = $ks_train->table("ks_train t")->field($fields)->join("ks_train_staff s on (t.id = s.train_id)")->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->where($where)->count();
		import('ORG.Util.Page');
		$_GET["p"] = $_REQUEST["page"];
		if(!$_REQUEST["rows"]){
			$para_sys = readS();
			$page_rows = $para_sys["page_rows"];
		}else{
			$page_rows = $_REQUEST["rows"];
		}
		$page = new Page($count,$page_rows);

		$arrData = $ks_train->order("t.create_time desc")->table("ks_train t")->field($fields)->join("ks_train_staff s on (t.id = s.train_id)")->join("ks_courseware c on (t.courseware_id = c.id)")->join("ks_curriculum kc on (t.curriculum_id = kc.id)")->limit($page->firstRow.','.$page->listRows)->where($where)->select();
		//echo $ks_train->getLastSql();die;

		foreach($arrData as &$val){
			$val["training_time"] = $val["training_start_time"]."~".$val["training_end_time"];
			$val["operating"] = "<a href='javascript:void(0);' onclick=\"openCourseware("."'".$val["courseware_id"]."'".")\" >查看课件</a>";
		}
		//dump($arrData);die;
		$rowsList = count($arrData) ? $arrData : false;
		$arrT["total"] = $count;
		$arrT["rows"] = $rowsList;

		echo json_encode($arrT);
	}

}
?>
