CREATE TABLE `pb_shift_definition` (
  `id` int(11) NOT NULL auto_increment,
  `createtime` datetime default NULL COMMENT '创建时间',
  `createname` varchar(50) default NULL COMMENT '创建人',
  `dept_id` int(11) default NULL COMMENT '部门id',
  `shift_name` varchar(120) default NULL COMMENT '班次名称',
  `start_working_hours` varchar(10) default NULL COMMENT '开始上班时间',
  `end_working_hours` varchar(10) default NULL COMMENT '上班结束时间',
  `hours` varchar(10) default NULL COMMENT '工时',
  `shift_background` varchar(10) default NULL COMMENT '班次底色',
  `subsidy` varchar(20) default NULL COMMENT '补贴',
  `max_classes` int(11) default NULL COMMENT '该班次员工每周最大上班数',
  `limited_duty` text COMMENT '限定值班人员',
  `description` text COMMENT '描述',
  `shift_status` varchar(10) default 'Y' COMMENT '状态【Y：表示启用，N：表示禁用】',
  `modify_time` datetime default NULL COMMENT '修改时间',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`createname`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='班次定义'