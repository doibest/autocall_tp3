CREATE TABLE `pb_attendance` (
  `id` int(11) NOT NULL auto_increment,
  `user_name` varchar(50) default NULL COMMENT '工号',
  `dept_id` int(11) default NULL COMMENT '部门id',
  `create_date` date default NULL COMMENT '打卡日期/创建日期/请假日期',
  `week_day` varchar(20) default NULL COMMENT '工作日中的周几',
  `start_time` varchar(20) default NULL COMMENT '上班打卡时间',
  `end_time` varchar(20) default NULL COMMENT '下班打卡时间',
  `are_late` char(1) default 'N' COMMENT '上班是否迟到【Y：表示迟到，N：表示没有迟到，T：表示没有打卡】',
  `leave_early` char(1) default 'N' COMMENT '上班是否早退【Y：表示早退，N：表示没有早退，T：表示没有打卡】',
  `whether_leave` char(1) default 'N' COMMENT '是否请假【Y：表示请假，N：表示没有请假】',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`user_name`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='考勤记录表'