CREATE TABLE `pb_holidays` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建人',
  `dept_id` int(11) default NULL COMMENT '部门id',
  `holiday_name` varchar(255) default NULL COMMENT '节假日名称',
  `holiday_description` text COMMENT '节假日描述',
  `holiday_dates` text COMMENT '节假日日期【json数据】',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='节假日设定表'