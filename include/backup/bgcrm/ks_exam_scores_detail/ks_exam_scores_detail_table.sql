CREATE TABLE `ks_exam_scores_detail` (
  `exam_scores_id` int(11) default NULL COMMENT '关联ks_exam_scores表id',
  `question_id` int(11) default NULL COMMENT '关联ks_question_bank表id',
  `question_type` varchar(10) default NULL COMMENT '试题类型',
  `fill_answer` text COMMENT '填写的答案',
  `answer_correct` char(1) default NULL COMMENT '该题是否答对了【Y：答对，N：答错】',
  `scores` int(11) default NULL COMMENT '该题得分',
  `reviewers_comments` text COMMENT '评阅意见',
  KEY `NewIndex1` (`exam_scores_id`,`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='考试详细分数'