CREATE TABLE `work_workflow_ticket_none` (
  `id` int(11) NOT NULL auto_increment,
  `work_workflow_id` int(11) default NULL COMMENT '工作流工单id【work_workflow_ticket】',
  `create_user` varchar(50) default NULL COMMENT '创建人',
  `create_time` datetime default NULL COMMENT '创建时间',
  `process_node_ids` int(11) default NULL COMMENT '流程节点id【work_process_design_node】',
  `process_status` varchar(50) default NULL COMMENT '流程状态',
  `user_name` text COMMENT '向这些人员发送事务提醒消息',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`work_workflow_id`,`create_user`,`process_node_ids`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='工作流工单的节点表'