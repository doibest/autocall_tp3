CREATE TABLE `pb_event` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建人',
  `dept_id` int(11) default NULL COMMENT '部门id',
  `event_name` varchar(255) default NULL COMMENT '事件名称',
  `event_date` date default NULL COMMENT '事件的发生日期',
  `event_start_time` varchar(20) default NULL COMMENT '事件的发生开始时间',
  `event_end_time` varchar(20) default NULL COMMENT '事件的发生结束时间',
  `traffic_rate` varchar(20) default NULL COMMENT '话务量的变化率',
  `traffic_abs` varchar(20) default NULL COMMENT '话务量变化的绝对值',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='突发事件管理'