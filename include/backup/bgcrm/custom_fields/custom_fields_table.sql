CREATE TABLE `custom_fields` (
  `id` int(11) NOT NULL auto_increment,
  `table_name` varchar(50) default NULL COMMENT '数据表名',
  `cn_name` varchar(100) default NULL COMMENT '字段描述',
  `en_name` varchar(100) default NULL COMMENT '字段名称',
  `field_attr` varchar(100) default NULL COMMENT '字段属性',
  `field_width` int(3) default NULL COMMENT '字段长度',
  `field_enabled` char(1) default 'Y' COMMENT '是否禁用',
  `list_enabled` char(1) default 'Y' COMMENT '是否显示',
  `tiptools_enabled` char(1) default 'N' COMMENT '表格中的提示工具(当单元格无法显示全部内容时，提示工具显示出所有内容)',
  `field_order` int(11) default NULL COMMENT '字段排序',
  `text_type` char(1) default NULL COMMENT '文本类型',
  `field_values` text COMMENT '选择框的值',
  `required_enabled` char(1) default 'N' COMMENT '是否必填',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8