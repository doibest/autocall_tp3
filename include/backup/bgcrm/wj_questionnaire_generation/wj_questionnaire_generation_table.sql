CREATE TABLE `wj_questionnaire_generation` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建人',
  `dept_id` int(11) default NULL,
  `questionnaire_type_id` int(11) default NULL COMMENT '问卷类型id',
  `questionnaire_name` varchar(255) default NULL COMMENT '问卷名称',
  `whether_to_audit` char(1) default NULL COMMENT '是否审核【Y：需要审核，N：不需要审核】',
  `questionnaire_description` text COMMENT '问卷描述',
  `topics_ids` text COMMENT '题目id【json数据】',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`,`questionnaire_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='问卷管理'