CREATE TABLE `wx_group` (
  `group_id` int(11) default NULL,
  `group_name` varchar(100) default NULL COMMENT '分组名称',
  `user_count` int(11) default NULL COMMENT '该分组下的用户数',
  UNIQUE KEY `NewIndex1` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信分组'