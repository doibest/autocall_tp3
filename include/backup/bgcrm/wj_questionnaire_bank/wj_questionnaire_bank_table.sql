CREATE TABLE `wj_questionnaire_bank` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建人',
  `dept_id` int(11) default NULL,
  `questionnaire_type_id` int(11) default NULL COMMENT '问卷类型id',
  `questionnaire_topics` varchar(255) default NULL COMMENT '问卷题目',
  `questionnaire_topic_id` int(11) default NULL COMMENT '问卷题型id',
  `topic_answers` text COMMENT '问卷题目答案',
  `questionnaire_content` text COMMENT '题目内容【json数据】',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`,`questionnaire_type_id`,`questionnaire_topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='问卷题库'