CREATE TABLE `ks_train_staff` (
  `train_id` int(11) default NULL COMMENT '关联ks_train表id',
  `trainers` varchar(50) default NULL COMMENT '培训人员/参加培训的坐席',
  `shift_id` int(11) default NULL COMMENT '班次id/判断改坐席在这个培训计划中是否安排了班次',
  KEY `NewIndex1` (`train_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='培训计划管理----参加人员列表'