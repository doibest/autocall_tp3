CREATE TABLE `wx_news_content` (
  `new_id` int(11) default NULL COMMENT '关联wx_news的id',
  `order` int(11) default NULL COMMENT '索引id【新闻索引】',
  `title` varchar(255) default NULL COMMENT '标题',
  `content` text COMMENT '摘要',
  `description` text COMMENT '描述、内容',
  `img_path` varchar(100) default NULL COMMENT '图片路径、名称',
  `news_url` varchar(255) default NULL COMMENT '图文信息路径',
  KEY `NewIndex1` (`new_id`,`order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信图文内容表'