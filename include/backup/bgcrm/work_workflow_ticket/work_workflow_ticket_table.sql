CREATE TABLE `work_workflow_ticket` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `applicant` varchar(50) default NULL COMMENT '发起人/申请人',
  `working_name` varchar(255) default NULL COMMENT '工作名称',
  `form_design_id` int(11) default NULL COMMENT '设计表单id【work_form_design确定表单表名】',
  `form_id` int(11) default NULL COMMENT '表单id【work_form_N】',
  `process_design_id` int(11) default NULL COMMENT '流程设计id【work_process_design】',
  `process_node_id` int(11) default NULL COMMENT '流程节点id【work_process_design_node】转交时用',
  `receiving_time` datetime default NULL COMMENT '接收时间',
  `process_node_list_id` int(11) default NULL COMMENT '流程节点id【work_process_design_node】列表时用',
  `first_step` char(1) default 'Y' COMMENT '是否为第一步【Y：是，N：否】',
  `work_status` varchar(20) default NULL COMMENT '工作状态',
  `work_result` char(1) default 'T' COMMENT '处理结果【Y：同意，N：不同意,T：未知[办理中]】',
  `user_name` text COMMENT '那些人可以看到这条资料',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`applicant`,`form_design_id`,`form_id`,`process_design_id`,`process_node_id`,`process_node_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='工作流工单表'