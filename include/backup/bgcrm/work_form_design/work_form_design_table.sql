CREATE TABLE `work_form_design` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL,
  `create_user` varchar(50) default NULL,
  `dept_id` int(11) default NULL COMMENT '表单应用部门【那个部门能用这个表单】',
  `form_name` varchar(255) default NULL COMMENT '表单名称',
  `form_category_id` int(11) default NULL COMMENT '表单分类',
  `form_description` text COMMENT '表单描述',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`,`form_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='设计表单'