CREATE TABLE `ks_question_bank_select` (
  `question_id` int(11) default NULL COMMENT '关联ks_question_bank表的id',
  `select_value` varchar(50) default NULL COMMENT '选择题的value值',
  `select_name` varchar(255) default NULL COMMENT '选择题的值',
  `select_enabled` char(1) default 'Y' COMMENT '是否显示 Y：显示，N：不显示',
  `select_order` int(11) default NULL,
  KEY `NewIndex1` (`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='试题内容'