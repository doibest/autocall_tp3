CREATE TABLE `wx_news` (
  `id` int(11) NOT NULL auto_increment,
  `createname` varchar(50) default NULL COMMENT '创建图文信息的坐席',
  `createtime` datetime default NULL COMMENT '创建时间',
  `newtype` char(1) default NULL COMMENT '图文类型，1：表示单图文，2：表示多图文',
  `sendMsg` text COMMENT '发送图文消息内容',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='微信图文表'