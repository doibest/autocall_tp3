CREATE TABLE `ks_papers` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建人',
  `dept_id` int(11) default NULL,
  `curriculum_id` int(11) default NULL COMMENT '课程id',
  `policy_id` int(11) default NULL COMMENT '试题策略id',
  `papers_name` varchar(255) default NULL COMMENT '试卷名称',
  `score_papers` int(11) default NULL COMMENT '试卷分数',
  `true_score` int(11) default NULL COMMENT '真实分数/随机选题时的分数',
  `topics_way` varchar(20) default NULL COMMENT '选题方式【1：人工选题，2：随机选题】',
  `questions_ids` text COMMENT '题目id【接送数据】',
  `papers_describe` text COMMENT '试卷描述',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`,`curriculum_id`,`policy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='试卷管理'