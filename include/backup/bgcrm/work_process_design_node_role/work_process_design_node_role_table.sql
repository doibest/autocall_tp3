CREATE TABLE `work_process_design_node_role` (
  `design_node_id` int(11) default NULL COMMENT '流程节点表id【work_process_design_node】',
  `user_name` varchar(50) default NULL COMMENT '工号',
  `dept_id` int(11) default NULL COMMENT '部门id',
  `role_id` int(11) default NULL COMMENT '角色id',
  KEY `NewIndex1` (`design_node_id`,`dept_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='步骤节点权限'