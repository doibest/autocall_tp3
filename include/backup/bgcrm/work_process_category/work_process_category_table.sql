CREATE TABLE `work_process_category` (
  `id` int(11) NOT NULL auto_increment,
  `category_name` varchar(90) default NULL COMMENT '分类名称',
  `category_pid` int(11) default NULL COMMENT '上级名称id',
  `sort_orderid` int(11) default NULL COMMENT '排序',
  `dept_id` int(11) default NULL COMMENT '所属部门',
  `category_enabled` char(1) default 'Y' COMMENT '是否显示，Y：显示，N:不显示',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`dept_id`,`category_pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='流程分类'