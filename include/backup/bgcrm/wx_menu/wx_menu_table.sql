CREATE TABLE `wx_menu` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL COMMENT '菜单标题，不超过16个字节，子菜单不超过40个字节 ',
  `pid` int(11) default NULL COMMENT '父菜单id',
  `action_type` varchar(10) default NULL COMMENT '菜单的响应动作类型，目前有click、view两种类型 ',
  `event_value` varchar(255) default NULL COMMENT '菜单KEY值或网页链接',
  `wx_order` int(4) default NULL COMMENT '排序',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信自定义菜单'