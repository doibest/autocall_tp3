CREATE TABLE `ks_curriculum` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建者',
  `dept_id` int(11) default NULL COMMENT '部门id',
  `curriculum_name` varchar(100) default NULL COMMENT '课程名称',
  `course_pid` int(11) default NULL COMMENT '课程pid',
  `curriculum_description` text COMMENT '课程描述',
  `term` char(1) default NULL COMMENT '是否有期限【Y：有期限，N：无期限】',
  `course_start_time` datetime default NULL COMMENT '开课开始时间',
  `course_end_time` datetime default NULL COMMENT '开课结束时间',
  `approval_status` varchar(20) default NULL COMMENT '审核状态【1：待审核，2：审核不通过，3：审核通过】',
  `auditors` varchar(50) default NULL COMMENT '审核人员',
  `review_time` datetime default NULL COMMENT '审核时间',
  `sort_orderid` int(11) default NULL COMMENT '排序',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='课程表'