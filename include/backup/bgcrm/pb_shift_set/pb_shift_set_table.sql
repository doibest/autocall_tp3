CREATE TABLE `pb_shift_set` (
  `id` int(11) NOT NULL auto_increment,
  `shift_id` int(11) default NULL COMMENT '关联班次定义表id',
  `work_day` varchar(200) default NULL COMMENT '时间[星期一到星期天]',
  `shift_num` int(11) default NULL COMMENT '班次人数',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`shift_id`,`shift_num`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='班次设置'