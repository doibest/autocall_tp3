CREATE TABLE `pb_leave_table` (
  `id` int(11) NOT NULL auto_increment,
  `createtime` datetime default NULL COMMENT '申请请假时间/创建时间',
  `leave_of_absence` varchar(50) default NULL COMMENT '请假人 [创建人]',
  `dept_id` int(11) default NULL COMMENT '请假人 所在部门',
  `start_leave_time` datetime default NULL COMMENT '开始请假时间',
  `end_leave_time` datetime default NULL COMMENT '结束请假时间',
  `batch_dummies` varchar(50) default NULL COMMENT '批假人',
  `leave_results` char(1) default NULL COMMENT '请假结果',
  `grant_leave` datetime default NULL COMMENT '批假同意时间',
  `leave_day` varchar(10) default NULL COMMENT '请假天数',
  `leave_type` int(11) default NULL COMMENT '请假类型',
  `reason_for_leave` text COMMENT '请假原因',
  `no_leave_reason` text COMMENT '不同意批假原因',
  `whether_approval` char(1) default 'N' COMMENT '该请假申请是否审批【Y：已经审批【请假人不能在修改】，N：还没审批】',
  `leave_dates` text COMMENT '请假日期【json数据】',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`leave_of_absence`,`batch_dummies`,`leave_day`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='请假表'