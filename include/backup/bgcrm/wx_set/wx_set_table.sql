CREATE TABLE `wx_set` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) default NULL COMMENT '参数名称',
  `description` varchar(255) default NULL COMMENT '参数描述',
  `wx_value` varchar(255) default NULL COMMENT '参数值',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COMMENT='微信参数设置表'