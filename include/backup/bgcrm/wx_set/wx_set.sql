INSERT INTO wx_set(`id`,`name`,`description`,`wx_value`)VALUES 
(1,'appid','微信开发者的AppId','');
(2,'appsecret','微信开发者的Appsecret','');
(3,'token','微信服务器配置中的Token','');
(5,'password','微信登录密码','');
(4,'openid','原始ID','');
(7,'max_chat','一个坐席做多可以跟跟几个微信客户聊天,如果大于该值，则不再向该坐席推送微信消息','10');
(6,'minutes','统计坐席几分钟内跟几个客户聊天，用于判断该坐席是否空闲，若不空闲，则有微信消息就不分配给该坐席','5');
