CREATE TABLE `ks_question_bank` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建人',
  `dept_id` int(11) default NULL,
  `curriculum_id` int(11) default NULL COMMENT '课程id',
  `question_title` text COMMENT '试题标题',
  `topic_answers` text COMMENT '题目答案/试题标准答案',
  `type_questions` varchar(10) default NULL COMMENT '试题类型【1：单选、2：多选、3：判断、4：填空、5：问答】',
  `question_content` text COMMENT '试题内容【json数据】',
  `examination_scores` int(11) default NULL COMMENT '试题分数',
  `term` char(1) default 'N' COMMENT '是否有期限【Y：有期限，N：无期限】',
  `start_validity_time` datetime default NULL COMMENT '开始使用期限【有效期从】',
  `end_validity_time` datetime default NULL COMMENT '结束使用期限【有效期到】',
  `easy_type` varchar(10) default NULL COMMENT '难易程度',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`,`type_questions`,`curriculum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='题库管理'