CREATE TABLE `work_form` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建者',
  `dept_id` int(11) default NULL COMMENT '部门id',
  `process_design_id` int(11) default NULL COMMENT '流程id【work_process_design】',
  `process_node_id` int(11) default NULL COMMENT '流程节点id【work_process_design_node】',
  `file_attachment` varchar(255) default NULL COMMENT '文件名称跟路径',
  `native_name` varchar(100) default NULL COMMENT '上传文件原名',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`,`process_design_id`,`process_node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='表单设计'