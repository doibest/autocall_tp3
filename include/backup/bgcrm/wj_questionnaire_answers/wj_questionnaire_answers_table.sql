CREATE TABLE `wj_questionnaire_answers` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建人/答卷人',
  `dept_id` int(11) default NULL,
  `questionnaire_generation_id` int(11) default NULL COMMENT '问卷id【wj_questionnaire_generation表id】',
  `questionnaire_type_id` int(11) default NULL COMMENT '问卷类型id',
  `answer_code` varchar(50) default NULL COMMENT '答卷编码',
  `modification_time` datetime default NULL COMMENT '修改时间',
  `task_id` int(11) default NULL COMMENT '外呼任务id',
  `customer_id` int(11) default NULL COMMENT '客户资料id',
  `answer_class` varchar(20) default NULL COMMENT '答卷分类【1：有效答卷，2：无效答卷，3：删除的答卷】',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`,`questionnaire_generation_id`,`questionnaire_type_id`,`task_id`,`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='问卷答卷'