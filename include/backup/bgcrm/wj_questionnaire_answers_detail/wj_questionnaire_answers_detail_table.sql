CREATE TABLE `wj_questionnaire_answers_detail` (
  `questionnaire_answers_id` int(11) default NULL COMMENT '关联wj_questionnaire_answers表id',
  `questionnaire_exam_id` int(11) default NULL COMMENT '关联wj_questionnaire_bank表id',
  `questionnaire_topic_id` int(11) default NULL COMMENT '关联wj_questionnaire_topic的id',
  `questionnaire_generation_id` int(11) default NULL COMMENT '问卷id【wj_questionnaire_generation表id】',
  `question_type` int(11) default NULL COMMENT '题目类型【单选、多选等】',
  `fill_answer` text COMMENT '填写的答案',
  KEY `NewIndex1` (`questionnaire_answers_id`,`questionnaire_exam_id`,`question_type`,`questionnaire_topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='问卷答卷详情'