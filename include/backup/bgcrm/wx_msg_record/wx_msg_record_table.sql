CREATE TABLE `wx_msg_record` (
  `id` int(11) NOT NULL auto_increment,
  `CreateTime` datetime default NULL COMMENT '消息创建时间',
  `ToUserName` varchar(100) default NULL COMMENT '开发者微信号 ',
  `FromUserName` varchar(100) default NULL COMMENT '发送方帐号（一个OpenID） ',
  `from_nickname` varchar(100) default NULL COMMENT '发送方的昵称 ',
  `to_nickname` varchar(100) default NULL COMMENT '开发者微信号的昵称 ',
  `MsgType` varchar(20) default NULL COMMENT '消息类型',
  `sendType` varchar(10) default NULL COMMENT '判断是发送的还是接收的(other：接收，my：发送的)',
  `Content` text COMMENT '文本消息内容',
  `ThumbMediaId` varchar(255) default NULL COMMENT '视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。 ',
  `Label` varchar(255) default NULL COMMENT '地理位置信息',
  `Title` varchar(200) default NULL COMMENT '消息标题',
  `Description` text COMMENT '消息描述',
  `Event` varchar(50) default NULL COMMENT '事件类型，subscribe(订阅)、unsubscribe(取消订阅) ',
  `EventKey` varchar(100) default NULL COMMENT '事件KEY值',
  `Ticket` varchar(255) default NULL COMMENT '二维码的ticket，可用来换取二维码图片 ',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信记录表'