CREATE TABLE `wj_questionnaire_topic` (
  `id` int(11) NOT NULL auto_increment,
  `topic_name` varchar(255) default NULL COMMENT '题型名称',
  `topic_type` varchar(20) default NULL COMMENT '题型类型【1：单选、2：多选、3：判断、4：填空、5：问答、6：排序】',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='问卷题型'