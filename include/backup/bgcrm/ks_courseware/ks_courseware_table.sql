CREATE TABLE `ks_courseware` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `curriculum_id` int(11) default NULL COMMENT '关联课程表id',
  `create_user` varchar(50) default NULL COMMENT '创建人',
  `dept_id` int(11) default NULL COMMENT '部门id',
  `courseware_name` varchar(255) default NULL COMMENT '课件名称',
  `courseware_description` text COMMENT '课件描述',
  `file_path_name` varchar(255) default NULL COMMENT '文件路径跟名称',
  `file_type` varchar(20) default NULL COMMENT '文件类型',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`curriculum_id`,`create_user`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='课件表'