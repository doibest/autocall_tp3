CREATE TABLE `work_process_design_node` (
  `id` int(11) NOT NULL auto_increment,
  `create_user` varchar(50) default NULL,
  `create_time` datetime default NULL,
  `process_design_id` int(11) default NULL COMMENT '流程id【work_process_design】',
  `order_num` int(11) default NULL COMMENT '序号',
  `step_name` varchar(100) default NULL COMMENT '步骤名称',
  `next_step_num` int(11) default NULL COMMENT '下一步骤序号',
  `writable_fields` text COMMENT '可写字段【json数据】',
  `authorized_personnel` text COMMENT '授权范围（人员）',
  `authorize_dept_ids` text COMMENT '授权范围（部门）',
  `authorize_role_id` text COMMENT '授权范围（角色）',
  `authorized_work_num` text COMMENT '符合授权的工号',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`process_design_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='流程节点'