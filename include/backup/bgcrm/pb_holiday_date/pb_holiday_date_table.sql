CREATE TABLE `pb_holiday_date` (
  `holiday_id` int(11) default NULL COMMENT '关联pb_holidays表的id',
  `holiday_date` date default NULL COMMENT '节假日日期',
  KEY `NewIndex1` (`holiday_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='节假日设定表----日期'