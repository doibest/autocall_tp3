CREATE TABLE `ks_exam_scores` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '交卷时间',
  `create_user` varchar(50) default NULL COMMENT '考试人员工号',
  `dept_id` int(11) default NULL,
  `modification_time` datetime default NULL COMMENT '修改时间',
  `paper_id` int(11) default NULL COMMENT '试卷id',
  `exam_score` int(11) default NULL COMMENT '考试得分',
  `whether_marking` char(1) default 'N' COMMENT '是否评卷【Y：已评卷，N：未评卷】',
  `curriculum_id` int(11) default NULL COMMENT '课程id',
  `program_id` int(11) default NULL COMMENT '考试计划id',
  `complaint_content` text COMMENT '申诉内容/对有异议的成绩发起申诉',
  `processing_agents` varchar(50) default NULL COMMENT '处理人/处理这个申诉的坐席',
  `processing_results` varchar(20) default NULL COMMENT '处理结果',
  `processing_content` text COMMENT '处理内容/对处理结果的描述',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`,`paper_id`,`curriculum_id`,`program_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='考试成绩表'