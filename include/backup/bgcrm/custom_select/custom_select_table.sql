CREATE TABLE `custom_select` (
  `field_id` int(11) default NULL COMMENT '关联customer_fields的id',
  `select_value` varchar(50) default NULL COMMENT 'select选择框的value值',
  `select_name` varchar(200) default NULL COMMENT '选择框的值',
  `select_enabled` char(1) default NULL COMMENT '是否显示 Y：显示，N：不显示',
  `select_order` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8