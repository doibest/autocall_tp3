CREATE TABLE `wj_questionnaire_type` (
  `id` int(11) NOT NULL auto_increment,
  `questionnaire_type_name` varchar(255) collate utf8_estonian_ci default NULL COMMENT '问卷类型名称',
  `questionnaire_type_description` text collate utf8_estonian_ci COMMENT '问卷类型描述',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci COMMENT='问卷类型'