CREATE TABLE `push_data` (
  `id` int(11) NOT NULL auto_increment,
  `push_type` varchar(20) default NULL COMMENT '推的类型',
  `push_user` varchar(50) default NULL COMMENT '需要推送的坐席',
  `customer_id` int(11) default NULL COMMENT '资料id',
  `push_time` datetime default NULL COMMENT '推的时间',
  `task_id` int(11) default NULL COMMENT '任务id',
  `shift_id` int(11) default NULL,
  `visit_id` int(11) default NULL COMMENT '回访id【visit_customer】',
  `phone` varchar(50) default NULL COMMENT '号码【外呼】',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`customer_id`,`task_id`,`shift_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='需要推送的数据'