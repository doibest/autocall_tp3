CREATE TABLE `ks_policy_configuration` (
  `id` int(11) NOT NULL auto_increment,
  `create_time` datetime default NULL COMMENT '创建时间',
  `create_user` varchar(50) default NULL COMMENT '创建人',
  `dept_id` int(11) default NULL,
  `policy_name` varchar(255) default NULL COMMENT '策略名称',
  `policy_description` text COMMENT '策略描述',
  `policy_content` text COMMENT '策略内容【json数据】',
  `policy_score` int(11) default NULL COMMENT '总分',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`create_user`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='试题策略配置'