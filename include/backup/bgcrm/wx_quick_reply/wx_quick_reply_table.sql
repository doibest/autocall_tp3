CREATE TABLE `wx_quick_reply` (
  `id` int(11) NOT NULL auto_increment,
  `answer_number` varchar(50) default NULL COMMENT '应答工号',
  `reply_content` text COMMENT '快捷回复的内容',
  `reply_enable` char(1) NOT NULL default 'N' COMMENT '是否启用该条快捷回复内容（Y：启用，N：禁用）',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`answer_number`,`reply_enable`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='快捷回复'