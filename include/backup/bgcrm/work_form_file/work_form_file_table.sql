CREATE TABLE `work_form_file` (
  `id` int(11) NOT NULL auto_increment,
  `work_form_id` int(11) default NULL COMMENT '表单id【关联work_form_n的id】',
  `file_path_name` varchar(255) default NULL COMMENT '文件名称',
  `native_name` varchar(100) default NULL COMMENT '文件原名',
  `node_num` int(11) default NULL COMMENT '节点序号',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='表单附件表'