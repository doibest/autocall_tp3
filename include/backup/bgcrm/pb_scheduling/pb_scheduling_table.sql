CREATE TABLE `pb_scheduling` (
  `id` int(11) NOT NULL auto_increment,
  `user_name` varchar(50) default NULL COMMENT '工号',
  `dept_id` int(11) default NULL COMMENT '部门id【备用】',
  `shift_id` int(11) default NULL COMMENT '班次',
  `scheduling_dates` date default NULL COMMENT '排班日期',
  `week_day` varchar(20) default NULL COMMENT '星期几',
  PRIMARY KEY  (`id`),
  KEY `NewIndex1` (`user_name`,`shift_id`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='排班表'