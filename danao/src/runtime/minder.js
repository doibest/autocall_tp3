/**
 * @fileOverview
 *
 * 脑图示例运行时
 *
 * @author: techird
 * @copyright: Baidu FEX, 2014
 */
define(function(require, exports, module) {
    var Minder = require('../minder');

    function MinderRuntime() {

        // 不使用 kityminder 的按键处理，由 ReceiverRuntime 统一处理
        var minder = new Minder({
            enableKeyReceiver: false,
            enableAnimation: true
        });

        // 渲染，初始化
        minder.renderTo(this.selector);
        minder.setTheme(null);
        minder.execCommand('template', 'filetree');
        minder.select(minder.getRoot(), true);
        minder.execCommand('text', '大脑');
        minder.select(minder.getRoot(), true);
        minder.execCommand('AppendChildNode', '左脑');
        minder.select(minder.getRoot(), true);
        minder.execCommand('AppendChildNode', '右脑');
        if (window.localStorage._editor_readonly_) {
            minder.disable();
        } else {
            minder.enable();
        }

        // 导出给其它 Runtime 使用
        this.minder = minder;
    }

    return module.exports = MinderRuntime;
});