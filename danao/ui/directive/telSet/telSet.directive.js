angular.module('kityminderEditor')
    .directive('telSet',['$modal', function($modal) {
        return {
            restrict: 'E',
            templateUrl: 'ui/directive/telSet/telSet.html',
            scope: {
                minder: '='
            },
            replace: true,
            link: function($scope) {

                var selectedNode;
                var rootNode;

                $scope.telSetDisabled = true;

                $scope.openTelSet = function() {

                    var hyperlinkModal = $modal.open({
                        animation: true,
                        templateUrl: 'ui/dialog/telSet/telSet.tpl.html',
                        controller: 'telSet.ctrl',
                        size: 'lg'
                    });
                };

                minder.on('dblclick', function() {
                    if(!window.localStorage._editor_readonly_) {
                        if(minder.getSelectedNode().data.id === minder.getRoot().data.id) {
                            $scope.openTelSet();
                        }
                    }
                });

            }
        }
    }]);