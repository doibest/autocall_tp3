angular.module('kityminderEditor')
    .directive('quickOperation', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'ui/directive/quickOperation/quickOperation.html'
        }
    })