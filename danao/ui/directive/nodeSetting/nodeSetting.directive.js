angular.module('kityminderEditor')
    .directive('nodeSetting', ['$modal', function($modal) {
        return {
            restrict: 'E',
            templateUrl: 'ui/directive/nodeSetting/nodeSetting.html',
            scope: {
                minder: '='
            },
            replace: true,
            link: function($scope) {
                var minder = $scope.minder;

                $scope.openNodeSetting = function() {

                    var link = minder.queryCommandValue('HyperLink');

                    var hyperlinkModal = $modal.open({
                        animation: true,
                        templateUrl: 'ui/dialog/nodeSetting/nodeSetting.tpl.html',
                        controller: 'hyperlink.ctrl',
                        size: 'md',
                        resolve: {
                            link: function() {
                                return link;
                            }
                        }
                    });

                    hyperlinkModal.result.then(function(result) {
                        minder.execCommand('HyperLink', result.url, result.title || '');
                    });
                };

              minder.on('dblclick', function() {
                if(!window.localStorage._editor_readonly_) {
                  var selectedNode = minder.getSelectedNode();
                  var rootNode = minder.getRoot();
                  var child0 = rootNode.children[0];
                  var child1 = rootNode.children[1];
                  if(selectedNode.data.id === rootNode.data.id) return;
                  if(child0) {
                      if(selectedNode.data.id === child0.data.id)
                          return;
                  }
                  if(child1) {
                      if(selectedNode.data.id === child1.data.id)
                          return;
                  }
                  $scope.openNodeSetting(); 
                }
              });
            }
        }
    }]);