angular.module('kityminderEditor')
  .directive('telData', ['$modal', function($modal) {
    return {
      restrict: 'E',
      templateUrl: 'ui/directive/telData/telData.html',
      scope: {
        minder: '='
      },
      replace: true,
      link: function($scope) {
        var minder = $scope.minder;

        $scope.telDataJson = function() {

            console.log('aaa')
          var image = minder.queryCommandValue('image');

          var imageModal = $modal.open({
            animation: true,
            backdrop: "static",
            templateUrl: 'ui/dialog/telData/telData.tpl.html',
            controller: 'telData.ctrl',
            size: 'lg',
            resolve: {
              telData: function() {
                return image;
              }
            }
          });

          imageModal.result.then(function(result) {
            minder.execCommand('telData', result.url, result.title || '');
          });

        }

        minder.on('dblclick', function() {
          if(!window.localStorage._editor_readonly_) {
            var selectedNode = minder.getSelectedNode();
            var rootNode = minder.getRoot();
            var child0 = rootNode.children[0];
            var child1 = rootNode.children[1];
            if(selectedNode.data.id === rootNode.data.id) return;
            if(child0) {
                if(selectedNode.data.id === child0.data.id)
                    return;
            }
            if(child1) {
                if(selectedNode.data.id === child1.data.id)
                    return;
            }
            $scope.telDataJson(); 
          }
        });
      }
    }
  }]);