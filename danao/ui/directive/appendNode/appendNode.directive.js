angular.module('kityminderEditor')
    .directive('appendNode', ['commandBinder', function(commandBinder) {
        return {
            restrict: 'E',
            templateUrl: 'ui/directive/appendNode/appendNode.html',
            scope: {
                minder: '='
            },
            replace: true,
            link: function($scope) {
                var minder = $scope.minder;

                commandBinder.bind(minder, 'appendchildnode', $scope)

                $scope.execCommand = function(command) {
                    minder.execCommand(command, '分支主题');
                    editText();
                };

                function editText() {
                    var receiverElement = editor.receiver.element;
                    var fsm = editor.fsm;
                    var receiver = editor.receiver;

                    receiverElement.innerText = minder.queryCommandValue('text');
                    fsm.jump('input', 'input-request');
                    receiver.selectAll();
                }

                $scope.isRootNode = function() {
                    return minder.getSelectedNode().data.id === minder.getRoot().data.id
                }

                $scope.isRootDirectChildrenNode = function() {
                    var rootNode = minder.getRoot();
                    var selectedNode = minder.getSelectedNode();
                    if(rootNode.children[0]) {
                        if(selectedNode.data.id === rootNode.children[0].data.id) {
                            return true;
                        }
                    }
                    if(rootNode.children[1]) {
                        if(selectedNode.data.id === rootNode.children[1].data.id) {
                            return true;
                        }
                    }
                    return false;
                }
            }
        }
    }]);