angular.module('kityminderEditor')
    .controller('telSet.ctrl', function ($scope, $modalInstance) {

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.telCont = {
            telSet:{
                share: 0,
                collect:1,
                docking:{
                    asr:[],
                    gender: {"times":3},
                    tts:[],
                    nlp:{
                        appid:"",
                        apiKey:"",
                        secretKey:"",
                        engine:"",
                        robotId:"",
                        botId: 0
                    },
                    hotWords:[],
                    database:[],
                    rest:[],
                    wechat:[],
                    sms:[],
                    email:[]
                },
                set: {
                    intentClassify:[],
                    labelManage:[],
                    failProcess:{},
                    intentProcess:{},
                    callSet:{},
                    otherSet: {
                        play_progress_null: true
                    },
                    actionLog:{}
                },
                dataStatistics:1,
                status:1
            }
        }
        if(!window.localStorage.getItem("__dev_minder_telContent")){
            window.localStorage.setItem("__dev_minder_telContent",JSON.stringify($scope.telCont))
        }else{
            $scope.telCont = JSON.parse(window.localStorage.getItem("__dev_minder_telContent"));
        };
        
        $scope.intentClassify = $scope.telCont.telSet.set.intentClassify;
        $scope.delYiTuOption = function (index) {
            $scope.intentClassify.splice(index,1);
        };
        $scope.addYiTuOption = function () {
            $scope.intentClassify.push({"typeofIntent":"","parentType":""});
        };
        $scope.$watch("telCont",function () {
            window.localStorage.setItem("__dev_minder_telContent",JSON.stringify($scope.telCont))
        },true)
    })