angular.module('kityminderEditor')
  .directive('myPopover', ['$modal', function($modal) {
    return {
      restrict: 'AE',
      templateUrl: 'ui/dialog/popover/popover.html',
      scope: {
        minder: '=',
      },
      replace: true,
      controller: function ($scope) {

      }
    }
  }]);