angular.module('kityminderEditor')
  .directive('selectExp', ['$modal', function($modal) {
    return {
      restrict: 'AE',
      templateUrl: 'ui/dialog/selectExp/selectExp.html',
      scope:false,
      replace: true,
      controller: function ($scope) {
        $scope.counts = [0, 1, 2,3,4,5,6,7,8,9, 10];
        $scope.times = [0,1,3,5,10, 20,30];        
      }
    }
  }]);