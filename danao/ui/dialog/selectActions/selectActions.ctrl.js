angular.module('kityminderEditor')
  .directive('selectAction', ['$modal', function($modal) {
    return {
      restrict: 'AE',
      templateUrl: 'ui/dialog/selectActions/selectActions.html',
      scope: false,
      controller: function ($scope) {
        $scope.actionCount = [
          {name: 0, val:0},
          {name: 1, val:1},
          {name: 2, val:2},
          {name: 3, val:3},
          {name: 4, val:4},
          {name: 5, val:5},
          {name: 6, val:6},
          {name: 7, val:7},
          {name: 8, val:8},
          {name: 9, val:9},
          {name: 10, val:10},
        ];
        $scope.times = [0,1,3,5,10, 20,30];
        $scope.outStrategyFun = function(item){
          return {
            0: '随机',
            1: '顺序',
            2: '拼接'
          }[item];
        };

      }
    }
  }]);