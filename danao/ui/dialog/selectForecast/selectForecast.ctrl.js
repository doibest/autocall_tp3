angular.module('kityminderEditor')
  .directive('selectActionForecast', ['$modal', function($modal) {
    return {
      restrict: 'AE',
      templateUrl: 'ui/dialog/selectForecast/selectForecast.html',
      scope: false,
      controller: function ($scope) {

      }
    }
  }]);