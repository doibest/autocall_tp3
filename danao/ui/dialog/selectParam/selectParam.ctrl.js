angular.module('kityminderEditor')
  .directive('selectParam', ['$modal', function($modal) {
    return {
      restrict: 'AE',
      templateUrl: 'ui/dialog/selectParam/selectParam.html',
      scope: false,
      replace: true,
      controller: function ($scope) {
        $scope.showSel = null;
        $scope.selKey = null;
        $scope.obj = [{
          name:'当前会话输出话术',val:" now_prompt",
        },{ name:'上轮会话输出话术',val:"last_prompt"
        },{ name:'当前会话 话术意图组',val:"now_sayShort"
        },{ name:'[',val:"["
        },{ name:']',val:"]"
        },{ name:'历史会话',val:"past_afterAnswer"
        },{ name:'当前会话 过滤后原话组 长度',val:"now_preSlot_number"
        },{ name:'上轮会话 过滤后意图组 数量',val:"last_afterIntent_number"
        },{ name:'历史会话',val:"past_afterAnswer"
        },{ name:'播放不达标',val:"failPlay"
        },{ name:'历史会话输出话术',val:" past_prompt"}
        ];
        $scope.zkh = [{
          name:'(',val:"(",
        },{ name:'!',val:"!"}
        ];
        $scope.ykh = [{
          name:')',val:")",
        }

        ];
        $scope.gl = [{
          name: '并且', val: '&&',
        }, {
          name: '或者', val: '||'
        }];

        $scope.gx = [
          {name :'大于',val:'>'},
          {name :'大于等于',val:'>='},
          {name :'小于',val:'<'},
          {name :'小于等于',val:'<='},
          {name :'不等于',val:'!='},
          {name :'等于',val:'=='},
          {name :'包含',val:'include'},
          {name :'符合正则表达式',val:'pattern'},
        ];
        $scope.params = [];
        $scope.STATE = {
          gl:null,
          zkh:null,
          ykh:null,
          gx:null,
          obj_lef:null,
          obj_rig:null,
          edit: true
        };
        $scope.param = {
          gl:null,
          zkh:null,
          ykh:null,
          gx:null,
          obj_lef:null,
          obj_rig:null,
          edit: true
        };
        $scope.selParam = {};
        $scope.addParmFun = function(param, index) {
          var param = angular.copy($scope.STATE);
          param['key'] = ($scope.params && $scope.params.length > 0) ? $scope.params[$scope.params.length-1]['key'] + 1: 1;
          $scope.params.push(param)
        };

        $scope.cleanAddParma = function() {
          $scope.selParam = angular.copy($scope.STATE) ;
          $scope.param = angular.copy($scope.STATE) ;
        }
        $scope.selectParms = function( par, val) {
          if(par == 'zkh') {
            $scope.selParam[par] = val +  '(';
          }else if(par == 'ykh') {
            $scope.selParam[par] = val+  ')';
          }else if(par =='obj_lef' || par == 'obj_rig'){
            if($scope.selParam[par]) {
              $scope.selParam[par] = $scope.selParam[par] + "_" + val;
            }else {
              $scope.selParam[par] = val;
            }
          }else {
            $scope.selParam[par] = val;
          }
        };
        $scope.delParam = function(index) {
          $scope.params.splice(index,1);
        };
        $scope.creatParam = function() {
          $scope.paramStr = '';
          for(var i=0; i<  $scope.params.length; i++) {
            var item =  $scope.params[i];
            var _gl = item['gl'] || ''
            $scope.paramStr  += _gl+ item['zkh'] + item['obj_lef']  +item['gx'] + item['obj_rig'] + item['ykh'];
          }
        };

        $scope.filter_fun = function(style, val) {
          switch (style) {
            case 'gl':
              return {
                '&&': '并且',
                '||': '或者',
              }[val] || val;
            case 'gx':
              return {
                'pattern': '正则表达式',
                '>': '大于',
                '>=': '大于等于',
                '<': '小于',
                '<=': '小于等于',
                '==': '等于',
                'inclode': '包含',
              }[val] || val;
          }
        }

        $scope.sortParam = function(index, sty) {
          if(sty == 'd') {
            if(index === $scope.params.length-1) return ;
            var _next = $scope.params[index+1];
            $scope.params[index+1] = $scope.params[index];
            $scope.params[index] = _next;
          }else {
            if(index === 0) return ;
            var _next = $scope.params[index-1];
            $scope.params[index-1] = $scope.params[index];
            $scope.params[index] = _next;
          }
        }

        $scope.delkh = function (sta) {
          if(sta == 'l') {
            $scope.selParam.zkh = null;
          }else {
            $scope.selParam.ykh = null;
          }
        }

        $scope.showSelectBool = function(index, state) {
          $scope.showSel = ($scope.showSel === state) ? null : state;
          $scope.selKey = index;
        };

        $scope.seletParam = function(param ,val, state ) {
          switch (state) {
            case 'gl':
              param[state] = val;
              break;
            case 'zkh':
              param[state] = param[state] ? param[state]  + val : val;
              break;
            case 'obj_lef':
              param[state] = param[state] ? param[state] +',' + val : val;
              break;
            case 'obj_rig':
              param[state] = param[state] ? param[state] + ',' + val : val;
              break;
            case 'gx':
              param[state] = val;
              break;
            case 'ykh':
              param[state] =param[state] ? param[state] + val: val;
              break;
          }
        }
      }
    }

   }]);