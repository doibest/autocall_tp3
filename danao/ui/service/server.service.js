/**
 * @fileOverview
 *
 *  与后端交互的服务
 *
 * @author: zhangbobell
 * @email : zhangbobell@163.com
 *
 * @copyright: Baidu FEX, 2015
 */
angular.module('kityminderEditor')
    .service('server', ['config', '$http', '$q', function (config, $http, $q) {

        return {
            uploadImage: function (file) {
                var url = config.get('imageUpload');
                var fd = new FormData();
                fd.append('upload_file', file);

                return $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                });
            },
            $api: function (url, method, params, headers) {
                var baseUrl = url || '';
                headers = headers || {};
                if (/^\/baiDuApi\//.test(url)) {
                    baseUrl = url.replace(/^\/baiDuApi\//, 'https://aip.baidubce.com/rpc/2.0/unit/');
                    headers = Object.assign(headers, {
                        "Content-Type": "application/json"
                    })
                };
                if (/^\/naotuApi\//.test(url)) {
                    baseUrl = url.replace(/^\/naotuApi\//, 'http://121.40.120.193/');
                    headers = Object.assign(headers, {
                        "Content-Type": "application/x-www-form-urlencoded"
                    });
                    // var formData = new FormData();
                    // for (var a in params) {
                    //     formData.append(a, params[a]);
                    // }
                    // params = formData;
                };
                method = method || "GET";
                var defer = $q.defer();
                if (method == 'GET' || method == 'get') {
                    $http({
                        url: baseUrl,
                        method: "GET",
                        headers: headers,
                        data: params,
                    }).success(function (data) {
                        console.log(data)
                        defer.resolve(data);
                    }).
                        error(function (data, status, headers, config) {
                            // defer.resolve(data);  
                            defer.reject(data);
                        });
                } else {
                    $http({
                        url: baseUrl,
                        method: method,
                        headers: headers,
                        data: params
                    }).success(function (data) {
                        console.log("post获取", data)
                        defer.resolve(data);
                    }).error(function (data, status, headers, config) {
                        // defer.resolve(data);  
                        defer.reject(data);
                    });
                }
                return defer.promise;
            },
            $transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            }
        }
    }]);