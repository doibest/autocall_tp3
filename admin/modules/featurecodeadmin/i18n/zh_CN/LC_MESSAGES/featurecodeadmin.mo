��    J      l  e   �      P  !   Q     s  	   �     �     �     �     �     �  %   
     0     K  &   h  +   �  -   �     �  
   �               -     G     O     _     r       
   �     �     �     �  	   �  	   �     �     �  	   �     �     	     	     0	     8	  ,   K	     x	  "   �	     �	     �	  	   �	     �	  "   �	      
  &   "
     I
     W
     g
     t
     �
      �
  N   �
     
       "        A     P     d     |     �     �     �     �     �     �     �  |   �     s     |     �  �  �     P     i  	   p     z  '   �     �     �     �     �          )     <     U     k     �     �     �     �     �     �     �     �     �               +     2     E     X     _     l  	   s     }     �     �     �     �     �  $   �     �     �                3     @     G     T     a     t     �     �     �     �     �  T   �     )     6  !   =     _     l     �  	   �     �     �     �     �     �     �     �  c        g     w     �     E   #   7   	       "   0   F                     @              !   +          5          9             D                    /          &   3   )      .   ,   G   I   8   J   '              <           1   -      *   C   
   ;   (       =             H                     B   %       >      6               :                         2          $       A      4      ?    Are you sure you wish to proceed? Asterisk General Call Pickup Blacklist Blacklist a number Blacklist the last caller Call Forward Call Forward All Activate Call Forward All Deactivate Call Forward All Prompting Deactivate Call Forward Busy Activate Call Forward Busy Deactivate Call Forward Busy Prompting Deactivate Call Forward No Answer/Unavailable Activate Call Forward No Answer/Unavailable Deactivate Call Forward Toggle Call Trace Call Waiting Call Waiting - Activate Call Waiting - Deactivate ChanSpy Check Recording Connect to Gabcast DND Activate DND Deactivate DND Toggle Default Dial System FAX Dial Voicemail Dictation Directory Disabled Do-Not-Disturb (DND) Echo Test Email completed dictation Enabled Fax Configuration Feature Feature Code Admin Feature Code Conflicts with other Extensions Feature Codes Feature Codes have been duplicated Featurecode:  Findme Follow Toggle Follow Me Gabcast In-Call Asterisk Attended Transfer In-Call Asterisk Disconnect Code In-Call Asterisk Toggle Call Recording Info Services Intercom prefix My Voicemail Paging and Intercom Perform dictation Phonebook dial-by-name directory Please enter a Feature Code or check Use Default for all Enabled Feature Codes Queue Toggle Queues Remove a number from the blacklist Save Recording Set user speed dial Speak Your Exten Number Speaking Clock Speed Dial Functions Speeddial prefix Status Submit Changes Use User Intercom Allow User Intercom Disallow You have feature code conflicts with extension numbers in other modules. This will result in unexpected and broken behavior. ZapBarge featuredescription moduledescription Project-Id-Version: FreePBX 2.5 Chinese Translation
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-09-17 11:15+0200
PO-Revision-Date: 2009-01-31 00:33+0800
Last-Translator: 周征晟 <zhougongjizhe@163.com>
Language-Team: EdwardBadBoy <zhougongjizhe@163.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Chinese
X-Poedit-Country: CHINA
X-Poedit-SourceCharset: utf-8
 你确定要继续吗？ 代接 黑名单 加入一个号码至黑名单 将最近的来电号码列入黑名单 呼叫转移 无条件转移激活 无条件转移禁止 无条件转移提示禁止 遇忙转移激活 遇忙转移禁止 遇忙转移提示禁止 无应答转移激活 无应答转移禁止 呼叫转移开关 呼叫跟踪 呼叫等待 呼叫等待激活 呼叫等待禁止 监听 检查录音 连接到播客台 免打扰开启 免打扰关闭 免打扰开关 默认 拨打系统传真 拨打语音信箱 测试 电话目录 禁用 免打扰 回音测试 邮件测试 启用 传真配置 功能 功能代码管理 功能代码与其他分机有冲突 功能代码 功能代码已被复制 功能代码： 分机随行开关 分机随行 播客 来电前转 指定代接 呼入录音开关 信息服务 对讲前缀 我的语音信箱 寻呼与对讲 模拟测试 电话本拔号目录 请输入一个功能代码或为所有启用的功能代码钩选“使用默认” 队列开关 队列 从黑名单里移除一个号码 保存录音 设置用户快速拨号 报当前分机号 报时间 快速拨号 快速拨号前缀 状态 提交更改 使用 允许用户对讲 禁止用户对讲 你的功能代码在其他的模块中与分机号有冲突。这将导致未知的错误行为。 监听模拟线 功能描述 模块描述 