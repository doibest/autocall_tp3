<?php
class DailyAction extends Action {
	/**
	 * 发布日报
	 * @author          zhang
	 */
	function addDaily(){
		//$src = $_FILES['file'];
		$res = $this->upload();
		$file = '';
		foreach ($res as $val) {
			$file .= $val['savename'];
			$file .= '|';
		}
		$file = trim($file, '|');
		$data['attachment'] = $file;
		$data["daily_content"] = $_REQUEST["daily_content"];
		$data["view_user"] = $_REQUEST["view_user"];
		$data["daily_type"] = $_REQUEST["daily_type"];
		$data["daily_title"] = $_REQUEST["username"]."的日报";
		$data["createname"] = $_REQUEST["username"];
		$data["createtime"] = date("Y-m-d H:i:s");

		$daily_info = M("daily_info");
		$result = $daily_info->add($data);
		if ($result){
			$data['status']  = 1;
			$data['msg'] = '发布成功！';
			$this->ajaxReturn($data);
		}else{
			$data['status']  = 0;
			$data['msg'] = '未知错误！';
			$this->ajaxReturn($data);
		}
	}

	/**
	 * 上传文件
	 * @author          zhang
	 */
	public function upload(){
        import('ORG.Net.UploadFile');
        $upload = new UploadFile();// 实例化上传类
        $upload->maxSize = -1 ;// 设置附件上传大小
        $upload->allowExts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->savePath = 'include/data/daily/'; // 设置附件上传根目录
		$upload->saveRule = 'uniqid';
        $upload->autoSub = true;
        $upload->subType = 'date';
        // 上传文件
        if(!$upload->upload()) {// 上传错误提示错误信息
            $info = $upload->getErrorMsg();
            return $info;
        }else{// 上传成功 获取上传文件信息
            $info =  $upload->getUploadFileInfo();
            return $info;
        }
    }

	/**
	 * 获取用户信息
	 * @author          zhang
	 */
	function userInfo(){
		$pageIndex = $_REQUEST["pageIndex"]*$_REQUEST["pageCount"];
		$pageCount = $_REQUEST["pageCount"];
		$users = M('users');

		$result = $users->table("users u")->join("department d on d.d_id = u.d_id")->join("role r on r.r_id = u.r_id")->field("u.*,d.d_name,r.r_name")->where("ryToken!=''")->limit("$pageIndex,$pageCount")->order("u.username asc")->select();
		$this->ajaxReturn($result);
	}

	/**
	 * 我的日报列表
	 * @author          zhang
	 */
	function myDailyList(){
		$username = $_REQUEST["username"];
		$pageIndex = $_REQUEST["pageIndex"]*$_REQUEST["pageCount"];
		$pageCount = $_REQUEST["pageCount"];

		$where = empty($username) ? " 1" : " d.createname='$username'";
		$daily_info = M('daily_info d');

		$result = $daily_info->join("users u on u.username = d.createname")->field("d.*,u.cn_name,u.user_img")->where($where)->order("d.createtime desc")->limit("$pageIndex,$pageCount")->select();
		$typeArr = array("1"=>"日报","2"=>"周报","3"=>"月报","4"=>"其他");
		foreach($result as &$val){
			$val["comment"] = $this->getCategoryTree($val["id"]);
			$val["daily_type"] = $typeArr[$val["daily_type"]];
		}

		$this->ajaxReturn($result);
	}

	function dailyList(){
		$username = $_REQUEST["username"];
		$pageIndex = $_REQUEST["pageIndex"]*$_REQUEST["pageCount"];
		$pageCount = $_REQUEST["pageCount"];
		if($username == "admin"){
			$where = " 1";
		}else{
			$where = "find_in_set('$username', d.view_user) or d.recipients='$username' or d.createname='$username'";
		}
		//$where = empty($username) ? " 1" : " d.createname='$username'";
		$daily_info = M('daily_info d');
		$typeArr = array("1"=>"日报","2"=>"周报","3"=>"月报","4"=>"其他");
		$result = $daily_info->join("users u on u.username = d.createname")->field("d.*,u.cn_name,u.user_img")->where($where)->order("d.createtime desc")->limit("$pageIndex,$pageCount")->select();
		$img_str = "jpg,gif,png,jpeg";
		foreach($result as &$val){
			$val["comment"] = $this->getCategoryTree($val["id"]);
			$val["daily_type"] = $typeArr[$val["daily_type"]];
			$val["attachment"] = explode("|",$val["attachment"]);

		}

		$this->ajaxReturn($result);
	}

	function getCategoryTree($daily_id, $pid = 0, $level = 0){
		$daily_comment = M("daily_comment");
		$data = $daily_comment->where("daily_id = $daily_id AND pid=$pid")->order("createtime asc")->select();
		$users = M("users");
		foreach($data as &$val){
			$createname = $users->field("cn_name")->where("username = '".$val["createname"]."'")->find();
			$val["createname"] = $createname["cn_name"];
			//$receiveman = $users->field("cn_name")->where("username = '".$val["receiveman"]."'")->find();
			//$val["receiveman"] = $receiveman["cn_name"];
		}

		//目录树
		$tree = array();
		//层级
		$level++;
		if(!empty($data)){
			foreach ($data as &$v){
				$child = $this->getCategoryTree($daily_id,$v['id'], $level);
				$tree[] = array('parent' => $v, 'child' => $child, 'level' => $level);
			}
		}
		return $tree;
	}

	/**
	 * 日报添加评论
	 * @author          zhang
	 */
	function addComment(){
		$data["daily_id"] = $_REQUEST["daily_id"];
		$data["comment"] = $_REQUEST["comment"];
		$data["createname"] = $_REQUEST["username"];
		$data["createtime"] = date("Y-m-d H:i:s");
		$daily_comment = M("daily_comment");
		$result = $daily_comment->add($data);
		if ($result){
			$data['status']  = 1;
			$data['msg'] = '评论成功！';
			$this->ajaxReturn($data);
		}else{
			$data['status']  = 0;
			$data['msg'] = '未知错误！';
			$this->ajaxReturn($data);
		}
	}

	/**
	 * 日报添加回复
	 * @author          zhang
	 */
	function addReply(){
		$data["pid"] = $_REQUEST["p_id"];
		$data["receiveman"] = $_REQUEST["receiveman"];
		$data["daily_id"] = $_REQUEST["daily_id"];
		$data["comment"] = $_REQUEST["comment"];
		$data["createname"] = $_REQUEST["username"];
		$data["createtime"] = date("Y-m-d H:i:s");
		$daily_comment = M("daily_comment");
		$result = $daily_comment->add($data);
		if ($result){
			$data['status']  = 1;
			$data['msg'] = '回复成功！';
			$this->ajaxReturn($data);
		}else{
			$data['status']  = 0;
			$data['msg'] = '未知错误！';
			$this->ajaxReturn($data);
		}
	}

}







?>
