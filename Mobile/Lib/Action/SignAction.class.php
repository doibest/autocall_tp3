<?php
class SignAction extends Action{
    function sign () {
        $username = $_REQUEST['username'];
        $remarks = $_REQUEST['remarks'];
        $place = $_REQUEST['place'];
        $signout = $_REQUEST['signout'];
        $sematic_description = $_REQUEST['sematic_description'];
        $sign = M('sign');
        $data = array(
            'username' => $username,
            'remarks' => $remarks,
            'signtime' => date('Y-m-d H:i:s'),
            'place' => $place,
            'signout' => $signout,
            'sematic_description' => $sematic_description,
        );
        $res = $sign->add($data);
        if ($res) {
            echo json_encode(array('status'=>1));
            exit;
        } else {
            echo json_encode(array('status'=>0));
            exit;
        }

    }

    function getSign () {
        $index = $_REQUEST['index'];
        $username = $_REQUEST['username'];
        $first = $index * 10;
        // echo $index;exit;
        // echo 111;
        $sign = M('sign');
        $result = $sign->field("id, username, place, DATE_FORMAT(signtime,'%m-%d %H:%i') formattime, signout")->where("username=".$username)->limit("$first, 10")->order('id desc')->select();
        // file_put_contents('/var/www/html/aaaaa.php',$sql);
        // var_dump($result);exit;
        echo json_encode($result);
    }

    function getSignDetails () {
        $id = $_REQUEST['id'];
        $sign = M('sign');
        $result = $sign->field("place, signtime, remarks, sematic_description")->where("id=".$id)->find();
        // file_put_contents('/var/www/html/aaaaa.php',$sql);
        // var_dump($result);exit;
        echo json_encode($result);
    }


    // 获取饼图的数据
    function getPieData () {
        header("content-type:text/html;charset=utf-8");
        // $today = $this->getTodaySign();
        $sign = M('sign');
        $mobile_work_time = M('mobile_work_time');
        // 获取上下班时间
        $work_time = $mobile_work_time->field('start_work_time, end_work_time')->where('id=1')->find();
        $date = date('Y-m-d');
        $start_work_time = $date.' '.$work_time['start_work_time']; //上班时间
        $end_work_time = $date.' '.$work_time['end_work_time']; //下班时间

        // 获取迟到人数
        $late = $sign->where("signtime LIKE '$date%' AND signtime > '$start_work_time' AND signout=1")->count();
        // 获取未签到人数
        $users = M('users u');
        $usersCount = $users->where("u.ryId != ''")->count(); // 总人数
        $result = $users->join('sign s ON u.username = s.username')->where("u.ryId != '' AND s.signtime LIKE '$date%' AND s.signout=1")->group('s.username')->select(); // 已签到人数
        // print_r($result);
        $noSign = $usersCount - count($result); //未签到人数
        // 获取早退人数
        $leaveEarly = $sign->where("signtime LIKE '$date%' AND signtime < '$end_work_time' AND signout=2")->count();
        // 获取正常人数
        $result = $sign->where("(signtime LIKE '$date%' AND signtime < '$end_work_time' AND signout=2) OR (signtime LIKE '$date%' AND signtime > '$start_work_time' AND signout=1)")->select(); // 迟到的或者早退的数据
        $arr = array();
        foreach ($result as $v) {
            $arr[$v['username']] = $v['id'];
        }
        $arrCount = count($arr); // 今天迟到早退了的人数
        $result = $sign->where("signtime LIKE '$date%' AND signout=1")->select(); // 今天签到的全部数据
        $all = array();
        foreach ($result as $v) {
            $all[$v['username']] = $v['id'];
        }
        $allCount = count($all); // 今天签到的总人数人数
        $normal = $allCount - $arrCount; // 今天正常的人数
        // print_r($allCount);
        // 发送数据
        $res['late'] = $late;
        $res['noSign'] = $noSign;
        $res['leaveEarly'] = $leaveEarly;
        $res['normal'] = $normal;
        // print_r($noSign);
        echo json_encode($res);
    }


    // 获取当天签到数据
    function getTodaySign() {
        $index = $_REQUEST['index'];
        $first = $index * 10;
        $sign = M('sign s');
        $date = date('Y-m-d');
        $result = $sign->join('users u ON u.username = s.username')->field("s.id, s.username, s.place, DATE_FORMAT(s.signtime,'%m-%d %H:%i') formattime, s.signout, u.cn_name, u.user_img")->where("s.signtime LIKE '$date%'")->limit("$first, 10")->order('id desc')->select();
        // file_put_contents('/var/www/html/aaaaa.php',$sql);
        // var_dump($result);exit;
        echo json_encode($result);
    }

}



