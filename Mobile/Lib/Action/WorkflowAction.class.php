<?php
class WorkflowAction extends Action{
    // 添加工作
    function addWork() {
        $type = $_REQUEST['type'];
        $user = $_REQUEST['user'];
        $starttime = $_REQUEST['starttime'];
        $endtime = $_REQUEST['endtime'];
        $reason = $_REQUEST['reason'];
        $timelength = $_REQUEST['timelength'];
        $leader = $_REQUEST['leader'];
        $leavetype = $_REQUEST['leavetype'];
        $place = $_REQUEST['place'];
        $reimbursement = $_REQUEST['reimbursement'];
        $cc = $_REQUEST['cc'];

        // file_put_contents('/var/www/html/include/data/workflow/qqqqqqq.php', json_encode($_FILES));
        $res = $this->myUpload();
        $file = '';
        foreach ($res as $val) {
            $file .= $val['savename'];
            $file .= '|';
        }
        $file = trim($file, '|');
        // file_put_contents('/var/www/html/include/data/workflow/qqqqqqq.php', json_encode($res));
        // file_put_contents('/var/www/html/qqqqqqq.php', json_encode(  );
        $data = array(
            'type' => $type,
            'user' => $user,
            'starttime' => $starttime,
            'endtime' => $endtime,
            'reason' => $reason,
            'timelength' => $timelength,
            'leader' => $leader,
            'leavetype' => $leavetype,
            'place' => $place,
            'reimbursement' => $reimbursement,
            'cc' => $cc,
            'addtime' => date("Y-m-d H:i:s"),
            'file' => $file,
        );


        $workflow_app = M('workflow_app');
        $res = $workflow_app->add($data);
        if ($res) {
            $user = M('users')->field('ryId')->where('username='.$leader)->find();
            echo json_encode(array('status'=>1,'leader'=>$user['ryId']));
            exit;
        } else {
            echo json_encode(array('status'=>0));
            exit;
        }
    }

    function myUpload () {
        // print_r($arr);exit;
        import('ORG.Net.UploadFile');
        $upload = new UploadFile();// 实例化上传类
        $upload->maxSize = -1; // 设置附件上传大小
        $upload->allowExts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->savePath = './include/data/workflow/'; // 设置附件上传根目录
        $upload->saveRule = 'uniqid';
        $upload->autoSub = true;
        $upload->subType = 'date';
        // 上传文件
        if(!$upload->upload()) {// 上传错误提示错误信息
            $info = $upload->getErrorMsg();
            return $info;
        }else{// 上传成功 获取上传文件信息
            $info =  $upload->getUploadFileInfo();
            return $info;
        }
    }


    // 获取带我审批列表
    function getMyAgreeWork() {
        $user = $_REQUEST['user'];
        $index = $_REQUEST['index'];
        $first = $index * 10;

        $workflow = M('workflow_app w');
        $res = $workflow->join('users u ON u.username = w.user')->field("w.id, w.user, DATE_FORMAT(w.addtime,'%m-%d %H:%i') addtime, w.type, w.status, u.user_img, u.cn_name")->where("w.leader='$user' AND w.status = 0")->limit("$first, 10")->order('addtime desc')->select();

        echo json_encode($res);
    }


    // 获取历史审批列表
    function getHistoryWork() {
        // $user = $_REQUEST['user'];
        $id = $_REQUEST['id'];
        $type = $_REQUEST['type'];
        $index = $_REQUEST['index'];
        $first = $index * 10;
        // 获取该条id对应的user
        $workflow = M('workflow_app w');
        $users = $workflow->where("id=$id")->find();
        $user = $users['user'];
        // 查历史记录
        $res = $workflow->join('users u ON u.username = w.user')->field("w.id, w.user, DATE_FORMAT(w.addtime,'%m-%d %H:%i') addtime, w.type, w.status, u.user_img, u.cn_name")->where("w.user='$user' AND w.type='$type' AND w.status > 0")->limit("$first, 10")->order('addtime desc')->select();

        echo json_encode($res);
    }


    // 获取工作详情
    function getWorkDetail() {
        $id = $_REQUEST['id'];

        $workflow = M('workflow_app');
        $res = $workflow->where("id=$id")->find();
        if ($res['file']) {
            $res['file'] = explode('|', $res['file']);
        }
        echo json_encode($res);

    }


    // 获取我提交的工作
    function getMyCommitWork() {
        $user = $_REQUEST['user'];
        $index = $_REQUEST['index'];
        $first = $index * 10;

        $workflow = M('workflow_app w');
        $res = $workflow->join('users u ON u.username = w.user')->field("w.id, w.user, DATE_FORMAT(w.addtime,'%m-%d %H:%i') addtime, w.type, w.status, u.user_img, u.cn_name")->where("w.user=$user")->limit("$first, 10")->order('addtime desc')->select();

        echo json_encode($res);

    }


    // 获取抄送给我的工作
    function getCcWork() {
        $user = $_REQUEST['user'];
        $index = $_REQUEST['index'];
        $first = $index * 10;

        $workflow = M('workflow_app w');
        $res = $workflow->join('users u ON u.username = w.user')->field("w.id, w.user, DATE_FORMAT(w.addtime,'%m-%d %H:%i') addtime, w.type, w.status, u.user_img, u.cn_name")->where("w.user=$user AND find_in_set('$user', cc)")->limit("$first, 10")->order('addtime desc')->select();
        echo json_encode($res);
    }


    // 转送审批人
    function sendOther () {
        $id = $_REQUEST['id'];
        $cc = $_REQUEST['cc'];
        $data = array(
            'leader' => $cc,
        );
        $workflow = M('workflow_app');
        $res = $workflow->where("id=$id")->save($data);
        if ($res) {
            $users = M('users')->field('ryId')->where('username='.$cc)->find();
            echo json_encode(array('status'=>1, 'cc'=>$users['ryId']));
            exit;
        } else {
            echo json_encode(array('status'=>0));
            exit;
        }
    }

    // 同意/驳回
    function updateStatus() {
        $id = $_REQUEST['id'];
        $status = $_REQUEST['status'];
        $status_reason = $_REQUEST['status_reason'];

        $data = array(
            'status' => $status,
            'status_reason' => $status_reason,
            'status_time' => date('Y-m-d H:i:s'),
        );

        $workflow = M('workflow_app');
        $res = $workflow->where("id=$id")->save($data);
        $user = $workflow->field('user')->where("id=$id")->find();
        $users = M('users')->field('ryId')->where('username='.$user['user'])->find();
        if ($res) {
            echo json_encode(array('status'=>1, 'user'=>$users['ryId']));
            exit;
        } else {
            echo json_encode(array('status'=>0));
            exit;
        }
    }


}



?>
