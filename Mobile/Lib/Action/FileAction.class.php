<?php
class FileAction extends Action{
    function fileList () {
        $index = $_REQUEST['index'];
        $first = $index * 10;
        // echo $index;exit;
        // echo 111;
        $where = "1 ";
        if ($_REQUEST['username']) {
            $username = $_REQUEST['username'];
            $where = " find_in_set('$username', collection_user) ";
        }
        $faq_content = M('faq_content');
        $result = $faq_content->field("id,  DATE_FORMAT(createtime,'%m-%d %H:%i') createtime, title, create_user")->where($where)->limit("$first, 10")->select();
        // $sql = $faq_content->getlastsql();
        // file_put_contents('/var/www/html/aaaaa.php',$sql);
        // var_dump($result);exit;
        echo json_encode($result);
    }

    function getFileDetails () {
        $id = $_REQUEST['id'];
        // echo $id;
        // 获取文档信息
        $faq_content = M('faq_content f');
        $result = $faq_content->field('f.*, users.user_img')->join('users ON users.username = f.create_user')->where('id='.$id)->find();
        // 判断文档类型
        $file = $result['faq_file'];
        $type = strrchr($file, '.');
        $type = ltrim($type, '.');
        // var_dump($file);
        // echo $type;exit;
        // 赋值图标
        if (in_array($type, array('doc', 'docx'))) {
            $icon = '<svg class="icon" aria-hidden="true"><use xlink:href="#icon-word"></use></svg>';
        } elseif (in_array($type, array('xls', 'xlsx'))) {
            $icon = '<svg class="icon" aria-hidden="true"><use xlink:href="#icon-excel"></use></svg>';
        } else {
            $icon = '<svg class="icon" aria-hidden="true"><use xlink:href="#icon-weizhiwenjian"></use></svg>';
        }
        $result['icon'] = $icon;

        // 获取评论信息
        $faq_operating_record = M('faq_operating_record f');
        $comment = $faq_operating_record->field('f.id, f.pid, f.create_time, f.create_user, f.comment_content, users.user_img')->join('users ON users.username = f.create_user')->where("f.operation_type=3 AND f.faq_id=$id")->order("f.create_time desc")->select();
        // $sql = $faq_operating_record->getlastsql();
        foreach ($comment as &$v) {
            if ($v['pid'] > 0) {
                $res = $faq_operating_record->where('id='.$v['pid'])->find();
                $v['pcreate_user'] = $res['create_user'];
            }
        }
        // $sql = $faq_operating_record->getlastsql();

        // 赋值结果集
        $res['file'] = $result;
        $res['comment'] = $comment;
        echo json_encode($res);
    }

    /*// 评论回复树
    function getCategoryTree($daily_id, $pid = 0, $level = 0){
        //$daily_comment = M("daily_comment");
        //$data = $daily_comment->where("daily_id = $daily_id AND pid=$pid")->order("createtime asc")->select();
        $faq_operating_record = M('faq_operating_record');
        $comment = $faq_operating_record->where("operation_type=3 AND faq_id=$daily_id AND pid=$pid")->order("create_time asc")->select();
        //目录树
        $tree = array();
        //层级
        $level++;
        if(!empty($comment)){
            foreach ($comment as &$v){
                $child = $this->getCategoryTree($daily_id,$v['id'], $level);
                $tree[] = array('parent' => $v, 'child' => $child, 'level' => $level);
            }
        }
        return $tree;
    }*/

    // 查询收藏状态
    function collectStatus () {
        $id = $_REQUEST['id'];
        $username = $_REQUEST['username'];
        $faq_content = M('faq_content');
        $result = $faq_content->field('collection_user')->where('id='.$id)->find();
        if (empty($result)) {
            return false;
        }
        $collection = $result['collection_user'];
        $arr = explode(',', $collection);
        if (in_array($username, $arr)) {
            echo json_encode(array('status'=>1));
        } else {
            return false;
        }
    }

    // 收藏文档
    function collectFile () {
        $id = $_REQUEST['id'];
        $username = $_REQUEST['username'];
        $faq_content = M('faq_content');
        $result = $faq_content->field('collection_user')->where('id='.$id)->find();
        if (empty($result['collection_user'])) {
            $data = array('collection_user'=>$username);
        } else {
            $collection = $result['collection_user'];
            $str = $collection.",".$username;
            $data = array('collection_user'=>$str);
        }

        $res = $faq_content->where('id='.$id)->save($data);
        if ($res) {
            echo json_encode(array('status'=>1));
            exit;
        }
    }

    // 取消收藏
    function cancelCollect () {
        $id = $_REQUEST['id'];
        $username = $_REQUEST['username'];
        $faq_content = M('faq_content');
        $result = $faq_content->field('collection_user')->where('id='.$id)->find();
        $collection = $result['collection_user'];
        $arr = explode(',', $collection);
        $key = array_search($username, $arr);
        unset($arr[$key]);
        $str = join(',', $arr);
        $data = array('collection_user'=>$str);
        $res = $faq_content->where('id='.$id)->save($data);
        if ($res) {
            echo json_encode(array('status'=>1));
            exit;
        }
    }

    // 评论
    function comment () {
        $faq_id = $_REQUEST['id'];
        $comment = $_REQUEST['comment'];
        $create_user = $_REQUEST['userid'];

        $data = array(
            'faq_id' => $faq_id,
            'pid' => '0',
            'create_time' => date('Y-m-d H:i:s'),
            'create_user' => $create_user,
            'operation_type' => '3',
            'comment_content' => $comment,
        );

        $faq_operating_record = M('faq_operating_record');
        $result = $faq_operating_record->add($data);
        if ($result) {
            echo json_encode(array('status'=>1));
            exit;
        } else {
            echo json_encode(array('status'=>0));
            exit;
        }
    }

    // 回复评论
    function replyComment () {
        $faq_id = $_REQUEST['faq'];
        $pid = $_REQUEST['cid'];
        $create_user = $_REQUEST['userid'];
        $comment = $_REQUEST['comment'];

        $data = array(
            'faq_id' => $faq_id,
            'pid' => $pid,
            'create_time' => date('Y-m-d H:i:s'),
            'create_user' => $create_user,
            'operation_type' => '3',
            'comment_content' => $comment,
        );

        $faq_operating_record = M('faq_operating_record');
        $result = $faq_operating_record->add($data);
        if ($result) {
            echo json_encode(array('status'=>1));
            exit;
        } else {
            echo json_encode(array('status'=>0));
            exit;
        }
    }


}


?>
